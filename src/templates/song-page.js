import React, { Component } from "react"
import Layout from "../components/layout"
import Seo from "../components/seo"
import SongTab from "../components/song-tab"
import { graphql, Link } from "gatsby"
import RenderSongs from "../components/RenderSongs"
import slugify from "react-slugify"
import ShareIcons from "../components/Share"
import SongInfo from "../components/SongInfo"
import { getImage } from "gatsby-plugin-image"
import AdSense from "react-adsense"

class SongPage extends Component {
  render() {
    var data = this.props.data
    var filteredAlbums = data.albumData.edges.filter(album => {
      return (
        album.node.fields.songslug !==
        data.songData.edges[0].node.fields.songslug
      )
    })

    var relatedSongs = filteredAlbums.map((song, index) => {
      return <RenderSongs data={song} key={index} />
    })

    var nodeData = data.songData.edges[0].node
    var tamilLyrics = nodeData.html
      .split('<pre class="lyrics-native">')[1]
      .split("</pre>")[0]
    var englishLyrics = nodeData.html
      .split('<pre class="lyrics-english">')[1]
      .split("</pre>")[0]

    var lyricsData = {
      tamil: tamilLyrics,
      english: englishLyrics,
      url: "/songs/" + slugify(nodeData.frontmatter.title),
      title: nodeData.frontmatter.title,
    }

    var songInfo = {
      title: nodeData.frontmatter.title,
      album: nodeData.frontmatter.album,
      artist: nodeData.frontmatter.artist,
      name: nodeData.frontmatter.song,
      lyricist: nodeData.frontmatter.lyricist,
    }
    var { title, album, artist, song } = nodeData.frontmatter

    var seoDescription =
      title + ", Movie Name : " + album + ", Music Director : " + artist
    var metaUrl = "https://lyrico.net" + lyricsData.url
    var backToAlbumLink = "/albums/" + slugify(nodeData.frontmatter.album)
    var metaTitle = song + " Song lyrics - " + album

    return (
      <Layout isHomePage={true}>
        <Seo
          title={metaTitle}
          description={seoDescription}
          isBlogPost={true}
          image={getImage(nodeData.frontmatter.image)}
          url={metaUrl}
        ></Seo>
        <div className="lyrico-song-content">
          <div className="lyrico-song-details">
            <div className="lyrico-album-art">
              <iframe
                width="560"
                height="315"
                src={nodeData.frontmatter.youtubeLink}
                title={nodeData.frontmatter.title}
                allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
              ></iframe>
            </div>
            <div className="lyrico-google-adds">
              <AdSense.Google
                client="ca-pub-7536246717624121"
                slot="9166584323"
                style={{
                  display: "block",
                  textAlign: "center",
                  marginBottom: "20px",
                }}
                format="auto"
              />
            </div>
            <div className="lyrico-song-artist-details">
              <h2 className="lyrico-capitalize">
                {nodeData.frontmatter.title}
              </h2>
              <div className="lyrico-share-icons-wrapper">
                <Link to={backToAlbumLink}>
                  <div className="lyrico-back-to-album">Back To Album</div>
                </Link>
              </div>
            </div>
          </div>
          <SongInfo data={songInfo}></SongInfo>
          <div className="lyrico-google-adds">
            <AdSense.Google
              className="adsbygoogle"
              style={{
                display: "block",
                textAlign: "center",
                marginBottom: "20px",
              }}
              layout="in-article"
              format="fluid"
              client="ca-pub-7536246717624121"
              slot="1671237681"
            />
          </div>
          <SongTab lyrics={lyricsData} typeData={data.songTypeData}></SongTab>
          <div className="lyrico-google-adds">
            <AdSense.Google
              className="adsbygoogle"
              style={{ display: "block", textAlign: "center" }}
              layout="in-article"
              format="fluid"
              client="ca-pub-7536246717624121"
              slot="1671237681"
            />
          </div>
          <ShareIcons data={lyricsData} />
          {relatedSongs.length ? (
            <div className="lyrico-related-songs">
              <div className="lyrico-related-songs-title">Related Songs</div>
              <div className="lyrico-related-songs-container">
                {relatedSongs}
              </div>
            </div>
          ) : (
            ""
          )}
        </div>
      </Layout>
    )
  }
}

export const songQuery = graphql`
  query songQuery($songslug: String, $albumName: String, $type: String) {
    songData: allMarkdownRemark(
      filter: { fields: { songslug: { eq: $songslug } } }
    ) {
      edges {
        node {
          html
          frontmatter {
            artist
            album
            director
            path
            title
            lyricist
            song
            singers
            youtubeLink
            image {
              childImageSharp {
                gatsbyImageData(layout: FULL_WIDTH)
              }
            }
          }
          fields {
            slug
            songslug
            albumName
          }
        }
      }
    }
    albumData: allMarkdownRemark(
      filter: { frontmatter: { album: { eq: $albumName } } }
    ) {
      edges {
        node {
          html
          frontmatter {
            artist
            album
            director
            path
            title
            lyricist
            song
            youtubeLink
            singers
            image {
              childImageSharp {
                gatsbyImageData(layout: FULL_WIDTH)
              }
            }
          }
          fields {
            slug
            songslug
            albumName
          }
        }
      }
    }
    songTypeData: allMarkdownRemark(
      limit: 5
      filter: { fields: { type: { eq: $type } } }
    ) {
      edges {
        node {
          html
          frontmatter {
            artist
            album
            director
            path
            title
            lyricist
            song
            youtubeLink
            image {
              childImageSharp {
                gatsbyImageData(layout: FULL_WIDTH)
              }
            }
          }
          fields {
            slug
            songslug
            albumName
            type
          }
        }
      }
    }
  }
`

export default SongPage
