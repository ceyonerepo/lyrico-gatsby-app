import React from 'react';
import { graphql } from 'gatsby';
import Layout from '../components/layout';
import Seo from '../components/seo';
import RenderSongs from '../components/RenderSongs';
import { GatsbyImage } from "gatsby-plugin-image";
import { Badge } from 'reactstrap';

function AlbumSongsList({ data }) {
    var songsList = data.allMarkdownRemark.edges.map((node, index) => {
        return <RenderSongs data={node} key={index}></RenderSongs>
    });

    var { title, album, artist, image, director } = data.allMarkdownRemark.edges[0].node.frontmatter;
    var seoDescription = title + ', Movie Name : ' + album + ', Music Director : ' + artist;
    var metaTitle = album + ' movie lyrics';
    var metaUrl = 'https://lyrico.net/albums/' + album;

    return (
      <Layout>
        <Seo
          title={metaTitle}
          description={seoDescription}
          isBlogPost={true}
          image={image.childImageSharp.gatsbyImageData.src}
          url={metaUrl}
        ></Seo>
        <div className="lyrico-song-details-container">
          <div className="lyrico-song-album-art">
            <GatsbyImage
              image={image.childImageSharp.gatsbyImageData}
              className="card-image-top"
              width="100%"
              alt={title}
            />
          </div>
          <div className="lyrico-song-director-details">
            <div className="lyrico-artists">
              <span>Director : </span>
              <Badge color="lyrico text-white">{director}</Badge>
            </div>
            <div className="lyrico-artists">
              <span>Music Director : </span>
              <Badge color="lyrico text-white">{artist}</Badge>
            </div>
          </div>
          <div className="lyrico-song-details-list">{songsList}</div>
        </div>
      </Layout>
    )
}

export const singleAlbumSongListQuery = graphql`query singleAlbumSongListQuery($slug: String!) {
  allMarkdownRemark(filter: {fields: {slug: {eq: $slug}}}) {
    edges {
      node {
        frontmatter {
          artist
          singers
          album
          director
          path
          title
          lyricist
          song
          image {
            childImageSharp {
              gatsbyImageData(layout: FULL_WIDTH)
            }
          }
        }
        fields {
          slug
          songslug
        }
      }
    }
  }
}
`

export default AlbumSongsList;