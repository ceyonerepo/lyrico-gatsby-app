import * as React from 'react';
import { graphql } from "gatsby"
import Layout from '../components/layout';
import RenderAlbums from '../components/RenderAlbums';

function renderAlbums(data) {
  var addedAlbums = []

  var albumData = data.allMarkdownRemark.edges.map((edge, index) => {
    if (addedAlbums.indexOf(edge.node.frontmatter.album) === -1) {
      addedAlbums.push(edge.node.frontmatter.album)
      return <RenderAlbums key={index} data={edge} />
    }
  })

  return albumData
}

export default function YearAlbums({ data }) {
  return (
    <Layout>
      <div className="home-songs-list">{renderAlbums(data)}</div>
    </Layout>
  )
}

export const YearAlbumsQuery = graphql`
         query YearAlbumsQuery($year: String!) {
           allMarkdownRemark(
             filter: { frontmatter: { date: { regex: $year } } }
           ) {
             edges {
               node {
                 frontmatter {
                   artist
                   album
                   path
                   image {
                     childImageSharp {
                       gatsbyImageData(layout: FIXED, width: 330, height: 200)
                     }
                   }
                 }
               }
             }
           }
         }
       `