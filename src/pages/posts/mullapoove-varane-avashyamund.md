---
title: "mullapoove song lyrics"
album: "Varane Avashyamund"
artist: "Alphons Joseph"
lyricist: "Santhosh Varma"
director: "Anoop Sathyan"
path: "/albums/varane-avashyamund-lyrics"
song: "Mullapoove"
image: ../../images/albumart/varane-avashyamund.jpg
date: 2020-02-07
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/QBLFS90Hmd8"
type: "happy"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mullappoove Ninneppolum Vellum Pennaanival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullappoove Ninneppolum Vellum Pennaanival"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanumthorum Kaanaan Thonnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanumthorum Kaanaan Thonnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnin Monchullaval Pakalozhukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnin Monchullaval Pakalozhukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaavo Kolussaniyunna Kavithayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaavo Kolussaniyunna Kavithayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaarum Chodikkille Chummaa Nee Poy Marayalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaarum Chodikkille Chummaa Nee Poy Marayalle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karayum Kadalum Thanchathilaadi Ivalekkandaa Kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karayum Kadalum Thanchathilaadi Ivalekkandaa Kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtam Koodi Pathivaay Kaanum Palaraanelum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtam Koodi Pathivaay Kaanum Palaraanelum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakalppooram Kaanaan Ivalodoppam Koodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakalppooram Kaanaan Ivalodoppam Koodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallonamaadaanaay Kothiyullolaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallonamaadaanaay Kothiyullolaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruthe Nilkkumbol Moolippadunnolaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruthe Nilkkumbol Moolippadunnolaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Janmam Theeraa Kanneril Neenthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Janmam Theeraa Kanneril Neenthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiri Thooki Nilkkum Pennaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiri Thooki Nilkkum Pennaanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mullappoove Ninneeppolum Vellum Pennanival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullappoove Ninneeppolum Vellum Pennanival"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanumthorum Kaanaan Thonnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanumthorum Kaanaan Thonnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnin Monchullaval Pakalozhukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnin Monchullaval Pakalozhukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaavo Kolussaniyunna Kavithayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaavo Kolussaniyunna Kavithayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaarum Chodikkille Chumma Nee Poy Marayalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaarum Chodikkille Chumma Nee Poy Marayalle"/>
</div>
</pre>
