---
title: 'vaathi raid song lyrics'
album: 'Master'
artist: 'Anirudh Ravichander'
lyricist: 'Arivu'
director: 'Lokesh Kanagaraj'
path: '/albums/master-song-lyrics'
song: 'Vaathi Raid'
image: ../../images/albumart/master.jpg
date: 2021-01-13
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/eHoIUNY-bG4'
type: 'mass'
singers: 
- Anirudh Ravichander
- Arivu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Aana aavanna
<input type="checkbox" class="lyrico-select-lyric-line" value="Aana aavanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Apna time na
<input type="checkbox" class="lyrico-select-lyric-line" value="Apna time na"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanganna vanakkamna
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanganna vanakkamna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu na
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu na"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana aavanna
<input type="checkbox" class="lyrico-select-lyric-line" value="Aana aavanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Apna time na
<input type="checkbox" class="lyrico-select-lyric-line" value="Apna time na"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanganna vanakkamna
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanganna vanakkamna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ulagatharam ullooru vaathiyaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulagatharam ullooru vaathiyaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooki pottu saathuvaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Thooki pottu saathuvaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti thatti thookuvaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Thatti thatti thookuvaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta pulla thirunthida
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketta pulla thirunthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattam thaanda edam
<input type="checkbox" class="lyrico-select-lyric-line" value="Sattam thaanda edam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla vanthu thappu senja
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla vanthu thappu senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi RAIDU varum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi RAIDU varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vanthu wanteda-avey maatidatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanthu wanteda-avey maatidatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu poidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paathu poidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanba nallamari sollum podhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanba nallamari sollum podhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu poidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kettu poidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Namma vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mela yethi vittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mela yethi vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthudaadha vera side-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarthudaadha vera side-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha vaathi kitta vechikaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha vaathi kitta vechikaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oram poidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oram poidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Namma vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Bangam bangam bathiladi
<input type="checkbox" class="lyrico-select-lyric-line" value="Bangam bangam bathiladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhum pondhum saravedi
<input type="checkbox" class="lyrico-select-lyric-line" value="Sandhum pondhum saravedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaa pannum athiradi
<input type="checkbox" class="lyrico-select-lyric-line" value="Annaa pannum athiradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaathi yaaru?…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi yaaru?…"/>
</div>
<div class="lyrico-lyrics-wrapper">THALAPATHY
<input type="checkbox" class="lyrico-select-lyric-line" value="THALAPATHY"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Black-u thangam de
<input type="checkbox" class="lyrico-select-lyric-line" value="Black-u thangam de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatu singam de
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatu singam de"/>
</div>
<div class="lyrico-lyrics-wrapper">Navuru de
<input type="checkbox" class="lyrico-select-lyric-line" value="Navuru de"/>
</div>
<div class="lyrico-lyrics-wrapper">Walk-ah paaru geththaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Walk-ah paaru geththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gate-ah moodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee gate-ah moodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tara raraaa. taraaraaaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Tara raraaa. taraaraaaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Tara raraaa. taraaraaaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Tara raraaa. taraaraaaah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">They call me master
<input type="checkbox" class="lyrico-select-lyric-line" value="They call me master"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatrangal varum faster
<input type="checkbox" class="lyrico-select-lyric-line" value="Maatrangal varum faster"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Karpi ondru ser
<input type="checkbox" class="lyrico-select-lyric-line" value="Karpi ondru ser"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri kondu seer
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetri kondu seer"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruthura vaathiyaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Thiruthura vaathiyaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Koor vizhiyila paar
<input type="checkbox" class="lyrico-select-lyric-line" value="Koor vizhiyila paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vagupparai nadungum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vagupparai nadungum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irumbhu karam adangum
<input type="checkbox" class="lyrico-select-lyric-line" value="Irumbhu karam adangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arasa maram ozhukkam
<input type="checkbox" class="lyrico-select-lyric-line" value="Arasa maram ozhukkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavarum neram
<input type="checkbox" class="lyrico-select-lyric-line" value="Thavarum neram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaathi raidu varum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pakkuvoma sollum pothae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pakkuvoma sollum pothae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettukonga chellam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kettukonga chellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholla panna vathiyaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Tholla panna vathiyaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kambu vanthu konjum …ayoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Kambu vanthu konjum …ayoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vendaam pilippu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vendaam pilippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu warning sirippu
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu warning sirippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu thaanavae oththukitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandhu thaanavae oththukitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Polaam chamaththu kutty
<input type="checkbox" class="lyrico-select-lyric-line" value="Polaam chamaththu kutty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennamaa anga saththam
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennamaa anga saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha
<input type="checkbox" class="lyrico-select-lyric-line" value="Vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooku mela raththam
<input type="checkbox" class="lyrico-select-lyric-line" value="Mooku mela raththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanba
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga odi olinchaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enga odi olinchaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi kannu thookkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi kannu thookkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaathiyaaru pecha ketta
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathiyaaru pecha ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai mela yerum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhkai mela yerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevailladha vela paartha
<input type="checkbox" class="lyrico-select-lyric-line" value="Thevailladha vela paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti poda nerum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mutti poda nerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tara raraaa taraaraaaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Tara raraaa taraaraaaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Tara raraaa taraaraaaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Tara raraaa taraaraaaah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nikkaadha nikkaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nikkaadha nikkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupadi sikkaadha sikkaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Marupadi sikkaadha sikkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Orumurai thottalae kettanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Orumurai thottalae kettanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathiladi pakkava veppaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Bathiladi pakkava veppaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Theruvil nadakkura kodumaiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Theruvil nadakkura kodumaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadakkura thalaimurai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadakkura thalaimurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padikkira thamizh-lil irukkuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Padikkira thamizh-lil irukkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhumarai edhukunu
<input type="checkbox" class="lyrico-select-lyric-line" value="Podhumarai edhukunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagura pazhakamum enakillae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vilagura pazhakamum enakillae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum ennodu irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Eppothum ennodu irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaalam unnai uraikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pattaalam unnai uraikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattayam mannai thirattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattayam mannai thirattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Baedhangal illadhirukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Baedhangal illadhirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadengal kannaai irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Naadengal kannaai irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idhu vara poruthhom
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu vara poruthhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Chumma
<input type="checkbox" class="lyrico-select-lyric-line" value="Chumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi vizhum inimae
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi vizhum inimae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumma (2  Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Gumma"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thattam thattam thaladaladi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thattam thattam thaladaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandum sindum udharumdi
<input type="checkbox" class="lyrico-select-lyric-line" value="Nandum sindum udharumdi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla vandha powera-di
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla vandha powera-di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Anna yaaru?…
<input type="checkbox" class="lyrico-select-lyric-line" value="Anna yaaru?…"/>
</div>
<div class="lyrico-lyrics-wrapper">THALAPATHY..
<input type="checkbox" class="lyrico-select-lyric-line" value="THALAPATHY.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Black-u thangam de
<input type="checkbox" class="lyrico-select-lyric-line" value="Black-u thangam de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatu singam de
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatu singam de"/>
</div>
<div class="lyrico-lyrics-wrapper">Navuru de
<input type="checkbox" class="lyrico-select-lyric-line" value="Navuru de"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu beast-u mode-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu beast-u mode-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Tara raraaa. taraaraaaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Tara raraaa. taraaraaaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Tara raraaa. taraaraaaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Tara raraaa. taraaraaaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Tara raraaa. taraaraaaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Tara raraaa. taraaraaaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Tara raraaa. taraaraaaah
<input type="checkbox" class="lyrico-select-lyric-line" value="Tara raraaa. taraaraaaah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rockstar for the Master
<input type="checkbox" class="lyrico-select-lyric-line" value="Rockstar for the Master"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vanthu wanteda-avae maatidatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanthu wanteda-avae maatidatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu poidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paathu poidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanba nallamari sollum podhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanba nallamari sollum podhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettu poidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kettu poidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Namma vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mela yethi vittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mela yethi vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthuradha vera side-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarthuradha vera side-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha vaathi kitta vechikaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha vaathi kitta vechikaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oram poidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oram poidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Namma vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu -da
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu -da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aana aavanna
<input type="checkbox" class="lyrico-select-lyric-line" value="Aana aavanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Apna time na
<input type="checkbox" class="lyrico-select-lyric-line" value="Apna time na"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanganna vanakkamna
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanganna vanakkamna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaathi raidu na (2  Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaathi raidu na"/></div>
</div>
</pre>