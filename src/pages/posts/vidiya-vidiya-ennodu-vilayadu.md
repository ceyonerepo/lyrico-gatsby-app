---
title: "vidiya vidiya song lyrics"
album: "Ennodu Vilayadu"
artist: "A. Moses - Sudharshan M. Kumar"
lyricist: "Viveka"
director: "Arun Krishnaswami"
path: "/albums/ennodu-vilayadu-lyrics"
song: "Vidiya Vidiya"
image: ../../images/albumart/ennodu-vilayadu.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TCnIpT4q70M"
type: "mass"
singers:
  - Vijay Prakash
  - B Mac
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vidiya vidiya ponnukkitta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiya vidiya ponnukkitta "/>
</div>
<div class="lyrico-lyrics-wrapper">maatta vacha aandavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatta vacha aandavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">eliya kaatti enna neeyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eliya kaatti enna neeyum "/>
</div>
<div class="lyrico-lyrics-wrapper">veliya konjam konduvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veliya konjam konduvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">veedu muzhukka theadiyaachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veedu muzhukka theadiyaachu "/>
</div>
<div class="lyrico-lyrics-wrapper">kannu muzhiyum pidhungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu muzhiyum pidhungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">anda vandha enakku eppo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anda vandha enakku eppo "/>
</div>
<div class="lyrico-lyrics-wrapper">anda vearu kazharudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anda vearu kazharudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Robey robaa ah robey robaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robey robaa ah robey robaa"/>
</div>
<div class="lyrico-lyrics-wrapper">robey robaa ah robey robaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="robey robaa ah robey robaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kezhattu poonakkooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kezhattu poonakkooda "/>
</div>
<div class="lyrico-lyrics-wrapper">kadavulaaga theriyudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavulaaga theriyudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">odanja elipporiyum koayilaanadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odanja elipporiyum koayilaanadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">iruttu Roomu ennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruttu Roomu ennai "/>
</div>
<div class="lyrico-lyrics-wrapper">imsai panni paarkkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imsai panni paarkkudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkum elikkumaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkum elikkumaana "/>
</div>
<div class="lyrico-lyrics-wrapper">rosam neeludhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rosam neeludhey"/>
</div>
<div class="lyrico-lyrics-wrapper">gajamuganey gajamuganey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gajamuganey gajamuganey "/>
</div>
<div class="lyrico-lyrics-wrapper">undhan vaaganam engedaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan vaaganam engedaa"/>
</div>
<div class="lyrico-lyrics-wrapper">gajamuganey gajamuganey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gajamuganey gajamuganey "/>
</div>
<div class="lyrico-lyrics-wrapper">unna kenjuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kenjuren"/>
</div>
“<div class="lyrico-lyrics-wrapper">Help me daa”
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Help me daa”"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum serndhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum serndhu "/>
</div>
<div class="lyrico-lyrics-wrapper">poagum paadhai ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poagum paadhai ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">meagammaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meagammaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkam neeyum nirkkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkam neeyum nirkkum "/>
</div>
<div class="lyrico-lyrics-wrapper">nearam thookkam ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nearam thookkam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">poagum thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poagum thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">rammy rosu soodhu ellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rammy rosu soodhu ellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">boadhai endru kidandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boadhai endru kidandheney"/>
</div>
<div class="lyrico-lyrics-wrapper">pennin vaasam kanda pinney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennin vaasam kanda pinney"/>
</div>
<div class="lyrico-lyrics-wrapper">matradhellaam marandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matradhellaam marandheney"/>
</div>
<div class="lyrico-lyrics-wrapper">nuraiyeeral kaaichalellaam uraiyaadudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuraiyeeral kaaichalellaam uraiyaadudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaasai nadhi indru karaimeerudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaasai nadhi indru karaimeerudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaaley udalellaam siragaanadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaaley udalellaam siragaanadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">marandhen ulagai marandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marandhen ulagai marandheney"/>
</div>
<div class="lyrico-lyrics-wrapper">simo hamo hamo hamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="simo hamo hamo hamo"/>
</div>
<div class="lyrico-lyrics-wrapper">simo hamo hamo hamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="simo hamo hamo hamo"/>
</div>
<div class="lyrico-lyrics-wrapper">hO O hO O  hO O
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hO O hO O  hO O"/>
</div>
<div class="lyrico-lyrics-wrapper">hO O hO O
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hO O hO O"/>
</div>
</pre>
