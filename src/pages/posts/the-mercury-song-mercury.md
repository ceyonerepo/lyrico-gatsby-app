---
title: "the mercury song song lyrics"
album: "Mercury"
artist: "Santhosh Narayanan"
lyricist: "Sayeed Quadri"
director: "Karthik Subbaraj"
path: "/albums/mercury-song-lyrics"
song: "The Mercury Song"
image: ../../images/albumart/mercury.jpg
date: 2018-04-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LEQz6gMVqdA"
type: "title track"
singers:
  - Haricharan
  - Gajendra Verma
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mercury sa tera pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mercury sa tera pyar"/>
</div>
<div class="lyrico-lyrics-wrapper">mercury ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mercury ho"/>
</div>
<div class="lyrico-lyrics-wrapper">sa tera pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa tera pyar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mercury sa tera pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mercury sa tera pyar"/>
</div>
<div class="lyrico-lyrics-wrapper">mercury ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mercury ho"/>
</div>
<div class="lyrico-lyrics-wrapper">sa tera pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa tera pyar"/>
</div>
<div class="lyrico-lyrics-wrapper">sar pe mere chada h
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sar pe mere chada h"/>
</div>
<div class="lyrico-lyrics-wrapper">aise tera bukhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aise tera bukhaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mercury sa tera pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mercury sa tera pyar"/>
</div>
<div class="lyrico-lyrics-wrapper">mercury ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mercury ho"/>
</div>
<div class="lyrico-lyrics-wrapper">sa tera pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa tera pyar"/>
</div>
<div class="lyrico-lyrics-wrapper">phir jana aisi kya ruswai hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="phir jana aisi kya ruswai hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tu mere dard-e-dil ki dawai hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu mere dard-e-dil ki dawai hai"/>
</div>
<div class="lyrico-lyrics-wrapper">tu jo mile aye karaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu jo mile aye karaar"/>
</div>
<div class="lyrico-lyrics-wrapper">majboori samjho naa yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="majboori samjho naa yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mercury ho sa tera pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mercury ho sa tera pyar"/>
</div>
<div class="lyrico-lyrics-wrapper">fir jana esi kya ruswai h
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fir jana esi kya ruswai h"/>
</div>
<div class="lyrico-lyrics-wrapper">tu mere darde dil ki dawai h
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu mere darde dil ki dawai h"/>
</div>
<div class="lyrico-lyrics-wrapper">tu jo mile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu jo mile"/>
</div>
<div class="lyrico-lyrics-wrapper">aye karaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aye karaar"/>
</div>
<div class="lyrico-lyrics-wrapper">majburi samjho na yar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="majburi samjho na yar"/>
</div>
<div class="lyrico-lyrics-wrapper">mercury sa tera pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mercury sa tera pyar"/>
</div>
<div class="lyrico-lyrics-wrapper">mercury ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mercury ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sa tera pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa tera pyar"/>
</div>
<div class="lyrico-lyrics-wrapper">hey girl can i talk to u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey girl can i talk to u"/>
</div>
<div class="lyrico-lyrics-wrapper">wana tell u somethin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wana tell u somethin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">what i wana do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what i wana do"/>
</div>
<div class="lyrico-lyrics-wrapper">everybody wana be with u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="everybody wana be with u"/>
</div>
<div class="lyrico-lyrics-wrapper">run be the one who loves u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run be the one who loves u"/>
</div>
<div class="lyrico-lyrics-wrapper">hey girl when i look at u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey girl when i look at u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">my knees go weak at the sight of u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="my knees go weak at the sight of u"/>
</div>
<div class="lyrico-lyrics-wrapper">aye girl i’m crazy about u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aye girl i’m crazy about u"/>
</div>
<div class="lyrico-lyrics-wrapper">wana can leave the world just be with u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wana can leave the world just be with u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hont mere tharr-tharr kanpe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hont mere tharr-tharr kanpe"/>
</div>
<div class="lyrico-lyrics-wrapper">hontho ki teri garmi h mange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hontho ki teri garmi h mange"/>
</div>
<div class="lyrico-lyrics-wrapper">dhup sa mera tapta badann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhup sa mera tapta badann"/>
</div>
<div class="lyrico-lyrics-wrapper">zulf ki teri narmi h mange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zulf ki teri narmi h mange"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tu mera hakim h ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu mera hakim h ha"/>
</div>
<div class="lyrico-lyrics-wrapper">mujhko tera marj hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mujhko tera marj hua"/>
</div>
<div class="lyrico-lyrics-wrapper">tu mera hakim h ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu mera hakim h ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mujhko tera marj hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mujhko tera marj hua"/>
</div>
<div class="lyrico-lyrics-wrapper">tera ishq hi meri shipha h
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tera ishq hi meri shipha h"/>
</div>
<div class="lyrico-lyrics-wrapper">fir jana esi kya ruswai h
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fir jana esi kya ruswai h"/>
</div>
<div class="lyrico-lyrics-wrapper">tu mere darde dil ki dawai h
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu mere darde dil ki dawai h"/>
</div>
<div class="lyrico-lyrics-wrapper">tu jo mile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu jo mile"/>
</div>
<div class="lyrico-lyrics-wrapper">aye karaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aye karaar"/>
</div>
<div class="lyrico-lyrics-wrapper">majburi samjho na yar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="majburi samjho na yar"/>
</div>
<div class="lyrico-lyrics-wrapper">mercury sa tera pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mercury sa tera pyar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mercury ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mercury ho"/>
</div>
<div class="lyrico-lyrics-wrapper">sa tera pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa tera pyar"/>
</div>
<div class="lyrico-lyrics-wrapper">do u have a dancer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do u have a dancer"/>
</div>
<div class="lyrico-lyrics-wrapper">do u have a dancer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do u have a dancer"/>
</div>
<div class="lyrico-lyrics-wrapper">do u have a dancer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do u have a dancer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mera ye hal kia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mera ye hal kia"/>
</div>
<div class="lyrico-lyrics-wrapper">kyu mujhe behal kia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kyu mujhe behal kia"/>
</div>
<div class="lyrico-lyrics-wrapper">jab koyi sawal kia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jab koyi sawal kia"/>
</div>
<div class="lyrico-lyrics-wrapper">kyu nahi jawab dia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kyu nahi jawab dia"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">do u have a dancer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do u have a dancer"/>
</div>
<div class="lyrico-lyrics-wrapper">tell me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tell me now"/>
</div>
<div class="lyrico-lyrics-wrapper">do u have a dancer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do u have a dancer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tell me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tell me now"/>
</div>
<div class="lyrico-lyrics-wrapper">do u have a dancer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do u have a dancer"/>
</div>
<div class="lyrico-lyrics-wrapper">tell me now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tell me now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">phir jana esi kya ruswaai hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="phir jana esi kya ruswaai hai"/>
</div>
<div class="lyrico-lyrics-wrapper">tu mere darde dil ki dawai hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu mere darde dil ki dawai hai"/>
</div>
<div class="lyrico-lyrics-wrapper">tu jo mile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu jo mile"/>
</div>
<div class="lyrico-lyrics-wrapper">aye karaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aye karaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mercury sa tera pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mercury sa tera pyar"/>
</div>
<div class="lyrico-lyrics-wrapper">mercury ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mercury ho"/>
</div>
<div class="lyrico-lyrics-wrapper">sa tera pyar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa tera pyar"/>
</div>
</pre>
