---
title: "joodham song lyrics"
album: "Pachchis"
artist: "Smaran Sai"
lyricist: "Niklesh Sunkoji"
director: "Sri Krishna - Rama Sai"
path: "/albums/pachchis-lyrics"
song: "Joodham - mukkala dekkulo"
image: ../../images/albumart/pachchis.jpg
date: 2021-06-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/rLirdxAzQsM"
type: "happy"
singers:
  - Smaran Sai
  - Saurabh Chaganty
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">mukkala dekkulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mukkala dekkulo"/>
</div>
<div class="lyrico-lyrics-wrapper">ekkadi viddooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekkadi viddooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">okamaata chepputhanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okamaata chepputhanu"/>
</div>
<div class="lyrico-lyrics-wrapper">nethiloki dhoorchukora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethiloki dhoorchukora"/>
</div>
<div class="lyrico-lyrics-wrapper">thammi ori thammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thammi ori thammi"/>
</div>
<div class="lyrico-lyrics-wrapper">okkamukka chalu life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okkamukka chalu life"/>
</div>
<div class="lyrico-lyrics-wrapper">mukkalayye aata idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mukkalayye aata idhi"/>
</div>
<div class="lyrico-lyrics-wrapper">thammi arey thammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thammi arey thammi"/>
</div>
<div class="lyrico-lyrics-wrapper">moothithippi mukkalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moothithippi mukkalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">muddimeeda thannelope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muddimeeda thannelope"/>
</div>
<div class="lyrico-lyrics-wrapper">nammi nanu nammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammi nanu nammi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasulanni koodavetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasulanni koodavetti"/>
</div>
<div class="lyrico-lyrics-wrapper">jaarukora dhaarivatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaarukora dhaarivatti"/>
</div>
<div class="lyrico-lyrics-wrapper">kampulona kaaluvetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kampulona kaaluvetti"/>
</div>
<div class="lyrico-lyrics-wrapper">kadagaraani buradha mutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadagaraani buradha mutti"/>
</div>
<div class="lyrico-lyrics-wrapper">eheeyamma jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eheeyamma jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">ongavette paisalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ongavette paisalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">pandavette juttu motham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandavette juttu motham"/>
</div>
<div class="lyrico-lyrics-wrapper">iidavatee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iidavatee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rangula jhoodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangula jhoodham"/>
</div>
<div class="lyrico-lyrics-wrapper">dharjaga dhigajaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharjaga dhigajaare"/>
</div>
<div class="lyrico-lyrics-wrapper">raajulu saitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raajulu saitham"/>
</div>
<div class="lyrico-lyrics-wrapper">yudhalle moddhalette ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yudhalle moddhalette ee"/>
</div>
<div class="lyrico-lyrics-wrapper">gatthara jhoodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gatthara jhoodham"/>
</div>
<div class="lyrico-lyrics-wrapper">siri maikam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siri maikam"/>
</div>
<div class="lyrico-lyrics-wrapper">lo muncheti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lo muncheti"/>
</div>
<div class="lyrico-lyrics-wrapper">rangula ratnam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangula ratnam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pakkodidhi pantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkodidhi pantham"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhara "/>
</div>
<div class="lyrico-lyrics-wrapper">soodara ah theeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soodara ah theeru"/>
</div>
<div class="lyrico-lyrics-wrapper">edhutodivi ethulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhutodivi ethulu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhara"/>
</div>
<div class="lyrico-lyrics-wrapper">aagadhu ah joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagadhu ah joru"/>
</div>
<div class="lyrico-lyrics-wrapper">aapakunte aagadalu thammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aapakunte aagadalu thammi"/>
</div>
<div class="lyrico-lyrics-wrapper">emaipothavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emaipothavo"/>
</div>
<div class="lyrico-lyrics-wrapper">anniveeki angatalakosthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anniveeki angatalakosthe"/>
</div>
<div class="lyrico-lyrics-wrapper">anna thammulu okate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anna thammulu okate"/>
</div>
<div class="lyrico-lyrics-wrapper">pekata pekate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pekata pekate"/>
</div>
<div class="lyrico-lyrics-wrapper">neethulu cheppe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethulu cheppe"/>
</div>
<div class="lyrico-lyrics-wrapper">dhorale ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhorale ani"/>
</div>
<div class="lyrico-lyrics-wrapper">nammithe porapaate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammithe porapaate"/>
</div>
<div class="lyrico-lyrics-wrapper">lothugunna bhaviloki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lothugunna bhaviloki"/>
</div>
<div class="lyrico-lyrics-wrapper">thongichu sudendhukanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thongichu sudendhukanta"/>
</div>
<div class="lyrico-lyrics-wrapper">thammi ori thammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thammi ori thammi"/>
</div>
<div class="lyrico-lyrics-wrapper">oobilona chikkukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oobilona chikkukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">oopirinka aaduthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oopirinka aaduthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">thammi ori thammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thammi ori thammi"/>
</div>
<div class="lyrico-lyrics-wrapper">aakulanni vadipoina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aakulanni vadipoina"/>
</div>
<div class="lyrico-lyrics-wrapper">chettulekka kakamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chettulekka kakamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">nammi nanu nammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammi nanu nammi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasulanni koodavetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasulanni koodavetti"/>
</div>
<div class="lyrico-lyrics-wrapper">jaarukora dhaarivatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaarukora dhaarivatti"/>
</div>
<div class="lyrico-lyrics-wrapper">kampulona kaaluvetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kampulona kaaluvetti"/>
</div>
<div class="lyrico-lyrics-wrapper">kadagaraani buradha mutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadagaraani buradha mutti"/>
</div>
<div class="lyrico-lyrics-wrapper">eheeyamma jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eheeyamma jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">ongavette paisalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ongavette paisalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">pandavette 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandavette "/>
</div>
<div class="lyrico-lyrics-wrapper">juttu motham oodavatte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="juttu motham oodavatte"/>
</div>
<div class="lyrico-lyrics-wrapper">rajyale munigeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rajyale munigeti"/>
</div>
<div class="lyrico-lyrics-wrapper">rangula jhoodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangula jhoodham"/>
</div>
<div class="lyrico-lyrics-wrapper">dharjaga dhigajaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharjaga dhigajaare"/>
</div>
<div class="lyrico-lyrics-wrapper">raajulu saitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raajulu saitham"/>
</div>
<div class="lyrico-lyrics-wrapper">yudhalle moddhalette
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yudhalle moddhalette"/>
</div>
<div class="lyrico-lyrics-wrapper">ee gatthara jhoodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee gatthara jhoodham"/>
</div>
<div class="lyrico-lyrics-wrapper">siri maikam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siri maikam"/>
</div>
<div class="lyrico-lyrics-wrapper">lo muncheti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lo muncheti"/>
</div>
<div class="lyrico-lyrics-wrapper">rangula ratnam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangula ratnam"/>
</div>
</pre>
