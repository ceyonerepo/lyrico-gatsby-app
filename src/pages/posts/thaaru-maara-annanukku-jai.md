---
title: "thaaru maara song lyrics"
album: "Annanukku Jai"
artist: "Arrol Corelli"
lyricist: "Rajkumar"
director: "Rajkumar"
path: "/albums/annanukku-jai-lyrics"
song: "Satti Melatha"
image: ../../images/albumart/annanukku-jai.jpg
date: 2018-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Yp-NKUBuHOk"
type: "love"
singers:
  - Deva
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">La la la la la la ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la la la la la ha"/>
</div>
<div class="lyrico-lyrics-wrapper">La la la la la la ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la la la la la ha"/>
</div>
<div class="lyrico-lyrics-wrapper">La la la la la la ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la la la la la ha"/>
</div>
<div class="lyrico-lyrics-wrapper">La la la la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la la la la la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru maara manasu paayuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru maara manasu paayuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasathi onna paarka thavikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasathi onna paarka thavikuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalatuthae un pinnala oduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalatuthae un pinnala oduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru maara manasu paayuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru maara manasu paayuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasathi onna paarka thavikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasathi onna paarka thavikuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalatuthae un pinnala oduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalatuthae un pinnala oduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaru maara manasu paayuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru maara manasu paayuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan sonu mittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sonu mittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee theanu mittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee theanu mittaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pappara mittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pappara mittaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee balli mittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee balli mittaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mittaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan mittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan mittaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mittaai mittaai mittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mittaai mittaai mittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttai pola kannazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttai pola kannazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappa mookku perazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappa mookku perazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae nee en aasai nayagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae nee en aasai nayagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa maram kallu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa maram kallu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thithikira thegatura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithikira thegatura"/>
</div>
<div class="lyrico-lyrics-wrapper">Erangatha soora bodhai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangatha soora bodhai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaraama chellam konja vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaraama chellam konja vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae unnai thaanga vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae unnai thaanga vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidama varuven unnai thookkavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidama varuven unnai thookkavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan vayasu pulla vaalu ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vayasu pulla vaalu ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan enakku aalu ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan enakku aalu ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonthu varuven gappu-la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonthu varuven gappu-la"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaan unakku mappila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaan unakku mappila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya putchi ethuppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya putchi ethuppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai baby pola parthupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai baby pola parthupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi vaadi chella kutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi vaadi chella kutty"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan enakku bommai kutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan enakku bommai kutty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasama vanthenae vittu pogatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasama vanthenae vittu pogatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sidu munja nee kaatuna thangathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidu munja nee kaatuna thangathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan enakku pondaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan enakku pondaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakku yetha vaayendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakku yetha vaayendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaathadi aan paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaathadi aan paavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veena sellathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena sellathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru maara manasu paayuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru maara manasu paayuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasathi onna paarka thavikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasathi onna paarka thavikuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalatuthae un pinnala oduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalatuthae un pinnala oduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaru maara manasu paayuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru maara manasu paayuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasathi onna paarka thavikuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasathi onna paarka thavikuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalatuthae un pinnala oduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalatuthae un pinnala oduthae"/>
</div>
</pre>
