---
title: "ezhada song lyrics"
album: "Ezhumin"
artist: "Ganesh Chandrasekaran"
lyricist: "Thamizhanangu"
director: "VP Viji"
path: "/albums/ezhumin-lyrics"
song: "Ezhada"
image: ../../images/albumart/ezhumin.jpg
date: 2018-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oEMlQElimTg"
type: "sad"
singers:
  - Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Engae nee ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae nee ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai neengi thaanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai neengi thaanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamal sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamal sendraai"/>
</div>
<div class="lyrico-lyrics-wrapper">En chellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chellamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayilla pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayilla pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanenae naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanenae naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro paada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa chellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa chellamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee illaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai enakkennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai enakkennada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ponaa pinnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ponaa pinnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan enna da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan enna da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un pinju paatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pinju paatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenju suduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenju suduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaramalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaramalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraararaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraararaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraariraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraararaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraararaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thoongada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraararaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraararaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraariraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraararaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraararaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paadavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paadavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En thanthai neeyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thanthai neeyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pillai naanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pillai naanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un marbin melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un marbin melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sainthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sainthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verodu veezhnthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verodu veezhnthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeril sainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeril sainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Irullodu vazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irullodu vazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhada ezhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhada ezhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhada en maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhada en maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhada ezhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhada ezhada"/>
</div>
<div class="lyrico-lyrics-wrapper">En chella maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chella maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhada ezhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhada ezhada"/>
</div>
<div class="lyrico-lyrics-wrapper">En singa maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En singa maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhada ezhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhada ezhada"/>
</div>
<div class="lyrico-lyrics-wrapper">En thanga maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thanga maganae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho hooo oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho hooo oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaahaaoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmm mmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm mmmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraararaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraararaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraariraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraararaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraararaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thoongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thoongada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraararaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraararaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraariraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraararaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraararaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paadavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paadavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un thaaiyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thaaiyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee indri naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee indri naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sellatha vazhvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sellatha vazhvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen vazhnthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen vazhnthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayatha eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayatha eeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaratha sogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaratha sogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen intha saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen intha saabam"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhada ezhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhada ezhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhada en maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhada en maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhada ezhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhada ezhada"/>
</div>
<div class="lyrico-lyrics-wrapper">En chella maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chella maganae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhada ezhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhada ezhada"/>
</div>
<div class="lyrico-lyrics-wrapper">En singa maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En singa maganae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhada ezhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhada ezhada"/>
</div>
<div class="lyrico-lyrics-wrapper">En thanga maganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thanga maganae"/>
</div>
</pre>
