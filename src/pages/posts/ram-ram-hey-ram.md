---
title: "ram ram song lyrics"
album: "Hey Ram"
artist: "Ilaiyaraaja"
lyricist: "Kamal Haasan"
director: "Kamal Haasan"
path: "/albums/hey-ram-song-lyrics"
song: "Ram Ram - Ragupathi Ragava"
image: ../../images/albumart/hey-ram.jpg
date: 2000-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nksUSqUxhWQ"
type: "devotional"
singers:
  - Kamal Haasan
  - Shruti Haasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raghupathi Raagav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raghupathi Raagav"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaja Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Patheetha Paavanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patheetha Paavanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seetha Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seetha Ram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadanthathai Ninaithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthathai Ninaithidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallathai Thodangidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallathai Thodangidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhanthathai Unarnthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhanthathai Unarnthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppathai Kaathidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppathai Kaathidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbenum Or Sol Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbenum Or Sol Ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naththigam Sol Inbamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naththigam Sol Inbamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Yaarum Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Yaarum Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marumurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Marumurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvathaai Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvathaai Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvathaai Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvathaai Solli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maainthavar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maainthavar"/>
</div>
<div class="lyrico-lyrics-wrapper">Maainthavar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maainthavar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthathe Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathe Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthathe Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathe Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodarvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naame Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naame Naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naame Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naame Naalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ver Oru Aale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ver Oru Aale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naale Anbenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naale Anbenum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theebaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theebaththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettri Nee Vaiththaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettri Nee Vaiththaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eriyum Un Per Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eriyum Un Per Sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jothi Jothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jothi Jothi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oruvanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruvanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruvanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruvanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruththiyum Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruththiyum Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruththiyum Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruththiyum Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuvathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Neeyum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Neeyum Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manithanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithanaai Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithanaai Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithanaai Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithanaai Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Kaana Odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Kaana Odum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannaale Thayagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaale Thayagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaiyai Nokkiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaiyai Nokkiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaiyum Namathena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaiyum Namathena"/>
</div>
<div class="lyrico-lyrics-wrapper">Saatchchiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saatchchiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollum Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollum Sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadanthathai Ninaithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthathai Ninaithidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallathai Thodangidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallathai Thodangidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhanthathai Unarnthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhanthathai Unarnthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppathai Kaathidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppathai Kaathidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbenum Oar Sol Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbenum Oar Sol Ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naththigam Sol Inbamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naththigam Sol Inbamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Yaarum Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Yaarum Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raghupathi Raagav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raghupathi Raagav"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaja Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Patheetha Paavanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patheetha Paavanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seetha Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seetha Ram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Ram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Ram"/>
</div>
</pre>
