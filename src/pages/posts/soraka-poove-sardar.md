---
title: "Soraka poovae song lyrics"
album: "Sardar"
artist: "G. V. Prakash Kumar"
lyricist: "Yugabharathi"
director: "P.S Mithran"
path: "/albums/sardar-lyrics"
song: "Soraka poovae"
image: ../../images/albumart/sardar.jpg
date: 2022-10-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6DZv2YAsZsg?controls=0"
type: "Love"
singers:
  - Aditya RK
  - Bhadra Rajin
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Soraka poovae enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soraka poovae enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka vachi kavuthuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka vachi kavuthuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaama ponen ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaama ponen ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Marakkaa santhanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakkaa santhanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakkura aambala thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakkura aambala thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukka suthurenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukka suthurenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaala thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaala thannaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vazhiyila vaasatha nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyila vaasatha nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thantha thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thantha thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyila naan vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyila naan vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponen pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponen pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Usurula nee natta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurula nee natta"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaala thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaala thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo poothae ninnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo poothae ninnen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un munnaala naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un munnaala naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nallakkaal munivanaaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallakkaal munivanaaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Munivanaaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munivanaaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nallakkaal munivanaaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallakkaal munivanaaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Munivanaaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munivanaaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hae hae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hae hae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaanaano ohoho kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaano ohoho kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro ohoho kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro ohoho kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaano ohoho kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaano ohoho kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro ohoho kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro ohoho kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nenja vagunthu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja vagunthu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ulla pugunthitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ulla pugunthitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna azhichi thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna azhichi thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi unna ezhuthitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi unna ezhuthitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Meesa mudiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesa mudiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoondil poduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondil poduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhaa kattikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhaa kattikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pera koovuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pera koovuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enakkaana aagaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkaana aagaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaan pondaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaan pondaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttaagi povendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttaagi povendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum illaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum illaatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Marama molachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marama molachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukkulla nikkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla nikkuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalaada vakkiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalaada vakkiriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nallakkaal munivanaaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallakkaal munivanaaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Munivanaaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munivanaaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nallakkaal munivanaaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallakkaal munivanaaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Munivanaaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munivanaaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hae hae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hae hae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaanaano ohoho kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaano ohoho kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro ohoho kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro ohoho kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaano ohoho kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaano ohoho kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro ohoho kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro ohoho kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unga nenapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga nenapula"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuru vaazhuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuru vaazhuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu pona alaiyunaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu pona alaiyunaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaagam Theerkkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaagam Theerkkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thoora velaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoora velaguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vela poguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela poguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaa senthu vaazhathaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaa senthu vaazhathaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum thudikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee morappaaga verappaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee morappaaga verappaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippaayae nippaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippaayae nippaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ennaala adi saanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ennaala adi saanja"/>
</div>
<div class="lyrico-lyrics-wrapper">Sippaayae sippaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sippaayae sippaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam poru nallathoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam poru nallathoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam porakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam porakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nallakkaal munivanaaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallakkaal munivanaaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Munivanaaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munivanaaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nallakkaal munivanaaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallakkaal munivanaaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Munivanaaratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munivanaaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hae hae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hae hae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaanaano ohoho kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaano ohoho kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro ohoho kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro ohoho kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaano ohoho kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaano ohoho kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraaro ohoho kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaro ohoho kannae"/>
</div>
</pre>
