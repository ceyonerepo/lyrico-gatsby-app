---
title: "rekkai thulirtha song lyrics"
album: "Kaatrin Mozhi"
artist: "AH Kaashif"
lyricist: "Madhan Karky"
director: "Radha Mohan"
path: "/albums/kaatrin-mozhi-lyrics"
song: "Rekkai Thulirtha"
image: ../../images/albumart/kaatrin-mozhi.jpg
date: 2018-11-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/n20_lMwAvSE"
type: "happy"
singers:
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan naan naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nananaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nananaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan naan naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nananaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nananaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan naan naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nananaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nananaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan naan naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nananaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nananaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennenna yennenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennenna yennenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennennum yennennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennennum yennennum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennenna yennenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennenna yennenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennenna tharuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennenna tharuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">En chinna ulagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chinna ulagamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ketkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ketkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennennum yennennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennennum yennennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennenna pozhivaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennenna pozhivaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevvaana sitharalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvaana sitharalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En vazhvai sethukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhvai sethukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaippondru kidaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaippondru kidaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">En koottai vittu konjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En koottai vittu konjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnil parakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnil parakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En vazhvai sethukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhvai sethukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaippondru kidaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaippondru kidaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">En koottai vittu konjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En koottai vittu konjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnil parakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnil parakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekkai thulirtha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkai thulirtha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachai kili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai kili"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimel naan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimel naan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrin mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrin mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru veru oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru veru oruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalil pudhu oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalil pudhu oruthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru veru oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru veru oruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalil ini oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalil ini oruthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrodu kaatraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrodu kaatraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuzhainthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuzhainthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pootti vaithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pootti vaithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiranthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiranthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkaamalae unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkaamalae unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooram nindrae unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooram nindrae unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyakkiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyakkiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru paadal odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru paadal odum"/>
</div>
<div class="lyrico-lyrics-wrapper">En vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vizhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un idhayam en sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un idhayam en sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pootti vaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pootti vaitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragisayangal thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragisayangal thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimel en sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimel en sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Salai meethilae pogum pothilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salai meethilae pogum pothilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan kaathilae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan kaathilae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanoliyaai theanoliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanoliyaai theanoliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sala sala sala vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sala sala sala vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Suda suda mazhaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suda suda mazhaiyena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennenna yennenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennenna yennenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennenna tharuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennenna tharuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">En chinna ulagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chinna ulagamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ketkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ketkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennennum yennennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennennum yennennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennenna pozhivaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennenna pozhivaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevvaana sitharalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvaana sitharalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En vazhvai sethukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vazhvai sethukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaippondru kidaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaippondru kidaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">En koottai vittu konjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En koottai vittu konjum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnil parakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnil parakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekkai thulirtha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkai thulirtha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachai kili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai kili"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimel naan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimel naan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrin mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrin mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru veru oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru veru oruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalil pudhu oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalil pudhu oruthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru veru oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru veru oruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalil ini oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalil ini oruthi"/>
</div>
</pre>
