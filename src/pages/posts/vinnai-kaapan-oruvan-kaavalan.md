---
title: "vinnai kaapan oruvan song lyrics"
album: "Kaavalan"
artist: "Vidyasagar"
lyricist: "Pa. Vijay"
director: "Siddique"
path: "/albums/kaavalan-lyrics"
song: "Vinnai Kaapan Oruvan"
image: ../../images/albumart/kaavalan.jpg
date: 2011-01-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/anGKI6oSIk0"
type: "mass"
singers:
  - Tippu
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vinnai Kaapan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai Kaapan Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannai Kappan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai Kappan Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ennai Kaakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ennai Kaakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avane Avane Iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avane Avane Iraivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnai Kappan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai Kappan Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannai Kappan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai Kappan Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ennai Kaakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ennai Kaakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avaney Avaney Iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaney Avaney Iraivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endha Ooril Illaiada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Ooril Illaiada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellai Sami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai Sami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ney Sadhi Illai Enbuthu Thaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ney Sadhi Illai Enbuthu Thaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Sami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Sami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammai Appan Mattumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammai Appan Mattumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadhi Sami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhi Sami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Aattam Unnai Enna Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Aattam Unnai Enna Alla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnai Kappan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai Kappan Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannai Kappan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai Kappan Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ennai Kaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ennai Kaakum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avaney Avanae Iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaney Avanae Iraivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeru Munnaeru Yeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeru Munnaeru Yeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peru Varum Peru Paeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru Varum Peru Paeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Ada Yaathum Ingey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Ada Yaathum Ingey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podu Ennodu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu Ennodu Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodu Anbodu Kudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu Anbodu Kudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvil Ada Vandhu Pogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil Ada Vandhu Pogam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallam Medu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallam Medu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edu Da Melatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edu Da Melatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalatha  Theivatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalatha  Theivatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inimela Thaan Vithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimela Thaan Vithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mottha Kuttatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottha Kuttatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kotti Thaan Kondadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotti Thaan Kondadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Sonthatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Sonthatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Banthatha Nenjaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banthatha Nenjaththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirunaalil Katti Onna Uravadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirunaalil Katti Onna Uravadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alavodu Vilayadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavodu Vilayadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalalagandaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalalagandaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattathukku Mannaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattathukku Mannaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanandha Thandavam Aaduvomey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha Thandavam Aaduvomey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnai Kappan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai Kappan Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannai Kappan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai Kappan Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ennai Kakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ennai Kakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avaney Avanae Iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaney Avanae Iraivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnai Kappan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai Kappan Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannai Kappan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai Kappan Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ennai Kakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ennai Kakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avaney Avaney Iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaney Avaney Iraivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaru Ila Nenjil Yaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaru Ila Nenjil Yaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooru Avan Kaadhil Kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooru Avan Kaadhil Kooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Radhai Mandhu Sonnadhu Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhai Mandhu Sonnadhu Ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannan Paeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannan Paeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Thirunalil Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Thirunalil Ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odum Thiruvarur Thaeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum Thiruvarur Thaeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatta Bali Ketathu Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatta Bali Ketathu Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyanarru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyanarru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbin Vazhi Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin Vazhi Ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mozhi Ondru Engana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi Ondru Engana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagu Ellam Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagu Ellam Ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetri Kodi Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Kodi Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parakattum Munnaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakattum Munnaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achcham Vilagatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achcham Vilagatum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilagatum Maraiyattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagatum Maraiyattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athai Thooki Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Thooki Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeram Vilaiyaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram Vilaiyaatum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyaatum Mannmelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaatum Mannmelae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Pattu Paduna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pattu Paduna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadaadum Aadum Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadaadum Aadum Na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallla Seidhi Yaru Sonnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallla Seidhi Yaru Sonnalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kettu Ko Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettu Ko Na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnai Kaappan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai Kaappan Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannai Kaappan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai Kaappan Oruvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ennai Kakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ennai Kakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avaney Avaney Iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaney Avaney Iraivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endha Ooril Illaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Ooril Illaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellai Sami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai Sami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ney Sadhi Illai Enbuthu Thaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ney Sadhi Illai Enbuthu Thaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Sami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Sami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammai Appan Mattumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammai Appan Mattumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadhi Sami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhi Sami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Attam Unnai Enna Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Attam Unnai Enna Alla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikkaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkaami"/>
</div>
</pre>
