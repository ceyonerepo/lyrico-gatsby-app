---
title: "singam singam he is durai singam song lyrics"
album: "Singam"
artist: "Devi Sri Prasad"
lyricist: "Na. Muthukumar - Megha"
director: "Hari Gopalakrishnan"
path: "/albums/singam-lyrics"
song: "Singam Singam He Is Durai Singam" 
image: ../../images/albumart/singam.jpg
date: 2010-05-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OfhkB_yRnL4"
type: "Mass"
singers:
  - Devi Sri Prasad
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Everybody Listen Listen Listen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody Listen Listen Listen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Make Way For The King
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make Way For The King"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everybody Listen Listen Listen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody Listen Listen Listen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Just Watch The Way He Is Gonna Swing Alright
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Watch The Way He Is Gonna Swing Alright"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Vinnai Theendum Kathiravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vinnai Theendum Kathiravan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enthan Ellai Thaandum Kaattrivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Ellai Thaandum Kaattrivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatai Erikkum Neruppivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatai Erikkum Neruppivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Pattaal Kothikkum Neer Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Pattaal Kothikkum Neer Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everybody Listen He Is On A Mission
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody Listen He Is On A Mission"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Can Never Stop Him
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Can Never Stop Him"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">He Is A Human Tornado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He Is A Human Tornado"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Everybody Listen He Has Got The Right Decision
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody Listen He Has Got The Right Decision"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Cannot Mess With Him Not Even With His Shadow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Cannot Mess With Him Not Even With His Shadow"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam Singam He Is Durai Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam Singam He Is Durai Singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Nadandhaal Pothum Nilamum Vanangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Nadandhaal Pothum Nilamum Vanangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Singam Singam He Is Durai Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Singam Singam He Is Durai Singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aym Boothangal Yaavum Ivanul Adangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aym Boothangal Yaavum Ivanul Adangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Rowdigalin Raajiyangal Ranagalamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Rowdigalin Raajiyangal Ranagalamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkum Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkum Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaavalthurai Therndheduthu Singathaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavalthurai Therndheduthu Singathaithaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anuppum Jeikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuppum Jeikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeram Adhu Idhudhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram Adhu Idhudhaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seerum Iru Vizhi Dhano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerum Iru Vizhi Dhano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theerum Adhu Evandhano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerum Adhu Evandhano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayavano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayavano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarkan Adhu Padithano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkan Adhu Padithano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakum Irru Kaidhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakum Irru Kaidhaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neekam Thani Vazhidhano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekam Thani Vazhidhano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nirandhara Bayam Ivano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirandhara Bayam Ivano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam Singam He Is Durai Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam Singam He Is Durai Singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Paarthaal Podhum Idiyum Irangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Paarthaal Podhum Idiyum Irangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vagai Vagaiyaai Thavaru Seiyum Kayavargalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vagai Vagaiyaai Thavaru Seiyum Kayavargalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arivaan Arivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivaan Arivaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Miga Migayaai Kalavaramaa Pugai Enavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miga Migayaai Kalavaramaa Pugai Enavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuzhaivaan Suduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuzhaivaan Suduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suththam Iru Kaidhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththam Iru Kaidhaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratham Oru Maidhano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratham Oru Maidhano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yutham Athu Meidhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yutham Athu Meidhaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porkalamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porkalamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suttrum Puvi Ivanthano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttrum Puvi Ivanthano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thattum Kadal Alaidhano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattum Kadal Alaidhano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttrum Pagai Azhipaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttrum Pagai Azhipaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanidan Avan Evano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanidan Avan Evano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam Singam He Is Durai Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam Singam He Is Durai Singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come On Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evan Nindral Podhum Nagaram Nadungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan Nindral Podhum Nagaram Nadungum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Singam Singam He Is Durai Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Singam Singam He Is Durai Singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Kandal Podhum Agaram Thodangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kandal Podhum Agaram Thodangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam"/>
</div>
</pre>
