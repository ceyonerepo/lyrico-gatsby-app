---
title: "anbae peranbae song lyrics"
album: "NGK"
artist: "Yuvan Shankar Raja"
lyricist: "Uma Devi"
director: "Selvaraghavan"
path: "/albums/ngk-lyrics"
song: "Anbae Peranbae"
image: ../../images/albumart/ngk.jpg
date: 2019-05-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/N_iW0VC3IdI"
type: "love"
singers:
  - Sid Sriram
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anbae Anbae Anbae Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae Anbae Anbae Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae Anbae Anbae Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae Anbae Anbae Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Peranbae Peranbae Peranbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peranbae Peranbae Peranbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Peranbae Peranbae Peranbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peranbae Peranbae Peranbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Ooo Oo Hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Ooo Oo Hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Iravodu Oliyaai Koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Iravodu Oliyaai Koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravondru Ketkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravondru Ketkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaimeerum Ivalin Aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaimeerum Ivalin Aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Niraivera Paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraivera Paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhi Serum Kadalin Meedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhi Serum Kadalin Meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Neraai Seruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Neraai Seruven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amuthae Peramuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amuthae Peramuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Penn Manadhin Kanavin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penn Manadhin Kanavin"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkam Theerkkumaa Eerkkumaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkam Theerkkumaa Eerkkumaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhiyai Than Madhiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhiyai Than Madhiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Azhagin Bimbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Azhagin Bimbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Paarkkumathorkkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Paarkkumathorkkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaivaanam Thoorum Pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaivaanam Thoorum Pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manal Enna Koosumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manal Enna Koosumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarodu Malargal Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarodu Malargal Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Enna Thootrumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Enna Thootrumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiraiyae Thirai Kadalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiraiyae Thirai Kadalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Athirum Anbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Athirum Anbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathilai Thaanduthaethoonduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathilai Thaanduthaethoonduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoram Thoongum Mogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoram Thoongum Mogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannoram Doobam Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannoram Doobam Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaatha Ragasiyam Nee Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaatha Ragasiyam Nee Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooor Ketka Yenguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooor Ketka Yenguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimaiyil Thunai Varum Yosanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyil Thunai Varum Yosanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivilum Manakkuthu Un Vaasanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivilum Manakkuthu Un Vaasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamae Ondraaga Maaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamae Ondraaga Maaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mananthida Seval Koovuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mananthida Seval Koovuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kodai Kaalaththin Megangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kodai Kaalaththin Megangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaarkaalam Thoorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarkaalam Thoorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aal Illaatha Kaatilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aal Illaatha Kaatilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Boobaalam Ketkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boobaalam Ketkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmmmmmmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Both  Anbae Peranbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Both  Anbae Peranbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neduvazhvin Nizhalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neduvazhvin Nizhalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Aaguthae Aaguthaeaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Aaguthae Aaguthaeaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravae Namm Uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravae Namm Uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Anuvin Pirivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Anuvin Pirivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Andril Aaguthae Aaguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andril Aaguthae Aaguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraiyaadha Sollin Porulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraiyaadha Sollin Porulai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhi Ingu Thaangumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi Ingu Thaangumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaaga Anbil Vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaaga Anbil Vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Aayul Pothumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Aayul Pothumo"/>
</div>
</pre>
