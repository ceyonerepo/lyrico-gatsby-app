---
title: 'nanba nanba song lyrics'
album: 'Comali'
artist: 'Hiphop Tamizha'
lyricist: 'Hiphop Tamizha'
director: 'Pradeep Ranganathan'
path: '/albums/comali-song-lyrics'
song: 'Nanba Nanba'
image: ../../images/albumart/comali.jpg
date: 2019-08-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VH3KZNIZwzM"
type: 'love'
singers: 
- Sanjith Hegde
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Nanbaa nanbaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanbaa nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naan naam aavom
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee naan naam aavom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbaa nanbaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanbaa nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee naan naam aavom
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee naan naam aavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hoo ooo oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo ooo oooo oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo ooo oooo oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh solli thanthadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thamizh solli thanthadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo ooo oooo oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo ooo oooo oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhathai manidhathai manidhathai
<input type="checkbox" class="lyrico-select-lyric-line" value="Manidhathai manidhathai manidhathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Inam ena pirindhadhu podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Inam ena pirindhadhu podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Madham ena pirindhadhu podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Madham ena pirindhadhu podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidham ondrae theervaagum
<input type="checkbox" class="lyrico-select-lyric-line" value="Manidham ondrae theervaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa…aaa…aaa….aaaa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa…aaa…aaa….aaaa…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uyirgalai izhandhadhu podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirgalai izhandhadhu podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravugal azhindhadhu podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Uravugal azhindhadhu podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae endrum theervaagum
<input type="checkbox" class="lyrico-select-lyric-line" value="Anbae endrum theervaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaa…aaa…aaan…
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaa…aaa…aaan…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaalam adhu kannmun
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam adhu kannmun"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerai polae karaindhaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanneerai polae karaindhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnyalam adhu gnyayiru melae
<input type="checkbox" class="lyrico-select-lyric-line" value="Gnyalam adhu gnyayiru melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai izhandhaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nambikkai izhandhaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nanbaa…aaaa….aaa…aaa…aaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanbaa…aaaa….aaa…aaa…aaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru naal vidiyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru naal vidiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulum vilagum
<input type="checkbox" class="lyrico-select-lyric-line" value="Irulum vilagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha naal varaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha naal varaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbinaal inaivaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Anbinaal inaivaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraiva…haaa…aaa…aaa…aaa…….
<input type="checkbox" class="lyrico-select-lyric-line" value="Iraiva…haaa…aaa…aaa…aaa……."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaalam adhu kannmun
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam adhu kannmun"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerai polae karaindhaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanneerai polae karaindhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gnyalam adhu gnyayiru melae
<input type="checkbox" class="lyrico-select-lyric-line" value="Gnyalam adhu gnyayiru melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai izhandhaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nambikkai izhandhaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hoo ooo oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo ooo oooo oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo ooo oooo oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo ooo oooo oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo ooo oooo oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo oo oo oo oo oooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo oo oo oo oo oo oooo ooo"/>
</div>
</pre>