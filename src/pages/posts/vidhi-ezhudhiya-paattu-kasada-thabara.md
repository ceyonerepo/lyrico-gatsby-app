---
title: "vidhi ezhudhiya paattu song lyrics"
album: "Kasada Thabara"
artist: "Yuvan Shankar Raja"
lyricist: "Gangai Amaran"
director: "Chimbudeven"
path: "/albums/kasada-thabara-lyrics"
song: "Vidhi Ezhudhiya Paattu"
image: ../../images/albumart/kasada-thabara.jpg
date: 2021-08-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Z_9DWBhjmdY"
type: "mass"
singers:
  - Sivam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">pirigirom endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirigirom endral"/>
</div>
<div class="lyrico-lyrics-wrapper">pothume pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothume pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">neram kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">ilaigal than endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilaigal than endral"/>
</div>
<div class="lyrico-lyrics-wrapper">pothume pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothume pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">anbil serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbil serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">anbile vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbile vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyuma mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyuma mudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">anbe unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbe unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivu than ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivu than ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">piriyuma piriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piriyuma piriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">piriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piriyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vidhi ezhuthiya paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhi ezhuthiya paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">athu oru vilaiyatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu oru vilaiyatu"/>
</div>
<div class="lyrico-lyrics-wrapper">mathi kalanguthu ketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathi kalanguthu ketu"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakathai kalanthootu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakathai kalanthootu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanaku vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaku vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhaikul ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhaikul ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">inaithathu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inaithathu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">enakul matum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakul matum"/>
</div>
<div class="lyrico-lyrics-wrapper">kulapam vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulapam vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">padaithathu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padaithathu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">pirantha velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirantha velai"/>
</div>
<div class="lyrico-lyrics-wrapper">etharku endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etharku endru"/>
</div>
<div class="lyrico-lyrics-wrapper">unarthu kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unarthu kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">arinthu kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arinthu kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">therintha puthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therintha puthi"/>
</div>
<div class="lyrico-lyrics-wrapper">yen yen il lai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen yen il lai"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vidhi ezhuthiya paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhi ezhuthiya paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">athu oru vilaiyatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu oru vilaiyatu"/>
</div>
<div class="lyrico-lyrics-wrapper">mathi kalanguthu ketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathi kalanguthu ketu"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakathai kalanthootu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakathai kalanthootu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">matravarku anbu seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matravarku anbu seiya"/>
</div>
<div class="lyrico-lyrics-wrapper">thondu seiya vendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thondu seiya vendi"/>
</div>
<div class="lyrico-lyrics-wrapper">vendi vendi vendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendi vendi vendi"/>
</div>
<div class="lyrico-lyrics-wrapper">ketu vantha naan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketu vantha naan than"/>
</div>
<div class="lyrico-lyrics-wrapper">indru ingu verupattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru ingu verupattu"/>
</div>
<div class="lyrico-lyrics-wrapper">kooru ketu maarupattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooru ketu maarupattu"/>
</div>
<div class="lyrico-lyrics-wrapper">en pathai moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pathai moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">vithathu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithathu yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu enna yaruraitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu enna yaruraitha"/>
</div>
<div class="lyrico-lyrics-wrapper">saabam endru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saabam endru "/>
</div>
<div class="lyrico-lyrics-wrapper">kobam endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kobam endru"/>
</div>
<div class="lyrico-lyrics-wrapper">kandu kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandu kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">parthu solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parthu solla"/>
</div>
<div class="lyrico-lyrics-wrapper">aal yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aal yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">indru ingu vaazhukindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru ingu vaazhukindra"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhlkai ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhlkai ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhlkai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhlkai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu marukindrathu endro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu marukindrathu endro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">pirikirom endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirikirom endral"/>
</div>
<div class="lyrico-lyrics-wrapper">pothume pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothume pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">neram kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">ivaikal than endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivaikal than endral"/>
</div>
<div class="lyrico-lyrics-wrapper">pothume pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothume pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">anbil serthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbil serthu "/>
</div>
<div class="lyrico-lyrics-wrapper">anbile vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbile vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyuma mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyuma mudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">anbe unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbe unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivu than enaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivu than enaai"/>
</div>
<div class="lyrico-lyrics-wrapper">piriyuma piriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piriyuma piriyuma"/>
</div>
</pre>
