---
title: "oorantha song lyrics"
album: "Rang De"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/rang-de-lyrics"
song: "Oorantha Vennela Manasantha"
image: ../../images/albumart/rang-de.jpg
date: 2021-03-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/txjwwsJ9PWI"
type: "sad"
singers:
  - Mangli
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Om ganeshaya namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om ganeshaya namaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekadanthaya namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekadanthaya namaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Om ganeshaya namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om ganeshaya namaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekadanthaya namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekadanthaya namaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooranthaa vennelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooranthaa vennelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasantha cheekati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasantha cheekati"/>
</div>
<div class="lyrico-lyrics-wrapper">Raalindhaa ninnalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raalindhaa ninnalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Repati kala okati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repati kala okati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagamantha veduka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagamantha veduka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasantha vedhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasantha vedhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichindhaa ninnilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichindhaa ninnilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adagani malupokati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagani malupokati"/>
</div>
<div class="lyrico-lyrics-wrapper">Madike musuge thodige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madike musuge thodige"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduge etuko nadake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduge etuko nadake"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi o kanta kanneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi o kanta kanneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">O kanta chirunavvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O kanta chirunavvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooranthaa vennelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooranthaa vennelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasantha cheekati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasantha cheekati"/>
</div>
<div class="lyrico-lyrics-wrapper">Raalindhaa ninnalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raalindhaa ninnalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Repati kala okati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repati kala okati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Om ganeshaya namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om ganeshaya namaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekadanthaya namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekadanthaya namaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evarikee cheppave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evarikee cheppave"/>
</div>
<div class="lyrico-lyrics-wrapper">Evarinee adagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evarinee adagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuko premake maatale nerpave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuko premake maatale nerpave"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopukandhani machhani kuda..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopukandhani machhani kuda.."/>
</div>
<div class="lyrico-lyrics-wrapper">Chandamamalo choopisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandamamalo choopisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopavalasina premanu maathram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopavalasina premanu maathram"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelopale daachesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelopale daachesthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enno rangulunnaa badha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enno rangulunnaa badha"/>
</div>
<div class="lyrico-lyrics-wrapper">Range bathukulo volikisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Range bathukulo volikisthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooranthaa vennelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooranthaa vennelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasantha cheekati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasantha cheekati"/>
</div>
<div class="lyrico-lyrics-wrapper">Raalindhaa ninnalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raalindhaa ninnalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Repati kala okati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repati kala okati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evaritho payanamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaritho payanamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Evarikai gamanamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evarikai gamanamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Erugani parugulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erugani parugulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Prashnavo badhuluvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prashnavo badhuluvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni kalalu kani emiti laabham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni kalalu kani emiti laabham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu kanulane velivesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu kanulane velivesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni kathalu vini emiti soukhyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni kathalu vini emiti soukhyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha kathanu madhi vadhilesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha kathanu madhi vadhilesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttoo inni santoshaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttoo inni santoshaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kappesthunte nee kanneellanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappesthunte nee kanneellanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooranthaa vennelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooranthaa vennelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasantha cheekati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasantha cheekati"/>
</div>
<div class="lyrico-lyrics-wrapper">Raalindhaa ninnalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raalindhaa ninnalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Repati kala okati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repati kala okati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Om ganeshaya namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om ganeshaya namaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekadanthaya namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekadanthaya namaha"/>
</div>
</pre>
