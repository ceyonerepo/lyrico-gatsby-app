---
title: "amma amma ne enga song lyrics"
album: "Vellaiilla Pattadhari"
artist: "Anirudh Ravichander"
lyricist: "Dhanush"
director: "Velraj"
path: "/albums/vellaiilla-pattadhari-lyrics"
song: "Amma Amma Ne Enga"
image: ../../images/albumart/vellaiilla-pattadhari.jpg
date: 2014-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OdVe7AUlJlU"
type: "Sad"
singers:
  - S. Janaki
  - Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Amma Amma Nee Enga Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Nee Enga Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vittaa Enakkaaru Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vittaa Enakkaaru Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Paarthenae Kaanom Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Paarthenae Kaanom Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaa Moochi Yen Vaa Nee Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaa Moochi Yen Vaa Nee Vella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaye Uyir Pirinthaayae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaye Uyir Pirinthaayae "/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thaniye Thavikkavittaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thaniye Thavikkavittaaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Nee Paadum Paattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Nee Paadum Paattukku"/>
</div>
 <div class="lyrico-lyrics-wrapper">Naan Thoonga Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thoonga Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paadum Paattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paadum Paattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayae Nee Un Kangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayae Nee Un Kangal "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirandhaalae Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirandhaalae Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Amma Nee Enga Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Nee Enga Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vittaa Enakkaaru Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vittaa Enakkaaru Amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thoongum Munney Nee Thoongi Ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thoongum Munney Nee Thoongi Ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayae Enmael Unakkenna Kovam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayae Enmael Unakkenna Kovam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaana Kannae En Deiva Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaana Kannae En Deiva Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Dhoosi Nee Ootha Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Dhoosi Nee Ootha Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyo Yaen Indha Saabam Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Yaen Indha Saabam Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Endro Naan Seidha Paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endro Naan Seidha Paavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalum Iravaagi Bayamaandhey Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalum Iravaagi Bayamaandhey Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilakkum Thunai Indri Irulaanadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilakkum Thunai Indri Irulaanadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin Oru Paadhi Pariponadhey Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Oru Paadhi Pariponadhey Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimai Nilaiyaandhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai Nilaiyaandhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Amma Nee Enga Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Nee Enga Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vitta Enakkaaru Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vitta Enakkaaru Amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Pona Pinnum Nee Vaazha Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pona Pinnum Nee Vaazha Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Moochu Unakullum Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Moochu Unakullum Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanengum Vannam Poovellaam Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanengum Vannam Poovellaam Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vaazhum Ulagil Deivangal Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vaazhum Ulagil Deivangal Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee En Perumaiyin Ellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee En Perumaiyin Ellai"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Thanthai Per Sollum Pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Thanthai Per Sollum Pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorum Peridhillai Kalangaadhey En Kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorum Peridhillai Kalangaadhey En Kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Vilaiyaada Un Kanmunnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Vilaiyaada Un Kanmunnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Karaindhodum Un Vaazhvil Karai Serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Karaindhodum Un Vaazhvil Karai Serum"/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum Naan Un Pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Naan Un Pillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Amma Nee Enga Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Nee Enga Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vitta Enakkaaru Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vitta Enakkaaru Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Ponaalum Naanum Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Ponaalum Naanum Varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaadi Paaru Naanum Therivaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Paaru Naanum Therivaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaye Uyir Pirindhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaye Uyir Pirindhaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Neeyum Ennuyir Thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Neeyum Ennuyir Thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Nee Paadum Paattukku Naan Thoonga Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Nee Paadum Paattukku Naan Thoonga Venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paadum Thaalaattu Nee Thoonga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paadum Thaalaattu Nee Thoonga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhoram Endrendrum Kaetkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhoram Endrendrum Kaetkum"/>
</div>
</pre>
