---
title: 'pularum song lyrics'
album: 'Dharala Prabhu'
artist: 'Vivek Mervin'
lyricist: 'Subu'
director: 'Krishna Marimuthu'
path: '/albums/dharala-prabhu-song-lyrics'
song: 'Pularum'
image: ../../images/albumart/dharala-prabhu.jpg
date: 2020-03-13
lang: tamil
singers: 
- Yazin Nizar
youtubeLink: 'https://www.youtube.com/embed/O8Bg4gAbRLk'
type: 'happy'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Haaa…aaa…aaa…aa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaa…aaa…aaa…aa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaa…aaa…aaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaaa…aaa…aaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa….aaa…aaa…aaa…aaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaa….aaa…aaa…aaa…aaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pularum vaazhvin
<input type="checkbox" class="lyrico-select-lyric-line" value="Pularum vaazhvin"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhalaam naal idhuvaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Mudhalaam naal idhuvaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ularum naalil
<input type="checkbox" class="lyrico-select-lyric-line" value="Ularum naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai thooridum mrudhuvaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhai thooridum mrudhuvaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Manadhai soozhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Manadhai soozhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Noi neekkum oliyaai vaandhaaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Noi neekkum oliyaai vaandhaaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhalai sollil
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhalai sollil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkaikae artham thandhaaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhkkaikae artham thandhaaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oohhhh…ohhh…ooooh…
<input type="checkbox" class="lyrico-select-lyric-line" value="Oohhhh…ohhh…ooooh…"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohhhh…ohhh…ooooh…
<input type="checkbox" class="lyrico-select-lyric-line" value="Oohhhh…ohhh…ooooh…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaatril nee kai aasaithaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaatril nee kai aasaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oviyam thondrudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oviyam thondrudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkidam suvargal ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kirukkidam suvargal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavidhai aagudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavidhai aagudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naan unai thozhil thookka
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan unai thozhil thookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Baarangal theerudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Baarangal theerudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaiyum vaazha vendum
<input type="checkbox" class="lyrico-select-lyric-line" value="Naalaiyum vaazha vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai thoondudhae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Aasai thoondudhae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Veryaaru endra podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Veryaaru endra podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">En anbu ondru yeraalamaa
<input type="checkbox" class="lyrico-select-lyric-line" value="En anbu ondru yeraalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeraalamaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeraalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thaai endru uravaada
<input type="checkbox" class="lyrico-select-lyric-line" value="Un thaai endru uravaada"/>
</div>
<div class="lyrico-lyrics-wrapper">En thaaram tharalaamaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="En thaaram tharalaamaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Veedendra ondru indru
<input type="checkbox" class="lyrico-select-lyric-line" value="Veedendra ondru indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir kondadhu un moolamaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyir kondadhu un moolamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thandha aanandham
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thandha aanandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar endhan kannoramaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Paar endhan kannoramaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Meendum meendum
<input type="checkbox" class="lyrico-select-lyric-line" value="Meendum meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha naatkal vendum ini
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha naatkal vendum ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini ini…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini ini…"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Podhum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha inbam podhum adi
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha inbam podhum adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Meendum meendum
<input type="checkbox" class="lyrico-select-lyric-line" value="Meendum meendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha naatkal vendum ini
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha naatkal vendum ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini ini…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini ini…"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Podhum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha inbam podhum adi
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha inbam podhum adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pularum vaazhvin
<input type="checkbox" class="lyrico-select-lyric-line" value="Pularum vaazhvin"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhalaam naal idhuvaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Mudhalaam naal idhuvaa…"/>
</div>
</pre>