---
title: "varlam varlam va song lyrics"
album: "Bairavaa"
artist: "Santhosh Narayanan"
lyricist: "Arunraja Kamaraj - Roshan Jamrock"
director: "Bharathan"
path: "/albums/bairavaa-lyrics"
song: "Varlam Varlam Va"
image: ../../images/albumart/bairavaa.jpg
date: 2017-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7JE_UaiIayY"
type: "mass"
singers:
  - Arunraja Kamaraj
  - Roshan Jamrock
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaarra yaarra ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarra yaarra ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooraketta theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooraketta theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarra munna vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarra munna vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu paarra puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu paarra puriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koora nirkkum kathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koora nirkkum kathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukkula milirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkula milirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarumaara ghilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarumaara ghilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Pera ketta adhirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pera ketta adhirum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam vaa bairavaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam vaa bairavaa "/>
</div>
<div class="lyrico-lyrics-wrapper">yeh yeh yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeh yeh yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam vaa bairavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam vaa bairavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modhum paarai kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhum paarai kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">manna maari pogattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manna maari pogattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha thaara pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha thaara pola "/>
</div>
<div class="lyrico-lyrics-wrapper">onna sernthu vaazhattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna sernthu vaazhattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Seerum kaalai ivan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerum kaalai ivan "/>
</div>
<div class="lyrico-lyrics-wrapper">kombil kuthi saaikattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kombil kuthi saaikattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Orrum paasam ellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orrum paasam ellam "/>
</div>
<div class="lyrico-lyrics-wrapper">nalla thala thookkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla thala thookkattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam vaa bairavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam vaa bairavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pull up in the middle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pull up in the middle"/>
</div>
<div class="lyrico-lyrics-wrapper">With a little bit 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="With a little bit "/>
</div>
<div class="lyrico-lyrics-wrapper">of rage and a gas
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="of rage and a gas"/>
</div>
<div class="lyrico-lyrics-wrapper">Full of vengeance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full of vengeance"/>
</div>
<div class="lyrico-lyrics-wrapper">Gotta put a cap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gotta put a cap"/>
</div>
<div class="lyrico-lyrics-wrapper">On my anger, keep cool
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On my anger, keep cool"/>
</div>
<div class="lyrico-lyrics-wrapper">Gotta handle this 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gotta handle this "/>
</div>
<div class="lyrico-lyrics-wrapper">whole situation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="whole situation"/>
</div>
<div class="lyrico-lyrics-wrapper">How the hell did 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How the hell did "/>
</div>
<div class="lyrico-lyrics-wrapper">i end up here?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i end up here?"/>
</div>
<div class="lyrico-lyrics-wrapper">Third eye blind now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Third eye blind now"/>
</div>
<div class="lyrico-lyrics-wrapper">Third eye clear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Third eye clear"/>
</div>
<div class="lyrico-lyrics-wrapper">Greedy money making machine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Greedy money making machine"/>
</div>
<div class="lyrico-lyrics-wrapper">Corrupt and violent
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Corrupt and violent"/>
</div>
<div class="lyrico-lyrics-wrapper">Time for thalapathy to 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time for thalapathy to "/>
</div>
<div class="lyrico-lyrics-wrapper">break the silence
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="break the silence"/>
</div>
<div class="lyrico-lyrics-wrapper">Self interest of a few
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Self interest of a few"/>
</div>
<div class="lyrico-lyrics-wrapper">The only reason you 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The only reason you "/>
</div>
<div class="lyrico-lyrics-wrapper">and me going throughWhat 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="and me going throughWhat "/>
</div>
<div class="lyrico-lyrics-wrapper">we go through
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we go through"/>
</div>
<div class="lyrico-lyrics-wrapper">Divide and conquer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Divide and conquer"/>
</div>
<div class="lyrico-lyrics-wrapper">Evil man’s game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evil man’s game"/>
</div>
<div class="lyrico-lyrics-wrapper">We dont wanna play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We dont wanna play"/>
</div>
<div class="lyrico-lyrics-wrapper">We dont wanna play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We dont wanna play"/>
</div>
<div class="lyrico-lyrics-wrapper">Used to get by doing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Used to get by doing"/>
</div>
<div class="lyrico-lyrics-wrapper">Bidding for the man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bidding for the man"/>
</div>
<div class="lyrico-lyrics-wrapper">Now enlightments 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Now enlightments "/>
</div>
<div class="lyrico-lyrics-wrapper">come my way
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="come my way"/>
</div>
<div class="lyrico-lyrics-wrapper">Shutdown the system
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shutdown the system"/>
</div>
<div class="lyrico-lyrics-wrapper">Make my way!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make my way!"/>
</div>
<div class="lyrico-lyrics-wrapper">Shutdown the system
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shutdown the system"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow me"/>
</div>
<div class="lyrico-lyrics-wrapper">follow me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="follow me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam vaa bairavaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam vaa bairavaa "/>
</div>
<div class="lyrico-lyrics-wrapper">yeh yeh yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeh yeh yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam vaa bairavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam vaa bairavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasathukke thathupulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasathukke thathupulla "/>
</div>
<div class="lyrico-lyrics-wrapper">thalapathy da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalapathy da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesathukke endrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesathukke endrum "/>
</div>
<div class="lyrico-lyrics-wrapper">ivan adhipadhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan adhipadhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey paasathukke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey paasathukke "/>
</div>
<div class="lyrico-lyrics-wrapper">thathupulla thalapathy da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thathupulla thalapathy da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesathukke endrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesathukke endrum "/>
</div>
<div class="lyrico-lyrics-wrapper">ivan adhipadhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan adhipadhida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My thirst for payback
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My thirst for payback"/>
</div>
<div class="lyrico-lyrics-wrapper">Is about to be quenched
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is about to be quenched"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow free like water
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow free like water"/>
</div>
<div class="lyrico-lyrics-wrapper">Like bruce lee’s revenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like bruce lee’s revenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Wolf in sheep clothing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wolf in sheep clothing"/>
</div>
<div class="lyrico-lyrics-wrapper">See the fake posing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="See the fake posing"/>
</div>
<div class="lyrico-lyrics-wrapper">Make em all disapper 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make em all disapper "/>
</div>
<div class="lyrico-lyrics-wrapper">like 1000’s
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="like 1000’s"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varlaam varlaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varlaam varlaam vaa"/>
</div>
</pre>
