---
title: "you are my love song lyrics"
album: "Paagal"
artist: "Radhan"
lyricist: "Simran"
director: "Naressh Kuppili"
path: "/albums/paagal-lyrics"
song: "You Are My Love"
image: ../../images/albumart/paagal.jpg
date: 2021-08-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/n7LfpZYf7iU"
type: "love"
singers:
  - Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Cheliya O Cheliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya O Cheliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaale Naakika O Cheliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaale Naakika O Cheliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Palike Tholi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Palike Tholi "/>
</div>
<div class="lyrico-lyrics-wrapper">Padanisa Nee Valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padanisa Nee Valla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalayaa Idhi Kalayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalayaa Idhi Kalayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaane Kaadhadhi O Sakhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaane Kaadhadhi O Sakhiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamai Niliche Naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamai Niliche Naa "/>
</div>
<div class="lyrico-lyrics-wrapper">Edhutana Ee Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhutana Ee Vela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottha FM lo Sangeethame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha FM lo Sangeethame "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundello"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Birdalle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Birdalle "/>
</div>
<div class="lyrico-lyrics-wrapper">Telaanule Aakashamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telaanule Aakashamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rivvuna Rivvuna Rocket-laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rivvuna Rivvuna Rocket-laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ringulu Thirige Rainbow Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ringulu Thirige Rainbow Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Merupula Basket Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Merupula Basket Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Minukullo Munigelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minukullo Munigelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangula Rangula Chitramlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangula Rangula Chitramlaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu Lokam Maarelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu Lokam Maarelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Premanu Jalli Niluvellaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premanu Jalli Niluvellaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurisaave Vennellaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurisaave Vennellaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Are My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Naa Pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Naa Pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Lokam Chooselaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Lokam Chooselaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inni Naalla Naa Edaarilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni Naalla Naa Edaarilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolathota Adugupettero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolathota Adugupettero"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandutenda Bulli Buggapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandutenda Bulli Buggapai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchumukka Muddhu Pettero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchumukka Muddhu Pettero"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho Unna Kottha Nannuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Unna Kottha Nannuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhu Anna Kannu Choosero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhu Anna Kannu Choosero"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kallallo Nenante 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kallallo Nenante "/>
</div>
<div class="lyrico-lyrics-wrapper">Asooyale Pongipoyero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asooyale Pongipoyero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattutheega Patti Laagero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattutheega Patti Laagero"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttukunna Mullu Tenchero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttukunna Mullu Tenchero"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenenaadu Oohinchalenanthalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenenaadu Oohinchalenanthalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme Pongero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme Pongero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tannula Tannulu Vennelale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tannula Tannulu Vennelale"/>
</div>
<div class="lyrico-lyrics-wrapper">Guppedu Gundenu Kappelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppedu Gundenu Kappelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnala Monnala Needalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnala Monnala Needalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hariville Guppelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hariville Guppelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomini Etthuku Bonthalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomini Etthuku Bonthalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Hatthuku Thirigaa Baaludilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hatthuku Thirigaa Baaludilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Polika Dhorakani Premikudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polika Dhorakani Premikudai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilichaane Nee Valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilichaane Nee Valla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Are My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Naa Pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Naa Pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Lokam Chooselaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Lokam Chooselaa"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbullo Egirelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbullo Egirelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Magic Nee Valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Magic Nee Valla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Are My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Naa Pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Naa Pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Lokam Chooselaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Lokam Chooselaa"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Love My Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbullo Egirelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbullo Egirelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Love Love Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Love Love Love"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Magic Nee Valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Magic Nee Valla"/>
</div>
</pre>
