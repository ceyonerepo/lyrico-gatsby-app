---
title: "madhura nagarilo song lyrics"
album: "Pelli Sanda D"
artist: "M.M.keeravaani"
lyricist: "Chandrabose"
director: "Gowri Ronanki"
path: "/albums/pelli-sandad-lyrics"
song: "Madhura Nagarilo"
image: ../../images/albumart/pelli-sandad.jpg
date: 2021-05-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/v-G34TZIEcY"
type: "happy"
singers:
  - Sreenidhi
  - Nayana Nair
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Madhura nagarilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura nagarilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yamunaa thaatilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamunaa thaatilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Muraali swaramule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muraali swaramule"/>
</div>
<div class="lyrico-lyrics-wrapper">Musirina yadhaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musirina yadhaalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuresenanta muripaala vaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuresenanta muripaala vaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Layalie hoyalie jaala jaala jathulie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Layalie hoyalie jaala jaala jathulie"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaala gaala gathulie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaala gaala gathulie"/>
</div>
<div class="lyrico-lyrics-wrapper">Valapula sruthulie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valapula sruthulie"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasula matruthalie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasula matruthalie"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dorakka dorakka dorikindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorakka dorakka dorikindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalukku chilaaka idi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalukku chilaaka idi"/>
</div>
<div class="lyrico-lyrics-wrapper">Palaakka paakka palikesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palaakka paakka palikesthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhalakku visirinadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhalakku visirinadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu kallalo kallu petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kallalo kallu petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kougilla illu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougilla illu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachaavu nuvvu annadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachaavu nuvvu annadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gunde gummamlo kaalupetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde gummamlo kaalupetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Guttantha petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guttantha petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Gurthunchuko mannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthunchuko mannadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhura nagarilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura nagarilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura nagarilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura nagarilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yamunaa thaatilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamunaa thaatilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Muraali swaramule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muraali swaramule"/>
</div>
<div class="lyrico-lyrics-wrapper">Musirina yaadhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musirina yaadhalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chentha koccheyyagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chentha koccheyyagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Chamakku chamakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamakku chamakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Churukku churukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Churukku churukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chatukku chatukku chitukkule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chatukku chatukku chitukkule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheeyi patteyagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheeyi patteyagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadakku thadakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadakku thadakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinakku dinakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinakku dinakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Udukku udukku dudukkule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udukku udukku dudukkule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvuleka chandamaama chinnaboye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvuleka chandamaama chinnaboye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu cheeri vennelantha velluvaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu cheeri vennelantha velluvaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu raaka mallepoolu thellaboye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu raaka mallepoolu thellaboye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu thaaki poolagutta thelikaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu thaaki poolagutta thelikaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye mataake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye mataake"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee rooju ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee rooju ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Innallu vechaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innallu vechaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dorakka dorakka dorikindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorakka dorakka dorikindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalukku chilaka idi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalukku chilaka idi"/>
</div>
<div class="lyrico-lyrics-wrapper">Palakka paakka palikesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palakka paakka palikesthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhalaakku visirinadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhalaakku visirinadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu kallalo kallu petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu kallalo kallu petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kougilla illu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougilla illu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachaavu nuvvu annadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachaavu nuvvu annadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gunde gummaamlo kaalupetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde gummaamlo kaalupetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Guttantha petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guttantha petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Gurthunchuko mannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthunchuko mannadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhura nagarilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura nagarilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura nagarilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura nagarilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yamunaa thatilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamunaa thatilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Muraali swaramule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muraali swaramule"/>
</div>
<div class="lyrico-lyrics-wrapper">Musirina yaadhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musirina yaadhalo"/>
</div>
</pre>
