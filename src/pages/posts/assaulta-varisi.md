---
title: "assaulta song lyrics"
album: "Varisi"
artist: "Nandha"
lyricist: "Umaramanan"
director: "Karthik Doss"
path: "/albums/varisi-song-lyrics"
song: "Assaulta"
image: ../../images/albumart/varisi.jpg
date: 2021-12-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/sVcvozelSrw"
type: "happy"
singers:
  - Seenu 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">assalta nee ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="assalta nee ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">getha than nee pinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="getha than nee pinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">engeyum nee win thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyum nee win thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">not out oh nee ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="not out oh nee ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">nock out nee pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nock out nee pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">epothum nee padu coola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothum nee padu coola"/>
</div>
<div class="lyrico-lyrics-wrapper">thil iruntha thil iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thil iruntha thil iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">epavum nee king da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epavum nee king da"/>
</div>
<div class="lyrico-lyrics-wrapper">illai inna illai inna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai inna illai inna"/>
</div>
<div class="lyrico-lyrics-wrapper">oothiduvom sangu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothiduvom sangu da"/>
</div>
<div class="lyrico-lyrics-wrapper">dandan nakka dandan nakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dandan nakka dandan nakka"/>
</div>
<div class="lyrico-lyrics-wrapper">danukunaka dangu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="danukunaka dangu da"/>
</div>
<div class="lyrico-lyrics-wrapper">therika vidu thierika vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therika vidu thierika vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">paatu putu sound da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatu putu sound da"/>
</div>
<div class="lyrico-lyrics-wrapper">othaya ponalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othaya ponalum "/>
</div>
<div class="lyrico-lyrics-wrapper">sakaya aanalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakaya aanalum"/>
</div>
<div class="lyrico-lyrics-wrapper">opitu epovum othatha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="opitu epovum othatha da"/>
</div>
<div class="lyrico-lyrics-wrapper">innaiki thothalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innaiki thothalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nallaiki jeyipome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallaiki jeyipome"/>
</div>
<div class="lyrico-lyrics-wrapper">unnakulle iruku power
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnakulle iruku power"/>
</div>
<div class="lyrico-lyrics-wrapper">power kattu terror
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="power kattu terror"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aasaya aasaya adaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaya aasaya adaki"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaikatha anubavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaikatha anubavi"/>
</div>
<div class="lyrico-lyrics-wrapper">anubavi eralama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anubavi eralama"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasa nee kaasa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasa nee kaasa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">sethu than vaikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethu than vaikatha"/>
</div>
<div class="lyrico-lyrics-wrapper">selavali selavali tharalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selavali selavali tharalama"/>
</div>
<div class="lyrico-lyrics-wrapper">irupom nalaiki theriyathu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupom nalaiki theriyathu da"/>
</div>
<div class="lyrico-lyrics-wrapper">inaike ippove kondadu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inaike ippove kondadu da"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku nee unaku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku nee unaku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kotta potta nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotta potta nee"/>
</div>
<div class="lyrico-lyrics-wrapper">aduchu nee noruku nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduchu nee noruku nee"/>
</div>
<div class="lyrico-lyrics-wrapper">veelatha vicket nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veelatha vicket nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">assalta nee ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="assalta nee ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">getha than nee pinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="getha than nee pinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">engeyum nee win thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyum nee win thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">not out oh nee ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="not out oh nee ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">nock out nee pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nock out nee pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">epothum nee padu coola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothum nee padu coola"/>
</div>
</pre>
