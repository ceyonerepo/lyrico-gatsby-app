---
title: "main sherni song lyrics"
album: "Sherni"
artist: "Utkarsh Dhotekar"
lyricist: "Raghav"
director: "Amit V. Masurkar"
path: "/albums/sherni-lyrics"
song: "Main Sherni"
image: ../../images/albumart/sherni.jpg
date: 2021-06-18
lang: hindi
youtubeLink: "https://www.youtube.com/embed/iobGAZAl0bY"
type: "happy"
singers:
  - Akasa Singh
  - Rap by - Raftaar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aakhon Mein Roshni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakhon Mein Roshni"/>
</div>
<div class="lyrico-lyrics-wrapper">Josh Hai Dil Mein Bhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Josh Hai Dil Mein Bhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Gehre Toofano Mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gehre Toofano Mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum Dhoondein Rasta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum Dhoondein Rasta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rahon Par Kaatein Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahon Par Kaatein Ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya Chahe Angaarein Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya Chahe Angaarein Ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Rukna Nahi Hai Humein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rukna Nahi Hai Humein"/>
</div>
<div class="lyrico-lyrics-wrapper">Rab Ka Vaasta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rab Ka Vaasta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ladenge Bhidenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladenge Bhidenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Girenge Uthenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girenge Uthenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh Dedi Humne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh Dedi Humne"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud Ko Zubaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud Ko Zubaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Musibat Ko Aisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musibat Ko Aisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum Panja Marenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum Panja Marenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghurrake Denge Aisi Lalkaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghurrake Denge Aisi Lalkaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darke Aage Dahaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darke Aage Dahaad"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Sherni Main Sherni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Sherni Main Sherni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chadh Lege Har Pahaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadh Lege Har Pahaad"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Sherni Main Sherni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Sherni Main Sherni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darke Aage Dahaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darke Aage Dahaad"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Sherni Main Sherni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Sherni Main Sherni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chadh Lege Har Pahaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadh Lege Har Pahaad"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Sherni Main Sherni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Sherni Main Sherni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namaskar!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namaskar!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">To All The Queens Out There
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To All The Queens Out There"/>
</div>
<div class="lyrico-lyrics-wrapper">You Know Who You Are?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Know Who You Are?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aye Rrrrh!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Rrrrh!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Movin’ Like A Don
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Movin’ Like A Don"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekhe Sab Yeh Godess Kaun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekhe Sab Yeh Godess Kaun"/>
</div>
<div class="lyrico-lyrics-wrapper">Every Step A Picture
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Every Step A Picture"/>
</div>
<div class="lyrico-lyrics-wrapper">Karlo Saare Slow Cam On
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karlo Saare Slow Cam On"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheeton Piche Ho Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheeton Piche Ho Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Karlo Sare Tiger Rest
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karlo Sare Tiger Rest"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy Aa Gayi Kabki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy Aa Gayi Kabki"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekho Aa Gayi Tigeress
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekho Aa Gayi Tigeress"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Woh Giri Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh Giri Padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh Uthi Chali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh Uthi Chali"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh Kare Kabhi Koyi Der Ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh Kare Kabhi Koyi Der Ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yun Jhund Bana Ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yun Jhund Bana Ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Peeche Pad Gaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peeche Pad Gaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Paya Koyi Gher Ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paya Koyi Gher Ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Woh Ghayal Zyada Ghatak Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh Ghayal Zyada Ghatak Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Toh Rakhna Koyi Bair Ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toh Rakhna Koyi Bair Ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu Khud Mein Jhaak Aur Dekhle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu Khud Mein Jhaak Aur Dekhle"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek Tujhme Bhi Hai Sherni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek Tujhme Bhi Hai Sherni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ladenge Bhidenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladenge Bhidenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Girenge Uthenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girenge Uthenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh Dedi Humne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh Dedi Humne"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud Ko Zubaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud Ko Zubaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Musibat Ko Aisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musibat Ko Aisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum Panja Marenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum Panja Marenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghurrake Denge Aisi Lalkaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghurrake Denge Aisi Lalkaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darke Aage Dahaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darke Aage Dahaad"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Sherni Main Sherni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Sherni Main Sherni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chadh Lege Har Pahaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadh Lege Har Pahaad"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Sherni Main Sherni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Sherni Main Sherni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darke Aage Dahaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darke Aage Dahaad"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Sherni Main Sherni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Sherni Main Sherni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chadh Lege Har Pahaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadh Lege Har Pahaad"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Sherni Main Sherni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Sherni Main Sherni"/>
</div>
</pre>
