---
title: "aatam song lyrics"
album: "Sila Nerangalil Sila Manidhargal"
artist: "Radhan"
lyricist: "Rakendu Mouli - M.C. Chetan"
director: "Vishal Venkat"
path: "/albums/sila-nerangalil-sila-manidhargal-lyrics"
song: "Aatam"
image: ../../images/albumart/sila-nerangalil-sila-manidhargal.jpg
date: 2022-01-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1m38s1y-u_Q"
type: "mass"
singers:
  - Andrea
  - M.C. Chetan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee neeyaaga iruppathul illai pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee neeyaaga iruppathul illai pizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kelvi kuri endraal yaar inge vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kelvi kuri endraal yaar inge vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha pooviyinile yaaridam illai kurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha pooviyinile yaaridam illai kurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvugal unarchigal soozhnilai karumvisirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvugal unarchigal soozhnilai karumvisirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karai serum varai oyavillai antha alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai serum varai oyavillai antha alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetra thaazhvu yenbathellaam oru mana nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetra thaazhvu yenbathellaam oru mana nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Break free from that karapanai tharum thirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break free from that karapanai tharum thirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellorum nam sondham yedaartham indha nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellorum nam sondham yedaartham indha nilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puriyaatha uraikutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaatha uraikutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaigal unakku theriyutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaigal unakku theriyutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Patharutha varundhutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patharutha varundhutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennangal sindhaigal udayutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennangal sindhaigal udayutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Level it up bedhangal udainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Level it up bedhangal udainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manitham nammulle kalanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manitham nammulle kalanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Light it up pagaiyai thuranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light it up pagaiyai thuranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragai virithu paranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai virithu paranthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattam ellaam konjam kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam ellaam konjam kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam ellaam vanthu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottam ellaam vanthu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedal kooda oyinthu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedal kooda oyinthu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuthaan inga nirandhiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuthaan inga nirandhiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattam ellaam konjam kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam ellaam konjam kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam ellaam vanthu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottam ellaam vanthu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedal kooda oyinthu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedal kooda oyinthu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuthaan inga nirandhiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuthaan inga nirandhiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen orumaiyai unara marakkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen orumaiyai unara marakkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen porumaiyai izhindthu nikkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen porumaiyai izhindthu nikkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen verumaiyil vaazhkkai tholaikkirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen verumaiyil vaazhkkai tholaikkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen varumaikodu ozhiyathuthen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen varumaikodu ozhiyathuthen "/>
</div>
<div class="lyrico-lyrics-wrapper">naadugal verupaadugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadugal verupaadugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen aayudham aatchi seivathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen aayudham aatchi seivathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen manithamum muzhikka marappathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen manithamum muzhikka marappathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen sudhanthiram verum pechaanadhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen sudhanthiram verum pechaanadhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirupori neruppu thandhavai yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirupori neruppu thandhavai yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naagareegam yena maatri vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naagareegam yena maatri vittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanathai thaandi bhoomiyai thondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathai thaandi bhoomiyai thondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyarkkaiyai seendi paarthu vittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyarkkaiyai seendi paarthu vittom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peraasaigalin vilaivugal yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peraasaigalin vilaivugal yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Peralayaaga pongaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peralayaaga pongaatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithan ennum inathaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithan ennum inathaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam muzhudhum saagaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam muzhudhum saagaathe"/>
</div>
</pre>
