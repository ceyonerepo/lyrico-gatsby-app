---
title: "kangal velaanadho song lyrics"
album: "Tamizhananean Ka"
artist: "Vynod Subramaniam"
lyricist: "Vynod Subramaniam"
director: "Sathish Ramakrishnan"
path: "/albums/tamizhananean-ka-song-lyrics"
song: "Kangal Velaanadho"
image: ../../images/albumart/tamizhananean-ka.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qKsF8TtGkLA"
type: "love"
singers:
  - Unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kangal velaanatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal velaanatho"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjam naaranatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjam naaranatho"/>
</div>
<div class="lyrico-lyrics-wrapper">vettathu min villaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettathu min villaku "/>
</div>
<div class="lyrico-lyrics-wrapper">oli ilanthatho pournami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oli ilanthatho pournami"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavaga amarnthavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavaga amarnthavalo"/>
</div>
<div class="lyrico-lyrics-wrapper">aval vaasam enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval vaasam enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir swasam aanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir swasam aanathe"/>
</div>
<div class="lyrico-lyrics-wrapper">aval vaasam enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval vaasam enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir swasam aanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir swasam aanathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kan paarka en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan paarka en "/>
</div>
<div class="lyrico-lyrics-wrapper">uyir kalaithathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir kalaithathe"/>
</div>
<div class="lyrico-lyrics-wrapper">avan viral pada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan viral pada "/>
</div>
<div class="lyrico-lyrics-wrapper">en manam thudikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manam thudikuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kai pada malarum mottaveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai pada malarum mottaveno"/>
</div>
<div class="lyrico-lyrics-wrapper">avan swasathil en kulal aadumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan swasathil en kulal aadumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vizhigal paarthathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigal paarthathal"/>
</div>
<div class="lyrico-lyrics-wrapper">manangal sernthathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manangal sernthathe"/>
</div>
<div class="lyrico-lyrics-wrapper">yamum manamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yamum manamum"/>
</div>
<div class="lyrico-lyrics-wrapper">ondravathuvai anthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondravathuvai anthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ko enna seithidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ko enna seithidumo"/>
</div>
<div class="lyrico-lyrics-wrapper">neerum nerupum malaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neerum nerupum malaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrum aliya uyirum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrum aliya uyirum "/>
</div>
<div class="lyrico-lyrics-wrapper">anuvin thugaluku anbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anuvin thugaluku anbathu"/>
</div>
<div class="lyrico-lyrics-wrapper">anukal inaithathu puriymo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anukal inaithathu puriymo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ondro ondro ondra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondro ondro ondra"/>
</div>
<div class="lyrico-lyrics-wrapper">thenanatho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenanatho "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">avan paarvai kaatril kalanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan paarvai kaatril kalanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">en moochinil pugunthathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en moochinil pugunthathe"/>
</div>
<div class="lyrico-lyrics-wrapper">uthirathil oodi oodi en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthirathil oodi oodi en"/>
</div>
<div class="lyrico-lyrics-wrapper">idhaya thudipu aanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhaya thudipu aanathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivin oosaiyai pattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivin oosaiyai pattam"/>
</div>
<div class="lyrico-lyrics-wrapper">poochiye ennai varudiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poochiye ennai varudiya"/>
</div>
<div class="lyrico-lyrics-wrapper">poongatre en uyirai avanidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poongatre en uyirai avanidam"/>
</div>
<div class="lyrico-lyrics-wrapper">ne ini kondu selvayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne ini kondu selvayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vettathu min villaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettathu min villaku "/>
</div>
<div class="lyrico-lyrics-wrapper">oli ilanthatho pournami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oli ilanthatho pournami"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavaga amarnthavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavaga amarnthavalo"/>
</div>
<div class="lyrico-lyrics-wrapper">aval vaasam enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval vaasam enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir swasam aanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir swasam aanathe"/>
</div>
<div class="lyrico-lyrics-wrapper">aval vaasam enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aval vaasam enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir swasam aanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir swasam aanathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anbe alage un uyir en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbe alage un uyir en"/>
</div>
<div class="lyrico-lyrics-wrapper">moochanathe un angangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochanathe un angangal"/>
</div>
<div class="lyrico-lyrics-wrapper">yaavum yaal aanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavum yaal aanathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en penmaiyin thimire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en penmaiyin thimire"/>
</div>
<div class="lyrico-lyrics-wrapper">avanidam adangiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanidam adangiyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarum thorka poril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarum thorka poril"/>
</div>
<div class="lyrico-lyrics-wrapper">avanai veeltha poradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanai veeltha poradi"/>
</div>
<div class="lyrico-lyrics-wrapper">thorpeno 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thorpeno "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhal kadhal kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal kadhal kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal kadhal kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal kadhal kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ne kalanthai en nenjukul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne kalanthai en nenjukul"/>
</div>
<div class="lyrico-lyrics-wrapper">ondragiya naan maraithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondragiya naan maraithen"/>
</div>
<div class="lyrico-lyrics-wrapper">un vinnukul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vinnukul"/>
</div>
<div class="lyrico-lyrics-wrapper">ne kalanthai en nenjukul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne kalanthai en nenjukul"/>
</div>
<div class="lyrico-lyrics-wrapper">ondragiya naan maraithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondragiya naan maraithen"/>
</div>
<div class="lyrico-lyrics-wrapper">un vinnukul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vinnukul"/>
</div>
</pre>
