---
title: "eeswaran vanthutan lyrics"
album: "Eeswaran"
artist: "Thaman S"
lyricist: "Yugabharathi"
director: "Susienthiran"
path: "/albums/eeswaran-song-lyrics"
song: "Eeswaran Vanthutan"
image: ../../images/albumart/eeswaran.jpg
date: 2021-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rNZTDeqCY6Q"
type: "Title Song"
singers:
  - Deepak Blue
  - Aravind Srinivas
  - Thaman S
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Eeswaran vanthuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeswaran vanthuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Arula thanthuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arula thanthuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Power yethithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power yethithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiya kattithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiya kattithan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaiva vanthuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiva vanthuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalamthula erangithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalamthula erangithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetiya kattithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetiya kattithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Matter mudichithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matter mudichithan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo podra ball
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo podra ball"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichi aadum annatha aatatha paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichi aadum annatha aatatha paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanilum all rounder kutatha seruu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanilum all rounder kutatha seruu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodutha vaaka kappathure kuzhantha manasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodutha vaaka kappathure kuzhantha manasu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakilama seiyum seilu perusu perusu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakilama seiyum seilu perusu perusu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eeswaran vanthuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeswaran vanthuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Arula thanthuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arula thanthuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Power yethithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power yethithan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhiya kattithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiya kattithan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaiva vanthuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiva vanthuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalamthula erangithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalamthula erangithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetiya kattithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetiya kattithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Matter mudichithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matter mudichithan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neraiya peru neraiya peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neraiya peru neraiya peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Avar solli pozhikkaranga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avar solli pozhikkaranga da"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha ulagam poora ulagam poora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ulagam poora ulagam poora"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravuthanu ninaikuranga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravuthanu ninaikuranga da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munna vecha kala avaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munna vecha kala avaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnala vechathum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnala vechathum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Osarum mattum pesi puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osarum mattum pesi puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya thaan vittatum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya thaan vittatum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Osanthu nikura appava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osanthu nikura appava"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan pozhuthu mathikkiraru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan pozhuthu mathikkiraru"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha nilamayil thozhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha nilamayil thozhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma thooki sumakkuraru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma thooki sumakkuraru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eeswaran vanthuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeswaran vanthuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeswaran vanthuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeswaran vanthuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Arula thanthuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arula thanthuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Arula thanthuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arula thanthuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Power yethithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power yethithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Power yethithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power yethithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiya kattithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiya kattithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiya kattithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiya kattithan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaiva vanthuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiva vanthuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiva vanthuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiva vanthuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalamthula erangithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalamthula erangithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalamthula erangithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalamthula erangithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetiya kattithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetiya kattithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetiya kattithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetiya kattithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Matter mudichithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matter mudichithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Matter mudichithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matter mudichithan"/>
</div>
</pre>
