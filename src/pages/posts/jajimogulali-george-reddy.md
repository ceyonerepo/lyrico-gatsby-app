---
title: "jajimogulali song lyrics"
album: "George Reddy"
artist: "Suresh Bobbili"
lyricist: "unknown"
director: "Jeevan Reddy"
path: "/albums/george-reddy-lyrics"
song: "Jajimogulali"
image: ../../images/albumart/george-reddy.jpg
date: 2019-11-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/fask7RWHlqY"
type: "melody"
singers:
  - Indravati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Masaka masaka mabbulintha jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masaka masaka mabbulintha jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">musurkune cheekatintha jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="musurkune cheekatintha jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">mansu ninda yennelunda jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mansu ninda yennelunda jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">vunde podhu poduku tenda jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vunde podhu poduku tenda jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">nidralunna melkununna jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nidralunna melkununna jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">navayuga pu alalu andham jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navayuga pu alalu andham jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">randi aadi paadudhamu jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="randi aadi paadudhamu jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">osmania galalai jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osmania galalai jajimogulali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naligi podhu nyayam yepudu jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naligi podhu nyayam yepudu jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">yediristhu ne adugeyyi jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yediristhu ne adugeyyi jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">anachi vesthey aagamani jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anachi vesthey aagamani jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">hakku lakai saval chey jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hakku lakai saval chey jajimogulali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">niraasha lanni doram cheyi jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niraasha lanni doram cheyi jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">aasha lodhi rajai jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasha lodhi rajai jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">penumantala tharam antu jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penumantala tharam antu jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">osmania jwalinchu jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osmania jwalinchu jajimogulali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">palley mana paatashala jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palley mana paatashala jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">sangarshana lenidhey jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangarshana lenidhey jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">maarabodhu mana charithra jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarabodhu mana charithra jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">samaragrula kanthu lentha jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samaragrula kanthu lentha jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">velakattani tyagam vundhi jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velakattani tyagam vundhi jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">sramikula rajyam kai jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sramikula rajyam kai jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">viplavala yugam idhani jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viplavala yugam idhani jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">osmania poratam jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osmania poratam jajimogulali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bhagath singu ni kanna jagathi jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhagath singu ni kanna jagathi jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">nava yugapu kalalu kandham jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nava yugapu kalalu kandham jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">idhi osmania aavesham jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhi osmania aavesham jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">idhi vidyartula poratam jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhi vidyartula poratam jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">randi aadi padudhaamu jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="randi aadi padudhaamu jajimogulali"/>
</div>
<div class="lyrico-lyrics-wrapper">sudigaalai redudhamu jajimogulali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudigaalai redudhamu jajimogulali"/>
</div>
</pre>
