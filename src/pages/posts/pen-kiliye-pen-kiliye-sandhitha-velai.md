---
title: "pen kiliye pen kiliye song lyrics"
album: "Sandhitha Velai"
artist: "Deva"
lyricist: "Vairamuthu"
director: "Ravichandran"
path: "/albums/sandhitha-velai-lyrics"
song: "Pen Kiliye Pen Kiliye"
image: ../../images/albumart/sandhitha-velai.jpg
date: 2000-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cVuZJTanBxI"
type: "happy"
singers:
  - P. Unnikrishnan
  - Sujatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pen kiliyae pen kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen kiliyae pen kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadugiren oru paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadugiren oru paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">En paatu vari pidithirundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paatu vari pidithirundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un siragal pachai kodi kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un siragal pachai kodi kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen kiliyae pen kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen kiliyae pen kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadugiren oru paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadugiren oru paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">En paatu vari pidithirundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paatu vari pidithirundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un siragal pachai kodi kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un siragal pachai kodi kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaai mozhi ellamae vaaimai sollaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai mozhi ellamae vaaimai sollaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzh manam pesamal unmai thondradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzh manam pesamal unmai thondradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaai mozhi ellamae vaaimai sollaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai mozhi ellamae vaaimai sollaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen kili poi sonnal aan kili thoongadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen kili poi sonnal aan kili thoongadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aan kiliyae aan kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aan kiliyae aan kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadugiren oru paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadugiren oru paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatu vari purindhu kondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatu vari purindhu kondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pallaviyai nee maatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pallaviyai nee maatru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanana nananana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanana nananana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanana nananana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanana nananana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanana nananana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanana nananana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanna nanna  naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanna nanna  naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen kangalae naadagam aadumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen kangalae naadagam aadumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen nenjamae oodagam aagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen nenjamae oodagam aagumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar solliyum pen manam ketkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar solliyum pen manam ketkumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai thattinaal mottukal pookumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai thattinaal mottukal pookumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidai ketten kelvi thandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai ketten kelvi thandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu pudhiraana pudhir allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu pudhiraana pudhir allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelvikullae badhil thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvikullae badhil thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu suvaiyaana suvai allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu suvaiyaana suvai allavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullathin vannam enna theriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathin vannam enna theriyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaithu chollum varai purivadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaithu chollum varai purivadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moodaadha poovukul endrum thaen illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodaadha poovukul endrum thaen illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen kiliyae pen kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen kiliyae pen kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadugiren oru paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadugiren oru paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">En paatu vari pidithirundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paatu vari pidithirundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un siragal pachai kodi kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un siragal pachai kodi kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En nenjilae aayiram osaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjilae aayiram osaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaadhilae ketkavae illaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhilae ketkavae illaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee aazhi pol alaigalai yevinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aazhi pol alaigalai yevinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan karaiyai pol mounamaai mevinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan karaiyai pol mounamaai mevinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil paasam kannil vesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil paasam kannil vesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu pen poosum aridhaaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu pen poosum aridhaaramaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmai kaana vanmai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai kaana vanmai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungal vizhi en mel pazhi podumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungal vizhi en mel pazhi podumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavai pirivadharku valimai undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavai pirivadharku valimai undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenjai purivadharku valimai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenjai purivadharku valimai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanal neer thedaadhae angae neer illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanal neer thedaadhae angae neer illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aan kiliyae aan kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aan kiliyae aan kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadugiren oru paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadugiren oru paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatu vari purindhu kondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatu vari purindhu kondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pallaviyai nee maatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pallaviyai nee maatru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen kiliyae pen kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen kiliyae pen kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadugiren oru paatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadugiren oru paatu"/>
</div>
<div class="lyrico-lyrics-wrapper">En paatu vari pidithirundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paatu vari pidithirundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un siragal pachai kodi kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un siragal pachai kodi kaatu"/>
</div>
</pre>
