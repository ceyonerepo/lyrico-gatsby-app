---
title: "papara mittai song lyrics"
album: "RK Nagar"
artist: "Premgi Amaren"
lyricist: "Kanchana logan"
director: "Saravana Rajan"
path: "/albums/rk-nagar-lyrics"
song: "Papara Mittai"
image: ../../images/albumart/rk-nagar.jpg
date: 2019-12-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pyxuzUz96RU"
type: "happy"
singers:
  - Gana Guna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yen Kavappu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kavappu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Karikali Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Karikali Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Thonjalu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thonjalu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Angelu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Angelu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla Mittai Coloru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla Mittai Coloru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaenu Mittai Uthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenu Mittai Uthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkathula Vandathummae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkathula Vandathummae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthuduchi Poweru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthuduchi Poweru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Retta Vellai Thosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Retta Vellai Thosai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmela Thaan Aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmela Thaan Aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiliya Pola Konji Konji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiliya Pola Konji Konji"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Perum Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Perum Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadi Titanic-Ku Heroinee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi Titanic-Ku Heroinee"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagula Gulapjamun Jeera Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagula Gulapjamun Jeera Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Weightula Godrej Beero Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weightula Godrej Beero Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Speedula Ronaldino Caro Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speedula Ronaldino Caro Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Chon Chon Chon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chon Chon Chon"/>
</div>
<div class="lyrico-lyrics-wrapper">Papara Mittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papara Mittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Vanthu Muththam Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Vanthu Muththam Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chon Chon Chon Papara Mittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chon Chon Chon Papara Mittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Vanthu Muththam Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Vanthu Muththam Kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Kavappu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kavappu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Kavappu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kavappu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Karikali Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Karikali Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Karikali Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Karikali Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Angelu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Angelu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Angelu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Angelu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Thonjalu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thonjalu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Thonjalu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thonjalu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Kavappu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kavappu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Kavappu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kavappu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Karikali Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Karikali Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Karikali Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Karikali Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Thonjalu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thonjalu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Thonjalu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thonjalu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Angeluuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Angeluuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla Mittai Coloru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla Mittai Coloru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaenu Mittai Uthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenu Mittai Uthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkathula Vandathummae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkathula Vandathummae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthuduchi Poweru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthuduchi Poweru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Retta Vellai Thosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Retta Vellai Thosai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmela Thaan Aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmela Thaan Aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiliya Pola Konji Konji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiliya Pola Konji Konji"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Perum Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Perum Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadi Titanic-Ku Heroinee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi Titanic-Ku Heroinee"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagula Gulapjamun Jeera Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagula Gulapjamun Jeera Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Weightula Godrej Beero Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weightula Godrej Beero Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Speedula Ronaldino Caro Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speedula Ronaldino Caro Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Chon Chon Chon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Chon Chon Chon"/>
</div>
<div class="lyrico-lyrics-wrapper">Papara Mittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papara Mittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Vanthu Muththam Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Vanthu Muththam Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chon Chon Chon Papara Mittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chon Chon Chon Papara Mittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Vanthu Muththam Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Vanthu Muththam Kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Kavappu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kavappu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Kavappu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kavappu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Karikali Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Karikali Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Karikali Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Karikali Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Angelu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Angelu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Angelu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Angelu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Thonjalu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thonjalu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Thonjalu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thonjalu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Kavappu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kavappu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Kavappu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kavappu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Karikali Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Karikali Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Karikali Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Karikali Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Thonjalu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thonjalu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Thonjalu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thonjalu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Angeluuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Angeluuu"/>
</div>
</pre>
