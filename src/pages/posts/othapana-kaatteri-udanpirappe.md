---
title: "othapana kaatteri song lyrics"
album: "Udanpirappe"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "Era. Saravanan"
path: "/albums/udanpirappe-lyrics"
song: "Othapana Kaatteri"
image: ../../images/albumart/udanpirappe.jpg
date: 2021-10-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/S8M4Fmpby10"
type: "happy"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oththa Panai Kaatteri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Panai Kaatteri"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththathu Thaan Un Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththathu Thaan Un Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu Thala Paambattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu Thala Paambattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koththuriye Vaazhva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththuriye Vaazhva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Panai Kaatteri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Panai Kaatteri"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththathu Thaan Un Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththathu Thaan Un Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu Thala Paambattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu Thala Paambattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koththuriye Vaazhva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththuriye Vaazhva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannazhagu Podhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannazhagu Podhaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Azhagai Kettaendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Azhagai Kettaendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Azhaga Ketta Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Azhaga Ketta Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaturiye Poochandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaturiye Poochandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallavala Polae Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallavala Polae Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Nadipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Nadipa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta Vara Yemaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Vara Yemaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Adipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Adipa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Panai Kaatteri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Panai Kaatteri"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththathu Thaan Un Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththathu Thaan Un Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu Thala Paambattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu Thala Paambattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koththuriye Vaazhva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththuriye Vaazhva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Panai Kaatteri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Panai Kaatteri"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththathu Thaan Un Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththathu Thaan Un Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu Thala Paambattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu Thala Paambattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koththuriye Vaazhva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththuriye Vaazhva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Paatha Yezha Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Paatha Yezha Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Neeyum Kaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Neeyum Kaata"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Nodiyil Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Nodiyil Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Norungiya Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norungiya Kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanivera Aanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanivera Aanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooluketha Saelai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooluketha Saelai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Podhum Settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Podhum Settai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa Usuraiyuym
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Usuraiyuym"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Unakena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Unakena"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatuvaene Thaalattaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatuvaene Thaalattaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagula Yenai Yendi Arukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagula Yenai Yendi Arukura"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasichadhum Yelai Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasichadhum Yelai Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Marukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marukura"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudusha Valainja Paiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudusha Valainja Paiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhaa Nellunu Paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhaa Nellunu Paakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Panai Kaatteri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Panai Kaatteri"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththathu Thaan Un Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththathu Thaan Un Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu Thala Paambattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu Thala Paambattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koththuriye Vaazhva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththuriye Vaazhva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Panai Kaatteri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Panai Kaatteri"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththathu Thaan Un Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththathu Thaan Un Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu Thala Paambattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu Thala Paambattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koththuriye Vaazhva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththuriye Vaazhva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannazhagu Podhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannazhagu Podhaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Azhagai Kettaendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Azhagai Kettaendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Azhaga Ketta Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Azhaga Ketta Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaturiye Poochandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaturiye Poochandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallavala Polae Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallavala Polae Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Nadipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Nadipa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta Vara Yemaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Vara Yemaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Adipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Adipa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Panai Kaatteri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Panai Kaatteri"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththathu Thaan Un Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththathu Thaan Un Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu Thala Paambattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu Thala Paambattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koththuriye Vaazhva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththuriye Vaazhva"/>
</div>
</pre>
