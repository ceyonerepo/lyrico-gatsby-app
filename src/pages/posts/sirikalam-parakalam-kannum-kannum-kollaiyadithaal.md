---
title: "sirikalam parakalam song lyrics"
album: "Kannum Kannum Kollaiyadithaal"
artist: "Masala Coffee - Harshavardhan Rameshwar"
lyricist: "Vignesh Shivan - Mani Amudhavan - Madurai Souljour - Hafeez Rumi - Desingh Periyasamy"
director: "Desingh Periyasamy"
path: "/albums/kannum-kannum-kollaiyadithaal-lyrics"
song: "Sirikalam Parakalam"
image: ../../images/albumart/kannum-kannum-kollaiyadithaal.jpg
date: 2020-02-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vlYv7Ri-GQU"
type: "Enjoyment"
singers:
  - Benny Dayal
  - Madurai Souljour	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sirikkalaam Parakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkalaam Parakkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraikkaigal Mulaithathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraikkaigal Mulaithathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Midhakkalaam Kudhikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhakkalaam Kudhikalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaigal Marakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigal Marakkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkalaam Parakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkalaam Parakkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraikkaigal Mulaithathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraikkaigal Mulaithathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Midhakkalaam Kudhikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhakkalaam Kudhikalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaigal Marakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigal Marakkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerangal Kaalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerangal Kaalangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chill Panna Thevai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chill Panna Thevai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheers Endru Koovi Paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheers Endru Koovi Paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sorgamum Dhooramilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgamum Dhooramilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saturday Night Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saturday Night Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Partigal Podhavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Partigal Podhavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andradam Sun Burn Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andradam Sun Burn Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ver Ingu Thevai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ver Ingu Thevai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkalaam Parakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkalaam Parakkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraikkaigal Mulaithathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraikkaigal Mulaithathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Midhakkalaam Kudhikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhakkalaam Kudhikalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaigal Marakkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigal Marakkumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkalaam Parakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkalaam Parakkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraikkaigal Mulaithathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraikkaigal Mulaithathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Midhakkalaam Kuthikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhakkalaam Kuthikalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaigal Marakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigal Marakkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kidaiyathu Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaiyathu Naalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru Mattum Kaiyil Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Mattum Kaiyil Undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaranam Thevai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaranam Thevai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkum Varaikkum Anubavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkum Varaikkum Anubavi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadigaram Mullaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigaram Mullaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalgal Engum Nirapathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal Engum Nirapathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Enna Sonnaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Enna Sonnaal Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku Pidithaal Edhum Sari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Pidithaal Edhum Sari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilakkanam Udaikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilakkanam Udaikkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padaipugal Pirakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaipugal Pirakkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhigalaal Niruthinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhigalaal Niruthinaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etti Udaithaal Kadhavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Udaithaal Kadhavugal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirakkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudiyatha Veppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyatha Veppam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moolaikkulla Patri Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolaikkulla Patri Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegam Vaandhaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam Vaandhaache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oivom Endra Ennam Pochey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oivom Endra Ennam Pochey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudivillaa Aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivillaa Aattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalibangal Ondrai Sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalibangal Ondrai Sera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Per Inbam Aache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per Inbam Aache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koochal Osai Bodhai Aache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koochal Osai Bodhai Aache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhiyadhaai Pirakka Naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiyadhaai Pirakka Naam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Virudhungal Nadakkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virudhungal Nadakkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isaikku Naam Isainthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaikku Naam Isainthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arangugal Adhiruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arangugal Adhiruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkalaam Parakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkalaam Parakkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraikkaigal Mulaithathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraikkaigal Mulaithathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Midhakkalaam Kudhikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhakkalaam Kudhikalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaigal Marakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigal Marakkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkalaam Parakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkalaam Parakkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraikkaigal Mulaithathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraikkaigal Mulaithathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Midhakkalaam Kudhikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhakkalaam Kudhikalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaigal Marakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigal Marakkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkalaam Parakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkalaam Parakkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraikkaigal Mulaithathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraikkaigal Mulaithathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Midhakkalaam Kuthikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhakkalaam Kuthikalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaigal Marakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigal Marakkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkalaam Parakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkalaam Parakkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraikkaigal Mulaithathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraikkaigal Mulaithathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mithakkalaam Kuthikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mithakkalaam Kuthikalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaigal Marakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigal Marakkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkalaam Parakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkalaam Parakkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraikkaigal Mulaithathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraikkaigal Mulaithathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mithakkalaam Kuthikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mithakkalaam Kuthikalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaigal Marakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigal Marakkume"/>
</div>
</pre>
