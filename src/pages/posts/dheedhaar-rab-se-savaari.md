---
title: "dheedhaar rab se song lyrics"
album: "Savaari"
artist: "Shekar Chandra"
lyricist: "Sriram"
director: "Saahith Mothkuri"
path: "/albums/savaari-lyrics"
song: "Dheedhaar Rab Se"
image: ../../images/albumart/savaari.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/d3jPDsE7FuE"
type: "happy"
singers:
  - Kareemulla
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dheedhaar Rab Se mukammal ho jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheedhaar Rab Se mukammal ho jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">tasawwur haqeeqat mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tasawwur haqeeqat mein"/>
</div>
<div class="lyrico-lyrics-wrapper">yoon badal jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yoon badal jaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheedhaar Rab Se mukammal ho jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheedhaar Rab Se mukammal ho jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">tasawwur haqeeqat mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tasawwur haqeeqat mein"/>
</div>
<div class="lyrico-lyrics-wrapper">yoon badal jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yoon badal jaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tere dar pe aaoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tere dar pe aaoon"/>
</div>
<div class="lyrico-lyrics-wrapper">main tujhme samaoooon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main tujhme samaoooon"/>
</div>
<div class="lyrico-lyrics-wrapper">yeh ulfat jo thujse..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeh ulfat jo thujse.."/>
</div>
<div class="lyrico-lyrics-wrapper">main rok na paaooon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="main rok na paaooon"/>
</div>
<div class="lyrico-lyrics-wrapper">qayamat bji aaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="qayamat bji aaye"/>
</div>
<div class="lyrico-lyrics-wrapper">usay cheer jaaoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usay cheer jaaoon"/>
</div>
<div class="lyrico-lyrics-wrapper">inaayat ho teri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inaayat ho teri"/>
</div>
<div class="lyrico-lyrics-wrapper">tho manzil main paoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tho manzil main paoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">khud se judaa main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khud se judaa main"/>
</div>
<div class="lyrico-lyrics-wrapper">ho raha hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ho raha hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">haq se tera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haq se tera"/>
</div>
<div class="lyrico-lyrics-wrapper">ho raha hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ho raha hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">zarra zarra dhoondtha hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zarra zarra dhoondtha hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">tujhko aye khudha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tujhko aye khudha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aagosh mein tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagosh mein tere"/>
</div>
<div class="lyrico-lyrics-wrapper">dono jaahan hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dono jaahan hai"/>
</div>
<div class="lyrico-lyrics-wrapper">hai zaahir hamaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hai zaahir hamaari"/>
</div>
<div class="lyrico-lyrics-wrapper">ibaadat kahaan hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ibaadat kahaan hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheedhaar Rab Se mukammal ho jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheedhaar Rab Se mukammal ho jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">tasawwur haqeeqat mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tasawwur haqeeqat mein"/>
</div>
<div class="lyrico-lyrics-wrapper">yoon badal jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yoon badal jaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naalonaa alajadi edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalonaa alajadi edho"/>
</div>
<div class="lyrico-lyrics-wrapper">madhininda oohalu enno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhininda oohalu enno"/>
</div>
<div class="lyrico-lyrics-wrapper">nee gundelo cheralani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee gundelo cheralani"/>
</div>
<div class="lyrico-lyrics-wrapper">chirunavvuga meravalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chirunavvuga meravalani"/>
</div>
<div class="lyrico-lyrics-wrapper">aaaa meravalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaaa meravalani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">matalakandhani bhavamedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matalakandhani bhavamedho"/>
</div>
<div class="lyrico-lyrics-wrapper">ninu chusake premaindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninu chusake premaindho"/>
</div>
<div class="lyrico-lyrics-wrapper">yugamula daagina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yugamula daagina"/>
</div>
<div class="lyrico-lyrics-wrapper">badha edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="badha edho"/>
</div>
<div class="lyrico-lyrics-wrapper">nee dhari cherchindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dhari cherchindho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee chelimi korindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee chelimi korindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">naa hrudhayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa hrudhayame"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu cheri aagindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu cheri aagindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">naa praname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa praname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheedhaar Rab Se mukammal ho jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheedhaar Rab Se mukammal ho jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">tasawwur haqeeqat mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tasawwur haqeeqat mein"/>
</div>
<div class="lyrico-lyrics-wrapper">yoon badal jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yoon badal jaaye"/>
</div>
</pre>
