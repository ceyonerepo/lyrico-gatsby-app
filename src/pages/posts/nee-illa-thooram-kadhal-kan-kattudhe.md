---
title: "nee illa thooram song lyrics"
album: "Kadhal Kan Kattudhe"
artist: "Pavan"
lyricist: "Mohanraja"
director: "Shivaraj"
path: "/albums/kadhal-kan-kattudhe-lyrics"
song: "Nee Illa Thooram"
image: ../../images/albumart/kadhal-kan-kattudhe.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jkEOUEXcSxY"
type: "sad"
singers:
  - Jananie SV
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee illa thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illa thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattum vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattum vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithaalum solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithaalum solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Verondrum ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verondrum ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegu thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum megamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum megamellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">En mele thooral podaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mele thooral podaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathodu pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathodu pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Orunaalum unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orunaalum unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seraathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seraathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee illa thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illa thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattum vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattum vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithaalum solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithaalum solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Verondrum ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verondrum ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam edhaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam edhaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">En kadhal sumakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kadhal sumakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimai mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam muzhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam muzhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">En kadhal irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kadhal irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannodu irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannodu irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ne ponapin kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne ponapin kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaarendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaarendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamal irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamal irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha vali mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha vali mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum ennul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum ennul"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee illa thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illa thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattum vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattum vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithaalum solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithaalum solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Verondrum ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verondrum ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegu thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum megamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum megamellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">En mele thooral podaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mele thooral podaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathodu pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathodu pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Orunaalum unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orunaalum unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seraathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seraathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee illa thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illa thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattum vendaam.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattum vendaam."/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithaalum solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithaalum solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Verondrum ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verondrum ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegu thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum megamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum megamellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">En mele thooral podaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mele thooral podaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathodu pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathodu pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Orunaalum unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orunaalum unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seraathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seraathe"/>
</div>
</pre>
