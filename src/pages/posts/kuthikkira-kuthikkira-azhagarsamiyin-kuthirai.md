---
title: "kuthikkira kuthikkira song lyrics"
album: "Azhagarsamiyin Kuthirai"
artist: "Ilaiyaraaja"
lyricist: "J. Francis Kiruba"
director: "Suseenthiran"
path: "/albums/azhagarsamiyin-kuthirai-lyrics"
song: "Kuthikkira Kuthikkira"
image: ../../images/albumart/azhagarsamiyin-kuthirai.jpg
date: 2011-05-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/033xk23prtg"
type: "happy"
singers:
  - Ilaiyaraaja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kuthikkira Kuthikkira Kuthirakutti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthikkira Kuthikkira Kuthirakutti "/>
</div>
<div class="lyrico-lyrics-wrapper">En Manasa Kaattuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasa Kaattuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Thidikkuthu Thudikkuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Thidikkuthu Thudikkuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasupadi En Usura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasupadi En Usura "/>
</div>
<div class="lyrico-lyrics-wrapper">Meettuthu Paal Pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meettuthu Paal Pola "/>
</div>
<div class="lyrico-lyrics-wrapper">Pani Pola Niram Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Pola Niram Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aae Ther Pola Asanchaadum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aae Ther Pola Asanchaadum "/>
</div>
<div class="lyrico-lyrics-wrapper">Nada Thaane Aae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nada Thaane Aae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Nelathula Nadukkuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Nelathula Nadukkuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nelavukutti Ithu Nesam Thaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelavukutti Ithu Nesam Thaane "/>
</div>
<div class="lyrico-lyrics-wrapper">Thisa Edukkuthu Padikkuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisa Edukkuthu Padikkuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thavilu Katti Puthu Sugam Thaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavilu Katti Puthu Sugam Thaane "/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthikkira Kuthikkira Kuthirakutti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthikkira Kuthikkira Kuthirakutti "/>
</div>
<div class="lyrico-lyrics-wrapper">En Manasa Kaattuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasa Kaattuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Thidikkuthu Thudikkuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Thidikkuthu Thudikkuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasupadi En Usura Meettuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasupadi En Usura Meettuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Rotti Vaikatta Jodi Katti Vaikatta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rotti Vaikatta Jodi Katti Vaikatta "/>
</div>
<div class="lyrico-lyrics-wrapper">Pulla Kutti Onnu Pethu Pottaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulla Kutti Onnu Pethu Pottaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Pathu Kollatta Pothi Vaikatta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu Kollatta Pothi Vaikatta "/>
</div>
<div class="lyrico-lyrics-wrapper">Nethi Pottu Vaikattaa Ettu Pattiyayum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethi Pottu Vaikattaa Ettu Pattiyayum "/>
</div>
<div class="lyrico-lyrics-wrapper">Aenga Vaikkattaa Patharaama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aenga Vaikkattaa Patharaama "/>
</div>
<div class="lyrico-lyrics-wrapper">Nada Podu Ulagam Namathaachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nada Podu Ulagam Namathaachu "/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaa Paya Usuru Aenguthulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaa Paya Usuru Aenguthulla "/>
</div>
</pre>
