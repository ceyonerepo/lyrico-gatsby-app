---
title: "rise of don song lyrics"
album: "Junga"
artist: "Siddharth Vipin"
lyricist: "Lalithanand"
director: "Gokul"
path: "/albums/junga-lyrics"
song: "Rise Of Don"
image: ../../images/albumart/junga.jpg
date: 2018-07-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WZbOu51kEFI"
type: "happy"
singers:
  - Suraj Jagan
  - Siddharth Vipin
  - Rockstar Ramani Ammal
  - Emcee Jesz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">High class ladies and gentlemen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="High class ladies and gentlemen"/>
</div>
<div class="lyrico-lyrics-wrapper">Introducing the brand new
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Introducing the brand new"/>
</div>
<div class="lyrico-lyrics-wrapper">Fresh gangster in the town
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fresh gangster in the town"/>
</div>
<div class="lyrico-lyrics-wrapper">Totally new trend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Totally new trend"/>
</div>
<div class="lyrico-lyrics-wrapper">He will make you bend makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He will make you bend makka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah…age 32 shirt 42 weight 82
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah…age 32 shirt 42 weight 82"/>
</div>
<div class="lyrico-lyrics-wrapper">What you gonna do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What you gonna do"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollunga keppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollunga keppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai gang-u naanga thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai gang-u naanga thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodittu poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodittu poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Center of the chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Center of the chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethukki kondaan thannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethukki kondaan thannai"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetka vanthaan mannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetka vanthaan mannai"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevan velvaan ivanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevan velvaan ivanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polamburaanga police
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamburaanga police"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadunguraanga rowdies
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadunguraanga rowdies"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Discount-il service
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Discount-il service"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dariyalil darr aagum parrys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dariyalil darr aagum parrys"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Junga aaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga aaaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga junga junga junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga junga junga junga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethukku ezhu colour-ula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku ezhu colour-ula"/>
</div>
<div class="lyrico-lyrics-wrapper">Rainbow rainbow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rainbow rainbow"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu colour-u thaan poo poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu colour-u thaan poo poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukku aala thookka thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku aala thookka thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tempo tempo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tempo tempo"/>
</div>
<div class="lyrico-lyrics-wrapper">Cycle pothum da sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cycle pothum da sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayyayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukku podanum sketch-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku podanum sketch-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukulla moonchila kuduppan punch-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukulla moonchila kuduppan punch-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei…ethukku podanum switch-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei…ethukku podanum switch-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga moon light-la mudippan match-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga moon light-la mudippan match-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Junga junga aaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga junga aaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga junga junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga junga junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh junga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaya katti vayitha kattiJunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya katti vayitha kattiJunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhugira singa kutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhugira singa kutty"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga"/>
</div>
<div class="lyrico-lyrics-wrapper">En peraandi vellakatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En peraandi vellakatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Velukka poraan round-u katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velukka poraan round-u katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Son of ranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Son of ranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Grandson of linga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Grandson of linga"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthuthaan da junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthuthaan da junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthuthaan da junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthuthaan da junga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Junga junga aaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga junga aaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatchi poiyaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatchi poiyaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavellaam meiyaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavellaam meiyaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathil pasi vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathil pasi vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kanavae unavaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kanavae unavaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nookam irunthalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nookam irunthalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi thokkam paranthodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi thokkam paranthodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkam yen nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkam yen nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini theakkam udainthodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini theakkam udainthodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Junga junga junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga junga junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jungajunga junga junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jungajunga junga junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga junga junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga junga junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Junga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Center of the Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Center of the Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethukki kondaan thannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethukki kondaan thannai"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetka vanthaan mannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetka vanthaan mannai"/>
</div>
<div class="lyrico-lyrics-wrapper">Junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevan velvaan ivanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevan velvaan ivanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polamburaanga police
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamburaanga police"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadunguraanga rowdies
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadunguraanga rowdies"/>
</div>
<div class="lyrico-lyrics-wrapper">Discount-il service
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Discount-il service"/>
</div>
<div class="lyrico-lyrics-wrapper">Dariyalil darr aagum parrys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dariyalil darr aagum parrys"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Junga junga aaa aaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Junga junga aaa aaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jungajunga junga junga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jungajunga junga junga"/>
</div>
</pre>
