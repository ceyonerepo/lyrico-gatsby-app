---
title: "koelae song lyrics"
album: "RRR Tamil"
artist: "Maragathamani"
lyricist: "Madhan Karky"
director: "S.S. Rajamouli "
path: "/albums/rrr-tamil-lyrics"
song: "Koelae"
image: ../../images/albumart/rrr-tamil.jpg
date: 2022-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ElO8AvR55Xg"
type: "happy"
singers:
  - Vishal Mishra
  - Benny Dayal
  - Sahithi Chaganti
  - Harika Narayan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naethellaam marandhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naethellaam marandhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Adingadaa jaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adingadaa jaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathula urumida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathula urumida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottunga konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottunga konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naethellaam marandhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naethellaam marandhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Adingadaa jaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adingadaa jaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathula urumida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathula urumida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottunga konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottunga konda"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindaa konda kathi chuthi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindaa konda kathi chuthi "/>
</div>
<div class="lyrico-lyrics-wrapper">kindha kundha keemae kooli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kindha kundha keemae kooli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idikkira koalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idikkira koalae "/>
</div>
<div class="lyrico-lyrics-wrapper">Chenjoatru koelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chenjoatru koelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivakkura koalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivakkura koalae "/>
</div>
<div class="lyrico-lyrics-wrapper">Sivangangai koelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivangangai koelae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nedu nedu koelae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedu nedu koelae "/>
</div>
<div class="lyrico-lyrics-wrapper">Echangal koelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Echangal koelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Achamillai cholluchu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achamillai cholluchu "/>
</div>
<div class="lyrico-lyrics-wrapper">ettaiyaburathu koelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettaiyaburathu koelae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naethellaam marandhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naethellaam marandhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Adingadaa jaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adingadaa jaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathula urumida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathula urumida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottunga konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottunga konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratham ranam rauthiramaa maaruchoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratham ranam rauthiramaa maaruchoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pithamunnu irudhaiyamu paaduchoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pithamunnu irudhaiyamu paaduchoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadi naerambelaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadi naerambelaam "/>
</div>
<div class="lyrico-lyrics-wrapper">veri paanjucho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veri paanjucho"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil sogamelaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil sogamelaam "/>
</div>
<div class="lyrico-lyrics-wrapper">muzhusaathaa mudunjuso
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muzhusaathaa mudunjuso"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi aadaama vaera 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi aadaama vaera "/>
</div>
<div class="lyrico-lyrics-wrapper">eppudi kondaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppudi kondaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppalaam kottaama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppalaam kottaama "/>
</div>
<div class="lyrico-lyrics-wrapper">naan eppadi aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan eppadi aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta poatta koothaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta poatta koothaa "/>
</div>
<div class="lyrico-lyrics-wrapper">koomae vekkaa koomae koelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koomae vekkaa koomae koelae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalavara koalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavara koalae "/>
</div>
<div class="lyrico-lyrics-wrapper">Kalkathaa koelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalkathaa koelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudiyaadhaar koalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudiyaadhaar koalae "/>
</div>
<div class="lyrico-lyrics-wrapper">Gujraathi koelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gujraathi koelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuchidum koelae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuchidum koelae "/>
</div>
<div class="lyrico-lyrics-wrapper">Ettooru koelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettooru koelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuninje cherukkae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninje cherukkae "/>
</div>
<div class="lyrico-lyrics-wrapper">thirunelvaeli koalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirunelvaeli koalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naethellaam marandhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naethellaam marandhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Adingadaa jaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adingadaa jaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathula urumida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathula urumida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottunga konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottunga konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chuthu chuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuthu chuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuthu chuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuthu chuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuthu chuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuthu chuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuthu chuthu chuthu chuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuthu chuthu chuthu chuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuthu chuthu chuthu chuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuthu chuthu chuthu chuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuthu chuthu chuthu chuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuthu chuthu chuthu chuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuthu chuthu chuthu chuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuthu chuthu chuthu chuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chuthuda chuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuthuda chuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">thalaippaaga chuthudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaippaaga chuthudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mala madhuchu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala madhuchu "/>
</div>
<div class="lyrico-lyrics-wrapper">kaikaappa maathuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaikaappa maathuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavasamunaa namma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavasamunaa namma "/>
</div>
<div class="lyrico-lyrics-wrapper">nenjunnu cholludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjunnu cholludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma maanam maela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma maanam maela "/>
</div>
<div class="lyrico-lyrics-wrapper">kaivecha vetudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaivecha vetudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhikkaamaraasa hae hae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhikkaamaraasa hae hae "/>
</div>
<div class="lyrico-lyrics-wrapper">neevecha pattaasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neevecha pattaasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koamasariyaasa kulaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koamasariyaasa kulaa "/>
</div>
<div class="lyrico-lyrics-wrapper">adudaa kanaeshaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adudaa kanaeshaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaa munnaa mullaa chullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaa munnaa mullaa chullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilae pilae paalae paalae poadae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilae pilae paalae paalae poadae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala pathavecha koalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala pathavecha koalae "/>
</div>
<div class="lyrico-lyrics-wrapper">Punjaabi koalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punjaabi koalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga thanga koalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga thanga koalae "/>
</div>
<div class="lyrico-lyrics-wrapper">Thathudhiri koalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathudhiri koalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koamalathu koalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koamalathu koalae "/>
</div>
<div class="lyrico-lyrics-wrapper">pala haasi koalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala haasi koalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri ellaam muzhanguchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri ellaam muzhanguchi "/>
</div>
<div class="lyrico-lyrics-wrapper">veer marathi koalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veer marathi koalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naethellaam marandhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naethellaam marandhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Adingadaa jaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adingadaa jaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathula urumida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathula urumida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottunga konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottunga konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naethellaam marandhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naethellaam marandhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Adingadaa jaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adingadaa jaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathula urumida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathula urumida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottunga konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottunga konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muru muru muru muru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muru muru muru muru"/>
</div>
</pre>
