---
title: "mudivilla mazhaiyodu song lyrics"
album: "Vandhaan Vendraan"
artist: "Thaman"
lyricist: "Madhan Karky"
director: "R. Kannan"
path: "/albums/vandhaan-vendraan-lyrics"
song: "Mudivilla Mazhaiyodu"
image: ../../images/albumart/vandhaan-vendraan.jpg
date: 2011-09-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ap2hOC3E14U"
type: "happy"
singers:
  - Hariharan
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mudivilla mazhaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivilla mazhaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaadum engal koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadum engal koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Adivaanin niram ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivaanin niram ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralodu otti kollattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralodu otti kollattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidikaalai nilavodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidikaalai nilavodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam punnagaiyin moottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam punnagaiyin moottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi nenjil urchaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi nenjil urchaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karpooram polae pattrattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpooram polae pattrattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seerippaayum vellam ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerippaayum vellam ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam thulli aadattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam thulli aadattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu theeyin pandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu theeyin pandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaalgal ingae odattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaalgal ingae odattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi vaithaal adhirattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vaithaal adhirattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanmeengal udhirattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanmeengal udhirattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraadum mattum yedhum ettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadum mattum yedhum ettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam mutti kottattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam mutti kottattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudivilla mazhaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivilla mazhaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaadum engal koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadum engal koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Adivaanin niram ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivaanin niram ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralodu otti kollattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralodu otti kollattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey un paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey un paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam theiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam theiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaazhkai endrum maaraadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaazhkai endrum maaraadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan eeram konjam kaayaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan eeram konjam kaayaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaayam ondrum aaraadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaayam ondrum aaraadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un tholvigal aayiram ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un tholvigal aayiram ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoranam korthidu thozhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranam korthidu thozhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha vetriyin vaasalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha vetriyin vaasalai "/>
</div>
<div class="lyrico-lyrics-wrapper">cheraKaaranam paarthidu thozhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheraKaaranam paarthidu thozhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seerippaayum vellam ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerippaayum vellam ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam thulli aadattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam thulli aadattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu theeyin pandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu theeyin pandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaalgal ingae odattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaalgal ingae odattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi vaithaal adhirattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vaithaal adhirattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanmeengal udhirattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanmeengal udhirattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraadum mattum yedhum ettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadum mattum yedhum ettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam mutti kottattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam mutti kottattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudivilla mazhaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivilla mazhaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaadum engal koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadum engal koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Adivaanin niram ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivaanin niram ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralodu otti kollattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralodu otti kollattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paadhai sellum neelam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paadhai sellum neelam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam punnagaiyin neelangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam punnagaiyin neelangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yeri varum aazhanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yeri varum aazhanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam inbangalin aazhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam inbangalin aazhangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Setrilae nee vizhunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setrilae nee vizhunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaamarai maalaigal maattidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaamarai maalaigal maattidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netru nee por izhanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru nee por izhanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai un naal ena kaattidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai un naal ena kaattidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seerippaayum vellam ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seerippaayum vellam ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam thulli aadattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam thulli aadattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu theeyin pandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu theeyin pandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaalgal ingae odattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaalgal ingae odattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi vaithaal adhirattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vaithaal adhirattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanmeengal udhirattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanmeengal udhirattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraadum mattum yedhum ettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadum mattum yedhum ettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam mutti kottattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam mutti kottattum"/>
</div>
</pre>
