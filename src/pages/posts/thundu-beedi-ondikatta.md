---
title: "thundu beedi song lyrics"
album: "Ondikatta"
artist: "Bharani"
lyricist: "R Dharmaraj"
director: "Bharani"
path: "/albums/ondikatta-lyrics"
song: "Thundu Beedi"
image: ../../images/albumart/ondikatta.jpg
date: 2018-07-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2q3pG8jEg7w"
type: "happy"
singers:
  - R Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thundu thundu thundu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thundu thundu thundu "/>
</div>
<div class="lyrico-lyrics-wrapper">thundu thundu beedi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thundu thundu beedi da"/>
</div>
<div class="lyrico-lyrics-wrapper">thundu beedi illaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thundu beedi illaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">thookam varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookam varathu"/>
</div>
<div class="lyrico-lyrics-wrapper">saraku adika villaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saraku adika villaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">satham varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham varathu"/>
</div>
<div class="lyrico-lyrics-wrapper">enada vaalkai ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enada vaalkai ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">ennada vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennada vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">aava vaangi podalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aava vaangi podalana"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu thangathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu thangathu"/>
</div>
<div class="lyrico-lyrics-wrapper">sanda vambu illaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sanda vambu illaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">poluthe pogathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poluthe pogathu"/>
</div>
<div class="lyrico-lyrics-wrapper">enada valkai ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enada valkai ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">ennada vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennada vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasu panam thedurathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu panam thedurathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thinamum enga polappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinamum enga polappu"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam konjam oothikita 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam konjam oothikita "/>
</div>
<div class="lyrico-lyrics-wrapper">sugama odum life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugama odum life"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thundu beedi thundu beedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thundu beedi thundu beedi"/>
</div>
<div class="lyrico-lyrics-wrapper">thundu beedi illaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thundu beedi illaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">thookam varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookam varathu"/>
</div>
<div class="lyrico-lyrics-wrapper">saraku adika villaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saraku adika villaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">satham varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham varathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalaiyila eluntha maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaiyila eluntha maten"/>
</div>
<div class="lyrico-lyrics-wrapper">moonji keenji kaluva maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonji keenji kaluva maten"/>
</div>
<div class="lyrico-lyrics-wrapper">aathathula inge enge da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathathula inge enge da"/>
</div>
<div class="lyrico-lyrics-wrapper">sata phant thuvaika maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sata phant thuvaika maten"/>
</div>
<div class="lyrico-lyrics-wrapper">olunga thechu kulikka maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olunga thechu kulikka maten"/>
</div>
<div class="lyrico-lyrics-wrapper">istathuku nan nadapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="istathuku nan nadapen"/>
</div>
<div class="lyrico-lyrics-wrapper">ange inge da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ange inge da"/>
</div>
<div class="lyrico-lyrics-wrapper">pasikira nerathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasikira nerathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">soru thanne thinga maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soru thanne thinga maten"/>
</div>
<div class="lyrico-lyrics-wrapper">kachal ethum vantha kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kachal ethum vantha kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">marunthu vangi poda maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marunthu vangi poda maten"/>
</div>
<div class="lyrico-lyrics-wrapper">enna pola yaru ada yaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pola yaru ada yaru"/>
</div>
<div class="lyrico-lyrics-wrapper">intha oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">sonna padi kelu ada kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonna padi kelu ada kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu thappe illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu thappe illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thundu beedi thundu beedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thundu beedi thundu beedi"/>
</div>
<div class="lyrico-lyrics-wrapper">thundu beedi illaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thundu beedi illaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">thookam varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookam varathu"/>
</div>
<div class="lyrico-lyrics-wrapper">saraku adika villaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saraku adika villaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">satham varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham varathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">road poora kuppa kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="road poora kuppa kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">naduvula nan than pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naduvula nan than pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">uppu moota potu vachan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppu moota potu vachan"/>
</div>
<div class="lyrico-lyrics-wrapper">kuppa koolam naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuppa koolam naan"/>
</div>
<div class="lyrico-lyrics-wrapper">aathu thanee nanum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathu thanee nanum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulathu thanee nanum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulathu thanee nanum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">sakadaya thengi nikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sakadaya thengi nikum"/>
</div>
<div class="lyrico-lyrics-wrapper">koovam thanni naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koovam thanni naan"/>
</div>
<div class="lyrico-lyrics-wrapper">mappu innum venumunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mappu innum venumunna"/>
</div>
<div class="lyrico-lyrics-wrapper">thuttu paiyil venumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuttu paiyil venumada"/>
</div>
<div class="lyrico-lyrics-wrapper">sappu kotti sapudave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sappu kotti sapudave"/>
</div>
<div class="lyrico-lyrics-wrapper">uppu kari pothumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppu kari pothumada"/>
</div>
<div class="lyrico-lyrics-wrapper">ullapadi oore intha oore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullapadi oore intha oore"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaiyikulla endru solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaiyikulla endru solli"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalum aasa ennukitta illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalum aasa ennukitta illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thundu beedi thundu beedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thundu beedi thundu beedi"/>
</div>
<div class="lyrico-lyrics-wrapper">thundu beedi illaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thundu beedi illaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">thookam varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookam varathu"/>
</div>
<div class="lyrico-lyrics-wrapper">saraku adika villaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saraku adika villaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">satham varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham varathu"/>
</div>
<div class="lyrico-lyrics-wrapper">enada vaalkai ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enada vaalkai ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">ennada vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennada vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">aava vaangi podalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aava vaangi podalana"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu thangathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu thangathu"/>
</div>
<div class="lyrico-lyrics-wrapper">sanda vambu illaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sanda vambu illaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">poluthe pogathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poluthe pogathu"/>
</div>
<div class="lyrico-lyrics-wrapper">enada valkai ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enada valkai ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">ennada vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennada vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasu panam thedurathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu panam thedurathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thinamum enga polappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinamum enga polappu"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam konjam oothikita 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam konjam oothikita "/>
</div>
<div class="lyrico-lyrics-wrapper">sugama odum life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugama odum life"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thundu beedi thundu beedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thundu beedi thundu beedi"/>
</div>
<div class="lyrico-lyrics-wrapper">thundu beedi illaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thundu beedi illaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">thookam varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookam varathu"/>
</div>
<div class="lyrico-lyrics-wrapper">saraku adika villaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saraku adika villaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">satham varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham varathu"/>
</div>
</pre>
