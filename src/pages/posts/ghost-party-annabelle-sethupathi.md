---
title: "ghost party song lyrics"
album: "Annabelle Sethupathi"
artist: "Krishna Kishor"
lyricist: "Ku. Karthik"
director: "Deepak Sundarrajan"
path: "/albums/annabelle-sethupathi-lyrics"
song: "Ghost Party"
image: ../../images/albumart/annabelle-sethupathi.jpg
date: 2021-09-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QYVzXIZkZf4"
type: "happy"
singers:
  - Jonita Gandhi
  - Aaryan Dinesh Kanagaratnam
  - Yashita Sharma
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aavi Parakkuthu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavi Parakkuthu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodachu Enga Petta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodachu Enga Petta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru Thoongura Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Thoongura Bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Start Aagum Enga Vetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Start Aagum Enga Vetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Kelambuthu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Kelambuthu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellame Enga Setta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellame Enga Setta"/>
</div>
<div class="lyrico-lyrics-wrapper">Night Vidiyuthu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night Vidiyuthu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Thaane Enga Kotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Thaane Enga Kotta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devi Chandramukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi Chandramukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Kanchanaakku Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kanchanaakku Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Freaky Dracula Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Freaky Dracula Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Conjuringa Kammi Kammi Allu Kammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Conjuringa Kammi Kammi Allu Kammi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Aranmanai Vaasal Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Aranmanai Vaasal Idhu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhillukku Dhuttu Naan Tharuven Variya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhillukku Dhuttu Naan Tharuven Variya"/>
</div>
<div class="lyrico-lyrics-wrapper">Arundhathi Ava Ullatha Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arundhathi Ava Ullatha Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili Bungili Kadhava Thoraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili Bungili Kadhava Thoraya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruttu Araiyila Ennamo Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttu Araiyila Ennamo Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayama Irukku Darling Muniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayama Irukku Darling Muniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Petromaxu Light Ah Eduthu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petromaxu Light Ah Eduthu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum Moginiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum Moginiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Halloweenu Party Naanga Seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halloweenu Party Naanga Seivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekka Aal Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekka Aal Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Follow Panni Vanthuduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Follow Panni Vanthuduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Kaal Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Kaal Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Number 13 Enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Number 13 Enga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dooru Numberu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooru Numberu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Dora Maya Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Dora Maya Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Naala Member
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Naala Member"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathu Karuppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Karuppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Iruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Iruppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Baagamathiya Ready Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baagamathiya Ready Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnala Vandhu Unnai Pudippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnala Vandhu Unnai Pudippa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aavi Parakkuthu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavi Parakkuthu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodachu Enga Petta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodachu Enga Petta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru Thoongura Bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Thoongura Bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Start Aagum Enga Vetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Start Aagum Enga Vetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Kelambuthu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Kelambuthu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellame Enga Setta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellame Enga Setta"/>
</div>
<div class="lyrico-lyrics-wrapper">Night Vidiyuthu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night Vidiyuthu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Thaane Enga Kotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Thaane Enga Kotta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devi Chandramukhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi Chandramukhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Kanchanaakku Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kanchanaakku Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Freaky Dracula Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Freaky Dracula Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Conjuringa Kammi Kammi Allu Kammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Conjuringa Kammi Kammi Allu Kammi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Aranmanai Vaasal Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Aranmanai Vaasal Idhu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhillukku Dhuttu Naan Tharuven Variya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhillukku Dhuttu Naan Tharuven Variya"/>
</div>
<div class="lyrico-lyrics-wrapper">Arundhathi Ava Ullatha Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arundhathi Ava Ullatha Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangili Bungili Kadhava Thoraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangili Bungili Kadhava Thoraya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruttu Araiyila Ennamo Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttu Araiyila Ennamo Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayama Irukku Darling Muniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayama Irukku Darling Muniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Petromaxu Light Ah Eduthu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petromaxu Light Ah Eduthu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum Moginiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum Moginiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aavi Parakkuthu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavi Parakkuthu Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodachu Enga Petta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodachu Enga Petta"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Petta Enga Settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Petta Enga Settai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Petta Enga Settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Petta Enga Settai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Enga Eng Eng Enga Petta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Enga Eng Eng Enga Petta"/>
</div>
</pre>
