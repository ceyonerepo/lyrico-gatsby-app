---
title: "tik tik tik title track song lyrics"
album: "Tik Tik Tik"
artist: "D. Imman"
lyricist: "Madhan Karky"
director: "Shakti Soundar Rajan"
path: "/albums/tik-tik-tik-lyrics"
song: "Tik Tik Tik Title Track"
image: ../../images/albumart/tik-tik-tik.jpg
date: 2018-06-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VzHaIWXWY-I"
type: "title track"
singers:
  - Yuvan Shankar Raja
  - Sunitha Sarathy
  - Yogi B	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Un sevigalil sottu chottena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sevigalil sottu chottena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala thuliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala thuliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivvadhirvugal undhal irudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvadhirvugal undhal irudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum oliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum oliyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evvathin nudhigaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvathin nudhigaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaatrum yedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaatrum yedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvaaru ingae pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvaaru ingae pizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pavvathin thulimondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavvathin thulimondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaganjeyyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaganjeyyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avvaanil engae mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avvaanil engae mazhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evvathin nudhigaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvathin nudhigaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaatrum yedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaatrum yedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvaaru ingae pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvaaru ingae pizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pavvathin thulimondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavvathin thulimondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaganjeyyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaganjeyyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avvaanil engae mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avvaanil engae mazhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhi siridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi siridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi peridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi peridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai siridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai siridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai peridhu yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai peridhu yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vali siridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali siridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi peridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi peridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porul siridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porul siridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arul peridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arul peridhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un sevigalil sottu chottena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sevigalil sottu chottena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala thuliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala thuliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivvadhirvugal undhal irudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvadhirvugal undhal irudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum oliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum oliyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un sevigalil sottu chottena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sevigalil sottu chottena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala thuliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala thuliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivvadhirvugal undhal irudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvadhirvugal undhal irudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum oliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum oliyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modhal thodangum padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhal thodangum padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarinthuvitta podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarinthuvitta podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulanthaigal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulanthaigal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam nirkka pidivaatham vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam nirkka pidivaatham vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravu vaanathai paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu vaanathai paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolikum natchathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolikum natchathiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae thaan pogalaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae thaan pogalaam vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yerikol aagasavanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerikol aagasavanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal nijamagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal nijamagalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum munnetram adainthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum munnetram adainthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattil irunthu vinvelikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattil irunthu vinvelikku"/>
</div>
<div class="lyrico-lyrics-wrapper">We are astronomical
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We are astronomical"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi-odi naam nadi thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi-odi naam nadi thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilakku adaivathai latchiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilakku adaivathai latchiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetriyudan naam thirumbuvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriyudan naam thirumbuvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir mel sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir mel sathiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namakkena pala porgal irukkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkena pala porgal irukkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyirundhoru vinkal varuvadhaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyirundhoru vinkal varuvadhaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai udaithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai udaithidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathavadaithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathavadaithidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pin naam poriduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pin naam poriduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mozhi madha ina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhi madha ina"/>
</div>
<div class="lyrico-lyrics-wrapper">Baedham irukkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baedham irukkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Namai azhithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namai azhithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam vizhuvadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam vizhuvadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhai thaduthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai thaduthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai mudithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai mudithidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnae naam azhivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnae naam azhivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nurai Siridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nurai Siridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai Peridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Peridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadam Siridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadam Siridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idam peridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam peridhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhum Manadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhum Manadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhum Pozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhum Pozhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyar Siridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyar Siridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir peridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir peridhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un sevigalil sottu chottena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sevigalil sottu chottena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala thuliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala thuliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivvadhirvugal undhal irudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvadhirvugal undhal irudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum oliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum oliyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Da tickin is the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da tickin is the"/>
</div>
<div class="lyrico-lyrics-wrapper">Trick trick tricklin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trick trick tricklin"/>
</div>
<div class="lyrico-lyrics-wrapper">Droplets of the time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Droplets of the time"/>
</div>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<div class="lyrico-lyrics-wrapper">Da beatin’ is the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da beatin’ is the"/>
</div>
<div class="lyrico-lyrics-wrapper">Dab dab doublin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dab dab doublin"/>
</div>
<div class="lyrico-lyrics-wrapper">Drummin od thy mind
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drummin od thy mind"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Abaaya roobangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abaaya roobangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Puviyai thaakinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puviyai thaakinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Atralin ellaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atralin ellaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaithu meeruvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaithu meeruvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjaadha ullangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaadha ullangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga koodinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga koodinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana peedhiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana peedhiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaamadham aakuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaamadham aakuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evvathin nudhigaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvathin nudhigaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaatrum yedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaatrum yedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvaaru ingae pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvaaru ingae pizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pavvathin thulimondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavvathin thulimondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaganjeyyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaganjeyyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avvaanil engae mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avvaanil engae mazhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhi siridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi siridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi peridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi peridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai siridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai siridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai peridhu yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai peridhu yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vali siridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali siridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi peridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi peridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porul siridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porul siridhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arul peridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arul peridhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un sevigalil sottu chottena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sevigalil sottu chottena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala thuliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala thuliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivvadhirvugal undhal irudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvadhirvugal undhal irudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum oliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum oliyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un sevigalil sottu chottena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sevigalil sottu chottena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala thuliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala thuliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivvadhirvugal undhal irudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvadhirvugal undhal irudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum oliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum oliyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadigaaram naadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigaaram naadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudi thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudi thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi idipol olli vedikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi idipol olli vedikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<div class="lyrico-lyrics-wrapper">Avasarathil kadhai mudipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasarathil kadhai mudipom"/>
</div>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bcoz when we do it you know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bcoz when we do it you know"/>
</div>
<div class="lyrico-lyrics-wrapper">We do it quick and slick
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We do it quick and slick"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam udal payirchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam udal payirchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiranin valarchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiranin valarchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithanin buddhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithanin buddhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Can conquer gravity
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can conquer gravity"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammul vazhum shakthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammul vazhum shakthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Modern technology
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modern technology"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattumae thannambikkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattumae thannambikkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam vetrikaana sangathee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam vetrikaana sangathee"/>
</div>
<div class="lyrico-lyrics-wrapper">Tik tik tik
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tik tik"/>
</div>
</pre>
