---
title: "vaanam song lyrics"
album: "Pa Paandi"
artist: "Sean Roldan"
lyricist: "Selvaraghavan"
director: "Dhanush"
path: "/albums/pa-paandi-lyrics"
song: "Vaanam"
image: ../../images/albumart/pa-paandi.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eTmBkDJxE0g"
type: "happy"
singers:
  -	Ananthu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaanam parandhu parkka yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam parandhu parkka yengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookal siragai neetudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookal siragai neetudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum nathiyinilae oodam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum nathiyinilae oodam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oiynthu karaiyai theduthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oiynthu karaiyai theduthaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endrum ivanum kuzhandhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum ivanum kuzhandhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai innum malalaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai innum malalaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siripil idhayam pongumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siripil idhayam pongumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karunai sindhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karunai sindhuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katru malaiyil modhalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katru malaiyil modhalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha kadalil seralam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha kadalil seralam"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kuzhandhai kootathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kuzhandhai kootathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanum thendralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanum thendralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannathi manna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannathi manna"/>
</div>
<div class="lyrico-lyrics-wrapper">Veerathi veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerathi veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Paandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyaadum singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadum singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyilla thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyilla thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Power paandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power paandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthiya vaanam parandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiya vaanam parandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parkka yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parkka yengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookal siragai neetudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookal siragai neetudhaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyae endrumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyae endrumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethaiyo thedum payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethaiyo thedum payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhiyil adaikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhiyil adaikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Peran pethi jananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peran pethi jananam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedinom odinom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedinom odinom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethanai kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethanai kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oiynthu poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oiynthu poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaivathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaivathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhandhai irukum koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhandhai irukum koodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu dhaan sugama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu dhaan sugama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavulin varamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulin varamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalin kaneer thalatumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalin kaneer thalatumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaiyum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaiyum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaramum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaramum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maganin magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maganin magalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee odi va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee odi va"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholil ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholil ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyil ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyil ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarndhal mattum puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarndhal mattum puriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam parandhu parkka yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam parandhu parkka yengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookal siragai neetudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookal siragai neetudhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum nathiyinilae oodam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum nathiyinilae oodam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oiynthu karaiyai theduthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oiynthu karaiyai theduthaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endrum ivanum kuzhandhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum ivanum kuzhandhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai innum malalaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai innum malalaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siripil idhayam pongumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siripil idhayam pongumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karunai sindhuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karunai sindhuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katru malaiyil modhalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katru malaiyil modhalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha kadalil seralam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha kadalil seralam"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kuzhandhai kootathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kuzhandhai kootathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanum thendralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanum thendralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannathi manna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannathi manna"/>
</div>
<div class="lyrico-lyrics-wrapper">Veerathi veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veerathi veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Paandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyaadum singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadum singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyilla thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyilla thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Power paandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power paandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthiya vaanam parandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiya vaanam parandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parkka yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parkka yengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookal siragai neetudhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookal siragai neetudhaam"/>
</div>
</pre>
