---
title: "sevandhu pochu nenju song lyrics"
album: "Chekka Chivantha Vaanam"
artist: "AR Rahman"
lyricist: "Vairamuthu"
director: "Mani Ratnam"
path: "/albums/chekka-chivantha-vaanam-lyrics"
song: "Sevandhu Pochu Nenju"
image: ../../images/albumart/chekka-chivantha-vaanam.jpg
date: 2018-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qnmMOKk_JpU"
type: "mass"
singers:
  - Arjun Chandy
  - D Sathyaprakash
  - Sunitha Sarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevandhu pochu nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevandhu pochu nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevandhu pochu nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevandhu pochu nenju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevandhu pochu nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevandhu pochu nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevandhu pochu nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevandhu pochu nenju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buththiya maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththiya maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhaikka sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhaikka sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththiya maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththiya maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavu vaangiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavu vaangiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevandhu pochu nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevandhu pochu nenju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappu thappa thappuga senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu thappa thappuga senju"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu arinjum thappuga senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu arinjum thappuga senju"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu thappa thappuga senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu thappa thappuga senju"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu arinjum thappuga senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu arinjum thappuga senju"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevandhu pochu nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevandhu pochu nenju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sol sol sol sol sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol sol sol sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol sol sol sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol sol sol sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevandhu pochu nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevandhu pochu nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol sol sol sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol sol sol sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada naana ada naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada naana ada naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada sonnadhum naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada sonnadhum naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Senjadhum naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjadhum naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana ada naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana ada naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada rendum onna vevveraala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada rendum onna vevveraala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana ada naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana ada naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada sonnadhu naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada sonnadhu naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Senjadhum naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjadhum naana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sol sol sol sol sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol sol sol sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol sol sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol sol sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theemai enbadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemai enbadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aamai pol nuzhaivadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamai pol nuzhaivadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththiyai kolvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththiyai kolvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai adhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey vanmuraiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vanmuraiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Valimaiyellaam jim jim jim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valimaiyellaam jim jim jim"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh vaanguvadhum kodupadhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh vaanguvadhum kodupadhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhom dhom dhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhom dhom dhom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kozhuththirukku kovam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhuththirukku kovam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paliththa varai laabam Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paliththa varai laabam Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">kollaigalae kolgai endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollaigalae kolgai endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Gum gum gum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gum gum gum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadaangu thada thada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaangu thada thada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadayadi dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadayadi dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Samooga sabaigalil sangeedham aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samooga sabaigalil sangeedham aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaadha edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaadha edathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettupatta thazhumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettupatta thazhumbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala pala aalukku vilaasam achu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala pala aalukku vilaasam achu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadaangu thada thada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaangu thada thada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadayadi dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadayadi dhaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Samooga sabaigalil sangeedham aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samooga sabaigalil sangeedham aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragadhimi thaga thithithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragadhimi thaga thithithom"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey seivom seivom seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey seivom seivom seivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevandhu pochu nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevandhu pochu nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevandhu pochu nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevandhu pochu nenju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevandhu pochu nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevandhu pochu nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevandhu pochu nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevandhu pochu nenju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buththiya maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththiya maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhaikka sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhaikka sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththiya maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththiya maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaavu vaangiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaavu vaangiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevandhu pochu nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevandhu pochu nenju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappu thappa thappuga senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu thappa thappuga senju"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu arinjum thappuga senju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu arinjum thappuga senju"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevandhu pochu nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevandhu pochu nenju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sol sol sol sol sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol sol sol sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol sol sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol sol sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada naana ada naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada naana ada naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada sonnadhum naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada sonnadhum naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Senjadhum naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjadhum naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana ada naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana ada naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada rendum onna vevveraala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada rendum onna vevveraala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana ada naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana ada naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada sonnadhu naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada sonnadhu naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Senjadhum naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjadhum naana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sol sol sol sol sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol sol sol sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol sol sol sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol sol sol sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Sol sol sol sol sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol sol sol sol sol"/>
</div>
</pre>
