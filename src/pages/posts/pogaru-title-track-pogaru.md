---
title: "pogaru title track song lyrics"
album: "Pogaru"
artist: "Chandan Shetty"
lyricist: "Bhaskarabhatla"
director: "Nandha kishor"
path: "/albums/pogaru-lyrics"
song: "Pogaru Totle Song"
image: ../../images/albumart/pogaru.jpg
date: 2021-02-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/lx0pIwDKPW0"
type: "title track"
singers:
 - Chandan Shetty
 - Aniruddha Sastry
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Notorious!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Notorious!"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalig Hawai Mettukondu Ududara Kattikondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalig Hawai Mettukondu Ududara Kattikondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadda Meese Bittukondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadda Meese Bittukondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Banda Nodale Anna Banda Nodale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banda Nodale Anna Banda Nodale"/>
</div>
<div class="lyrico-lyrics-wrapper">Jidda Jiddi Madikondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jidda Jiddi Madikondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mai Mele Dhoolu Vadrukondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mai Mele Dhoolu Vadrukondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Simha Nadkond Bandag Anna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simha Nadkond Bandag Anna"/>
</div>
<div class="lyrico-lyrics-wrapper">Barthavn Nodale Anna Banda Nodale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barthavn Nodale Anna Banda Nodale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogaru Annanige Pogaru Pogaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaru Annanige Pogaru Pogaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yagaru Anna Banda Yagaru Yagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yagaru Anna Banda Yagaru Yagaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaru Annanige Pogaru Pogaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaru Annanige Pogaru Pogaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yagaru Anna Banda Yagaru Yagru Kalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yagaru Anna Banda Yagaru Yagru Kalale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Notorious!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Notorious!"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkath Idre Bandu Nanna Munde Nilthiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkath Idre Bandu Nanna Munde Nilthiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nan Hodeyo Hodetha Ayyo Papa Hengo Tadithiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan Hodeyo Hodetha Ayyo Papa Hengo Tadithiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaru Anna Banda Yagaru Yagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaru Anna Banda Yagaru Yagaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ello Kaddu Koothu Nange Sketch-U Hakthiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ello Kaddu Koothu Nange Sketch-U Hakthiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Munde Matra Barle Beda Satte Hogthiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Munde Matra Barle Beda Satte Hogthiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sanghanadru Kattthini Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanghanadru Kattthini Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru Tumba Meritheeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Tumba Meritheeni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann Kolluttale Baltheeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann Kolluttale Baltheeni"/>
</div>
<div class="lyrico-lyrics-wrapper">Adu Nange Ninge Yakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adu Nange Ninge Yakale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishta Bandang Irthini Na Kiri Kiri Madthini Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishta Bandang Irthini Na Kiri Kiri Madthini Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Bekadange Balthini Na Nambirodu Anjaneyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bekadange Balthini Na Nambirodu Anjaneyane"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaru Annanige Pogaru Pogaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaru Annanige Pogaru Pogaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yagaru Anna Banda Yagaru Yagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yagaru Anna Banda Yagaru Yagaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaru Annanige Pogaru Pogaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaru Annanige Pogaru Pogaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yagaru Anna Banda Yagaru Yagru Kalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yagaru Anna Banda Yagaru Yagru Kalale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidye Buddi Odu Baraha Butte Butte Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidye Buddi Odu Baraha Butte Butte Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Onti Salagadange Belakand Butte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onti Salagadange Belakand Butte"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaru Anna Banda Yagaru Yagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaru Anna Banda Yagaru Yagaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Punya Gintha Jasthi Papa Made Butte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punya Gintha Jasthi Papa Made Butte"/>
</div>
<div class="lyrico-lyrics-wrapper">Papi Neecha Kroori Anno Patta Kattkond Butte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papi Neecha Kroori Anno Patta Kattkond Butte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kopa Bandre Katukane Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopa Bandre Katukane Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Daye Illada Dushtane Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daye Illada Dushtane Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Edrakondre Nanna Avnakkan Sigdakthini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edrakondre Nanna Avnakkan Sigdakthini"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasu Kotre Hodithini Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasu Kotre Hodithini Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Yare Bandru Badithini Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yare Bandru Badithini Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Oore Bandru Tadithini Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oore Bandru Tadithini Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal Ittmele Ella Kaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal Ittmele Ella Kaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogaru Annanige Pogaru Pogaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaru Annanige Pogaru Pogaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yagaru Anna Banda Yagaru Yagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yagaru Anna Banda Yagaru Yagaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaru Annanige Pogaru Pogaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaru Annanige Pogaru Pogaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yagaru Anna Banda Yagaru Yagru Kalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yagaru Anna Banda Yagaru Yagru Kalale"/>
</div>
</pre>
