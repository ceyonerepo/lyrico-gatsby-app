---
title: "jug jug jiyo re raja song lyrics"
album: "Kaagaz"
artist: "Rahul Jain"
lyricist: "Aseem Ahmed Abbasee - Kunaal Vermaa"
director: "Satish Kaushik"
path: "/albums/kaagaz-lyrics"
song: "Jug Jug Jiyo Re Raja"
image: ../../images/albumart/kaagaz.jpg
date: 2021-01-07
lang: hindi
youtubeLink: "https://www.youtube.com/embed/ch8_Qhd_ZX0"
type: "melody"
singers:
  - Rahul Jain
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rehne do kuch saans baaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rehne do kuch saans baaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai abhi to aas baaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai abhi to aas baaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Khwaab mein thodi jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwaab mein thodi jaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai abhi..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai abhi.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manzilo ki hai pyaas baaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manzilo ki hai pyaas baaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil ki hai ardaas baaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil ki hai ardaas baaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ik naya imtehan hai abhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ik naya imtehan hai abhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal gir ke uthh ja tu firse jutt jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal gir ke uthh ja tu firse jutt jaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tinke tinke jod kar bana pool
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tinke tinke jod kar bana pool"/>
</div>
<div class="lyrico-lyrics-wrapper">Uss paar jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uss paar jaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jug jug jiyo re raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jug jug jiyo re raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Jug jug jiyo re raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jug jug jiyo re raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Jug jug jiyo re bhaiya re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jug jug jiyo re bhaiya re"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhool ki chaadar mein lipti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhool ki chaadar mein lipti"/>
</div>
<div class="lyrico-lyrics-wrapper">Gal chuke kaagaz mein simati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gal chuke kaagaz mein simati"/>
</div>
<div class="lyrico-lyrics-wrapper">Thhi padi kabse yahaan zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thhi padi kabse yahaan zindagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jiske khaatir itna tarse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jiske khaatir itna tarse"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoondte thhe umar bhar se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoondte thhe umar bhar se"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa gayi haath woh haan zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gayi haath woh haan zindagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeeta tune khudko jeeta haari duniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeeta tune khudko jeeta haari duniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kehti hai tujhse ab ye saari duniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kehti hai tujhse ab ye saari duniya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jug jug jiyo re raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jug jug jiyo re raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Jug jug jiyo re raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jug jug jiyo re raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Jug jug jiyo re bhaiya re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jug jug jiyo re bhaiya re"/>
</div>
</pre>
