---
title: "nenji kizhinji pochu song lyrics"
album: "Sagaa"
artist: "Shabir"
lyricist: "Shabir"
director: "Murugesh"
path: "/albums/sagaa-lyrics"
song: "Nenji Kizhinji Pochu"
image: ../../images/albumart/sagaa.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/m3WAyAVt724"
type: "happy"
singers:
  - Hariharasudhan
  - Shabir
  - Sathyaprakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Heyhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyoorukulla Naan Veenagittenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyoorukulla Naan Veenagittenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jailukulla Naan Aalagittenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jailukulla Naan Aalagittenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Vanthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Vanthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkuvera Aanivera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkuvera Aanivera"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Sethachidum Therinjikkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Sethachidum Therinjikkada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Fuse Aagitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Fuse Aagitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Piece Aagitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Piece Aagitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Moolaikku Keezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Moolaikku Keezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Loosaagitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Loosaagitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenji Kizhinji Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenji Kizhinji Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Ellaam Thelinji Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ellaam Thelinji Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai Molaikkum Munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Molaikkum Munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Gnyanam Porandhu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Gnyanam Porandhu Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Maamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Maamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamamamamamamamama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamamamamamamamama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamamamamamamama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamamamamamamama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamamamamamamamama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamamamamamamamama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamamamamammaammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamamamamammaammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei Maamae Hei Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Maamae Hei Hei Hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maga Maga Manga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maga Maga Manga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Konjam Nondha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Konjam Nondha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatam Podu Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatam Podu Nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Theva Illa Vetti Bandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theva Illa Vetti Bandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vithaiyellaam Kathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithaiyellaam Kathen"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Vayasula Naan Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Vayasula Naan Ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Kathathellaam Vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Kathathellaam Vitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Evanum Illai Budhdhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Evanum Illai Budhdhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adadaa Mandelavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Mandelavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandhiyum Kathukitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandhiyum Kathukitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadamellam Jailukulla Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadamellam Jailukulla Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vida Vaa Innum Rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vida Vaa Innum Rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathuvangal Kathukkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathuvangal Kathukkada"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaadiyilla Socrates
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaadiyilla Socrates"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaana Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaana Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Oththu Oththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Oththu Oththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Ooram Thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Ooram Thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Engaloda Reelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Engaloda Reelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Podathae Scene-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Podathae Scene-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthu Kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthu Kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Jailu Kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Jailu Kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettukada Enga Paatukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettukada Enga Paatukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aattam Poduhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aattam Poduhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenji Kizhinji Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenji Kizhinji Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Ellaam Thelinji Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ellaam Thelinji Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai Molaikkum Munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Molaikkum Munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Gnyanam Porandhu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Gnyanam Porandhu Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu Kalanjathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu Kalanjathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Nenavum Tholanjathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Nenavum Tholanjathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal Pona Pokkil Naanum Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Pona Pokkil Naanum Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Engada Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Ennada Vidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Ennada Vidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Usurukku Natpu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Usurukku Natpu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Hei Hei Puuurrrr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei Hei Puuurrrr"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Hei Hei Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hei Hei Hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotta Kora Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Kora Vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vitta Kora Thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitta Kora Thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Aagiduma Setta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Aagiduma Setta"/>
</div>
<div class="lyrico-lyrics-wrapper">Illae Paayumada Jet-Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illae Paayumada Jet-Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ikkaraiyum Thotten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkaraiyum Thotten"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkaraiyum Thotten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkaraiyum Thotten"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu Ekkara Thaan Pachai Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Ekkara Thaan Pachai Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaiyum Sothai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaiyum Sothai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adadaa Othumaya Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Othumaya Ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Jaichidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Jaichidalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Machan Nambikayai Vei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Machan Nambikayai Vei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodu Da Uchatha Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodu Da Uchatha Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagamellam Paakatumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagamellam Paakatumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyam Nee Sonnathellam Sei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyam Nee Sonnathellam Sei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Oththu Oththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Oththu Oththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Ooram Thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Ooram Thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Engaloda Reelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Engaloda Reelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Podathae Scene-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Podathae Scene-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthu Kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthu Kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Jailu Kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Jailu Kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettukada Enga Paatukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettukada Enga Paatukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aattam Poduhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aattam Poduhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyoorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyoorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Veenagittenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Veenagittenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jailukulla Naan Aalagittenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jailukulla Naan Aalagittenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Vanthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Vanthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkuvera Aanivera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkuvera Aanivera"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Sethachidum Therinjikkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Sethachidum Therinjikkada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Fuse Aagitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Fuse Aagitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Piece Aagitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Piece Aagitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa Moolaikku Keezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa Moolaikku Keezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Loosaagitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Loosaagitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenji Kizhinji Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenji Kizhinji Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Ellam Thelinji Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ellam Thelinji Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai Molaikkum Munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Molaikkum Munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Gnyanam Porandhu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Gnyanam Porandhu Pochu"/>
</div>
</pre>
