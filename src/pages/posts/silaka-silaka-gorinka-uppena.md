---
title: "silaka silaka song lyrics"
album: "Uppena"
artist: "Devi Sri Prasad"
lyricist: "Devi Sri Prasad"
director: "Buchi Babu Sana"
path: "/albums/uppena-lyrics"
song: "Silaka Silaka Gorinka"
image: ../../images/albumart/uppena.jpg
date: 2021-02-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/NR7XxXB_uU4"
type: "melody"
singers:
  - Kailash Kher
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye silaka silaka gorinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye silaka silaka gorinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Egire egirevendhaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egire egirevendhaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaare leni nee uraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaare leni nee uraka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee dharikaa mari aa dharikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee dharikaa mari aa dharikaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye sinukaa sinukaa jaaraakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye sinukaa sinukaa jaaraakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Megham needhe kaadhinkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megham needhe kaadhinkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha rekkalu kattaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha rekkalu kattaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dhaaredho needhinkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee dhaaredho needhinkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selayerundho sudigaalundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selayerundho sudigaalundho"/>
</div>
<div class="lyrico-lyrics-wrapper">velle dhaarilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velle dhaarilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirujallundho jadivaanundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirujallundho jadivaanundho"/>
</div>
<div class="lyrico-lyrics-wrapper">ee malupulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee malupulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vichhe poolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vichhe poolu"/>
</div>
<div class="lyrico-lyrics-wrapper">guchhe mulluvaale vaakitlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guchhe mulluvaale vaakitlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Em dhaagundho emo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em dhaagundho emo "/>
</div>
<div class="lyrico-lyrics-wrapper">premane mungitlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premane mungitlo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo silaka silaka gorinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo silaka silaka gorinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelaakaasham needhinkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelaakaasham needhinkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekke vippu egurinkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekke vippu egurinkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne aapedhevarinkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne aapedhevarinkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye sinuka sinuka jaarinkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye sinuka sinuka jaarinkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaagu vanka needhinkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaagu vanka needhinkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupu solupu ledhinkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupu solupu ledhinkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhorikindhiraa dhaarinkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhorikindhiraa dhaarinkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selayeralle pongiporle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selayeralle pongiporle "/>
</div>
<div class="lyrico-lyrics-wrapper">preme santosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="preme santosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaanni attepettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaanni attepettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gundellone kalakaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gundellone kalakaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Polimerale leneleni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polimerale leneleni"/>
</div>
<div class="lyrico-lyrics-wrapper">Preme nee sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preme nee sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika ninne veedi ponepodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika ninne veedi ponepodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee vasantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee vasantham"/>
</div>
</pre>
