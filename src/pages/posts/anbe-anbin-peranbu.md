---
title: "anbe anbin song lyrics"
album: "Peranbu"
artist: "Yuvan Shankar Raja"
lyricist: "Sumathy Ram"
director: "Ram"
path: "/albums/peranbu-lyrics"
song: "Anbe Anbin"
image: ../../images/albumart/peranbu.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hEuyjXbW9D4"
type: "happy"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anbe anbin athanaium neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe anbin athanaium neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal kaanum karpanaium neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal kaanum karpanaium neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanathaium nilathaium nirapidavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanathaium nilathaium nirapidavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru paravai pothum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru paravai pothum pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal sumantha siru padagey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal sumantha siru padagey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe anbin athanium neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe anbin athanium neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal kaanum karpanaium neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal kaanum karpanaium neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuruvi neenthum nadhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruvi neenthum nadhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Meengal paragum vanamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meengal paragum vanamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sottum kulirey sudarum maayamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottum kulirey sudarum maayamey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eari neeril Un mugamtham vilugaiyiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eari neeril Un mugamtham vilugaiyiley"/>
</div>
<div class="lyrico-lyrics-wrapper">Eanthi kolla theavathaigal vanthidumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eanthi kolla theavathaigal vanthidumey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisaigal tholaitheney alaiyil mithntheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisaigal tholaitheney alaiyil mithntheney"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevai pola vanthai nintrai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevai pola vanthai nintrai neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe anbin athanium neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe anbin athanium neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal kaanum karpanaium neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal kaanum karpanaium neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ohh Kadavilin kaigalai kandathu unnidam madumthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohh Kadavilin kaigalai kandathu unnidam madumthan"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyir bhoomiyil piranthathu pidithathu innodi madumthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyir bhoomiyil piranthathu pidithathu innodi madumthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idium minnalum murinthathu intru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idium minnalum murinthathu intru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaga maram ontru ventrathu nintru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaga maram ontru ventrathu nintru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavin mozhiyil nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavin mozhiyil nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilathin mozhiyil naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilathin mozhiyil naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa pesa poogal pesuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa pesa poogal pesuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">ohh en magaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohh en magaley"/>
</div>
<div class="lyrico-lyrics-wrapper">ohh en magaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohh en magaley"/>
</div>
<div class="lyrico-lyrics-wrapper">ohh en magaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohh en magaley"/>
</div>
<div class="lyrico-lyrics-wrapper">ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohh"/>
</div>
</pre>
