---
title: 'siriki song lyrics'
album: 'Kaappaan'
artist: 'Harris Jayaraj'
lyricist: 'S Gnanakaravel'
director: 'K V Anand'
path: '/albums/kaappaan-song-lyrics'
song: 'Siriki'
image: ../../images/albumart/kaappaan.jpg
date: 2019-09-20
lang: tamil
singers: 
- Senthil Ganesh
- Ramani Ammal
youtubeLink: "https://www.youtube.com/embed/_pmcgBWf0n4"
type: 'folk'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Dumchaa hkum hkum hkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumchaa hkum hkum hkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumchaa hkum hkum hkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumchaa hkum hkum hkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dumchaa hkum hkum hkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumchaa hkum hkum hkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumchaa hkum hkum hkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumchaa hkum hkum hkum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Pachcha oodha manja vella
<input type="checkbox" class="lyrico-select-lyric-line" value="Pachcha oodha manja vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Machaan vandhanae machiniga thulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Machaan vandhanae machiniga thulla"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hoiii
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoiii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dumchaa hkum hkum hkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumchaa hkum hkum hkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumchaa hkum hkum hkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumchaa hkum hkum hkum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Pachcha oodha manja vella
<input type="checkbox" class="lyrico-select-lyric-line" value="Pachcha oodha manja vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Machaan vandhanae machiniga thulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Machaan vandhanae machiniga thulla"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hoiii
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoiii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dumchaa hkum hkum hkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumchaa hkum hkum hkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumchaa hkum hkum hkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumchaa hkum hkum hkum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Moochu keechu mutti chella
<input type="checkbox" class="lyrico-select-lyric-line" value="Moochu keechu mutti chella"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga machakaaran varaan paaru alla
<input type="checkbox" class="lyrico-select-lyric-line" value="Inga machakaaran varaan paaru alla"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hoii
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dumchaa hkum hkum hkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumchaa hkum hkum hkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumchaa hkum hkum hkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumchaa hkum hkum hkum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ivan ichu thantha pichikumae moola
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivan ichu thantha pichikumae moola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo manmadhanin kollu perapulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Ayyo manmadhanin kollu perapulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaama manmadhanin kollu perapulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaama manmadhanin kollu perapulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yamma manmadhanin kollu perapulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Yamma manmadhanin kollu perapulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Raakaayi mookaayi
<input type="checkbox" class="lyrico-select-lyric-line" value="Raakaayi mookaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakaayi raamaayi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaakaayi raamaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Engadi poninga …soomaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Engadi poninga …soomaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sirikki seenikatti
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirikki seenikatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinungi singaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Sinungi singaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi kuthi nikkum munazhagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi kuthi nikkum munazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttudhu keeri
<input type="checkbox" class="lyrico-select-lyric-line" value="Muttudhu keeri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Minukki meenukutti
<input type="checkbox" class="lyrico-select-lyric-line" value="Minukki meenukutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalukki oyaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalukki oyaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava oththa jadai pinazhagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava oththa jadai pinazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethana kodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethana kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vanaja karrupanoda
<input type="checkbox" class="lyrico-select-lyric-line" value="Vanaja karrupanoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora menjaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oora menjaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana raajavoda senthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Aana raajavoda senthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vella pulla pethaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vella pulla pethaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kirija kovilathaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kirija kovilathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi vanthaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Suthi vanthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana jodi thedi aalilaama
<input type="checkbox" class="lyrico-select-lyric-line" value="Aana jodi thedi aalilaama"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanju ninaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="kaanju ninaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum roattu mela
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinamum roattu mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Route viddum pokku nallaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Route viddum pokku nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala vaala otta narukavandha
<input type="checkbox" class="lyrico-select-lyric-line" value="Avala vaala otta narukavandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathir naandhaanlae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kathir naandhaanlae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sirikki…yelaa….sirikki…
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirikki…yelaa….sirikki…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sirikki seenikatti
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirikki seenikatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinungi singaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Sinungi singaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi kuthi nikkum munazhagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi kuthi nikkum munazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttudhu keeri
<input type="checkbox" class="lyrico-select-lyric-line" value="Muttudhu keeri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Minukki meenukutti
<input type="checkbox" class="lyrico-select-lyric-line" value="Minukki meenukutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalukki oyaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalukki oyaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava oththa jadai pinazhagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava oththa jadai pinazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethana kodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethana kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kattampotta silukku satta
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattampotta silukku satta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti mela kaili katti
<input type="checkbox" class="lyrico-select-lyric-line" value="Mutti mela kaili katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetta veli puyala pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetta veli puyala pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi varra naaadodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Suthi varra naaadodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sappipotta panampazhama
<input type="checkbox" class="lyrico-select-lyric-line" value="Sappipotta panampazhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nattukkunikkum koramudi
<input type="checkbox" class="lyrico-select-lyric-line" value="Nattukkunikkum koramudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachchapulla kanji kudikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Pachchapulla kanji kudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaathan ippo poochandi
<input type="checkbox" class="lyrico-select-lyric-line" value="Naaathan ippo poochandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vepangutchi marandha appatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Vepangutchi marandha appatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un bokkaiyila pal molaikka vekkatta
<input type="checkbox" class="lyrico-select-lyric-line" value="Un bokkaiyila pal molaikka vekkatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Keppa cholam nellae pochaetha
<input type="checkbox" class="lyrico-select-lyric-line" value="Keppa cholam nellae pochaetha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pizza thinnu pollangkaati ennatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee pizza thinnu pollangkaati ennatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Jinthaak jinthaak jinthaak jinthaak
<input type="checkbox" class="lyrico-select-lyric-line" value="Jinthaak jinthaak jinthaak jinthaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinthaak jinthaak jinthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Jinthaak jinthaak jinthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinthaak jinthaak jinthaak jinthaak
<input type="checkbox" class="lyrico-select-lyric-line" value="Jinthaak jinthaak jinthaak jinthaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinthaak jinthaak jinthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Jinthaak jinthaak jinthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sirikki seenikatti
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirikki seenikatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinungi singaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Sinungi singaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi kuthi nikkum munazhagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi kuthi nikkum munazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttudhu keeri
<input type="checkbox" class="lyrico-select-lyric-line" value="Muttudhu keeri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Minukki meenukutti
<input type="checkbox" class="lyrico-select-lyric-line" value="Minukki meenukutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalukki oyaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalukki oyaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava oththa jadai pinazhagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava oththa jadai pinazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethana kodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethana kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kammakara kalathumedu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kammakara kalathumedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nellu velaiyum pachcha kaadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nellu velaiyum pachcha kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattadama molaichi ninna
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattadama molaichi ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla thinga pora nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalla thinga pora nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Othakodam thannipidikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Othakodam thannipidikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Malluku nikkum pombalainga
<input type="checkbox" class="lyrico-select-lyric-line" value="Malluku nikkum pombalainga"/>
</div>
<div class="lyrico-lyrics-wrapper">Othumaiya pongi ezhuntha
<input type="checkbox" class="lyrico-select-lyric-line" value="Othumaiya pongi ezhuntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi varum cauvery
<input type="checkbox" class="lyrico-select-lyric-line" value="Odi varum cauvery"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaasu pavusu thooki kadaasu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaasu pavusu thooki kadaasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee seththaalum vedipainga pattasu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee seththaalum vedipainga pattasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu mavusu ponaa varaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Maasu mavusu ponaa varaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada…avucha mutta omelettaaga maaraadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada…avucha mutta omelettaaga maaraadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Jinthaak jinthaak jinthaak jinthaak
<input type="checkbox" class="lyrico-select-lyric-line" value="Jinthaak jinthaak jinthaak jinthaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinthaak jinthaak jinthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Jinthaak jinthaak jinthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinthaak jinthaak jinthaak jinthaak
<input type="checkbox" class="lyrico-select-lyric-line" value="Jinthaak jinthaak jinthaak jinthaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinthaak jinthaak jinthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Jinthaak jinthaak jinthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sirikki seenikatti
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirikki seenikatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinungi singaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Sinungi singaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi kuthi nikkum munazhagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi kuthi nikkum munazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttudhu keeri
<input type="checkbox" class="lyrico-select-lyric-line" value="Muttudhu keeri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Muttudhu keeri
<input type="checkbox" class="lyrico-select-lyric-line" value="Muttudhu keeri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Minukki meenukutti
<input type="checkbox" class="lyrico-select-lyric-line" value="Minukki meenukutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalukki oyaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalukki oyaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava oththa jadai pinazhagu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava oththa jadai pinazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethana kodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethana kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heyy vanaja karrupanoda
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy vanaja karrupanoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora menjaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oora menjaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana raajavoda senthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Aana raajavoda senthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vella pulla pethaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vella pulla pethaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kirija kovilathaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kirija kovilathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi vanthaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Suthi vanthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana jodi thedi aalilaama
<input type="checkbox" class="lyrico-select-lyric-line" value="Aana jodi thedi aalilaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanju ninaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaanju ninaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum roattu mela
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinamum roattu mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Route viddum pokku nallaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Route viddum pokku nallaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avala vaala otta narukavandha
<input type="checkbox" class="lyrico-select-lyric-line" value="Avala vaala otta narukavandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathir naandhaanlae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kathir naandhaanlae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kathiru…. ka ka kathiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Kathiru…. ka ka kathiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
         <div class="lyrico-lyrics-wrapper">Kathiru…. ka ka kathiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Kathiru…. ka ka kathiru"/>
</div>
</pre>