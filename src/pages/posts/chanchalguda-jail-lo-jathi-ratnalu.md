---
title: "chanchalguda jail lo song lyrics"
album: "Jathi Ratnalu"
artist: "Radhan"
lyricist: "Kasarla Shyam"
director: "Anudeep KV"
path: "/albums/jathi-ratnalu-lyrics"
song: "Chanchalguda jail lo"
image: ../../images/albumart/jathi-ratnalu.jpg
date: 2021-03-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/JizQbPx31kU"
type: "love"
singers:
  - Yogi Sekar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chanchal guda jail lo chilakalayyi chikkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chanchal guda jail lo chilakalayyi chikkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Palaka meedha kekkindhayyo number
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palaka meedha kekkindhayyo number"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalandhukonu rekkalu vippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalandhukonu rekkalu vippi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thurrumantu yegiraru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurrumantu yegiraru"/>
</div>
<div class="lyrico-lyrics-wrapper">Villa gaacharame gunji thanthe bokkal paddaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villa gaacharame gunji thanthe bokkal paddaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye nimishaniki emi jaruguno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye nimishaniki emi jaruguno"/>
</div>
<div class="lyrico-lyrics-wrapper">Maataku arthame telisochhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maataku arthame telisochhene"/>
</div>
<div class="lyrico-lyrics-wrapper">Venna thinna noti tho mannu bukkisthire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venna thinna noti tho mannu bukkisthire"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemi kannunnadho endho raatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemi kannunnadho endho raatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rangula paalapongulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rangula paalapongulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Masthu masthu kalalu kante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masthu masthu kalalu kante"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitti gundeke chepppakundane aasha puttene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti gundeke chepppakundane aasha puttene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neellallo challa bathike chepane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neellallo challa bathike chepane"/>
</div>
<div class="lyrico-lyrics-wrapper">Odduke esthire yama domapadithire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odduke esthire yama domapadithire"/>
</div>
<div class="lyrico-lyrics-wrapper">Gattununna pulla theesi addu pettanoniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gattununna pulla theesi addu pettanoniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Netthi meedha banda petti urikisthundre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netthi meedha banda petti urikisthundre"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey maaraje theerey unnodini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey maaraje theerey unnodini"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye randhi lenodni bathukaagam chesinre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye randhi lenodni bathukaagam chesinre"/>
</div>
<div class="lyrico-lyrics-wrapper">O bondhala thosinre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O bondhala thosinre"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey beta meeru edhi pattina adhi sarvanashanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey beta meeru edhi pattina adhi sarvanashanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi dhaiva shasanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi dhaiva shasanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Intlo unna anninallu viluva telvaledhuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intlo unna anninallu viluva telvaledhuro"/>
</div>
<div class="lyrico-lyrics-wrapper">Karma Kaalipoyinanka kathe maarero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karma Kaalipoyinanka kathe maarero"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaidhi battalu rowdy gangulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaidhi battalu rowdy gangulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalugu godale nee dosthulaayero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalugu godale nee dosthulaayero"/>
</div>
<div class="lyrico-lyrics-wrapper">Avva paayero buvva paayero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avva paayero buvva paayero"/>
</div>
<div class="lyrico-lyrics-wrapper">Pori thoti love-ey paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pori thoti love-ey paaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhugunna mee life vaddhami vadili paayero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhugunna mee life vaddhami vadili paayero"/>
</div>
</pre>
