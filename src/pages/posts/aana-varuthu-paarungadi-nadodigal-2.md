---
title: "aana varuthu paarungadi song lyrics"
album: "Nadodigal 2"
artist: "Justin Prabhakaran"
lyricist: "Yugabharathi"
director: "Samuthrakani"
path: "/albums/nadodigal-2-lyrics"
song: "Aana Varuthu Paarungadi"
image: ../../images/albumart/nadodigal-2.jpg
date: 2020-01-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/i4rApXVO4V8"
type: "Celebration"
singers:
  - Reema
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aana Varradha Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Varradha Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Asanju Varradha Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Asanju Varradha Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Pola Enga Kula Maga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Pola Enga Kula Maga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeepula Varuvaa Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeepula Varuvaa Parungadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudhira Varradha Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhira Varradha Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhira Kudhichu Varradha Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhira Kudhichu Varradha Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhira Pola Namma Kula Maga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhira Pola Namma Kula Maga"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullettil Varuvaa Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullettil Varuvaa Parungadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aana Varradha Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Varradha Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Asanju Varradha Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Asanju Varradha Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Pola Enga Kula Maga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Pola Enga Kula Maga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeepula Varuvaa Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeepula Varuvaa Parungadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emmaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emmaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Pethavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Pethavale"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vayithil Naan Porandhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vayithil Naan Porandhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuku Enna Othikki Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuku Enna Othikki Vecha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emmaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeril Moozhgadichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeril Moozhgadichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalayila Neruppu Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalayila Neruppu Vecha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pethedutha Rathinatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethedutha Rathinatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Moolayila Modanga Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolayila Modanga Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthankora Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthankora Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vedhanaiyil Kalanga Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vedhanaiyil Kalanga Vecha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenga Ethirthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Ethirthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichaalum Aruthaalum Podhaachalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichaalum Aruthaalum Podhaachalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali Thaangum Idhaiyathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Thaangum Idhaiyathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uru Maari Varuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uru Maari Varuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aana Varradha Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Varradha Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Asanju Varradha Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Asanju Varradha Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Pola Enga Kula Maga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Pola Enga Kula Maga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeepula Varuvaa Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeepula Varuvaa Parungadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudhira Varradha Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhira Varradha Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhira Kudhichu Varradha Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhira Kudhichu Varradha Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhira Pola Namma Kula Maga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhira Pola Namma Kula Maga"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullettil Varuvaa Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullettil Varuvaa Parungadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aana Varradha Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Varradha Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Asanju Varradha Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Asanju Varradha Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Pola Enga Kula Maga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Pola Enga Kula Maga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeepula Varuvaa Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeepula Varuvaa Parungadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudhira Varradha Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhira Varradha Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhira Kudhichu Varradha Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhira Kudhichu Varradha Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhira Pola Namma Kula Maga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhira Pola Namma Kula Maga"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullettil Varuvaa Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullettil Varuvaa Parungadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudhira Pola Namma Kula Maga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhira Pola Namma Kula Maga"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullettil Varuvaa Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullettil Varuvaa Parungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhira Pola Namma Kula Maga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhira Pola Namma Kula Maga"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullettil Varuvaa Parungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullettil Varuvaa Parungadi"/>
</div>
</pre>
