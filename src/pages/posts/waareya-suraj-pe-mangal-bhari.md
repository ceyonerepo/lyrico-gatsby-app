---
title: "waareya song lyrics"
album: "Suraj pe mangal bhari"
artist: "Javed - Mohsin"
lyricist: "Kunaal Vermaa"
director: "Abhishek Sharma"
path: "/albums/suraj-pe-mangal-bhari-lyrics"
song: "Waareya"
image: ../../images/albumart/suraj-pe-mangal-bhari.jpg
date: 2020-11-15
lang: hindi
youtubeLink: "https://www.youtube.com/embed/YaSVWTkBaeA"
type: "love"
singers:
  - Javed - Mohsin
  - Vibhor Parashar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Din raat ankhiyan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Din raat ankhiyan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas tu hi dissda ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas tu hi dissda ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu na dikhe to jee ni lagda mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu na dikhe to jee ni lagda mera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil di jagah tu hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil di jagah tu hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pal pal dhadakta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pal pal dhadakta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhull ke vi yaara tu ni hona juda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhull ke vi yaara tu ni hona juda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haqdaar tera hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haqdaar tera hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Har baar tera hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har baar tera hoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab loon chahe janam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab loon chahe janam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chahe main sau dafa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chahe main sau dafa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Duniya saari chhad ke challeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duniya saari chhad ke challeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Teriyan raahwan teri galiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teriyan raahwan teri galiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sadke sab main waareya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sadke sab main waareya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jis din se tu saanu mileya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jis din se tu saanu mileya"/>
</div>
<div class="lyrico-lyrics-wrapper">Main tere pichhe pichhe turreya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main tere pichhe pichhe turreya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sadke sab main waareya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sadke sab main waareya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O.. Ho..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O.. Ho.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mujh se hazaron hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujh se hazaron hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujh sa nahi koyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujh sa nahi koyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Main hoon sitaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main hoon sitaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu chann ae mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu chann ae mera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mainu khuda teri ankhan ch dissda ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu khuda teri ankhan ch dissda ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ki ve karaan tera main shukriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki ve karaan tera main shukriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jis di nahi raatan hai woh savera tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jis di nahi raatan hai woh savera tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Badle kade vi na mausam tera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badle kade vi na mausam tera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khushiyan teri main gham rakheya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khushiyan teri main gham rakheya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mar jaana main je tu russeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mar jaana main je tu russeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sadke sab main waareya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sadke sab main waareya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rishte saare naate chhadeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rishte saare naate chhadeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sab hai laare tu hi sacheya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab hai laare tu hi sacheya"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sadke sab main waareya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sadke sab main waareya"/>
</div>
</pre>
