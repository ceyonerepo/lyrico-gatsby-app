---
title: "bitch i'm back song lyrics"
album: "Moosetape"
artist: "The Kidd"
lyricist: "Sidhu Moose Wala"
director: "Sidhu Moose Wala"
path: "/albums/moosetape-lyrics"
song: "Bitch I'm Back"
image: ../../images/albumart/moosetape.jpg
date: 2021-05-15
lang: punjabi
youtubeLink: "https://www.youtube.com/embed/W5NgXKe4SJk"
type: "Mass"
singers:
  - Sidhu Moose Wala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oye aa gaya, oye aa gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye aa gaya, oye aa gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Oye aa gaya, oye aa gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye aa gaya, oye aa gaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aan sidhu moose wala!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aan sidhu moose wala!"/>
</div>
<div class="lyrico-lyrics-wrapper">Moosetape!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moosetape!"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye yo the kidd!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye yo the kidd!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho kacha maas paad ke jawaan hunda jo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kacha maas paad ke jawaan hunda jo"/>
</div>
<div class="lyrico-lyrics-wrapper">Beehdan di nahiyon haryaai jatt da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beehdan di nahiyon haryaai jatt da"/>
</div>
<div class="lyrico-lyrics-wrapper">Launde je banayi kayi team'an phirde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Launde je banayi kayi team'an phirde"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kalla kehda chup dekho putt jatt da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kalla kehda chup dekho putt jatt da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pith ki ghumayi ni main thode time layin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pith ki ghumayi ni main thode time layin"/>
</div>
<div class="lyrico-lyrics-wrapper">Time mera gaya gallan down lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time mera gaya gallan down lag paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dass ohna nu prauhauna thodda pher aa gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dass ohna nu prauhauna thodda pher aa gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jehde meri gairhaziri ch gaun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jehde meri gairhaziri ch gaun lag paye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohna nu prauhauna thodda pher je aa gaya ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohna nu prauhauna thodda pher je aa gaya ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Jehde meri gairhaziri ch chhaun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jehde meri gairhaziri ch chhaun lag paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maar dyange chakk dyange gallan jo kehne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maar dyange chakk dyange gallan jo kehne"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadhu hi adayi aivein mitte jaande ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadhu hi adayi aivein mitte jaande ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho mere wangu chaunde ne aagaaz karna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mere wangu chaunde ne aagaaz karna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho ke insecure jaane sitti jaande ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ke insecure jaane sitti jaande ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho chhade hi trend sathon copy karke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho chhade hi trend sathon copy karke"/>
</div>
<div class="lyrico-lyrics-wrapper">Saade utte hype jehi banaun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saade utte hype jehi banaun lag paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dass ohna nu prauhauna thodda pher aa gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dass ohna nu prauhauna thodda pher aa gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jehde meri gairhaziri ch gaun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jehde meri gairhaziri ch gaun lag paye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohna nu prauhauna thodda pher aa gaya ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohna nu prauhauna thodda pher aa gaya ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Jehde meri gairhaziri ch chhaun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jehde meri gairhaziri ch chhaun lag paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neele wali filmi na life jatt di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neele wali filmi na life jatt di"/>
</div>
<div class="lyrico-lyrics-wrapper">Hor task bathere likhe laune painde ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hor task bathere likhe laune painde ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalli rab ne tarakki nahiyon ditti jatt nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalli rab ne tarakki nahiyon ditti jatt nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bacche vi ditte pachhtaune painde ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bacche vi ditte pachhtaune painde ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khair te khadaiyan ki main band kareya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khair te khadaiyan ki main band kareya"/>
</div>
<div class="lyrico-lyrics-wrapper">Game vichon out tarsaun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Game vichon out tarsaun lag paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dass ohna nu prauhauna thodda pher aa gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dass ohna nu prauhauna thodda pher aa gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jehde meri gairhaziri ch gaun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jehde meri gairhaziri ch gaun lag paye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohna nu prauhauna thodda pher je aa gaya ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohna nu prauhauna thodda pher je aa gaya ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Jehde meri gairhaziri ch chhaun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jehde meri gairhaziri ch chhaun lag paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh tere te jawani navi navi aayi ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh tere te jawani navi navi aayi ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Beizzat ki na moh ni hunda mere jeha nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beizzat ki na moh ni hunda mere jeha nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinna nu tu puchhe jihde oh ni bakhshe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinna nu tu puchhe jihde oh ni bakhshe"/>
</div>
<div class="lyrico-lyrics-wrapper">Keele hi baitha hun woh tere jeha nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keele hi baitha hun woh tere jeha nu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na narkan nu naal jaande layak mittra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na narkan nu naal jaande layak mittra"/>
</div>
<div class="lyrico-lyrics-wrapper">Kahton sudhi payi maut aa ghasaun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kahton sudhi payi maut aa ghasaun lag paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dass ohna nu prauhauna thodda pher aa gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dass ohna nu prauhauna thodda pher aa gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jehde meri gairhaziri ch gaun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jehde meri gairhaziri ch gaun lag paye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohna nu prauhauna thodda pher aa gaya ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohna nu prauhauna thodda pher aa gaya ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Jehde meri gairhaziri ch chhaun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jehde meri gairhaziri ch chhaun lag paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh kalam na likhe itihas hun tanyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kalam na likhe itihas hun tanyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saukha nahiyon kamm jo har ek kar dun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saukha nahiyon kamm jo har ek kar dun"/>
</div>
<div class="lyrico-lyrics-wrapper">Talent place te depend kare na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talent place te depend kare na"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho pind baitha baitha duniya nu shake kar dun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho pind baitha baitha duniya nu shake kar dun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh moosetape sun thirty'an track'an di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh moosetape sun thirty'an track'an di"/>
</div>
<div class="lyrico-lyrics-wrapper">Vairi tattan vich ungal chabaun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vairi tattan vich ungal chabaun lag paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dass ohna nu prauhauna thodda pher aa gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dass ohna nu prauhauna thodda pher aa gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni jehde meri gairhaziri ch gaun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni jehde meri gairhaziri ch gaun lag paye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohna nu prauhauna thodda pher aa gaya ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohna nu prauhauna thodda pher aa gaya ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Jehde meri gairhaziri ch chhaun lag paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jehde meri gairhaziri ch chhaun lag paye"/>
</div>
</pre>
