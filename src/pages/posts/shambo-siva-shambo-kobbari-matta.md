---
title: "shambo siva shambo song lyrics"
album: "Kobbari Matta"
artist: "Kamran"
lyricist: "Kittu Vissapragada"
director: "Rupak Ronaldson"
path: "/albums/kobbari-matta-lyrics"
song: "Shambo Siva Shambo"
image: ../../images/albumart/kobbari-matta.jpg
date: 2019-08-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/SbyVCPoapRs"
type: "mass"
singers:
  - Saaketh Kommandoori
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakashamlo Meghalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashamlo Meghalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaveshamtho Yekam Kaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaveshamtho Yekam Kaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiri Shabdham Yedalo Shankam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiri Shabdham Yedalo Shankam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poorinche Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poorinche Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nitturpullo Nirvedanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nitturpullo Nirvedanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadagaalullo Sandesanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadagaalullo Sandesanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadagallayye Aahwananni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadagallayye Aahwananni"/>
</div>
<div class="lyrico-lyrics-wrapper">Pampinche Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pampinche Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadame Aade Naadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadame Aade Naadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Damarukame Kaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damarukame Kaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Praname Chese Gaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praname Chese Gaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangaku Gudi Kaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangaku Gudi Kaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonthulo Daaham Deham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonthulo Daaham Deham"/>
</div>
<div class="lyrico-lyrics-wrapper">Dahinche Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dahinche Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Natarajuga Raajuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natarajuga Raajuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruga Chusina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruga Chusina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigika Nelaku Nadi Kaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigika Nelaku Nadi Kaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimisham Vishamaithe Galame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimisham Vishamaithe Galame"/>
</div>
<div class="lyrico-lyrics-wrapper">Siddam Ani Cheppe Siva Thatvame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siddam Ani Cheppe Siva Thatvame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesham Veyinche Naatyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham Veyinche Naatyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyisthe Megham Pulakinchada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyisthe Megham Pulakinchada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorike Baadhe Mose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorike Baadhe Mose"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaje Kaadantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaje Kaadantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorikai Yuddam Chese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorikai Yuddam Chese"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakthi Undantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakthi Undantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandina Homam Lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandina Homam Lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Samidhe Tanantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samidhe Tanantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Dehame Mabbuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Dehame Mabbuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Swedame Vaanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swedame Vaanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonthulu Neruga Thadipena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonthulu Neruga Thadipena"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Tyagam Thanavaipe Chusthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tyagam Thanavaipe Chusthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholisaari Paatam Chadiveyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholisaari Paatam Chadiveyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Thana Pere Vintu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Thana Pere Vintu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai Kalakaalam Keerthinchada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai Kalakaalam Keerthinchada"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadhalo Mande Gundeku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadhalo Mande Gundeku "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenunnanantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenunnanantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonthulo Garalam Nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonthulo Garalam Nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Mosthale Antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosthale Antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Amrutham Panche Theeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amrutham Panche Theeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusthu Oorantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthu Oorantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Maatani Geethaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Maatani Geethaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Chethani Leelaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethani Leelaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Devuni Theeruga Kolichela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devuni Theeruga Kolichela"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Shiva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Shiva Sambo"/>
</div>
</pre>
