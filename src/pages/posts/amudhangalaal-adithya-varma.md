---
title: "amudhangalaal song lyrics"
album: "Adithya Varma"
artist: "Radhan"
lyricist: "Thamarai"
director: "Gireesaaya"
path: "/albums/adithya-varma-lyrics"
song: "Amudhangalaal"
image: ../../images/albumart/adithya-varma.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/o3DPOJfJRjg"
type: "love"
singers:
  - Priyanka
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Amudhangalaal Niraindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amudhangalaal Niraindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Idzhal Amudhangalaal Niraindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Idzhal Amudhangalaal Niraindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumudhangalaal Malarndhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumudhangalaal Malarndhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Dhinam Kumudhangalaal Malarndhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Dhinam Kumudhangalaal Malarndhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi Irandum Pudhidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Irandum Pudhidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Imai Irandum Pudhidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Irandum Pudhidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugalo Inidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugalo Inidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaivathellam Amudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaivathellam Amudham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maragadha Raagangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maragadha Raagangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani Vizhi Deebangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani Vizhi Deebangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthiduma Naam Kolangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthiduma Naam Kolangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thananom Thananana Thiranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananom Thananana Thiranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Dheem Dhemmthanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Dheem Dhemmthanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananom Thananana Thiranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananom Thananana Thiranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Dheem Dhemmthanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Dheem Dhemmthanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veedhiyil Veesida Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedhiyil Veesida Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamandhi Poo Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamandhi Poo Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrilae Modhira Maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrilae Modhira Maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyaana Aavesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaana Aavesam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalangal Nerangal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal Nerangal Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadigara Kooppaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigara Kooppaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vangum Muthangal Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vangum Muthangal Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Peraasai Theeradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peraasai Theeradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valipadhu Pol Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valipadhu Pol Thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inipadhu Thaan Yenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inipadhu Thaan Yenum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavulagil Illa Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavulagil Illa Sugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amudhangalaal Niraindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amudhangalaal Niraindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Idzhal Amudhangalaal Niraindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Idzhal Amudhangalaal Niraindhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvai Pogira Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Pogira Thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Indri Yaar Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Indri Yaar Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavai Oon Uyir Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavai Oon Uyir Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Ondraagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Ondraagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam Paarkirathendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Paarkirathendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Konji Pesaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Konji Pesaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaikkum Verkkirathendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaikkum Verkkirathendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani Kaatru Veesaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Kaatru Veesaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Karai Neeyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Karai Neeyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Karai Naanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Karai Naanaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Karai Purandae Aaraaginom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Purandae Aaraaginom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amudhangalaal Niraindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amudhangalaal Niraindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Idzhal Amudhangalaal Niraindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Idzhal Amudhangalaal Niraindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumudhangalaal Malarndhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumudhangalaal Malarndhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Dhinam Kumudhangalaal Malarndhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Dhinam Kumudhangalaal Malarndhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi Irandum Pudhidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Irandum Pudhidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Imai Irandum Pudhidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Irandum Pudhidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugalo Inidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugalo Inidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaivathellam Amudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaivathellam Amudham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maragadha Raagangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maragadha Raagangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani Vizhi Deebangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani Vizhi Deebangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthiduma Naam Kolangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthiduma Naam Kolangal"/>
</div>
</pre>
