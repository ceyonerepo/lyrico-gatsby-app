---
title: "ival thaana song lyrics"
album: "Veeram"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Siva"
path: "/albums/veeram-lyrics"
song: "ival thaana"
image: ../../images/albumart/veeram.jpg
date: 2014-01-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-fU8jy1D9-w"
type: "Love"
singers:
  - Sagar
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannum Kannum Moodi Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Moodi Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkam Karai Meeri Sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkam Karai Meeri Sella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkam Pakkam Yaarum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkam Pakkam Yaarum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaiyi Ennagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyi Ennagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Nejam Mutti Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Nejam Mutti Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Acham Mattum Vittu Thalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham Mattum Vittu Thalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solla Oru Vaartha Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla Oru Vaartha Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaiyo Ennagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyo Ennagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Moodi Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Moodi Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkam Karai Meeri Sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkam Karai Meeri Sella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkam Pakkam Yaarum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkam Pakkam Yaarum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaiyi Ennagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyi Ennagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Nejam Mutti Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Nejam Mutti Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Acham Mattum Vittu Thalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham Mattum Vittu Thalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solla Oru Vaartha Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla Oru Vaartha Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaiyo Ennagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyo Ennagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Vaanavillin Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Vaanavillin Paadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavil Meedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavil Meedhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennuruvil Vandhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennuruvil Vandhaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ival Dhaana Ival Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Dhaana Ival Dhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Minnal Ena Mothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Minnal Ena Mothi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manthirangal Othi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manthirangal Othi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanavai Vendrane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanavai Vendrane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Dhaana Ivan Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Dhaana Ivan Dhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potti Pottu En Vizhi Rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti Pottu En Vizhi Rendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Paarkka Munthi Sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarkka Munthi Sellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaigal Kooda Ethiril Nee Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigal Kooda Ethiril Nee Vandhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sumaigal Aaguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumaigal Aaguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ival Dhaana Ival Dhaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Dhaana Ival Dhaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Moodi Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Moodi Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkam Karai Meeri Sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkam Karai Meeri Sella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkam Pakkam Yaarum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkam Pakkam Yaarum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaiyi Ennagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyi Ennagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Nejam Mutti Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Nejam Mutti Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Acham Mattum Vittu Thalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham Mattum Vittu Thalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solla Oru Vaarthai Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla Oru Vaarthai Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaiyo Ennagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyo Ennagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinaa Vina Aayiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaa Vina Aayiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhan Vidai Ellam Un Vizhiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Vidai Ellam Un Vizhiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidai Vidai Mudivile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Vidai Mudivile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Vina Vanthaal Athu Kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Vina Vanthaal Athu Kadhale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniye Nee Veedhiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniye Nee Veedhiyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadanthaal Athu Perazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthaal Athu Perazhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Poo Kortha Noolaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Poo Kortha Noolaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theruve Angu Therigirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruve Angu Therigirathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaichal Vanthu Neechal Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaichal Vanthu Neechal Poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraai Maarinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraai Maarinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Dhaana Ivan Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Dhaana Ivan Dhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudai Kudai Yenthiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudai Kudai Yenthiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varum Mazhai Ondrai Ingu Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Mazhai Ondrai Ingu Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ival Illa Vaazhkaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Illa Vaazhkaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Pizhai Endru Naan Unargiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Pizhai Endru Naan Unargiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adada Un Kan Asaivum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Un Kan Asaivum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athiraa Un Punnagaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiraa Un Punnagaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udane En Uyir Pisaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udane En Uyir Pisaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalil Oar Per Asaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalil Oar Per Asaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril Pota Kolam Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Pota Kolam Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrai Marakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrai Marakkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ival Dhaana Oh Ival Dhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Dhaana Oh Ival Dhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Moodi Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Moodi Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkam Karai Meeri Sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkam Karai Meeri Sella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkam Pakkam Yarum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkam Pakkam Yarum Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaiyo Ennagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyo Ennagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Nejam Mutti Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Nejam Mutti Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Acham Mattum Vittu Thalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham Mattum Vittu Thalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solla Oru Vaartha Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla Oru Vaartha Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaiyo Ennagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyo Ennagumo"/>
</div>
</pre>
