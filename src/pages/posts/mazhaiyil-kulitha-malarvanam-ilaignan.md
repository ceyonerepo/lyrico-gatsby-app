---
title: "mazhaiyil kulitha malarvanam song lyrics"
album: "Ilaignan"
artist: "Vidyasagar"
lyricist: "Pa. Vijay"
director: "Suresh Krishna"
path: "/albums/ilaignan-lyrics"
song: "Mazhaiyil Kulitha Malarvanam"
image: ../../images/albumart/ilaignan.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/s4hdIuBhE1E"
type: "love"
singers:
  - Karthik
  - Anweshaa
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mazhaiyil Kulitha Malarvanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyil Kulitha Malarvanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Naera Kadal Niram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Naera Kadal Niram"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai Kavithai Vennilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai Kavithai Vennilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atrai Pookkalin Thiruvizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atrai Pookkalin Thiruvizha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Azhagu Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Azhagu Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuthaan Azhagu Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuthaan Azhagu Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Azhagu Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Azhagu Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuthaan Azhagu Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuthaan Azhagu Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyil Kulitha Malarvanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyil Kulitha Malarvanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Naera Kadal Niram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Naera Kadal Niram"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai Kavithai Vennilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai Kavithai Vennilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atrai Pookkalin Thiruvizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atrai Pookkalin Thiruvizha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Thaaraala Kaathalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Thaaraala Kaathalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Punalin Thaenootrum Puthu Chaaralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punalin Thaenootrum Puthu Chaaralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinthum Vidiyaatha Maargazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinthum Vidiyaatha Maargazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagengum Kaetkum Thamizh Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagengum Kaetkum Thamizh Mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Kanavo Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Kanavo Kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuthaan Kanavo Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuthaan Kanavo Kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Kanavo Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Kanavo Kanavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuthaan Kanavo Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuthaan Kanavo Kanavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eeram Ularaatha Ithazhchedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeram Ularaatha Ithazhchedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangi Munnerum Thazhuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangi Munnerum Thazhuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Otti Udaiyaatha Idaikkudam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti Udaiyaatha Idaikkudam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotti Uthiraatha Kaalthadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotti Uthiraatha Kaalthadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Ilamai Ilamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Ilamai Ilamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuthaan Ilamai Ilamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuthaan Ilamai Ilamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Ilamai Ilamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Ilamai Ilamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuthaan Ilamai Ilamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuthaan Ilamai Ilamai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moodi Vaikkaatha Ezhuthukoal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodi Vaikkaatha Ezhuthukoal"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu Illaatha Unarvu Poal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Illaatha Unarvu Poal"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyil Mithakkum Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil Mithakkum Kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthi Mudiyaatha Kadithangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthi Mudiyaatha Kadithangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Kaathal Kaathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Kaathal Kaathal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuthaan Kaathal Kaathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuthaan Kaathal Kaathal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Kaathal Kaathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Kaathal Kaathal"/>
</div>
</pre>
