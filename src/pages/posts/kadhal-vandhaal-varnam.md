---
title: "kadhal vandhaal song lyrics"
album: "Varnam"
artist: "Isaac Thomas Kottukapally"
lyricist: "Na. Muthukumar"
director: "SM Raju"
path: "/albums/varnam-lyrics"
song: "Kadhal Vandhaal"
image: ../../images/albumart/varnam.jpg
date: 2011-10-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/sP2gtpWTar4"
type: "love"
singers:
  - Karthik
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kaadhal Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">un Kannin Maniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un Kannin Maniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul Uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul Uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">therindhidudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therindhidudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadavul Vandhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul Vandhum"/>
</div>
<div class="lyrico-lyrics-wrapper">en Manadhin Ullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en Manadhin Ullae"/>
</div>
<div class="lyrico-lyrics-wrapper">peigal Koodi Aadidudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peigal Koodi Aadidudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaadhal Enbadhu Rahasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal Enbadhu Rahasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee Veliyae Sollaadhay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee Veliyae Sollaadhay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neruppai Thottadhum Kattida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppai Thottadhum Kattida"/>
</div>
<div class="lyrico-lyrics-wrapper">ada Endrum Mudiyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada Endrum Mudiyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaadhal Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">un Kannin Maniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un Kannin Maniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul Uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul Uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">therindhidudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therindhidudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">malai Meedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai Meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">varum Maegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varum Maegam"/>
</div>
<div class="lyrico-lyrics-wrapper">pallaththaakilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pallaththaakilum"/>
</div>
<div class="lyrico-lyrics-wrapper">pozhindhidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pozhindhidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu Polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu Polae"/>
</div>
<div class="lyrico-lyrics-wrapper">indha Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">endha Nenjilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endha Nenjilum"/>
</div>
<div class="lyrico-lyrics-wrapper">nuzhaindhidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuzhaindhidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaadhal Kaththi Irandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal Kaththi Irandum"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku Kaayam Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku Kaayam Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">andha Kaayam Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha Kaayam Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">kaala Pokkil Iniththuvidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaala Pokkil Iniththuvidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neervizhal Pol Nam Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neervizhal Pol Nam Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">kal Vizhundhaal Kalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kal Vizhundhaal Kalaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kal Vizhundhaal Kulam Maelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kal Vizhundhaal Kulam Maelae"/>
</div>
<div class="lyrico-lyrics-wrapper">valaiyalgal Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valaiyalgal Theriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">indha Kaadhal Kaayamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha Kaadhal Kaayamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaadhal Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">un Kannin Maniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un Kannin Maniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul Uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul Uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">therindhidudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therindhidudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadavul Vandhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul Vandhum"/>
</div>
<div class="lyrico-lyrics-wrapper">en Manadhin Ullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en Manadhin Ullae"/>
</div>
<div class="lyrico-lyrics-wrapper">peigal Koodi Aadidudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peigal Koodi Aadidudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">varalaatril Vanmuraiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varalaatril Vanmuraiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">thee Vaiththadhu Kaadhal Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee Vaiththadhu Kaadhal Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aanaalum Vazhi Dhorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanaalum Vazhi Dhorum"/>
</div>
<div class="lyrico-lyrics-wrapper">poovaiththadhu Kaadhal Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovaiththadhu Kaadhal Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">moodi Vaiththa Vaiththa Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moodi Vaiththa Vaiththa Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum Vaadidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum Vaadidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">malar Moodi Vaithum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malar Moodi Vaithum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam Veliyil Veesidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam Veliyil Veesidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">porundhaadha Indha Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porundhaadha Indha Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">ini Engae Poga Mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini Engae Poga Mudiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyar Vellam Vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyar Vellam Vandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">vara Naalil Vidiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vara Naalil Vidiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">indha Kaadhal Maayamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha Kaadhal Maayamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaadhal Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">un Kannin Maniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un Kannin Maniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul Uruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul Uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">therindhidudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therindhidudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadavul Vandhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul Vandhum"/>
</div>
<div class="lyrico-lyrics-wrapper">en Manadhin Ullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en Manadhin Ullae"/>
</div>
<div class="lyrico-lyrics-wrapper">peigal Koodi Aadidudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peigal Koodi Aadidudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaadhal Enbadhu Rahasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal Enbadhu Rahasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee Veliyae Sollaadhay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee Veliyae Sollaadhay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neruppai Thottadhum Kattida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppai Thottadhum Kattida"/>
</div>
<div class="lyrico-lyrics-wrapper">ada Endrum Mudiyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada Endrum Mudiyaadhae"/>
</div>
</pre>
