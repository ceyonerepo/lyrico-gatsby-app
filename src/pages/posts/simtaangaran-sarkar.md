---
title: "simtaangaran song lyrics"
album: "Sarkar"
artist: "AR Rahman"
lyricist: "Vivek"
director: "AR Murugadoss"
path: "/albums/sarkar-lyrics"
song: "Simtaangaran"
image: ../../images/albumart/sarkar.jpg
date: 2018-11-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/aP7qPDeoIv0"
type: "happy"
singers:
  - Bamba Bakya
  - Vipin Aneja
  - Aparna Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Palti pakkura  darla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palti pakkura  darla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vudanum palthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vudanum palthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Worldu mothamum arla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Worldu mothamum arla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vudanum pisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vudanum pisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pisuru kelappi perla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pisuru kelappi perla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vudanum palthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vudanum palthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yee nekkulu pickleu maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yee nekkulu pickleu maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh thottanna thokalumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh thottanna thokalumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru kukkarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru kukkarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi tharaila ukkaarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi tharaila ukkaarumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yee nekkulu pickleu maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yee nekkulu pickleu maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh thottanna thokalumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh thottanna thokalumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru kukkarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru kukkarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi tharaila ukkaarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi tharaila ukkaarumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simataangaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simataangaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Enganaanee seeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enganaanee seeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Nintaen paaren mushtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nintaen paaren mushtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Apdikaa poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apdikaa poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simataangaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simataangaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Enganaanee seeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enganaanee seeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Nintaen paaren mushtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nintaen paaren mushtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Apdikaa poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apdikaa poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simataangaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simataangaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Enganaanee seeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enganaanee seeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Nintaen paaren mushtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nintaen paaren mushtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Apdikaa poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apdikaa poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simataangaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simataangaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Enganaanee seeran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enganaanee seeran"/>
</div>
<div class="lyrico-lyrics-wrapper">Nintaen paaren mushtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nintaen paaren mushtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Apdikaa poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apdikaa poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simataangaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simataangaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Silpinukka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silpinukka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Buckle-la poden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buckle-la poden"/>
</div>
<div class="lyrico-lyrics-wrapper">Virundhu vaikkaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virundhu vaikkaporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simataangaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simataangaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Silpinukka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silpinukka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Buckle-la poden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buckle-la poden"/>
</div>
<div class="lyrico-lyrics-wrapper">Virundhu vaikkaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virundhu vaikkaporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simataangaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simataangaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Silpinukka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silpinukka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Buckle-la poden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buckle-la poden"/>
</div>
<div class="lyrico-lyrics-wrapper">Virundhu vaikkaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virundhu vaikkaporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simataangaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simataangaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Silpinukka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silpinukka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Buckle-la poden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buckle-la poden"/>
</div>
<div class="lyrico-lyrics-wrapper">Virundhu vaikkaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virundhu vaikkaporen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palti pakkura  darla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palti pakkura  darla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vudanum palthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vudanum palthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Worldu mothamum arla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Worldu mothamum arla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vudanum pisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vudanum pisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pisuru kelappi perla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pisuru kelappi perla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vudanum palthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vudanum palthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yee nekkulu pickleu maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yee nekkulu pickleu maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh thottanna thokalumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh thottanna thokalumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru kukkarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru kukkarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi tharaila ukkaarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi tharaila ukkaarumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yee nekkulu pickleu maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yee nekkulu pickleu maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh thottanna thokalumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh thottanna thokalumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru kukkarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru kukkarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi tharaila ukkaarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi tharaila ukkaarumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andharu pannikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharu pannikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaaa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andharu pannikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharu pannikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaaa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andharu pannikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharu pannikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaaa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andharu pannikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharu pannikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaaa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannavaa nee vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannavaa nee vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththanglai nee thaa thaa thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththanglai nee thaa thaa thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhinthathuu nilavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhinthathuu nilavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarnthathu kanavoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarnthathu kanavoo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha ha ha ha haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha ha haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha ha haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha ha haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha ha haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha ha haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha ha haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha ha ha ha haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yee nekkulu pickleu maa Ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yee nekkulu pickleu maa Ohh"/>
</div>
<div class="lyrico-lyrics-wrapper">thottanna thokalumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottanna thokalumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru kukkarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru kukkarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi tharaila ukkaarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi tharaila ukkaarumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yee nekkulu pickleu maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yee nekkulu pickleu maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh thottanna thokalumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh thottanna thokalumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru kukkarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru kukkarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi tharaila ukkaarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi tharaila ukkaarumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andharu pannikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharu pannikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaaa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Gubeelu pisthu palthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gubeelu pisthu palthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikkalu vikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikkalu vikkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thottanna thottanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thottanna thottanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikkalu vikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikkalu vikkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooooooo oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooooooo oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokkalangaa Kokkalangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkalangaa Kokkalangaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kokkalangaa gubeelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkalangaa gubeelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heightla irndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heightla irndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dive adichaa dammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dive adichaa dammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma pustu irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma pustu irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottaiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Alla jorum pettayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alla jorum pettayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichinukom saettaiyila gubeelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichinukom saettaiyila gubeelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pisuru kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pisuru kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pisuru kelappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pisuru kelappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kokkalangaa kokkalangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkalangaa kokkalangaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kokkalangaa kuththa podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkalangaa kuththa podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oooooooooo oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooooooo oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palti pakkura  darla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palti pakkura  darla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vudanum palthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vudanum palthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Worldu mothamum arla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Worldu mothamum arla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vudanum pisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vudanum pisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pisuru kelappi perla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pisuru kelappi perla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vudanum palthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vudanum palthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yee nekkulu pickleu maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yee nekkulu pickleu maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh thottanna thokalumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh thottanna thokalumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru kukkarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru kukkarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi tharaila ukkaarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi tharaila ukkaarumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yee nekkulu pickleu maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yee nekkulu pickleu maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh thottanna thokalumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh thottanna thokalumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru kukkarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru kukkarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi tharaila ukkaarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi tharaila ukkaarumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andharu pannikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharu pannikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaaa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andharu pannikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharu pannikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaaa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andharu pannikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharu pannikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaaa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andharu pannikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharu pannikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaaa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaa Oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaa Oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vikkalu vikkaluHey thottanna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikkalu vikkaluHey thottanna "/>
</div>
<div class="lyrico-lyrics-wrapper">thottannaVikkalu vikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottannaVikkalu vikkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooooooo oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooooooo oooooo"/>
</div>
</pre>
