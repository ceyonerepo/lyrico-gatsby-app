---
title: "en kai enakku song lyrics"
album: "Puppy"
artist: "Dharan Kumar"
lyricist: "Mirchi Vijay"
director: "Nattu Dev"
path: "/albums/puppy-lyrics"
song: "En Kai Enakku"
image: ../../images/albumart/puppy.jpg
date: 2019-10-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pSPxoEB83-M"
type: "love"
singers:
  - Dharan Kumar
  - Sandy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hoo Hoo Ooo Hoo Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Ooo Hoo Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Ooo Oo Pap Paaba Ba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Ooo Oo Pap Paaba Ba"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Ooo Pap Paaba Ba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Ooo Pap Paaba Ba"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Ooo Pap Paaba Ba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Ooo Pap Paaba Ba"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Ooo Oo Pap Paaba Ba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Ooo Oo Pap Paaba Ba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Life-ula Problem Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Life-ula Problem Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Problemae Illaena Lifeae Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Problemae Illaena Lifeae Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Biceps-um Triceps-um Naanum Kaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biceps-um Triceps-um Naanum Kaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paartha Figure-um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paartha Figure-um"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Taata Kaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Taata Kaatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Tall-ah Fair-ah Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Tall-ah Fair-ah Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukken Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukken Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Summaru Moonji Ponnu Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaru Moonji Ponnu Kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatturenae Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatturenae Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tinted Glassu Car-u Ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tinted Glassu Car-u Ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Poottenae Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottenae Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kissu Kooda Missu Aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kissu Kooda Missu Aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bussunu Thaan Pochae Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bussunu Thaan Pochae Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaiyu Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaiyu Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaiyu Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaiyu Unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Than Kaiyae Udhavi Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than Kaiyae Udhavi Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Hoo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Hoo Ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaiyu Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaiyu Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaiyu Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaiyu Unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Than Kaiyae Udhavi Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than Kaiyae Udhavi Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Wowhu Wowhu Wohu Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wowhu Wowhu Wohu Haa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellakutty Bujjima Needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellakutty Bujjima Needhan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyirae Uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyirae Uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Varaikkum Sonnadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Varaikkum Sonnadhilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Naa Enakku Girlfriend Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Naa Enakku Girlfriend Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpae Thunaiyae Ellamae Needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpae Thunaiyae Ellamae Needhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nee Neeyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nee Neeyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Naan Naanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Naan Naanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Undaanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Undaanadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Theenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Theenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Vittu Selladha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Vittu Selladha"/>
</div>
<div class="lyrico-lyrics-wrapper">Break Up-um Pannadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break Up-um Pannadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Softu Semma Sweetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Softu Semma Sweetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Mummy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Mummy"/>
</div>
<div class="lyrico-lyrics-wrapper">Avangalayae Ottama Irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avangalayae Ottama Irundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu Kammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Kammi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Military Man-u Pola Irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Military Man-u Pola Irundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Strict-u Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Strict-u Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhanaala Naanum Aanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhanaala Naanum Aanenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Kedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Kedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puriya Vaikka Aalu Illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriya Vaikka Aalu Illama"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarnthen Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarnthen Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Puppy Oda Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puppy Oda Munnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Urundu Perandenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urundu Perandenadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life-ah Nenachu Tension
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-ah Nenachu Tension"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaikku Yerumae Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaikku Yerumae Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namba Nenacha Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Nenacha Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Soon-ah Life-u Thaana Maarum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soon-ah Life-u Thaana Maarum Da"/>
</div>
</pre>
