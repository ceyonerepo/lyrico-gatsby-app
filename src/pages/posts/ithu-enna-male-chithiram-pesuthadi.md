---
title: "ithu enna male song lyrics"
album: "Chithiram Pesuthadi"
artist: "Sundar C Babu"
lyricist: "Mysskin"
director: "Mysskin"
path: "/albums/chithiram-pesuthadi-lyrics"
song: "Ithu Enna Male"
image: ../../images/albumart/chithiram-pesuthadi.jpg
date: 2006-02-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jwz5BI_gOlY"
type: "love"
singers:
  - Vijay Gopal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ithu enna puthu unarvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu enna puthu unarvo"/>
</div>
<div class="lyrico-lyrics-wrapper">manathil paayuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil paayuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu enna puthu kanavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu enna puthu kanavo"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjai varuduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjai varuduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu sugam puthu sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu sugam puthu sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">ini ellam inimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini ellam inimai"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu yugam thinam thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu yugam thinam thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">ini ellam puthumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini ellam puthumai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu enna puthu unarvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu enna puthu unarvo"/>
</div>
<div class="lyrico-lyrics-wrapper">manathil paayuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil paayuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu enna puthu kanavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu enna puthu kanavo"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjai varuduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjai varuduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu sugam puthu sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu sugam puthu sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">ini ellam inimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini ellam inimai"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu yugam thinam thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu yugam thinam thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">ini ellam puthumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini ellam puthumai"/>
</div>
</pre>
