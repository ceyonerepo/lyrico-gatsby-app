---
title: "en nadanam song lyrics"
album: "Tamizh Padam 2"
artist: "N. Kannan"
lyricist: "Thiyaru"
director: "CS Amudhan"
path: "/albums/tamizh-padam-2-lyrics"
song: "En Nadanam"
image: ../../images/albumart/tamizh-padam-2.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eVOI2GsEAoY"
type: "happy"
singers:
  - Sharreth
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tha theem tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha theem tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakka tha theem tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakka tha theem tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharikida tha theem tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikida tha theem tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaththa tharikida theenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaththa tharikida theenn"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha theem tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha theem tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En nadanam nalinam padham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nadanam nalinam padham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini ivvulagam urigidum dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini ivvulagam urigidum dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En bharatham kalaiyin sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En bharatham kalaiyin sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini moovulagam mayangidum vitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini moovulagam mayangidum vitham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velvathu yaar yaar yaar yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velvathu yaar yaar yaar yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraivinil paar paar paar paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraivinil paar paar paar paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nathagan naan naan naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nathagan naan naan naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithagan naan naan naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithagan naan naan naan naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sivapatham naan naan naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivapatham naan naan naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanavam yen yen yen yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavam yen yen yen yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madivathu yaar yaar yaar yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madivathu yaar yaar yaar yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivinil paar paar paar paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivinil paar paar paar paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En nadanam nalinam padham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nadanam nalinam padham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini ivvulagam urigidum dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini ivvulagam urigidum dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvilae bharathamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvilae bharathamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Payindravan aatamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payindravan aatamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Menagai urvasi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menagai urvasi "/>
</div>
<div class="lyrico-lyrics-wrapper">parambarai aalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parambarai aalumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyilae viralilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyilae viralilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Abinayam paarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abinayam paarada"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaivilae azhagilae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaivilae azhagilae "/>
</div>
<div class="lyrico-lyrics-wrapper">athisayam naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athisayam naanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poda patharidum mooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda patharidum mooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadana medai unakkoru keda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadana medai unakkoru keda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada ularuvathaenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada ularuvathaenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadana maadi idupodippenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadana maadi idupodippenda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethirinil nil nil nil nil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirinil nil nil nil nil"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayamillai sol sol sol sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayamillai sol sol sol sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivanoru thee thee thee thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanoru thee thee thee thee"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaivathu nee nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaivathu nee nee nee nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arumporul naan naan naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumporul naan naan naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thurumbena nee nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurumbena nee nee nee nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varampera naan naan naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varampera naan naan naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vathaipada nee nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vathaipada nee nee nee nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Per pugazhum porulum varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per pugazhum porulum varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini en nadanam viruthugal perum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini en nadanam viruthugal perum"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirum udalum kalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirum udalum kalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini en nizhalum vendridum unai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini en nizhalum vendridum unai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakathimi jal jal jal jal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakathimi jal jal jal jal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari sari sel sel sel sel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari sari sel sel sel sel"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanmayil naan naan naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanmayil naan naan naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiganam yen yen yen yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiganam yen yen yen yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaikadal naan naan naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaikadal naan naan naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaivathu nee nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaivathu nee nee nee nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaimugil naan naan naan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaimugil naan naan naan naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhavadhu nee nee nee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhavadhu nee nee nee nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaka thagina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thagina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka thagina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thagina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka thagina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thagina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaka thagina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thagina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka thagina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thagina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka thagina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thagina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saga sagama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saga sagama"/>
</div>
<div class="lyrico-lyrics-wrapper">Dha dha dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dha dha dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Gama gamadha ni ni ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gama gamadha ni ni ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaka thagina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka thagina"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa sa ga ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa ga ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga ga ma ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga ga ma ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma ma dha dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma ma dha dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha tha ree ree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha tha ree ree"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga ga ma ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga ga ma ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma ma gaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma ma gaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha tha ree ree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha tha ree ree"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga ga ma ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga ga ma ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma ma gaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma ma gaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha tha ree ree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha tha ree ree"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha tha ree sa ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha tha ree sa ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yega yegana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yega yegana"/>
</div>
<div class="lyrico-lyrics-wrapper">Yega yegana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yega yegana"/>
</div>
<div class="lyrico-lyrics-wrapper">Yega yegana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yega yegana"/>
</div>
<div class="lyrico-lyrics-wrapper">Yega yegana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yega yegana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paga pagana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga pagana"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga pagana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga pagana"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga pagana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga pagana"/>
</div>
<div class="lyrico-lyrics-wrapper">Paga pagana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paga pagana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raga ragana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raga ragana"/>
</div>
<div class="lyrico-lyrics-wrapper">Raga ragana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raga ragana"/>
</div>
<div class="lyrico-lyrics-wrapper">Raga ragana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raga ragana"/>
</div>
<div class="lyrico-lyrics-wrapper">Raga ragana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raga ragana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaga thagana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga thagana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga thagana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga thagana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga thagana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga thagana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga thagana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga thagana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa"/>
</div>
</pre>
