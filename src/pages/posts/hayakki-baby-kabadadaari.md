---
title: "hayakki baby song lyrics"
album: "Kabadadaari"
artist: "Simon K.King"
lyricist: "Ku Karthick"
director: "Pradeep Krishnamoorthy"
path: "/albums/kabadadaari-song-lyrics"
song: "Hayakki Baby"
image: ../../images/albumart/kabadadaari.jpg
date: 2021-01-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lNxylYfhzhw"
type: "Celebration"
singers:
  - Sanah Moidutty
  - Krishan Maheson
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Irul Soorum... Neramidhu..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul Soorum... Neramidhu.."/>
</div>
<div class="lyrico-lyrics-wrapper">Athanulle... Vidai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanulle... Vidai.."/>
</div>
<div class="lyrico-lyrics-wrapper">Theerum.. Varai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerum.. Varai.."/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan... Pirai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan... Pirai.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mudintha Piragum Thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudintha Piragum Thodarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraa... Kadhai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraa... Kadhai.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hop Up In This Club With Some Model (Model)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hop Up In This Club With Some Model (Model)"/>
</div>
<div class="lyrico-lyrics-wrapper">Sittin VIP Poppin Bottles (Bottles)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittin VIP Poppin Bottles (Bottles)"/>
</div>
<div class="lyrico-lyrics-wrapper">We Aint Slowin Down We Full Throttle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Aint Slowin Down We Full Throttle"/>
</div>
<div class="lyrico-lyrics-wrapper">We Just Do It Big Home
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Just Do It Big Home"/>
</div>
<div class="lyrico-lyrics-wrapper">We Just So Collosal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Just So Collosal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Just Break It Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Just Break It Now"/>
</div>
<div class="lyrico-lyrics-wrapper">Baby Girl You Know To Work It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Girl You Know To Work It"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce It To The Sides And
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce It To The Sides And"/>
</div>
<div class="lyrico-lyrics-wrapper">I Love The Way You Twerk It Shake It N U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Love The Way You Twerk It Shake It N U"/>
</div>
<div class="lyrico-lyrics-wrapper">Break It Exoyic Like Teriyaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break It Exoyic Like Teriyaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Spin It Disc Jockey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spin It Disc Jockey"/>
</div>
<div class="lyrico-lyrics-wrapper">I'M Dancin With This Hayaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'M Dancin With This Hayaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Hayakki Hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hayakki Hayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Macham Macham Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macham Macham Padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Mayakki Mayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Mayakki Mayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham Meethi Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham Meethi Pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Hayakki Hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hayakki Hayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Macham Macham Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macham Macham Padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Mayakki Mayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Mayakki Mayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham Meethi Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham Meethi Pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pon Vilakkirukk.. Nee Urasida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Vilakkirukk.. Nee Urasida Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Boodham Veliyera..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boodham Veliyera.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Madippugalai.. Nee Alasida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Madippugalai.. Nee Alasida Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhaiyal Karaiyera...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhaiyal Karaiyera..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Love It The Way You Shake It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Love It The Way You Shake It"/>
</div>
<div class="lyrico-lyrics-wrapper">Around Girl You Make Me Psycho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Around Girl You Make Me Psycho"/>
</div>
<div class="lyrico-lyrics-wrapper">Keep What You Doin Don't Ever Stop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keep What You Doin Don't Ever Stop"/>
</div>
<div class="lyrico-lyrics-wrapper">Babe You Make My Mind Blow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babe You Make My Mind Blow"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Can't Get You Out Of Mind 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can't Get You Out Of Mind "/>
</div>
<div class="lyrico-lyrics-wrapper">Can't Can't Can't Get You Out of Mind
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can't Can't Can't Get You Out of Mind"/>
</div>
<div class="lyrico-lyrics-wrapper">When The Night is Done
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When The Night is Done"/>
</div>
<div class="lyrico-lyrics-wrapper">Let Me Take Control
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let Me Take Control"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inni Inni Hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni Inni Hayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">inni Inni Hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inni Inni Hayakki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Hayakki Hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Hayakki Hayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Macham Macham Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macham Macham Padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Mayakki Mayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Mayakki Mayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham Meethi Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham Meethi Pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayakki Hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayakki Hayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Macham Macham Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macham Macham Padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Mayakki Mayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Mayakki Mayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham Meethi Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham Meethi Pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayakki On The Floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayakki On The Floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Yalla... Yalla...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yalla... Yalla..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inni Inni Hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni Inni Hayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Valla Habibi...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valla Habibi..."/>
</div>
<div class="lyrico-lyrics-wrapper">Valla Habibi...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valla Habibi..."/>
</div>
<div class="lyrico-lyrics-wrapper">Valla Habibi...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valla Habibi..."/>
</div>
<div class="lyrico-lyrics-wrapper">Inni Inni Hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni Inni Hayakki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
( <div class="lyrico-lyrics-wrapper">Inni Inni Hayakki..)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni Inni Hayakki..)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh En Madiyinil Manmadhanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh En Madiyinil Manmadhanum "/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunthiruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthiruppa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mizhiyinil Indhiranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mizhiyinil Indhiranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhainthirupaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhainthirupaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayathin Kavasaithai Parithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayathin Kavasaithai Parithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaipesa Valaiveesa Thullum Ooridhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaipesa Valaiveesa Thullum Ooridhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karchilai Ena Ennai Vadithaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karchilai Ena Ennai Vadithaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Brahmmanum Unnai Konji Anaippaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmmanum Unnai Konji Anaippaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Oviyamena Ennai Varainthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyamena Ennai Varainthaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Picaso Pirappaan Un Kaalil Vizhuvaan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picaso Pirappaan Un Kaalil Vizhuvaan.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayakki Baby.. Hayakki Baby..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayakki Baby.. Hayakki Baby.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Azhagirkku Adimaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Azhagirkku Adimaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Latcham Latchame...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latcham Latchame..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayakki Baby.. Hayakki Baby..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayakki Baby.. Hayakki Baby.."/>
</div>
<div class="lyrico-lyrics-wrapper">En Iravukkul Pudhumaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iravukkul Pudhumaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekka Chakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekka Chakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seena Pottutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seena Pottutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillaraiya Kottitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillaraiya Kottitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttu Santhula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttu Santhula"/>
</div>
<div class="lyrico-lyrics-wrapper">Traina Ottittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Traina Ottittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Plana Pottuttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plana Pottuttaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniyathaa Kaattittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniyathaa Kaattittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachu Senjuttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachu Senjuttaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inni Inni Hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni Inni Hayakki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayakki Hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayakki Hayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Macham Macham Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macham Macham Padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakki Mayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakki Mayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham Micham Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham Micham Pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayakki Hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayakki Hayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Macham Macham Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macham Macham Padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakki Mayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakki Mayakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham Micham Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham Micham Pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yalla.. Yalla..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yalla.. Yalla.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Habibi..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Habibi.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inni Inni Hayakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni Inni Hayakki"/>
</div>
</pre>
