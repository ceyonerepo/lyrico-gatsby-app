---
title: "muthan muthalaga song lyrics"
album: "Sankarankovil"
artist: "Rajini"
lyricist: "Snehan"
director: "Palanivel Raja"
path: "/albums/sankarankovil-lyrics"
song: "Muthan Muthalaga"
image: ../../images/albumart/sankarankovil.jpg
date: 2011-08-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zX0bGSp4WUk"
type: "love"
singers:
  - Surmukhi
  - Balaji
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hi common
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hi common"/>
</div>
<div class="lyrico-lyrics-wrapper">kannum kannum moadhumboadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannum kannum moadhumboadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal neruppu paththikkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal neruppu paththikkudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">karuppu vellai kanavugalellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuppu vellai kanavugalellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kalar kalaraaga maarikkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalar kalaraaga maarikkudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhan mudhalaaga kaadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhan mudhalaaga kaadhal "/>
</div>
<div class="lyrico-lyrics-wrapper">duyat paada vandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="duyat paada vandheney"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhan mudhalaaga kaadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhan mudhalaaga kaadhal "/>
</div>
<div class="lyrico-lyrics-wrapper">duyat paada vandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="duyat paada vandheney"/>
</div>
<div class="lyrico-lyrics-wrapper">en manadhai parithavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manadhai parithavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyiril kulithavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyiril kulithavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">un paarvai saaraliley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai saaraliley"/>
</div>
<div class="lyrico-lyrics-wrapper">naan uraindhen thooraliley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan uraindhen thooraliley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mudhan mudhalaaga kaadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhan mudhalaaga kaadhal "/>
</div>
<div class="lyrico-lyrics-wrapper">duyat paada vandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="duyat paada vandheney"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhan mudhalaaga kaadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhan mudhalaaga kaadhal "/>
</div>
<div class="lyrico-lyrics-wrapper">duyat paada vandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="duyat paada vandheney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaga vaazhum naatkal sugamaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga vaazhum naatkal sugamaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">neethaaney endhan ulagam nijamaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethaaney endhan ulagam nijamaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaaga vaazhum naatkal sugamaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga vaazhum naatkal sugamaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">neethaaney endhan ulagam nijamaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethaaney endhan ulagam nijamaanadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaadha nadigan neethaan manam solludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaadha nadigan neethaan manam solludhu"/>
</div>
<div class="lyrico-lyrics-wrapper">veenaana poigal vendaam vidu nalladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veenaana poigal vendaam vidu nalladhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Herokkal poala naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Herokkal poala naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">vesham poattu viththaikaattavenuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vesham poattu viththaikaattavenuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoada noakkam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoada noakkam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaranam theriyadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaranam theriyadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan poigaliley unna purattugiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan poigaliley unna purattugiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaaga sinamaaga nambidavendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaaga sinamaaga nambidavendaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mudhan mudhalaaga kaadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhan mudhalaaga kaadhal "/>
</div>
<div class="lyrico-lyrics-wrapper">duyat paada vandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="duyat paada vandheney"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhan mudhalaaga kaadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhan mudhalaaga kaadhal "/>
</div>
<div class="lyrico-lyrics-wrapper">duyat paada vandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="duyat paada vandheney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum kannum moadhumboadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum kannum moadhumboadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal neruppu paththikkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal neruppu paththikkudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">karuppu vellai kanavugalellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuppu vellai kanavugalellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kalar kalaraaga maarikkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalar kalaraaga maarikkudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uruvaththai vidavum ullam azhagaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvaththai vidavum ullam azhagaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulleri vandhupaarkka manam yengudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulleri vandhupaarkka manam yengudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvaththai vidavum ullam azhagaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvaththai vidavum ullam azhagaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulleri vandhupaarkka manam yengudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulleri vandhupaarkka manam yengudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kaadhal unmai endraal tharugindreney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaadhal unmai endraal tharugindreney"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoadu ulagai sutra varugindreney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoadu ulagai sutra varugindreney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orukkannaal OK sonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orukkannaal OK sonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">indha ulagai otrai nodiyil velluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha ulagai otrai nodiyil velluven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En meedhu eppadi vandhadhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En meedhu eppadi vandhadhu "/>
</div>
<div class="lyrico-lyrics-wrapper">iththanai kaadhaladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iththanai kaadhaladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi paadhakathi nee kavuthuttiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi paadhakathi nee kavuthuttiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan paaduna vesaththa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan paaduna vesaththa "/>
</div>
<div class="lyrico-lyrics-wrapper">paarthadhum yendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthadhum yendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mudhan mudhalaaga kaadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhan mudhalaaga kaadhal "/>
</div>
<div class="lyrico-lyrics-wrapper">duyat paada vandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="duyat paada vandheney"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhan mudhalaaga kaadhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhan mudhalaaga kaadhal "/>
</div>
<div class="lyrico-lyrics-wrapper">duyat paada vandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="duyat paada vandheney"/>
</div>
</pre>
