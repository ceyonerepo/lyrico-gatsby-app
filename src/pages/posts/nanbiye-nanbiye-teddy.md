---
title: "nanbiye nanbiye song lyrics"
album: "teddy"
artist: "D Imman"
lyricist: "madhan karky"
director: "shakti soundar rajan"
path: "/albums/teddy-song-lyrics"
song: "nanbiye nanbiye"
image: ../../images/albumart/teddy.jpg
date: 2021-03-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6o8pxa1PJ1g"
type: "friendship"
singers:
  - anirudh ravichander
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Endhan Nanbiye Nanbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nanbiye Nanbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thirakkum Anbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thirakkum Anbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Nanbiye Nanbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nanbiye Nanbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Izhukkum Inbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Izhukkum Inbiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Mugam Kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Mugam Kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagaigal Theettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagaigal Theettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhin Kannadi Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhin Kannadi Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Ennai Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Ennai Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Yettrukondathaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yettrukondathaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhiroliyaagiduvaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiroliyaagiduvaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandathai Paadavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandathai Paadavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmoodi Aadavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodi Aadavum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thunaiaagida Vathaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thunaiaagida Vathaaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandaigal Podavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaigal Podavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pin Vandhu Koodavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pin Vandhu Koodavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram Kaaranam Thanthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Kaaranam Thanthaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannangal Naane Nee Thoorigaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannangal Naane Nee Thoorigaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Nanbiye Nanbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nanbiye Nanbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thirakkum Anbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thirakkum Anbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Nanbiye Nanbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nanbiye Nanbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Izhukkum Inbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Izhukkum Inbiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Nanbiye Nanbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nanbiye Nanbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thirakkum Anbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thirakkum Anbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Nanbiye Nanbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nanbiye Nanbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Izhukkum Inbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Izhukkum Inbiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Manam Paarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Manam Paarka"/>
</div>
<div class="lyrico-lyrics-wrapper">Solvathellam Kekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solvathellam Kekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaitha Oar Uyir Thunai Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaitha Oar Uyir Thunai Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Siripil Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Siripil Paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thuyaril Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thuyaril Paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagirndhu Nee Arundhugiraaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagirndhu Nee Arundhugiraaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Arundhugiraaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arundhugiraaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaame Poiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaame Poiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mattum Meiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mattum Meiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">En Acham Yaavaiyum Kondraaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Acham Yaavaiyum Kondraaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Ingu Unmaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ingu Unmaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaiyil Bommaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaiyil Bommaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Indha Naan Ena Sonnaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Indha Naan Ena Sonnaaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevvaanam Naane Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvaanam Naane Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanthigaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanthigaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Nanbiye Nanbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nanbiye Nanbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thirakkum Anbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thirakkum Anbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Nanbiye Nanbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nanbiye Nanbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Izhukkum Inbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Izhukkum Inbiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mei Nigaraatangal Aadidum Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei Nigaraatangal Aadidum Podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram Edhirigal Porakalam Meedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Edhirigal Porakalam Meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Padaiyil Neeyum Irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Padaiyil Neeyum Irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Vettri Endhan Kaaladiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Vettri Endhan Kaaladiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaya Thodarai Inanithe Meikaanbome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaya Thodarai Inanithe Meikaanbome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhuthaal Udane Nee Thudaipaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuthaal Udane Nee Thudaipaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathil Ninaithu Oru Sol Sollumpothey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathil Ninaithu Oru Sol Sollumpothey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodangum Ethaiyum Nee Mudippaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodangum Ethaiyum Nee Mudippaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Endhan Thanimaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Endhan Thanimaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Vida Inimaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Vida Inimaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaya Suvaril Iraivan Varaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaya Suvaril Iraivan Varaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karunagaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karunagaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Nanbiye Nanbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nanbiye Nanbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thirakkum Anbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thirakkum Anbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Nanbiye Nanbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Nanbiye Nanbiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Izhukkum Inbiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Izhukkum Inbiye"/>
</div>
</pre>
