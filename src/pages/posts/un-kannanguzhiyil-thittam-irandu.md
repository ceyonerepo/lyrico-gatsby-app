---
title: "un kannanguzhiyil song lyrics"
album: "Thittam Irandu"
artist: "Satish Raghunathan"
lyricist: "Mohan Rajan"
director: "Vignesh Karthick"
path: "/albums/thittam-irandu-lyrics"
song: "Un Kannanguzhiyil"
image: ../../images/albumart/thittam-irandu.jpg
date: 2021-07-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5yG_7HiBNYs"
type: "love"
singers:
  - Karthika Vaidyanathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">un kannanguzhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannanguzhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna izhukka paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna izhukka paakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedanthu parithavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedanthu parithavikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaum en imaigalaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaum en imaigalaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan parakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan parakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nizhala kooda naan rasikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nizhala kooda naan rasikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhi kannil naa paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi kannil naa paakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhaalaanu naa ketkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhaalaanu naa ketkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhi kannil naa paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi kannil naa paakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna senja naa thokkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna senja naa thokkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un kannanguzhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannanguzhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna izhukka paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna izhukka paakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedanthu parithavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedanthu parithavikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaum en imaigalaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaum en imaigalaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan parakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan parakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nizhala kooda naan rasikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nizhala kooda naan rasikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhi kannil naa paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi kannil naa paakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhaalaanu naa ketkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhaalaanu naa ketkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhi kannil naa paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi kannil naa paakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna senja naa thokkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna senja naa thokkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venaam venaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaam venaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum solla solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum solla solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettidaama kaal poguthae daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettidaama kaal poguthae daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Venum venunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venum venunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhaan pesum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaan pesum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Poividaama neela venumae daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poividaama neela venumae daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan inga naanaaga illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan inga naanaaga illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naethoda naan kaanavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naethoda naan kaanavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamae thallaaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamae thallaaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna paadhi kannil naa paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paadhi kannil naa paakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhaalaanu naa ketkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhaalaanu naa ketkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhi kannil naa paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi kannil naa paakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna senja naa thokkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna senja naa thokkuren"/>
</div>
</pre>
