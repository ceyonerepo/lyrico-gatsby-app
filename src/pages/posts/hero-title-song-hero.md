---
title: "hero title song song lyrics"
album: "Hero"
artist: "Ghibran"
lyricist: "Pa. Vijay"
director: "Sriram Aditya"
path: "/albums/hero-lyrics"
song: "Hero Title Song"
image: ../../images/albumart/hero-telugu.jpg
date: 2022-01-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/dDDUgaQuWMY"
type: "title track"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Saga manidhanai madhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saga manidhanai madhikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha manidhanum hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha manidhanum hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Suya sindhanaigal irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suya sindhanaigal irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoruthanume hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoruthanume hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan mun nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan mun nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappai thatti thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappai thatti thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkiravan medai yerinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkiravan medai yerinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai mattume pesubavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai mattume pesubavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugazh vandha pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugazh vandha pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharai meedhudhaan nirkkiravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharai meedhudhaan nirkkiravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin munne thotru nirpavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin munne thotru nirpavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero hero hero hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero hero hero hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Medai yeruda vaanam thoduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medai yeruda vaanam thoduda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottutta keezha thalli taan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottutta keezha thalli taan"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam sirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottam sirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunda meendum yezhunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunda meendum yezhunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthu paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthu paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Siricha koottam munna nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha koottam munna nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeichu kaattuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeichu kaattuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Netrin valikal unakku kaattum vazhikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrin valikal unakku kaattum vazhikal"/>
</div>
<div class="lyrico-lyrics-wrapper">Selvom vaa saivom vaa hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selvom vaa saivom vaa hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal kanavum engal aasayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal kanavum engal aasayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paritchai paperil karaigiradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paritchai paperil karaigiradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna chinna siragugal ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna chinna siragugal ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthakam sumandhe udaigiradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthakam sumandhe udaigiradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vagupugal ellam siraigalum alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vagupugal ellam siraigalum alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Varugira kanavukal thavarugal alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varugira kanavukal thavarugal alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar badhil tharuvaar?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar badhil tharuvaar?"/>
</div>
<div class="lyrico-lyrics-wrapper">Mark ku kaana ottam podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mark ku kaana ottam podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mark ai thandi odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mark ai thandi odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelviku badhalai yezhudhum nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelviku badhalai yezhudhum nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelviya neeyum thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelviya neeyum thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chemistry lla fail aanavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chemistry lla fail aanavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttaal ondrum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttaal ondrum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Matha il neeyum medhai aalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matha il neeyum medhai aalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mele vaada vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mele vaada vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiramayai pattai theettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiramayai pattai theettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittam dhaane kalvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittam dhaane kalvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthagatha mug up pannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthagatha mug up pannum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalvi sariya thampi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalvi sariya thampi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivikku filter podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivikku filter podum"/>
</div>
<div class="lyrico-lyrics-wrapper">System thaane neetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="System thaane neetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhayum meeri talentil nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhayum meeri talentil nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarunu dhaan kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarunu dhaan kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thervil thorpadhu tholvi illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thervil thorpadhu tholvi illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani thiramaiyai thervugal ketpathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani thiramaiyai thervugal ketpathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I felt the way on my hair
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I felt the way on my hair"/>
</div>
<div class="lyrico-lyrics-wrapper">When i saw you across the hall
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When i saw you across the hall"/>
</div>
<div class="lyrico-lyrics-wrapper">Wearing a coat that spells H E R O fireball
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wearing a coat that spells H E R O fireball"/>
</div>
<div class="lyrico-lyrics-wrapper">Everytime you enter in my space
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everytime you enter in my space"/>
</div>
<div class="lyrico-lyrics-wrapper">I am always blessed with
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am always blessed with"/>
</div>
<div class="lyrico-lyrics-wrapper">Your everlasting grace
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Your everlasting grace"/>
</div>
<div class="lyrico-lyrics-wrapper">Boy you got me good
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boy you got me good"/>
</div>
<div class="lyrico-lyrics-wrapper">You’re a king you’re a saint
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You’re a king you’re a saint"/>
</div>
<div class="lyrico-lyrics-wrapper">You’re a nighthood
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You’re a nighthood"/>
</div>
<div class="lyrico-lyrics-wrapper">Can’t take you down with a smother
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can’t take you down with a smother"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonna hold you tight daylight firefight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonna hold you tight daylight firefight"/>
</div>
<div class="lyrico-lyrics-wrapper">You’re my kryptonite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You’re my kryptonite"/>
</div>
<div class="lyrico-lyrics-wrapper">I will never trade you for another
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I will never trade you for another"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurai solvadhai thaandi adhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurai solvadhai thaandi adhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seri saibavane hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seri saibavane hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Matravan vetriyai paarthu nija
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matravan vetriyai paarthu nija"/>
</div>
<div class="lyrico-lyrics-wrapper">Magizhchi kolbavan hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magizhchi kolbavan hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan mun nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan mun nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappai thatti thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappai thatti thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkiravan medai yerinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkiravan medai yerinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai mattume pesubavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai mattume pesubavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugazh vandha pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugazh vandha pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharai meedhudhaan nirkkiravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharai meedhudhaan nirkkiravan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin munne thotru nirpavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin munne thotru nirpavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero hero hero hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero hero hero hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Medai yeruda vaanam thoduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medai yeruda vaanam thoduda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottutta keezha thalli taan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottutta keezha thalli taan"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam sirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottam sirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunda meendum yezhunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunda meendum yezhunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthu paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthu paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Siricha koottam munna nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha koottam munna nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeichu kaattuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeichu kaattuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Know that you never gonna fight alone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Know that you never gonna fight alone"/>
</div>
<div class="lyrico-lyrics-wrapper">Standing so tall like a firestone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Standing so tall like a firestone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Know that you never gonna fight alone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Know that you never gonna fight alone"/>
</div>
<div class="lyrico-lyrics-wrapper">Standing so tall like a firestone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Standing so tall like a firestone"/>
</div>
</pre>
