---
title: "rakshasudey song lyrics"
album: "O Pitta Katha"
artist: "Pravin Lakkaraju"
lyricist: "Sreejo"
director: "Chendu Muddhu"
path: "/albums/o-pitta-katha-lyrics"
song: "Rakshasudey"
image: ../../images/albumart/o-pitta-katha.jpg
date: 2020-03-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/GJWzhAlzeGk"
type: "mass"
singers:
  - Pravin Lakkaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Needhannadhe Neekandhadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhannadhe Neekandhadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Karmidhe Malupo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Karmidhe Malupo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Praaname Chejaarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Praaname Chejaarene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Himsadhee Gelupo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Himsadhee Gelupo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manishi Musugulone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manishi Musugulone"/>
</div>
<div class="lyrico-lyrics-wrapper">Asuramsa Dhaagi Undhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asuramsa Dhaagi Undhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagavu Cheripi Chusthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagavu Cheripi Chusthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Narakanni Niluputhaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narakanni Niluputhaade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raakshasudey Veedu Raakshasudey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakshasudey Veedu Raakshasudey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Kathake Veedu Raakshasudey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kathake Veedu Raakshasudey"/>
</div>
</pre>
