---
title: "adavi thalli maata song lyrics"
album: "Bheemla Nayak"
artist: "S. Thaman"
lyricist: "Ramajogayya Sastry"
director: "Saagar K Chandra"
path: "/albums/bheemla-nayak-lyrics"
song: "Adavi Thalli Maata"
image: ../../images/albumart/bheemla-nayak.jpg
date: 2022-02-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/-Fvd2LXv5YY"
type: "sad"
singers:
  - Kummari Durgavva
  - Sahiti Chaganti
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kindunna Madusulakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindunna Madusulakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kopaalu Themalavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopaalu Themalavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Painunna Saamemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Painunna Saamemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kimmani Palakadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kimmani Palakadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhooketu Katthulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooketu Katthulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanikaramerugavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanikaramerugavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antukunna Aggeelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antukunna Aggeelona"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanavaallu Migalavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavaallu Migalavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sebuthunna Nee Manchi Seddaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sebuthunna Nee Manchi Seddaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthoti Panthaalu Poboku Biddaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthoti Panthaalu Poboku Biddaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sebuthunna Nee Manchi Seddaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sebuthunna Nee Manchi Seddaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthoti Panthaalu Poboku Biddaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthoti Panthaalu Poboku Biddaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siguraaku Sittadavi Gadda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siguraaku Sittadavi Gadda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chichhullo Attudiki Poraadhu Bidda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chichhullo Attudiki Poraadhu Bidda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Putta Thene Buvva Petnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putta Thene Buvva Petnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Selayeti Neellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selayeti Neellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinka Paalu Patnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinka Paalu Patnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oodalla Uyyaala Gatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodalla Uyyaala Gatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Penchi Ninnu Usthaadalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penchi Ninnu Usthaadalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninchobetnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninchobetnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachani Bathikitthe Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachani Bathikitthe Neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellelli Kachhalla Padaboku Bidda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellelli Kachhalla Padaboku Bidda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sebuthunna Nee Manchi Seddaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sebuthunna Nee Manchi Seddaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthoti Panthaalu Poboku Biddaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthoti Panthaalu Poboku Biddaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siguraaku Sittadavi Gadda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siguraaku Sittadavi Gadda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chichhullo Attudiki Poraadhu Bidda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chichhullo Attudiki Poraadhu Bidda"/>
</div>
</pre>
