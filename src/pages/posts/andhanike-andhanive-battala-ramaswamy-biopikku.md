---
title: "andhanike song lyrics"
album: "Battala Ramaswamy Biopikku"
artist: "Ram Narayan"
lyricist: "Vasudeva Murthy"
director: "Rama Narayanan"
path: "/albums/battala-ramaswamy-biopikku-lyrics"
song: "Andhanike Andhanive"
image: ../../images/albumart/battala-ramaswamy-biopikku.jpg
date: 2021-05-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Liexvdq51-E"
type: "sad"
singers:
  - Dinker Kalvala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Andhanike andhanive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhanike andhanive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andaala jaabili neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andaala jaabili neeve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marumallive sirijalluve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumallive sirijalluve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhela ennela neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhela ennela neeve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa baadhe marisipoye maayedho sesesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa baadhe marisipoye maayedho sesesaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelone kalisipoye matthedho jallesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelone kalisipoye matthedho jallesaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee gundelone illesukuntaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gundelone illesukuntaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa chota nanne dhaachesukuntaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa chota nanne dhaachesukuntaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pillaa nee needanu nenai thiruguthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaa nee needanu nenai thiruguthunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaa naa thodugaa ninne aduguthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaa naa thodugaa ninne aduguthunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thellaarlu nidhure tharime kalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellaarlu nidhure tharime kalale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nannu melikalu esaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu melikalu esaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thellaari maname kalise varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellaari maname kalise varaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enni godavalu sesaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni godavalu sesaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee pilupulo nenu pudithe murisipothaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pilupulo nenu pudithe murisipothaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee gundelone illesukuntaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gundelone illesukuntaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa chota nanne dhaachesukuntaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa chota nanne dhaachesukuntaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhanike andhanive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhanike andhanive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andaala jaabili neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andaala jaabili neeve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marumallive sirijalluve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumallive sirijalluve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhela ennela neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhela ennela neeve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa baadhe marisipoye maayedho sesesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa baadhe marisipoye maayedho sesesaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelone kalisipoye matthedho jallesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelone kalisipoye matthedho jallesaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee gundelone illesukuntaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gundelone illesukuntaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa chota nanne dhaachesukuntaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa chota nanne dhaachesukuntaane"/>
</div>
</pre>
