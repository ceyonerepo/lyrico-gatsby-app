---
title: "fififee fifeefee song lyrics"
album: "Gaali Sampath"
artist: "Achu"
lyricist: "Ramajogayya Sastry"
director: "Anish Krishna"
path: "/albums/gaali-sampath-lyrics"
song: "Fififee Fifeefee - Raja Rajashri"
image: ../../images/albumart/gaali-sampath.jpg
date: 2021-03-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/O_NkZms9wS4"
type: "happy"
singers:
 - Rahul Nambiyar
 - Srikrishna Vishnubotla
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Fififee fifee fee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fififee fifee fee"/>
</div>
<div class="lyrico-lyrics-wrapper">Fififee fifee fee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fififee fifee fee"/>
</div>
<div class="lyrico-lyrics-wrapper">Fififee fifee fee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fififee fifee fee"/>
</div>
<div class="lyrico-lyrics-wrapper">Fififee fifee fee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fififee fifee fee"/>
</div>
<div class="lyrico-lyrics-wrapper">Fififee fifee fee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fififee fifee fee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja rajashri gaali sampath gaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja rajashri gaali sampath gaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">My dear dady baabandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My dear dady baabandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa baabu gaaru chese daily vinyaasaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa baabu gaaru chese daily vinyaasaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohatheetham sumandee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohatheetham sumandee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey arre kokkoroko thellaarindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey arre kokkoroko thellaarindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho oka problem inti meedhiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho oka problem inti meedhiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Theesukuraandhe nidharodandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theesukuraandhe nidharodandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre thittee kottee manchi cheddaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre thittee kottee manchi cheddaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppe vayasu kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppe vayasu kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeyana gaaritho ettaa egaalandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeyana gaaritho ettaa egaalandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fififee fifee fee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fififee fifee fee"/>
</div>
<div class="lyrico-lyrics-wrapper">Fififee fifee fee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fififee fifee fee"/>
</div>
<div class="lyrico-lyrics-wrapper">Crazy daddy, fififee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crazy daddy, fififee"/>
</div>
<div class="lyrico-lyrics-wrapper">Fififee fifee fee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fififee fifee fee"/>
</div>
<div class="lyrico-lyrics-wrapper">Fififee fifee fee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fififee fifee fee"/>
</div>
<div class="lyrico-lyrics-wrapper">Crazy daddy, fififee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crazy daddy, fififee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy thussu bussu chater box-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy thussu bussu chater box-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Useless ganguku boss-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Useless ganguku boss-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Edaadhi mottham pele lakshmi patasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edaadhi mottham pele lakshmi patasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasthaa choosukovayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasthaa choosukovayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atte ee master ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atte ee master ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorimeedha vadhalamaakayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorimeedha vadhalamaakayyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy oosuponi makeup rangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy oosuponi makeup rangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodipoye meesaala kingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodipoye meesaala kingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baahubalinantaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baahubalinantaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee drama bigg boss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee drama bigg boss"/>
</div>
<div class="lyrico-lyrics-wrapper">Jara jaagartha bhayyaa poddhunlesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jara jaagartha bhayyaa poddhunlesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Intlo eeyantho undedhi nuvvayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intlo eeyantho undedhi nuvvayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre ontlo unde energylu chaala overdose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre ontlo unde energylu chaala overdose"/>
</div>
<div class="lyrico-lyrics-wrapper">Panipaata leni panlaki syllabus
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panipaata leni panlaki syllabus"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja rajashri gaali sampath gaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja rajashri gaali sampath gaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">My dear dady baabandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My dear dady baabandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa baabu gaaru chese daily vinyaasaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa baabu gaaru chese daily vinyaasaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohatheetham sumandee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohatheetham sumandee"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre kokkoroko thellaarindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre kokkoroko thellaarindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho oka proble inti meedhiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho oka proble inti meedhiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Theesukuraandhe nidharodandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theesukuraandhe nidharodandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre thittee kottee manchi cheddaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre thittee kottee manchi cheddaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppe vayasu kaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppe vayasu kaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeyana gaaritho ettaa egaalandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeyana gaaritho ettaa egaalandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fififee fifee fee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fififee fifee fee"/>
</div>
<div class="lyrico-lyrics-wrapper">Fififee fifee fee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fififee fifee fee"/>
</div>
<div class="lyrico-lyrics-wrapper">Crazy daddy, fififee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crazy daddy, fififee"/>
</div>
<div class="lyrico-lyrics-wrapper">Fififee fifee fee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fififee fifee fee"/>
</div>
<div class="lyrico-lyrics-wrapper">Fififee fifee fee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fififee fifee fee"/>
</div>
<div class="lyrico-lyrics-wrapper">Crazy daddy, fififee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crazy daddy, fififee"/>
</div>
</pre>
