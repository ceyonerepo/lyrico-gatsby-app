---
title: "cocktail party song lyrics"
album: "Cocktail"
artist: "Sai Bhaskar"
lyricist: "Sai Bhaskar"
director: "Ra.Vijaya Murugan"
path: "/albums/cocktail-lyrics"
song: "Cocktail Party - Bodhai Yethum"
image: ../../images/albumart/cocktail.jpg
date: 2020-07-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/egspFAzjK3Y"
type: "happy"
singers:
  - Paul Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">bothai yethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothai yethum"/>
</div>
<div class="lyrico-lyrics-wrapper">indha bottom sippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha bottom sippu"/>
</div>
<div class="lyrico-lyrics-wrapper">bottle oda dhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bottle oda dhane"/>
</div>
<div class="lyrico-lyrics-wrapper">lip 2 lipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lip 2 lipu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaiyula scotchudhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyula scotchudhan"/>
</div>
<div class="lyrico-lyrics-wrapper">pathathum santhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathathum santhosham"/>
</div>
<div class="lyrico-lyrics-wrapper">natpula naangadhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpula naangadhan"/>
</div>
<div class="lyrico-lyrics-wrapper">endrume ullasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrume ullasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey mama"/>
</div>
<div class="lyrico-lyrics-wrapper">feelin adhan oram katalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="feelin adhan oram katalama"/>
</div>
<div class="lyrico-lyrics-wrapper">ada rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada rama"/>
</div>
<div class="lyrico-lyrics-wrapper">ravodadhan party panalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ravodadhan party panalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">beat a dan yethi podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beat a dan yethi podu"/>
</div>
<div class="lyrico-lyrics-wrapper">kavalayum poyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalayum poyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">mixingum maari pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mixingum maari pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu cocktail party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu cocktail party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru cutting sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru cutting sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">maama maama maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama maama maama"/>
</div>
<div class="lyrico-lyrics-wrapper">company kooda vaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="company kooda vaama"/>
</div>
<div class="lyrico-lyrics-wrapper">inoru cutting sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inoru cutting sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">maama maama maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama maama maama"/>
</div>
<div class="lyrico-lyrics-wrapper">company kooda vaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="company kooda vaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thimura suthuvome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thimura suthuvome"/>
</div>
<div class="lyrico-lyrics-wrapper">right o wrong o theriyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="right o wrong o theriyadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">namaku natpa thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namaku natpa thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">epco vum kedaiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epco vum kedaiyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">parakkum rocketukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakkum rocketukku"/>
</div>
<div class="lyrico-lyrics-wrapper">passport ellam ketkadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="passport ellam ketkadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">life a live a paapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life a live a paapom"/>
</div>
<div class="lyrico-lyrics-wrapper">like o share o korayadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="like o share o korayadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pudhu malar vasanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu malar vasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">un meniyil perinbamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un meniyil perinbamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">unarnthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unarnthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sorry pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sorry pa"/>
</div>
<div class="lyrico-lyrics-wrapper">raava kudichuten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raava kudichuten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">cadbury kannula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cadbury kannula"/>
</div>
<div class="lyrico-lyrics-wrapper">robbery senjuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="robbery senjuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">un teguillavin bodhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un teguillavin bodhaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thikki mukki aakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thikki mukki aakuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna kanava?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kanava?"/>
</div>
<div class="lyrico-lyrics-wrapper">hey vaada kushiyoda than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vaada kushiyoda than"/>
</div>
<div class="lyrico-lyrics-wrapper">aatam paatam joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam paatam joru"/>
</div>
<div class="lyrico-lyrics-wrapper">ada poda meiyaalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada poda meiyaalume"/>
</div>
<div class="lyrico-lyrics-wrapper">majavaachu ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="majavaachu ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sketcha dhan pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sketcha dhan pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">thani route u kodi yeratum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thani route u kodi yeratum"/>
</div>
<div class="lyrico-lyrics-wrapper">soundayum kootu jathi maathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soundayum kootu jathi maathu "/>
</div>
<div class="lyrico-lyrics-wrapper">sruthi yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sruthi yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tulukkan gelippukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tulukkan gelippukku"/>
</div>
<div class="lyrico-lyrics-wrapper">inga damukkan pidilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga damukkan pidilu"/>
</div>
<div class="lyrico-lyrics-wrapper">jilipu jabarukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jilipu jabarukku"/>
</div>
<div class="lyrico-lyrics-wrapper">inga aralum bigilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga aralum bigilu"/>
</div>
<div class="lyrico-lyrics-wrapper">setaru matteru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="setaru matteru "/>
</div>
<div class="lyrico-lyrics-wrapper">petaru daughteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petaru daughteru"/>
</div>
<div class="lyrico-lyrics-wrapper">daputaru dipputaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daputaru dipputaru"/>
</div>
<div class="lyrico-lyrics-wrapper">dammutaru dapptaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dammutaru dapptaru"/>
</div>
<div class="lyrico-lyrics-wrapper">do do do do do do 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do do do do do do "/>
</div>
<div class="lyrico-lyrics-wrapper">domeru  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="domeru  "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bambaram pola suthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bambaram pola suthi"/>
</div>
<div class="lyrico-lyrics-wrapper">pasanga dhan patta kaththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasanga dhan patta kaththi"/>
</div>
<div class="lyrico-lyrics-wrapper">patha vei oodhuvathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha vei oodhuvathi"/>
</div>
<div class="lyrico-lyrics-wrapper">solli thaarom natpa pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli thaarom natpa pathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adada kudichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada kudichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">anubavangal nooracha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anubavangal nooracha"/>
</div>
<div class="lyrico-lyrics-wrapper">adichu pudichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adichu pudichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">natpukinga eedaachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpukinga eedaachaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey mama"/>
</div>
<div class="lyrico-lyrics-wrapper">feeling adhan oram katalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="feeling adhan oram katalama"/>
</div>
<div class="lyrico-lyrics-wrapper">ada rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada rama"/>
</div>
<div class="lyrico-lyrics-wrapper">ravoda dhan party panalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ravoda dhan party panalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">beat a dan yethi podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beat a dan yethi podu"/>
</div>
<div class="lyrico-lyrics-wrapper">kavalayum poyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalayum poyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">mixingum maari pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mixingum maari pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu cocktail party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu cocktail party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru cutting sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru cutting sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">maama maama maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama maama maama"/>
</div>
<div class="lyrico-lyrics-wrapper">company kooda vaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="company kooda vaama"/>
</div>
<div class="lyrico-lyrics-wrapper">inoru cutting sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inoru cutting sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">maama maama maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama maama maama"/>
</div>
<div class="lyrico-lyrics-wrapper">company kooda vaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="company kooda vaama"/>
</div>
</pre>
