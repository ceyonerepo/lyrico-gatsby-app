---
title: "neethan venumadi song lyrics"
album: "Dhanusu Raasi Neyargale"
artist: "Ghibran"
lyricist: "Vignesh Shivan"
director: "Sanjay Bharathi"
path: "/albums/dhanusu-raasi-neyargale-lyrics"
song: "Neethan Venumadi"
image: ../../images/albumart/dhanusu-raasi-neyargale.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/k4cB0YlXKNk"
type: "happy"
singers:
  - Sarath Santosh
  - Rajan Chelliah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nokku Varmaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nokku Varmaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nekkaa Vuttale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nekkaa Vuttale"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya Kadhalal Mayanga Vachaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Kadhalal Mayanga Vachaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Modern Mogathil Moozhga Vachaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modern Mogathil Moozhga Vachaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Old Style-il La Vetka Pada Vittale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Old Style-il La Vetka Pada Vittale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho Edho Solli Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Edho Solli Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Senjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Senjaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Regaiyil Kadhal Kootta Varainjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Regaiyil Kadhal Kootta Varainjaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Single Ah Irundha Vethunna Sonnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Ah Irundha Vethunna Sonnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Committed Status Gift Ah Vandhu Thanthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Committed Status Gift Ah Vandhu Thanthaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan Venumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Venumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Podhumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Podhumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Venumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Venumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Podhumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Podhumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eyes Ah Thoranthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyes Ah Thoranthaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Theriyanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Theriyanume"/>
</div>
<div class="lyrico-lyrics-wrapper">Nice Ah Enna Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nice Ah Enna Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa Nee Sirikkanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaa Nee Sirikkanume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathu Nolaiyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Nolaiyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hug Ah Start Panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hug Ah Start Panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Week End Varaikume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Week End Varaikume"/>
</div>
<div class="lyrico-lyrics-wrapper">Appadiye Irukkanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadiye Irukkanume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Souo Cup-il Potti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Souo Cup-il Potti"/>
</div>
<div class="lyrico-lyrics-wrapper">Olympics La Sekkanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olympics La Sekkanume"/>
</div>
<div class="lyrico-lyrics-wrapper">Gold Medal Vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gold Medal Vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorellaam Kaattanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorellaam Kaattanume"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Ah Paathi Oru Book Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Ah Paathi Oru Book Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Ezhuthanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Ezhuthanume"/>
</div>
<div class="lyrico-lyrics-wrapper">Book Fulla Un Perathaane Kirukkanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Book Fulla Un Perathaane Kirukkanume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan Venumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Venumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Podhumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Podhumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Venumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Venumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Podhumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Podhumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru Naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Naan Sevikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Naan Sevikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkooda Oora Suththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda Oora Suththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallaa Semikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallaa Semikkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Naan Irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Naan Irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Thedidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Thedidanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukullaa Unna Pootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukullaa Unna Pootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thookkidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thookkidanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho Edho Solli Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Edho Solli Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Senjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Senjaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Regaiyil Kadhal Kootta Varainjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Regaiyil Kadhal Kootta Varainjaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Single Ah Irundha Vethunna Sonnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Ah Irundha Vethunna Sonnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Committed Status Gift Ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Committed Status Gift Ah "/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Thanthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Thanthaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan Venumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Venumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Podhumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Podhumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Venumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Venumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Podhumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Podhumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho Edho Solli Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Edho Solli Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Senjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Senjaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Regaiyil Kadhal Kootta Varainjaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Regaiyil Kadhal Kootta Varainjaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Single Ah Irundha Vethunna Sonnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single Ah Irundha Vethunna Sonnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Committed Status Gift Ah Vandhu Thanthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Committed Status Gift Ah Vandhu Thanthaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan Venumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Venumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Podhumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Podhumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Venumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Venumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Podhumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Podhumadi"/>
</div>
</pre>
