---
title: "azhuku jatti amudhavalli song lyrics"
album: "Iruttu Araiyil Murattu Kuththu"
artist: "Balamurali Balu"
lyricist: "MC Vickey"
director: "Uday yadav"
path: "/albums/iruttu-araiyil-murattu-kuththu-song-lyrics"
song: "Azhuku Jatti Amudhavalli"
image: ../../images/albumart/iruttu-araiyil-murattu-kuththu.jpg
date: 2018-05-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Z6t2EpwtJxM"
type: "mass"
singers:
  - MC Vickey
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadi Kadi Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadi Kadi Adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Marubadi Adi Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marubadi Adi Adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Podi Mokka Monji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Podi Mokka Monji"/>
</div>
<div class="lyrico-lyrics-wrapper">Moosi Pona Kozhi Kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moosi Pona Kozhi Kari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudi Pudi Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudi Pudi Pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha Pudi Mudinja Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Pudi Mudinja Pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Adi Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Adi Adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhi Adi Kunjiadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi Adi Kunjiadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Mari Indha Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Mari Indha Mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Vecha Theri Theri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vecha Theri Theri"/>
</div>
<div class="lyrico-lyrics-wrapper">Putchi Kadi Putchi Kadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putchi Kadi Putchi Kadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja Putchikadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja Putchikadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Virgin Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virgin Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Saabam Summa Vidathudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saabam Summa Vidathudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Agni Kunja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agni Kunja"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu Saambal Aagatha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Saambal Aagatha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae Peayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Peayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naara Moonji Naaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naara Moonji Naaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja Guninji Mavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja Guninji Mavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Vechi Paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vechi Paaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhukku Jatti Amudhavalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhukku Jatti Amudhavalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru Anga Periya Balli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Anga Periya Balli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhukku Jatti Amudhavalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhukku Jatti Amudhavalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru Anga Periya Balli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Anga Periya Balli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniyaga Kanni Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaga Kanni Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Malayeara Vandhomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malayeara Vandhomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saniyana Pol Enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saniyana Pol Enga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Adichi Adichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Adichi Adichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooraikiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooraikiriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Danga Maari Pola Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danga Maari Pola Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatam Poda Vandhomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatam Poda Vandhomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veriyeari Oc Gaaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veriyeari Oc Gaaja"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaetu Kadupa Kalapuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaetu Kadupa Kalapuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pool Party Vechom Pussycata Theadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pool Party Vechom Pussycata Theadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Flashbackal Nenju Surunguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flashbackal Nenju Surunguthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Evil Deadaal En Seval Deadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Evil Deadaal En Seval Deadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Conjuring Moonji Kaanji Pochae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Conjuring Moonji Kaanji Pochae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhukku Jatti Amudhavalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhukku Jatti Amudhavalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru Anga Periya Balli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Anga Periya Balli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhukku Jatti Amudhavalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhukku Jatti Amudhavalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru Anga Periya Balli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Anga Periya Balli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadi Kadi Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadi Kadi Adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Marubadi Adi Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marubadi Adi Adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Podi Mokka Monji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Podi Mokka Monji"/>
</div>
<div class="lyrico-lyrics-wrapper">Moosi Pona Kozhi Kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moosi Pona Kozhi Kari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudi Pudi Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudi Pudi Pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha Pudi Mudinja Pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Pudi Mudinja Pudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Adi Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Adi Adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhi Adi Kunjiadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi Adi Kunjiadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Mari Indha Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Mari Indha Mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Vecha Theri Theri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vecha Theri Theri"/>
</div>
<div class="lyrico-lyrics-wrapper">Putchi Kadi Putchi Kadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putchi Kadi Putchi Kadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja Putchikadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja Putchikadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Virgin Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virgin Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Saabam Summa Vidathudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saabam Summa Vidathudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Agni Kunja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agni Kunja"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu Saambal Aagatha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Saambal Aagatha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae Peayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Peayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naara Moonji Naaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naara Moonji Naaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja Guninji Mavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja Guninji Mavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Vechi Paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vechi Paaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhukku Jatti Amudhavalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhukku Jatti Amudhavalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru Anga Periya Balli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Anga Periya Balli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhukku Jatti Amudhavalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhukku Jatti Amudhavalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru Anga Periya Balli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru Anga Periya Balli"/>
</div>
<div class="lyrico-lyrics-wrapper">Read more at Checklyrics Azhuku Jatti Amudhavalli Lyrics https//checklyrics.com/?p=5330
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Read more at Checklyrics Azhuku Jatti Amudhavalli Lyrics https//checklyrics.com/?p=5330"/>
</div>
</pre>
