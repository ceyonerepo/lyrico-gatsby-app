---
title: "kanne en kanne song lyrics"
album: "Endraavathu Oru Naal"
artist: "N.R. Raghunanthan"
lyricist: "Vairamuthu"
director: "Vetri Duraisamy"
path: "/albums/endraavathu-oru-naal-lyrics"
song: "Kanne en Kanne"
image: ../../images/albumart/endraavathu-oru-naal.jpg
date: 2021-10-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EYFMzFLIeJI"
type: "happy"
singers:
  - Vijay Yesudas
  - Saindhavi Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kanne en kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne en kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">un kannathil sevanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannathil sevanama"/>
</div>
<div class="lyrico-lyrics-wrapper">penne semponne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne semponne"/>
</div>
<div class="lyrico-lyrics-wrapper">athu thaimaiyin vannama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu thaimaiyin vannama"/>
</div>
<div class="lyrico-lyrics-wrapper">kanne en kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne en kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">un kannathil sevanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannathil sevanama"/>
</div>
<div class="lyrico-lyrics-wrapper">penne semponne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne semponne"/>
</div>
<div class="lyrico-lyrics-wrapper">athu thaimaiyin vannama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu thaimaiyin vannama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaasal vantha vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal vantha vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">ini valarpirai aagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini valarpirai aagume"/>
</div>
<div class="lyrico-lyrics-wrapper">nam aasai ennam polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam aasai ennam polave"/>
</div>
<div class="lyrico-lyrics-wrapper">oru pillai thondrume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru pillai thondrume"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum nanum thayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum nanum thayum"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthai aagum kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthai aagum kaalame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanne en kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne en kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">un kannathil sevanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannathil sevanama"/>
</div>
<div class="lyrico-lyrics-wrapper">penne semponne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne semponne"/>
</div>
<div class="lyrico-lyrics-wrapper">athu thaimaiyin vannama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu thaimaiyin vannama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pulithi kathu vaalkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulithi kathu vaalkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">katti thangam vanthathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti thangam vanthathe"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye en kodiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye en kodiye"/>
</div>
<div class="lyrico-lyrics-wrapper">oru parisu keladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru parisu keladi"/>
</div>
<div class="lyrico-lyrics-wrapper">madiyai enthi vanthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madiyai enthi vanthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">puthaiyal thantha samiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthaiyal thantha samiye"/>
</div>
<div class="lyrico-lyrics-wrapper">neeye en parisu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeye en parisu"/>
</div>
<div class="lyrico-lyrics-wrapper">ini puthusa venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini puthusa venuma"/>
</div>
<div class="lyrico-lyrics-wrapper">en aasai poongodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aasai poongodi"/>
</div>
<div class="lyrico-lyrics-wrapper">un jeevan rendadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un jeevan rendadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en pangu serthume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pangu serthume"/>
</div>
<div class="lyrico-lyrics-wrapper">nee unnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee unnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">sukku minjiya marunthu iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sukku minjiya marunthu iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai  katiyum uravu iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai  katiyum uravu iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">petha thaiyai pol nethan pakaum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petha thaiyai pol nethan pakaum"/>
</div>
<div class="lyrico-lyrics-wrapper">peru kaalam enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peru kaalam enaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thai paal ootum velaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thai paal ootum velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">tamilium serthu ootanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamilium serthu ootanum"/>
</div>
<div class="lyrico-lyrics-wrapper">oore kondada atha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore kondada atha "/>
</div>
<div class="lyrico-lyrics-wrapper">parthu rasikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parthu rasikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">veetu paadam polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetu paadam polave"/>
</div>
<div class="lyrico-lyrics-wrapper">maatu paadam padikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatu paadam padikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">kalvi vevasayam rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalvi vevasayam rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal aagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal aagume"/>
</div>
<div class="lyrico-lyrics-wrapper">aagayam oonjalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagayam oonjalil"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalati pakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalati pakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">oor aalum pillaiyai aalakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor aalum pillaiyai aalakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">un vayithil valaruthu oru vilaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vayithil valaruthu oru vilaku"/>
</div>
<div class="lyrico-lyrics-wrapper">vamsam valakura thiruvilaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vamsam valakura thiruvilaku"/>
</div>
<div class="lyrico-lyrics-wrapper">masam pathula porakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="masam pathula porakum"/>
</div>
<div class="lyrico-lyrics-wrapper">pullaku vaala kaathu iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullaku vaala kaathu iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanne en kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne en kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">un kannathil sevanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannathil sevanama"/>
</div>
<div class="lyrico-lyrics-wrapper">penne semponne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne semponne"/>
</div>
<div class="lyrico-lyrics-wrapper">athu thaimaiyin vannama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu thaimaiyin vannama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaasal vantha vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal vantha vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">ini valarpirai aagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini valarpirai aagume"/>
</div>
<div class="lyrico-lyrics-wrapper">nam aasai ennam polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam aasai ennam polave"/>
</div>
<div class="lyrico-lyrics-wrapper">oru pillai thondrume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru pillai thondrume"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum nanum thayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum nanum thayum"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthai aagum kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthai aagum kaalame"/>
</div>
</pre>
