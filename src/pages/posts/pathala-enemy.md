---
title: "pathala song lyrics"
album: "Enemy"
artist: "S. Thaman"
lyricist: "Arivu"
director: "Anand Shankar"
path: "/albums/enemy-lyrics"
song: "Pathala"
image: ../../images/albumart/enemy.jpg
date: 2021-11-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WO95KERe90U"
type: "love"
singers:
  - Deepak Blue
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae un paarvai pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae un paarvai pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaga vandhen yekkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaga vandhen yekkathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum unnai kanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum unnai kanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam konjam sandhoshathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam konjam sandhoshathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alava un pechu pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alava un pechu pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuninji nee paartha look-ula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninji nee paartha look-ula"/>
</div>
<div class="lyrico-lyrics-wrapper">Steady-ah kanna paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Steady-ah kanna paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa thaanae vaarthai pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa thaanae vaarthai pathala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee vandhalala thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vandhalala thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En thingalae ippa weekend aachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thingalae ippa weekend aachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan romba kaalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan romba kaalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga single man aachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga single man aachae"/>
</div>
<div class="lyrico-lyrics-wrapper">En rendu kaalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En rendu kaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkaiyanadhae unnai thedi poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkaiyanadhae unnai thedi poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal anju varama en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal anju varama en "/>
</div>
<div class="lyrico-lyrics-wrapper">night-um day achae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="night-um day achae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathala pathala pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala pathala pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend-ah nenacha pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend-ah nenacha pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathala pathala pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala pathala pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasu adha yethukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasu adha yethukkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathala pathala pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala pathala pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Alava siricha pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alava siricha pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathala pathala pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala pathala pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kedakkuren yekkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kedakkuren yekkathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae un paarvai pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae un paarvai pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaga vandhen yekkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaga vandhen yekkathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum unnai kanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum unnai kanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam konjam sandhoshathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam konjam sandhoshathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah aahaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah aahaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathala pathala pathala pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala pathala pathala pathala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathala pathala pathala pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala pathala pathala pathala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anuppum smiley thaan pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuppum smiley thaan pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Adukkum story thaan pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adukkum story thaan pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku status vaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku status vaikka"/>
</div>
4<div class="lyrico-lyrics-wrapper">G 5G edhuvum pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="G 5G edhuvum pathala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Follow pannaalum pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow pannaalum pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Solo aanaalum pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solo aanaalum pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Evlo lovvu-nu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evlo lovvu-nu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla kooda dhillum pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla kooda dhillum pathala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee vandhalala thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vandhalala thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En thingalae ippa weekend aache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thingalae ippa weekend aache"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan romba kaalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan romba kaalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga single man aachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga single man aachae"/>
</div>
<div class="lyrico-lyrics-wrapper">En rendu kaalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En rendu kaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkaiyanadhae unnai thedi poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkaiyanadhae unnai thedi poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal anju varama en 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal anju varama en "/>
</div>
<div class="lyrico-lyrics-wrapper">night-um day achae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="night-um day achae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathala pathala pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala pathala pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend-ah nenacha pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend-ah nenacha pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathala pathala pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala pathala pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasu adha yethukkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasu adha yethukkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathala pathala pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala pathala pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Alava siricha pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alava siricha pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathala pathala pathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala pathala pathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kedakkuren yekkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kedakkuren yekkathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
</pre>
