---
title: "arere aakasham song lyrics"
album: "Colour Photo"
artist: "Kaala Bhairava"
lyricist: "Kittu Vissapragada"
director: "Vijay Kumar Konda"
path: "/albums/colour-photo-lyrics"
song: "Arere Aakasham"
image: ../../images/albumart/colour-photo.jpg
date: 2020-10-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/7AEmCTjqrD4"
type: "love"
singers:
  - Kaala Bhairava
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Arere aaksam lonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere aaksam lonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ille kaduthunnava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ille kaduthunnava"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooridu kuda padaleni sota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooridu kuda padaleni sota"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangesinaadu thaladaasukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangesinaadu thaladaasukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana rropu thaane thega suskunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana rropu thaane thega suskunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa kitti gaadu paddadu thanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa kitti gaadu paddadu thanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Arere aaksam lonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere aaksam lonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ille kaduthunnana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ille kaduthunnana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Sitralahari paatantha thaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Sitralahari paatantha thaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Radio lo golanta nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radio lo golanta nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma kadhilelaa Gonthu kalisena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma kadhilelaa Gonthu kalisena"/>
</div>
<div class="lyrico-lyrics-wrapper">Touriungu talkiiesu Thera nuvvani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touriungu talkiiesu Thera nuvvani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenemo cut ayina ticket ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenemo cut ayina ticket ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana janta hit ayina cinema ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana janta hit ayina cinema ani"/>
</div>
<div class="lyrico-lyrics-wrapper">Abhimanule vachi sutharani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abhimanule vachi sutharani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalu reyantu ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalu reyantu ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalale kantu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalale kantu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanatho nunchunte chaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanatho nunchunte chaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Coloru photo lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coloru photo lona"/>
</div>
</pre>
