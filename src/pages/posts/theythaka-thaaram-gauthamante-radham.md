---
title: "theythaka song lyrics"
album: "Gauthamante Radham"
artist: "Ankit Menon - Anuraj O. B"
lyricist: "O. B. Anuraj"
director: "Anand Menon"
path: "/albums/gauthamante-radham-lyrics"
song: "Theythaka"
image: ../../images/albumart/gauthamante-radham.jpg
date: 2020-01-31
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/pzPu-6eC2Wg"
type: "happy"
singers:
  - Sooraj Santhosh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaa Theyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa Theyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkida Theyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkida Theyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharikida Theyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikida Theyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tahrikida Thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tahrikida Thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaa Theyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa Theyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkida Theyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkida Theyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharikida Thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikida Thaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaa Theyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa Theyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkida Theyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkida Theyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharikida Theyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikida Theyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tahrikida Thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tahrikida Thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaa Theyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa Theyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkida Theyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkida Theyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharikida Thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharikida Thaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mele Njaan Mele Keri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mele Njaan Mele Keri"/>
</div>
<div class="lyrico-lyrics-wrapper">Doorathetho Kaattinullil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doorathetho Kaattinullil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraarum Kaanadhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraarum Kaanadhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Paayum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarambham Thedaaniniyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarambham Thedaaniniyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedaaniniyum Dooram Ponam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedaaniniyum Dooram Ponam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaana Kadha Kelkkaniniyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana Kadha Kelkkaniniyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaryam Kaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaryam Kaanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mamayilukal Kaadukaleri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamayilukal Kaadukaleri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanathoru Malayum Keri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanathoru Malayum Keri"/>
</div>
<div class="lyrico-lyrics-wrapper">Aararum Kaanathengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aararum Kaanathengo"/>
</div>
<div class="lyrico-lyrics-wrapper">Doorathekko Poyoru Vattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doorathekko Poyoru Vattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iniyengan Kandavarundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyengan Kandavarundo"/>
</div>
<div class="lyrico-lyrics-wrapper">Iniyengan Kettavarundo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyengan Kettavarundo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthaney Yenthaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthaney Yenthaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthanu Enthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthanu Enthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthonnaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthonnaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">They Thakathaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They Thakathaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka Thaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Thaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">They Thakathaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They Thakathaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka Thaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Thaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">They Thakathaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They Thakathaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka Thaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Thaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Gauthamante Ee Avathaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gauthamante Ee Avathaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">They Thakathaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They Thakathaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka Thaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Thaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">They Thakathaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They Thakathaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka Thaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Thaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">They Thakathaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They Thakathaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaka Thaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaka Thaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Gauthamante Ee Avathaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gauthamante Ee Avathaaram"/>
</div>
</pre>
