---
title: "nee podhum kanna song lyrics"
album: "Kasada Thabara"
artist: "Yuvan Shankar Raja"
lyricist: "Gangai Amaran"
director: "Chimbudeven"
path: "/albums/kasada-thabara-lyrics"
song: "Nee Podhum Kanna"
image: ../../images/albumart/kasada-thabara.jpg
date: 2021-08-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/A6piUczr11M"
type: "happy"
singers:
  - Sean Roldan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nee pothum kanna enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pothum kanna enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalvu unnil iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalvu unnil iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee pothum kanna enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pothum kanna enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalvu unnil iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalvu unnil iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pon boomi ellam etharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pon boomi ellam etharku"/>
</div>
<div class="lyrico-lyrics-wrapper">unnodu ellam iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnodu ellam iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">magane unthan magil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magane unthan magil"/>
</div>
<div class="lyrico-lyrics-wrapper">ondru than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondru than"/>
</div>
<div class="lyrico-lyrics-wrapper">athu pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">manam nallathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam nallathu"/>
</div>
<div class="lyrico-lyrics-wrapper">gunam ullathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunam ullathu"/>
</div>
<div class="lyrico-lyrics-wrapper">athu pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">uravil uyarthu iru kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravil uyarthu iru kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">uyarntha nilaiyil iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyarntha nilaiyil iru"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaikum yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaikum yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">nadathi kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadathi kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee pothum kanna enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pothum kanna enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalvu unnil iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalvu unnil iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">pon boomi ellam etharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pon boomi ellam etharku"/>
</div>
<div class="lyrico-lyrics-wrapper">unnodu ellam iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnodu ellam iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enaku pillai nee endraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku pillai nee endraga"/>
</div>
<div class="lyrico-lyrics-wrapper">iraivan thantha theerpada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraivan thantha theerpada"/>
</div>
<div class="lyrico-lyrics-wrapper">enathu gunam nee kollamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathu gunam nee kollamal"/>
</div>
<div class="lyrico-lyrics-wrapper">evarkum vali kaatada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evarkum vali kaatada"/>
</div>
<div class="lyrico-lyrics-wrapper">ondrai iru nandrai iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondrai iru nandrai iru"/>
</div>
<div class="lyrico-lyrics-wrapper">or thunbam unai anugathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="or thunbam unai anugathu"/>
</div>
<div class="lyrico-lyrics-wrapper">anbai iru panbai iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbai iru panbai iru"/>
</div>
<div class="lyrico-lyrics-wrapper">thembai iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thembai iru"/>
</div>
<div class="lyrico-lyrics-wrapper">uravil uyarnthu iru kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravil uyarnthu iru kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">uyarntha nilaiyil iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyarntha nilaiyil iru"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaikum yavum nadathi katu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaikum yavum nadathi katu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee pothum kanna enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pothum kanna enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalvu unnil iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalvu unnil iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">pon boomi ellam etharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pon boomi ellam etharku"/>
</div>
<div class="lyrico-lyrics-wrapper">unnodu ellam iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnodu ellam iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">magane unthan magil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magane unthan magil"/>
</div>
<div class="lyrico-lyrics-wrapper">ondru than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondru than"/>
</div>
<div class="lyrico-lyrics-wrapper">athu pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu pothume"/>
</div>
</pre>
