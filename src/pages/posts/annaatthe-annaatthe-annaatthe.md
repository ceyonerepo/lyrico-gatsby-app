---
title: "annaatthe annaatthe song lyrics"
album: "Annaatthe"
artist: "D.Imman"
lyricist: "Viveka"
director: "Siva"
path: "/albums/annaatthe-lyrics"
song: "Annaatthe Annaatthe"
image: ../../images/albumart/annaatthe.jpg
date: 2021-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FK6dUrfYx84"
type: "mass"
singers:
  - S.P. Balasubrahmanyam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">gaantham kanakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaantham kanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna paru kanna paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna paru kanna paru"/>
</div>
<div class="lyrico-lyrics-wrapper">aale miduka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aale miduka "/>
</div>
<div class="lyrico-lyrics-wrapper">annan paru annan paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annan paru annan paru"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru poora thaarumaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru poora thaarumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">visilu parakka aaravarathodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="visilu parakka aaravarathodu"/>
</div>
<div class="lyrico-lyrics-wrapper">satham therika therika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham therika therika"/>
</div>
<div class="lyrico-lyrics-wrapper">veerathuku vera peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veerathuku vera peru"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalaiyannu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalaiyannu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri vaaga suda porom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri vaaga suda porom"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda senthu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda senthu nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annatha annatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha annatha"/>
</div>
<div class="lyrico-lyrics-wrapper">varen athiradi saravedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varen athiradi saravedi"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvengum veesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvengum veesu"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha annatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha annatha"/>
</div>
<div class="lyrico-lyrics-wrapper">varan nadaiyila udaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varan nadaiyila udaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">kola kola maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola kola maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">koondula puyaluku velaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koondula puyaluku velaiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">thandi va kadamaigal kathiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandi va kadamaigal kathiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">thoondila thimingalam mathipathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoondila thimingalam mathipathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">thuniji vaa kadavule thunai namaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuniji vaa kadavule thunai namaku"/>
</div>
<div class="lyrico-lyrics-wrapper">uruthiyudan modhu modhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruthiyudan modhu modhu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagaiye jeyikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagaiye jeyikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku inai yethu yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku inai yethu yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanaiyum valaikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanaiyum valaikalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annatha pesuna stailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha pesuna stailu"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha paduna stailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha paduna stailu"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha aaduna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha aaduna "/>
</div>
<div class="lyrico-lyrics-wrapper">kondattam kondattam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondattam kondattam than"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha pesuna stailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha pesuna stailu"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha paduna stailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha paduna stailu"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha aaduna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha aaduna "/>
</div>
<div class="lyrico-lyrics-wrapper">kondattam kondattam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondattam kondattam than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annatha annatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha annatha"/>
</div>
<div class="lyrico-lyrics-wrapper">varen athiradi saravedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varen athiradi saravedi"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvengum veesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvengum veesu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thalaiva thalaiva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaiva thalaiva "/>
</div>
<div class="lyrico-lyrics-wrapper">thalaiva thalaiva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaiva thalaiva "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">keeladiku pakathooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keeladiku pakathooru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaari tharum vaigai aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaari tharum vaigai aaru"/>
</div>
<div class="lyrico-lyrics-wrapper">nitham nitham nellu soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitham nitham nellu soru"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaladikum kalathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaladikum kalathula"/>
</div>
<div class="lyrico-lyrics-wrapper">thapi pona nellai alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thapi pona nellai alli"/>
</div>
<div class="lyrico-lyrics-wrapper">pasi aarum pathu ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasi aarum pathu ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">veeril veeram thangi ongi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeril veeram thangi ongi"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalum athisaya nilamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalum athisaya nilamada"/>
</div>
<div class="lyrico-lyrics-wrapper">maaril eeti vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaril eeti vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">poril mothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poril mothum"/>
</div>
<div class="lyrico-lyrics-wrapper">maratamil inamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maratamil inamada"/>
</div>
<div class="lyrico-lyrics-wrapper">paasakaraa nesakaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasakaraa nesakaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">velaikaaraa moolaikaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velaikaaraa moolaikaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">maayakaraa machakaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayakaraa machakaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">kavakaraa vayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavakaraa vayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalam valvil ponnanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam valvil ponnanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">athai kavanam vaithu muneridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai kavanam vaithu muneridu"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai migavum pollathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai migavum pollathathu"/>
</div>
<div class="lyrico-lyrics-wrapper">athan kathai thirugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athan kathai thirugi"/>
</div>
<div class="lyrico-lyrics-wrapper">karai sernthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karai sernthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaginil alagu ethu sollava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaginil alagu ethu sollava"/>
</div>
<div class="lyrico-lyrics-wrapper">ethirikum irangum gunamallava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethirikum irangum gunamallava"/>
</div>
<div class="lyrico-lyrics-wrapper">uyarthara veeram ethu sollava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyarthara veeram ethu sollava"/>
</div>
<div class="lyrico-lyrics-wrapper">suya thavaru unarum seyal allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suya thavaru unarum seyal allava"/>
</div>
<div class="lyrico-lyrics-wrapper">netriyila verva venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netriyila verva venum"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil nerma venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil nerma venum"/>
</div>
<div class="lyrico-lyrics-wrapper">mathathellam ennam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathathellam ennam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">thaana vanthu serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaana vanthu serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">viral pathu irukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral pathu irukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyala elupidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyala elupidu"/>
</div>
<div class="lyrico-lyrics-wrapper">etharkum nee anja kudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etharkum nee anja kudathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugal nadanthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugal nadanthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">athu varai valkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu varai valkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">nodiyum ne thunja koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nodiyum ne thunja koodathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annatha pesuna stailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha pesuna stailu"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha paduna stailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha paduna stailu"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha aaduna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha aaduna "/>
</div>
<div class="lyrico-lyrics-wrapper">kondattam kondattam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondattam kondattam than"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha pesuna stailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha pesuna stailu"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha paduna stailu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha paduna stailu"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha aaduna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha aaduna "/>
</div>
<div class="lyrico-lyrics-wrapper">kondattam kondattam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondattam kondattam than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annatha annatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha annatha"/>
</div>
<div class="lyrico-lyrics-wrapper">varen athiradi saravedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varen athiradi saravedi"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvengum veesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvengum veesu"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha annatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha annatha"/>
</div>
<div class="lyrico-lyrics-wrapper">varan nadaiyila udaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varan nadaiyila udaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">kola kola maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola kola maasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annatha maasu ke boss su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha maasu ke boss su"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha walking eh race su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha walking eh race su"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha mothuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha mothuna"/>
</div>
<div class="lyrico-lyrics-wrapper">patasu patasu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patasu patasu than"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha maasu ke boss su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha maasu ke boss su"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha walking eh race su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha walking eh race su"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha mothuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha mothuna"/>
</div>
<div class="lyrico-lyrics-wrapper">patasu patasu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patasu patasu than"/>
</div>
</pre>
