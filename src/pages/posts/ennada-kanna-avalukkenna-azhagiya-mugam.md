---
title: "ennada kanna song lyrics"
album: "Avalukkenna Azhagiya Mugam"
artist: "David Showrrn"
lyricist: "Vairamuthu"
director: "A Kesavan"
path: "/albums/avalukkenna-azhagiya-mugam-lyrics"
song: "Ennada Kanna"
image: ../../images/albumart/avalukkenna-azhagiya-mugam.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7IOgflEwxrU"
type: "love"
singers:
  - Chandralekha
  - Aravind Sreenivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ennada kanna yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennada kanna yen"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kanneer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kanneer"/>
</div>
<div class="lyrico-lyrics-wrapper">nan unthan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan unthan "/>
</div>
<div class="lyrico-lyrics-wrapper">thaai allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaai allava"/>
</div>
<div class="lyrico-lyrics-wrapper">aagayam aluthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagayam aluthal"/>
</div>
<div class="lyrico-lyrics-wrapper">sooriyan nanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sooriyan nanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kangal aluthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kangal aluthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">en kangal nanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kangal nanaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennada kanna yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennada kanna yen"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kanneer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kanneer"/>
</div>
<div class="lyrico-lyrics-wrapper">nan unthan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan unthan "/>
</div>
<div class="lyrico-lyrics-wrapper">thaai allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaai allava"/>
</div>
<div class="lyrico-lyrics-wrapper">aagayam aluthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagayam aluthal"/>
</div>
<div class="lyrico-lyrics-wrapper">sooriyan nanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sooriyan nanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kangal aluthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kangal aluthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">en kangal nanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kangal nanaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaaname thanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaname thanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiye annai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiye annai"/>
</div>
<div class="lyrico-lyrics-wrapper">saagatha uravu allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saagatha uravu allava"/>
</div>
<div class="lyrico-lyrics-wrapper">neye than pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neye than pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">inbamo kollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbamo kollai"/>
</div>
<div class="lyrico-lyrics-wrapper">niravoda uravada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niravoda uravada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pen endru sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen endru sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thin endra porul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thin endra porul"/>
</div>
<div class="lyrico-lyrics-wrapper">kaana vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaana vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kodi saaithu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi saaithu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">malar koiya vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malar koiya vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">madi saaithu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madi saaithu naan"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai kotha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai kotha vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennada kanna yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennada kanna yen"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kanneer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kanneer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai yenni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai yenni "/>
</div>
<div class="lyrico-lyrics-wrapper">nejam maruguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nejam maruguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">sudu veyilil vennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudu veyilil vennai"/>
</div>
<div class="lyrico-lyrics-wrapper">pola aavi uruguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola aavi uruguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul iruntha idathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul iruntha idathile"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal vanthu niranthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal vanthu niranthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal veru"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul veru illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul veru illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">arthamana vaalvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arthamana vaalvile"/>
</div>
<div class="lyrico-lyrics-wrapper">sirpamaana selviye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirpamaana selviye"/>
</div>
<div class="lyrico-lyrics-wrapper">oru vetridamaai vaalthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vetridamaai vaalthavan"/>
</div>
<div class="lyrico-lyrics-wrapper">vetriyaale niraigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetriyaale niraigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">mullile mulaithavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mullile mulaithavan"/>
</div>
<div class="lyrico-lyrics-wrapper">poovil madiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovil madiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">vaalthu varugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalthu varugiren"/>
</div>
</pre>
