---
title: "ithana naala yaarum song lyrics"
album: "Vanakkam Da Mappilei"
artist: "G V Prakash Kumar"
lyricist: "Snehan"
director: "M. Rajesh"
path: "/albums/vanakkam-da-mappilei-song-lyrics"
song: "Ithana Naala Yaarum"
image: ../../images/albumart/vanakkam-da-mappilei.jpg
date: 2021-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Yc_0z6TxRwc"
type: "Love"
singers:
  - GV Prakash Kumar
  - Prathima Dinesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">itthana naalaa yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itthana naalaa yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkalaiyaa unna thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkalaiyaa unna thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthathum thonuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthathum thonuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pichu thinna thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pichu thinna thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">varanju vechirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varanju vechirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">usura kodanju unna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura kodanju unna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">neranju nikkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neranju nikkuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">ippa enna panna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippa enna panna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai peiya kaatthirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai peiya kaatthirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">man vaasam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man vaasam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda asaivula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda asaivula"/>
</div>
<div class="lyrico-lyrics-wrapper">naan aavi aaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan aavi aaguren"/>
</div>
<div class="lyrico-lyrics-wrapper">engae naan ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engae naan ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethirila pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethirila pora"/>
</div>
<div class="lyrico-lyrics-wrapper">adi pora ava pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi pora ava pora"/>
</div>
<div class="lyrico-lyrics-wrapper">enna sozhatti pottu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna sozhatti pottu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthaa ava paarthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthaa ava paarthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna parakka vaikkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna parakka vaikkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathaa puyal kaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathaa puyal kaathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naa suthi alaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa suthi alaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">kadanthaa enna kadanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadanthaa enna kadanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan sethu ezhugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan sethu ezhugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthu eetti kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthu eetti kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthaama nee kollura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthaama nee kollura"/>
</div>
<div class="lyrico-lyrics-wrapper">puzhungarisi polathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puzhungarisi polathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">yendi enna mellura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yendi enna mellura"/>
</div>
<div class="lyrico-lyrics-wrapper">hey kirukku pudikka vaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey kirukku pudikka vaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaikkura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaikkura nee"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniyaa pulamburane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyaa pulamburane"/>
</div>
<div class="lyrico-lyrics-wrapper">rasikkura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasikkura nee"/>
</div>
<div class="lyrico-lyrics-wrapper">ethukku en kanava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethukku en kanava"/>
</div>
<div class="lyrico-lyrics-wrapper">erikkura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erikkura nee"/>
</div>
<div class="lyrico-lyrics-wrapper">bomma pola enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bomma pola enna"/>
</div>
<div class="lyrico-lyrics-wrapper">udaikkura nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udaikkura nee"/>
</div>
<div class="lyrico-lyrics-wrapper">onna vida ulagatthula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna vida ulagatthula"/>
</div>
<div class="lyrico-lyrics-wrapper">osanthathonnum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osanthathonnum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">oviya maa vaanathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oviya maa vaanathula"/>
</div>
<div class="lyrico-lyrics-wrapper">varanju vaippendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varanju vaippendi"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaattam unnathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaattam unnathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathalippathaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathalippathaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">adi pora ava pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi pora ava pora"/>
</div>
<div class="lyrico-lyrics-wrapper">enna sozhatti pottu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna sozhatti pottu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthaa ava paarthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthaa ava paarthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna parakka vaikkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna parakka vaikkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathaa puyal kaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathaa puyal kaathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naa suthi alaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa suthi alaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">kadanthaa enna kadanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadanthaa enna kadanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naa sethu ezhugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa sethu ezhugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">itthana naalaa yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itthana naalaa yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkalaiyaa unna thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkalaiyaa unna thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthathum thonuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthathum thonuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pichu thinna thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pichu thinna thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">varanju venchirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varanju venchirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">usura kodanju unna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura kodanju unna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">neranju nikkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neranju nikkuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">ippa enna panna naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippa enna panna naan"/>
</div>
<div class="lyrico-lyrics-wrapper">mazha peiya kaatthirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazha peiya kaatthirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">man vaasam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man vaasam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda asaivula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda asaivula"/>
</div>
<div class="lyrico-lyrics-wrapper">naan aavi aaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan aavi aaguren"/>
</div>
<div class="lyrico-lyrics-wrapper">engae naan ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engae naan ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethirila pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethirila pora"/>
</div>
<div class="lyrico-lyrics-wrapper">adi pora ava pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi pora ava pora"/>
</div>
<div class="lyrico-lyrics-wrapper">enna sozhatti pottu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna sozhatti pottu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthaa ava paarthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthaa ava paarthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna parakka vaikkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna parakka vaikkuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathaa puyal kaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathaa puyal kaathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naa suthi alaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa suthi alaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">kadanthaa enaa kadanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadanthaa enaa kadanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naa sethu ezhugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa sethu ezhugiren"/>
</div>
</pre>
