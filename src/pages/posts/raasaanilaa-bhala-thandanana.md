---
title: "raasaanilaa song lyrics"
album: "Bhala Thandanana"
artist: "Mani Sharma"
lyricist: "Shreemani"
director: "Chaitanya Dantuluri"
path: "/albums/bhala-thandanana-lyrics"
song: "Raasaanilaa"
image: ../../images/albumart/bhala-thandanana.jpg
date: 2022-05-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/gwKLevbZOjI"
type: "love"
singers:
  - Anurag Kulkarni
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raasaanilaa kanabadanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaanilaa kanabadanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinabadanee premalekhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinabadanee premalekhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusaanilaa kadhalanane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusaanilaa kadhalanane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalanane mounarekhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalanane mounarekhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasaanilaa kanabadanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasaanilaa kanabadanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinabadanee premalekhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinabadanee premalekhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ningiloni neerulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningiloni neerulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeti paina perulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeti paina perulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaraani premalekha raasaanila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaraani premalekha raasaanila"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetaleni veenalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetaleni veenalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maataraani paapalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maataraani paapalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhamaina mounarekha chusaanilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhamaina mounarekha chusaanilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pusthakamlo daachukunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pusthakamlo daachukunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Emalikanna gnyaapakamlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emalikanna gnyaapakamlaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu daataleni modhati kavithalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu daataleni modhati kavithalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Allukunna panjaraane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allukunna panjaraane"/>
</div>
<div class="lyrico-lyrics-wrapper">Theluthunna paavuramlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theluthunna paavuramlaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Geesukunna geethe daatalenelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geesukunna geethe daatalenelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanani naatho kalapagalige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanani naatho kalapagalige"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilupu edho theliyakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilupu edho theliyakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelupaleka nilupaleka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelupaleka nilupaleka"/>
</div>
<div class="lyrico-lyrics-wrapper">Alasipoyaa nikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alasipoyaa nikaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daachaanilaa nijamidhanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daachaanilaa nijamidhanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Rujuvidhanee choopalekhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rujuvidhanee choopalekhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheraanilaa iruvuridhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheraanilaa iruvuridhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Okati anee theerarekhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okati anee theerarekhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadiyedaari daarilo mouna baatasaarilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiyedaari daarilo mouna baatasaarilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vontaralle sagaane naaku nenee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vontaralle sagaane naaku nenee"/>
</div>
<div class="lyrico-lyrics-wrapper">Endamaavi gundelo manduthunna endane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endamaavi gundelo manduthunna endane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennelalle kaachaave neevu nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennelalle kaachaave neevu nanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalisi unna velalonaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisi unna velalonaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Veelukaani maatalanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veelukaani maatalanni "/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuthoti neede maatalaadanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuthoti neede maatalaadanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopiraagi aagamanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopiraagi aagamanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde chappu daaputhunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde chappu daaputhunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Daachukuna preme neeku choopanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daachukuna preme neeku choopanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thegina gaayam thagadhu anna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegina gaayam thagadhu anna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagipodhe mana kathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagipodhe mana kathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chivari kshaname madhurakaavyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chivari kshaname madhurakaavyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Premakepudu kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premakepudu kadhaa"/>
</div>
</pre>
