---
title: "ye vela chusano kaani song lyrics"
album: "Ye Mantram Vesave"
artist: "Abdus Samad"
lyricist: "Arun Vemuri"
director: "Shridhar Marri"
path: "/albums/ye-mantram-vesave-lyrics"
song: "Ye Vela Chusano Kaani"
image: ../../images/albumart/ye-mantram-vesave.jpg
date: 2018-03-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/5Cv-8lTyLWk"
type: "melody"
singers:
  -	Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey nuvvem maaya chesaavo gaanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nuvvem maaya chesaavo gaanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mare ooha lede chelee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mare ooha lede chelee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ade dhyaasa naa gunde nindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ade dhyaasa naa gunde nindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee unike teliyaalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee unike teliyaalee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey nuvvem mantram vesaavo kaanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nuvvem mantram vesaavo kaanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Marem gurutu raade chelee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marem gurutu raade chelee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oke roopu aakaasamantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oke roopu aakaasamantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala nijamai kalavaalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala nijamai kalavaalee"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai pongu aaraadhanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai pongu aaraadhanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dare raadu aavedanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dare raadu aavedanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne cherchu naa sodhanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne cherchu naa sodhanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey ninne vela choosaano kaanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ninne vela choosaano kaanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mare ooha lede chelee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mare ooha lede chelee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ade dhyaasa naa gunde nindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ade dhyaasa naa gunde nindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee unike teliyaalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee unike teliyaalee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Reppalaaga kaapaade jhaamuvelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppalaaga kaapaade jhaamuvelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaabilamma jaade kaanakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaabilamma jaade kaanakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Egurutondi manase jaari povake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egurutondi manase jaari povake"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishamaina jaage cheyakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishamaina jaage cheyakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi nela todai vetukutaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi nela todai vetukutaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey mugimpinka edaina kaanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mugimpinka edaina kaanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mare digulu lede chelee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mare digulu lede chelee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oke dhyaaname reyi pagaloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oke dhyaaname reyi pagaloo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee needai nilavaalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee needai nilavaalee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey nuvvem maaya chesaavo kaanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nuvvem maaya chesaavo kaanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mare ooha lede chelee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mare ooha lede chelee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ade dhyaasa naa gunde nindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ade dhyaasa naa gunde nindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee unike teliyaalee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee unike teliyaalee"/>
</div>
</pre>
