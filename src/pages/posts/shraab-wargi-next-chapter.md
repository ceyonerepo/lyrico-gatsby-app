---
title: "shraab wargi song lyrics"
album: "Next Chapter"
artist: "Desi Crew"
lyricist: "Narinder Baath"
director: "Nanni Gill"
path: "/albums/next-chapter-lyrics"
song: "Shraab Wargi"
image: ../../images/albumart/next-chapter.jpg
date: 2021-04-09
lang: pubjabi
youtubeLink: "https://www.youtube.com/embed/1u05VXZPnOw"
type: "Enjoy"
singers:
  - Dilpreet Dhillon
  - Gurlez Akhtar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Desi crew! Desi crew!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desi crew! Desi crew!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho mittran te dull gi shraab wargi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mittran te dull gi shraab wargi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho pair chakk ho jae na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho pair chakk ho jae na"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve tu bebe nu jawana jaake dede warning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve tu bebe nu jawana jaake dede warning"/>
</div>
<div class="lyrico-lyrics-wrapper">Munda patt ho jae na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munda patt ho jae na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho mittran te dull gi sharaab wargi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mittran te dull gi sharaab wargi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho pair chakk ho jae na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho pair chakk ho jae na"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve tu bebe nu jawana jaake dede warning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve tu bebe nu jawana jaake dede warning"/>
</div>
<div class="lyrico-lyrics-wrapper">Munda patt ho jae na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munda patt ho jae na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh chann thalle laa leya jattan de putt ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh chann thalle laa leya jattan de putt ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni patt tan shaukeen silky jehi gutt ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni patt tan shaukeen silky jehi gutt ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Oye aashiqan di toli isteefa de gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye aashiqan di toli isteefa de gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jadon faisle sunaye gabru di chup ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jadon faisle sunaye gabru di chup ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho darshan khaaye nala heavy ne jawana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho darshan khaaye nala heavy ne jawana"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinni vatt ho jae na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinni vatt ho jae na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho mittran te dull gi shraab wargi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mittran te dull gi shraab wargi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho pair chakk ho jae na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho pair chakk ho jae na"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve tu bebe nu jawana jaake dede warning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve tu bebe nu jawana jaake dede warning"/>
</div>
<div class="lyrico-lyrics-wrapper">Munda patt ho jae na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munda patt ho jae na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vailli bande fan meri billi ankh de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vailli bande fan meri billi ankh de"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatt rounda naal riffle'an raja ke rakhde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt rounda naal riffle'an raja ke rakhde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve laa leyan de double story chhak li
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve laa leyan de double story chhak li"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu suit jihna ton khareede half half lakh de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu suit jihna ton khareede half half lakh de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho baathan wale baath naal chhad de yarana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho baathan wale baath naal chhad de yarana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitte att ho jae na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitte att ho jae na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho mittran te dull gi sharaab wargi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mittran te dull gi sharaab wargi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho pair chakk ho jae na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho pair chakk ho jae na"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve tu bebe nu jawana jaake dede warning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve tu bebe nu jawana jaake dede warning"/>
</div>
<div class="lyrico-lyrics-wrapper">Munda patt ho jae na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munda patt ho jae na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ni got mera dhillon shaunk rakha gaun da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni got mera dhillon shaunk rakha gaun da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve chadheya junoon jatta tainu paun da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve chadheya junoon jatta tainu paun da"/>
</div>
<div class="lyrico-lyrics-wrapper">Baahar da trip koyi plan kariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baahar da trip koyi plan kariye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho aa jauga swaad zindagi jyon da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho aa jauga swaad zindagi jyon da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bahutan hawa vich uddna patang rangiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahutan hawa vich uddna patang rangiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni dor katt ho jae na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni dor katt ho jae na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho mittran te dull gi shraab wargi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mittran te dull gi shraab wargi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho pair chakk ho jae na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho pair chakk ho jae na"/>
</div>
<div class="lyrico-lyrics-wrapper">Ve tu bebe nu jawana jaake dede warning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ve tu bebe nu jawana jaake dede warning"/>
</div>
<div class="lyrico-lyrics-wrapper">Munda patt ho jae na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munda patt ho jae na"/>
</div>
</pre>
