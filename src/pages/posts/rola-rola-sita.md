---
title: "rola rola song lyrics"
album: "Sita"
artist: "Anoop Rubens"
lyricist: "Ramajogayya Sastry"
director: "Teja"
path: "/albums/sita-lyrics"
song: "Rola Rola"
image: ../../images/albumart/sita.jpg
date: 2019-05-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/bWrKl9kAMvw"
type: "happy"
singers:
  - Paroma Dasgupta
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">rola rola rola rola rola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rola rola rola rola rola"/>
</div>
<div class="lyrico-lyrics-wrapper">round the globe everu lere nala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="round the globe everu lere nala"/>
</div>
<div class="lyrico-lyrics-wrapper">chala chala chala chala chala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chala chala chala chala chala"/>
</div>
<div class="lyrico-lyrics-wrapper">nenante nakishta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenante nakishta"/>
</div>
<div class="lyrico-lyrics-wrapper">neninte princess la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neninte princess la"/>
</div>
<div class="lyrico-lyrics-wrapper">ozygen naaku currency ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ozygen naaku currency ante"/>
</div>
<div class="lyrico-lyrics-wrapper">upiroddanta adi naatho unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="upiroddanta adi naatho unte"/>
</div>
<div class="lyrico-lyrics-wrapper">russia rubles us dollars
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="russia rubles us dollars"/>
</div>
<div class="lyrico-lyrics-wrapper">london pounds saudi riyals
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="london pounds saudi riyals"/>
</div>
<div class="lyrico-lyrics-wrapper">uredaina peredaina money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uredaina peredaina money"/>
</div>
<div class="lyrico-lyrics-wrapper">makes many things
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makes many things"/>
</div>
<div class="lyrico-lyrics-wrapper">me hu madam cleopatra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="me hu madam cleopatra"/>
</div>
<div class="lyrico-lyrics-wrapper">naa attitude naa power anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa attitude naa power anta"/>
</div>
<div class="lyrico-lyrics-wrapper">number 1 nene prathi chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="number 1 nene prathi chota"/>
</div>
<div class="lyrico-lyrics-wrapper">adugeste gelychi thiralant
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugeste gelychi thiralant"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rola rola rola rola rola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rola rola rola rola rola"/>
</div>
<div class="lyrico-lyrics-wrapper">round the globe everu lere nala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="round the globe everu lere nala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh myatsu fisiksu anni utter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh myatsu fisiksu anni utter"/>
</div>
<div class="lyrico-lyrics-wrapper">non sense u anukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="non sense u anukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">oh full to naa laphu one and
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh full to naa laphu one and"/>
</div>
<div class="lyrico-lyrics-wrapper">only kamarse prathi put
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="only kamarse prathi put"/>
</div>
<div class="lyrico-lyrics-wrapper">hey cheti geetll ledule lakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey cheti geetll ledule lakku"/>
</div>
<div class="lyrico-lyrics-wrapper">bank balanse asalin naa kikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bank balanse asalin naa kikku"/>
</div>
<div class="lyrico-lyrics-wrapper">europe euro turkey lira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="europe euro turkey lira"/>
</div>
<div class="lyrico-lyrics-wrapper">bangla taka macawe pataca
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bangla taka macawe pataca"/>
</div>
<div class="lyrico-lyrics-wrapper">uredaina peredaina money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uredaina peredaina money"/>
</div>
<div class="lyrico-lyrics-wrapper">makes many things
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makes many things"/>
</div>
<div class="lyrico-lyrics-wrapper">me hu madam cleopatra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="me hu madam cleopatra"/>
</div>
<div class="lyrico-lyrics-wrapper">naa attitude naa power anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa attitude naa power anta"/>
</div>
<div class="lyrico-lyrics-wrapper">number 1 nene prathi chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="number 1 nene prathi chota"/>
</div>
<div class="lyrico-lyrics-wrapper">adugeste gelychi thiralant
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugeste gelychi thiralant"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">andni attuli andri kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andni attuli andri kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">chusela nenudali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusela nenudali"/>
</div>
<div class="lyrico-lyrics-wrapper">avevaru emina dont care
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avevaru emina dont care"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkaga nene gelvali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkaga nene gelvali"/>
</div>
<div class="lyrico-lyrics-wrapper">ye emoshanni pettukonant
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye emoshanni pettukonant"/>
</div>
<div class="lyrico-lyrics-wrapper">naa relationship dabbutonant
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa relationship dabbutonant"/>
</div>
<div class="lyrico-lyrics-wrapper">malaya ringiit kenya shilling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaya ringiit kenya shilling"/>
</div>
<div class="lyrico-lyrics-wrapper">normway krone japan yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="normway krone japan yen"/>
</div>
<div class="lyrico-lyrics-wrapper">uredaina peredaina money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uredaina peredaina money"/>
</div>
<div class="lyrico-lyrics-wrapper">makes many things
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makes many things"/>
</div>
<div class="lyrico-lyrics-wrapper">me hu madam cleopatra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="me hu madam cleopatra"/>
</div>
<div class="lyrico-lyrics-wrapper">naa attitude naa power anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa attitude naa power anta"/>
</div>
<div class="lyrico-lyrics-wrapper">number 1 nene prathi chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="number 1 nene prathi chota"/>
</div>
<div class="lyrico-lyrics-wrapper">adugeste gelychi thiralant
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugeste gelychi thiralant"/>
</div>
</pre>
