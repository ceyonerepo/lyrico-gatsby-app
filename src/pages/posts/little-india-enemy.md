---
title: "little india song lyrics"
album: "Enemy"
artist: "S. Thaman"
lyricist: "Arivu"
director: "Anand Shankar"
path: "/albums/enemy-lyrics"
song: "Little India"
image: ../../images/albumart/enemy.jpg
date: 2021-11-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HXFBhOnlPco"
type: "happy"
singers:
  - Sarath Santosh
  - Arivu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kootaali vaala munna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootaali vaala munna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kacheri polaam pinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kacheri polaam pinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaadi naala pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaadi naala pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma jenam podum gummaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma jenam podum gummaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattadam vaanam mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattadam vaanam mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattunom verva sotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattunom verva sotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattanam undakki dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattanam undakki dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechom inga ellorum vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechom inga ellorum vaazha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy eppovo vandhom inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy eppovo vandhom inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethana sondham anga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethana sondham anga"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthura bhoomikkudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthura bhoomikkudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkaratha senjom kaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkaratha senjom kaiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey akkara seemaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey akkara seemaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkira kappal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkira kappal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethana thunbam thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethana thunbam thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhom indha manna ponnakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhom indha manna ponnakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelaa povom vaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaa povom vaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa pangali nee singaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa pangali nee singaikkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaa povom vaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaa povom vaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee step ah podu semma style ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee step ah podu semma style ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kootaali vaala munna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootaali vaala munna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kacheri polaam pinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kacheri polaam pinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaadi naala pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaadi naala pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma jenam podum gummaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma jenam podum gummaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singapooriyan vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singapooriyan vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seenaga nirkindra sooriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seenaga nirkindra sooriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha seemai kattikkatha kalaiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha seemai kattikkatha kalaiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga thaerodum veethiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga thaerodum veethiku"/>
</div>
<div class="lyrico-lyrics-wrapper">Verivan sollisai paduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verivan sollisai paduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Macha nee kelu kelu engal sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macha nee kelu kelu engal sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Singapooru thamizh makkal valarppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singapooru thamizh makkal valarppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna serum bothu perum neruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna serum bothu perum neruppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Looking modernity
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Looking modernity"/>
</div>
<div class="lyrico-lyrics-wrapper">Machi kannai kavarnthitta thamizhachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi kannai kavarnthitta thamizhachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannin sarithiram thondi paru machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannin sarithiram thondi paru machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnum natchathirangalin ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnum natchathirangalin ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pangali mela mela mela yaeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangali mela mela mela yaeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Konadu mela thaalam vechu aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konadu mela thaalam vechu aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennaalum marakkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennaalum marakkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli kudutha panpaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli kudutha panpaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Railodum paadhaikkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Railodum paadhaikkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veiyiloda uzhachchom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veiyiloda uzhachchom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudiyerum nattukuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudiyerum nattukuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuralaga olichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuralaga olichom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravoda vaazhum edam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravoda vaazhum edam"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagana ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagana ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varungalam marakkathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varungalam marakkathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Velakethi vanangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakethi vanangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey eppovo vandhom inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eppovo vandhom inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethana sondham anga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethana sondham anga"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthura bhoomikku dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthura bhoomikku dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkaratha senjom kaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkaratha senjom kaiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey akkara seemiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey akkara seemiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkira kappal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkira kappal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethana thunbam thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethana thunbam thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhom indha manna ponnakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhom indha manna ponnakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelaa povom vaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaa povom vaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa pangali nee singaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa pangali nee singaikkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaa povom vaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaa povom vaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee step ah podu semma style ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee step ah podu semma style ah"/>
</div>
</pre>
