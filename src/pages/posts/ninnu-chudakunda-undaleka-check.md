---
title: "ninnu chudakunda song lyrics"
album: "Check"
artist: "Kalyani Malik"
lyricist: "Sri Mani"
director: "Chandra Sekhar Yeleti"
path: "/albums/check-lyrics"
song: "Ninnu Chudakunda Undaleka"
image: ../../images/albumart/check.jpg
date: 2021-02-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/8ntf5Ombxbs"
type: "love"
singers:
  - Haricharan
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ninnu chudakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaleka pothunnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaleka pothunnanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu chudakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaleka pothunnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaleka pothunnanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morning avvakamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morning avvakamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Velugultho vacchesthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velugultho vacchesthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Full moon lekundane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full moon lekundane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennello munchesthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennello munchesthanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Addulakinka check check
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addulakinka check check"/>
</div>
<div class="lyrico-lyrics-wrapper">Haddhulakinka check check
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haddhulakinka check check"/>
</div>
<div class="lyrico-lyrics-wrapper">Stop sign leni lokam lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stop sign leni lokam lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu chudakunda undaleka pothunnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudakunda undaleka pothunnanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu chudakunda undaleka pothunnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudakunda undaleka pothunnanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedhi maarani samayam aagani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedhi maarani samayam aagani"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojedho puttinchinaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojedho puttinchinaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne chudani nimisham undani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne chudani nimisham undani"/>
</div>
<div class="lyrico-lyrics-wrapper">Chotedho srushtinchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chotedho srushtinchana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanureppale musunchina teesunchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanureppale musunchina teesunchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Na kallake eh ganthaloo vesunchinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na kallake eh ganthaloo vesunchinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chupe oka lipi mounam inko lipi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chupe oka lipi mounam inko lipi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenni bhashalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenni bhashalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadiche koname niliche vainamoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiche koname niliche vainamoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenni varasalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenni varasalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh bhashalo ne palikino palikinchinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh bhashalo ne palikino palikinchinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prati matalo nee perune vinipinchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prati matalo nee perune vinipinchana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnu chudakunda undaleka pothunnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudakunda undaleka pothunnanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Morning avvakamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morning avvakamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Velugultho vacchesthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velugultho vacchesthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Full moon lekundane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full moon lekundane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennello munchesthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennello munchesthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Addulakinka check check
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addulakinka check check"/>
</div>
<div class="lyrico-lyrics-wrapper">Haddhulakinka check check
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haddhulakinka check check"/>
</div>
<div class="lyrico-lyrics-wrapper">Stop sign leni lokam lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stop sign leni lokam lona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnu chudakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaleka pothunnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaleka pothunnanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu chudakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaleka pothunnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaleka pothunnanu"/>
</div>
</pre>
