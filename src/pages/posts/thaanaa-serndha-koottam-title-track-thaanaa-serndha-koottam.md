---
title: "thaanaa serndha koottam title track song lyrics"
album: "Thaanaa Serndha Koottam"
artist: "Anirudh Ravichander"
lyricist: "Vignesh Shivan"
director: "Vignesh Shivan"
path: "/albums/thaanaa-serndha-koottam-lyrics"
song: "Thaanaa Serndha Koottam - Title Track"
image: ../../images/albumart/thaanaa-serndha-koottam.jpg
date: 2018-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qbx1dCI8tco"
type: "title track"
singers:
  - Anirudh Ravichander
  - Vignesh Shivan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vetti veerathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti veerathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Veena serndha kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena serndha kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Verasa aaduvom da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verasa aaduvom da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamoochi aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoochi aatam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera maari vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera maari vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedala pasanga kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedala pasanga kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thedi serndha kootam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thedi serndha kootam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaa serndha kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaa serndha kootam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey parakkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey parakkattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkattum parakkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkattum parakkattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam veliyae konjam parakkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam veliyae konjam parakkattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakattum marakattum marakattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakattum marakattum marakattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai maranthu sirikattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai maranthu sirikattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalangattum kolambattum kelambattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangattum kolambattum kelambattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethiri ellam nalla kadharattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethiri ellam nalla kadharattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakattum nadakattum nadakattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakattum nadakattum nadakattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachathu ellam nadakattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachathu ellam nadakattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhaigalum maarattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaigalum maarattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal koodattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal koodattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Palamurai thorthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palamurai thorthavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Orumurai vaazhatumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orumurai vaazhatumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyalgal neezhattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalgal neezhattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvugal moodattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvugal moodattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irrutinai thediyae vazhbhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irrutinai thediyae vazhbhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi ozhiyattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi ozhiyattumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi serthathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi serthathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Duw duw duw duw duw duw
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duw duw duw duw duw duw"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaa serndha kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaa serndha kootam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Duw duw duw duw duw duw
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duw duw duw duw duw duw"/>
</div>
<div class="lyrico-lyrics-wrapper">Duw duw duw duw duw duw
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duw duw duw duw duw duw"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudiyaathu nadakkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathu nadakkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yena endrum ennakoodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yena endrum ennakoodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakoodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakoodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu kidayaathu kidaikkaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu kidayaathu kidaikkaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevarum solla koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevarum solla koodathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theriyaathu puriyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaathu puriyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yena etharkkum thitta koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yena etharkkum thitta koodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam thotraalum thuvandaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam thotraalum thuvandaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvinilae nikka koodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvinilae nikka koodathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sari ethuvum thavarae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari ethuvum thavarae illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunivirku nigarae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunivirku nigarae illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuravaathavan uyirae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuravaathavan uyirae illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum ingu thaniyae illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum ingu thaniyae illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhaigalum maarattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaigalum maarattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal koodattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal koodattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Palamurai thorthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palamurai thorthavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Orumurai vaazhatumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orumurai vaazhatumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyalgal neezhattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalgal neezhattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvugal moodattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvugal moodattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irrutinai thediyae vazhbhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irrutinai thediyae vazhbhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi ozhiyattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi ozhiyattumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetti veerathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti veerathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Veena serndha kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena serndha kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Verasa aaduvom da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verasa aaduvom da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamoochi aatamaww
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoochi aatamaww"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera maari vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera maari vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedala pasanga kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedala pasanga kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi serndha kootam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi serndha kootam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaa serndha kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaa serndha kootam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey parakkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey parakkattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkattum parakkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkattum parakkattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam veliyae konjam parakkattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam veliyae konjam parakkattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakattum marakattum marakattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakattum marakattum marakattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai maranthu sirikattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai maranthu sirikattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalangattum kolambattum kelambattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangattum kolambattum kelambattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethiri ellam nalla kadharattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethiri ellam nalla kadharattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakattum nadakattum nadakattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakattum nadakattum nadakattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachathu ellam nadakattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachathu ellam nadakattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoooooooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooooooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoooooooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooooooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoooooooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooooooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoooooooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooooooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaa thaana thaana serndha kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaa thaana thaana serndha kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoooooooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooooooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoooooooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooooooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoooooooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooooooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoooooooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooooooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaa thaana thaana serndha kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaa thaana thaana serndha kootam"/>
</div>
</pre>
