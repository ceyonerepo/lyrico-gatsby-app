---
title: "star song lyrics"
album: "Nevalle Nenunna"
artist: "Siddarth Sadasivuni"
lyricist: "Krishna kanth"
director: "Saibaba. M"
path: "/albums/neevalle-nenunna-lyrics"
song: "Star"
image: ../../images/albumart/neevalle-nenunna.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/4B90uh4JY-M"
type: "love"
singers:
  - Navya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ulala Ulala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulala Ulala"/>
</div>
<div class="lyrico-lyrics-wrapper">Anesi Startu Cheyiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anesi Startu Cheyiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Epaina Eeazy Lera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epaina Eeazy Lera"/>
</div>
<div class="lyrico-lyrics-wrapper">come on do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="come on do"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulala Ulala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulala Ulala"/>
</div>
<div class="lyrico-lyrics-wrapper">Comforte Marichipora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comforte Marichipora"/>
</div>
<div class="lyrico-lyrics-wrapper">Goal Paina Focuse Cheyiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goal Paina Focuse Cheyiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Succsess Cheraddhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Succsess Cheraddhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Full Moonlo Missaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Moonlo Missaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Watar Drap Ayyela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watar Drap Ayyela"/>
</div>
<div class="lyrico-lyrics-wrapper">Trai Chesi Agoddu Ekkadinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trai Chesi Agoddu Ekkadinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Lifeu Desingnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Lifeu Desingnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangultho Nideenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangultho Nideenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramandhi Aa Screenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramandhi Aa Screenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaggakinka Dukudeeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaggakinka Dukudeeraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hero Acchu Kallo Telipora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Acchu Kallo Telipora"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero Nigantha Needhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Nigantha Needhiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvinkamarchipoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvinkamarchipoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninna Eedunna History
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna Eedunna History"/>
</div>
<div class="lyrico-lyrics-wrapper">Visiles Cheppava Vacche Victory
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visiles Cheppava Vacche Victory"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvesi Saagipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvesi Saagipo"/>
</div>
<div class="lyrico-lyrics-wrapper">Daachi Antunna Kashtame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daachi Antunna Kashtame"/>
</div>
<div class="lyrico-lyrics-wrapper">Sykalu Vaalava Allthebest Ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sykalu Vaalava Allthebest Ani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Movies Paties Realese Joorugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Movies Paties Realese Joorugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tvs Promos Selaveleduraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tvs Promos Selaveleduraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Runnings Shootings Selfies Tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Runnings Shootings Selfies Tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Holiday Telini Pegetho Dairy Nindi poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Holiday Telini Pegetho Dairy Nindi poye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hero Acchu Kallo Telipora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Acchu Kallo Telipora"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero Nigantha Needhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Nigantha Needhiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hero Acchu Kallo Telipora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Acchu Kallo Telipora"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero Nigantha Needhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Nigantha Needhiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvventha Cheppina Povu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvventha Cheppina Povu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Thipi Rojulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Thipi Rojulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethone Vundile Naa Futuree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethone Vundile Naa Futuree"/>
</div>
<div class="lyrico-lyrics-wrapper">Pai Paina Chusene Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pai Paina Chusene Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Roju Lokame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Roju Lokame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenento Telusugaa Neeku Mundaree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenento Telusugaa Neeku Mundaree"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Movies parties Eennenno Chesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Movies parties Eennenno Chesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa vente Nuvvundee Kshaname Verule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa vente Nuvvundee Kshaname Verule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalallo Chuseti Padala Tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalallo Chuseti Padala Tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello Rasiunna Dairy Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello Rasiunna Dairy Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vudhi Nee Okka Peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vudhi Nee Okka Peru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hero Acchu Kallo Telipora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Acchu Kallo Telipora"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero Nigantha Needhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Nigantha Needhiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hero Acchu Kallo Telipora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Acchu Kallo Telipora"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero Nigantha Needhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Nigantha Needhiraa"/>
</div>
</pre>
