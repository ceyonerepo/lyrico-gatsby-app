---
title: "yennayidhu song lyrics"
album: "Mouna Guru"
artist: "S Thaman"
lyricist: "unknown"
director: "Shantha Kumar"
path: "/albums/mouna-guru-lyrics"
song: "Yennayidhu"
image: ../../images/albumart/mouna-guru.jpg
date: 2011-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/E36GP-Hvr-U"
type: "love"
singers:
  - Rahul Nambiar
  - Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yennaidhu idhu enna thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennaidhu idhu enna thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasu ennidam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasu ennidam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam vandhu milungudhu sollai, yeno ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam vandhu milungudhu sollai, yeno ?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalirandum udan vara villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalirandum udan vara villai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un adhayum prindhida villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un adhayum prindhida villai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannimaigal urangida villai, yeno ?, yeno ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannimaigal urangida villai, yeno ?, yeno ?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan thanimaiyil ulaginil mudhalurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan thanimaiyil ulaginil mudhalurai"/>
</div>
<div class="lyrico-lyrics-wrapper">oru mugam adik kadi adik kadi varudha ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru mugam adik kadi adik kadi varudha ?"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan thalaiyanai uraigalil kanavennum poochedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan thalaiyanai uraigalil kanavennum poochedi"/>
</div>
<div class="lyrico-lyrics-wrapper">muthamittu pookalai tharudha ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthamittu pookalai tharudha ?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan nadai udai bhaavanai sindhanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan nadai udai bhaavanai sindhanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavilum pudhu pudhu maatrangal varudha ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavilum pudhu pudhu maatrangal varudha ?"/>
</div>
<div class="lyrico-lyrics-wrapper">oru chellamazhai pookal kannukulle vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru chellamazhai pookal kannukulle vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">vittu vittu vedhanaigal tharudha ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittu vittu vedhanaigal tharudha ?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu yeno ? oh ..Idhu yeno ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu yeno ? oh ..Idhu yeno ?"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennaidhuvo? Yennaidhuvo?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennaidhuvo? Yennaidhuvo?"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavendru ayyo puriyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavendru ayyo puriyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannimoodi neye unnai uttru paarthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannimoodi neye unnai uttru paarthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam sollaadha ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam sollaadha ?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennaidhuvo? Yennaidhuvo?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennaidhuvo? Yennaidhuvo?"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavendru ayyo puriyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavendru ayyo puriyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrum ondrum ondruserndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrum ondrum ondruserndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">ondraagum unmai puriyaadha?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondraagum unmai puriyaadha?"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno? yeno ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno? yeno ?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennaidhu idhu enna thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennaidhu idhu enna thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasu ennidam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasu ennidam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam vandhu milungudhu sollai, yeno ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam vandhu milungudhu sollai, yeno ?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram varum varai, en manadhukul nooru thee azhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram varum varai, en manadhukul nooru thee azhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu yedhanaalo ? ayyoooh ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu yedhanaalo ? ayyoooh ?"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvai orumurai nee paarthadhum saaral palamurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai orumurai nee paarthadhum saaral palamurai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu yedhanaalo ? oh ...oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu yedhanaalo ? oh ...oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ne orumurai paarthaal irudhyam uraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne orumurai paarthaal irudhyam uraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">idhuenna idhu yeno ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhuenna idhu yeno ?"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo ne marumurai paarthail irudhyam thudikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo ne marumurai paarthail irudhyam thudikum"/>
</div>
<div class="lyrico-lyrics-wrapper">idhuenna idhu yeno ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhuenna idhu yeno ?"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu yeno ?.... Ondraaga thozhaindhomo ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu yeno ?.... Ondraaga thozhaindhomo ?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennaidhuvo? Yennaidhuvo?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennaidhuvo? Yennaidhuvo?"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavendru ayyo puriyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavendru ayyo puriyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbi vandhu maationdaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbi vandhu maationdaal"/>
</div>
<div class="lyrico-lyrics-wrapper">mandhirathai ullam sollaaadha ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandhirathai ullam sollaaadha ?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennaidhuvo? Yennaidhuvo?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennaidhuvo? Yennaidhuvo?"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavendru ayyo theriyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavendru ayyo theriyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumba thirumba maatik kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumba thirumba maatik kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">thandhirathaal nejjam thullaadha?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandhirathaal nejjam thullaadha?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno? Yeno? yeno? yeno ?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno? Yeno? yeno? yeno ?"/>
</div>
</pre>
