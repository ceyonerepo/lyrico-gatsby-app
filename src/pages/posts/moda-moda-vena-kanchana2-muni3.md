---
title: "moda moda vena song lyrics"
album: "Kanchana2-Muni3"
artist: "S. Thaman - Leon James - C. Sathya"
lyricist: "Viveka"
director: "Raghava Lawrence"
path: "/albums/kanchana2-muni3-lyrics"
song: "Moda Moda Vena"
image: ../../images/albumart/kanchana2-muni3.jpg
date: 2015-04-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rV4S_76znts"
type: "Terror"
singers:
  - Master Sriman Roshan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Moda Moda Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moda Moda Vena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veda Vedakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veda Vedakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutti Thirinja Modaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutti Thirinja Modaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gada Gadavena Gadhi Kalangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gada Gadavena Gadhi Kalangi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayandhu Odi Ponaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayandhu Odi Ponaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadu Kaduvena Kobaththoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadu Kaduvena Kobaththoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumbi Thirumbi Vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbi Thirumbi Vandhaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pada Padavena Vedi Eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada Padavena Vedi Eduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koluthipodu Pattaasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthipodu Pattaasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Paal Kudicha Vaasamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Paal Kudicha Vaasamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neengidaadha Vayasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengidaadha Vayasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Paavam Paarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Paavam Paarka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marandhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallu Pola Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu Pola Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thulli Thulli Aaduna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli Thulli Aaduna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Kaalam Pochey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kaalam Pochey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Peththa Thaayikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Peththa Thaayikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkkai Irundu Pochey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai Irundu Pochey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thulladhey Thulladhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulladhey Thulladhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Romba Thulladhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Romba Thulladhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selladhey Selladhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selladhey Selladhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattam Ini Selladhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam Ini Selladhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pooda Dei Pooda Dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooda Dei Pooda Dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooru Kooraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooru Kooraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthi Unna Podaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthi Unna Podaporen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodala Uruvum Neram Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodala Uruvum Neram Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modamodavena Vedavedakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modamodavena Vedavedakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutti Thirinja Modaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutti Thirinja Modaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gada Gadavena Gadhi Kalangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gada Gadavena Gadhi Kalangi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayandhu Oda Poraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayandhu Oda Poraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadu Kaduvena Kobaththoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadu Kaduvena Kobaththoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumbi Thirumbi Vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbi Thirumbi Vandhaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pada Padavena Vedi Eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada Padavena Vedi Eduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koluthipodu Pattaasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthipodu Pattaasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttaaley Muttaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttaaley Muttaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moolai Illaa Muttaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolai Illaa Muttaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttaaley Muttaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttaaley Muttaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mosamaana Muttaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosamaana Muttaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada Dei Vaada Dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Dei Vaada Dei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaenu Koottula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenu Koottula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee Alli Pottuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Alli Pottuputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paapa Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapa Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paridhabam Paakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paridhabam Paakkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moda Moda Vena Veda Vedakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moda Moda Vena Veda Vedakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutti Thirinja Modaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutti Thirinja Modaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gada Gadavena Gadhi Kalangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gada Gadavena Gadhi Kalangi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayandhu Odi Ponaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayandhu Odi Ponaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadu Kaduvena Kobaththoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadu Kaduvena Kobaththoda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumbi Thirumbi Vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbi Thirumbi Vandhaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pada Padavena Vedi Eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada Padavena Vedi Eduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koluthipodu Pattasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthipodu Pattasa"/>
</div>
</pre>
