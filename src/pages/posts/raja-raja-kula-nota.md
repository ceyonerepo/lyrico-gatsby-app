---
title: "raja raja kula song lyrics"
album: "Nota"
artist: "Sam CS"
lyricist: "Madhan Karky"
director: "Anand Shankar"
path: "/albums/nota-lyrics"
song: "Raja Raja Kula"
image: ../../images/albumart/nota.jpg
date: 2018-10-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ph8fVdBk3hk"
type: "happy"
singers:
  - Anirudh Ravichander
  - Swagatha S Krishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaaa naaaa aaa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaa naaaa aaa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula raja raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula raja raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula raja raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula raja raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaa aaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaa aaaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula raja raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula raja raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaa aaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaa aaaa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodhai unthan melae raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai unthan melae raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgam unthan keezhae raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgam unthan keezhae raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Pengal unthan pinnae raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pengal unthan pinnae raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesam unthan munnae raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesam unthan munnae raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai aasai thurathuthu raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai aasai thurathuthu raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada solli thudikuthu raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada solli thudikuthu raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha innum izhukkuthu raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha innum izhukkuthu raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatchi unnai azhaikuthu raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatchi unnai azhaikuthu raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadai konjam nazhuvuthu raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadai konjam nazhuvuthu raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana nenjam magizhiluthu raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana nenjam magizhiluthu raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram aandu aaduga vendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram aandu aaduga vendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanai ondru podunga raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanai ondru podunga raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula raja raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula raja raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula raja raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula raja raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Garisa garisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garisa garisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisadhasanidhapamapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisadhasanidhapamapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Garisa garisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garisa garisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisadha gamapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisadha gamapa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Garisa garisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garisa garisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisadhasanidhapamapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisadhasanidhapamapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Garisa garisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garisa garisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisadha gamapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisadha gamapa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula raja raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula raja raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula raja raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula raja raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathula methakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathula methakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathathu adikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathathu adikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oduthu oduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oduthu oduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odam dam dam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odam dam dam"/>
</div>
<div class="lyrico-lyrics-wrapper">Odam oodattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odam oodattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan atha verukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan atha verukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethukka marukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethukka marukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeruthu yeruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeruthu yeruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kireedam dam dam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kireedam dam dam"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaram koodatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram koodatum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arasu oru puram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arasu oru puram"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagasam oru puram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagasam oru puram"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvinil kuzhambum raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvinil kuzhambum raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai oru puram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai oru puram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaigal oru puram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaigal oru puram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvena muzhikkum raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvena muzhikkum raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solvathu kelaa nenjudanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solvathu kelaa nenjudanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhthavanae intha raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhthavanae intha raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaigal ennum ambugalal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaigal ennum ambugalal"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhthavanae intha raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhthavanae intha raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingu ethanai perasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu ethanai perasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kedayam yenthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kedayam yenthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenthida yenthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthida yenthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambugal paayuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambugal paayuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaipayum raja rajan ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaipayum raja rajan ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Athi veera raja rajan ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athi veera raja rajan ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagana raja rajan ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagana raja rajan ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula rajan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula rajan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula raja raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula raja raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula raja raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula raja raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Garisagarisarigari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garisagarisarigari"/>
</div>
<div class="lyrico-lyrics-wrapper">Garisagarisarigari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garisagarisarigari"/>
</div>
<div class="lyrico-lyrics-wrapper">Garisa garisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garisa garisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisadhasanidhapamapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisadhasanidhapamapa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula raja raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula raja raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raja kula raja raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja kula raja raja"/>
</div>
</pre>
