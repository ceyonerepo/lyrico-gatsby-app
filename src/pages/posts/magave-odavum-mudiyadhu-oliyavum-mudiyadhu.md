---
title: 'magave song lyrics'
album: 'Odavum Mudiyadhu Oliyavum Mudiyadhu'
artist: 'Kaushik Krish'
lyricist: 'Ramesh Venkat'
director: 'Ramesh Venkat'
path: '/albums/odavum-mudiyadhu-oliyavum-mudiyadhu-song-lyrics'
song: 'Magave'
image: ../../images/albumart/odavum-mudiyadhu-oliyavum-mudiyadhu.jpg
date: 2020-06-19
lang: tamil
singers: 
- Shakthisree Gopalan
youtubeLink: "https://www.youtube.com/embed/AbBBxy7big0"
type: 'lullaby'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Magavae iniya naal idhu vaaraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Magavae iniya naal idhu vaaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimel pudhu vidiyal varum vaaraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Inimel pudhu vidiyal varum vaaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkkum mudhal naalil
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarkkum mudhal naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarumae pirappomae
<input type="checkbox" class="lyrico-select-lyric-line" value="Iruvarumae pirappomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irappomae oru araiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Irappomae oru araiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enadhu thamizh
<input type="checkbox" class="lyrico-select-lyric-line" value="Enadhu thamizh"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai valarkkum anbae vaaraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai valarkkum anbae vaaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanadhu anubavam
<input type="checkbox" class="lyrico-select-lyric-line" value="Avanadhu anubavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai anaikum vaaraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai anaikum vaaraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enadhu thamizh
<input type="checkbox" class="lyrico-select-lyric-line" value="Enadhu thamizh"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai valarkkum anbae vaaraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai valarkkum anbae vaaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanadhu anubavam
<input type="checkbox" class="lyrico-select-lyric-line" value="Avanadhu anubavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai anaikum vaaraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai anaikum vaaraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vizhi oram eeram illaamal
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhi oram eeram illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhoram baaram illaamal
<input type="checkbox" class="lyrico-select-lyric-line" value="Manadhoram baaram illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaappenae anaippenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaappenae anaippenae"/>
</div>
<div class="lyrico-lyrics-wrapper">En maarbin naduvinilae
<input type="checkbox" class="lyrico-select-lyric-line" value="En maarbin naduvinilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaazhkaiyin oottam nindraalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhkaiyin oottam nindraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagamae edhirthu nindraalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulagamae edhirthu nindraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpirukkum adhu thaangum
<input type="checkbox" class="lyrico-select-lyric-line" value="Natpirukkum adhu thaangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh kodukkum ennaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thozh kodukkum ennaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Iraiyae iraiyae endru kaigalai
<input type="checkbox" class="lyrico-select-lyric-line" value="Iraiyae iraiyae endru kaigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendhi sendru nillu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yendhi sendru nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanum illai endraal
<input type="checkbox" class="lyrico-select-lyric-line" value="Avanum illai endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha iraiviyai thaedi vandhu nillu
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha iraiviyai thaedi vandhu nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idhu nilavin madi vandhu thoongada
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu nilavin madi vandhu thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu thoongada
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandhu thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini edhuvum kadandhu pogum thoongada
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini edhuvum kadandhu pogum thoongada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongada…
<input type="checkbox" class="lyrico-select-lyric-line" value="Thoongada…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Magavae iniya naal idhu vaaraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Magavae iniya naal idhu vaaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimel pudhu vidiyal varum vaaraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Inimel pudhu vidiyal varum vaaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkka vazhi illaiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarkka vazhi illaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarumae pirappomae
<input type="checkbox" class="lyrico-select-lyric-line" value="Iruvarumae pirappomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaivomae maru ulagil
<input type="checkbox" class="lyrico-select-lyric-line" value="Inaivomae maru ulagil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pirandhenae irandhenae oru araiyal
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan pirandhenae irandhenae oru araiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pirakkiraai irakkiraai karuvaraiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee pirakkiraai irakkiraai karuvaraiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pirakkiraai irakkiraai en karuvaraiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee pirakkiraai irakkiraai en karuvaraiyil"/>
</div>
</pre>