---
title: 'breakup song lyrics'
album: 'Naan Sirithal'
artist: 'Hiphop Tamizha'
lyricist: 'Hiphop Tamizha'
director: 'Raana'
path: '/albums/naan-sirithal-song-lyrics'
song: 'Breakup'
image: ../../images/albumart/naan-sirithal.jpg  
date: 2019-07-19
lang: tamil
singers: 
- Hiphop Tamizha
youtubeLink: "https://www.youtube.com/embed/lxROTQJEB58"
type: 'love failure'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Ennakku breakup
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennakku breakup"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula en thappu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhula en thappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum illainaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhuvum illainaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna villanaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna villanaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku shock adichu
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku shock adichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula heart veduchu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhula heart veduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karugi poiruchu
<input type="checkbox" class="lyrico-select-lyric-line" value="Karugi poiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poda goyyalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Poda goyyalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enna poga sonnalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna poga sonnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi saaga sonnalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Poi saaga sonnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sogathula paadurendi
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan sogathula paadurendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami munnalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Saami munnalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enna poga sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna poga sonnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naandukittu saaga sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Naandukittu saaga sonnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Lavala illa pullainga irukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Lavala illa pullainga irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan pinnaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Endhan pinnaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Valikutha valikutha nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Valikutha valikutha nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava single-nu status maathi potutaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava single-nu status maathi potutaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogamaa irukuthae thaanga mudila
<input type="checkbox" class="lyrico-select-lyric-line" value="Sogamaa irukuthae thaanga mudila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava wallpaper la en moonji-ya thookitaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava wallpaper la en moonji-ya thookitaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Breakup… enakku breakupppuu
<input type="checkbox" class="lyrico-select-lyric-line" value="Breakup… enakku breakupppuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Breakup… enakku breakupppuu
<input type="checkbox" class="lyrico-select-lyric-line" value="Breakup… enakku breakupppuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En sirippu pirachana
<input type="checkbox" class="lyrico-select-lyric-line" value="En sirippu pirachana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichu mudichona
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirichu mudichona"/>
</div>
<div class="lyrico-lyrics-wrapper">Aluga mudiyala sirichiten
<input type="checkbox" class="lyrico-select-lyric-line" value="Aluga mudiyala sirichiten"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-la pirachana life-la pirachana
<input type="checkbox" class="lyrico-select-lyric-line" value="Love-la pirachana life-la pirachana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan adakka mudiyala sirichiten
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan adakka mudiyala sirichiten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sondha pirachana soga pirachana
<input type="checkbox" class="lyrico-select-lyric-line" value="Sondha pirachana soga pirachana"/>
</div>
<div class="lyrico-lyrics-wrapper">Markka mudiyala marachitean
<input type="checkbox" class="lyrico-select-lyric-line" value="Markka mudiyala marachitean"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirachana pirachana ethuna pirachana
<input type="checkbox" class="lyrico-select-lyric-line" value="Pirachana pirachana ethuna pirachana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirchi sirichi therichiten
<input type="checkbox" class="lyrico-select-lyric-line" value="Sirchi sirichi therichiten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaadhala marakka ninachu sirikiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhala marakka ninachu sirikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhali mugatha nenachu sirikiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhali mugatha nenachu sirikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogathil life ah nenachu sirikiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Sogathil life ah nenachu sirikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kovatha adakka mudiyala sirikiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan kovatha adakka mudiyala sirikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Valikutha valikutha nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Valikutha valikutha nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava single-nu status maathi potutaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava single-nu status maathi potutaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogamaa irukuthae thaanga mudila
<input type="checkbox" class="lyrico-select-lyric-line" value="Sogamaa irukuthae thaanga mudila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava wallpaper la en moonji-ya thookitaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava wallpaper la en moonji-ya thookitaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Breakup… enakku breakupppuu
<input type="checkbox" class="lyrico-select-lyric-line" value="Breakup… enakku breakupppuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Breakup… enakku breakupppuu
<input type="checkbox" class="lyrico-select-lyric-line" value="Breakup… enakku breakupppuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Love-u pichikichu
<input type="checkbox" class="lyrico-select-lyric-line" value="Love-u pichikichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-u puttukichu
<input type="checkbox" class="lyrico-select-lyric-line" value="Life-u puttukichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manda pichikichu mental aachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Manda pichikichu mental aachu"/>
</div>
</pre>