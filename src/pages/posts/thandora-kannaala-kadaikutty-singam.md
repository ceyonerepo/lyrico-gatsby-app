---
title: "thandora kannaala song lyrics"
album: "Kadaikutty Singam"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "Pandiraj"
path: "/albums/kadaikutty-singam-lyrics"
song: "Thandora Kannaala"
image: ../../images/albumart/kadaikutty-singam.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/J1QMVBGuhZs"
type: "love"
singers:
  - VV Prasanna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thandora kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandora kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithamum padicha aaraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithamum padicha aaraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundaasa en vazhva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundaasa en vazhva"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattida varum nee poondhaero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattida varum nee poondhaero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanai mora naan paathalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai mora naan paathalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathala yena pesum vaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala yena pesum vaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkarai mazhai pol un pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkarai mazhai pol un pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottida tholavachae paayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottida tholavachae paayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullanthidukuthae ullatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullanthidukuthae ullatha "/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu aaraayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu aaraayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandora kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandora kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithamum padicha aaraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithamum padicha aaraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundaasa en vazhva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundaasa en vazhva"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattida varum nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattida varum nee "/>
</div>
<div class="lyrico-lyrics-wrapper">poondhaero oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poondhaero oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottu vecha ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu vecha ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi adikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi adikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta muthu kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta muthu kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam katti ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam katti ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kummi adikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kummi adikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu rendil onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu rendil onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kizhakalathanae varum suriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhakalathanae varum suriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena naanum kooda ninachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena naanum kooda ninachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Egathalum pesum magarasi un mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egathalum pesum magarasi un mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarka thoongi muzhichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarka thoongi muzhichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennavo podi unnaiyethaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavo podi unnaiyethaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekka mulaikuthadi nenju parakuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekka mulaikuthadi nenju parakuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandora kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandora kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithamum padicha aaraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithamum padicha aaraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundaasa en vazhva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundaasa en vazhva"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattida varum nee poondhaero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattida varum nee poondhaero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senga kallum unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senga kallum unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda nodiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda nodiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga kalla minna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga kalla minna"/>
</div>
<div class="lyrico-lyrics-wrapper">Rotti thunda enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rotti thunda enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna kuzhiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna kuzhiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Mennu nalla thinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mennu nalla thinna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puli mela paaya thuninjaalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli mela paaya thuninjaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meralatha aala kavutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meralatha aala kavutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Siripaala yendi mayil thogaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siripaala yendi mayil thogaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyar koodaiyaatam izhutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyar koodaiyaatam izhutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vallalla vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallalla vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alliyae podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alliyae podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandi nodikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandi nodikuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nondi adikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nondi adikuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandora kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandora kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithamum padicha aaraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithamum padicha aaraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundaasa en vazhva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundaasa en vazhva"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattida varum nee poondhaero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattida varum nee poondhaero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanai mora naan paathalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai mora naan paathalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathala yena pesum vaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathala yena pesum vaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkarai mazhai pol un pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkarai mazhai pol un pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottida tholavachae paayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottida tholavachae paayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullanthidukuthae ullatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullanthidukuthae ullatha "/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu aaraayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu aaraayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandora"/>
</div>
</pre>
