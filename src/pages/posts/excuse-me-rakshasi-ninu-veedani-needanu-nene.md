---
title: "excuse me rakshasi song lyrics"
album: "Ninu Veedani Needanu Nene"
artist: "S. Thaman"
lyricist: "Samrat"
director: "Caarthick Raju"
path: "/albums/ninu-veedani-needanu-nene-lyrics"
song: "Excuse Me Rakshasi"
image: ../../images/albumart/ninu-veedani-needanu-nene.jpg
date: 2019-07-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/8XXOfQ85iDk"
type: "love"
singers:
  - Siddharth
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vinave Rakshasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinave Rakshasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakenduke Preyasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakenduke Preyasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hello I’m Sorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello I’m Sorry"/>
</div>
<div class="lyrico-lyrics-wrapper">Manninchave Nannisaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manninchave Nannisaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo Padhamari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Padhamari"/>
</div>
<div class="lyrico-lyrics-wrapper">Telisindhega Mana Story
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telisindhega Mana Story"/>
</div>
<div class="lyrico-lyrics-wrapper">Kopam Yendhuke O Sukumari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopam Yendhuke O Sukumari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothaga Marchaave Naa Dhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothaga Marchaave Naa Dhaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Premaga Thiduthune Prathisaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premaga Thiduthune Prathisaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuni Dochave Vayyari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuni Dochave Vayyari"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekai Adugulu Paruguga Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekai Adugulu Paruguga Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Marichaane Ninukori…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Marichaane Ninukori…"/>
</div>
<div class="lyrico-lyrics-wrapper">Excuse Me Rakshasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Excuse Me Rakshasi"/>
</div>
<div class="lyrico-lyrics-wrapper">You My Fantasy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You My Fantasy"/>
</div>
<div class="lyrico-lyrics-wrapper">O Naa Preyasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Naa Preyasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvena Extasy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvena Extasy"/>
</div>
<div class="lyrico-lyrics-wrapper">Excuse Me Rakshasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Excuse Me Rakshasi"/>
</div>
<div class="lyrico-lyrics-wrapper">You My Fantasy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You My Fantasy"/>
</div>
<div class="lyrico-lyrics-wrapper">O Naa Preyasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Naa Preyasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvena Extasy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvena Extasy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hello I’m Sorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello I’m Sorry"/>
</div>
<div class="lyrico-lyrics-wrapper">Manninchave Nannisaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manninchave Nannisaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalo Padhamari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalo Padhamari"/>
</div>
<div class="lyrico-lyrics-wrapper">Telisindhega Mana Story
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telisindhega Mana Story"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okko Kshanam Naako Varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okko Kshanam Naako Varam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Maaya Chesindhevvare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maaya Chesindhevvare"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Kshanam O Sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Kshanam O Sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Haayi Peru Nuvvule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Haayi Peru Nuvvule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallolam Nee Muddhu Pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallolam Nee Muddhu Pera"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundelona Gola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundelona Gola "/>
</div>
<div class="lyrico-lyrics-wrapper">Gola Chesinaavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gola Chesinaavule"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurrollam Kaastha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurrollam Kaastha "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanikarinchave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanikarinchave"/>
</div>
<div class="lyrico-lyrics-wrapper">O Muddhu Kosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Muddhu Kosam "/>
</div>
<div class="lyrico-lyrics-wrapper">Yuddhamendhukey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuddhamendhukey"/>
</div>
<div class="lyrico-lyrics-wrapper">Excuse Me Rakshasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Excuse Me Rakshasi"/>
</div>
<div class="lyrico-lyrics-wrapper">You My Fantasy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You My Fantasy"/>
</div>
<div class="lyrico-lyrics-wrapper">O Naa Preyasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Naa Preyasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvena Extasy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvena Extasy"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Naa Fantasy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Naa Fantasy"/>
</div>
<div class="lyrico-lyrics-wrapper">O Naa Preyasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Naa Preyasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvena Extasy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvena Extasy"/>
</div>
</pre>
