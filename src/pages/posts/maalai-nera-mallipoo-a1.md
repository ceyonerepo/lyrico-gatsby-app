---
title: "maalai nera mallipoo song lyrics"
album: "A1"
artist: "Santhosh Narayanan"
lyricist: "GKB"
director: "Johnson K"
path: "/albums/a1-lyrics"
song: "Maalai Nera Mallipoo"
image: ../../images/albumart/a1.jpg
date: 2019-07-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Q46bWgZMjcc"
type: "love"
singers:
  - Chinna
  - Arunraja Kamaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maalai Nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Nera"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallippu Mallippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallippu Mallippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Nera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga Thimi Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga Thimi Thom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai Nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Nera"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallippu Mallippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallippu Mallippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Nera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga Thimi Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga Thimi Thom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudavai Vaasathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudavai Vaasathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mohana Rangathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohana Rangathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikki Thavikirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki Thavikirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Mookkuthi Ozhiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mookkuthi Ozhiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathiyin Aattathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathiyin Aattathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittam Podurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittam Podurene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan Pidichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan Pidichirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaama Neruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaama Neruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Nera"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidichirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchchi Narambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi Narambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallippu Mallippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallippu Mallippu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai Nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Nera"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallippu Mallippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallippu Mallippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Nera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puttukku Neiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttukku Neiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotukka Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotukka Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallathor Suvaithane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallathor Suvaithane"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattadhu Theeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattadhu Theeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttathu Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttathu Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya Kalaidhaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Kalaidhaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai Nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Nera"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Nera"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallippu Mallippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallippu Mallippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Nera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaga Thimi Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga Thimi Thom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotta Mela Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Mela Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta Mela Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Mela Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotti Kidakku Muththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotti Kidakku Muththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta Sinungi Di Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Sinungi Di Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesi Thavikka Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesi Thavikka Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyira Thudikk Avittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyira Thudikk Avittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sittaa Parandha Di Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittaa Parandha Di Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laali Laali Subha Laali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laali Laali Subha Laali"/>
</div>
<div class="lyrico-lyrics-wrapper">Laali Laali Yendi Veli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laali Laali Yendi Veli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan Pidichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan Pidichirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaama Neruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaama Neruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Nera"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidichirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchchi Narambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi Narambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pidichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidichirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaama Neruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaama Neruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Nera"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidichirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchchi Narambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi Narambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pidichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidichirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaama Neruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaama Neruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Nera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Nera"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidichirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchchi Narambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi Narambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaga Thimi Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaga Thimi Thom"/>
</div>
</pre>
