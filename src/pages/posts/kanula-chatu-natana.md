---
title: "kanula chatu song lyrics"
album: "Natana"
artist: "Prabhu Praveen Lanka"
lyricist: "Bharati Babu Penupatruni"
director: "Bharati Babu Penupatruni"
path: "/albums/natana-lyrics"
song: "Kanula Chatu"
image: ../../images/albumart/natana.jpg
date: 2019-01-04
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ro4lDm7gk7w"
type: "love"
singers:
  - Saicharan Bhaskaruni
  - Malavika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanula Chatu Kalalu Tesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanula Chatu Kalalu Tesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavitha Allukunna Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavitha Allukunna Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha Ahha Ahha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha Ahha Ahha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pedavi Maata Matu Tessi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi Maata Matu Tessi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paata Rasukunna Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paata Rasukunna Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahaha Haha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahaha Haha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallvinchuthundi mellaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallvinchuthundi mellaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Parimalala Poola challaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Parimalala Poola challaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Theta Telugu Tanamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theta Telugu Tanamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi Tenaluru Veramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi Tenaluru Veramai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siri poola paina Bhanamudhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siri poola paina Bhanamudhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bramaramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bramaramai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanula Chatu Kalalu Tesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanula Chatu Kalalu Tesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavitha Allukunna Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavitha Allukunna Vela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pedavi Maata Matu Tessi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi Maata Matu Tessi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paata Rasikunna Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paata Rasikunna Vela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogasu Rendu Pushkaralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogasu Rendu Pushkaralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Datuthunnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Datuthunnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu Nindi Pushkalamga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu Nindi Pushkalamga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Ninda Avasaralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Ninda Avasaralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninpukunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninpukunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarasu Donda Parisaralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarasu Donda Parisaralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimpamannadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimpamannadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanuke Modhalavuthu Undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanuke Modhalavuthu Undhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedha Venuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedha Venuka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanuke Sega Thaguluthondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanuke Sega Thaguluthondi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi Molaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Molaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvula Thalapulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvula Thalapulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera Teyanandhi Ipude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera Teyanandhi Ipude"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuvana Gelupuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuvana Gelupuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijayosthavalu Appude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijayosthavalu Appude"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanula Chatu Kalalu Tesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanula Chatu Kalalu Tesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavitha Allukunna Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavitha Allukunna Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha Ahha Ahha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha Ahha Ahha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pedavi Maata Matu Tessi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi Maata Matu Tessi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paata Rasikunna Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paata Rasikunna Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahaha Haha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahaha Haha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanti Choopu Laaguthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanti Choopu Laaguthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhara Pushpamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhara Pushpamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panti Gaatu Koruthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panti Gaatu Koruthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavi Pushpamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi Pushpamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onti Virupulone Undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onti Virupulone Undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oagala Pushpamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oagala Pushpamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vint Sharamu Tagilithe adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vint Sharamu Tagilithe adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura Pushpamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura Pushpamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Latha gaa Thanuallukune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latha gaa Thanuallukune"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharunamidhee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharunamidhee"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathaga Shruthi Kalputhunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathaga Shruthi Kalputhunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranayamidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranayamidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhurula Podhahalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhurula Podhahalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Kandhuthundi Pilupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Kandhuthundi Pilupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedurai Madhuvuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedurai Madhuvuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhanosthavalu Jarupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhanosthavalu Jarupu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pedavi Maata Matu Tessi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi Maata Matu Tessi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paata Rasikunna Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paata Rasikunna Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahaha haha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahaha haha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanula Chatu Kalalu Tesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanula Chatu Kalalu Tesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavitha AlluKunna Vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavitha AlluKunna Vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha Ahha Ahha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha Ahha Ahha"/>
</div>
</pre>
