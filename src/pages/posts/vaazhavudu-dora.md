---
title: "vaazhavudu song lyrics"
album: "Dora"
artist: "Vivek–Mervin"
lyricist: "Vignesh Shivan"
director: "Doss Ramasamy"
path: "/albums/dora-lyrics"
song: "Vaazhavudu"
image: ../../images/albumart/dora.jpg
date: 2017-03-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/l0oO1ZYa3FQ"
type: "happy"
singers:
  - Mervin Solomon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh oh ohoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ohoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhka oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhka oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oota vandi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oota vandi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikaiya vachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikaiya vachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaa oodum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaa oodum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalu peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu pesuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu pesuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhil yethikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil yethikkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookki podu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookki podu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu thaanadaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu thaanadaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavittu vazhnthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavittu vazhnthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Fun-nu thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fun-nu thaanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandavana nambum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavana nambum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam naangadaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottam naangadaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaathayum avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathayum avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathupaanadaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathupaanadaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhka oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhka oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oota vandi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oota vandi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikaiya vachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikaiya vachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaa oodum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaa oodum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalu peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu pesuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu pesuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhil yethikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil yethikkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookki podu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookki podu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ohoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ohoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu thaanadaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu thaanadaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavittu vazhnthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavittu vazhnthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ohoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ohoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Fun-nu thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fun-nu thaanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandavana nambum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavana nambum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ohoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ohoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam naangadaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottam naangadaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaathayum avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathayum avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathupaanadaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathupaanadaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhavudu vaazhavudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu vaazhavudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavudu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala vaara yosikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala vaara yosikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalavidudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalavidudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhavudu vaazhavudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu vaazhavudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavudu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga vandi ooda konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga vandi ooda konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gappu kodu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gappu kodu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhavudu vaazhavudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu vaazhavudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavudu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga vandi ooda konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga vandi ooda konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gappu kodu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gappu kodu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ohoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ohoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu thaanadaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu thaanadaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavittu vazhnthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavittu vazhnthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ohoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ohoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Fun-nu thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fun-nu thaanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandavana nambum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavana nambum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ohoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ohoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam naangadaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottam naangadaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaathayum avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathayum avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ohoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ohoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathupaanadaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathupaanadaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oota vandi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oota vandi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkaiya otti otti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkaiya otti otti"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Luck-a thedaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luck-a thedaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Work-a niruththaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Work-a niruththaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigam pesaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigam pesaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhuthu polambaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuthu polambaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappae ninaikkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappae ninaikkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavarae seiyyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavarae seiyyaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorthae ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorthae ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tension aagaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tension aagaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Speed breaker nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speed breaker nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Jerkku aagaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jerkku aagaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konja neram slowa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja neram slowa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konja neram slowa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja neram slowa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhavudu vaazhavudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu vaazhavudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavudu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala vaara yosikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala vaara yosikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalavidudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalavidudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhavudu vaazhavudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu vaazhavudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavudu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga vandi ooda konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga vandi ooda konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gappu kodu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gappu kodu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhavudu vaazhavudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu vaazhavudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavudu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga vandi ooda konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga vandi ooda konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gappu kodu daa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gappu kodu daa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhka oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhka oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oota vandi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oota vandi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikaiya vachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikaiya vachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaa oodum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaa oodum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalu peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu pesuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu pesuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhil yethikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil yethikkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookki podu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookki podu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ohoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ohoh"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu thaanadaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu thaanadaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavittu vazhnthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavittu vazhnthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Fun-nu thaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fun-nu thaanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandavana nambum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavana nambum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam naangadaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottam naangadaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaathayum avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathayum avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathupaanadaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathupaanadaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhavudu vaazhavudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu vaazhavudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavudu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala vaara yosikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala vaara yosikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalavidudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalavidudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhavudu vaazhavudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu vaazhavudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavudu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga vandi ooda konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga vandi ooda konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gappu kodu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gappu kodu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhavudu vaazhavudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu vaazhavudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavudu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala vaara yosikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala vaara yosikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalavidudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalavidudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhavudu vaazhavudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu vaazhavudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhavudu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhavudu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Free-ya vududaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free-ya vududaa"/>
</div>
</pre>
