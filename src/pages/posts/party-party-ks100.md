---
title: "party party song lyrics"
album: "KS100"
artist: "Navaneeth Chari"
lyricist: "Bhashya Sree"
director: "Sher"
path: "/albums/ks100-lyrics"
song: "Party Party"
image: ../../images/albumart/ks100.jpg
date: 2019-07-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/erS17sBoXcU"
type: "happy"
singers:
  - Varam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">party party mid night party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="party party mid night party"/>
</div>
<div class="lyrico-lyrics-wrapper">beauty girls chesti party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beauty girls chesti party"/>
</div>
<div class="lyrico-lyrics-wrapper">rules lev geels lev 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rules lev geels lev "/>
</div>
<div class="lyrico-lyrics-wrapper">maakemi limits lev
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maakemi limits lev"/>
</div>
<div class="lyrico-lyrics-wrapper">modhaledadhame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modhaledadhame "/>
</div>
<div class="lyrico-lyrics-wrapper">mid night party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mid night party"/>
</div>
<div class="lyrico-lyrics-wrapper">maga vaalakena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maga vaalakena "/>
</div>
<div class="lyrico-lyrics-wrapper">ee enjoymentu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee enjoymentu"/>
</div>
<div class="lyrico-lyrics-wrapper">manamendhulo thakkuva so
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manamendhulo thakkuva so"/>
</div>
<div class="lyrico-lyrics-wrapper">cheers ye kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheers ye kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">raccha raccha cheeddam pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raccha raccha cheeddam pattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jimpa chikka jimpa chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka jimpa chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka jimpa chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka jimpa chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka jimpa chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka jimpa chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka jimpa chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka jimpa chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mohamatam paduthu unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mohamatam paduthu unte"/>
</div>
<div class="lyrico-lyrics-wrapper">venuka venuke untaam manamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venuka venuke untaam manamu"/>
</div>
<div class="lyrico-lyrics-wrapper">oo steppu mundhuku vesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo steppu mundhuku vesi"/>
</div>
<div class="lyrico-lyrics-wrapper">gelicheddam timuni manamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gelicheddam timuni manamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vantintlona rabbit laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantintlona rabbit laga"/>
</div>
<div class="lyrico-lyrics-wrapper">unte lifemi miss avuthamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unte lifemi miss avuthamu"/>
</div>
<div class="lyrico-lyrics-wrapper">cheekati paradha dhinchesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheekati paradha dhinchesi"/>
</div>
<div class="lyrico-lyrics-wrapper">chalo chalo chalo chalo  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chalo chalo chalo chalo  "/>
</div>
<div class="lyrico-lyrics-wrapper">enjoy chesddam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enjoy chesddam "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jimpa chikka jimpa chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka jimpa chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka jimpa chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka jimpa chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka jimpa chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka jimpa chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka jimpa chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka jimpa chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aadavalu unde theerey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadavalu unde theerey"/>
</div>
<div class="lyrico-lyrics-wrapper">bottle lo posthe beeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bottle lo posthe beeru"/>
</div>
<div class="lyrico-lyrics-wrapper">kallaloni matthe theesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallaloni matthe theesi"/>
</div>
<div class="lyrico-lyrics-wrapper">label vesthe adhe whisky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="label vesthe adhe whisky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manakanna hot aadhu mandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manakanna hot aadhu mandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">manakanna kicku ivvadhu mandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manakanna kicku ivvadhu mandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">andhuke nemo magavalantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhuke nemo magavalantha"/>
</div>
<div class="lyrico-lyrics-wrapper">maguvallante marre padi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maguvallante marre padi "/>
</div>
<div class="lyrico-lyrics-wrapper">padi chastharu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padi chastharu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jimpa chikka jimpa chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka jimpa chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka jimpa chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka jimpa chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka jimpa chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka jimpa chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka jimpa chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka jimpa chikka"/>
</div>
<div class="lyrico-lyrics-wrapper">jimpa chikka chikka chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jimpa chikka chikka chikka"/>
</div>
</pre>
