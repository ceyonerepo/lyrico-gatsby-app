---
title: "preme aakasamaithe song lyrics"
album: "Rowdy Boys"
artist: "Devi Sri Prasad"
lyricist: "Shreemani"
director: "Harsha Konuganti"
path: "/albums/rowdy-boys-lyrics"
song: "Preme Aakasamaithe"
image: ../../images/albumart/rowdy-boys.jpg
date: 2022-01-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/UBR-GsWCI7E"
type: "love"
singers:
  - Jaspreet Jasz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">preme aakashamaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="preme aakashamaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">oh my jaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh my jaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">anthulo egire pakchulanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anthulo egire pakchulanta"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">preme pusthagamaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="preme pusthagamaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">oh my jaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh my jaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">mathe ninde page anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathe ninde page anta"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">boome gundramu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boome gundramu"/>
</div>
<div class="lyrico-lyrics-wrapper">aakasham neelamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aakasham neelamu"/>
</div>
<div class="lyrico-lyrics-wrapper">antha pedha nijamanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha pedha nijamanta"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvuante naaku pranamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvuante naaku pranamu"/>
</div>
<div class="lyrico-lyrics-wrapper">yentho isthamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yentho isthamu"/>
</div>
<div class="lyrico-lyrics-wrapper">dachalante kastamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dachalante kastamu"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu ekkadunte unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu ekkadunte unte"/>
</div>
<div class="lyrico-lyrics-wrapper">akkade ga swargamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="akkade ga swargamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh thele thelavare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh thele thelavare"/>
</div>
<div class="lyrico-lyrics-wrapper">velallo kalu therichi chustunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velallo kalu therichi chustunte"/>
</div>
<div class="lyrico-lyrics-wrapper">naa kougitlo nuvvunte varame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa kougitlo nuvvunte varame"/>
</div>
<div class="lyrico-lyrics-wrapper">idhivarakipudu kannulo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhivarakipudu kannulo "/>
</div>
<div class="lyrico-lyrics-wrapper">kanapadani rangullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanapadani rangullo"/>
</div>
<div class="lyrico-lyrics-wrapper">kotha prapancham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotha prapancham"/>
</div>
<div class="lyrico-lyrics-wrapper">chusedham maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusedham maname"/>
</div>
<div class="lyrico-lyrics-wrapper">doorala dharalu thempeyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="doorala dharalu thempeyana"/>
</div>
<div class="lyrico-lyrics-wrapper">kalanni chimpeyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalanni chimpeyanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thedilu vaaralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedilu vaaralu"/>
</div>
<div class="lyrico-lyrics-wrapper">levinka manamadyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="levinka manamadyanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">letha gulabhi pedhavulatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="letha gulabhi pedhavulatho"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu raase kavithalake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu raase kavithalake"/>
</div>
<div class="lyrico-lyrics-wrapper">naa padhave oo kagithamayindey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa padhave oo kagithamayindey"/>
</div>
<div class="lyrico-lyrics-wrapper">ardhamkani chaduvante manakasale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ardhamkani chaduvante manakasale "/>
</div>
<div class="lyrico-lyrics-wrapper">padadhante nee saigaalane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padadhante nee saigaalane "/>
</div>
<div class="lyrico-lyrics-wrapper">chadivithe bangundhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chadivithe bangundhe "/>
</div>
<div class="lyrico-lyrics-wrapper">ennenno kgla kavyalugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennenno kgla kavyalugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">marayi naaoohale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marayi naaoohale "/>
</div>
<div class="lyrico-lyrics-wrapper">ennunna nijamaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennunna nijamaina"/>
</div>
<div class="lyrico-lyrics-wrapper">nee mudduke thoogavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mudduke thoogavey"/>
</div>
</pre>
