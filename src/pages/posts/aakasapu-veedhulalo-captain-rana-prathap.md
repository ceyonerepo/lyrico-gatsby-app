---
title: "aakasapu veedhulalo song lyrics"
album: "Captain Rana Prathap"
artist: "Shran"
lyricist: "Sri Mani"
director: "Haranath Policherla"
path: "/albums/captain-rana-prathap-lyrics"
song: "Aakasapu Veedhulalo"
image: ../../images/albumart/captain-rana-prathap.jpg
date: 2019-06-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/qEoX5QbJQG0"
type: "mass"
singers:
  - Mangli
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aakasapu veedhulalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aakasapu veedhulalo"/>
</div>
<div class="lyrico-lyrics-wrapper">egire jendanadugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="egire jendanadugu"/>
</div>
<div class="lyrico-lyrics-wrapper">himashikharapu dharulalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="himashikharapu dharulalo"/>
</div>
<div class="lyrico-lyrics-wrapper">vechhani kiranaannadugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechhani kiranaannadugu"/>
</div>
<div class="lyrico-lyrics-wrapper">sarihaddhula kanchelalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarihaddhula kanchelalo"/>
</div>
<div class="lyrico-lyrics-wrapper">netthuti dharalanadugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netthuti dharalanadugu"/>
</div>
<div class="lyrico-lyrics-wrapper">sainikude evarantoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sainikude evarantoo"/>
</div>
<div class="lyrico-lyrics-wrapper">nirvachanam nuvvadugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirvachanam nuvvadugu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thellavarithe sooryodhayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thellavarithe sooryodhayame"/>
</div>
<div class="lyrico-lyrics-wrapper">choose mundhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choose mundhara"/>
</div>
<div class="lyrico-lyrics-wrapper">gatha rathri kanulalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gatha rathri kanulalo"/>
</div>
<div class="lyrico-lyrics-wrapper">kammani niddhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kammani niddhura"/>
</div>
<div class="lyrico-lyrics-wrapper">evvari chalavara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evvari chalavara"/>
</div>
<div class="lyrico-lyrics-wrapper">vela ledu nee gundelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vela ledu nee gundelona"/>
</div>
<div class="lyrico-lyrics-wrapper">mandhu patara aa rakshana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandhu patara aa rakshana"/>
</div>
<div class="lyrico-lyrics-wrapper">kavachaniki oo pere 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavachaniki oo pere "/>
</div>
<div class="lyrico-lyrics-wrapper">sainika sodhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sainika sodhara"/>
</div>
<div class="lyrico-lyrics-wrapper">gundepai thoota santhakame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundepai thoota santhakame"/>
</div>
<div class="lyrico-lyrics-wrapper">athadikadhi bangaru pathakamule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athadikadhi bangaru pathakamule"/>
</div>
<div class="lyrico-lyrics-wrapper">ontipai netthuri charikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ontipai netthuri charikathe"/>
</div>
<div class="lyrico-lyrics-wrapper">athadikavi puvvula malikale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athadikavi puvvula malikale"/>
</div>
<div class="lyrico-lyrics-wrapper">thana varinodhili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana varinodhili"/>
</div>
<div class="lyrico-lyrics-wrapper">thana oorinododhili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana oorinododhili"/>
</div>
<div class="lyrico-lyrics-wrapper">mana dhesapu rakshana kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana dhesapu rakshana kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">urini thana siri anukuntade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urini thana siri anukuntade"/>
</div>
<div class="lyrico-lyrics-wrapper">barini thana badi anukuntade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="barini thana badi anukuntade"/>
</div>
<div class="lyrico-lyrics-wrapper">balini balamanukuntade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="balini balamanukuntade"/>
</div>
<div class="lyrico-lyrics-wrapper">maranane oka gelupanukuntade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranane oka gelupanukuntade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sankellenni patti unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sankellenni patti unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">lekka ledhu choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lekka ledhu choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">emukanaina choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emukanaina choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">emukanaina eetelaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emukanaina eetelaga"/>
</div>
<div class="lyrico-lyrics-wrapper">naatuthadu veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naatuthadu veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">dhoosukelle thootaalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhoosukelle thootaalona"/>
</div>
<div class="lyrico-lyrics-wrapper">pothu unnavadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothu unnavadu"/>
</div>
<div class="lyrico-lyrics-wrapper">lakshya chedhanay yedhaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lakshya chedhanay yedhaka"/>
</div>
<div class="lyrico-lyrics-wrapper">aagipodu veedu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagipodu veedu "/>
</div>
<div class="lyrico-lyrics-wrapper">thyagamanukumtha manamanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thyagamanukumtha manamanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">badhyathanukunta dithadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="badhyathanukunta dithadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">kastamanukuntam manamanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kastamanukuntam manamanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">istapadi chese yuddhamanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="istapadi chese yuddhamanta"/>
</div>
<div class="lyrico-lyrics-wrapper">thana varinodhili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana varinodhili"/>
</div>
<div class="lyrico-lyrics-wrapper">thana oorinodhili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana oorinodhili"/>
</div>
<div class="lyrico-lyrics-wrapper">mana dhesapu rakshanakosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana dhesapu rakshanakosam"/>
</div>
<div class="lyrico-lyrics-wrapper">urini thana siri anukuntade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urini thana siri anukuntade"/>
</div>
<div class="lyrico-lyrics-wrapper">barini thana badi anukuntade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="barini thana badi anukuntade"/>
</div>
<div class="lyrico-lyrics-wrapper">balini balamanukuntade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="balini balamanukuntade"/>
</div>
<div class="lyrico-lyrics-wrapper">marananne oka gelupanukuntade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marananne oka gelupanukuntade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">porugadda ee telanganamulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porugadda ee telanganamulu"/>
</div>
<div class="lyrico-lyrics-wrapper">rakshana kavacham raana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rakshana kavacham raana"/>
</div>
<div class="lyrico-lyrics-wrapper">koti rathanala veenanu meete
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koti rathanala veenanu meete"/>
</div>
<div class="lyrico-lyrics-wrapper">rakthapu kiranam rana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rakthapu kiranam rana"/>
</div>
<div class="lyrico-lyrics-wrapper">thana oopirini jana oopirikai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana oopirini jana oopirikai"/>
</div>
<div class="lyrico-lyrics-wrapper">thrunamuga vidiche rana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thrunamuga vidiche rana"/>
</div>
<div class="lyrico-lyrics-wrapper">svarga seemalo veerulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="svarga seemalo veerulu"/>
</div>
<div class="lyrico-lyrics-wrapper">meesam thippe garvam rana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meesam thippe garvam rana"/>
</div>
<div class="lyrico-lyrics-wrapper">shatruvu gundela pele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shatruvu gundela pele"/>
</div>
<div class="lyrico-lyrics-wrapper">anubanbula shabdham ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anubanbula shabdham ee"/>
</div>
<div class="lyrico-lyrics-wrapper">rana mushkarulaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rana mushkarulaku "/>
</div>
<div class="lyrico-lyrics-wrapper">bari lopala shikshanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bari lopala shikshanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">vidichina bhanam rana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidichina bhanam rana"/>
</div>
<div class="lyrico-lyrics-wrapper">dhesamatha kanneruni thudiche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhesamatha kanneruni thudiche"/>
</div>
<div class="lyrico-lyrics-wrapper">veera prathapam rana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veera prathapam rana"/>
</div>
<div class="lyrico-lyrics-wrapper">sipayilandhari ammulapodhilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sipayilandhari ammulapodhilo"/>
</div>
<div class="lyrico-lyrics-wrapper">brahmasthram ee rana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="brahmasthram ee rana"/>
</div>
<div class="lyrico-lyrics-wrapper">pidikile piduguga vesthade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikile piduguga vesthade"/>
</div>
<div class="lyrico-lyrics-wrapper">adugule aayuthamantale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugule aayuthamantale"/>
</div>
<div class="lyrico-lyrics-wrapper">cheduni chenike churakathi ithadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheduni chenike churakathi ithadu"/>
</div>
<div class="lyrico-lyrics-wrapper">kotha charithai udhayisthade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotha charithai udhayisthade"/>
</div>
</pre>
