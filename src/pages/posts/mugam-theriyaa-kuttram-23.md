---
title: "mugam theriyaa song lyrics"
album: "Kuttram 23"
artist: "Vishal Chandrasekhar"
lyricist: "Viveka"
director: "Arivazhagan Venkatachalam"
path: "/albums/kuttram-23-lyrics"
song: "Mugam Theriyaa"
image: ../../images/albumart/kuttram-23.jpg
date: 2017-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GbnhoY81skE"
type: "mass"
singers:
  - Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mugam theriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam theriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam thiruppi selvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam thiruppi selvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanban vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasigaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasigaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nazhuval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nazhuval"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugam theriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam theriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam thiruppi selvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam thiruppi selvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanban vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasigaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasigaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nazhuval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nazhuval"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethi mandram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethi mandram"/>
</div>
<div class="lyrico-lyrics-wrapper">Solvathu mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solvathu mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Niyaayathai keladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyaayathai keladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaal eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaal eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolavathu mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolavathu mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthaigalaal kolvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaigalaal kolvathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai nambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum pennaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum pennaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirgathiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirgathiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Viduthal Kuttram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduthal Kuttram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir aakkiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir aakkiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Petroraii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petroraii"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraththal kuttram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraththal kuttram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nettrikan thiranthaal kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nettrikan thiranthaal kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram kuttram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram kuttram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttrathin thozhan aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin thozhan aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrathin vaasal kovam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin vaasal kovam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrathin palanae pavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin palanae pavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrangal vaazhvin saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrangal vaazhvin saabam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttrathin thozhan aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin thozhan aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrathin vaasal kovam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin vaasal kovam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrathin palanae pavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin palanae pavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrangal vaazhvin saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrangal vaazhvin saabam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugam theriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam theriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam thiruppi selvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam thiruppi selvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanban vizhiKasigaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban vizhiKasigaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nazhuval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nazhuval"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arivugalin koochal kuttram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivugalin koochal kuttram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei marakkum aanmeegaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei marakkum aanmeegaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Alavedutha vaarthai kuttram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavedutha vaarthai kuttram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbargal koodaraththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbargal koodaraththil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arivugalin koochal kuttram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivugalin koochal kuttram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei marakkum aanmeegaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei marakkum aanmeegaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Alavedutha vaarthai kuttram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavedutha vaarthai kuttram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbargal koodaraththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbargal koodaraththil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Piran manai nokkuthal kuttramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piran manai nokkuthal kuttramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivinai pesuthal kuttramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivinai pesuthal kuttramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karppinai tholaippathu polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karppinai tholaippathu polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadamaiyai marappathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadamaiyai marappathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttramae kuttramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttramae kuttramae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttrathin thozhan aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin thozhan aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrathin vaasal kovam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin vaasal kovam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrathin palanae pavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin palanae pavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrangal vaazhvin saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrangal vaazhvin saabam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttrathin thozhan aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin thozhan aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrathin vaasal kovam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin vaasal kovam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrathin palanae pavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin palanae pavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrangal vaazhvin saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrangal vaazhvin saabam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugam theriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam theriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam thiruppi selvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam thiruppi selvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanban vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasigaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasigaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nazhuval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nazhuval"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoruvan kuttrathukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoruvan kuttrathukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanukkena niyaayam undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanukkena niyaayam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruthiyaai kaalam sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruthiyaai kaalam sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Niyaangkalin sabaiyil nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyaangkalin sabaiyil nindru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoruvan kuttrathukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoruvan kuttrathukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanukkena niyaayam undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanukkena niyaayam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruthiyaai kaalam sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruthiyaai kaalam sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Niyaangkalin sabaiyil nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyaangkalin sabaiyil nindru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manithanai yevi vittae thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithanai yevi vittae thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithanai kolgiraanae manithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithanai kolgiraanae manithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethu mattru azhuvorkkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethu mattru azhuvorkkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan thaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan thaane "/>
</div>
<div class="lyrico-lyrics-wrapper">koolippadai thalaivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koolippadai thalaivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttrathin thozhan aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin thozhan aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrathin vaasal kovam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin vaasal kovam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrathin palanae pavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin palanae pavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrangal vaazhvin saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrangal vaazhvin saabam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttrathin thozhan aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin thozhan aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrathin vaasal kovam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin vaasal kovam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrathin palanae pavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrathin palanae pavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttrangal vaazhvin saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttrangal vaazhvin saabam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugam theriyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam theriyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam thiruppi selvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam thiruppi selvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanban vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasigaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasigaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nazhuval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nazhuval"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttram thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttram thaan"/>
</div>
</pre>
