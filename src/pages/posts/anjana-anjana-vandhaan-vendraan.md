---
title: "anjana anjana song lyrics"
album: "Vandhaan Vendraan"
artist: "Thaman"
lyricist: "Yugabharathi"
director: "R. Kannan"
path: "/albums/vandhaan-vendraan-lyrics"
song: "Anjana Anjana"
image: ../../images/albumart/vandhaan-vendraan.jpg
date: 2011-09-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/aTmCZC_28uI"
type: "love"
singers:
  - Aalap Raju
  - Bhasi
  - Chinmayi
  - Ramya NSK
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Indru mudhal naal pudhithaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru mudhal naal pudhithaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un iniya sirippinil mugil aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un iniya sirippinil mugil aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottum mazhai pol sugam aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum mazhai pol sugam aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un konjum udhattinil tamil aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un konjum udhattinil tamil aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un konjum udhattinil tamil aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un konjum udhattinil tamil aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjana anjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjana anjana"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae anbe anjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae anbe anjana"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ottrai paarvai podhum anjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ottrai paarvai podhum anjana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjana anjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjana anjana"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai naanae anjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai naanae anjana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum neeyaai aanen anjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum neeyaai aanen anjana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah podu podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah podu podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthanathom podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthanathom podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee andharathil aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee andharathil aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh thulli vilaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh thulli vilaiyaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu thottu paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu thottu paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhukku kattuppaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhukku kattuppaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vandhu vandhu thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vandhu vandhu thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa katta katta soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa katta katta soodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mutti mutti moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mutti mutti moodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathil ennai naadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathil ennai naadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu vizhiyodu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu vizhiyodu "/>
</div>
<div class="lyrico-lyrics-wrapper">ennai marandhenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai marandhenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiyaanen unmaiyaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyaanen unmaiyaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai polae anmaiyaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai polae anmaiyaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Venmaiyaanen venmaiyaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venmaiyaanen venmaiyaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella naanum thanmai aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella naanum thanmai aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal kaadhal vandhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kaadhal vandhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneerum kooda thee polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneerum kooda thee polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaalae maarum mann melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaalae maarum mann melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosham koodum nenjullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosham koodum nenjullae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaayam undhan kaal keezhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam undhan kaal keezhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo kolam podum panbaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo kolam podum panbaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhaalam ondru unnullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhaalam ondru unnullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaadi pogum cell ullaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadi pogum cell ullaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo ooo ooo ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo ooo ooo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjana anjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjana anjana"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai naanae anjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai naanae anjana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo ooo ooo ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo ooo ooo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru chinna paarvaiyilNaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru chinna paarvaiyilNaan "/>
</div>
<div class="lyrico-lyrics-wrapper">viduthalai viduthalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viduthalai viduthalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adainthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adainthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhanbu vaarthaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhanbu vaarthaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan piraviyin payanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan piraviyin payanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arindhenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arindhenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye kelu kelu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kelu kelu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennavendru kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavendru kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee eppozhuthum kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee eppozhuthum kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan solluvadhai kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan solluvadhai kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaathathaiyum kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaathathaiyum kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerungi vandhu kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungi vandhu kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadharugil mozhiyaai varuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadharugil mozhiyaai varuvenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiyaanen unmaiyaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyaanen unmaiyaanen"/>
</div>
.
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siragillai yaayinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragillai yaayinum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan iragena iragena paranthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan iragena iragena paranthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavillai aayinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavillai aayinum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan muzhuvadhum muzhuvadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan muzhuvadhum muzhuvadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalainthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalainthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye paaru paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye paaru paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah pakkam vandhu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah pakkam vandhu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paadi paadi paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paadi paadi paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah bathirama paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah bathirama paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadharasam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadharasam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhukkavillai paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhukkavillai paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila nodigalil enna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila nodigalil enna "/>
</div>
<div class="lyrico-lyrics-wrapper">naan tharuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan tharuvenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmaiyaanen unmaiyaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyaanen unmaiyaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai polae anmaiyaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai polae anmaiyaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Venmai aanen venmai aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venmai aanen venmai aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella naanum thanmai aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella naanum thanmai aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal kaadhal vandhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kaadhal vandhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneerum kooda thee polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneerum kooda thee polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaalae maarum mann melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaalae maarum mann melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosham koodum nenjullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosham koodum nenjullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagaayam undhan kaal keezhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam undhan kaal keezhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo kolam podum panbaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo kolam podum panbaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhaalam ondru unnullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhaalam ondru unnullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaadi pogum cell ullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadi pogum cell ullae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo ooo ooo ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo ooo ooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo ooo ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo ooo ooo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjana anjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjana anjana"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai naanae anjana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai naanae anjana"/>
</div>
</pre>
