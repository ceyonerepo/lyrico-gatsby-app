---
title: "bam bhole song lyrics"
album: "Laxmii"
artist: "Ullumanati"
lyricist: "Ullumanati"
director: "Raghava Lawrence"
path: "/albums/laxmii-lyrics"
song: "Bam Bhole"
image: ../../images/albumart/laxmii.jpg
date: 2020-11-09
lang: hindi
youtubeLink: "https://www.youtube.com/embed/gBq9Zcs_5D8"
type: "mass"
singers:
  - Viruss
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Akaal mrityu marta kaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akaal mrityu marta kaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karta jo chandaal ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karta jo chandaal ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal uska kya bigaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal uska kya bigaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhakt jo mahakaal ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhakt jo mahakaal ka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akaal mrityu marta kaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akaal mrityu marta kaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karta jo chandaal ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karta jo chandaal ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal uska kya bigaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal uska kya bigaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhakt jo mahakaal ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhakt jo mahakaal ka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akaal mrityu marta kaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akaal mrityu marta kaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karta jo chandaal ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karta jo chandaal ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal uska kya bigaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal uska kya bigaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhakt jo mahakaal ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhakt jo mahakaal ka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akaal mrityu marta kaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akaal mrityu marta kaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karta jo chandaal ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karta jo chandaal ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal uska kya bigaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal uska kya bigaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhakt jo mahakaal ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhakt jo mahakaal ka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bam bholle bam bam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bam bholle bam bam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bam bholle bam bam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bam bholle bam bam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhole ki masti hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhole ki masti hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bam bhole bam bam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bam bhole bam bam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bam bholle bam bam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bam bholle bam bam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bam bholle bam bam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bam bholle bam bam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhole ki masti hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhole ki masti hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bam bhole bam bam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bam bhole bam bam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhole ki masti mein naachenge saare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhole ki masti mein naachenge saare"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhole harenge jo kasht tumhare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhole harenge jo kasht tumhare"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhole ke naam pe chalti yeh duniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhole ke naam pe chalti yeh duniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar lagega bhole ke sahaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar lagega bhole ke sahaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhole ke naam ke deewane huye hum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhole ke naam ke deewane huye hum"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir bhool gaye hum saare zindagi ke gham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir bhool gaye hum saare zindagi ke gham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyuki denge bhole darshan sakshat saamne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyuki denge bhole darshan sakshat saamne"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise hanumaan ji ko diye shree ram ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise hanumaan ji ko diye shree ram ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhole ki masti mein milke saare aaj bole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhole ki masti mein milke saare aaj bole"/>
</div>
<div class="lyrico-lyrics-wrapper">Har har mahadev mahakaal bam bholle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har har mahadev mahakaal bam bholle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bam bholle bam bholle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bam bholle bam bholle"/>
</div>
<div class="lyrico-lyrics-wrapper">Bam bholle bam bholle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bam bholle bam bholle"/>
</div>
<div class="lyrico-lyrics-wrapper">Bam bholle bam bam bholle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bam bholle bam bam bholle"/>
</div>
<div class="lyrico-lyrics-wrapper">Bam bam bholle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bam bam bholle"/>
</div>
<div class="lyrico-lyrics-wrapper">Bam bam bam bam bam.. Bhole..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bam bam bam bam bam.. Bhole.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mujhe to chadha hai bas bhole ka rang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe to chadha hai bas bhole ka rang"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan kan mein har kshan mein hai bhole mere sang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan kan mein har kshan mein hai bhole mere sang"/>
</div>
<div class="lyrico-lyrics-wrapper">Leke tere naam bhakti mein teri hi raha hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leke tere naam bhakti mein teri hi raha hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Sab kuchh chhod kar shakti mein teri jee raha hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab kuchh chhod kar shakti mein teri jee raha hun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bam bam bam bholle bholle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bam bam bam bholle bholle"/>
</div>
</pre>
