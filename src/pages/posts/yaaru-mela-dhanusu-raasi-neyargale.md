---
title: "yaaru mela song lyrics"
album: "Dhanusu Raasi Neyargale"
artist: "Ghibran"
lyricist: "Madhan Karky"
director: "Sanjay Bharathi"
path: "/albums/dhanusu-raasi-neyargale-lyrics"
song: "Yaaru Mela"
image: ../../images/albumart/dhanusu-raasi-neyargale.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JhhcBaRTfJ8"
type: "love"
singers:
  - Sowmya Mahadevan
  - Lijisha Praveen
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manmadha Azhagil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadha Azhagil"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadha Azhagil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadha Azhagil"/>
</div>
<div class="lyrico-lyrics-wrapper">Maan Iva Vizhundhaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maan Iva Vizhundhaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon Iva Arivil Pon Iva Arivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Iva Arivil Pon Iva Arivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulithaan Sarinjaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulithaan Sarinjaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maapla Muliyo Maapla Muliyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maapla Muliyo Maapla Muliyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkathil Vilaiyaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkathil Vilaiyaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon Iva Vizhiyo Pon Iva Vizhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Iva Vizhiyo Pon Iva Vizhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhaiyo Edai Podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhaiyo Edai Podaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anubavame Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubavame Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum Paravaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Paravaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thairiyame Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thairiyame Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinchaandi Inimela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinchaandi Inimela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Konjam Valiyavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Konjam Valiyavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhazh Konjam Neliyavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhazh Konjam Neliyavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragasiyam Veli Varumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyam Veli Varumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarumela Kuththam Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumela Kuththam Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Ketpomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Ketpomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Senja Thappaa Innoru Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senja Thappaa Innoru Time"/>
</div>
<div class="lyrico-lyrics-wrapper">Senji Paappomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senji Paappomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarumela Kuththam Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumela Kuththam Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Ketpomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Ketpomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Senja Thappaa Innoru Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senja Thappaa Innoru Time"/>
</div>
<div class="lyrico-lyrics-wrapper">Senji Paappomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senji Paappomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhu Arundhiya Bodhai Mel Thappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhu Arundhiya Bodhai Mel Thappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi Mayangiya Kodhai Mel Thappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi Mayangiya Kodhai Mel Thappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaala Kannaala Polladha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Kannaala Polladha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Mel Thappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Mel Thappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udal Nadukkiya Kuliralai Mel Thappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Nadukkiya Kuliralai Mel Thappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Udai Virumbiya Viduthalai Mel Thappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udai Virumbiya Viduthalai Mel Thappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaala Kannaala Illadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Kannaala Illadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhukkam Mel Thappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhukkam Mel Thappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathi Thavaru Avan Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathi Thavaru Avan Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Meedhi Thavarum Avan Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meedhi Thavarum Avan Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Avana Solli Kuthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana Solli Kuthamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva Konjam Sirichathunaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Konjam Sirichathunaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarumela Kuththam Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumela Kuththam Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Ketpomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Ketpomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Senja Thappaa Innoru Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senja Thappaa Innoru Time"/>
</div>
<div class="lyrico-lyrics-wrapper">Senji Paappomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senji Paappomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarumela Kuththam Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumela Kuththam Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Ketpomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Ketpomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Senja Thappaa Innoru Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senja Thappaa Innoru Time"/>
</div>
<div class="lyrico-lyrics-wrapper">Senji Paappomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senji Paappomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munu Munukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munu Munukkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamanukkudhu Manasu Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamanukkudhu Manasu Unnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnala Pinnala Idhapol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnala Pinnala Idhapol"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalum Irundhadhu Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalum Irundhadhu Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nama Namakudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nama Namakudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhattil Edhanaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhattil Edhanaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaba Kabangudhu Viralil Edhanaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaba Kabangudhu Viralil Edhanaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnala Pinnala Idhapol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnala Pinnala Idhapol"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaum Irundhadhu Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaum Irundhadhu Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru Mudhalil Adhai Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Mudhalil Adhai Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Mudhalil Sari Sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Mudhalil Sari Sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettadhu Onnum Kuththamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettadhu Onnum Kuththamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellame Mudinchittathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellame Mudinchittathaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manmadha Azhagil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadha Azhagil"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmadha Azhagil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmadha Azhagil"/>
</div>
<div class="lyrico-lyrics-wrapper">Maan Iva Vizhundhaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maan Iva Vizhundhaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon Iva Arivil Pon Iva Arivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Iva Arivil Pon Iva Arivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulithaan Sarinjaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulithaan Sarinjaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anubavame Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubavame Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum Paravaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Paravaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thairiyame Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thairiyame Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinchaandi Inimela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinchaandi Inimela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Konjam Valiyavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Konjam Valiyavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhazh Konjam Neliyavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhazh Konjam Neliyavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragasiyam Veli Varumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyam Veli Varumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarumela Kuththam Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumela Kuththam Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Ketpomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Ketpomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Senja Thappaa Innoru Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senja Thappaa Innoru Time"/>
</div>
<div class="lyrico-lyrics-wrapper">Senji Paappomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senji Paappomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarumela Kuththam Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumela Kuththam Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Ketpomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Ketpomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Senja Thappaa Innoru Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senja Thappaa Innoru Time"/>
</div>
<div class="lyrico-lyrics-wrapper">Senji Paappomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senji Paappomaa"/>
</div>
</pre>
