---
title: "aa dhevude song lyrics"
album: "Idi Naa Love Story"
artist: "Srinath Vijay"
lyricist: "Ravikiran"
director: "Ramesh Gopi"
path: "/albums/idi-naa-love-story-lyrics"
song: "Aa Dhevude"
image: ../../images/albumart/idi-naa-love-story.jpg
date: 2018-02-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/B7RmKyDQwXQ"
type: "love"
singers:
  -	Vijay Prakash
  - Padhma Latha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aa Dhevude Kalekantu Ila Chesenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Dhevude Kalekantu Ila Chesenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Swargam Ee Nelapai Ila Dhinchenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Swargam Ee Nelapai Ila Dhinchenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Chaithram Naathone Ila Saagennaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Chaithram Naathone Ila Saagennaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikottha Andhaalila Mundhunchenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikottha Andhaalila Mundhunchenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaale Ganetheesi Ilaa Vacchi Chalerepenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaale Ganetheesi Ilaa Vacchi Chalerepenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poole Adhechoosi Kale Thecchi Vale Vesenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poole Adhechoosi Kale Thecchi Vale Vesenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neere Chirujallaithe Ninge Harivillaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neere Chirujallaithe Ninge Harivillaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi Thenejallai Ilaa Kurisenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi Thenejallai Ilaa Kurisenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Dhevude Kalekantu Ila Chesenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Dhevude Kalekantu Ila Chesenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Swargam Ee Nelapai Ila Dhinchenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Swargam Ee Nelapai Ila Dhinchenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Chaithram Naathone Ila Saagennaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Chaithram Naathone Ila Saagennaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikottha Andhaalila Mundhunchenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikottha Andhaalila Mundhunchenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ga Ri Sa Ni Sa Sa Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ri Sa Ni Sa Sa Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Sa Sa Sa Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Sa Sa Sa Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Sa Sa Sa Sa Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Sa Sa Sa Sa Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Sa Ri Ga Ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Sa Ri Ga Ri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ga Ri Sa Ni Sa Sa Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ri Sa Ni Sa Sa Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Sa Sa Sa Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Sa Sa Sa Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Sa Sa Sa Sa Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Sa Sa Sa Sa Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Sa Ri Ga Ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Sa Ri Ga Ri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Vaagulo Aa Sooryude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Vaagulo Aa Sooryude"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Andham Choosenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Andham Choosenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Addhaane Chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addhaane Chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Kondalo Aa Chandrude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kondalo Aa Chandrude"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoboochu Laadenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoboochu Laadenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoralaaga Vacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoralaaga Vacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosi Choodangaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosi Choodangaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopulemo Nilichipoyele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopulemo Nilichipoyele"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi Paadagaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Paadagaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupanedhi Rakapoyene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupanedhi Rakapoyene"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Eevelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Eevelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaa Gamapaa Panidhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaa Gamapaa Panidhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa Ma Ga Ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Ma Ga Ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ri Sa Ni Sa Sa Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ri Sa Ni Sa Sa Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Sa Sa Sa Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Sa Sa Sa Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Sa Sa Sa Sa Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Sa Sa Sa Sa Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Sa Ri Ga Ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Sa Ri Ga Ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ri Sa Ni Sa Sa Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ri Sa Ni Sa Sa Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Sa Sa Sa Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Sa Sa Sa Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Sa Sa Sa Sa Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Sa Sa Sa Sa Sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Sa Ni Da Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Sa Ni Da Pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Kiranam Naakosam Udhayinchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kiranam Naakosam Udhayinchene"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Andham Chooddanike Ilaa Vacchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Andham Chooddanike Ilaa Vacchene"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Roopam Choodagaane Mathipoyene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Roopam Choodagaane Mathipoyene"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhahaaru Vayasocchilaa Sogsicchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhahaaru Vayasocchilaa Sogsicchene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaluva Puvvulaage 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaluva Puvvulaage "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kalle Vicchukunnaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kalle Vicchukunnaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhonda Panduchoosthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhonda Panduchoosthe "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pedhaale Gurthukocchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pedhaale Gurthukocchene"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Parugu Teesthunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Parugu Teesthunte "/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Aapalekunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Aapalekunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemo Yemayindho Ilaa Eevela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemo Yemayindho Ilaa Eevela"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kiranam Naakosam Udhayinchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kiranam Naakosam Udhayinchene"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Andham Chooddanike Ilaa Vacchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Andham Chooddanike Ilaa Vacchene"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Roopam Choodagaane Mathipoyene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Roopam Choodagaane Mathipoyene"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhahaaru Vayasocchilaa Sogsicchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhahaaru Vayasocchilaa Sogsicchene"/>
</div>
</pre>
