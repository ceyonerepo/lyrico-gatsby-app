---
title: "hai en kai mela song lyrics"
album: "Sangili Bungili Kadhava Thorae"
artist: "Vishal Chandrasekhar"
lyricist: "Vivek"
director: "Ike"
path: "/albums/sangili-bungili-kadhava-thorae-lyrics"
song: "Hai En Kai Mela"
image: ../../images/albumart/sangili-bungili-kadhava-thorae.jpg
date: 2017-05-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZsTfRpSCr8w"
type: "love"
singers:
  -	Anirudh Ravichander
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hai en kai mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai en kai mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nela kuttindhaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nela kuttindhaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Panju kannam kavvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panju kannam kavvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadipen seendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadipen seendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un eriyeeti idupaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un eriyeeti idupaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagalae kulipaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagalae kulipaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ula mooti pogadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ula mooti pogadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Angaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un udhatorram kodiyatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un udhatorram kodiyatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiroda rayilotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiroda rayilotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Siruthotti podadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruthotti podadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Singaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh pathura pathura paarvayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh pathura pathura paarvayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Buthiyae sethuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buthiyae sethuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru sakkara muthira podudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru sakkara muthira podudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sithira puthiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithira puthiriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh pathura pathura paarvayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh pathura pathura paarvayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Buthiyae sethuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buthiyae sethuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru sakkara muthira podudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru sakkara muthira podudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sithira puthiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithira puthiriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sela mela un pattapatti ottaotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sela mela un pattapatti ottaotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Palla ilikithuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palla ilikithuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Goli kannu oru pitta thatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli kannu oru pitta thatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta katti ulla izhukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta katti ulla izhukkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En velamutti kaattaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En velamutti kaattaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiya vekkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiya vekkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon nela nethi kotoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon nela nethi kotoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaiyavekkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyavekkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un oralukatta veralu patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un oralukatta veralu patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Silusiluppu vedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silusiluppu vedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sariyavitta nizhalupatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sariyavitta nizhalupatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilukilukuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilukilukuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada patta ada kanna pinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada patta ada kanna pinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Enamellam vandhu pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enamellam vandhu pogudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadu onnu indha ponnaganni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu onnu indha ponnaganni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponna alli thinnu pogudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna alli thinnu pogudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi silla etta seekakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi silla etta seekakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiriya vittalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiriya vittalae"/>
</div>
<div class="lyrico-lyrics-wrapper">En salla katta kaathaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En salla katta kaathaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaiya vittanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiya vittanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iva madippukulla madinjuputen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva madippukulla madinjuputen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadha mudinjiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadha mudinjiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru modhala vandhu moyalu kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru modhala vandhu moyalu kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Saranadanjuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saranadanjuruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey thanthana thanthaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thanthana thanthaana "/>
</div>
<div class="lyrico-lyrics-wrapper">ra raa aaa rarara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra raa aaa rarara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthana thanthaana ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthana thanthaana ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thanthana thanthaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thanthana thanthaana "/>
</div>
<div class="lyrico-lyrics-wrapper">ra raa aaa rarara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ra raa aaa rarara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthana thanthaana ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthana thanthaana ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana thandhana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana thandhana "/>
</div>
<div class="lyrico-lyrics-wrapper">thandhana ra ra ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandhana ra ra ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthana thanthaana ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthana thanthaana ra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ta rara ra ra ta ra ra ra raaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ta rara ra ra ta ra ra ra raaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ta rara ra ra ta ra ra ra raaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ta rara ra ra ta ra ra ra raaa"/>
</div>
</pre>
