---
title: 'piriyadha enna song lyrics'
album: 'Pattas'
artist: 'Vivek Mervin'
lyricist: 'Ku.Karthik'
director: 'R.S.Durai Senthilkumar'
path: '/albums/pattas-song-lyrics'
song: 'Piriyadha Enna'
image: ../../images/albumart/pattas.jpg
date: 2020-01-15
lang: tamil
singers:
- Vijay Yesudas
- Niranjana
youtubeLink: 'https://www.youtube.com/embed/KLxrjuV_hkg'
type: 'duet'
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Edhedho aasai nenjula
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhedho aasai nenjula"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla thaan theriyala
<input type="checkbox" class="lyrico-select-lyric-line" value="Solla thaan theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala kannu thoongala
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnaala kannu thoongala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukunnu puriyala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethukunnu puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Olagamae namakku thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Olagamae namakku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kooda irunthutta
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee kooda irunthutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgamae thorakkudha
<input type="checkbox" class="lyrico-select-lyric-line" value="Sorgamae thorakkudha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kooda nadanthutta
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kooda nadanthutta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Usurellaam inikkudha
<input type="checkbox" class="lyrico-select-lyric-line" value="Usurellaam inikkudha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thozhil saanjitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Un thozhil saanjitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragu thaan mulaikkudha
<input type="checkbox" class="lyrico-select-lyric-line" value="Siragu thaan mulaikkudha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaiya pidichitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kaiya pidichitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru vaarthai kooda
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru vaarthai kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu solla thaeva illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ingu solla thaeva illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal idhu thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal idhu thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Piriyadha enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Piriyadha enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum needhaan nenappula
<input type="checkbox" class="lyrico-select-lyric-line" value="Eppodhum needhaan nenappula"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraiyama unna
<input type="checkbox" class="lyrico-select-lyric-line" value="Koraiyama unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjodu serkka marakkala
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjodu serkka marakkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Piriyadha enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Piriyadha enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum needhaan nenappula
<input type="checkbox" class="lyrico-select-lyric-line" value="Eppodhum needhaan nenappula"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraiyama unna
<input type="checkbox" class="lyrico-select-lyric-line" value="Koraiyama unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjodu serkka marakkala
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjodu serkka marakkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaiyaala naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiyaala naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Megatha thatturavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Megatha thatturavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mattum paartha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee mattum paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai katti nikkuravan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai katti nikkuravan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En pera naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="En pera naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum sonnadhu illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Eppodhum sonnadhu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pera serthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un pera serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaadha naalum illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Sollaadha naalum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Eerezhu jenmam thaandi
<input type="checkbox" class="lyrico-select-lyric-line" value="Eerezhu jenmam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna thozhil thaanganum
<input type="checkbox" class="lyrico-select-lyric-line" value="Onna thozhil thaanganum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Thallaadum podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thallaadum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu rendum onna thaedanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannu rendum onna thaedanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Uyir vittu pogum
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyir vittu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha nodiyil un uruvam
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha nodiyil un uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnal varanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Munnal varanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Piriyadha enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Piriyadha enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum needhaan nenappula
<input type="checkbox" class="lyrico-select-lyric-line" value="Eppodhum needhaan nenappula"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraiyama unna
<input type="checkbox" class="lyrico-select-lyric-line" value="Koraiyama unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjodu serkka marakkala
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjodu serkka marakkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Both  Piriyadha enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Both  Piriyadha enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum needhaan nenappula
<input type="checkbox" class="lyrico-select-lyric-line" value="Eppodhum needhaan nenappula"/>
</div>
<div class="lyrico-lyrics-wrapper">Koraiyama unna
<input type="checkbox" class="lyrico-select-lyric-line" value="Koraiyama unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjodu serkka marakkala
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjodu serkka marakkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edhedho aasai nenjula
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhedho aasai nenjula"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla thaan theriyala
<input type="checkbox" class="lyrico-select-lyric-line" value="Solla thaan theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala kannu thoongala
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnaala kannu thoongala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukunnu puriyalaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethukunnu puriyalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukunnu Puriyala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethukunnu Puriyala"/>
</div>
</pre>