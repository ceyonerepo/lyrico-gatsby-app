---
title: "jala jala jala song lyrics"
album: "Uppena"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Buchi Babu Sana"
path: "/albums/uppena-lyrics"
song: "Jala Jala Jala Patham Nuvvu"
image: ../../images/albumart/uppena.jpg
date: 2021-02-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/PTpimuHzlvE"
type: "love"
singers:
  - Shreya Ghoshal
  - Jaspreet Jasz
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jala Jala Jala Jalapatham Nuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jala Jala Jala Jalapatham Nuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sela Sela Sela Selayeruni Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sela Sela Sela Selayeruni Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sala Sala Nuvu Thakithe Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sala Sala Nuvu Thakithe Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongey Varadhaipothanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongey Varadhaipothanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chali Chali Chali Galivi Nuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chali Chali Chali Galivi Nuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiru Chiru Chiru Alaney Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiru Chiru Chiru Alaney Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chara Chara Nuvallithe Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chara Chara Nuvallithe Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegasey Karatannauthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegasey Karatannauthanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Mana Jantavaipu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Mana Jantavaipu "/>
</div>
<div class="lyrico-lyrics-wrapper">Jabilamma Thongi Chooseney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jabilamma Thongi Chooseney"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Itu Choodakantu Mabbu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Itu Choodakantu Mabbu "/>
</div>
<div class="lyrico-lyrics-wrapper">Remma Danni Moosensey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Remma Danni Moosensey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Neeti Chemma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Neeti Chemma "/>
</div>
<div class="lyrico-lyrics-wrapper">Theeracheleni Dhahamesense
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeracheleni Dhahamesense"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jala Jala Jala Patham Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jala Jala Jala Patham Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sela Sela Sela Selayeruni Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sela Sela Sela Selayeruni Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sala Sala Nuvu Thakithe Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sala Sala Nuvu Thakithe Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponge Varadhaipothanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponge Varadhaipothanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chali Chali Chali Galivi Nuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chali Chali Chali Galivi Nuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiru Chiru Chiru Alane Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiru Chiru Chiru Alane Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chara Chara Nuvallithe Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chara Chara Nuvallithe Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegase Karatannauthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegase Karatannauthanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samudhramantha Prema Muthyamantha Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samudhramantha Prema Muthyamantha Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaga Dhagi Untundi Lopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaga Dhagi Untundi Lopala"/>
</div>
<div class="lyrico-lyrics-wrapper">Akasamantha Pranayam Chukkalanti Hrudayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akasamantha Pranayam Chukkalanti Hrudayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaga Baitapaduthondi Eevela Huh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaga Baitapaduthondi Eevela Huh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadi Yedari Lanti Pranam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadi Yedari Lanti Pranam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thadi Meghanitho Prayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadi Meghanitho Prayanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Na Nunchi Ninnu Nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Na Nunchi Ninnu Nee "/>
</div>
<div class="lyrico-lyrics-wrapper">Nunchi Nannu Thenchaledhu Lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nunchi Nannu Thenchaledhu Lokam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jala Jala Jala Jalapatham Nuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jala Jala Jala Jalapatham Nuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sela Sela Sela Selayeruni Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sela Sela Sela Selayeruni Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sala Sala Nuvu Thakithe Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sala Sala Nuvu Thakithe Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongey Varadhaipothanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongey Varadhaipothanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilanti Theepi Roju Radhu Radhu Roju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilanti Theepi Roju Radhu Radhu Roju"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaga Vellipokunda Apadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaga Vellipokunda Apadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilanti Vana Jallu Thadapadhanta Vollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilanti Vana Jallu Thadapadhanta Vollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaga Dheenni Gundello Dachadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaga Dheenni Gundello Dachadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppudu Lenidhi Yekantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudu Lenidhi Yekantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkada Leni Yedho Prasantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkada Leni Yedho Prasantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Naalona Nuvu Neelona Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Naalona Nuvu Neelona Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manaku Maname Sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manaku Maname Sontham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jala Jala Jala Jalapatham Nuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jala Jala Jala Jalapatham Nuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sela Sela Sela Selayeruni Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sela Sela Sela Selayeruni Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sala Sala Nuvu Thakithe Nannu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sala Sala Nuvu Thakithe Nannu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pongey Varadhaipothanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongey Varadhaipothanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chali Chali Chali Galivi Nuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chali Chali Chali Galivi Nuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiru Chiru Chiru Alane Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiru Chiru Chiru Alane Nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chara Chara Nuvallithe Nannu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chara Chara Nuvallithe Nannu "/>
</div>
<div class="lyrico-lyrics-wrapper">Yegasey Karatannauthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegasey Karatannauthanu"/>
</div>
</pre>
