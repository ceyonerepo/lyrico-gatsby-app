---
title: "hey choosa song lyrics"
album: "Bheeshma"
artist: "Mahati Swara Sagar"
lyricist: "Krishna Chaitanya"
director: "Venky Kudumula"
path: "/albums/bheeshma-lyrics"
song: "Hey Choosa"
image: ../../images/albumart/bheeshma.jpg
date: 2020-02-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/MSez8vFUv3k"
type: "love"
singers:
  - Sanjana Kalmanje
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey nenu ni vaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nenu ni vaipu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu nannu chudanantha sepu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu nannu chudanantha sepu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhobuchulateho neetho baavundhi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhobuchulateho neetho baavundhi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa istam dhachukundhi choopu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa istam dhachukundhi choopu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kopam paiki kasepu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kopam paiki kasepu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthhuleni aashedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthhuleni aashedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhalo dhagunde raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalo dhagunde raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aligina adigina needhanini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aligina adigina needhanini"/>
</div>
<div class="lyrico-lyrics-wrapper">Mursina merisina ni vallane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mursina merisina ni vallane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalichina tharimina ni dhyasane oho oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalichina tharimina ni dhyasane oho oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Guppedu gundelo oo thondhare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppedu gundelo oo thondhare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvani adhbutham na mundhare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvani adhbutham na mundhare"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhuke vinthaga ee allare oho oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhuke vinthaga ee allare oho oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha na kosam aaratam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha na kosam aaratam"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhagani undhi chaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhagani undhi chaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo kottha momatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo kottha momatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vela kaani ee vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vela kaani ee vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha venta padi mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha venta padi mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanta padanu ga vichitramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanta padanu ga vichitramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vintha vaikari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vintha vaikari"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha vaaritho prayanamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha vaaritho prayanamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aligina adigina needhanini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aligina adigina needhanini"/>
</div>
<div class="lyrico-lyrics-wrapper">Mursina merisina ni vallane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mursina merisina ni vallane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalichina tharimina ni dhyasane oho oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalichina tharimina ni dhyasane oho oho"/>
</div>
<div class="lyrico-lyrics-wrapper">Guppedu gundelo oo thondhare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppedu gundelo oo thondhare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvani adhbutham na mundhare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvani adhbutham na mundhare"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhuke vinthaga ee allare oho oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhuke vinthaga ee allare oho oho"/>
</div>
</pre>
