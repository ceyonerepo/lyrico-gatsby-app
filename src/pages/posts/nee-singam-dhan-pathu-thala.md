---
title: nee singam dhan song lyrics
album: Pathu Thala
artist: A.R. Rahman
lyricist: Vivek
director: Obeli.N.Krishna
path: /albums/pathu-thala-lyrics
song: Nee Singam Dhan
image: ../../images/albumart/pathu-thala.jpg
date: 2023-03-25
lang: tamil
youtubeLink: https://www.youtube.com/embed/BryuGsrBEX4
type: mass
singers:
  - Sarthak Kalyani
---

<pre class="lyrics-native"></pre>
<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Suttri nindru oorae paarkka</div>

<div class="lyrico-lyrics-wrapper">Kalam kaanban</div>

<div class="lyrico-lyrics-wrapper">Punnagaiyil seanai vaala</div>

<div class="lyrico-lyrics-wrapper">Ranam kaanban</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un perai saaikka</div>

<div class="lyrico-lyrics-wrapper">Palayanigal serntha poodhae</div>

<div class="lyrico-lyrics-wrapper">Nee singam dhan</div>

<div class="lyrico-lyrics-wrapper">Andha aagayam poodhaatha</div>

<div class="lyrico-lyrics-wrapper">Paravai ondru</div>

<div class="lyrico-lyrics-wrapper">Nadhi kannadi paarthu</div>

<div class="lyrico-lyrics-wrapper">Manam nirainthathu indru</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadalal theeratha</div>

<div class="lyrico-lyrics-wrapper">Erumbin dhaagangal</div>

<div class="lyrico-lyrics-wrapper">Nilaiyin vaeladum</div>

<div class="lyrico-lyrics-wrapper">Panithuli theerkkum</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Theeyai nee pagirnthaalum</div>

<div class="lyrico-lyrics-wrapper">Rendaai vaalum</div>

<div class="lyrico-lyrics-wrapper">Ivanum andha thee</div>

<div class="lyrico-lyrics-wrapper">Poola thaan</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andha aagayam poodhaatha</div>

<div class="lyrico-lyrics-wrapper">Paravai ondru</div>

<div class="lyrico-lyrics-wrapper">Nadhi kannadi paarthu</div>

<div class="lyrico-lyrics-wrapper">Manam nirainthathu indru</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadalal theeratha</div>

<div class="lyrico-lyrics-wrapper">Erumbin dhaagangal</div>

<div class="lyrico-lyrics-wrapper">Nilaiyin vaeladum</div>

<div class="lyrico-lyrics-wrapper">Panithuli theerkkum</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yae paar endra thearukkul</div>

<div class="lyrico-lyrics-wrapper">Oorkolangal</div>

<div class="lyrico-lyrics-wrapper">Thear yaar sondham</div>

<div class="lyrico-lyrics-wrapper">Aanal than enna sol</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Malai kaatru</div>

<div class="lyrico-lyrics-wrapper">Maan kutti polae</div>

<div class="lyrico-lyrics-wrapper">Suyam indri vaalvan</div>

<div class="lyrico-lyrics-wrapper">Mann melae</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un nilathin malarai</div>

<div class="lyrico-lyrics-wrapper">Neeyum sirayinil yidalam</div>

<div class="lyrico-lyrics-wrapper">Adhan narumanam ellaiyai</div>

<div class="lyrico-lyrics-wrapper">Kadanthu veesum</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Humming  Hoo..hoo ooo oo</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andha aagayam poodhaatha</div>

<div class="lyrico-lyrics-wrapper">Paravai ondru</div>

<div class="lyrico-lyrics-wrapper">Nadhi kannadi paarthu</div>

<div class="lyrico-lyrics-wrapper">Manam nirainthathu indru</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadalal theeratha</div>

<div class="lyrico-lyrics-wrapper">Erumbin dhaagangal</div>

<div class="lyrico-lyrics-wrapper">Nilaiyin vaeladum</div>

<div class="lyrico-lyrics-wrapper">Panithuli theerkkum</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Puravo yaar ena</div>

<div class="lyrico-lyrics-wrapper">Neeyum ketkalaam</div>

<div class="lyrico-lyrics-wrapper">Oorellam sondham kondaadum</div>

<div class="lyrico-lyrics-wrapper">Silarin baedhathal</div>

<div class="lyrico-lyrics-wrapper">Saritham aalamaai</div>

<div class="lyrico-lyrics-wrapper">Kaalangal poonaalum pesum</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adhu yaar endra mudivu</div>

<div class="lyrico-lyrics-wrapper">Ingu yaarodum illai</div>

<div class="lyrico-lyrics-wrapper">Adhu nee endru ninaithal</div>

<div class="lyrico-lyrics-wrapper">Nee iraivan kai pillai</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pugal vanthaalum</div>

<div class="lyrico-lyrics-wrapper">Adhu kooda kadan than indru</div>

<div class="lyrico-lyrics-wrapper">Avan gredathai thanthalae</div>

<div class="lyrico-lyrics-wrapper">Gynanam enbaen</div>

<div class="lyrico-lyrics-wrapper">Nilavin yeani nee</div>

<div class="lyrico-lyrics-wrapper">Vilakkendru aanalum</div>

<div class="lyrico-lyrics-wrapper">Iravai ketkamal nilavoli veesum</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Theeyai nee pagirnthaalum</div>

<div class="lyrico-lyrics-wrapper">Rendaai vaalum</div>

<div class="lyrico-lyrics-wrapper">Ivanum andha thee</div>

<div class="lyrico-lyrics-wrapper">Poola thaan….</div>
</pre>