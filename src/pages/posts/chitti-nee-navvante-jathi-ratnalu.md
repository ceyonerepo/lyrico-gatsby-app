---
title: "mana jathiratnalu song lyrics"
album: "Jathi Ratnalu"
artist: "Radhan"
lyricist: "Kasarla Shyam"
director: "Anudeep KV"
path: "/albums/jathi-ratnalu-lyrics"
song: "Mana Jathi Ratnalau"
image: ../../images/albumart/jathi-ratnalu.jpg
date: 2021-03-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/oyHasipretM"
type: "happy"
singers:
  - Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Soo soodu herolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soo soodu herolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Otti buddara khaanulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti buddara khaanulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Value leni vajraalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Value leni vajraalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana jaathi rathnaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana jaathi rathnaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee suttu padhoorllu lere itlaantollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee suttu padhoorllu lere itlaantollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veellainaa puttaalante inko vandhellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veellainaa puttaalante inko vandhellu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Satellite kainaa chikkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satellite kainaa chikkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Veello gully rocketlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veello gully rocketlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Daily billugates ki mokke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daily billugates ki mokke"/>
</div>
<div class="lyrico-lyrics-wrapper">Veellai chillula pocketlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veellai chillula pocketlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suddhaapoosalu sonte maatalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suddhaapoosalu sonte maatalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindiki thimma raajulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindiki thimma raajulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pante levaru, lesthe aagaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pante levaru, lesthe aagaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paniki potharaajulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniki potharaajulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soo soodu herolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soo soodu herolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Otti buddara khaanulu value leni vajraalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti buddara khaanulu value leni vajraalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana jathi rathnalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana jathi rathnalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee suttu padhoollu lere itlaantollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee suttu padhoollu lere itlaantollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veellainaa puttaalante inko vandhellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veellainaa puttaalante inko vandhellu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaan jigar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaan jigar"/>
</div>
<div class="lyrico-lyrics-wrapper">Veellathoti polchaamante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veellathoti polchaamante"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharna chesthai kothulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharna chesthai kothulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veellugaani japam chesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veellugaani japam chesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooki chasthai kongalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooki chasthai kongalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorimeedha paddaarante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorimeedha paddaarante"/>
</div>
<div class="lyrico-lyrics-wrapper">Uresukuntai watch lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uresukuntai watch lu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veella kandlu paddayante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veella kandlu paddayante"/>
</div>
<div class="lyrico-lyrics-wrapper">Migiledhinkaa gocheelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migiledhinkaa gocheelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakistankaina potharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakistankaina potharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Free wifi choopisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free wifi choopisthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangladesh kaina vastharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangladesh kaina vastharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bottle ne ippisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottle ne ippisthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yengello ranga ungaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengello ranga ungaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Jadethaadu bongaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadethaadu bongaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Veellani gelikinonni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veellani gelikinonni"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathuku joosthe bhayankaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathuku joosthe bhayankaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Din ki batho se kaam kharab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Din ki batho se kaam kharab"/>
</div>
<div class="lyrico-lyrics-wrapper">Hath ki kamo se meen kharab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hath ki kamo se meen kharab"/>
</div>
<div class="lyrico-lyrics-wrapper">Veellani baguchedham annonkemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veellani baguchedham annonkemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimaak kharab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimaak kharab"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soo soodu herolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soo soodu herolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Otti buddara khaanulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti buddara khaanulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Value leni vajraalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Value leni vajraalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana jathi rathnalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana jathi rathnalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee suttu padhoollu lere itlaantollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee suttu padhoollu lere itlaantollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veellainaa puttaalante inko vandhellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veellainaa puttaalante inko vandhellu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veellu raasina supplymentlatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veellu raasina supplymentlatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Achheyochhu pusthakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achheyochhu pusthakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veella kathalu jeppukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veella kathalu jeppukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadipeyochhu oo shakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadipeyochhu oo shakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli maree lolli pette
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli maree lolli pette"/>
</div>
<div class="lyrico-lyrics-wrapper">Santi pillalu achhamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santi pillalu achhamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilli vella joliki raadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilli vella joliki raadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyyaru ganaja bichhamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyyaru ganaja bichhamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Izzath ki savaalante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izzath ki savaalante"/>
</div>
<div class="lyrico-lyrics-wrapper">Inti gadapa thokkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inti gadapa thokkaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Buddhi gaddi thinnaarante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhi gaddi thinnaarante"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoddi dhaari idavaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoddi dhaari idavaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bola hariloranga aa mokham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bola hariloranga aa mokham"/>
</div>
<div class="lyrico-lyrics-wrapper">Panganaamaalu vaalakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panganaamaalu vaalakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Moode paathralatho roju veedhi naatakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moode paathralatho roju veedhi naatakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Shambho linga ee thrikam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shambho linga ee thrikam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gappaalu arraachakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gappaalu arraachakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Babo, evaniki mooduthundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babo, evaniki mooduthundho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettaa undho jaathakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettaa undho jaathakam"/>
</div>
</pre>
