---
title: "eththanai kanavu kandirippom song lyrics"
album: "Thirumanam"
artist: "Siddharth Vipin"
lyricist: "Cheran"
director: "Cheran"
path: "/albums/thirumanam-lyrics"
song: "Eththanai Kanavu Kandirippom"
image: ../../images/albumart/thirumanam.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JBAxNpmSlsc"
type: "happy"
singers:
  - Jagadeesh
  - Ranjith Unni
  - Aparna
  - Kamalaja
  - Swagatha
  - Siddharth Vipin
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ethanai kanavu kandirupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai kanavu kandirupom"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanai perai ninaithirupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai perai ninaithirupom"/>
</div>
<div class="lyrico-lyrics-wrapper">athanai mugangal kadanthu inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athanai mugangal kadanthu inge"/>
</div>
<div class="lyrico-lyrics-wrapper">arugil amarum puthu uravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arugil amarum puthu uravu"/>
</div>
<div class="lyrico-lyrics-wrapper">aayul muluka varum uravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayul muluka varum uravu"/>
</div>
<div class="lyrico-lyrics-wrapper">naamum kodupom nalvaravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naamum kodupom nalvaravu"/>
</div>
<div class="lyrico-lyrics-wrapper">all the best best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all the best best"/>
</div>
<div class="lyrico-lyrics-wrapper">all the best best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all the best best"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bachulor life kum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bachulor life kum "/>
</div>
<div class="lyrico-lyrics-wrapper">inimel good bye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inimel good bye"/>
</div>
<div class="lyrico-lyrics-wrapper">fridns oda partikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fridns oda partikum"/>
</div>
<div class="lyrico-lyrics-wrapper">inimel good bye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inimel good bye"/>
</div>
<div class="lyrico-lyrics-wrapper">oru wife vanthaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru wife vanthaka"/>
</div>
<div class="lyrico-lyrics-wrapper">namma life eh change aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma life eh change aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">roopa sethaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roopa sethaka"/>
</div>
<div class="lyrico-lyrics-wrapper">antha life safe aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha life safe aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">all the best best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all the best best"/>
</div>
<div class="lyrico-lyrics-wrapper">all the best best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all the best best"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">inime unga personal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inime unga personal"/>
</div>
<div class="lyrico-lyrics-wrapper">prachana ethuva irunthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prachana ethuva irunthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">talk and share
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="talk and share"/>
</div>
<div class="lyrico-lyrics-wrapper">unga parents prachana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga parents prachana"/>
</div>
<div class="lyrico-lyrics-wrapper">perusa veduchalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perusa veduchalum"/>
</div>
<div class="lyrico-lyrics-wrapper">jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">ada yaru bestu yaru worstu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada yaru bestu yaru worstu"/>
</div>
<div class="lyrico-lyrics-wrapper">list venam eerakattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="list venam eerakattu"/>
</div>
<div class="lyrico-lyrics-wrapper">ego illatta epavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ego illatta epavum"/>
</div>
<div class="lyrico-lyrics-wrapper">happy than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="happy than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">don't worry don't worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="don't worry don't worry"/>
</div>
<div class="lyrico-lyrics-wrapper">don't worry don't worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="don't worry don't worry"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaaram oru naal long drive 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaram oru naal long drive "/>
</div>
<div class="lyrico-lyrics-wrapper">ponga joliya joliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponga joliya joliya"/>
</div>
<div class="lyrico-lyrics-wrapper">partiku pona thanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="partiku pona thanni "/>
</div>
<div class="lyrico-lyrics-wrapper">adinga joliya joliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adinga joliya joliya"/>
</div>
<div class="lyrico-lyrics-wrapper">sunday muluka thoongiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sunday muluka thoongiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ponga joliya joliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponga joliya joliya"/>
</div>
<div class="lyrico-lyrics-wrapper">oho samaika venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oho samaika venam"/>
</div>
<div class="lyrico-lyrics-wrapper">food order panunga joliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="food order panunga joliya"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyanam aanalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam aanalum"/>
</div>
<div class="lyrico-lyrics-wrapper">lovers ah irunga joliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lovers ah irunga joliya"/>
</div>
<div class="lyrico-lyrics-wrapper">honey moon photos ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="honey moon photos ah"/>
</div>
<div class="lyrico-lyrics-wrapper">instagram la upload
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="instagram la upload"/>
</div>
<div class="lyrico-lyrics-wrapper">pannunga joliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pannunga joliya"/>
</div>
<div class="lyrico-lyrics-wrapper">veetil epavum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetil epavum "/>
</div>
<div class="lyrico-lyrics-wrapper">selphone vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selphone vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">bed la epavume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bed la epavume"/>
</div>
<div class="lyrico-lyrics-wrapper">idaiveli vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaiveli vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">life onnu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life onnu than"/>
</div>
<div class="lyrico-lyrics-wrapper">ada wife onnu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada wife onnu than"/>
</div>
<div class="lyrico-lyrics-wrapper">irunga joliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irunga joliya"/>
</div>
<div class="lyrico-lyrics-wrapper">all the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all the best"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">intha chinan sirusugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha chinan sirusugal"/>
</div>
<div class="lyrico-lyrics-wrapper">siragadithu paranthidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragadithu paranthidave"/>
</div>
<div class="lyrico-lyrics-wrapper">all the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all the best"/>
</div>
<div class="lyrico-lyrics-wrapper">all the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all the best"/>
</div>
<div class="lyrico-lyrics-wrapper">all the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all the best"/>
</div>
<div class="lyrico-lyrics-wrapper">all the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all the best"/>
</div>
</pre>
