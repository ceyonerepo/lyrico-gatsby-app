---
title: "hey nijame song lyrics"
album: "Enai Noki Paayum Thota"
artist: "Darbuka Siva"
lyricist: "Madhan Karky"
director: "Gautham Vasudev Menon"
path: "/albums/enai-noki-paayum-thota-lyrics"
song: "Hey Nijame"
image: ../../images/albumart/enai-noki-paayum-thota.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WNC5rXl3lt0"
type: "love"
singers:
  - Bombay Jayashri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Nijamae Kalaiyadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nijamae Kalaiyadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu Nee Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu Nee Alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirindhida Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirindhida Vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">aayiram Muyaladhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram Muyaladhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerungida Vazhi Ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungida Vazhi Ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Solgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Solgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Konjam Pakkam Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Konjam Pakkam Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Solgiren Vaa Aruge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Solgiren Vaa Aruge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suzhaladhiru Ulagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhaladhiru Ulagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meenigazhunarvondrilae Vasikindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenigazhunarvondrilae Vasikindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudikkaa Muththangalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudikkaa Muththangalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Michangalil Vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Michangalil Vaazha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suttraadhiru Sattrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttraadhiru Sattrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Nodi Neela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Nodi Neela"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Pol Maaraadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pol Maaraadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theiyaadha Poompaathai Ondrodu Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theiyaadha Poompaathai Ondrodu Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyaadha Kaatraaga Ennodu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaadha Kaatraaga Ennodu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirkaadha Paattaaga Un Kaadhil Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirkaadha Paattaaga Un Kaadhil Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhaadha Urchaaga Ootraaga Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhaadha Urchaaga Ootraaga Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaraadha Inbaththu Paalaaga Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraadha Inbaththu Paalaaga Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraadha Theekkaamam Ondraaga Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraadha Theekkaamam Ondraaga Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongaatha Un Kannin Kanavaaga Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongaatha Un Kannin Kanavaaga Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoorathil Irundhaalum Piriyaadha Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoorathil Irundhaalum Piriyaadha Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasathin Vaasalil Thooranam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasathin Vaasalil Thooranam Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthaigal Thithikkum Kaaranam Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaigal Thithikkum Kaaranam Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaasithu Nee Ketta Iravaaga Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaasithu Nee Ketta Iravaaga Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkum Theriyaadha Uravaaga Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum Theriyaadha Uravaaga Nee"/>
</div>
</pre>
