---
title: "sadka kiya song lyrics"
album: "I Hate Luv Storys"
artist: "Vishal Shekhar"
lyricist: "Anvita Dutt Guptan"
director: "Punit Malhotra"
path: "/albums/i-hate-luv-storys-lyrics"
song: "Sadka Kiya Yun Ishq"
image: ../../images/albumart/i-hate-luv-storys.jpg
date: 2010-07-02
lang: hindi
youtubeLink: "https://www.youtube.com/embed/9hIgYEF42so"
type: "love"
singers:
  - Mahalakshmi Iyer
  - Suraj Jagan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kuchh khwaab dekhe hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh khwaab dekhe hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchh rang soche hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh rang soche hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab maine kal apne..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab maine kal apne.."/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang soche hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang soche hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Is raah mein jab bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is raah mein jab bhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu saath hoti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu saath hoti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kisson ki pannon si har baat hoti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisson ki pannon si har baat hoti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rooh jo hui meri fida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rooh jo hui meri fida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">To kal mein uthi koi sada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To kal mein uthi koi sada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke dil se hua juda juda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke dil se hua juda juda"/>
</div>
<div class="lyrico-lyrics-wrapper">Toota main is tarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toota main is tarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadka kiya yun ishq ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadka kiya yun ishq ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke sar jhuka jahan deedar hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke sar jhuka jahan deedar hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadka kiya yun ishq ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadka kiya yun ishq ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke sar jhuka jahan deedar hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke sar jhuka jahan deedar hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere mudne se suraj mud gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere mudne se suraj mud gaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere roshni ke saaye mein main dhoop si khili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere roshni ke saaye mein main dhoop si khili"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera aasman bhi chhota pad gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera aasman bhi chhota pad gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe jab se hai baahon mein teri panah mili..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe jab se hai baahon mein teri panah mili.."/>
</div>
<div class="lyrico-lyrics-wrapper">Woh thehari teri ada ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh thehari teri ada ada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke ruk bhi gaya mera Khuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke ruk bhi gaya mera Khuda"/>
</div>
<div class="lyrico-lyrics-wrapper">To mujhpe hai yeh asar hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To mujhpe hai yeh asar hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Toota main is tarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toota main is tarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadka kiya yun ishq ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadka kiya yun ishq ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke sar jhuka jahan deedar hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke sar jhuka jahan deedar hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadka kiya yun ishq ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadka kiya yun ishq ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke sar jhuka jahan deedar hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke sar jhuka jahan deedar hua"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuchh khwaab dekhe hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh khwaab dekhe hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchh rang soche hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh rang soche hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab maine kal apne..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab maine kal apne.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teri khushbu mein bheege khat mile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri khushbu mein bheege khat mile"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere rang ke syaahi se likhe, padhe, sune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere rang ke syaahi se likhe, padhe, sune"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri baaton ke woh saare silsile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri baaton ke woh saare silsile"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere dil ki kahani si sune, kahe, bune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere dil ki kahani si sune, kahe, bune"/>
</div>
<div class="lyrico-lyrics-wrapper">Main kar na sakun bayaan bayaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main kar na sakun bayaan bayaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke chup si hui meri zubaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke chup si hui meri zubaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh dil mehman hua hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh dil mehman hua hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Toota main is tarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toota main is tarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadka kiya yun ishq ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadka kiya yun ishq ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke sar jhuka jahan deedar hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke sar jhuka jahan deedar hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadka kiya yun ishq ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadka kiya yun ishq ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke sar jhuka jahan deedar hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke sar jhuka jahan deedar hua"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuchh khwaab dekhe hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh khwaab dekhe hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchh rang soche hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh rang soche hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab maine kal apne..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab maine kal apne.."/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang soche hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang soche hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Is raah mein jab bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is raah mein jab bhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu saath hoti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu saath hoti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kisson ki pannon si har baat hoti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisson ki pannon si har baat hoti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rooh jo hui meri fida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rooh jo hui meri fida"/>
</div>
<div class="lyrico-lyrics-wrapper">To kal mein uthi koi sada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To kal mein uthi koi sada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke dil se hua juda juda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke dil se hua juda juda"/>
</div>
<div class="lyrico-lyrics-wrapper">Toota main is tarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toota main is tarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadka kiya yun ishq ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadka kiya yun ishq ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke sar jhuka jahan deedar hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke sar jhuka jahan deedar hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadka kiya yun ishq ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadka kiya yun ishq ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ke sar jhuka jahan deedar hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ke sar jhuka jahan deedar hua"/>
</div>
</pre>
