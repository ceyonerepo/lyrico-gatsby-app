---
title: "potta kaatil poovasam song lyrics"
album: "Pariyerum Perumal"
artist: "Santhosh Narayanan"
lyricist: "Vivek"
director: "Mari Selvaraj"
path: "/albums/pariyerum-perumal-lyrics"
song: "Potta Kaatil Poovasam"
image: ../../images/albumart/pariyerum-perumal.jpg
date: 2018-09-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zRnPYVDIiJY"
type: "happy"
singers:
  - Yogi Sekar
  - Fareedha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Potta kaatil poovasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta kaatil poovasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sittu pulla saavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittu pulla saavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kaakum un nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kaakum un nesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullu mela aagasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullu mela aagasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potta kaatil poovasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta kaatil poovasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sittu pulla saavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittu pulla saavasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai kaakum un nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kaakum un nesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullu mela aagasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullu mela aagasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekkai vechan vaanathai alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkai vechan vaanathai alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi ninnen karanam illa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi ninnen karanam illa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooram nikka thevai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooram nikka thevai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda pakkam pul kanjathuthilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda pakkam pul kanjathuthilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Osai ippo yeruthu kaathil mella aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osai ippo yeruthu kaathil mella aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ikkaraiyil naanumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkaraiyil naanumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Meettuedukka paalam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meettuedukka paalam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathiramma paakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathiramma paakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan ivala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potta kaatil poovasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta kaatil poovasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sittu pulla saavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittu pulla saavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kaakum un nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kaakum un nesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullu mela aagasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullu mela aagasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potta kaatil poovasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta kaatil poovasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sittu pulla saavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittu pulla saavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kaakum un nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kaakum un nesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullu mela aagasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullu mela aagasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai vechen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathukanum ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathukanum ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera mugam kai vasam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera mugam kai vasam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu manam nikkavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu manam nikkavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Meratturen yen kekkavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meratturen yen kekkavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Venam daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaga padum thaana thavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaga padum thaana thavala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ikkaraiyil naanumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkaraiyil naanumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Meettu edukka paalam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meettu edukka paalam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathiramma paakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathiramma paakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan ivala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potta kaatil poovasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta kaatil poovasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sittu pulla saavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittu pulla saavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kaakum un nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kaakum un nesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullu mela aagasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullu mela aagasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potta kaatil poovasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta kaatil poovasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sittu pulla saavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittu pulla saavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kaakum un nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kaakum un nesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullu mela aagasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullu mela aagasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninna mazhai koora otti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna mazhai koora otti"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu rendu thoovi vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu rendu thoovi vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu vanthu serntha pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu vanthu serntha pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenappil thoovi vachan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenappil thoovi vachan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panjarathu thookku naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjarathu thookku naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu nodi kozhi nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu nodi kozhi nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasa maathunaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasa maathunaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja avan kottil vachan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja avan kottil vachan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solla vantha varthai ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla vantha varthai ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thonda kuzhi thaanda villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thonda kuzhi thaanda villai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maya thirai pottu ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya thirai pottu ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullai adaichan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullai adaichan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekkai vechan vaanathai alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkai vechan vaanathai alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi ninnen karanam illa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi ninnen karanam illa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ikkaraiyil naanumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkaraiyil naanumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Meettu edukka paalam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meettu edukka paalam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathiramma paakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathiramma paakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan ivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan ivala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potta kaatil poovasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta kaatil poovasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sittu pulla saavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittu pulla saavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kaakum un nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kaakum un nesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullu mela aagasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullu mela aagasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaa aaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaa aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potta kaatil poovasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta kaatil poovasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sittu pulla saavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittu pulla saavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kaakum un nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kaakum un nesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullu mela aagasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullu mela aagasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potta kaatil poovasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta kaatil poovasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sittu pulla saavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittu pulla saavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kaakum un nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kaakum un nesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullu mela aagasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullu mela aagasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaa aaaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Potta kaatil poovasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta kaatil poovasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sittu pulla saavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittu pulla saavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kaakum un nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kaakum un nesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullu mela aagasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullu mela aagasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaa aaaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maya thirai pottu puttan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya thirai pottu puttan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullai vechu pootti puttan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullai vechu pootti puttan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maya thirai pottu puttan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya thirai pottu puttan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullai vechu pootti puttan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullai vechu pootti puttan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maya thirai pottu puttan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya thirai pottu puttan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullai vechu pootti puttan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullai vechu pootti puttan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maya thirai pottu puttan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maya thirai pottu puttan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullai vechu pootti puttan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullai vechu pootti puttan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaa aaaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmm mmmooo oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm mmmooo oooo"/>
</div>
</pre>
