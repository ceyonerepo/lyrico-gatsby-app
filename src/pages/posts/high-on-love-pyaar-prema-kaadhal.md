---
title: "high on love song lyrics"
album: "Pyaar Prema Kaadhal"
artist: "Yuvan Shankar Raja"
lyricist: "Niranjan Bharathi"
director: "Elan"
path: "/albums/pyaar-prema-kaadhal-lyrics"
song: "High On Love"
image: ../../images/albumart/pyaar-prema-kaadhal.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IGI4jnKn6IU"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haanaaauuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haanaaauuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey pennae en nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pennae en nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Saainthu saaikiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saainthu saaikiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee arugil puriyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee arugil puriyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayam seigirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayam seigirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai polavae naan ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai polavae naan ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangi kirangi thaan ponenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangi kirangi thaan ponenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhaiyaaga thaan aanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhaiyaaga thaan aanenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalladum jeevanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalladum jeevanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jannal oramaai munnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannal oramaai munnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey minnal polavae vanthaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey minnal polavae vanthaaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnai thandi oru sorgaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai thandi oru sorgaththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil engumae thanthaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil engumae thanthaaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyae neengi nee vilagathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyae neengi nee vilagathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodiyum en manam thangathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodiyum en manam thangathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna nerumo theriyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna nerumo theriyathae"/>
</div>
<div class="lyrico-lyrics-wrapper">En jeevan yenguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jeevan yenguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirinai vathaithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirinai vathaithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En idhayathil amarunthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En idhayathil amarunthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arasi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arasi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En udalinil nathiyaai odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En udalinil nathiyaai odum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthiram neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiram neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un siripinil kavithaigal Kalanguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un siripinil kavithaigal Kalanguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mozhigalil isaigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mozhigalil isaigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un iru vizhi minnal yendha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un iru vizhi minnal yendha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam yenguthae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam yenguthae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkul enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkul enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kaangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyil solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyil solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthaigal thevaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaigal thevaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthum un ithalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthum un ithalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha varthai sollumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha varthai sollumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuruvi polavae en ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruvi polavae en ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaththi thaavuthae unnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaththi thaavuthae unnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhanthai polavae en kaalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhanthai polavae en kaalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi thiriyuthae pinnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi thiriyuthae pinnaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeyai polavae en dhegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyai polavae en dhegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththi yeriyuthae thannalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththi yeriyuthae thannalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruvi polavae anandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruvi polavae anandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillamal paayuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillamal paayuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey pennae en nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pennae en nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Saainthu saaikiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saainthu saaikiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee arugil puriyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee arugil puriyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayam seigirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayam seigirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooohooohoooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooohooohoooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar prema kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar prema kaadhal"/>
</div>
</pre>
