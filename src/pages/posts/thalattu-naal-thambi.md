---
title: "thalattu naal song lyrics"
album: "Thambi"
artist: "Govind Vasantha"
lyricist: "Vivek"
director: "Jeethu Joseph"
path: "/albums/thambi-lyrics"
song: "Thalattu Naal"
image: ../../images/albumart/thambi.jpg
date: 2019-12-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UlgX84phZy8"
type: "happy"
singers:
  - Crishna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kai paiyil sadhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai paiyil sadhosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathoda poo vesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathoda poo vesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannaththu thaalukku anbaagurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannaththu thaalukku anbaagurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhoomikku venumnnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomikku venumnnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vin thaavum meen onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vin thaavum meen onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaalae innaala kondadurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaalae innaala kondadurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endha sogangal vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha sogangal vandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unnai suththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unnai suththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppen thaduppen nee poothida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppen thaduppen nee poothida"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha saamiku munnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha saamiku munnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ketpathellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ketpathellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichu sezhichi nee vaazhnthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichu sezhichi nee vaazhnthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalattu naal undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalattu naal undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalattu naal ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalattu naal ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalattu naal unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalattu naal unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarattum naal andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarattum naal andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Megangal thaen sotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megangal thaen sotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrellaam kai thatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrellaam kai thatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomikku nee vandhaThaalattu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomikku nee vandhaThaalattu "/>
</div>
<div class="lyrico-lyrics-wrapper">naal kooruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naal kooruven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam vaazhkaiyae mann seravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam vaazhkaiyae mann seravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam petra kadan thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam petra kadan thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai theerkkavae elloridam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai theerkkavae elloridam"/>
</div>
<div class="lyrico-lyrics-wrapper">Siripputti nagarndhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siripputti nagarndhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvil nee indru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil nee indru "/>
</div>
<div class="lyrico-lyrics-wrapper">kaanpikkum paasangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanpikkum paasangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Verellaamae vendaatha veshangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verellaamae vendaatha veshangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhaavo kanaavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhaavo kanaavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhaatha vinavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhaatha vinavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaigal mele nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaigal mele nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh saaikum nesangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh saaikum nesangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalattu naal undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalattu naal undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalattu naal ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalattu naal ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalattu naal unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalattu naal unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarattum naal andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarattum naal andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Megangal thaen sotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megangal thaen sotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrellaam kai thatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrellaam kai thatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomikku nee vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomikku nee vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalattu naal kooruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalattu naal kooruven"/>
</div>
</pre>
