---
title: "drunk and drive song lyrics"
album: "Chalo"
artist: "Mahati Swara Sagar"
lyricist: "Shyam Kasrala"
director: "Venky Kudumula"
path: "/albums/chalo-lyrics"
song: "Drunk and Drive"
image: ../../images/albumart/chalo.jpg
date: 2018-02-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/vyp3fatQa9g"
type: "love"
singers:
  - Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Choosthunte poovula shape u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthunte poovula shape u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kani poolan devi type uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kani poolan devi type uu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oye abba cha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye abba cha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sentimental anipisthabey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sentimental anipisthabey"/>
</div>
<div class="lyrico-lyrics-wrapper">Naku mental teppisthavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naku mental teppisthavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oye abba cha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye abba cha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O chandamama laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O chandamama laga"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayatki buildup isthavey ye ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayatki buildup isthavey ye ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandramukhi laga lopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandramukhi laga lopala"/>
</div>
<div class="lyrico-lyrics-wrapper">Eshal vesthave ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eshal vesthave ye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orginal ni urgent ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orginal ni urgent ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Choodali ani unde ye ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choodali ani unde ye ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Rainbow laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rainbow laga"/>
</div>
<div class="lyrico-lyrics-wrapper">Full open ayipove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full open ayipove"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tekkulu appavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tekkulu appavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Trickulu appavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trickulu appavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikkinavve nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikkinavve nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Drunk n driving la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drunk n driving la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O race car la dosukellaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O race car la dosukellaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Breakulesi nuvvu okay cheppala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Breakulesi nuvvu okay cheppala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teeskukelli slippule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teeskukelli slippule"/>
</div>
<div class="lyrico-lyrics-wrapper">Pass gaavu supply le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pass gaavu supply le"/>
</div>
<div class="lyrico-lyrics-wrapper">Computer kanipitettinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Computer kanipitettinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cutting lu iyyodhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cutting lu iyyodhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Average beauty ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Average beauty ve"/>
</div>
<div class="lyrico-lyrics-wrapper">RGV tweet ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="RGV tweet ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Tokkalo tikkalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tokkalo tikkalu "/>
</div>
<div class="lyrico-lyrics-wrapper">chupi brathikeyodde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chupi brathikeyodde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">B.Com lo physics undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="B.Com lo physics undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ane baapathu nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ane baapathu nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana madhya Chemistry ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana madhya Chemistry ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ardam cheskove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ardam cheskove"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bill gates bidda ayinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bill gates bidda ayinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Buildup lu vadde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buildup lu vadde"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa dil lo gate 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa dil lo gate "/>
</div>
<div class="lyrico-lyrics-wrapper">teriche unchane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teriche unchane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tekkulu appavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tekkulu appavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Trickulu appavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trickulu appavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikkinavve nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikkinavve nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Drunk n driving la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drunk n driving la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo race car 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo race car "/>
</div>
<div class="lyrico-lyrics-wrapper">la dosukellaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="la dosukellaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Breakulesi nuvvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Breakulesi nuvvu "/>
</div>
<div class="lyrico-lyrics-wrapper">okay cheppala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okay cheppala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Holiday triplaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Holiday triplaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Everyday treatu la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everyday treatu la"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu naa chentaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu naa chentaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaste nee la undoche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaste nee la undoche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rules neeku undavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rules neeku undavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Boundary lu aasal undavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boundary lu aasal undavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mansuki maske vese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mansuki maske vese"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshaname raade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshaname raade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Right ayina wrong 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right ayina wrong "/>
</div>
<div class="lyrico-lyrics-wrapper">ayina naa vote neeke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayina naa vote neeke"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vente nenunta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vente nenunta "/>
</div>
<div class="lyrico-lyrics-wrapper">veedani shadow la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veedani shadow la"/>
</div>
<div class="lyrico-lyrics-wrapper">Bad ayina sad ayina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bad ayina sad ayina "/>
</div>
<div class="lyrico-lyrics-wrapper">dhaatali nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaatali nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Kastu unta ninne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kastu unta ninne "/>
</div>
<div class="lyrico-lyrics-wrapper">pranam la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pranam la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tekku lu appakaey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tekku lu appakaey"/>
</div>
<div class="lyrico-lyrics-wrapper">Tricku lu appakay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tricku lu appakay"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkasari nuvvu natho cheraka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkasari nuvvu natho cheraka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo race car la dooskuvellavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo race car la dooskuvellavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Break lu veyake okay cheppaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break lu veyake okay cheppaka"/>
</div>
</pre>
