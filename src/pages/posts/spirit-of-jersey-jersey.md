---
title: "spirit of jersey song lyrics"
album: "Jersey"
artist: "Anirudh Ravichander"
lyricist: "Krishna Kanth"
director: "Gowtam Tinnanuri"
path: "/albums/jersey-lyrics"
song: "Spirit Of Jersey"
image: ../../images/albumart/jersey.jpg
date: 2019-04-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/_xuI60USDjw"
type: "mass"
singers:
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anigi Manigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anigi Manigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Alalika Egasenu Choodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alalika Egasenu Choodaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalu Avadhalu Levura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalu Avadhalu Levura"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupu Darikika Cheraneeka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupu Darikika Cheraneeka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadraa Malupu Malupuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadraa Malupu Malupuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheragani Gurutulu Veedara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheragani Gurutulu Veedara"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalu Merupulu Choopara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalu Merupulu Choopara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu Sagamuga Maaripoyi Oderaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu Sagamuga Maaripoyi Oderaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupe Adugaduguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupe Adugaduguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluge Ninu Alimena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluge Ninu Alimena"/>
</div>
<div class="lyrico-lyrics-wrapper">Digulepade Marugunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digulepade Marugunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madale Ika Samaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madale Ika Samaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padina Bedaraka Padaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padina Bedaraka Padaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruge Vijayamu Kadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruge Vijayamu Kadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Urike Chematala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urike Chematala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai Kadilenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai Kadilenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Tagalaga Meghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tagalaga Meghame"/>
</div>
<div class="lyrico-lyrics-wrapper">Egurika Ningi Vipuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egurika Ningi Vipuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolavani Vegame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolavani Vegame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegame"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulo  Choopatanike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulo  Choopatanike"/>
</div>
<div class="lyrico-lyrics-wrapper">Marichina Thaarave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marichina Thaarave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarave"/>
</div>
<div class="lyrico-lyrics-wrapper">Musugika Nedu Veedele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musugika Nedu Veedele"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugula Daahame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugula Daahame"/>
</div>
<div class="lyrico-lyrics-wrapper">Daahame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daahame"/>
</div>
<div class="lyrico-lyrics-wrapper">Baruvika Telikaayele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baruvika Telikaayele"/>
</div>
<div class="lyrico-lyrics-wrapper">Anigi Manigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anigi Manigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Alalika Egasenu Choodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alalika Egasenu Choodaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalu Avadhalu Levura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalu Avadhalu Levura"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupu Darikika Cheraneeka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupu Darikika Cheraneeka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadraa Malupu Malupuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadraa Malupu Malupuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheragani Gurutulu Veedara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheragani Gurutulu Veedara"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalu Merupulu Choopara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalu Merupulu Choopara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu Sagamuga Maaripoyi Oderaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu Sagamuga Maaripoyi Oderaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamanaalane Gamanincharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamanaalane Gamanincharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamanaalane Gamanincharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamanaalane Gamanincharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Okaroju Gamyam Eduravadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okaroju Gamyam Eduravadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamanaalane Gurichoodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamanaalane Gurichoodaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Nela Niku Vashamavadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Nela Niku Vashamavadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamanaalane Gamanincharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamanaalane Gamanincharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamanaalane Gamanincharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamanaalane Gamanincharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Okaroju Gamyam Eduravadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okaroju Gamyam Eduravadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamanaalane Gurichoodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamanaalane Gurichoodaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Nela Niku Vashamavadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Nela Niku Vashamavadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidugu Valene Paduthu Kalupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidugu Valene Paduthu Kalupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Ee Ningee Nela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Ee Ningee Nela"/>
</div>
<div class="lyrico-lyrics-wrapper">Urumu Merupu Barilo Nilupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urumu Merupu Barilo Nilupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Antha Needera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Antha Needera"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugu Kadupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugu Kadupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayamu Jagamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayamu Jagamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sontham Ayyela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sontham Ayyela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiki Eduru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiki Eduru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilichi Gelichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilichi Gelichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pantham Choopela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pantham Choopela"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagalaka Meghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagalaka Meghame"/>
</div>
<div class="lyrico-lyrics-wrapper">Tagalaga Meghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tagalaga Meghame"/>
</div>
<div class="lyrico-lyrics-wrapper">Egurika Ningi Vipuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egurika Ningi Vipuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolavani Vegame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolavani Vegame"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulo  Choopatanike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulo  Choopatanike"/>
</div>
<div class="lyrico-lyrics-wrapper">Marichina Thaarave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marichina Thaarave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarave"/>
</div>
<div class="lyrico-lyrics-wrapper">Musugika Nedu Veedele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musugika Nedu Veedele"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugula Daahame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugula Daahame"/>
</div>
<div class="lyrico-lyrics-wrapper">Daahame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daahame"/>
</div>
<div class="lyrico-lyrics-wrapper">Baruvika Telikaayele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baruvika Telikaayele"/>
</div>
<div class="lyrico-lyrics-wrapper">Anigi Manigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anigi Manigina"/>
</div>
<div class="lyrico-lyrics-wrapper">Alalika Egasenu Choodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alalika Egasenu Choodaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalu Avadhalu Levura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalu Avadhalu Levura"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupu Darikika Cheraneeka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupu Darikika Cheraneeka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadraa Malupu Malupuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadraa Malupu Malupuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheragani Gurutulu Veedara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheragani Gurutulu Veedara"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalu Merupulu Choopara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalu Merupulu Choopara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu Sagamuga Maaripoyi Oderaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu Sagamuga Maaripoyi Oderaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupe Adugaduguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupe Adugaduguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluge Ninu Alimena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluge Ninu Alimena"/>
</div>
<div class="lyrico-lyrics-wrapper">Digulepade Marugunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Digulepade Marugunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madale Ika Samaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madale Ika Samaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Padina Bedaraka Padaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padina Bedaraka Padaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruge Vijayamu Kadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruge Vijayamu Kadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Urike Chematala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urike Chematala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai Kadilenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai Kadilenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Tagalaga Meghame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tagalaga Meghame"/>
</div>
<div class="lyrico-lyrics-wrapper">Egurika Ningi Vipuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egurika Ningi Vipuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolavani Vegame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolavani Vegame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegame"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulo  Choopatanike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulo  Choopatanike"/>
</div>
<div class="lyrico-lyrics-wrapper">Marichina Thaarave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marichina Thaarave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarave"/>
</div>
<div class="lyrico-lyrics-wrapper">Musugika Nedu Veedele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musugika Nedu Veedele"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugula Daahame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugula Daahame"/>
</div>
<div class="lyrico-lyrics-wrapper">Daahame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daahame"/>
</div>
<div class="lyrico-lyrics-wrapper">Baruvika Telikaayele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baruvika Telikaayele"/>
</div>
</pre>
