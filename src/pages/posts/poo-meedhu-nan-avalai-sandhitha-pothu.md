---
title: "poo meedhu song lyrics"
album: "Nan Avalai Sandhitha Pothu"
artist: "Hithesh Murugavel- Jai"
lyricist: "Na Muthukumar"
director: "L G Ravichnadar"
path: "/albums/nan-avalai-sandhitha-pothu-lyrics"
song: "Poo Meedhu"
image: ../../images/albumart/nan-avalai-sandhitha-pothu.jpg
date: 2019-12-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0uh1QgPyvrg"
type: "affection"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">poo methu paniyai parthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo methu paniyai parthene"/>
</div>
<div class="lyrico-lyrics-wrapper">ada gyana penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada gyana penne"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukul eeram kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukul eeram kondene"/>
</div>
<div class="lyrico-lyrics-wrapper">poi manai thedi sendrene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poi manai thedi sendrene"/>
</div>
<div class="lyrico-lyrics-wrapper">ada gyana penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada gyana penne"/>
</div>
<div class="lyrico-lyrics-wrapper">mei anbil matti kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mei anbil matti kondene"/>
</div>
<div class="lyrico-lyrics-wrapper">malai ange megam mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai ange megam mele"/>
</div>
<div class="lyrico-lyrics-wrapper">kadal inge boomi mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal inge boomi mele"/>
</div>
<div class="lyrico-lyrics-wrapper">rendum than ondrai kuriyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendum than ondrai kuriyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">sirpikul muthai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirpikul muthai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">muthin mel pithai mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthin mel pithai mari"/>
</div>
<div class="lyrico-lyrics-wrapper">manam indru oonjal aadiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam indru oonjal aadiyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">nadakatum betham sonnathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadakatum betham sonnathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">nam valkai ingu nool mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam valkai ingu nool mele"/>
</div>
<div class="lyrico-lyrics-wrapper">aadum bommai adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadum bommai adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poo methu paniyai parthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo methu paniyai parthene"/>
</div>
<div class="lyrico-lyrics-wrapper">ada gyana penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada gyana penne"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukul eeram kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukul eeram kondene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alagiya iraiva oru varam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagiya iraiva oru varam "/>
</div>
<div class="lyrico-lyrics-wrapper">kodu vaa en uyir kathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodu vaa en uyir kathu"/>
</div>
<div class="lyrico-lyrics-wrapper">parisali da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parisali da"/>
</div>
<div class="lyrico-lyrics-wrapper">ennuda thane uthavida da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennuda thane uthavida da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poo methu paniyai parthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo methu paniyai parthene"/>
</div>
<div class="lyrico-lyrics-wrapper">ada gyana penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada gyana penne"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukul eeram kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukul eeram kondene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennulle en uyir 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennulle en uyir "/>
</div>
<div class="lyrico-lyrics-wrapper">kai neeti alaithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai neeti alaithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">inbathai nan enni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbathai nan enni "/>
</div>
<div class="lyrico-lyrics-wrapper">kathu irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathu irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">anbe un malalaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbe un malalaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">poo pondra punnagaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo pondra punnagaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">aanantha kaneerile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanantha kaneerile"/>
</div>
<div class="lyrico-lyrics-wrapper">karaithu irupen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karaithu irupen "/>
</div>
<div class="lyrico-lyrics-wrapper">thanthaikum thaimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthaikum thaimai"/>
</div>
<div class="lyrico-lyrics-wrapper">undu endru sonnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undu endru sonnal"/>
</div>
<div class="lyrico-lyrics-wrapper">unnale valthida virumbukiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnale valthida virumbukiren"/>
</div>
<div class="lyrico-lyrics-wrapper">inbathai mattum than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbathai mattum than"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kaana vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kaana vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">un vaalkai valam pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vaalkai valam pera"/>
</div>
<div class="lyrico-lyrics-wrapper">vendukiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendukiren"/>
</div>
<div class="lyrico-lyrics-wrapper">malai thuli varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai thuli varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">manam athai tharuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam athai tharuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thannuyir kathu veli varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannuyir kathu veli varuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poo methu paniyai parthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo methu paniyai parthene"/>
</div>
<div class="lyrico-lyrics-wrapper">ada gyana penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada gyana penne"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukul eeram kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukul eeram kondene"/>
</div>
</pre>
