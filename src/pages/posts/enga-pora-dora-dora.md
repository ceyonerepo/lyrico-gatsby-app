---
title: "enga pora dora song lyrics"
album: "Dora"
artist: "Vivek–Mervin"
lyricist: "Mohan Rajan"
director: "Doss Ramasamy"
path: "/albums/dora-lyrics"
song: "Enga Pora Dora"
image: ../../images/albumart/dora.jpg
date: 2017-03-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cYzC3XKCbd8"
type: "happy"
singers:
  - Mervin Solomon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enga pora enga pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora enga pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pora dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora dora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh nenjodu nenjaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh nenjodu nenjaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhaayae adadadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhaayae adadadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannodu kannaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannodu kannaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindraayae azhagazhaga aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindraayae azhagazhaga aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga pora enga pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora enga pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pora dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora dora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjodu nenjaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu nenjaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhaayae adadadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhaayae adadadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannodu kannaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannodu kannaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindraayae azhagazhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindraayae azhagazhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Baashai ondraai maarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baashai ondraai maarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal moodi unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal moodi unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Konja kaalam podhumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja kaalam podhumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neril vandha ammaa appaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neril vandha ammaa appaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan theriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna indha paasam endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna indha paasam endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnaal puriyumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnaal puriyumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjodu nenjaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu nenjaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhaayae adadadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhaayae adadadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannodu kannaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannodu kannaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindraayae azhagazhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindraayae azhagazhagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pora dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora dora"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu pora thottu pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu pora thottu pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogaadha dora dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogaadha dora dora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pora dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora dora"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu pora thottu pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu pora thottu pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogaadha dora dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogaadha dora dora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En idhaya saththam kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En idhaya saththam kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongum un azhagai paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongum un azhagai paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum ezhundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum ezhundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga marappen adhigaalaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga marappen adhigaalaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kodukkum muththam vaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kodukkum muththam vaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Un arugil nerungum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un arugil nerungum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum unai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum unai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Maara ninaippen manadhorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maara ninaippen manadhorathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh vilaiyaadum bommai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh vilaiyaadum bommai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhupola nee vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhupola nee vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalaattum anbil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaattum anbil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai pola maarinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai pola maarinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee illaa vazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaa vazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu vendaam eppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu vendaam eppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vazhum ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vazhum ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu podhum ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu podhum ingae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pora dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora dora"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu pora thottu pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu pora thottu pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogaadha dora dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogaadha dora dora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pora dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora dora"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu pora thottu pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu pora thottu pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogaadha dora dora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogaadha dora dora"/>
</div>
</pre>
