---
title: "eeran mukil song lyrics"
album: "Cold Case"
artist: "Prakash Alex"
lyricist: "Sreenath V nath"
director: "Tanu Balak"
path: "/albums/cold-case-lyrics"
song: "Eeran Mukil"
image: ../../images/albumart/cold-case.jpg
date: 2021-06-30
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/VZwzdPQ5qGU"
type: "melody"
singers:
  - Harshankar KS
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Eeran mukil mashiyaale nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeran mukil mashiyaale nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthum mizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthum mizhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoraatha nin mizhineerinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoraatha nin mizhineerinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Marayum chiriyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marayum chiriyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelea nilaavoornnu veezhunna theerangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelea nilaavoornnu veezhunna theerangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarea nizhalariyathe thirayunnathaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarea nizhalariyathe thirayunnathaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanal veena vazhiyakhi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanal veena vazhiyakhi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane pokunnuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane pokunnuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaleezhumoru thoniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaleezhumoru thoniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaane thuzhayunnuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaane thuzhayunnuvo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eeran mukil mashiyaale nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeran mukil mashiyaale nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthum mizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthum mizhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthum mizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthum mizhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoraatha nin mizhineerinaal Marayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoraatha nin mizhineerinaal Marayum"/>
</div>
</pre>
