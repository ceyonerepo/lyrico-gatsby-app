---
title: "maaya bazaaru song lyrics"
album: "Pakkiri"
artist: "Amit Trivedi"
lyricist: "Madhan Karky"
director: "Ken Scott"
path: "/albums/pakkiri-lyrics"
song: "Maaya Bazaaru"
image: ../../images/albumart/pakkiri.jpg
date: 2019-06-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/M5tPuKU0DYY"
type: "happy"
singers:
  - Benny Dayal
  - Nikhita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ong Kutti Nethi Vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ong Kutti Nethi Vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Noolu Onna Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Noolu Onna Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaathaadi Pannattumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaathaadi Pannattumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadi En Raasaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi En Raasaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Om Poli Kobam Aathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om Poli Kobam Aathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ong Kann Renda Thinnattumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ong Kann Renda Thinnattumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayaa Basaaru Bazaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaa Basaaru Bazaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamaa Konjam Usaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaa Konjam Usaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhiram Thoovattumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiram Thoovattumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayaa Basaaru Bazaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaa Basaaru Bazaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamaa Konjam Usaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaa Konjam Usaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhiram Thoovattumaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiram Thoovattumaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Roasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Roasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Engitta Mullaala Pesaadhadee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engitta Mullaala Pesaadhadee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Manmadha Raasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Manmadha Raasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhiram Pottennaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiram Pottennaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhaan Radhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaan Radhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vatta Vatta Vennilaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta Vatta Vennilaava"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavaraichu Maavaraichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavaraichu Maavaraichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhosa Chuttu Oottattumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhosa Chuttu Oottattumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Currentu Kattu Aana Vaanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Currentu Kattu Aana Vaanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Om Moochiya Maattivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om Moochiya Maattivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelavunnu Kaattattumaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelavunnu Kaattattumaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Iduppil Thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Iduppil Thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Hip Hoppu Aadattumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Hip Hoppu Aadattumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Udhatta Pootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Udhatta Pootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Lip Lockku Podattumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Lip Lockku Podattumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Pola Vithakkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pola Vithakkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum Illa Kettuppaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Illa Kettuppaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Engooril Poi Keludee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engooril Poi Keludee"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Dee Un Kaadhalgaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Dee Un Kaadhalgaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaa Naanum Paathukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaa Naanum Paathukkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Inime En Aaludee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Inime En Aaludee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodhai Yeravilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai Yeravilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkam Kooda Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkam Kooda Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ummela Naan Yen Saayuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ummela Naan Yen Saayuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Indha Tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Indha Tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Nethu Vara Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nethu Vara Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannaala Naan Maaruren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannaala Naan Maaruren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayaa Basaaru Bazaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaa Basaaru Bazaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamaa Konjam Usaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaa Konjam Usaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhiram Thoovapporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiram Thoovapporen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayaa Basaaru Bazaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaa Basaaru Bazaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamaa Konjam Usaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaa Konjam Usaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhiram Thoovapporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiram Thoovapporen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandhiram Thoovattumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiram Thoovattumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhiram Thoovattumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiram Thoovattumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhiram Thoovattumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiram Thoovattumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhiram Thoovattumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiram Thoovattumaa"/>
</div>
</pre>
