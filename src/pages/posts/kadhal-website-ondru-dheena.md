---
title: "kadhal website ondru song lyrics"
album: "Dheena"
artist: "Yuvan Shankar Raja"
lyricist: "Vaali"
director: "A.R. Murugadoss"
path: "/albums/dheena-lyrics"
song: "Kadhal Website Ondru"
image: ../../images/albumart/dheena.jpg
date: 2001-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VpfJSSTGYXA"
type: "love"
singers:
  - Shankar Mahadevan
  - Harini
  - Timmy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaadhal Website Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Website Ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanden Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanden Kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum Kangal Rendil Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Kangal Rendil Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Virus Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Virus Vanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Computer Pol Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Computer Pol Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Confuse Aanen Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confuse Aanen Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hardware Ullam Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hardware Ullam Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponathenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Software Endrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Software Endrae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Aanathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Aanathenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Something Enakkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Something Enakkulle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nernthathenna Unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nernthathenna Unthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal Ennai Kadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Ennai Kadathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poga Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Centimeter Thootharum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Centimeter Thootharum Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Cassette
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Cassette"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharuvarharkillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvarharkillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thottren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thottren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnidam Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam Ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Love You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Love You"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dangerous Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dangerous Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Endrum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnidam Kaithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam Kaithi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">News Channel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="News Channel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollume Seithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollume Seithi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Acupuncture
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acupuncture"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needle La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needle La"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Turkey Chicken Noodle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Turkey Chicken Noodle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Aadai Konjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Aadai Konjum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unthan Idaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Idaiyilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Donald Duck In
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donald Duck In"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaathiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaathiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Disney Dolphin Jodiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disney Dolphin Jodiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Aadi Chellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Aadi Chellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unthan Nadaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Nadaiyilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Website Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Website Ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Website Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Website Ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanden Kanden Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanden Kanden Naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal Rendil Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Rendil Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hot Boxil Vaitha Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot Boxil Vaitha Poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unbathillai Ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unbathillai Ini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvil Entha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvil Entha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalum En Ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum En Ullam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engum Nee Nindrirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Nee Nindrirukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ushnam Thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ushnam Thakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelada Kaadhala Thanimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelada Kaadhala Thanimai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Than Dragula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than Dragula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mississippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mississippi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaigalai Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigalai Thaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pacificil Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacificil Vanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilinthathu Paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilinthathu Paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magilchiyil Idhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magilchiyil Idhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirippinai Maarthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirippinai Maarthum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirippinil Puthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirippinil Puthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Symphony Ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Symphony Ketkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Oru Sun Flower
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oru Sun Flower"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavithaiyil Unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithaiyil Unthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhaginai Paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhaginai Paada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Oru Shakespeare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Oru Shakespeare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Anbe Kaadhal Kaadhal Than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Anbe Kaadhal Kaadhal Than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithoolagam Elunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithoolagam Elunthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhuthaalum Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuthaalum Love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seiyvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum Meendum Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Meendum Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Saxophona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Saxophona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Kaiyil Enthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Kaiyil Enthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bill Clinton Pola Vaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bill Clinton Pola Vaasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ival Kanni Yalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Kanni Yalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kanini Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kanini Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bill Gates-ai Pola Nesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bill Gates-ai Pola Nesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollada Manmatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollada Manmatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Billu Nee Enbatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Billu Nee Enbatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fridge Inil Ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fridge Inil Ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Freezerai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Freezerai Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulir Thara Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulir Thara Oru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunnaiyundu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunnaiyundu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalil Oru Pack
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalil Oru Pack"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pannu Maane Viraivinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannu Maane Viraivinil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthu Uthaviduvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Uthaviduvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sammatham Winter Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sammatham Winter Raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivaladhu Viral Padugira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivaladhu Viral Padugira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothu Quarter Um Liquor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothu Quarter Um Liquor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Uyire Intha Nootraandil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyire Intha Nootraandil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orr Kavigan Evanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr Kavigan Evanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhudhadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhudhadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Poem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Poem"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivvulagil Engu Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvulagil Engu Ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orr Ilaignan Idhayam Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr Ilaignan Idhayam Kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenthum Love Logo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthum Love Logo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Website Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Website Ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanden Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanden Kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum Kangal Rendil Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Kangal Rendil Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hardware Ullam Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hardware Ullam Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponathenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Something Enakkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Something Enakkulle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nernthathenna Unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nernthathenna Unthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal Ennai Kadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Ennai Kadathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poga Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Poga"/>
</div>
</pre>
