---
title: "dham dham dham song lyrics"
album: "Konda Polam"
artist: "M. M. Keeravani"
lyricist: "Chandrabose"
director: "Krish Jagarlamudi"
path: "/albums/konda-polam-lyrics"
song: "Dham Dham Dham"
image: ../../images/albumart/konda-polam.jpg
date: 2021-10-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/cVsg_AFrf3A"
type: "happy"
singers:
  - Rahul Sipligunj
  - Damini Bhatla
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pacha Pacha Settusema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Pacha Settusema"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu Seerelantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu Seerelantaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Nalla Mullakampa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Nalla Mullakampa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Pusalantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Pusalantaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kicha Kichalade Udutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kicha Kichalade Udutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichika Laalipaatanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichika Laalipaatanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Galagala Pare Selalo Neellu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galagala Pare Selalo Neellu"/>
</div>
<div class="lyrico-lyrics-wrapper">Salubaalantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salubaalantaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adavi Thalli Intikochina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adavi Thalli Intikochina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaggari Suttaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaggari Suttaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanalachhimi Vodilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanalachhimi Vodilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattalanni Gattekichedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattalanni Gattekichedham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adavi Thalli Intikochina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adavi Thalli Intikochina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaggari Suttaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaggari Suttaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanalachhimi Vodilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanalachhimi Vodilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattalanni Gattekichedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattalanni Gattekichedham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dham Dham Dham Thirigedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Dham Thirigedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham Dham Dham Dhorledham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Dham Dhorledham"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham Dham Dham Thirigedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Dham Thirigedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham Dham Dham Dhorledham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Dham Dhorledham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dham Dham Dham Thirigedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Dham Thirigedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham Dham Dham Dhorledham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Dham Dhorledham"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham Dham Dham Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Dham Dham"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaya Suupalani Adavini Adagedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaya Suupalani Adavini Adagedham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mana Panalanni Nilipe Thalliki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Panalanni Nilipe Thalliki"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagilapadipodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagilapadipodham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogamanchemo Sambranesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogamanchemo Sambranesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Premaga Thalanii Nimirenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premaga Thalanii Nimirenanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethiki Thagile Pedu Beraduu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethiki Thagile Pedu Beraduu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayathalle Thadimenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayathalle Thadimenanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maddhiteku Aakulu Manaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maddhiteku Aakulu Manaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Isana Karralu Isirenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isana Karralu Isirenanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoyy Hoyy Hoyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyy Hoyy Hoyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaddi Garika Pachika Manaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaddi Garika Pachika Manaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Parupe Parisi Pilisenantaa Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parupe Parisi Pilisenantaa Ho Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dham Dham Dham Susedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Dham Susedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham Dham Dham Suttedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Dham Suttedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham Dham Dham Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Dham Dham"/>
</div>
<div class="lyrico-lyrics-wrapper">Adave Manaku Kovela Anukundhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adave Manaku Kovela Anukundhaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooraka Mundhe Varalaniche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooraka Mundhe Varalaniche"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallini Kolisedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallini Kolisedham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sukka Sukka Dachalantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukka Sukka Dachalantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thene Teege Thelipenantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thene Teege Thelipenantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Surukuntene Bathukundhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surukuntene Bathukundhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhuppikadithii Seppenantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuppikadithii Seppenantaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peddhapulitho Thalapadu Dhairnam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peddhapulitho Thalapadu Dhairnam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adavipandhe Nerpenantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adavipandhe Nerpenantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalise Unte Balamundhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalise Unte Balamundhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Resu Kukkalu Saatenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Resu Kukkalu Saatenanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottakuutiki Etadeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottakuutiki Etadeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevulu Seppe Paatam Okate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevulu Seppe Paatam Okate"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinna Intini Dvamsam Sese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinna Intini Dvamsam Sese"/>
</div>
<div class="lyrico-lyrics-wrapper">Papaniki Vodi Kattodhanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papaniki Vodi Kattodhanthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dham Dham Dham Sadhivedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Dham Sadhivedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham Dham Dham Nersedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Dham Nersedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Dham Dham Dham Dham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dham Dham Dham Dham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Pataalanu Bhathukuna Paatidham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Pataalanu Bhathukuna Paatidham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adavini Minchina Badi Ledhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adavini Minchina Badi Ledhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulu Kadhipedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulu Kadhipedham"/>
</div>
</pre>
