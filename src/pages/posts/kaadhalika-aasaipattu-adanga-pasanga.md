---
title: "kaadhalika aasaipattu song lyrics"
album: "Adanga Pasanga"
artist: "Maneesh"
lyricist: "N Jaigar Devadass "
director: "R. Selvanathan"
path: "/albums/adanga-pasanga-lyrics"
song: "Kaadhalika Aasaipattu"
image: ../../images/albumart/adanga-pasanga.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/An3Ie2P2Ync"
type: "sad"
singers:
  - Sujith
  - Anu
  - Ashwini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kathalikka aasaipattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalikka aasaipattu"/>
</div>
<div class="lyrico-lyrics-wrapper">thevi unnai ninaithu vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevi unnai ninaithu vitten"/>
</div>
<div class="lyrico-lyrics-wrapper">unnethire naan irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnethire naan irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">kandu kolla villaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandu kolla villaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathalikka aasaipattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalikka aasaipattu"/>
</div>
<div class="lyrico-lyrics-wrapper">thevi unnai ninaithu vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevi unnai ninaithu vitten"/>
</div>
<div class="lyrico-lyrics-wrapper">unnethire naan irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnethire naan irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">kandu kolla villaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandu kolla villaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un manathil naan irunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un manathil naan irunthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam enthan kayil adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam enthan kayil adi"/>
</div>
<div class="lyrico-lyrics-wrapper">un manathil naan irunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un manathil naan irunthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam enthan kayil adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam enthan kayil adi"/>
</div>
<div class="lyrico-lyrics-wrapper">un manathil naan irunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un manathil naan irunthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam enthan kayil adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam enthan kayil adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathalikka aasaipattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalikka aasaipattu"/>
</div>
<div class="lyrico-lyrics-wrapper">thevi unnai ninaithu vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevi unnai ninaithu vitten"/>
</div>
<div class="lyrico-lyrics-wrapper">unnethire naan irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnethire naan irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">kandu kolla villaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandu kolla villaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee nadakkum paathai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nadakkum paathai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaal kadukka nadanthu vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaal kadukka nadanthu vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">un manathil nan irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un manathil nan irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">en manamum virumbuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manamum virumbuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">iravum pagalum un ninappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravum pagalum un ninappu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethana naala aengi thavikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethana naala aengi thavikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">yendi amma eduthu solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yendi amma eduthu solli"/>
</div>
<div class="lyrico-lyrics-wrapper">purunjukaamale irukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="purunjukaamale irukura"/>
</div>
<div class="lyrico-lyrics-wrapper">unna kaathalikka aasaipattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kaathalikka aasaipattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathalikka aasaipattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalikka aasaipattu"/>
</div>
<div class="lyrico-lyrics-wrapper">thevi unnai ninaithu vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevi unnai ninaithu vitten"/>
</div>
<div class="lyrico-lyrics-wrapper">unnethire naan irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnethire naan irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">kandu kolla villaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandu kolla villaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paarijatham malarai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarijatham malarai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">paathu unna rasikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathu unna rasikuren"/>
</div>
<div class="lyrico-lyrics-wrapper">paamarana naan irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paamarana naan irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">parikka nenaikuren mudiyalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parikka nenaikuren mudiyalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu ketta devathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu ketta devathaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">enna aereduthu paakalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna aereduthu paakalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">epothu ennai paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothu ennai paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee irangi varuvayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee irangi varuvayo"/>
</div>
<div class="lyrico-lyrics-wrapper">unna kathalikka aasaipattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kathalikka aasaipattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathalikka aasaipattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalikka aasaipattu"/>
</div>
<div class="lyrico-lyrics-wrapper">thevi unnai ninaithu vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevi unnai ninaithu vitten"/>
</div>
<div class="lyrico-lyrics-wrapper">unnethire naan irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnethire naan irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">kandu kolla villaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandu kolla villaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathalikka aasaipattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalikka aasaipattu"/>
</div>
<div class="lyrico-lyrics-wrapper">thevi unnai ninaithu vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevi unnai ninaithu vitten"/>
</div>
<div class="lyrico-lyrics-wrapper">unnethire naan irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnethire naan irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">kandu kolla villaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandu kolla villaiyadi"/>
</div>
</pre>
