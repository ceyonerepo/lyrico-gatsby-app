---
title: "3.59am jwala jwala jwala song lyrics"
album: "Punya Paap"
artist: "Stunnah Beatz"
lyricist: "DIVINE"
director: "Gil Green"
path: "/albums/punya-paap-lyrics"
song: "3.59am Jwala Jwala Jwala"
image: ../../images/albumart/punya-paap.jpg
date: 2020-12-03
lang: hindi
youtubeLink: "https://www.youtube.com/embed/eHyl9KG9RWA"
type: "happy"
singers:
  - DIVINE
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haan!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan!"/>
</div>
<div class="lyrico-lyrics-wrapper">Punya paap punya paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punya paap punya paap"/>
</div>
<div class="lyrico-lyrics-wrapper">We back!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We back!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jwala jwala jwala, chhote laaya main game mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jwala jwala jwala, chhote laaya main game mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Dollar dollar dollar, chhote laaya main game mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dollar dollar dollar, chhote laaya main game mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Bada naam naam banaya main game mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bada naam naam banaya main game mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Liya nahi chain tab to heera hai chain mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Liya nahi chain tab to heera hai chain mein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Likhta tha gully mein ab bhi likhta hun plane mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Likhta tha gully mein ab bhi likhta hun plane mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Asal zindagi hai meri chhote no entertainment
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asal zindagi hai meri chhote no entertainment"/>
</div>
<div class="lyrico-lyrics-wrapper">Itihas likh diya tujhe classics diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itihas likh diya tujhe classics diya"/>
</div>
5<div class="lyrico-lyrics-wrapper">x plat (platinum) gully wale ne diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="x plat (platinum) gully wale ne diya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pichhe se aake phir saamne se diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichhe se aake phir saamne se diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sachin ka bat bambai ka maine naam rakh diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sachin ka bat bambai ka maine naam rakh diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jab studio mein lock tab sab khol diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab studio mein lock tab sab khol diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar 2 maine liye to 10 maine diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar 2 maine liye to 10 maine diya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usne 10 mujhse lekar dhass mujhe diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usne 10 mujhse lekar dhass mujhe diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Roll karke pehle sabko kash maine diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roll karke pehle sabko kash maine diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadalon ke upar maine game rakh diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadalon ke upar maine game rakh diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Quality gaanon se make it rain kar diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Quality gaanon se make it rain kar diya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bas bas ho gaya abhi sirf pariwar mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas bas ho gaya abhi sirf pariwar mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Pehle shani tha ab har din shanivar mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehle shani tha ab har din shanivar mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dentist se zyada game mujhe daant de raha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dentist se zyada game mujhe daant de raha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaccine yeh bars aur main sabko shot de raha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaccine yeh bars aur main sabko shot de raha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mujhe game saath de raha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe game saath de raha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise liya maine saat phera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise liya maine saat phera"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirf main janta hun punya paap mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirf main janta hun punya paap mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Deemag ko rakhte dusra aur khwab pehla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deemag ko rakhte dusra aur khwab pehla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalam se bar fest pentecost mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam se bar fest pentecost mera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agar maloom tu jeeta chal hath upar kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar maloom tu jeeta chal hath upar kar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil se salaam mere sath thhe agar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil se salaam mere sath thhe agar"/>
</div>
<div class="lyrico-lyrics-wrapper">Gehra paani bol ke mere sath thhe magar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gehra paani bol ke mere sath thhe magar"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagwan mujhe bola divine paani pe chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagwan mujhe bola divine paani pe chal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeezus wale mode mein nass wale mode mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeezus wale mode mein nass wale mode mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Khoon se behtar koyi nahi shaks wale mode mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khoon se behtar koyi nahi shaks wale mode mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandagi hai tu aur main flush wale mode mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandagi hai tu aur main flush wale mode mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Seekha sab road pe aur mein 100 100 tha board mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seekha sab road pe aur mein 100 100 tha board mein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sab kuch hai zor mein divine apne zone mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab kuch hai zor mein divine apne zone mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Gully gully gang aur ghar mera bombay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gully gully gang aur ghar mera bombay"/>
</div>
<div class="lyrico-lyrics-wrapper">Bacche bacche karte rap har kone mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bacche bacche karte rap har kone mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapne hote sach tab maza aata sone mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapne hote sach tab maza aata sone mein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Classic har baar, har shabd mere sholay mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Classic har baar, har shabd mere sholay mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhati inki nahi hai isliye baar baar yeh bolenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhati inki nahi hai isliye baar baar yeh bolenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum kholenge jaise kho kho ka game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum kholenge jaise kho kho ka game"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu bahar dikhta acha sirf promo ka game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu bahar dikhta acha sirf promo ka game"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rakta char mere sath jaise 2 2 ka game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakta char mere sath jaise 2 2 ka game"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu aur tere homie chhote tum dono ka game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu aur tere homie chhote tum dono ka game"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhutta paisa rakh ke dekh laaya na change
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhutta paisa rakh ke dekh laaya na change"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakh tera fame, main banda nahi same
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakh tera fame, main banda nahi same"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaptani innings yeh hai kohli wala shot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaptani innings yeh hai kohli wala shot"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghar ke bahar tere pahunche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghar ke bahar tere pahunche"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangoli wala shot, konkani karnataka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangoli wala shot, konkani karnataka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kohli wala shot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kohli wala shot"/>
</div>
<div class="lyrico-lyrics-wrapper">Khade hokar jala diye modi wala shot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khade hokar jala diye modi wala shot"/>
</div>
16 <div class="lyrico-lyrics-wrapper">se hai khauf aur 16 se hun hot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="se hai khauf aur 16 se hun hot"/>
</div>
<div class="lyrico-lyrics-wrapper">Khatarnak coats jaise tailor mera baap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khatarnak coats jaise tailor mera baap"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edit karke tune image mera meme bana diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edit karke tune image mera meme bana diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehnat karke tere bhai ne poora dream bana diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehnat karke tere bhai ne poora dream bana diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Picture ya sadkon pe scene bana diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picture ya sadkon pe scene bana diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Khudka time laaya maine sabse bada gaana diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khudka time laaya maine sabse bada gaana diya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shikshak se pehle thugs dekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shikshak se pehle thugs dekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tiffin box se pehle maine drugs dekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tiffin box se pehle maine drugs dekha"/>
</div>
29 <div class="lyrico-lyrics-wrapper">saal ka main par lagta maine sab dekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saal ka main par lagta maine sab dekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rishton mein pyar se zyada shaq dekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rishton mein pyar se zyada shaq dekha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shohrat ke liye zyada mehnat thoda luck dekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shohrat ke liye zyada mehnat thoda luck dekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Circle rakta tight aur main circle jitna fu*k deta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Circle rakta tight aur main circle jitna fu*k deta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasha apan karte isliye nashe mein na rakh deta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasha apan karte isliye nashe mein na rakh deta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na na zindagi tamasha hai views toh 69 ko bhi aata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na na zindagi tamasha hai views toh 69 ko bhi aata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Apna missionary shot sab kuchh saamne se jaata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apna missionary shot sab kuchh saamne se jaata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bank mera ghar ka khud ka mera khata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bank mera ghar ka khud ka mera khata hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">License pe hai album kyun ki maths mujhe aata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="License pe hai album kyun ki maths mujhe aata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dollar jab involved tab cents samajh aata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dollar jab involved tab cents samajh aata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jab bills aur passion dono sath mein nahi jaata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab bills aur passion dono sath mein nahi jaata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo passion na de paise woh passion nahi sikhata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo passion na de paise woh passion nahi sikhata hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Independent mein nahi sirf entertainment mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Independent mein nahi sirf entertainment mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Independent mein hun har street ke pavement pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Independent mein hun har street ke pavement pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Inke baap ke payment se aaye lame men ke sath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inke baap ke payment se aaye lame men ke sath"/>
</div>
<div class="lyrico-lyrics-wrapper">Nazrein to milana kabhi real men ke sath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazrein to milana kabhi real men ke sath"/>
</div>
<div class="lyrico-lyrics-wrapper">Main karte rahunga grind jab tak 6 men de hath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main karte rahunga grind jab tak 6 men de hath"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sab kuchh apan risk karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab kuchh apan risk karte"/>
</div>
<div class="lyrico-lyrics-wrapper">Toota taara dikhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toota taara dikhe"/>
</div>
<div class="lyrico-lyrics-wrapper">To abhi bhi hum wish karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To abhi bhi hum wish karte"/>
</div>
<div class="lyrico-lyrics-wrapper">Gyan liya naani se haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gyan liya naani se haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Abhi bhi hum miss karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abhi bhi hum miss karte"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasman ko dekh kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasman ko dekh kar"/>
</div>
<div class="lyrico-lyrics-wrapper">Abhi hum kiss karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abhi hum kiss karte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalam mera hindu muslim ko milata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam mera hindu muslim ko milata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hindi mein verse mera punjab ko hilata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hindi mein verse mera punjab ko hilata hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pair se leke sar tak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pair se leke sar tak"/>
</div>
<div class="lyrico-lyrics-wrapper">Sar se leke pair tak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sar se leke pair tak"/>
</div>
<div class="lyrico-lyrics-wrapper">Sab kuch proper hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab kuch proper hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan haan album mode mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan haan album mode mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Stunnah mere sath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stunnah mere sath"/>
</div>
<div class="lyrico-lyrics-wrapper">Mumbai shehar 2020
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mumbai shehar 2020"/>
</div>
<div class="lyrico-lyrics-wrapper">Naani yaad aa jayegi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naani yaad aa jayegi"/>
</div>
</pre>
