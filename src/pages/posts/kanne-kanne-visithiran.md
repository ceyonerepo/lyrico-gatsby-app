---
title: "kanne kanne song lyrics"
album: "Visithiran"
artist: "GV Prakash"
lyricist: "Yugabharathi"
director: "M Padmakumar"
path: "/albums/visithiran-lyrics"
song: "Kanne Kanne"
image: ../../images/albumart/visithiran.jpg
date: 2022-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KdnxHDC9dJo"
type: "happy"
singers:
  - Roshan Sebastian
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannae kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kandadhor varammae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kandadhor varammae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil veithaein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil veithaein"/>
</div>
<div class="lyrico-lyrics-wrapper">Konja ennuven naall ellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja ennuven naall ellaamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudarae sudarin thodarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudarae sudarin thodarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamae sondham kai neettutha amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamae sondham kai neettutha amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalae pagalin ezhizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalae pagalin ezhizhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham ullae pookadhu amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham ullae pookadhu amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amudha kadalil naanae vila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amudha kadalil naanae vila"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam kulungudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam kulungudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyum veyilum serndhae vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyum veyilum serndhae vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirum thazhumbudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirum thazhumbudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudarae sudarin thodarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudarae sudarin thodarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamae sondham kai neettutha amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamae sondham kai neettutha amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalae pagalin ezhizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalae pagalin ezhizhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham ullae pookadhu amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham ullae pookadhu amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paal nilavae neram paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal nilavae neram paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Keezh vaarumo saadham ootta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keezh vaarumo saadham ootta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezh isaiyum kaigal korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezh isaiyum kaigal korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadidumo thoozhi aatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadidumo thoozhi aatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavul kodutha puthiyallae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul kodutha puthiyallae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhaindai vadivil kidaithadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhaindai vadivil kidaithadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhil virindha magzhizham poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhil virindha magzhizham poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhiril sirithadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiril sirithadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudarae sudarin thodarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudarae sudarin thodarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamae sondham kai neettutha amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamae sondham kai neettutha amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalae pagalin ezhizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalae pagalin ezhizhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham ullae pookadhu amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham ullae pookadhu amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaen mazhaipol pesum pechai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen mazhaipol pesum pechai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal muzhudhum kettaal podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal muzhudhum kettaal podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo vizhigal kaatum katchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo vizhigal kaatum katchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neer madhagai modhum kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer madhagai modhum kolam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aruvi vazhiyum azhagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruvi vazhiyum azhagumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu siripil sidharudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu siripil sidharudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinungum unnadhu kurallilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinungum unnadhu kurallilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam vidiyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam vidiyudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannae kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannae kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kandadhor varammae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kandadhor varammae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil veithaein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil veithaein"/>
</div>
<div class="lyrico-lyrics-wrapper">Konja ennuven naalellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja ennuven naalellaamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudarae sudarin thodarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudarae sudarin thodarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamae sondham kai neettutha amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamae sondham kai neettutha amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalae pagalin ezhizhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalae pagalin ezhizhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham ullae pookadhu amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham ullae pookadhu amma"/>
</div>
</pre>
