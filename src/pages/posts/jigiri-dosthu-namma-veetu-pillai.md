---
title: "jigiri dosthu lyrics"
album: "namma veetu pillai"
artist: "D Imman"
lyricist: "Arunraja kamaraj"
director: "pandiraj"
path: "/albums/namma-veetu-pillai-song-lyrics"
song: "jigiri dosthu"
image: ../../images/albumart/namma-veetu-pillai.jpg
date: 2019-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5QBkGgBtX34"
type: "friendship"
singers:
  - Puduvaisithan Jayamoorthy
  - Anthakudi Ilayaraja
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gopuraththil Thookki Vaikkum Sontham Idhuthaana,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gopuraththil Thookki Vaikkum Sontham Idhuthaana,"/>
</div>
<div class="lyrico-lyrics-wrapper">Theepporikke Thozh Kodukkum Bandham Idhuthana,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theepporikke Thozh Kodukkum Bandham Idhuthana,"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Manna Onna Ninna Prachana Parakkum Thana,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Manna Onna Ninna Prachana Parakkum Thana,"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Vandha Sami Thana,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vandha Sami Thana,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jigiri Dosthu, Jigiri Dosthu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigiri Dosthu, Jigiri Dosthu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanava Seththu, Sumakkum Dosthu (X2).
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanava Seththu, Sumakkum Dosthu (X2)."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gopuraththil Thookki Vaikkum Sontham Idhuthana,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gopuraththil Thookki Vaikkum Sontham Idhuthana,"/>
</div>
<div class="lyrico-lyrics-wrapper">Theepporikke Thozh Kodukkum Bandham Idhuthana,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theepporikke Thozh Kodukkum Bandham Idhuthana,"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Manna Onna Ninna Prachana Parakkum Thana,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Manna Onna Ninna Prachana Parakkum Thana,"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Vandha Sami Thana,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vandha Sami Thana,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jigiri Dosthu, Jigiri Dosthu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigiri Dosthu, Jigiri Dosthu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanava Seththu, Sumakkum Dosthu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanava Seththu, Sumakkum Dosthu,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh Thattan Tharaiyila Pottan Nizhalula,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh Thattan Tharaiyila Pottan Nizhalula,"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti Thiriyura Kalai,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutti Thiriyura Kalai,"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetta Kanigalai Yetti Parichida,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetta Kanigalai Yetti Parichida,"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttu Kodukkanum Thozhan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttu Kodukkanum Thozhan,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhum Kalamellam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhum Kalamellam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Thane Ellam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Thane Ellam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vanthalum, Yeththukalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vanthalum, Yeththukalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jigiri Dosthu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigiri Dosthu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigiri Dosthu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigiri Dosthu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanava Seththu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanava Seththu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumakkum Dosthu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumakkum Dosthu,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama Va Mapla,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Va Mapla,"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Varthaikum Arthamum Illa,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Varthaikum Arthamum Illa,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ana Namakkulla,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ana Namakkulla,"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Anbukku Ellaiye Illa,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Anbukku Ellaiye Illa,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanthaththa Poloru Manasoda,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaththa Poloru Manasoda,"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Singaththa Pola Nadappom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Singaththa Pola Nadappom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppom Thanakkena Vazhama,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppom Thanakkena Vazhama,"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Natpukku Ilakkanam Padaippom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Natpukku Ilakkanam Padaippom,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammoda Pasam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda Pasam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnalum Pasam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalum Pasam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Marandhida Palagalaiye,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhida Palagalaiye,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vattikadai Pola,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattikadai Pola,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty Pottu Kooda,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty Pottu Kooda,"/>
</div>
<div class="lyrico-lyrics-wrapper">Valanthida Thadai Illaiye,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valanthida Thadai Illaiye,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Sangatham Eppodhume,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Sangatham Eppodhume,"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Nenjil Kudipattathan Thangume,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Nenjil Kudipattathan Thangume,"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnala Polathan Ennalume,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnala Polathan Ennalume,"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusaga Porakkattum Nam Sondhame,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaga Porakkattum Nam Sondhame,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peththava Pechaiyum Maththava Pechaiyum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peththava Pechaiyum Maththava Pechaiyum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandaiyil Yeththunathu Illa,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaiyil Yeththunathu Illa,"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpukku Maththiyil Nikkura Yaraiyum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpukku Maththiyil Nikkura Yaraiyum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththama Seendunathu Illa Illa Illa Illa,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththama Seendunathu Illa Illa Illa Illa,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jigiri Dosthu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigiri Dosthu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigiri Dosthu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigiri Dosthu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa Paththu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Paththu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Palagum Dosthu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palagum Dosthu,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gopuraththil Thookki Vaikkum Sontham Idhuthana,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gopuraththil Thookki Vaikkum Sontham Idhuthana,"/>
</div>
<div class="lyrico-lyrics-wrapper">Theepporikke Thozh Kodukkum Bandham Idhuthana,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theepporikke Thozh Kodukkum Bandham Idhuthana,"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Manna Onna Ninna Prachana Parakkum Thana,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Manna Onna Ninna Prachana Parakkum Thana,"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Vandha Sami Thana,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vandha Sami Thana,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh Thattan Tharaiyila Pottan Nizhalula,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh Thattan Tharaiyila Pottan Nizhalula,"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti Thiriyura Kalai,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutti Thiriyura Kalai,"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetta Kanigalai Yetti Parichida,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetta Kanigalai Yetti Parichida,"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttu Kodukkanum Thozhan,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttu Kodukkanum Thozhan,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhum Kalamellam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhum Kalamellam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Thane Ellam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Thane Ellam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vanthalum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vanthalum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeththukalam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeththukalam,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jigiri Dosthu, Jigiri Dosthu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigiri Dosthu, Jigiri Dosthu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanava Seththu, Sumakkum Dosthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanava Seththu, Sumakkum Dosthu "/>
</div>
</pre>
