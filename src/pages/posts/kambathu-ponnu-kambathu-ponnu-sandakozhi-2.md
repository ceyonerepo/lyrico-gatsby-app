---
title: "kambathu ponnu kambathu ponnu song lyrics"
album: "Sandakozhi 2"
artist: "Yuvan Sankar Raja"
lyricist: "K. Ekadesi"
director: "N. Linguswamy"
path: "/albums/sandakozhi-2-lyrics"
song: "Kambathu Ponnu Kambathu Ponnu"
image: ../../images/albumart/sandakozhi-2.jpg
date: 2018-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KPDhwjmb11o"
type: "Love"
singers:
  - Yuvan shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kambathu Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambathu Ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kambathu Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambathu Ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannala Vetti Thookura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala Vetti Thookura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engooru Kaathu Soorali Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engooru Kaathu Soorali Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puzhuthi Parakka Thakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuthi Parakka Thakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aala Marathu Ilai Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Marathu Ilai Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Kanna Kuliyila Vila Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Kanna Kuliyila Vila Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pambattam Rettai Sadai Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pambattam Rettai Sadai Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Paakuthu Ennai Thoda Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Paakuthu Ennai Thoda Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Da Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Da Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manja Sevappu Kannadi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja Sevappu Kannadi Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Nee Saaikatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nee Saaikatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Katti Kedakkura Aatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Katti Kedakkura Aatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Avathuttu Meikaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Avathuttu Meikaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podi Poo Thaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi Poo Thaangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raathiri Puraa Thoongala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiri Puraa Thoongala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kambathu Ponnu Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambathu Ponnu Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kambathu Ponnu Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kambathu Ponnu Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamabathu Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamabathu Ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamabathu Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamabathu Ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannala Vetti Thookkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala Vetti Thookkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engoru Kaathu Soorali Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engoru Kaathu Soorali Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puzhuthi Parakka Thaakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuthi Parakka Thaakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhavani Kaathu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhavani Kaathu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhura Moochadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhura Moochadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesuna Pechalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesuna Pechalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakkarai Aachadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkarai Aachadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammiyaa Araikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammiyaa Araikira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aala Nee Asathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Nee Asathura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnala Kannula Vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnala Kannula Vaangi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnasaaratha Paachura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnasaaratha Paachura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Savvu Mittaiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savvu Mittaiyu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Watcha Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watcha Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Thaan Katti Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thaan Katti Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Kuchi Ice U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Kuchi Ice U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaiye Pola Sattiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiye Pola Sattiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otti Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadu Coffee Itham Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadu Coffee Itham Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasa Nee Thaan Aakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Nee Thaan Aakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kambathu Ponnu Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambathu Ponnu Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kambathu Ponnu Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kambathu Ponnu Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raattinam Pola Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raattinam Pola Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookki Nee Suththura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookki Nee Suththura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melluren Muzhunguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melluren Muzhunguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarthaiye Sikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaiye Sikkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyula Pesura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyula Pesura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannula Kekkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula Kekkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathula Kammala Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathula Kammala Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasa Neeyum Aattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Neeyum Aattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panju Mittaiyi Renda Thirudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panju Mittaiyi Renda Thirudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannatha Senju Kitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannatha Senju Kitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Eesal Rekkaiya Pichi Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Eesal Rekkaiya Pichi Vanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayatha Nenju Kitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayatha Nenju Kitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathadi Kaathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathadi Kaathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Pera Thaan Koovuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pera Thaan Koovuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kambathu Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambathu Ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kambathu Ponnu Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kambathu Ponnu Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamabaththu Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamabaththu Ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamabaththu Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamabaththu Ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannala Vetti Thookkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala Vetti Thookkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engoru Kaathu Soorali Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engoru Kaathu Soorali Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puzhuthi Parakka Thaakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhuthi Parakka Thaakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kamabathu Ponnu Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamabathu Ponnu Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kamabathu Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kamabathu Ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kamabathu Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kamabathu Ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kamabathu Ponnu Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kamabathu Ponnu Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kamabathu Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kamabathu Ponnu"/>
</div>
</pre>
