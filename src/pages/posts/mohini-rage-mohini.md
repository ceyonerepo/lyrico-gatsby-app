---
title: "mohini rage song lyrics"
album: "Mohini"
artist: "	Vivek-Mervin"
lyricist: "Pa Vijay"
director: "Ramana Madhesh"
path: "/albums/mohini-lyrics"
song: "Mohini rage"
image: ../../images/albumart/mohini.jpg
date: 2018-07-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/as_dm4fczx0"
type: "mass"
singers:
  - Nithyashree Mahadevan
  - Prashanthi
  - Vignesh Natarajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Om chamundae namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om chamundae namaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Om chandigaya namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om chandigaya namaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Om chamundae namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om chamundae namaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Om chandigaya namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Om chandigaya namaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhira dhira dheena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhira dhira dheena"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemaiku thee vaipaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemaiku thee vaipaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannodu gira gara gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannodu gira gara gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Girangangal kooduthu maaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girangangal kooduthu maaruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeruthu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeruthu thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhira dhira dheena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhira dhira dheena"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemaiku thee vaipaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemaiku thee vaipaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannodu gira gara gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannodu gira gara gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Girangangal kooduthu maaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girangangal kooduthu maaruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeruthu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeruthu thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaya jaya devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya jaya devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Devi namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi namaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Devi namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi namaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaya jaya devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya jaya devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Devi namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi namaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Devi namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi namaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhira dhira dheena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhira dhira dheena"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemaiku thee vaipaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemaiku thee vaipaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannodu gira gara gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannodu gira gara gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Girangangal kooduthu maaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girangangal kooduthu maaruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeruthu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeruthu thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhoora dhoora varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoora dhoora varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Soora soora mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soora soora mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanjam thannil thanjam kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjam thannil thanjam kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veera veera vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera veera vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghora ghoram yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghora ghoram yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarum maarum kanamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarum maarum kanamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaala jaala thava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaala jaala thava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kola kola mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kola kola mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemai kandu kobam kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemai kandu kobam kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala kaala alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala kaala alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada aada silai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada aada silai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aara vaara nilaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aara vaara nilaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar seidha thappukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar seidha thappukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerpondru illamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerpondru illamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogadhu pogadhu vaazh naalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogadhu pogadhu vaazh naalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhezhu jenmangal aanalum maaradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhezhu jenmangal aanalum maaradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam seidha paavangal nam thozhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam seidha paavangal nam thozhilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaya jaya devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya jaya devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Devi namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi namaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Devi namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi namaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaya jaya devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya jaya devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Devi namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi namaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Devi namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi namaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaya jaya devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya jaya devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Devi namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi namaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Devi namaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi namaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkul acchatthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkul acchatthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Acchatthin ucchatthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acchatthin ucchatthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee konda karmathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konda karmathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerpaalae dharmatthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerpaalae dharmatthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavamthin motthathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavamthin motthathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Parigaara ratthathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parigaara ratthathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkindra yutthathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkindra yutthathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seigindra or thath-thai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seigindra or thath-thai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ainooru jaamangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainooru jaamangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adai kaatha kobangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adai kaatha kobangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvei uru-maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvei uru-maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thedi alayudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir thedi alayudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee yendha kottaikul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yendha kottaikul"/>
</div>
<div class="lyrico-lyrics-wrapper">Ododi sendralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ododi sendralum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalodu nizhalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalodu nizhalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai dhaan thurathudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai dhaan thurathudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeraga vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeraga vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaga vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaga vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappikka vazhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappikka vazhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettaiku vizhiyillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaiku vizhiyillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ai giri nandini nandhita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ai giri nandini nandhita"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhini viswa vinodhini nandanuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhini viswa vinodhini nandanuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Girivara vindhya sirodhi nivasini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girivara vindhya sirodhi nivasini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishnu vilasini jishnu nuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishnu vilasini jishnu nuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhagawathi hey sithi kanda kudumbini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhagawathi hey sithi kanda kudumbini"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoori kudumbini bhoori kruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoori kudumbini bhoori kruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya jaya he mahishasura mardini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya jaya he mahishasura mardini"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramya kapardini shaila suthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramya kapardini shaila suthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhira dhira dheena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhira dhira dheena"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemaiku thee vaipaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemaiku thee vaipaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannodu gira gara gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannodu gira gara gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Girangangal kooduthu maaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girangangal kooduthu maaruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeruthu thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeruthu thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhoora dhoora varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoora dhoora varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Soora soora mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soora soora mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanjam thannil thanjam kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjam thannil thanjam kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veera veera vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera veera vizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghora ghoram yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghora ghoram yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarum maarum kanamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarum maarum kanamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaala jaala thava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaala jaala thava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kola kola mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kola kola mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemai kandu kobam kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemai kandu kobam kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala kaala alai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala kaala alai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada aada silai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada aada silai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aara vaara nilaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aara vaara nilaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar seidha thappukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar seidha thappukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerpondru illamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerpondru illamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogadhu pogadhu vaazh naalilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogadhu pogadhu vaazh naalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhezhu jenmangal aanalum maaradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhezhu jenmangal aanalum maaradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam seidha paavangal nam thozhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam seidha paavangal nam thozhilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahaa aaahaaa aahaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaa aaahaaa aahaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaya jaya devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya jaya devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya jaya devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya jaya devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya jaya devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya jaya devi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hara hara devi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hara hara devi"/>
</div>
</pre>
