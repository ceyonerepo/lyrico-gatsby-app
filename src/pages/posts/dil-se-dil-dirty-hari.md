---
title: "dil se dil song lyrics"
album: "Dirty Hari"
artist: "Mark K. Robin"
lyricist: "Abbas"
director: "M. S. Raju"
path: "/albums/dirty-hari-lyrics"
song: "Dil Se Dil"
image: ../../images/albumart/dirty-hari.jpg
date: 2020-12-18
lang: telugu
youtubeLink: "https://www.youtube.com/embed/RtieCqp8DjQ"
type: "love"
singers:
  - Sarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naine Ladaake Dil Mein Samaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naine Ladaake Dil Mein Samaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Naine Ladaake Dil Mein Samaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naine Ladaake Dil Mein Samaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyar Kiya Hein Pyar Nibhaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyar Kiya Hein Pyar Nibhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere Dil Se Dil Tu Lagaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere Dil Se Dil Tu Lagaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu Pyaar Se Dil Basaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu Pyaar Se Dil Basaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyar Ki Deep Jalaa Dhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyar Ki Deep Jalaa Dhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere Dil Se Dil Tu Lagaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere Dil Se Dil Tu Lagaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu Pyaar Se Dil Basaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu Pyaar Se Dil Basaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyar Ki Deep Jalaa Dhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyar Ki Deep Jalaa Dhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Aa Aaa Aa Mere Dil Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Aaa Aa Mere Dil Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil Tu Lagaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil Tu Lagaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu Pyaar Se Dil Basaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu Pyaar Se Dil Basaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyar Ki Deep Jalaa Dhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyar Ki Deep Jalaa Dhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere Dil Se Dil Tu Lagaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere Dil Se Dil Tu Lagaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu Pyaar Se Dil Basaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu Pyaar Se Dil Basaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyar Ki Deep Jalaa Dhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyar Ki Deep Jalaa Dhe"/>
</div>
</pre>
