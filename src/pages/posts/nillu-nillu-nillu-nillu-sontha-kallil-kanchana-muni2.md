---
title: "nillu nillu nillu nillu sontha kallil song lyrics"
album: "Kanchana-Muni2"
artist: "S. Thaman"
lyricist: "Raghava Lawrence"
director: "Raghava Lawrence"
path: "/albums/kanchana-muni2-lyrics"
song: "Nillu Nillu Nillu Nillu Sontha Kallil"
image: ../../images/albumart/kanchana-muni2.jpg
date: 2011-07-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/s9m3435p9OQ"
type: "Motivational"
singers:
  - Tippu
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaaa….aaa….aaa…aa….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa….aaa….aaa…aa…."/>
</div>
<div class="lyrico-lyrics-wrapper">Ooiyaadhae ooiyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooiyaadhae ooiyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooiyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooiyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey nillu nillu nillu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nillu nillu nillu nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondha kaalil neeyum nillu…. nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondha kaalil neeyum nillu…. nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye thallu thallu thallu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thallu thallu thallu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaividhiya kaiyaal thallu…thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaividhiya kaiyaal thallu…thallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooiyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooiyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanaper vandhaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanaper vandhaangadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanaper poanaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanaper poanaangadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhichavar mattum inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhichavar mattum inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Selaiyaa ninnaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selaiyaa ninnaangadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konduvandhadhu edhuvumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konduvandhadhu edhuvumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondupovadhu edhuvumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondupovadhu edhuvumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kai paarthidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kai paarthidalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkaiyai jeyichidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkaiyai jeyichidalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolludaa kolludaa kolludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolludaa kolludaa kolludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un bayatha neeyum kolludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un bayatha neeyum kolludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velludaa velludaa velludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velludaa velludaa velludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vidhiya neeyum velludaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vidhiya neeyum velludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Odudaa Odudaa Odudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odudaa Odudaa Odudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum kaalillaama Odudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum kaalillaama Odudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadundaa paadundaa paadundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadundaa paadundaa paadundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pugazha ulagam paadundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pugazha ulagam paadundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey nillu nillu nillu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nillu nillu nillu nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondha kaalil neeyum nillu…. nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondha kaalil neeyum nillu…. nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye thallu thallu thallu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thallu thallu thallu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaividhiya kaiyaal thallu…thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaividhiya kaiyaal thallu…thallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooiyaadhae ooiyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooiyaadhae ooiyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kashtam nashtam adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtam nashtam adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyaram vandhu thaakkum …paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyaram vandhu thaakkum …paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhu vandhaalum thuninji nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu vandhaalum thuninji nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu bayandhu odum …paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu bayandhu odum …paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbam mattum ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam mattum ada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkaiyinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkaiyinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkai boru aagum …paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai boru aagum …paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha inbam vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha inbam vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkai joru aagum …paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai joru aagum …paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey valiya thalli vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey valiya thalli vaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Latchiyatha ulla vaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latchiyatha ulla vaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna paarthu kaththukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna paarthu kaththukkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongaravan mulichukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongaravan mulichukkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odundaa odundaa odundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odundaa odundaa odundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam seekkiram odundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam seekkiram odundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodundaa koodundaa koodundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodundaa koodundaa koodundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vayasu seekkiram koodundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vayasu seekkiram koodundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedudaa thedudaa thedudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedudaa thedudaa thedudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un latchiya paathaiya thedudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un latchiya paathaiya thedudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethudaa yethudaa yethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethudaa yethudaa yethudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kodiya ulagil yethudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kodiya ulagil yethudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyi kaalu adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyi kaalu adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallaa irundhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallaa irundhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongura moonja paaru…paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongura moonja paaru…paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonam marandhu thulli gudhikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonam marandhu thulli gudhikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha paiyana paaru…paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha paiyana paaru…paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Computer pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Computer pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Moolai koduthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolai koduthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamiya thitraan paaru …paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamiya thitraan paaru …paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalillaama ponaal kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalillaama ponaal kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadum azhaga paaru…paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadum azhaga paaru…paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambikkaiya yethikkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkaiya yethikkadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththathula serthukkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththathula serthukkadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyaa uzhaichi neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyaa uzhaichi neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagatha maathikkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagatha maathikkadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaangadaa vaangadaa vaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangadaa vaangadaa vaangadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini varum kaalam naamadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini varum kaalam naamadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedundaa thedundaa thedundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedundaa thedundaa thedundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ulagam unna thedundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ulagam unna thedundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadandaa paadandaa paadandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadandaa paadandaa paadandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaazhkkai oru paadandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaazhkkai oru paadandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Serundaa serundaa serundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serundaa serundaa serundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum koottam serundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum koottam serundaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey nillu nillu nillu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey nillu nillu nillu nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondha kaalil neeyum nillu…. nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondha kaalil neeyum nillu…. nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye thallu thallu thallu thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thallu thallu thallu thallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaividhiya kaiyaal thallu…thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaividhiya kaiyaal thallu…thallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooiyaadhae ooiyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooiyaadhae ooiyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooiyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooiyaadhae"/>
</div>
</pre>
