---
title: "mudhal murai song lyrics"
album: "Seemathurai"
artist: "Jose Franklin"
lyricist: "Hari Krishna Devan"
director: "Santhosh Thiyagarajan"
path: "/albums/seemathurai-lyrics"
song: "Mudhal Murai"
image: ../../images/albumart/seemathurai.jpg
date: 2018-12-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EwSk5W6zp2c"
type: "love"
singers:
  - Jose Franklin
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">muthal murai parkinrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthal murai parkinrathe"/>
</div>
<div class="lyrico-lyrics-wrapper">un iru vizhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un iru vizhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">muthal murai ketkindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthal murai ketkindrathe"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir valiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir valiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">thana thana na nana na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana thana na nana na"/>
</div>
<div class="lyrico-lyrics-wrapper">tha nana nana na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tha nana nana na"/>
</div>
<div class="lyrico-lyrics-wrapper">thana thana na nana na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana thana na nana na"/>
</div>
<div class="lyrico-lyrics-wrapper">tha nana nana na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tha nana nana na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">idhu varaiyil naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu varaiyil naan"/>
</div>
<div class="lyrico-lyrics-wrapper">kanda megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanda megam"/>
</div>
<div class="lyrico-lyrics-wrapper">imaigal rendil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imaigal rendil"/>
</div>
<div class="lyrico-lyrics-wrapper">theera thagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera thagam"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugalil neeyanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugalil neeyanal"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal thiraveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal thiraveno"/>
</div>
<div class="lyrico-lyrics-wrapper">seematti vanthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seematti vanthale"/>
</div>
<div class="lyrico-lyrics-wrapper">sippam pol aanene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sippam pol aanene"/>
</div>
<div class="lyrico-lyrics-wrapper">nee theendi pogavittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee theendi pogavittal"/>
</div>
<div class="lyrico-lyrics-wrapper">sillai naanaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sillai naanaven"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjellam nee thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjellam nee thane"/>
</div>
<div class="lyrico-lyrics-wrapper">nee indri naan veene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee indri naan veene"/>
</div>
<div class="lyrico-lyrics-wrapper">neerukkul meena pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neerukkul meena pola"/>
</div>
<div class="lyrico-lyrics-wrapper">unnul vazhvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnul vazhvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaar parka vantha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar parka vantha "/>
</div>
<div class="lyrico-lyrics-wrapper">nilavu ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavu ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan partha udan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan partha udan"/>
</div>
<div class="lyrico-lyrics-wrapper">muraikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muraikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">nee parthu sendra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee parthu sendra"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan jeevan unakena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan jeevan unakena"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir kondathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir kondathe"/>
</div>
<div class="lyrico-lyrics-wrapper">unakena thavikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakena thavikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">enakulle than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakulle than"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yaru"/>
</div>
<div class="lyrico-lyrics-wrapper">usurula uraiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usurula uraiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">naane naanilla nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naane naanilla nee than"/>
</div>
<div class="lyrico-lyrics-wrapper">en nijam athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nijam athu"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai seraamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai seraamal"/>
</div>
<div class="lyrico-lyrics-wrapper">en nizhalathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nizhalathu "/>
</div>
<div class="lyrico-lyrics-wrapper">ennai serathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai serathu"/>
</div>
<div class="lyrico-lyrics-wrapper">en pagalgalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pagalgalum"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai paramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai paramal"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal moodi uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal moodi uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">vidum thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidum thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">seematti unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seematti unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">sirupillai anene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirupillai anene"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaiyaati bomma pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaiyaati bomma pola"/>
</div>
<div class="lyrico-lyrics-wrapper">un sol ketpene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sol ketpene"/>
</div>
<div class="lyrico-lyrics-wrapper">karagam thaan illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karagam thaan illama"/>
</div>
<div class="lyrico-lyrics-wrapper">thara meethu nillama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thara meethu nillama"/>
</div>
<div class="lyrico-lyrics-wrapper">yen boomi unna than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen boomi unna than"/>
</div>
<div class="lyrico-lyrics-wrapper">nitham suthuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitham suthuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaan parka thendral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaan parka thendral"/>
</div>
<div class="lyrico-lyrics-wrapper">therikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">then varka malar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="then varka malar"/>
</div>
<div class="lyrico-lyrics-wrapper">virigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thalatta manam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalatta manam "/>
</div>
<div class="lyrico-lyrics-wrapper">thudikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">tholil unnai saithukolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholil unnai saithukolla"/>
</div>
<div class="lyrico-lyrics-wrapper">azhaithidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaithidave"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum vazhvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum vazhvene"/>
</div>
<div class="lyrico-lyrics-wrapper">senmam nooraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senmam nooraga"/>
</div>
<div class="lyrico-lyrics-wrapper">en oru varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en oru varam"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai naan kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai naan kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum illamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum illamal"/>
</div>
<div class="lyrico-lyrics-wrapper">jeevan nillathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeevan nillathe"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir unathaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir unathaai"/>
</div>
<div class="lyrico-lyrics-wrapper">en iravugal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en iravugal "/>
</div>
<div class="lyrico-lyrics-wrapper">oynthu ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oynthu ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">un ninaivugal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ninaivugal "/>
</div>
<div class="lyrico-lyrics-wrapper">thooram pogathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram pogathe"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir athu"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai seraamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai seraamal"/>
</div>
<div class="lyrico-lyrics-wrapper">irandhida thuniven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irandhida thuniven"/>
</div>
<div class="lyrico-lyrics-wrapper">un iru karam pidithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un iru karam pidithida"/>
</div>
<div class="lyrico-lyrics-wrapper">seematti unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seematti unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">seemathurai naan anen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seemathurai naan anen"/>
</div>
<div class="lyrico-lyrics-wrapper">therotti varuvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therotti varuvene"/>
</div>
<div class="lyrico-lyrics-wrapper">unna thooka than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna thooka than"/>
</div>
<div class="lyrico-lyrics-wrapper">rasathi unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasathi unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">raa thookam poyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raa thookam poyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">raasakku yetha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raasakku yetha"/>
</div>
<div class="lyrico-lyrics-wrapper">rani sodi serthachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rani sodi serthachu"/>
</div>
<div class="lyrico-lyrics-wrapper">seematti seematti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seematti seematti"/>
</div>
<div class="lyrico-lyrics-wrapper">sirasellam pochooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirasellam pochooti"/>
</div>
<div class="lyrico-lyrics-wrapper">seeraga unna keppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeraga unna keppen"/>
</div>
<div class="lyrico-lyrics-wrapper">maman kitta thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maman kitta thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">mana malai tharuvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana malai tharuvene"/>
</div>
<div class="lyrico-lyrics-wrapper">madi mela vizhuvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madi mela vizhuvene"/>
</div>
<div class="lyrico-lyrics-wrapper">mannodu ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannodu ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">unnil vaazhvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnil vaazhvene"/>
</div>
</pre>
