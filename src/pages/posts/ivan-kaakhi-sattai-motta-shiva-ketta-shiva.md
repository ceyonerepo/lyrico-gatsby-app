---
title: "ivan kaakhi sattai song lyrics"
album: "Motta Shiva Ketta Shiva"
artist: "Amresh Ganesh"
lyricist: "Vairamuthu"
director: "Sai Ramani"
path: "/albums/motta-shiva-ketta-shiva-lyrics"
song: "Ivan Kaakhi Sattai"
image: ../../images/albumart/motta-shiva-ketta-shiva.jpg
date: 2017-03-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dhy_cjy3tCI"
type: "mass"
singers:
  -	Amresh Ganesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Motta shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta shiva daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta shiva daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan kaakhi satta fire-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan kaakhi satta fire-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kanna paaru power-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kanna paaru power-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ema tharman veettu kayiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ema tharman veettu kayiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan kayyil edhiri uyiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan kayyil edhiri uyiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan vazhumpodhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan vazhumpodhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Valadhu kayyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valadhu kayyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Revolveru paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Revolveru paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Real real
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Real real"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan real hero thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan real hero thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyal seiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyal seiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan seiyal puyalthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan seiyal puyalthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Real real
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Real real"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan real hero thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan real hero thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyal seiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyal seiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan seiyal puyalthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan seiyal puyalthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivanai vellathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanai vellathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada unnaal mudiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada unnaal mudiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrai vettathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrai vettathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kaththi kidaiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaththi kidaiyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda idhamenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda idhamenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhirithaan theermaanippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirithaan theermaanippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum yutham vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum yutham vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanaethaan munnae nippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanaethaan munnae nippaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalaatta evan vandhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalaatta evan vandhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veppendaa aappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppendaa aappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai keezha pidichalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai keezha pidichalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaanda toppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaanda toppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Real real
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Real real"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan real hero thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan real hero thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyal seiyalIvan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyal seiyalIvan "/>
</div>
<div class="lyrico-lyrics-wrapper">seiyal puyalthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seiyal puyalthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Real real
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Real real"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan real hero thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan real hero thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyal seiyalIvan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyal seiyalIvan "/>
</div>
<div class="lyrico-lyrics-wrapper">seiyal puyalthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seiyal puyalthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru kadavul kaiyyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru kadavul kaiyyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vevveru aayudham undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vevveru aayudham undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhandha aayudham ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhandha aayudham ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kayyil mattum undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kayyil mattum undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkalthaan undhan pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkalthaan undhan pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkalthaan endhan pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkalthaan endhan pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvaraikkum trailer paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvaraikkum trailer paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimaethaan yekka chakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimaethaan yekka chakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemanoda kootaali naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemanoda kootaali naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamae dhoolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamae dhoolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanthaanda manitha singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthaanda manitha singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Climax-il paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Climax-il paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Real real
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Real real"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan real hero thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan real hero thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyal seiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyal seiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan seiyal puyalthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan seiyal puyalthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Real real
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Real real"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan real hero thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan real hero thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyal seiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyal seiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan seiyal puyalthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan seiyal puyalthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan kaakhi satta fire-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan kaakhi satta fire-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kanna paaru power-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kanna paaru power-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ema tharman veettu kayiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ema tharman veettu kayiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan kayyil edhiri uyiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan kayyil edhiri uyiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan vazhumpodhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan vazhumpodhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Valadhu kayyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valadhu kayyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Revolveru paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Revolveru paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Real real
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Real real"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan real hero thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan real hero thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyal seiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyal seiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan seiyal puyalthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan seiyal puyalthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Real real
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Real real"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan real hero thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan real hero thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyal seiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyal seiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan seiyal puyalthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan seiyal puyalthaan"/>
</div>
</pre>
