---
title: "thalaiva song lyrics"
album: "Dha Dha 87"
artist: "Leander Lee Marty"
lyricist: "Vijay Sri G"
director: "Vijay Sri G"
path: "/albums/dha-dha-87-lyrics"
song: "Thalaiva"
image: ../../images/albumart/dha-dha-87.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lE-B_WM1_Ew"
type: "motivational"
singers:
  - Anand Aravindakshan
  - Shenbagaraj Ganesalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thamizha en thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizha en thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poraa nam mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poraa nam mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">un munnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un munnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">varavaa en udanvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varavaa en udanvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">en thozhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thozhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nam naatil jadhiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam naatil jadhiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">ver aruppaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ver aruppaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee seran sozhan pandiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee seran sozhan pandiya"/>
</div>
<div class="lyrico-lyrics-wrapper">inam endru nee edharkai  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inam endru nee edharkai  "/>
</div>
<div class="lyrico-lyrics-wrapper">marandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">vinai aruppai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinai aruppai"/>
</div>
<div class="lyrico-lyrics-wrapper">dhrogam ozhipaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhrogam ozhipaai"/>
</div>
<div class="lyrico-lyrics-wrapper">udan sera koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udan sera koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee azhipaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee azhipaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jaadhi madham paaramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaadhi madham paaramal"/>
</div>
<div class="lyrico-lyrics-wrapper">naangal ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangal ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhum koottangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhum koottangal"/>
</div>
<div class="lyrico-lyrics-wrapper">anbu ullam kollungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu ullam kollungal"/>
</div>
<div class="lyrico-lyrics-wrapper">manidham pazhagungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manidham pazhagungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jaadhi madham paaramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaadhi madham paaramal"/>
</div>
<div class="lyrico-lyrics-wrapper">naangal ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangal ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhum koottangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhum koottangal"/>
</div>
<div class="lyrico-lyrics-wrapper">anbu ullam kollungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbu ullam kollungal"/>
</div>
<div class="lyrico-lyrics-wrapper">manidham pazhagungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manidham pazhagungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">madhangal vedithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhangal vedithal"/>
</div>
<div class="lyrico-lyrics-wrapper">naan padayaga thiralvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan padayaga thiralvene"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaigal thaduthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaigal thaduthal"/>
</div>
<div class="lyrico-lyrics-wrapper">udaitherivene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udaitherivene "/>
</div>
<div class="lyrico-lyrics-wrapper">neruppai azhipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppai azhipen"/>
</div>
<div class="lyrico-lyrics-wrapper">en makkalai vadhathale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en makkalai vadhathale"/>
</div>
<div class="lyrico-lyrics-wrapper">verum silayaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verum silayaai"/>
</div>
<div class="lyrico-lyrics-wrapper">naan irukamaatene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan irukamaatene"/>
</div>
<div class="lyrico-lyrics-wrapper">vazha vazhiyindri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazha vazhiyindri"/>
</div>
<div class="lyrico-lyrics-wrapper">agadhiyai vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="agadhiyai vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">enai suttaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai suttaye"/>
</div>
<div class="lyrico-lyrics-wrapper">unna nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna nambi"/>
</div>
<div class="lyrico-lyrics-wrapper">vandha pengalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandha pengalin"/>
</div>
<div class="lyrico-lyrics-wrapper">thugil urithaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thugil urithaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">vidamatten 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidamatten "/>
</div>
<div class="lyrico-lyrics-wrapper">edhir nirppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhir nirppen"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir keppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir keppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thamizha en thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizha en thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poraa nam mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poraa nam mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">un munnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un munnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">varavaa en udanvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varavaa en udanvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">en thozhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thozhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nam naatil jadhiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam naatil jadhiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">ver aruppaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ver aruppaai"/>
</div>
</pre>
