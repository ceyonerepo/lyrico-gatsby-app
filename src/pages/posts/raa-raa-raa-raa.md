---
title: "raa raa song lyrics"
album: "Raa Raa"
artist: "Srikanth Deva"
lyricist: "Pon. Ravindran"
director: "Sandilya"
path: "/albums/raa-raa-lyrics"
song: "Raa Raa"
image: ../../images/albumart/raa-raa.jpg
date: 2011-10-07
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Keerthiga Udhaya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raa raa raa raa chennaikki raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa raa raa raa chennaikki raa"/>
</div>
<div class="lyrico-lyrics-wrapper">munneralaam joaraa joaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munneralaam joaraa joaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">raa raa raa raa chenaikki raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raa raa raa raa chenaikki raa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhavaikkum joara joara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhavaikkum joara joara"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasu panatha serthukkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu panatha serthukkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaruvaangi Ottikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaruvaangi Ottikkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">latchiyatha adainjikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="latchiyatha adainjikkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu dhisaiyum pugazh vaangalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu dhisaiyum pugazh vaangalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalu poayi kaththi vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalu poayi kaththi vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">dom dom dom dom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dom dom dom dom"/>
</div>
<div class="lyrico-lyrics-wrapper">kaththi poayi vaalu vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaththi poayi vaalu vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">dum dum dum dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dum dum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">raa raa raa raa chenaikki raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raa raa raa raa chenaikki raa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhavaikkum joara joara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhavaikkum joara joara"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasu panatha serthukkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu panatha serthukkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaruvaangi Ottikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaruvaangi Ottikkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">latchiyatha adainjikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="latchiyatha adainjikkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu dhisaiyum pugazh vaangalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu dhisaiyum pugazh vaangalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennai thaandaa thamizhnaattoada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai thaandaa thamizhnaattoada"/>
</div>
<div class="lyrico-lyrics-wrapper">sangamama irukkudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangamama irukkudhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nanbaa therinjikkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanbaa therinjikkadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">chennaithaandaa indha maadarkkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chennaithaandaa indha maadarkkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">bestaaga irukkudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bestaaga irukkudhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam solludhadaa nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam solludhadaa nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu serum munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu serum munnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">poiyum sollu thappilladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poiyum sollu thappilladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru mechumbadiyum neeyirundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru mechumbadiyum neeyirundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">jenmam yezhum Super thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jenmam yezhum Super thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalu poayi kaththi vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalu poayi kaththi vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">dom dom dom dom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dom dom dom dom"/>
</div>
<div class="lyrico-lyrics-wrapper">kaththi poayi vaalu vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaththi poayi vaalu vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">dum dum dum dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dum dum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">raa raa raa raa chenaikki raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raa raa raa raa chenaikki raa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhavaikkum joara joara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhavaikkum joara joara"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasu panatha serthukkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu panatha serthukkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaruvaangi Ottikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaruvaangi Ottikkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">latchiyatha adainjikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="latchiyatha adainjikkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu dhisaiyum pugazh vaangalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu dhisaiyum pugazh vaangalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palaber thaan indha pattanathil uzhaichaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palaber thaan indha pattanathil uzhaichaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">osandhaanga thoazhaa padichikkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osandhaanga thoazhaa padichikkadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">jaadhi madham boy city
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaadhi madham boy city"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-fukkulla illaiyappa illaiyappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-fukkulla illaiyappa illaiyappa"/>
</div>
<div class="lyrico-lyrics-wrapper">onnaa vaazhndhukkappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnaa vaazhndhukkappa"/>
</div>
<div class="lyrico-lyrics-wrapper">beachil neeyum irundhuppaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beachil neeyum irundhuppaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kavalai theerum thannaaladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalai theerum thannaaladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sitti muzhukka suththi vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sitti muzhukka suththi vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhathoanum munaaladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhathoanum munaaladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalu poayi kaththi vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalu poayi kaththi vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">dom dom dom dom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dom dom dom dom"/>
</div>
<div class="lyrico-lyrics-wrapper">kaththi poayi vaalu vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaththi poayi vaalu vandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">dum dum dum dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dum dum dum dum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">raa raa raa raa chenaikki raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raa raa raa raa chenaikki raa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhavaikkum joara joara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhavaikkum joara joara"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasu panatha serthukkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu panatha serthukkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaruvaangi Ottikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaruvaangi Ottikkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">latchiyatha adainjikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="latchiyatha adainjikkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu dhisaiyum pugazh vaangalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu dhisaiyum pugazh vaangalaam"/>
</div>
</pre>
