---
title: 'maaris aanandhi song lyrics'
album: 'Maari 2'
artist: 'Yuvan Shankar Raja'
lyricist: 'Poetu Dhanush'
director: 'Balaji Mohan'
path: '/albums/maari2-song-lyrics'
song: "Maari's Aanandhi"
image: ../../images/albumart/Maari-2.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AGiVsZGmkJU"
type: 'Love'
singers: 
- Dr. Ilaiyaraaja
- M M Manasi
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Vaanam pozhiyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam pozhiyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi vilaiyuma kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi vilaiyuma kooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pokkal malarnthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkal malarnthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodum azhagil thaan paeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodum azhagil thaan paeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan uyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unna paathukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unna paathukkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu thuniyaa poththikkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu thuniyaa poththikkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai medhuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai medhuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalaiyae maththikitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaiyae maththikitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Keedhallam koottikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keedhallam koottikkitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Joraa nada pottu vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joraa nada pottu vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda veera aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda veera aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey eh eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eh eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Pair ah auto la polaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pair ah auto la polaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda meeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda meeraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hae ae ae hae ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hae ae ae hae ey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattilum raagam paaduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattilum raagam paaduthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saanjathum thookkam modhuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanjathum thookkam modhuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimmathi unnaal vanthathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimmathi unnaal vanthathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedalum thaanaai ponathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalum thaanaai ponathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjilae unna naan sumappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjilae unna naan sumappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnilae niththam naan parappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnilae niththam naan parappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyae enna suththuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiyae enna suththuthaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalum thaanaai sokkuthaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalum thaanaai sokkuthaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhiyai sari seiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiyai sari seiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi vandha dhevathaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi vandha dhevathaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhithaai piranthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhithaai piranthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandri solla vaarthai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandri solla vaarthai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam uruguthae raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam uruguthae raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullavarai ellaam neethaan di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullavarai ellaam neethaan di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam pozhiyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam pozhiyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi vilaiyuma kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi vilaiyuma kooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pokkal malarnthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkal malarnthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodum azhagil thaan peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodum azhagil thaan peru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee endhan singakkutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee endhan singakkutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum urasaa thangakkatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum urasaa thangakkatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha morattu payakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha morattu payakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu vasamaa enkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu vasamaa enkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattikkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikkitta"/>
</div>
</pre>