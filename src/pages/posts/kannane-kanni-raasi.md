---
title: "kannane song lyrics"
album: "Kanni Raasi"
artist: "Vishal Chandrasekhar"
lyricist: "Yugabharathi"
director: "Muthukumaran"
path: "/albums/kanni-raasi-lyrics"
song: "Kannane"
image: ../../images/albumart/kanni-raasi.jpg
date: 2020-12-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CdIZ6-uc-Ng"
type: "happy"
singers:
  - K. S. Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kannane maaya kannane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannane maaya kannane"/>
</div>
<div class="lyrico-lyrics-wrapper">unai ennida en manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai ennida en manam"/>
</div>
<div class="lyrico-lyrics-wrapper">paaduthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaduthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anbile thooya anbile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbile thooya anbile"/>
</div>
<div class="lyrico-lyrics-wrapper">sugam thanthathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugam thanthathai"/>
</div>
<div class="lyrico-lyrics-wrapper">solida venumaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solida venumaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhale sellum padhai endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhale sellum padhai endru"/>
</div>
<div class="lyrico-lyrics-wrapper">kangalal sonna gokulane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangalal sonna gokulane"/>
</div>
<div class="lyrico-lyrics-wrapper">koyile indha veedum endra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koyile indha veedum endra"/>
</div>
<div class="lyrico-lyrics-wrapper">kolgaiyai konda naayagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolgaiyai konda naayagane"/>
</div>
<div class="lyrico-lyrics-wrapper">vennai pole ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennai pole ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">ne virumbi thirudida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne virumbi thirudida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannane maaya kannane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannane maaya kannane"/>
</div>
<div class="lyrico-lyrics-wrapper">unai ennida en manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai ennida en manam"/>
</div>
<div class="lyrico-lyrics-wrapper">paaduthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaduthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enniya velaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enniya velaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">edhaiyum kudupathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhaiyum kudupathu"/>
</div>
<div class="lyrico-lyrics-wrapper">un valakamaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un valakamaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">pannidum leelaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pannidum leelaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">manathai kalaipathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathai kalaipathu"/>
</div>
<div class="lyrico-lyrics-wrapper">un palakamaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un palakamaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">illai pirivinai ennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai pirivinai ennai "/>
</div>
<div class="lyrico-lyrics-wrapper">ne sernthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne sernthida"/>
</div>
<div class="lyrico-lyrics-wrapper">inbamum kooduthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbamum kooduthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">ellai kadanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellai kadanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">anbena therndhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbena therndhida"/>
</div>
<div class="lyrico-lyrics-wrapper">thollaiyum ooduthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thollaiyum ooduthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai naan koodiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai naan koodiye"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjaiyum thatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjaiyum thatti"/>
</div>
<div class="lyrico-lyrics-wrapper">pudu vidha sugam tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudu vidha sugam tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannane maaya kannane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannane maaya kannane"/>
</div>
<div class="lyrico-lyrics-wrapper">unai ennida en manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai ennida en manam"/>
</div>
<div class="lyrico-lyrics-wrapper">paaduthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaduthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">punnagai veedhiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnagai veedhiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">idhalgal purapada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhalgal purapada "/>
</div>
<div class="lyrico-lyrics-wrapper">alaikuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaikuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">kan imai oorangalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan imai oorangalum"/>
</div>
<div class="lyrico-lyrics-wrapper">unaiye kalanthida thudikuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaiye kalanthida thudikuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">manjal mathi mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manjal mathi mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">kungumam aagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kungumam aagida"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaiyum theduthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaiyum theduthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">pongum ragasiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pongum ragasiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">poigalai pesida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poigalai pesida"/>
</div>
<div class="lyrico-lyrics-wrapper">othigai poduthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othigai poduthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">etheetho thonuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etheetho thonuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ennile vetkam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennile vetkam "/>
</div>
<div class="lyrico-lyrics-wrapper">thodarkathai eluthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodarkathai eluthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannane maaya kannane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannane maaya kannane"/>
</div>
<div class="lyrico-lyrics-wrapper">unai ennida en manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai ennida en manam"/>
</div>
<div class="lyrico-lyrics-wrapper">paaduthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaduthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnaiye naalum unnaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaiye naalum unnaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">valam vanthida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valam vanthida "/>
</div>
<div class="lyrico-lyrics-wrapper">jenmanum neelumaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jenmanum neelumaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">kodhai nan un poo madiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodhai nan un poo madiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">konjave vandha therkavipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjave vandha therkavipu"/>
</div>
<div class="lyrico-lyrics-wrapper">deepam pol intha veetinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deepam pol intha veetinile"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalave ennum or ninaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalave ennum or ninaipu"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai nan sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai nan sera"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam thinam urugida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam thinam urugida"/>
</div>
<div class="lyrico-lyrics-wrapper">kannane maaya kannane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannane maaya kannane"/>
</div>
</pre>
