---
title: "paathu paathu song lyrics"
album: "Virudhu"
artist: "Dhina "
lyricist: "Athavan"
director: "Athavan"
path: "/albums/virudhu-lyrics"
song: "Paathu Paathu"
image: ../../images/albumart/virudhu.jpg
date: 2019-12-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MvNRlXqSTV8"
type: "happy"
singers:
  - Varsha 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kokara kokara kokarako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokara kokara kokarako"/>
</div>
<div class="lyrico-lyrics-wrapper">kokara kokara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokara kokara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">kokara kokara kokarako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokara kokara kokarako"/>
</div>
<div class="lyrico-lyrics-wrapper">kokara kokara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokara kokara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">kokara kokara kokarako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokara kokara kokarako"/>
</div>
<div class="lyrico-lyrics-wrapper">kokara kokara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokara kokara ko"/>
</div>
<div class="lyrico-lyrics-wrapper">kokara kokara kokarako
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokara kokara kokarako"/>
</div>
<div class="lyrico-lyrics-wrapper">kokara kokara ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokara kokara ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pathu pathu pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu pathu pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha pookal asaiva pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha pookal asaiva pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">mella mella mella 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella mella mella "/>
</div>
<div class="lyrico-lyrics-wrapper">puthu thendral 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu thendral "/>
</div>
<div class="lyrico-lyrics-wrapper">thavanthu varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavanthu varuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu pathu pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu pathu pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha pookal asaiva pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha pookal asaiva pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">mella mella mella 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella mella mella "/>
</div>
<div class="lyrico-lyrics-wrapper">puthu thendral 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu thendral "/>
</div>
<div class="lyrico-lyrics-wrapper">thavanthu varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavanthu varuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vanthu vanthu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu vanthu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">chinna kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna kavithai"/>
</div>
<div class="lyrico-lyrics-wrapper">manathil vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">alaga alaga alaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaga alaga alaga"/>
</div>
<div class="lyrico-lyrics-wrapper">gaanam pada soluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaanam pada soluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">megal thantha malayale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megal thantha malayale"/>
</div>
<div class="lyrico-lyrics-wrapper">mannum manasum kulirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannum manasum kulirum"/>
</div>
<div class="lyrico-lyrics-wrapper">kulathu metu ellam pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulathu metu ellam pada"/>
</div>
<div class="lyrico-lyrics-wrapper">aadum medum athirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadum medum athirum"/>
</div>
<div class="lyrico-lyrics-wrapper">megal thantha malayale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megal thantha malayale"/>
</div>
<div class="lyrico-lyrics-wrapper">mannum manasum kulirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannum manasum kulirum"/>
</div>
<div class="lyrico-lyrics-wrapper">kulathu metu ellam pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulathu metu ellam pada"/>
</div>
<div class="lyrico-lyrics-wrapper">aadum medum athirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadum medum athirum"/>
</div>
<div class="lyrico-lyrics-wrapper">en manasum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasum "/>
</div>
<div class="lyrico-lyrics-wrapper">kuliruthadi thanala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuliruthadi thanala"/>
</div>
<div class="lyrico-lyrics-wrapper">en patum irukumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en patum irukumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">athu pol athu ketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu pol athu ketu"/>
</div>
<div class="lyrico-lyrics-wrapper">unga param 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga param "/>
</div>
<div class="lyrico-lyrics-wrapper">kuraiyuthadi papala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuraiyuthadi papala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pathu pathu pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu pathu pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha pookal asaiva pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha pookal asaiva pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">mella mella mella 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella mella mella "/>
</div>
<div class="lyrico-lyrics-wrapper">puthu thendral 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu thendral "/>
</div>
<div class="lyrico-lyrics-wrapper">thavanthu varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavanthu varuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aathu thanila adichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathu thanila adichu"/>
</div>
<div class="lyrico-lyrics-wrapper">oodum kenda aiyra meenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodum kenda aiyra meenum"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum thulli vilaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum thulli vilaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">mayilum aattam podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayilum aattam podum"/>
</div>
<div class="lyrico-lyrics-wrapper">aathu thanila adichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathu thanila adichu"/>
</div>
<div class="lyrico-lyrics-wrapper">oodum kenda aiyra meenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodum kenda aiyra meenum"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum thulli vilaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum thulli vilaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">mayilum aattam podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayilum aattam podum"/>
</div>
<div class="lyrico-lyrics-wrapper">kolantha pola manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolantha pola manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">iruku maiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruku maiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">puliya pola sanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puliya pola sanda"/>
</div>
<div class="lyrico-lyrics-wrapper">poduven kaiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poduven kaiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">enna pathu podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pathu podum"/>
</div>
<div class="lyrico-lyrics-wrapper">koothu adhiruam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koothu adhiruam "/>
</div>
<div class="lyrico-lyrics-wrapper">adi appala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi appala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pathu pathu pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu pathu pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha pookal asaiva pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha pookal asaiva pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">mella mella mella 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella mella mella "/>
</div>
<div class="lyrico-lyrics-wrapper">puthu thendral 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu thendral "/>
</div>
<div class="lyrico-lyrics-wrapper">thavanthu varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavanthu varuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu pathu pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu pathu pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha pookal asaiva pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha pookal asaiva pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">mella mella mella 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella mella mella "/>
</div>
<div class="lyrico-lyrics-wrapper">puthu thendral 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu thendral "/>
</div>
<div class="lyrico-lyrics-wrapper">thavanthu varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavanthu varuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vanthu vanthu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu vanthu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">chinna kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna kavithai"/>
</div>
<div class="lyrico-lyrics-wrapper">manathil vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathil vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">alaga alaga alaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaga alaga alaga"/>
</div>
<div class="lyrico-lyrics-wrapper">gaanam pada soluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaanam pada soluthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pathu pathu pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu pathu pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha pookal asaiva pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha pookal asaiva pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">mella mella mella 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella mella mella "/>
</div>
<div class="lyrico-lyrics-wrapper">puthu thendral 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu thendral "/>
</div>
<div class="lyrico-lyrics-wrapper">thavanthu varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavanthu varuthu"/>
</div>
</pre>
