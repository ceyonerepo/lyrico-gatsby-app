---
title: "ponnukulle song lyrics"
album: "Antony"
artist: "Sivatmikha"
lyricist: "Marabin Maindan Muthaiah"
director: "Kutti Kumar"
path: "/albums/antony-lyrics"
song: "Ponnukkulle"
image: ../../images/albumart/antony.jpg
date: 2018-06-01
lang: tamil
youtubeLink: 
type: "sad"
singers:
  - Vel Murugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ponnukulle naan edutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnukulle naan edutha"/>
</div>
<div class="lyrico-lyrics-wrapper">puthaiyal enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthaiyal enge"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla naan valartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla naan valartha"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu enge"/>
</div>
<div class="lyrico-lyrics-wrapper">mannukulla thendratha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannukulla thendratha "/>
</div>
<div class="lyrico-lyrics-wrapper">magane magane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magane magane "/>
</div>
<div class="lyrico-lyrics-wrapper">vinnukulla unna thedrene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnukulla unna thedrene"/>
</div>
<div class="lyrico-lyrics-wrapper">magane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magane"/>
</div>
<div class="lyrico-lyrics-wrapper">natchathiram pol 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natchathiram pol "/>
</div>
<div class="lyrico-lyrics-wrapper">jolipane nenjukulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jolipane nenjukulle"/>
</div>
<div class="lyrico-lyrics-wrapper">silu siluppane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silu siluppane"/>
</div>
<div class="lyrico-lyrics-wrapper">mannukule thedratha magane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannukule thedratha magane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">ohho ohho ohho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohho ohho ohho"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai thedum en manasukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedum en manasukku"/>
</div>
<div class="lyrico-lyrics-wrapper">enna pathil than solven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pathil than solven"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai thedum en manasukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedum en manasukku"/>
</div>
<div class="lyrico-lyrics-wrapper">enna pathil than solven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pathil than solven"/>
</div>
<div class="lyrico-lyrics-wrapper">inthe theer thaan pathi than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inthe theer thaan pathi than"/>
</div>
<div class="lyrico-lyrics-wrapper">soll anbe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soll anbe "/>
</div>
<div class="lyrico-lyrics-wrapper">nee ennai pirintha nimidangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ennai pirintha nimidangal"/>
</div>
<div class="lyrico-lyrics-wrapper">naragame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naragame"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu thisai suthi vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu thisai suthi vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">sooriyane nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sooriyane nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">petha pulla engu irukan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petha pulla engu irukan"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu vanthu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu vanthu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">olichama enga boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olichama enga boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">unna enna saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna enna saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ponnukulle naan edutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnukulle naan edutha"/>
</div>
<div class="lyrico-lyrics-wrapper">puthaiyal enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthaiyal enge"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla naan valartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla naan valartha"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu enge"/>
</div>
<div class="lyrico-lyrics-wrapper">mannukulla thendratha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannukulla thendratha "/>
</div>
<div class="lyrico-lyrics-wrapper">magane magane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magane magane "/>
</div>
<div class="lyrico-lyrics-wrapper">vinnukulla unna therene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnukulla unna therene"/>
</div>
<div class="lyrico-lyrics-wrapper">magane magane magane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magane magane magane"/>
</div>
</pre>
