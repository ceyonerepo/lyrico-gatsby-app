---
title: 'kaalam song lyrics'
album: 'Nerkonda Paarvai'
artist: 'Yuvan Shankar Raja'
lyricist: 'Nagarjoon R, Yunohoo'
director: 'H.Vinoth'
path: '/albums/comali-song-lyrics'
song: 'Kaalam'
image: ../../images/albumart/nerkonda-paarvai.jpg
date: 2019-08-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QiKEnrvr0N0"
type: 'sad'
singers: 
- Alisha Thomas
- Yunohoo
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Kaalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pudhiya rajiyam inimel
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru pudhiya rajiyam inimel"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosham
<input type="checkbox" class="lyrico-select-lyric-line" value="Santhosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Just keep on smiling
<input type="checkbox" class="lyrico-select-lyric-line" value="Just keep on smiling"/>
</div>
<div class="lyrico-lyrics-wrapper">Ain’t got time for kovam
<input type="checkbox" class="lyrico-select-lyric-line" value="Ain’t got time for kovam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaalam kaalamaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam kaalamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkudhu veri vendaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Irukkudhu veri vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pick up get up lookup
<input type="checkbox" class="lyrico-select-lyric-line" value="Pick up get up lookup"/>
</div>
<div class="lyrico-lyrics-wrapper">It’s a new millennium
<input type="checkbox" class="lyrico-select-lyric-line" value="It’s a new millennium"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kavalai vendaamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavalai vendaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thozha
<input type="checkbox" class="lyrico-select-lyric-line" value="En thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vidhamaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudhu vidhamaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbangalai theda
<input type="checkbox" class="lyrico-select-lyric-line" value="Inbangalai theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu paarthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarthu paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nadaiyai padi
<input type="checkbox" class="lyrico-select-lyric-line" value="Un nadaiyai padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Purindhum puriyaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Purindhum puriyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanai midhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Avanai midhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kavalai vendaamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavalai vendaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thozha
<input type="checkbox" class="lyrico-select-lyric-line" value="En thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vidhamaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudhu vidhamaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbangalai theda
<input type="checkbox" class="lyrico-select-lyric-line" value="Inbangalai theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu paarthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarthu paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nadaiyai padi
<input type="checkbox" class="lyrico-select-lyric-line" value="Un nadaiyai padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Purindhum puriyaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Purindhum puriyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanai midhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Avanai midhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ring tong
<input type="checkbox" class="lyrico-select-lyric-line" value="Ring tong"/>
</div>
  <div class="lyrico-lyrics-wrapper">Haa haa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa haa…."/>
</div>
  <div class="lyrico-lyrics-wrapper">Ring tong
<input type="checkbox" class="lyrico-select-lyric-line" value="Ring tong"/>
</div>
  <div class="lyrico-lyrics-wrapper">Haa haa…. (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa haa…."/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haa haa haa haa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa haa haa haa…."/>
</div>
  <div class="lyrico-lyrics-wrapper">Ring tong
<input type="checkbox" class="lyrico-select-lyric-line" value="Ring tong"/>
</div>
<div class="lyrico-lyrics-wrapper">I gotta sing a song
<input type="checkbox" class="lyrico-select-lyric-line" value="I gotta sing a song"/>
</div>
<div class="lyrico-lyrics-wrapper">Bidi bop bop
<input type="checkbox" class="lyrico-select-lyric-line" value="Bidi bop bop"/>
</div>
<div class="lyrico-lyrics-wrapper">Lemme hear you sing along
<input type="checkbox" class="lyrico-select-lyric-line" value="Lemme hear you sing along"/>
</div>
<div class="lyrico-lyrics-wrapper">King to a guy hong kong
<input type="checkbox" class="lyrico-select-lyric-line" value="King to a guy hong kong"/>
</div>
<div class="lyrico-lyrics-wrapper">Never go wrong
<input type="checkbox" class="lyrico-select-lyric-line" value="Never go wrong"/>
</div>
<div class="lyrico-lyrics-wrapper">Just I wanna tag along
<input type="checkbox" class="lyrico-select-lyric-line" value="Just I wanna tag along"/>
</div>
<div class="lyrico-lyrics-wrapper">Summer long baywatch
<input type="checkbox" class="lyrico-select-lyric-line" value="Summer long baywatch"/>
</div>
<div class="lyrico-lyrics-wrapper">Got ma billabong on
<input type="checkbox" class="lyrico-select-lyric-line" value="Got ma billabong on"/>
</div>
<div class="lyrico-lyrics-wrapper">Let me say what they want
<input type="checkbox" class="lyrico-select-lyric-line" value="Let me say what they want"/>
</div>
<div class="lyrico-lyrics-wrapper">Got it like a lion
<input type="checkbox" class="lyrico-select-lyric-line" value="Got it like a lion"/>
</div>
<div class="lyrico-lyrics-wrapper">Goose egg billion
<input type="checkbox" class="lyrico-select-lyric-line" value="Goose egg billion"/>
</div>
<div class="lyrico-lyrics-wrapper">Doesn’t matter stay strong
<input type="checkbox" class="lyrico-select-lyric-line" value="Doesn’t matter stay strong"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  …………………………………
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey yeah…
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey yeah…"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekka katti naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Rekka katti naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannil parakka poren
<input type="checkbox" class="lyrico-select-lyric-line" value="Vannil parakka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Laughing smiley otti
<input type="checkbox" class="lyrico-select-lyric-line" value="Laughing smiley otti"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna marakka poren
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna marakka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu mettu katti
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudhu mettu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil midhakka poren
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanavil midhakka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgatha mootta katta poren
<input type="checkbox" class="lyrico-select-lyric-line" value="Sorgatha mootta katta poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Save it
<input type="checkbox" class="lyrico-select-lyric-line" value="Save it"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t wanna hear it
<input type="checkbox" class="lyrico-select-lyric-line" value="Don’t wanna hear it"/>
</div>
<div class="lyrico-lyrics-wrapper">I got it lit now bag it
<input type="checkbox" class="lyrico-select-lyric-line" value="I got it lit now bag it"/>
</div>
<div class="lyrico-lyrics-wrapper">Gimme the credit
<input type="checkbox" class="lyrico-select-lyric-line" value="Gimme the credit"/>
</div>
<div class="lyrico-lyrics-wrapper">Back with a hit
<input type="checkbox" class="lyrico-select-lyric-line" value="Back with a hit"/>
</div>
<div class="lyrico-lyrics-wrapper">So save it (3 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="So save it"/></div>
</div>
<div class="lyrico-lyrics-wrapper">Don’t wanna hear it
<input type="checkbox" class="lyrico-select-lyric-line" value="Don’t wanna hear it"/>
</div>
<div class="lyrico-lyrics-wrapper">I got it lit now bag it
<input type="checkbox" class="lyrico-select-lyric-line" value="I got it lit now bag it"/>
</div>
<div class="lyrico-lyrics-wrapper">Gimme the credit
<input type="checkbox" class="lyrico-select-lyric-line" value="Gimme the credit"/>
</div>
<div class="lyrico-lyrics-wrapper">Back with a hit so
<input type="checkbox" class="lyrico-select-lyric-line" value="Back with a hit so"/>
</div>
<div class="lyrico-lyrics-wrapper">Hit so
<input type="checkbox" class="lyrico-select-lyric-line" value="Hit so"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mela yeri vaarom
<input type="checkbox" class="lyrico-select-lyric-line" value="Mela yeri vaarom"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hit so
<input type="checkbox" class="lyrico-select-lyric-line" value="Hit so"/>
</div>
  <div class="lyrico-lyrics-wrapper">Odhungaadhadaa dhooram
<input type="checkbox" class="lyrico-select-lyric-line" value="Odhungaadhadaa dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu namma aadum neram
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithu namma aadum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponga ellaam oram
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponga ellaam oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Alpha beta gaamma
<input type="checkbox" class="lyrico-select-lyric-line" value="Alpha beta gaamma"/>
</div>
<div class="lyrico-lyrics-wrapper">En kooda aada vaamaa
<input type="checkbox" class="lyrico-select-lyric-line" value="En kooda aada vaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkathaema calm-ah
<input type="checkbox" class="lyrico-select-lyric-line" value="Irukkathaema calm-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey adraa adraa
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey adraa adraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ring tong
<input type="checkbox" class="lyrico-select-lyric-line" value="Ring tong"/>
</div>
  <div class="lyrico-lyrics-wrapper">Haa haa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa haa…."/>
</div>
  <div class="lyrico-lyrics-wrapper">Ring tong
<input type="checkbox" class="lyrico-select-lyric-line" value="Ring tong"/>
</div>
  <div class="lyrico-lyrics-wrapper">Haa haa…. (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa haa…."/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haa haa haa haa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa haa haa haa…."/>
</div>
  <div class="lyrico-lyrics-wrapper">Ring tong
<input type="checkbox" class="lyrico-select-lyric-line" value="Ring tong"/>
</div>
<div class="lyrico-lyrics-wrapper">I gotta sing a song
<input type="checkbox" class="lyrico-select-lyric-line" value="I gotta sing a song"/>
</div>
<div class="lyrico-lyrics-wrapper">Bidi bop bop
<input type="checkbox" class="lyrico-select-lyric-line" value="Bidi bop bop"/>
</div>
<div class="lyrico-lyrics-wrapper">Lemme hear you sing along
<input type="checkbox" class="lyrico-select-lyric-line" value="Lemme hear you sing along"/>
</div>
<div class="lyrico-lyrics-wrapper">King to a guy hong kong
<input type="checkbox" class="lyrico-select-lyric-line" value="King to a guy hong kong"/>
</div>
<div class="lyrico-lyrics-wrapper">Never go wrong
<input type="checkbox" class="lyrico-select-lyric-line" value="Never go wrong"/>
</div>
<div class="lyrico-lyrics-wrapper">Just I wanna tag along
<input type="checkbox" class="lyrico-select-lyric-line" value="Just I wanna tag along"/>
</div>
<div class="lyrico-lyrics-wrapper">Summer long baywatch
<input type="checkbox" class="lyrico-select-lyric-line" value="Summer long baywatch"/>
</div>
<div class="lyrico-lyrics-wrapper">Got ma billabong on
<input type="checkbox" class="lyrico-select-lyric-line" value="Got ma billabong on"/>
</div>
<div class="lyrico-lyrics-wrapper">Let me say what they want
<input type="checkbox" class="lyrico-select-lyric-line" value="Let me say what they want"/>
</div>
<div class="lyrico-lyrics-wrapper">Got it like a lion
<input type="checkbox" class="lyrico-select-lyric-line" value="Got it like a lion"/>
</div>
<div class="lyrico-lyrics-wrapper">Goose egg billion
<input type="checkbox" class="lyrico-select-lyric-line" value="Goose egg billion"/>
</div>
<div class="lyrico-lyrics-wrapper">Doesn’t matter stay strong
<input type="checkbox" class="lyrico-select-lyric-line" value="Doesn’t matter stay strong"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haa haa haa haa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa haa haa haa…."/>
</div>
</pre>