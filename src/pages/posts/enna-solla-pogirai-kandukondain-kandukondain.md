---
title: "enna solla pogirai song lyrics"
album: "Kandukondain Kandukondain"
artist: "A. R. Rahman"
lyricist: "Vairamuthu"
director: "Rajiv Menon"
path: "/albums/kandukondain-kandukondain-lyrics"
song: "Enna Solla Pogirai"
image: ../../images/albumart/kandukondain-kandukondain.jpg
date: 2000-05-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/v3fZpt4R8jc"
type: "love"
singers:
  - Shankar Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Illai lllai Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai lllai Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Ganam Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Ganam Pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illai Endra Sollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Endra Sollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanguvathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanguvathendraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Innum Enakkor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Innum Enakkor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jenmam Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenmam Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Solla Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhana Thendralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhana Thendralai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jannalgal Thandiththal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannalgal Thandiththal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niyayamaa Niyayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyayamaa Niyayamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalin Kelvikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalin Kelvikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalin Bathil Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalin Bathil Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mounamaa Mounamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamaa Mounamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Enthan Kadhal Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Enthan Kadhal Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nodi Ondru Pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi Ondru Pothume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athai Naanum Meipikkathaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Naanum Meipikkathaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Aayul Vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Aayul Vendume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illai lllai Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai lllai Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Ganam Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Ganam Pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illai Endra Sollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Endra Sollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanguvathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanguvathendraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Innum Enakkor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Innum Enakkor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jenmam Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenmam Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Solla Pogirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Pogirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Solla Pogirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Pogirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhana Thendralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhana Thendralai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jannalgal Thandiththal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannalgal Thandiththal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niyayamaa Niyayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyayamaa Niyayamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalin Kelvikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalin Kelvikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalin Bathil Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalin Bathil Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mounamaa Mounamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamaa Mounamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Enthan Kadhal Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Enthan Kadhal Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nodi Ondru Pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi Ondru Pothume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athai Naanum Meipikkathaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Naanum Meipikkathaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Aayul Vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Aayul Vendume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illai lllai Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai lllai Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Ganam Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Ganam Pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illai Endra Sollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Endra Sollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanguvathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanguvathendraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Innum Enakkor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Innum Enakkor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jenmam Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenmam Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Solla Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Solla Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Oru Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Oru Kannaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unathu Bimbam Vizhunthathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathu Bimbam Vizhunthathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithuthaan Un Sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuthaan Un Sontham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Sonnathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Sonnathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Bimbam Katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Bimbam Katta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kayir Ondrum illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayir Ondrum illaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Oonjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Oonjal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bimbam Aaduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bimbam Aaduthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ondru Solladi Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ondru Solladi Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">illai Nindru Kolladi Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai Nindru Kolladi Kanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enthan Vaazhkaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Vaazhkaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unthan Vizhi Vilimbil Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Vizhi Vilimbil Ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuraththaathey Uyir Karaiyeraathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuraththaathey Uyir Karaiyeraathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illai lllai Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai lllai Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Ganam Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Ganam Pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illai Endra Sollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Endra Sollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanguvathendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanguvathendraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Innum Enakkor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Innum Enakkor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jenmam Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenmam Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Solla Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Solla Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhana Thendralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhana Thendralai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jannalgal Thandiththal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannalgal Thandiththal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niyayamaa Niyayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyayamaa Niyayamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalin Kelvikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalin Kelvikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalin Bathil Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalin Bathil Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mounamaa Mounamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamaa Mounamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyal Vantha Pinnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyal Vantha Pinnaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyaatha Iravu Yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyaatha Iravu Yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovaasam Veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaasam Veesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unthan Koonthaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Koonthaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivvulagam Irunda Pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvulagam Irunda Pinnum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulaatha Baagam Yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulaatha Baagam Yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathir Vanthu Paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathir Vanthu Paayum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unthan Kangaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Kangaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Uzhaga Azhagigal Koodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Uzhaga Azhagigal Koodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Paatham Kazhuvalaam Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paatham Kazhuvalaam Vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Thalirmalare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thalirmalare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Thayakkamenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Thayakkamenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Puriyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Puriyaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Vaazhvaa Saavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Vaazhvaa Saavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Solla Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Solla Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Solla Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Solla Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niyayamaa Niyayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyayamaa Niyayamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Solla Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Solla Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mounamaa Mounamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamaa Mounamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Solla Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Solla Pogiraai"/>
</div>
</pre>
