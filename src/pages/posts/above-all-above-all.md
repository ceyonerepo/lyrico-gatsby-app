---
title: "above all song lyrics"
album: "Above All"
artist: "Gur Sidhu"
lyricist: "Jassa Dhillon"
director: "Sukh Sanghera"
path: "/albums/above-all-lyrics"
song: "Above All"
image: ../../images/albumart/above-all.jpg
date: 2021-05-11
lang: punjabi
youtubeLink: "https://www.youtube.com/embed/ry0mGxg-Vco"
type: "Mass"
singers:
  - Jassa Dhillon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaun kehnda, kaun kehnda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda, kaun kehnda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun kehnda,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaun kehnda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaun kehnda jatt maahde ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda jatt maahde ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunde vair paaye maahde bas jattan de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunde vair paaye maahde bas jattan de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun kehnda,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho chulleyan gandase rehnde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho chulleyan gandase rehnde"/>
</div>
<div class="lyrico-lyrics-wrapper">Bulliyan te haase rehnde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulliyan te haase rehnde"/>
</div>
<div class="lyrico-lyrics-wrapper">Gairan vich khauf rehnda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gairan vich khauf rehnda"/>
</div>
<div class="lyrico-lyrics-wrapper">Khade vaili passe rehnde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khade vaili passe rehnde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho utre dunali vichon chobbran te chadh'de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho utre dunali vichon chobbran te chadh'de"/>
</div>
<div class="lyrico-lyrics-wrapper">Add'de ni gabbru taan jadaan vichon wad'de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Add'de ni gabbru taan jadaan vichon wad'de"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar te pyaar pichhe muhre hoke lad'de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar te pyaar pichhe muhre hoke lad'de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haarde na yaar sohniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haarde na yaar sohniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Raule mukkde na hunde kade vattan de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raule mukkde na hunde kade vattan de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaun kehnda,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun kehnda jatt maahde ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda jatt maahde ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunde vair paaye maahde bas jattan de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunde vair paaye maahde bas jattan de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun kehnda jatt maahde ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda jatt maahde ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunde vair paaye maahde bas jattan de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunde vair paaye maahde bas jattan de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun kehnda jatt maahde ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda jatt maahde ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan uchhiyan udariyan niviyan ferrari aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan uchhiyan udariyan niviyan ferrari aan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dass kehda shehar jithe malla nahiyon maariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dass kehda shehar jithe malla nahiyon maariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan uchhiyan udariyan niviyan ferrari aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan uchhiyan udariyan niviyan ferrari aan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dass kehda shehar jithe malla nahiyon maariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dass kehda shehar jithe malla nahiyon maariyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
24/7 <div class="lyrico-lyrics-wrapper">lor ae ni wakhri jehi taur ae ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lor ae ni wakhri jehi taur ae ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nave tere shehar ch puraneyan te zor ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nave tere shehar ch puraneyan te zor ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho bharde na takk balliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho bharde na takk balliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vajjde aa fire jadon pattan te
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vajjde aa fire jadon pattan te"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaun kehnda,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun kehnda jatt maahde ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda jatt maahde ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunde vair paaye maahde bas jattan de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunde vair paaye maahde bas jattan de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun kehnda jatt maahde ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda jatt maahde ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunde vair paaye maahde bas jattan de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunde vair paaye maahde bas jattan de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun kehnda jatt maahde ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda jatt maahde ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tattiyan taseeran ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tattiyan taseeran ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichhe pichhe heeran ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichhe pichhe heeran ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Saade utte hath kude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saade utte hath kude"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkeyan fakeeran de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkeyan fakeeran de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pale aan toofan'an vich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pale aan toofan'an vich"/>
</div>
<div class="lyrico-lyrics-wrapper">Jammeya bayana vich
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jammeya bayana vich"/>
</div>
<div class="lyrico-lyrics-wrapper">Tikha naale tez kude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tikha naale tez kude"/>
</div>
<div class="lyrico-lyrics-wrapper">Jivein shamheeran ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jivein shamheeran ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho baneya wajood saada todeya ni jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho baneya wajood saada todeya ni jaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Hikkan pichhe lad'de aan modeya ni jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hikkan pichhe lad'de aan modeya ni jaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Saak sada saure kehda jaanda ni dhillon nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saak sada saure kehda jaanda ni dhillon nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Phat sidhu jatt da marodeya ni jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phat sidhu jatt da marodeya ni jaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho pehlan jehde saun vajjde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho pehlan jehde saun vajjde"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichhon jhalde na bhaar phir lattan te
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichhon jhalde na bhaar phir lattan te"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho kaun kehnda jatt maahde ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kaun kehnda jatt maahde ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunde vair paaye maahde bas jattan de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunde vair paaye maahde bas jattan de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun kehnda jatt maahde ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda jatt maahde ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunde vair paaye maahde bas jattan de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunde vair paaye maahde bas jattan de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun kehnda jatt maahde ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda jatt maahde ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho dass kehda ae wangar da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho dass kehda ae wangar da"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunne jatt thaal da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunne jatt thaal da"/>
</div>
<div class="lyrico-lyrics-wrapper">Mausam de hisab naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mausam de hisab naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladayi munda bhaal da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladayi munda bhaal da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mare na zameer kyun ki ziddi jatt jyonde ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mare na zameer kyun ki ziddi jatt jyonde ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ginti ch nahiyon heer gaddran ch aunde ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ginti ch nahiyon heer gaddran ch aunde ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho care vi bandook aivein theda takk na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho care vi bandook aivein theda takk na"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamm hustle aa aundi jithe aunda luck na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamm hustle aa aundi jithe aunda luck na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho jadon jadon jatt jud de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jadon jadon jatt jud de"/>
</div>
<div class="lyrico-lyrics-wrapper">Hisaab lagde na bottle'an de dattan de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hisaab lagde na bottle'an de dattan de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaun kehnda jatt maahde ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda jatt maahde ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunde vair paaye maahde bas jattan de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunde vair paaye maahde bas jattan de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun kehnda jatt maahde ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda jatt maahde ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunde vair paaye maahde bas jattan de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunde vair paaye maahde bas jattan de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun kehnda jatt maahde ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda jatt maahde ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaun kehnda, kaun kehnda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda, kaun kehnda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun kehnda jatt maahde ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun kehnda jatt maahde ae"/>
</div>
</pre>
