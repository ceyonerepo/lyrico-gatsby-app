---
title: "kadukumanikkoru kannundu song lyrics"
album: "Kappela"
artist: "Sushin Shyam"
lyricist: "Vishnu Shobhana"
director: "Muhammad Musthafa"
path: "/albums/kappela-lyrics"
song: "Kadukumanikkoru Kannundu"
image: ../../images/albumart/kappela.jpg
date: 2020-03-06
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/_r_99_KeERE"
type: "happy"
singers:
  - Sithara Krishnakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadukumanikkoru Kannundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadukumanikkoru Kannundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanninakathoru Karadundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanninakathoru Karadundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadukumanikkoru Kannundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadukumanikkoru Kannundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanninakathoru Karadundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanninakathoru Karadundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalundeennoru Changaayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalundeennoru Changaayee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaya Varuthu Kazhinjittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaya Varuthu Kazhinjittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudamoru Kallu Kudichittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudamoru Kallu Kudichittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karimeen Mullu Kudichittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karimeen Mullu Kudichittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaham Parayan Kavalayilinnoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaham Parayan Kavalayilinnoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannada Vaangi Varunnundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannada Vaangi Varunnundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallippaala Punchiri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallippaala Punchiri "/>
</div>
<div class="lyrico-lyrics-wrapper">Poloru Kanmashiyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poloru Kanmashiyaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaanaay Koombiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaanaay Koombiya "/>
</div>
<div class="lyrico-lyrics-wrapper">Konthala Mundel Murukiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konthala Mundel Murukiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalkkanda Kadha Kelkkaanaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalkkanda Kadha Kelkkaanaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandal Cheril Kaalukal Chithari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandal Cheril Kaalukal Chithari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Kaayalthirakalumaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kaayalthirakalumaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalichaaya Kadavukal Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalichaaya Kadavukal Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondu Pidichu Varunnundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu Pidichu Varunnundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhayude Kadamoru Kachaankaattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhayude Kadamoru Kachaankaattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudayude Keezhil Karuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudayude Keezhil Karuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Parayunnavalude Karalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parayunnavalude Karalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Karivalayaadana Kadalodiyunnundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karivalayaadana Kadalodiyunnundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavilil Kavaruthirunnundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavilil Kavaruthirunnundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavilil Kavaruthirunnundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavilil Kavaruthirunnundu"/>
</div>
</pre>
