---
title: "anbin vazhi song lyrics"
album: "Nenjamundu Nermaiyundu Odu Raja"
artist: "Shabir"
lyricist: "Priyan"
director: "Karthik Venugopalan"
path: "/albums/nenjamundu-nermaiyundu-odu-raja-lyrics"
song: "Anbin Vazhi"
image: ../../images/albumart/nenjamundu-nermaiyundu-odu-raja.jpg
date: 2019-06-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mfrjsvkVnWw"
type: "love"
singers:
  - Shabir
  - Sharanya Srinivas
  - Aishwarya
  - Sahana Niren Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anbin Vazhi Adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin Vazhi Adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirnilai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirnilai Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbirkillai Adaikun Thaazhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbirkillai Adaikun Thaazhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Hohohoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Hohohoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Hohohoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Hohohoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoho Ho Hoho Ho Ho Ho Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoho Ho Hoho Ho Ho Ho Ho Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbin Vazhi Adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin Vazhi Adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirnilai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirnilai Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbirkillai Adaikun Thaazhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbirkillai Adaikun Thaazhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaadhum Vasapadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadhum Vasapadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naazhigai Koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naazhigai Koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavarum Thunai Ena Maarida Nerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavarum Thunai Ena Maarida Nerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodidum Vaazhvidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodidum Vaazhvidhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaal Unarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaal Unarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadidum Nilaiyilum Pookkal Sirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadidum Nilaiyilum Pookkal Sirikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbin Vazhi Adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin Vazhi Adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirnilai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirnilai Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbirkillai Adaikun Thaazhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbirkillai Adaikun Thaazhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Hohohoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Hohohoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Hohohoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Hohohoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoho Ho Hoho Ho Ho Ho Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoho Ho Hoho Ho Ho Ho Ho Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanakkenna Thanakkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakkenna Thanakkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithamum Oodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithamum Oodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Suyanala Vaazhvu Mothamum Baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suyanala Vaazhvu Mothamum Baaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduthavan Sirippil Iraivanai Kaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduthavan Sirippil Iraivanai Kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathin Anbil Theivam Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathin Anbil Theivam Vaazhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayathin Anbil Theivam Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathin Anbil Theivam Vaazhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pillaiyin Azhukural
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaiyin Azhukural"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettadhum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettadhum Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayin Maarbadhu Thaanaai Oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayin Maarbadhu Thaanaai Oorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saga Oru Manithanin Thuyaram Pookkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saga Oru Manithanin Thuyaram Pookkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaal Ulagam Azhagaai Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaal Ulagam Azhagaai Maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Hohohoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Hohohoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Hoo Hohohoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Hoo Hohohoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoho Ho Hoho Ho Ho Ho Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoho Ho Hoho Ho Ho Ho Ho Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yele Yeelee Ye Le Ye Le Ye Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yele Yeelee Ye Le Ye Le Ye Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Yele Yeelee Ye Le Ye Le Ye Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yele Yeelee Ye Le Ye Le Ye Le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbin Vazhi Adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin Vazhi Adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirnilai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirnilai Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirnilai Thaanae Uyirnilai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirnilai Thaanae Uyirnilai Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbirkillai Adaikun Thaazhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbirkillai Adaikun Thaazhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaadhum Vasapadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadhum Vasapadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naazhigai Koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naazhigai Koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavarum Thunai Ena Maarida Nerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavarum Thunai Ena Maarida Nerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodidum Vaazhvidhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodidum Vaazhvidhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaal Unarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaal Unarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadidum Nilaiyilum Pookkal Sirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadidum Nilaiyilum Pookkal Sirikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">s  Anbin Vazhi Adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="s  Anbin Vazhi Adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirnilai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirnilai Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbirkillai Adaikun Thaazhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbirkillai Adaikun Thaazhae"/>
</div>
</pre>
