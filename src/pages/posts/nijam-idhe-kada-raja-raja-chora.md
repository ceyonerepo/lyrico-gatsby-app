---
title: "nijam idhe kada song lyrics"
album: "Raja Raja Chora"
artist: "Vivek Sagar"
lyricist: "Krishna Kanth"
director: "Hasith Goli"
path: "/albums/raja-raja-chora-lyrics"
song: "Nijam Idhe Kada"
image: ../../images/albumart/raja-raja-chora.jpg
date: 2021-08-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/DO_BkGFoTF8"
type: "happy"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sannelaudu leni paavuraniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannelaudu leni paavuraniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Needa dhorikenu ivaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needa dhorikenu ivaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhanunna eda leni sontha rekkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhanunna eda leni sontha rekkale"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayam ivvanunna savaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam ivvanunna savaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nunnagunna dooraley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nunnagunna dooraley"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhalinche theeraley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhalinche theeraley"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam idhe kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam idhe kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaley veedi padha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaley veedi padha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagalatula naligipodhuva migilipodhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagalatula naligipodhuva migilipodhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalu maatavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalu maatavai"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalu meeruvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalu meeruvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maralarayuva maralarayuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maralarayuva maralarayuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee prakshlana sweeyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee prakshlana sweeyame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee lokanike sevaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee lokanike sevaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna idhi marchipothe manalevule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna idhi marchipothe manalevule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadi yedarilo nadiche daarilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadi yedarilo nadiche daarilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinuku lalana chinuku vaanalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinuku lalana chinuku vaanalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Malupu veeduthu… malupu koruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malupu veeduthu… malupu koruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka prayanama idhi pryanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka prayanama idhi pryanama"/>
</div>
<div class="lyrico-lyrics-wrapper">Telisinappude nappude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telisinappude nappude"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi chikkule idhi dhichikkule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi chikkule idhi dhichikkule"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhili rekkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhili rekkale"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka thapassudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka thapassudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraptune chali cheyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraptune chali cheyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Anumnaname ithagayedha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anumnaname ithagayedha"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukatame vadhilesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukatame vadhilesina"/>
</div>
<div class="lyrico-lyrics-wrapper">Abimaname dari cherune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abimaname dari cherune"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika cheekate teliveyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika cheekate teliveyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Marumaruna toli vekuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumaruna toli vekuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharimera thudi cheekate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharimera thudi cheekate"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayinasare poodhotane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayinasare poodhotane"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharimeyara thudi cheekate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharimeyara thudi cheekate"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina sare nuvu lokuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina sare nuvu lokuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirugalike chera ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirugalike chera ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Chera cherina padiponule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chera cherina padiponule"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirugalike chera ledhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirugalike chera ledhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Chera cherina padiponule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chera cherina padiponule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam idhe kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam idhe kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kale veedi pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kale veedi pada"/>
</div>
</pre>
