---
title: "i want to marry you mama song lyrics"
album: "charlie chaplin 2"
artist: "amrish"
lyricist: "Yugabharathi"
director: "Sakthi chithambaram"
path: "/albums/charlie-chaplin-2-song-lyrics"
song: "i want to marry you mama"
image: ../../images/albumart/charlie-chaplin-2.jpg
date: 2019-01-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mSppJuBZaUU"
type: "love"
singers:
  - Jagadeesh Kumar
  - Bhargavi
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kandathum Kadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandathum Kadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattathe Bavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattathe Bavala"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kuda Jodi Sehra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kuda Jodi Sehra"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthen Aavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthen Aavala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kokkarakko Sevala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kokkarakko Sevala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhikki Nee Kaavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhikki Nee Kaavala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammoda Kuttani Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda Kuttani Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadum Mersala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadum Mersala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Want To Marry You Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Marry You Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Marry You Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Marry You Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Marry You Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Marry You Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Marry You Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Marry You Mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandathum Kadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandathum Kadhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattathe Bavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattathe Bavala"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kuda Jodi Sehra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kuda Jodi Sehra"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthen Aavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthen Aavala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cinema Theatherukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cinema Theatherukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Munnamme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Munnamme"/>
</div>
<div class="lyrico-lyrics-wrapper">Teaser Veliyavathilaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teaser Veliyavathilaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Marriage Nadakkum Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marriage Nadakkum Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Honeymoon-U Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Honeymoon-U Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Polama Jolly-Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polama Jolly-Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Edakku Madakku Pesi Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Edakku Madakku Pesi Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhukka Paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukka Paakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mudichi Onnu Podum Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mudichi Onnu Podum Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Avukka Paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avukka Paakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kada Kadanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kada Kadanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai Mehla Oodura Vandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Mehla Oodura Vandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kelambidama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kelambidama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukku Innum Adikkira Nondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku Innum Adikkira Nondi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yooo…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yooo…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Want To Marry You Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Marry You Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Marry You Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Marry You Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Marry You Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Marry You Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">I Want To Marry You Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Want To Marry You Mama"/>
</div>
</div>
</pre>
