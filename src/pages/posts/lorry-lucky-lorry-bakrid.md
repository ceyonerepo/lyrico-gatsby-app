---
title: "lorry lucky lorry song lyrics"
album: "Bakrid"
artist: "D. Imman"
lyricist: "Gnanakaravel"
director: "Jagadeesan Subu"
path: "/albums/bakrid-lyrics"
song: "Lorry Lucky Lorry"
image: ../../images/albumart/bakrid.jpg
date: 2019-08-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TN03Qi_pBp8"
type: "happy"
singers:
  - Ashwin Sharma
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Lorry Lucky Lorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lorry Lucky Lorry"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Laddu Savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Laddu Savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Lorry Lucky Lorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lorry Lucky Lorry"/>
</div>
<div class="lyrico-lyrics-wrapper">Paayum Rocketumaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayum Rocketumaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottagathoda Roobathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottagathoda Roobathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamy Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamy Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettila Dhuttu Tharapodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettila Dhuttu Tharapodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondaattiyoda Thollayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondaattiyoda Thollayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththam Nondhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam Nondhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Pattapaadu Theerappodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pattapaadu Theerappodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakkaram Suzhalum Paadhayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkaram Suzhalum Paadhayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sukkira Thisayaa Maarayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukkira Thisayaa Maarayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkaraa Oodura Lorry Ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaraa Oodura Lorry Ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru Pannuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru Pannuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottagathoda Loottiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottagathoda Loottiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatikkitten Maatikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikkitten Maatikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaaga Seththukkulla Meenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaaga Seththukkulla Meenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatikkitten Maatikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikkitten Maatikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaaga Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaaga Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatikkitten Maatikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikkitten Maatikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaaga Thoondil Mullil Meenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaaga Thoondil Mullil Meenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatikkitten Maatikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikkitten Maatikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaaga Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaaga Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lorry Lucky Lorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lorry Lucky Lorry"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Laddu Savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Laddu Savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Lorry Lucky Lorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lorry Lucky Lorry"/>
</div>
<div class="lyrico-lyrics-wrapper">Paayum Rocketumaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayum Rocketumaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oodhi Oodhi Aasaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodhi Oodhi Aasaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Perukkivechcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perukkivechcha"/>
</div>
<div class="lyrico-lyrics-wrapper">Baloonu Onnu Toppunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baloonu Onnu Toppunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odainjichae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odainjichae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaavi Thaavi Kudhikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavi Thaavi Kudhikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Mosalukutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosalukutty"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Ippa Kuppura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Ippa Kuppura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavundhuchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavundhuchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imsaiya Vandiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imsaiya Vandiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikkaasa Karaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikkaasa Karaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal Uppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Uppaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thindaadi Thenarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaadi Thenarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pozhappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pozhappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellaatta Muzhunguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaatta Muzhunguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Malappambaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malappambaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasu Vaada Kanna Mooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu Vaada Kanna Mooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Boni Pannum Aasayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boni Pannum Aasayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaniyaachae Lorriyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaniyaachae Lorriyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatikkitten Maatikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikkitten Maatikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaaga Seththukkulla Meenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaaga Seththukkulla Meenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatikkitten Maatikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikkitten Maatikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaaga Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaaga Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatikkitten Maatikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikkitten Maatikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaaga Thoondil Mullil Meenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaaga Thoondil Mullil Meenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatikkitten Maatikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikkitten Maatikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaaga Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaaga Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lorry Lucky Lorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lorry Lucky Lorry"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Laddu Savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Laddu Savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Lorry Lucky Lorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lorry Lucky Lorry"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagarum Aama Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarum Aama Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottagathoda Roobathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottagathoda Roobathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Boodham Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boodham Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nimmadhiya Thinnu Pochae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nimmadhiya Thinnu Pochae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondaattiyoda Nacharippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondaattiyoda Nacharippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnae Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnae Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnadi Aappu Vechu Pochae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnadi Aappu Vechu Pochae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakkaram Suzhalum Paadhayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkaram Suzhalum Paadhayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhara Saniyan Paarkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhara Saniyan Paarkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkaraa Oodura Lorry Ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaraa Oodura Lorry Ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkaru Pannuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkaru Pannuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottagathoda Loottiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottagathoda Loottiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatikkitten Maatikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikkitten Maatikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaaga Seththukkulla Meenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaaga Seththukkulla Meenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatikkitten Maatikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikkitten Maatikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaaga Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaaga Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatikkitten Maatikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikkitten Maatikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaaga Thoondil Mullil Meenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaaga Thoondil Mullil Meenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatikkitten Maatikkitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikkitten Maatikkitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaaga Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaaga Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lorry Lucky Lorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lorry Lucky Lorry"/>
</div>
</pre>
