---
title: "kattaduraikku song lyrics"
album: "Sangili Bungili Kadhava Thorae"
artist: "Vishal Chandrasekhar"
lyricist: "Arunraj Kamaraj"
director: "Ike"
path: "/albums/sangili-bungili-kadhava-thorae-lyrics"
song: "Kattaduraikku"
image: ../../images/albumart/sangili-bungili-kadhava-thorae.jpg
date: 2017-05-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qyrGM5Zzp3k"
type: "happy"
singers:
  -	Gangai Amaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kattaduraikku inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaduraikku inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattam sariyilla da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam sariyilla da"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaba riba riba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaba riba riba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichu veratta pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu veratta pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram vanthurucchu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram vanthurucchu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaba riba riba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaba riba riba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha kota thaandi inimae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha kota thaandi inimae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum ulla varamudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum ulla varamudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha gate-ah thandi veliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha gate-ah thandi veliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum neram vanthuruchu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum neram vanthuruchu thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma munnadi nikka alladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma munnadi nikka alladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu thathingina thakita thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu thathingina thakita thom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattaduraikku inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaduraikku inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattam sariyilla da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam sariyilla da"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaba riba riba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaba riba riba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichu veratta pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu veratta pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram vanthurucchu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram vanthurucchu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaba riba riba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaba riba riba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadu jaamam paathuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu jaamam paathuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pei kaatthu veesumam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pei kaatthu veesumam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagajothi aagattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagajothi aagattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arnold-a koodathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arnold-a koodathan"/>
</div>
<div class="lyrico-lyrics-wrapper">Attack-ku pannumam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attack-ku pannumam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu nalla seasonuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu nalla seasonuthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna thoranthu mannai adhil thoovi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna thoranthu mannai adhil thoovi"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam allu kaatidumae aavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam allu kaatidumae aavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha correcta use panni maapi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha correcta use panni maapi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum besha kalakitada kaapi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum besha kalakitada kaapi"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam sollama kollama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam sollama kollama "/>
</div>
<div class="lyrico-lyrics-wrapper">ellarum odaththan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellarum odaththan"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaba riba shaba riba thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaba riba shaba riba thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey kattaduraikku inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kattaduraikku inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattam sariyilla da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam sariyilla da"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaba riba ribaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaba riba ribaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichu veratta pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu veratta pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram vanthurucchu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram vanthurucchu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaba riba riba aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaba riba riba aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey… ennoda ellaikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey… ennoda ellaikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevan scene-eh poduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevan scene-eh poduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanukku vecchupomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanukku vecchupomada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellamae mela irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamae mela irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan paathu pannuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan paathu pannuvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kitta viduvomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kitta viduvomada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga ulla nulainjavanga gathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga ulla nulainjavanga gathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu thaana senjukita sathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu thaana senjukita sathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha bakthi kavasam kojam padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha bakthi kavasam kojam padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum kaaka varalena idi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum kaaka varalena idi"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu dingu dingu digaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu dingu dingu digaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dangu dangu dagaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dangu dangu dagaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaba riba shaba riba thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaba riba shaba riba thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey kattaduraikku inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey kattaduraikku inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattam sariyilla da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam sariyilla da"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaba riba ribaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaba riba ribaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichu veratta pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu veratta pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram vanthurucchu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram vanthurucchu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaba riba riba aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaba riba riba aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha kota thaandi inimae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha kota thaandi inimae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum ulla varamudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum ulla varamudiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha gate-ah thandi veliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha gate-ah thandi veliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum neram vanthuruchu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum neram vanthuruchu thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma munnadi nikka alladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma munnadi nikka alladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu thathingina thakita thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu thathingina thakita thom"/>
</div>
</pre>
