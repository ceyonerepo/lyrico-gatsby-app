---
title: 'kadhaipoma kadhaipoma song lyrics'
album: 'Oh My Kadavule'
artist: 'Leon James'
lyricist: 'Ko Sesha'
director: 'Ashwath Marimuthu'
path: '/albums/oh-my-kadavule-song-lyrics'
song: 'Kadhaipoma Kadhaipoma'
image: ../../images/albumart/oh-my-kadavule.jpg
date: 2020-02-14
lang: tamil
singers: 
- Sid Sriram
youtubeLink: 'https://www.youtube.com/embed/DScFlfN9vDk'
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Dhirana dhiranam dhirana
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhirana dhiranam dhirana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhirana dhiranam dhirana
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhirana dhiranam dhirana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam thananam thananam thana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thananam thananam thananam thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharinina thaa naaa naa (2  Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharinina thaa naaa naa"/></div>
</div>
<div class="lyrico-lyrics-wrapper">Thananam thananam thananam thana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thananam thananam thananam thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharinina thaa naaa naa
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharinina thaa naaa naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hmm mmm mm mm mm
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm mmm mm mm mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Naetru naan unnai paartha paarvai veru
<input type="checkbox" class="lyrico-select-lyric-line" value="Naetru naan unnai paartha paarvai veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Neengaadha ennam aaga aanaai indru
<input type="checkbox" class="lyrico-select-lyric-line" value="Neengaadha ennam aaga aanaai indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu naanum pona dhooram
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnodu naanum pona dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum nenjilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaavum nenjilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Reengaara ninaivugalaaga alaiyai
<input type="checkbox" class="lyrico-select-lyric-line" value="Reengaara ninaivugalaaga alaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae minjudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ingae minjudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Noolarundha pattam polae
<input type="checkbox" class="lyrico-select-lyric-line" value="Noolarundha pattam polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai sutri naanum aada
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai sutri naanum aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal neeti neeyum pidikka kaathirukkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaigal neeti neeyum pidikka kaathirukkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idharkellaam arthanagal enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Idharkellaam arthanagal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekka vendum unnai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kekka vendum unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam kai koodinal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam kai koodinal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadhaippomaa kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa kadhaippomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga neeyum naanum dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ondraaga neeyum naanum dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaippomaa kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa kadhaippomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pesa pesa kaayam aarumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee pesa pesa kaayam aarumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adhikaalai vandhaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhikaalai vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaai en vaanil nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagaai en vaanil nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaiyaadha suriyan aagiraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Anaiyaadha suriyan aagiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedu neram kaaindhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nedu neram kaaindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadha kadhappu thandhavudan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadha kadhappu thandhavudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavaai urumaari nirkkiraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Nilavaai urumaari nirkkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unnai indru paarthadhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai indru paarthadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai naanae ketkkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai naanae ketkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vairam ondrai kaiyil vaithu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vairam ondrai kaiyil vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae thaedi alainthaaiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Engae thaedi alainthaaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unmai endru therinthumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unmai endru therinthumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam solla thayangudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjam solla thayangudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal korthu pesinaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaigal korthu pesinaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhairiyangal thondrumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhairiyangal thondrumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadhaippomaa kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa kadhaippomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaippomaa   Kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa   Kadhaippomaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ondraaga neeyum naanum dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ondraaga neeyum naanum dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaippomaa   Kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa   Kadhaippomaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Kadhaippomaa   Kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa   Kadhaippomaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pesa pesa kaayam aarumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee pesa pesa kaayam aarumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadhaippomaa   Kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa   Kadhaippomaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Kadhaippomaa   Kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa   Kadhaippomaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhirana dhiranam dhirana
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhirana dhiranam dhirana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhirana dhiranam dhirana
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhirana dhiranam dhirana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananam thananam thananam thana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thananam thananam thananam thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharinina thaa naaa naa (2  Times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharinina thaa naaa naa"/></div>
</div>
<div class="lyrico-lyrics-wrapper">Thananam thananam thananam thana
<input type="checkbox" class="lyrico-select-lyric-line" value="Thananam thananam thananam thana"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharinina thaa naaa naa
<input type="checkbox" class="lyrico-select-lyric-line" value="Tharinina thaa naaa naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kadhaippomaa kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa kadhaippomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga neeyum naanum dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ondraaga neeyum naanum dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaippomaa kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa kadhaippomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaippomaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaippomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pesa pesa kaayam aarumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee pesa pesa kaayam aarumae"/>
</div>
</pre>