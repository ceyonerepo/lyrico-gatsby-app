---
title: "saarattu vandiyila song lyrics"
album: "Kaatru Veliyidai"
artist: "A R Rahman"
lyricist: "Vairamuthu"
director: "Mani Ratnam"
path: "/albums/kaatru-veliyidai-lyrics"
song: "Saarattu Vandiyila"
image: ../../images/albumart/kaatru-veliyidai.jpg
date: 2017-04-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Bw9b1LSxg-I"
type: "love"
singers:
  -	A R Reihana
  - Nikhita Gandhi
  - Tippu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Saarattu vandiyila seerattoliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarattu vandiyila seerattoliyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Orantherinjathu unmugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orantherinjathu unmugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam killum antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam killum antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla chirippila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla chirippila"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella chivandhadhu enmugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella chivandhadhu enmugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saarattu vandiyila seerattoliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saarattu vandiyila seerattoliyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Orantherinjathu unmugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orantherinjathu unmugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam killum antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam killum antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla chirippila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla chirippila"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella chivandhadhu enmugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella chivandhadhu enmugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi vethala potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vethala potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhatta enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhatta enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathiram pannikkudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathiram pannikkudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kudutha kadana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kudutha kadana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppi kudukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppi kudukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyam pannikkudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyam pannikkudu"/>
</div>
<div class="lyrico-lyrics-wrapper">En ratham soodukkolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ratham soodukkolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathu nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaan raasaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaan raasaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanukko pathu nimisham -ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanukko pathu nimisham -ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnuko anju nimisham -ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnuko anju nimisham -ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhuvaa sandi thanam pannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhuvaa sandi thanam pannum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aambalaya ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aambalaya ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindi kezhangeduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindi kezhangeduppaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selaikku saayam pogu mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selaikku saayam pogu mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna naan velukka venumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna naan velukka venumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadu pattu vidiyum pozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadu pattu vidiyum pozhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyil solla poigal venumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyil solla poigal venumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuthaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuthaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizh naattu baanii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizh naattu baanii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saratu vandiyila seerattoliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saratu vandiyila seerattoliyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Orantherinjathu unmugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orantherinjathu unmugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam killum antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam killum antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla chirippila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla chirippila"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella chivandhadhu enmugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella chivandhadhu enmugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saratu vandiyila seerattoliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saratu vandiyila seerattoliyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Orantherinjathu unmugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orantherinjathu unmugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam killum antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam killum antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla chirippila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla chirippila"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella chivandhadhu enmugam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella chivandhadhu enmugam "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkaththayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkaththayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhachchu kozhachchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhachchu kozhachchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kungumam poosikkodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kungumam poosikkodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai ulla vervaiya pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai ulla vervaiya pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam edhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam edhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae poongodiVandhu thenkudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae poongodiVandhu thenkudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kaigalil udaiyattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kaigalil udaiyattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanni kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni kannadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathaazhang gaattukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaazhang gaattukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Maththaalam kekkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththaalam kekkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sithaanai rendukkum kondaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithaanai rendukkum kondaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththaala chaaralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththaala chaaralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththana panneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththana panneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithaara kallikku thallaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithaara kallikku thallaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan manmadha kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan manmadha kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanam eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarbil appikittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbil appikittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva kurangu kazhuththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva kurangu kazhuththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttiya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttiya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholil ottikittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholil ottikittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini buthi kalangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini buthi kalangura"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam koduthidu raasaavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam koduthidu raasaavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnu dhaan rathina katti – ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu dhaan rathina katti – ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maappilla vethala potti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maappilla vethala potti"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduthu rathina kattiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu rathina kattiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethala pottiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethala pottiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooda chollungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooda chollungadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Modhalil maala maathungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalil maala maathungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Piraghu paala maathungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piraghu paala maathungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattil vittu kaalaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattil vittu kaalaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasangi vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasangi vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Selai maathungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selai maathungadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maharaani adhuthaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maharaani adhuthaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizh naattu baani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizh naattu baani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathaazhang gaattukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaazhang gaattukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Maththaalam kekkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththaalam kekkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sithaanai rendukkum kondaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithaanai rendukkum kondaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththaala chaaralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththaala chaaralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththana panneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththana panneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithaara kallikku thallaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithaara kallikku thallaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathaazhang gaattukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaazhang gaattukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Maththaalam kekkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththaalam kekkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sithaanai rendukkum kondaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithaanai rendukkum kondaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththaala chaaralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththaala chaaralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththana panneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththana panneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithaara kallikku thallaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithaara kallikku thallaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathaazhang gaattukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaazhang gaattukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Maththaalam kekkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththaalam kekkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sithaanai rendukkum kondaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithaanai rendukkum kondaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththaala chaaralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththaala chaaralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththana panneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththana panneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithaara kallikku thallaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithaara kallikku thallaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathaazhang gaattukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaazhang gaattukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Maththaalam kekkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththaalam kekkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sithaanai rendukkum kondaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithaanai rendukkum kondaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththaala chaaralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththaala chaaralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththana panneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththana panneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithaara kallikku thallaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithaara kallikku thallaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuthaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuthaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizh naattu baanii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizh naattu baanii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuthaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuthaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizh naattu baanii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizh naattu baanii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuthaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuthaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizh naattu baanii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizh naattu baanii"/>
</div>
</pre>
