---
title: "itemkaaran song lyrics"
album: "Semma Botha Aaguthey"
artist: "Yuvan Shankar Raja"
lyricist: "Rokesh"
director: "Badri Venkatesh"
path: "/albums/semma-botha-aaguthey-lyrics"
song: "Itemkaaran"
image: ../../images/albumart/semma-botha-aaguthey.jpg
date: 2018-06-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RVc-0v2efzc"
type: "happy"
singers:
  - Ranjith
  - Anitha Karthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaaju Eduthunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaju Eduthunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadiyila Suthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadiyila Suthuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Vaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Vaangala"/>
</div>
<div class="lyrico-lyrics-wrapper">Avarukoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avarukoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Figuru Thattuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figuru Thattuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Attu Payyan Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attu Payyan Ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Bongala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Bongala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Vachukinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Vachukinu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innona Paakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innona Paakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizha Payyanukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizha Payyanukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhu Santhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhu Santhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Setupu Kekkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Setupu Kekkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellarukkum Ellame Irukudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarukkum Ellame Irukudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Itemkaran Single Ah Thavikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itemkaran Single Ah Thavikkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mingle Aavondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mingle Aavondaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bippa Bilippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bippa Bilippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Powerfullu Bulb Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Powerfullu Bulb Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bihulathaan Thookura Paatthelluppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bihulathaan Thookura Paatthelluppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yappa Dappa Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yappa Dappa Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sownmittai Siluppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sownmittai Siluppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapava Paaruda Palapalappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapava Paaruda Palapalappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Thottathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Thottathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerum Un Doubtuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerum Un Doubtuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thottaaka Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thottaaka Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kick Outduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Outduda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakalappathan Sirippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakalappathan Sirippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Palapala Palappa Ada Iruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palapala Palappa Ada Iruppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kosamthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kosamthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Item Kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Item Kaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Item Kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Item Kaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Item Kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Item Kaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Item Kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Item Kaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala Thalathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Thalathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vida Vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vida Vida"/>
</div>
<div class="lyrico-lyrics-wrapper">Super Figureu Yethuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super Figureu Yethuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Pala Pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Pala Pala"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonji Minukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonji Minukkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Makeup Pothuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Makeup Pothuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selfie Eduthunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selfie Eduthunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siluppinnu Nadakkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siluppinnu Nadakkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalanovanaalum Unna Yosikarom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalanovanaalum Unna Yosikarom"/>
</div>
<div class="lyrico-lyrics-wrapper">Amirthanjanaa Than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amirthanjanaa Than"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam Thechukurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam Thechukurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kickoutu Antha Painu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kickoutu Antha Painu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vitta Pala Queenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vitta Pala Queenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pona Podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pona Podi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Item Kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Item Kaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Item Kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Item Kaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Item Kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Item Kaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Item Kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Item Kaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Item Kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Item Kaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Item Kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Item Kaaran"/>
</div>
</pre>
