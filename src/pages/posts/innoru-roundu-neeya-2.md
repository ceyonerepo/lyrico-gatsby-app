---
title: "innoru roundu song lyrics"
album: "Neeya 2"
artist: "Shabir"
lyricist: "Ku. Karthik"
director: "L. Suresh"
path: "/albums/neeya-2-lyrics"
song: "Innoru Roundu"
image: ../../images/albumart/neeya-2.jpg
date: 2019-05-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0utS03qBD7w"
type: "happy"
singers:
  - Mukesh
  - Shabir
  - DJ Sathyia
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Podu…quarter-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu…quarter-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Cutting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cutting"/>
</div>
<div class="lyrico-lyrics-wrapper">Half-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Half-na"/>
</div>
<div class="lyrico-lyrics-wrapper">sitting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sitting"/>
</div>
<div class="lyrico-lyrics-wrapper">Full-u-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full-u-na"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharing"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothi kudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothi kudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Brandy-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brandy-na"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Whiskey-na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whiskey-na"/>
</div>
<div class="lyrico-lyrics-wrapper">Soda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soda"/>
</div>
<div class="lyrico-lyrics-wrapper">Rummuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rummuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Cola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cola"/>
</div>
<div class="lyrico-lyrics-wrapper">No more worry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No more worry"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha sarakka pathathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha sarakka pathathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjo kondaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjo kondaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada sarakku marammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada sarakku marammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambu thindaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambu thindaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey yethikkada liquoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey yethikkada liquoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oothikinna takkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oothikinna takkaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innoru roundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru roundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothu innoru roundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothu innoru roundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oothi kudi oothi kudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothi kudi oothi kudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothi kudi sharp-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothi kudi sharp-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaipatta ethikka nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaipatta ethikka nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Podatha daa thaapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podatha daa thaapa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thattungadaa glassu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattungadaa glassu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa kottungadaa cheersu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa kottungadaa cheersu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummy pieceu kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummy pieceu kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo kaaturaanda massu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo kaaturaanda massu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oothi kudi oothi kudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothi kudi oothi kudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothi kudi sharp-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothi kudi sharp-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaipatta ethikka nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaipatta ethikka nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Podatha daa thaapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podatha daa thaapa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thattungadaa glassu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattungadaa glassu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa kottungadaa cheersu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa kottungadaa cheersu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dummy pieceu kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dummy pieceu kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo kaaturaanda massu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo kaaturaanda massu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodhai yeri pona manushan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai yeri pona manushan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyathaan pesuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyathaan pesuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bar-u-kulla saathi maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bar-u-kulla saathi maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Samarasama sirikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samarasama sirikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththu mani aagumItha pirinji poga nogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu mani aagumItha pirinji poga nogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey yethikkada liquoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey yethikkada liquoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oothikinna takkaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oothikinna takkaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innoru roundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru roundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothu innoru roundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothu innoru roundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarakirukkuthu adichikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakirukkuthu adichikoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Murukkirukkuthu kadichikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murukkirukkuthu kadichikko"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakirukkuthu adichikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakirukkuthu adichikoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Murukkirukkuthu kadichikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murukkirukkuthu kadichikko"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan haan nalla adichikkoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan haan nalla adichikkoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye ye piothum kadichikkoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye ye piothum kadichikkoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondham bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondham bandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam dupe-u machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam dupe-u machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ootha ootha sorgam vanthurichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ootha ootha sorgam vanthurichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valikkum thunbam ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikkum thunbam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi pogumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi pogumdaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada sogatha otta vittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada sogatha otta vittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai nogun daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai nogun daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarakku santhaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakku santhaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosam thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosam thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee erangu thanniyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee erangu thanniyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothadalaam daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothadalaam daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodhaiyula paadurenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhaiyula paadurenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelu chittappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelu chittappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudichavanlaan chinna pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudichavanlaan chinna pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru chittappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru chittappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodhai maram keela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai maram keela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada porakuthadaa nyanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada porakuthadaa nyanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalladikittu nikkiren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalladikittu nikkiren naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondai kizhiya kaththuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondai kizhiya kaththuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innoru roundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru roundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothu innoru roundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothu innoru roundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey oothu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey oothu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innoru roundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru roundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru roundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru roundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru roundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru roundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru roundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru roundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One moreu roundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One moreu roundu"/>
</div>
<div class="lyrico-lyrics-wrapper">One moreu roundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One moreu roundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru roundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru roundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru roundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru roundu"/>
</div>
<div class="lyrico-lyrics-wrapper">One moreuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One moreuuu"/>
</div>
</pre>
