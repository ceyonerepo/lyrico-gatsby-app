---
title: "unnimaya song lyrics"
album: "Maniyarayile Ashokan"
artist: "Sreehari K. Nair"
lyricist: "Shihas Ammedkoya"
director: "Shamzu Zayba"
path: "/albums/maniyarayile-ashokan-lyrics"
song: "Unnimaya"
image: ../../images/albumart/maniyarayile-ashokan.jpg
date: 2020-08-31
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/exVwK5RBOoY"
type: "love"
singers:
  - Dulquer Salmaan
  - Jacob Gregory
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Monjathi Penne Unnimaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monjathi Penne Unnimaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanchathil Oppana Padi Vayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanchathil Oppana Padi Vayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenoorum Ente Premam Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenoorum Ente Premam Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Palitta Panchasara Chaya Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palitta Panchasara Chaya Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Monjathi Penne Unnimaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monjathi Penne Unnimaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanchathil Oppana Padi Vayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanchathil Oppana Padi Vayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenoorum Ente Premam Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenoorum Ente Premam Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Palitta Panchasara Chaya Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palitta Panchasara Chaya Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uppilitta Manga Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppilitta Manga Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thengin Mele Thenga Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thengin Mele Thenga Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirathin Mele Mathi Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirathin Mele Mathi Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Roattin Mele Tar Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roattin Mele Tar Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uppilitta Manga Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppilitta Manga Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thengin Mele Thenga Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thengin Mele Thenga Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirathin Mele Mathi Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirathin Mele Mathi Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Roattin Mele Tar Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roattin Mele Tar Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana Nathana Thana Nathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nathana Thana Nathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Nathana Thana Nathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nathana Thana Nathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Nanna Thana Nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nanna Thana Nanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Nanna Nanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nanna Nanana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana Nathana Thana Nathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nathana Thana Nathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Nathana Thana Nathana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nathana Thana Nathana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Nanna Thana Nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nanna Thana Nanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Nanna Nanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Nanna Nanana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Monjathi Penne Unnimaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monjathi Penne Unnimaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanchathil Oppana Padi Vayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanchathil Oppana Padi Vayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenoorum Ente Premam Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenoorum Ente Premam Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Palitta Panchasara Chaya Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palitta Panchasara Chaya Neeye"/>
</div>
</pre>
