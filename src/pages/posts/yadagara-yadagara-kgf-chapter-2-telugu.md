---
title: "yadagara yadagara song lyrics"
album: "KGF Chapter 2"
artist: "Ravi Basrur"
lyricist: "Ramajogayya Sastry"
director: "Prashanth Neel"
path: "/albums/kgf-chapter-2-lyrics"
song: "Yadagara Yadagara"
image: ../../images/albumart/kgf-chapter-2.jpg
date: 2022-04-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Zq5-8tQ9eOQ"
type: "melody"
singers:
  - Suchetha Basrur
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yadagara yadagara dinakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yadagara yadagara dinakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagathike jyothiga nilavara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagathike jyothiga nilavara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padamara nisitera vaalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padamara nisitera vaalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Charithaga ghanathaga velagara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charithaga ghanathaga velagara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aantuleni gamyamukadara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aantuleni gamyamukadara"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthavaraku ledika nidura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthavaraku ledika nidura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ashtadikku lanniyu adara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ashtadikku lanniyu adara"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma kanna kalagaapadara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma kanna kalagaapadara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Charithaga ghanathaga velagara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charithaga ghanathaga velagara"/>
</div>
<div class="lyrico-lyrics-wrapper">Charithaga ghanathaga velagara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charithaga ghanathaga velagara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jananiga deevanam gelupuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jananiga deevanam gelupuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Pusthakam neeshakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pusthakam neeshakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaga dhaga kiranamay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaga dhaga kiranamay"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharanipai cheeyara santhakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharanipai cheeyara santhakam"/>
</div>
</pre>
