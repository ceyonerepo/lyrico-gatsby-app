---
title: "thadaigalai udaithidu song lyrics"
album: "Dhoni Kabadi Kuzhu"
artist: "Roshan Joseph"
lyricist: "N Rasa"
director: "P Iyyappan"
path: "/albums/dhoni-kabadi-kuzhu-lyrics"
song: "Thadaigalai Udaithidu"
image: ../../images/albumart/dhoni-kabadi-kuzhu.jpg
date: 2018-12-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QFs79iSD9rY"
type: "mass"
singers:
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thadaigalai udaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaigalai udaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">thadayangal padithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadayangal padithida"/>
</div>
<div class="lyrico-lyrics-wrapper">poraadu nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poraadu nanba nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kavalaigal kalainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalaigal kalainthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugal kaninthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugal kaninthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaada themba themba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaada themba themba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thadaigalai udaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaigalai udaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">thadayangal padithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadayangal padithida"/>
</div>
<div class="lyrico-lyrics-wrapper">poraadu nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poraadu nanba nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kavalaigal kalainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalaigal kalainthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugal kaninthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugal kaninthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaada themba themba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaada themba themba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muyarchigal seiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muyarchigal seiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivugal theriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivugal theriyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">mothi thaan paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothi thaan paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">unai vella yaru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai vella yaru da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">viyarvaigal sinthamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viyarvaigal sinthamal"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri unai serathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri unai serathu"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi than paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi than paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">unai potrum ooru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai potrum ooru da"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadi nee aadu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadi nee aadu da"/>
</div>
<div class="lyrico-lyrics-wrapper">kalangamal odu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangamal odu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thadaigalai udaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaigalai udaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">thadayangal padithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadayangal padithida"/>
</div>
<div class="lyrico-lyrics-wrapper">poraadu nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poraadu nanba nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kavalaigal kalainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalaigal kalainthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugal kaninthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugal kaninthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaada themba themba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaada themba themba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vetriyum tholviyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetriyum tholviyum"/>
</div>
<div class="lyrico-lyrics-wrapper">maari maari uruvagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maari maari uruvagum"/>
</div>
<div class="lyrico-lyrics-wrapper">jeyikkum varaiyil nee oodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeyikkum varaiyil nee oodu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetriyum tholviyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetriyum tholviyum"/>
</div>
<div class="lyrico-lyrics-wrapper">maari maari uruvagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maari maari uruvagum"/>
</div>
<div class="lyrico-lyrics-wrapper">jeyikkum varaiyil nee oodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeyikkum varaiyil nee oodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kayangal aaridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kayangal aaridum"/>
</div>
<div class="lyrico-lyrics-wrapper">maayangal kaatidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayangal kaatidu"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyathu endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyathu endru"/>
</div>
<div class="lyrico-lyrics-wrapper">moolaiyile othungi vidathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moolaiyile othungi vidathe"/>
</div>
<div class="lyrico-lyrics-wrapper">nee singam endru seeri elu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee singam endru seeri elu"/>
</div>
<div class="lyrico-lyrics-wrapper">pathungi vidaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathungi vidaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thadaigalai udaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaigalai udaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">thadayangal padithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadayangal padithida"/>
</div>
<div class="lyrico-lyrics-wrapper">poraadu nanba nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poraadu nanba nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kavalaigal kalainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavalaigal kalainthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugal kaninthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugal kaninthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaada themba themba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaada themba themba"/>
</div>
</pre>