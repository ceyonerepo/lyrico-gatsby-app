---
title: "desamante song lyrics"
album: "Captain Rana Prathap"
artist: "Shran"
lyricist: "Geddam Veeru"
director: "Haranath Policherla"
path: "/albums/captain-rana-prathap-lyrics"
song: "Desamante"
image: ../../images/albumart/captain-rana-prathap.jpg
date: 2019-06-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/s2p2ZnFCg-k"
type: "mass"
singers:
  - Moushi
  - Meha Venkat
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">desamante bhakthi neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="desamante bhakthi neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">dehamantha shakthi neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dehamantha shakthi neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">khayamantha gaayamainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khayamantha gaayamainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">desa rakshane dhyeyamantoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="desa rakshane dhyeyamantoo"/>
</div>
<div class="lyrico-lyrics-wrapper">bandhupreethi mruthyubheethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bandhupreethi mruthyubheethi"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkanettina sainika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkanettina sainika"/>
</div>
<div class="lyrico-lyrics-wrapper">jaathi swechaku rathi godale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaathi swechaku rathi godale"/>
</div>
<div class="lyrico-lyrics-wrapper">kapugase sainika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kapugase sainika"/>
</div>
<div class="lyrico-lyrics-wrapper">jananam maranam roju neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jananam maranam roju neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">maa hrudhaya vandhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa hrudhaya vandhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">ooo javanudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooo javanudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">maa swecha kanthi bhanuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa swecha kanthi bhanuda"/>
</div>
<div class="lyrico-lyrics-wrapper">oo javanudaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo javanudaa "/>
</div>
<div class="lyrico-lyrics-wrapper">mamu rakshinche bhagavanuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mamu rakshinche bhagavanuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saage vagu sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saage vagu sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">ambaraanni vidichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambaraanni vidichi"/>
</div>
<div class="lyrico-lyrics-wrapper">varadhalle nadichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varadhalle nadichi"/>
</div>
<div class="lyrico-lyrics-wrapper">aa kadali chere nimisham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa kadali chere nimisham "/>
</div>
<div class="lyrico-lyrics-wrapper">chiguru chilaka kalisunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chiguru chilaka kalisunte"/>
</div>
<div class="lyrico-lyrics-wrapper">thummedha paata thodayithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thummedha paata thodayithe"/>
</div>
<div class="lyrico-lyrics-wrapper">vasanthale prathipadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasanthale prathipadham"/>
</div>
<div class="lyrico-lyrics-wrapper">ningi nela kannullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ningi nela kannullo"/>
</div>
<div class="lyrico-lyrics-wrapper">punnami poose vennello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnami poose vennello"/>
</div>
<div class="lyrico-lyrics-wrapper">aakasame haddhuluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aakasame haddhuluga"/>
</div>
<div class="lyrico-lyrics-wrapper">minnulu dhate aanandam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnulu dhate aanandam"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosale prathidinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosale prathidinam"/>
</div>
<div class="lyrico-lyrics-wrapper">mana kutumbamavadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana kutumbamavadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poola vanam  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poola vanam  "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edhuruchoopu choopeduthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuruchoopu choopeduthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">premaloni thiyyadhanalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premaloni thiyyadhanalu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavarintha cheripesthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavarintha cheripesthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">mana madhyana mailu raallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana madhyana mailu raallu"/>
</div>
<div class="lyrico-lyrics-wrapper">edhuruchoopu choopeduthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhuruchoopu choopeduthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">premaloni thiyyadhanalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premaloni thiyyadhanalu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavarintha cheripesthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavarintha cheripesthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">mana madhyana mailu raallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana madhyana mailu raallu"/>
</div>
<div class="lyrico-lyrics-wrapper">ninu vanamuku pampevela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninu vanamuku pampevela"/>
</div>
<div class="lyrico-lyrics-wrapper">gunde karigina sandhram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunde karigina sandhram"/>
</div>
<div class="lyrico-lyrics-wrapper">ninu thirigi choosina tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninu thirigi choosina tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">thanu reppalu doodina alalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanu reppalu doodina alalu"/>
</div>
<div class="lyrico-lyrics-wrapper">okka kshanam okka kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okka kshanam okka kshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">ee chiru illu vandhellu maa harivillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee chiru illu vandhellu maa harivillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chini chonniki enno edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chini chonniki enno edu"/>
</div>
<div class="lyrico-lyrics-wrapper">chitti thalliki enno edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chitti thalliki enno edu"/>
</div>
<div class="lyrico-lyrics-wrapper">danikippudu enno edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="danikippudu enno edu"/>
</div>
<div class="lyrico-lyrics-wrapper">thamara moggaku enno edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamara moggaku enno edu"/>
</div>
<div class="lyrico-lyrics-wrapper">chinniki chonikki pado edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinniki chonikki pado edu"/>
</div>
<div class="lyrico-lyrics-wrapper">chitti thalliki pado edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chitti thalliki pado edu"/>
</div>
<div class="lyrico-lyrics-wrapper">danikippudu pado edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="danikippudu pado edu"/>
</div>
<div class="lyrico-lyrics-wrapper">thamara moggaku pado edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamara moggaku pado edu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee navvula puvvulanthone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee navvula puvvulanthone"/>
</div>
<div class="lyrico-lyrics-wrapper">perchukunta edha bathukamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perchukunta edha bathukamma"/>
</div>
<div class="lyrico-lyrics-wrapper">nee guruthula neeruthraagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee guruthula neeruthraagi"/>
</div>
<div class="lyrico-lyrics-wrapper">peruguthundhi ee chiru komma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peruguthundhi ee chiru komma"/>
</div>
<div class="lyrico-lyrics-wrapper">mee premaku dhooramaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mee premaku dhooramaina "/>
</div>
<div class="lyrico-lyrics-wrapper">thirigi raani enno kshanalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirigi raani enno kshanalu"/>
</div>
<div class="lyrico-lyrics-wrapper">meerichhe rakshana chalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meerichhe rakshana chalu"/>
</div>
<div class="lyrico-lyrics-wrapper">nireekshanalainaa sukhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nireekshanalainaa sukhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">rela rela relaare rela relaare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rela rela relaare rela relaare "/>
</div>
<div class="lyrico-lyrics-wrapper">rela rela relaare rela relaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rela rela relaare rela relaare"/>
</div>
<div class="lyrico-lyrics-wrapper">rela rela relaare rela relaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rela rela relaare rela relaare"/>
</div>
<div class="lyrico-lyrics-wrapper">rela rela relaare rela relaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rela rela relaare rela relaare"/>
</div>
</pre>
