---
title: "priya priya priya song lyrics"
album: "Parris Jeyaraj"
artist: "Santhosh Narayanan"
lyricist: "Rokesh"
director: "Johnson K"
path: "/albums/parris-jeyaraj-lyrics"
song: "priya priya priya"
image: ../../images/albumart/parris-jeyaraj.jpg
date: 2021-02-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NT3_MPHucDU"
type: "Love"
singers:
  - Gana Muthu
  - Isaivani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa koorukatta kuppama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa koorukatta kuppama"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkooda vantha nippenma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda vantha nippenma"/>
</div>
<div class="lyrico-lyrics-wrapper">Noovama daavu adippenma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noovama daavu adippenma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee road-u kada curry vadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee road-u kada curry vadai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unakku yetha medhu vadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unakku yetha medhu vadai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkaama kaadhalippoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkaama kaadhalippoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaithunu poidiviya maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaithunu poidiviya maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundha irukki kattipen maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundha irukki kattipen maamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa priya priya priya enna serthupiya di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa priya priya priya enna serthupiya di"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa free-ah free-ah free-ah enna paarthupiya di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa free-ah free-ah free-ah enna paarthupiya di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dey paiya paiya paiya neeyum palana kedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dey paiya paiya paiya neeyum palana kedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa haiyaa haiyaa haiyaa naanum unnoda jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa haiyaa haiyaa haiyaa naanum unnoda jodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chal chal chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal chal chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal chal chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal chal chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal chal chal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethurula vandhu nee daaladikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethurula vandhu nee daaladikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Bussunu poondhu en kichita maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bussunu poondhu en kichita maara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozha thottu naan pinnadiya yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha thottu naan pinnadiya yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaadiya thattu vaa suthalam oora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaadiya thattu vaa suthalam oora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedinu varuven voottanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedinu varuven voottanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunthinu pesuvom rotaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunthinu pesuvom rotaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasthy illatha ennanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasthy illatha ennanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiya kaadhala sollu kaathanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiya kaadhala sollu kaathanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh priya priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh priya priya"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh priya priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh priya priya"/>
</div>
<div class="lyrico-lyrics-wrapper">Priya priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priya priya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennada"/>
</div>
<div class="lyrico-lyrics-wrapper">Free ah priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free ah priya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa priya priya priya enna serthupiya di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa priya priya priya enna serthupiya di"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa free-ah free-ah free-ah enna paarthupiya di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa free-ah free-ah free-ah enna paarthupiya di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dey paiya paiya paiya neeyum palana kedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dey paiya paiya paiya neeyum palana kedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa haiyaa haiyaa haiyaa naanum unnoda jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa haiyaa haiyaa haiyaa naanum unnoda jodi"/>
</div>
</pre>
