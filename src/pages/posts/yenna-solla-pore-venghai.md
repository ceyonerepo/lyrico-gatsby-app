---
title: "yenna solla pore song lyrics"
album: "Venghai"
artist: "Devi Sri Prasad"
lyricist: "Hari"
director: "Hari"
path: "/albums/venghai-lyrics"
song: "Yenna Solla Pore"
image: ../../images/albumart/venghai.jpg
date: 2011-07-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LPGGO_QoUU8"
type: "love"
singers:
  - Devi Sri Prasad, 
  - M.L.R. Karthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unna Matum Nenjukula Vachirukan Intha Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Matum Nenjukula Vachirukan Intha Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenaga Ivan Manasa Killaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenaga Ivan Manasa Killaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu Maasam Aaru Maasam Kaathirukum Payapulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu Maasam Aaru Maasam Kaathirukum Payapulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaaga Ivan Manasa Kollaatha Nee Kollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaaga Ivan Manasa Kollaatha Nee Kollaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Hoo Oh Kollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Hoo Oh Kollaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenna Solla Pora Nee Yenna Solla Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Solla Pora Nee Yenna Solla Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppa Solla Pora Nee Yeppa Solla Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa Solla Pora Nee Yeppa Solla Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Yenna Solla Pora Nee Yenna Solla Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yenna Solla Pora Nee Yenna Solla Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppa Solla Pora Nee Yeppa Solla Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa Solla Pora Nee Yeppa Solla Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathirupen Kaathirupen Aaru Maasam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirupen Kaathirupen Aaru Maasam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Mulichu Paduthirunthen Moonu Maasam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Mulichu Paduthirunthen Moonu Maasam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo Nadakuthu Idhayam Valikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Nadakuthu Idhayam Valikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Thavikuthu Unnodaya Vaarthaikaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Thavikuthu Unnodaya Vaarthaikaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenna Solla Pora Nee Yenna Solla Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Solla Pora Nee Yenna Solla Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppa Solla Pora Nee Yeppa Solla Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa Solla Pora Nee Yeppa Solla Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Matum Nenjukula Vachirukan Intha Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Matum Nenjukula Vachirukan Intha Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenaga Ivan Manasa Killaatha Killaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenaga Ivan Manasa Killaatha Killaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu Maasam Aaru Maasam Kaathirukum Payapulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu Maasam Aaru Maasam Kaathirukum Payapulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaaga Ivan Manasa Kollaatha Kollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaaga Ivan Manasa Kollaatha Kollaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm Chinna Pulla Nesam Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Chinna Pulla Nesam Ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacha Pulla Paasam Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Pulla Paasam Ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manasu Thaakiyathu Unnala Unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu Thaakiyathu Unnala Unnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm Jaathi Matham Paarkalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Jaathi Matham Paarkalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sammathatha Ketkalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sammathatha Ketkalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalunu Aayiruchu Thanaala Thanaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalunu Aayiruchu Thanaala Thanaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nesama Nesama Nenjukulla Naan Aluthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesama Nesama Nenjukulla Naan Aluthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudaiya Vaarthaikaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudaiya Vaarthaikaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenna Solla Pora Nee Yenna Solla Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Solla Pora Nee Yenna Solla Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppa Solla Pora Nee Yeppa Solla Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa Solla Pora Nee Yeppa Solla Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Matum Nenjukula Vachirukan Intha Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Matum Nenjukula Vachirukan Intha Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenaga Ivan Manasa Killaatha Killaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenaga Ivan Manasa Killaatha Killaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu Maasam Aaru Maasam Kaathirukum Payapulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu Maasam Aaru Maasam Kaathirukum Payapulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaaga Ivan Manasa Kollaatha Nee Kollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaaga Ivan Manasa Kollaatha Nee Kollaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Vettaruva Thookikittu Vetti Paya Polirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Vettaruva Thookikittu Vetti Paya Polirunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekka Pattu Naan Nadanthen Unnala Unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekka Pattu Naan Nadanthen Unnala Unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta Kambi Thookikittu Kandapadi Naan Thirinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta Kambi Thookikittu Kandapadi Naan Thirinjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu Patu Naan Nadanthen Pinnala Un Pinnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu Patu Naan Nadanthen Pinnala Un Pinnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthusa Puthusa Maari Iruken Theri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthusa Puthusa Maari Iruken Theri "/>
</div>
<div class="lyrico-lyrics-wrapper">Iruken Unnodaya Paarvaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruken Unnodaya Paarvaiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenna Solla Pora Yenna Solla Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Solla Pora Yenna Solla Pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppa Solla Pora Nee Yeppa Solla Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa Solla Pora Nee Yeppa Solla Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Matum Nenjukula Vachirukan Intha Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Matum Nenjukula Vachirukan Intha Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenaga Ivan Manasa Killaatha Killaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenaga Ivan Manasa Killaatha Killaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu Maasam Aaru Maasam Kaathirukum Payapulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu Maasam Aaru Maasam Kaathirukum Payapulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaaga Ivan Manasa Kollaatha Nee Kollaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaaga Ivan Manasa Kollaatha Nee Kollaatha"/>
</div>
</pre>
