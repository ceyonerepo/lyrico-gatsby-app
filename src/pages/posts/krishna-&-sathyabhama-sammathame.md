---
title: "krishna & sathyabhama song lyrics"
album: "Sammathame"
artist: "Shekar Chandra"
lyricist: "Krishna Kanth"
director: "Gopinath Reddy"
path: "/albums/sammathame-lyrics"
song: "Krishna & Sathyabhama"
image: ../../images/albumart/sammathame.jpg
date: 2022-06-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ItHiyJlSVhw"
type: "happy"
singers:
  -	Yazin Nizar
  - Sireesha Bhagavatula
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nenu Oohinchale Nenanukunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Oohinchale Nenanukunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Ammayi Nuvvenani Asalu Oohinchale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammayi Nuvvenani Asalu Oohinchale "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Oohinchale Inthi Easy Ga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Oohinchale Inthi Easy Ga "/>
</div>
<div class="lyrico-lyrics-wrapper">Nen Neeku Padathanani Asalu Oohinchale  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nen Neeku Padathanani Asalu Oohinchale  "/>
</div>
<div class="lyrico-lyrics-wrapper">Ento Prathi Paatalo Cheppe Padame Kadha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento Prathi Paatalo Cheppe Padame Kadha "/>
</div>
<div class="lyrico-lyrics-wrapper">Ina Prathisaari Sari Kotha Veluge Idha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ina Prathisaari Sari Kotha Veluge Idha "/>
</div>
<div class="lyrico-lyrics-wrapper">Vere Pani Leduga Preme Saripoduga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vere Pani Leduga Preme Saripoduga "/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Chaalu Chaalu Ani Konthasepu Mari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Chaalu Chaalu Ani Konthasepu Mari "/>
</div>
<div class="lyrico-lyrics-wrapper">Konthasepu Poneedu Antha Twaraga  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konthasepu Poneedu Antha Twaraga  "/>
</div>
<div class="lyrico-lyrics-wrapper">Krishna And Sathyabhama Prema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna And Sathyabhama Prema "/>
</div>
<div class="lyrico-lyrics-wrapper">Slow Slow Ga Start Ayyenu Lema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Slow Slow Ga Start Ayyenu Lema "/>
</div>
<div class="lyrico-lyrics-wrapper">Krishna And Sathyabhama Prema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna And Sathyabhama Prema "/>
</div>
<div class="lyrico-lyrics-wrapper">Impress Ey Chese Veela Drama  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Impress Ey Chese Veela Drama  "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andham Tappele Control Ey Tappisthunde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andham Tappele Control Ey Tappisthunde "/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Cheyyemo Naa Maata Vinabodhule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Cheyyemo Naa Maata Vinabodhule "/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Maatale Tagginchara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Maatale Tagginchara "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Chempa Pai Thagilisthe Vinuna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Chempa Pai Thagilisthe Vinuna "/>
</div>
<div class="lyrico-lyrics-wrapper">Kopaalu Dupe Ey Le Neekaina Ok Le 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopaalu Dupe Ey Le Neekaina Ok Le "/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhante Pai Paike Thidathavu Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhante Pai Paike Thidathavu Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Krishna And Sathyabhama Prema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna And Sathyabhama Prema "/>
</div>
<div class="lyrico-lyrics-wrapper">Slow Slow Ga Start Ayyenu Lema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Slow Slow Ga Start Ayyenu Lema "/>
</div>
<div class="lyrico-lyrics-wrapper">Krishna And Sathyabhama Prema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna And Sathyabhama Prema "/>
</div>
<div class="lyrico-lyrics-wrapper">Impress Ey Chese Veela Drama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Impress Ey Chese Veela Drama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dress Ey Baagundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dress Ey Baagundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mantalne Puttistunde Gaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mantalne Puttistunde Gaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Parikinilo Nee Beauty Oh Range Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parikinilo Nee Beauty Oh Range Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Istame Nakundadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Istame Nakundadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Taste Le Ruddhesthey Thaguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Taste Le Ruddhesthey Thaguna"/>
</div>
<div class="lyrico-lyrics-wrapper">Duet Centre Lo Ee Fightu Aapamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duet Centre Lo Ee Fightu Aapamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddhante Comment Ey Cheyabonule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddhante Comment Ey Cheyabonule"/>
</div>
<div class="lyrico-lyrics-wrapper">Krishna And Sathyabhama Prema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna And Sathyabhama Prema "/>
</div>
<div class="lyrico-lyrics-wrapper">Slow Slow Ga Start Ayyindi Lema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Slow Slow Ga Start Ayyindi Lema "/>
</div>
<div class="lyrico-lyrics-wrapper">Krishna And Sathyabhama Prema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna And Sathyabhama Prema "/>
</div>
<div class="lyrico-lyrics-wrapper">Impress Ey Chese Veela Drama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Impress Ey Chese Veela Drama"/>
</div>
</pre>
