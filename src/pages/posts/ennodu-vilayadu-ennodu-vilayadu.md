---
title: "ennodu vilayadu song lyrics"
album: "Ennodu Vilayadu"
artist: "A. Moses - Sudharshan M. Kumar"
lyricist: "Sarathy"
director: "Arun Krishnaswami"
path: "/albums/ennodu-vilayadu-lyrics"
song: "Ennodu Vilayadu"
image: ../../images/albumart/ennodu-vilayadu.jpg
date: 2017-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ngQzm8GudTE"
type: "mass"
singers:
  - Naresh Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ennodu nee vilayadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu nee vilayadinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhi aanathu kai neetuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhi aanathu kai neetuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">munneru nee ena kooriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munneru nee ena kooriye"/>
</div>
<div class="lyrico-lyrics-wrapper">iru kaathile thee mootuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru kaathile thee mootuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">nalum nee thadumaarave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalum nee thadumaarave"/>
</div>
<div class="lyrico-lyrics-wrapper">pala aasaigal undakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala aasaigal undakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thindadi nee alainthodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thindadi nee alainthodave"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhi melum melum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhi melum melum "/>
</div>
<div class="lyrico-lyrics-wrapper">thaayam aaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayam aaduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nallavanum thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallavanum thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">vallavanum thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vallavanum thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">kulla nari koondukkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulla nari koondukkula"/>
</div>
<div class="lyrico-lyrics-wrapper">kalla thanam kaatu mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalla thanam kaatu mella"/>
</div>
<div class="lyrico-lyrics-wrapper">kayangalai kaatum yarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kayangalai kaatum yarum"/>
</div>
<div class="lyrico-lyrics-wrapper">niyayangalai paarpathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niyayangalai paarpathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">laabangalai paarkum yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laabangalai paarkum yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">paavangalai theerpathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavangalai theerpathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadal munnala nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal munnala nee "/>
</div>
<div class="lyrico-lyrics-wrapper">kadai potaka neeye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadai potaka neeye "/>
</div>
<div class="lyrico-lyrics-wrapper">thane alai endraganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane alai endraganum"/>
</div>
<div class="lyrico-lyrics-wrapper">valai undakkanum porada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valai undakkanum porada"/>
</div>
<div class="lyrico-lyrics-wrapper">puli munnala nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puli munnala nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kalam kantakka neeye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalam kantakka neeye "/>
</div>
<div class="lyrico-lyrics-wrapper">thane irai endraganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane irai endraganum"/>
</div>
<div class="lyrico-lyrics-wrapper">athai vendraaganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai vendraaganum"/>
</div>
<div class="lyrico-lyrics-wrapper">munneri vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munneri vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennodu nee vilayadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu nee vilayadinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhi aanathu kai neetuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhi aanathu kai neetuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">munneru nee ena kooriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munneru nee ena kooriye"/>
</div>
<div class="lyrico-lyrics-wrapper">iru kaathile thee mootuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru kaathile thee mootuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">nalum nee thadumaarave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalum nee thadumaarave"/>
</div>
<div class="lyrico-lyrics-wrapper">pala aasaigal undakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala aasaigal undakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thindadi nee alainthodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thindadi nee alainthodave"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhi melum melum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhi melum melum "/>
</div>
<div class="lyrico-lyrics-wrapper">thaayam aaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayam aaduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dhraavagam pole 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhraavagam pole "/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">sooriyan mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sooriyan mele"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai irakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai irakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalgalai thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalgalai thanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyai murikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyai murikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavil thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavil thanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannai parikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannai parikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu mosakkaran vaalun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu mosakkaran vaalun"/>
</div>
<div class="lyrico-lyrics-wrapper">pala maayakaaran kottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala maayakaaran kottai"/>
</div>
<div class="lyrico-lyrics-wrapper">adha moodi pottu neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha moodi pottu neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">ada aadi paaru vettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada aadi paaru vettai"/>
</div>
<div class="lyrico-lyrics-wrapper">ethiraali pogum paathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethiraali pogum paathai"/>
</div>
<div class="lyrico-lyrics-wrapper">athai thedi thedi thondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai thedi thedi thondu"/>
</div>
<div class="lyrico-lyrics-wrapper">avan munthi pogum munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan munthi pogum munne"/>
</div>
<div class="lyrico-lyrics-wrapper">antha vetri kottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha vetri kottai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thandu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadal munnala nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal munnala nee "/>
</div>
<div class="lyrico-lyrics-wrapper">kadai potaka neeye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadai potaka neeye "/>
</div>
<div class="lyrico-lyrics-wrapper">thane alai endraganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane alai endraganum"/>
</div>
<div class="lyrico-lyrics-wrapper">valai undakkanum porada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valai undakkanum porada"/>
</div>
<div class="lyrico-lyrics-wrapper">puli munnala nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puli munnala nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kalam kantakka neeye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalam kantakka neeye "/>
</div>
<div class="lyrico-lyrics-wrapper">thane irai endraganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane irai endraganum"/>
</div>
<div class="lyrico-lyrics-wrapper">athai vendraaganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai vendraaganum"/>
</div>
<div class="lyrico-lyrics-wrapper">munneri vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munneri vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennodu nee vilayadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu nee vilayadinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhi aanathu kai neetuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhi aanathu kai neetuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">munneru nee ena kooriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munneru nee ena kooriye"/>
</div>
<div class="lyrico-lyrics-wrapper">iru kaathile thee mootuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru kaathile thee mootuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">nalum nee thadumaarave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalum nee thadumaarave"/>
</div>
<div class="lyrico-lyrics-wrapper">pala aasaigal undakuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala aasaigal undakuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">thindadi nee alainthodave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thindadi nee alainthodave"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhi melum melum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhi melum melum "/>
</div>
<div class="lyrico-lyrics-wrapper">thaayam aaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayam aaduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadal munnala nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal munnala nee "/>
</div>
<div class="lyrico-lyrics-wrapper">kadai potaka neeye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadai potaka neeye "/>
</div>
<div class="lyrico-lyrics-wrapper">thane alai endraganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane alai endraganum"/>
</div>
<div class="lyrico-lyrics-wrapper">valai undakkanum porada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valai undakkanum porada"/>
</div>
<div class="lyrico-lyrics-wrapper">puli munnala nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puli munnala nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kalam kantakka neeye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalam kantakka neeye "/>
</div>
<div class="lyrico-lyrics-wrapper">thane irai endraganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane irai endraganum"/>
</div>
<div class="lyrico-lyrics-wrapper">athai vendraaganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai vendraaganum"/>
</div>
<div class="lyrico-lyrics-wrapper">munneri vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munneri vaa vaa vaa"/>
</div>
</pre>
