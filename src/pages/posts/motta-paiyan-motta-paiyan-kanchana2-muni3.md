---
title: "motta paiyan motta paiyan song lyrics"
album: "Kanchana2-Muni3"
artist: "S. Thaman - Leon James - C. Sathya"
lyricist: "Viveka"
director: "Raghava Lawrence"
path: "/albums/kanchana2-muni3-lyrics"
song: "Motta Paiyan Motta Paiyan"
image: ../../images/albumart/kanchana2-muni3.jpg
date: 2015-04-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2kLR5IKhW-M"
type: "Love"
singers:
  - K. S. Chithra
  - Sooraj Santhosh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Motta Paiyan Motta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Paiyan Motta Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Romba Ketta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Romba Ketta Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Paiyan Motta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Paiyan Motta Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Romba Ketta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Romba Ketta Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketta Paiyan Ketta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Paiyan Ketta Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Usura Thotta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usura Thotta Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Uravu Kedaikkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Uravu Kedaikkumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Thavichadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Thavichadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Varamum Indru Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Varamum Indru Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyil Kedachadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil Kedachadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Kadhal Jeyikkum Neramey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kadhal Jeyikkum Neramey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Kadavul Thondrumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kadavul Thondrumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Paadha Golusu Modhi Therikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paadha Golusu Modhi Therikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadham Rasithu Naalaikkazhippeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadham Rasithu Naalaikkazhippeney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Paiyan Motta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Paiyan Motta Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Romba Ketta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Romba Ketta Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukulley Irundhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulley Irundhadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarthaigalaa Vizhundhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaigalaa Vizhundhadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Nodi Indha Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Nodi Indha Nodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manniley Sorgam Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manniley Sorgam Kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennavanai Thandhadharkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavanai Thandhadharkku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saamikku Nandri Sonnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamikku Nandri Sonnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kodi Varusham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kodi Varusham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhnthaana Piragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhnthaana Piragum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Udal Adhira Uyir Adhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Udal Adhira Uyir Adhira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ninaithu Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ninaithu Ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandheney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnavida Azhagigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnavida Azhagigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanaiyo Ulagile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanaiyo Ulagile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkalaam Adhu Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkalaam Adhu Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkazhagu Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkazhagu Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkum Un Sirippu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkum Un Sirippu Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagugalin Ellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagugalin Ellai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therodum Theruvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therodum Theruvil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Poga Paarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Poga Paarthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Madam Marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Madam Marandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadharugil Idam Pidikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadharugil Idam Pidikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhu Thavippeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Thavippeney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Paiyan Motta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Paiyan Motta Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Romba Ketta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Romba Ketta Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketta Paiyan Ketta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta Paiyan Ketta Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Usura Thotta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usura Thotta Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Uravu Kedaikkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Uravu Kedaikkumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam Thavichadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Thavichadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Varamum Indru Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Varamum Indru Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyil Kedachadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil Kedachadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Kadhal Jeyikkum Neramey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kadhal Jeyikkum Neramey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Kadavul Thondrumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kadavul Thondrumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Paadha Golusu Modhi Therikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paadha Golusu Modhi Therikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadham Rasithu Naalaikkazhippeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadham Rasithu Naalaikkazhippeney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Paiyan Motta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Paiyan Motta Paiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Romba Ketta Paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Romba Ketta Paiyan"/>
</div>
</pre>
