---
title: "va sagee song lyrics"
album: "Neeya 2"
artist: "Shabir"
lyricist: "Kabilan"
director: "L. Suresh"
path: "/albums/neeya-2-lyrics"
song: "Va Sagee"
image: ../../images/albumart/neeya-2.jpg
date: 2019-05-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/g5aBDFUFLGQ"
type: "love"
singers:
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaththikuchi paarvaiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaththikuchi paarvaiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkiren thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkiren thavikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Va sagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va sagee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutterikkum kaadhalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutterikkum kaadhalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkiren thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkiren thudikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Va sagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va sagee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada vaana devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada vaana devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neril vanthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neril vanthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aimpulangala theendi sendraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aimpulangala theendi sendraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal paarvaiyaal amba vittalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal paarvaiyaal amba vittalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu kaathula nenja thottalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu kaathula nenja thottalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kannmunnavae nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannmunnavae nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kai korthida vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kai korthida vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mei theendidum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mei theendidum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Or poi punnagai yedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or poi punnagai yedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un dhegathai yerkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un dhegathai yerkka"/>
</div>
<div class="lyrico-lyrics-wrapper">En thevaiyai theerkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thevaiyai theerkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaana pogumae selai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana pogumae selai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sei manmadha leelai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sei manmadha leelai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeyena nee enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyena nee enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendum viral ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendum viral ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvaaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaasanai vaanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasanai vaanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam muzhuvadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam muzhuvadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuvaaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvaaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhum en ilamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum en ilamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam unathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochil dhinam erigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochil dhinam erigirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va sagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va sagee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Va sagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va sagee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyae en iravin manjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae en iravin manjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enbathu sallaabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enbathu sallaabam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil pozhigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil pozhigirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va sagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va sagee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Va sagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va sagee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamugan paarthadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamugan paarthadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaenil kulithida azhaithaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenil kulithida azhaithaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaamam thean thottam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam thean thottam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaamam muzhuvathum pizhivaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaamam muzhuvathum pizhivaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaraai un paruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraai un paruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan viratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan viratham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pokkum vanthu parimaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pokkum vanthu parimaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va sagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va sagee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Va sagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va sagee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaraai en thayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraai en thayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendam udanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendam udanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu nee pasiyaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu nee pasiyaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va sagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va sagee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Va sagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va sagee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaththikuchi paarvaiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaththikuchi paarvaiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkiren thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkiren thavikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Va sagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va sagee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutterikkum kaadhalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutterikkum kaadhalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkiren thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkiren thudikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thanakkathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thanakkathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Va sagee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va sagee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada vaana devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada vaana devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neril vanthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neril vanthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aimpulangala theendi sendraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aimpulangala theendi sendraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal paarvaiyaal amba vittalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal paarvaiyaal amba vittalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu kaathula nenja thottalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu kaathula nenja thottalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kannmunnavae nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannmunnavae nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kai korthida vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kai korthida vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mei theendidum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mei theendidum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Or poi punnagai yedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or poi punnagai yedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un dhegathai yerkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un dhegathai yerkka"/>
</div>
<div class="lyrico-lyrics-wrapper">En thevaiyai theerkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thevaiyai theerkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaana pogumae selai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana pogumae selai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sei manmadha leelai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sei manmadha leelai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va sagee eee ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va sagee eee ee"/>
</div>
</pre>
