---
title: "lojakku mojakku song lyrics"
album: "Aranmanai 3"
artist: "C. Sathya"
lyricist: "Mohan Rajan"
director: "Sundar C"
path: "/albums/aranmanai-3-lyrics"
song: "Lojakku Mojakku"
image: ../../images/albumart/aranmanai-3.jpg
date: 2021-10-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rbA_6a17Yxg"
type: "happy"
singers:
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Lojakku mojakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lojakku mojakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Baajakku baajakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baajakku baajakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Baajakku baajakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baajakku baajakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Baajakku baajakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baajakku baajakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lojakku mojakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lojakku mojakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Baajakku baajakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baajakku baajakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Lojakku mojakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lojakku mojakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Baajakku baajakku ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baajakku baajakku ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama minnalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama minnalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Morattu single ahh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu single ahh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lojakku mojakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lojakku mojakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajakku bajakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajakku bajakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Lojakku mojakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lojakku mojakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajakku bajakku maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajakku bajakku maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaemaa angel uhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaemaa angel uhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaap laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaap laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bitta cycle laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bitta cycle laa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lojakku mojakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lojakku mojakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajakku bajakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajakku bajakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Lojakku mojakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lojakku mojakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajakku bajakku maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajakku bajakku maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ka ka ka po solitu po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka ka ka po solitu po"/>
</div>
<div class="lyrico-lyrics-wrapper">Tik tok panni manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tok panni manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru gps ah kootitu po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru gps ah kootitu po"/>
</div>
<div class="lyrico-lyrics-wrapper">Jik jak aana vayasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jik jak aana vayasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wire uu ilaa minsaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wire uu ilaa minsaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">En usura vaangum samsaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En usura vaangum samsaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvaga nee maaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvaga nee maaranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love uhh melaa love yum vaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love uhh melaa love yum vaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enn usura melaa yum thechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn usura melaa yum thechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhuduven unoda kai melaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhuduven unoda kai melaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avatar padatha kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avatar padatha kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Blue ahh irukudhunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blue ahh irukudhunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo vaara paakala kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo vaara paakala kannala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathiyam pannuven vaa di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyam pannuven vaa di"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa rombha uthaman di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa rombha uthaman di"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiyam theatre uh pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyam theatre uh pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna getha paathupen di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna getha paathupen di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa patta kadam theeranumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa patta kadam theeranumey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangam vandha seranumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam vandha seranumey"/>
</div>
<div class="lyrico-lyrics-wrapper">Google uh polana thoongama thedurendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Google uh polana thoongama thedurendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaama minnalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaama minnalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Morattu single uhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu single uhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lojakku mojakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lojakku mojakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajakku bajakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajakku bajakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Lojakku mojakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lojakku mojakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajakku bajakku maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajakku bajakku maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ka ka ka poo solitu poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ka ka ka poo solitu poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Tik tok panni manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik tok panni manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru gps ahh kootitu poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru gps ahh kootitu poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Jik jak aana vayasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jik jak aana vayasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wire uu ilaa minsaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wire uu ilaa minsaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">En usura vaangum samsaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En usura vaangum samsaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvaga nee maaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvaga nee maaranum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhaalu kulfiya kudukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaalu kulfiya kudukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyulaa kuchi dhaan michama irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyulaa kuchi dhaan michama irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arambam yelamey inikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arambam yelamey inikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaa pooga pooga bore uu bore uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaa pooga pooga bore uu bore uu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhaalu kulfiya kudukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaalu kulfiya kudukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyulaa kuchi dhaan michama irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyulaa kuchi dhaan michama irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arambam yelamey inikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arambam yelamey inikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaa lojakku mojakku bajakku bajakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaa lojakku mojakku bajakku bajakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhaalu kulfiya kudukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaalu kulfiya kudukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyulaa kuchi dhaan michama irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyulaa kuchi dhaan michama irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arambam yelamey inikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arambam yelamey inikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaa pooga pooga bore uu bore uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaa pooga pooga bore uu bore uu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhaalu kulfiya kudukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaalu kulfiya kudukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyulaa kuchi dhaan michama irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyulaa kuchi dhaan michama irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arambam yelamey inikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arambam yelamey inikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaa lojakku mojakku bajakku bajakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaa lojakku mojakku bajakku bajakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaama minnalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaama minnalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Morattu single uhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu single uhh"/>
</div>
</pre>
