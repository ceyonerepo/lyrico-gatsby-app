---
title: "chidambarathil seeri varum singam song lyrics"
album: "Chidambaram Railwaygate"
artist: "Karthik Raja"
lyricist: "Arun Bharathi "
director: "Sivabalan"
path: "/albums/chidambaram-railwaygate-lyrics"
song: "Chidambarathil Seeri Varum Singam"
image: ../../images/albumart/chidambaram-railwaygate.jpg
date: 2021-02-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MV3fy7M9Jyc"
type: "Friendship"
singers:
  - Mukesh
  - Senthil Dass
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">chidambarathil seeri varum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chidambarathil seeri varum "/>
</div>
<div class="lyrico-lyrics-wrapper">singam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan singam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan singam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">pasathula epavume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasathula epavume"/>
</div>
<div class="lyrico-lyrics-wrapper">thangam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">pasathula epavume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasathula epavume"/>
</div>
<div class="lyrico-lyrics-wrapper">thangam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam thana da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chidambarathil seeri varum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chidambarathil seeri varum "/>
</div>
<div class="lyrico-lyrics-wrapper">singam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan singam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan singam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">pasathula epavume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasathula epavume"/>
</div>
<div class="lyrico-lyrics-wrapper">thangam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">sutha thangam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutha thangam thana da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ne vaana thoda unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne vaana thoda unaku"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum thol kodupen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum thol kodupen da"/>
</div>
<div class="lyrico-lyrics-wrapper">ini vetri maalai namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini vetri maalai namma"/>
</div>
<div class="lyrico-lyrics-wrapper">tholil vanthu serum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholil vanthu serum da"/>
</div>
<div class="lyrico-lyrics-wrapper">oru vambu vantha piruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vambu vantha piruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">naanga menjuduvom da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga menjuduvom da"/>
</div>
<div class="lyrico-lyrics-wrapper">ella kottaiyilum namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ella kottaiyilum namma"/>
</div>
<div class="lyrico-lyrics-wrapper">kodi dhool kelappum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi dhool kelappum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chidambarathil seeri varum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chidambarathil seeri varum "/>
</div>
<div class="lyrico-lyrics-wrapper">singam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan singam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan singam thana da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thozhanaga neril vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thozhanaga neril vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul neyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul neyada"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul neyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul neyada"/>
</div>
<div class="lyrico-lyrics-wrapper">ey aayul vara enna sumakum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ey aayul vara enna sumakum "/>
</div>
<div class="lyrico-lyrics-wrapper">thaayum neyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayum neyada"/>
</div>
<div class="lyrico-lyrics-wrapper">thaayum neyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayum neyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna pola natpu iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pola natpu iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam kooda sirusu thanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam kooda sirusu thanada"/>
</div>
<div class="lyrico-lyrics-wrapper">un manasu kadala vida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un manasu kadala vida "/>
</div>
<div class="lyrico-lyrics-wrapper">romba romba perusu thanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba romba perusu thanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nerungi varum ethiringa ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerungi varum ethiringa ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">namma kita thoosu thanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma kita thoosu thanada"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum nanum serthu iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum nanum serthu iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">epavume maasu thanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epavume maasu thanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">arjunaru pilla petha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arjunaru pilla petha"/>
</div>
<div class="lyrico-lyrics-wrapper">kannan neyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannan neyada"/>
</div>
<div class="lyrico-lyrics-wrapper">kannan neyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannan neyada"/>
</div>
<div class="lyrico-lyrics-wrapper">aabathula kaakka varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aabathula kaakka varum"/>
</div>
<div class="lyrico-lyrics-wrapper">karnan thanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karnan thanada"/>
</div>
<div class="lyrico-lyrics-wrapper">karnan thanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karnan thanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">natpa pola sontha pantham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpa pola sontha pantham "/>
</div>
<div class="lyrico-lyrics-wrapper">yarum inga ooril illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarum inga ooril illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">natpu irukum oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpu irukum oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">thunbam ethum etti kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunbam ethum etti kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">parpathilla da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parpathilla da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ne iruntha pothum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne iruntha pothum "/>
</div>
<div class="lyrico-lyrics-wrapper">nanum jeichuduven da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum jeichuduven da"/>
</div>
<div class="lyrico-lyrics-wrapper">en usura kooda thanamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en usura kooda thanamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">kuduthuduven da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuduthuduven da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chidambarathil seeri varum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chidambarathil seeri varum "/>
</div>
<div class="lyrico-lyrics-wrapper">singam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan singam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan singam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">pasathula epavume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasathula epavume"/>
</div>
<div class="lyrico-lyrics-wrapper">thangam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">pasathula epavume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasathula epavume"/>
</div>
<div class="lyrico-lyrics-wrapper">thangam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam thana da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chidambarathil seeri varum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chidambarathil seeri varum "/>
</div>
<div class="lyrico-lyrics-wrapper">singam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan singam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan singam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">pasathula epavume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasathula epavume"/>
</div>
<div class="lyrico-lyrics-wrapper">thangam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">sutha thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutha thana da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ne vaana thoda unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne vaana thoda unaku"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum thol kodupen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum thol kodupen da"/>
</div>
<div class="lyrico-lyrics-wrapper">ini vetri maalai namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini vetri maalai namma"/>
</div>
<div class="lyrico-lyrics-wrapper">tholil vanthu serum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholil vanthu serum da"/>
</div>
<div class="lyrico-lyrics-wrapper">oru vambu vantha piruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vambu vantha piruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">naanga menjuduvom da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanga menjuduvom da"/>
</div>
<div class="lyrico-lyrics-wrapper">ella kottaiyilum namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ella kottaiyilum namma"/>
</div>
<div class="lyrico-lyrics-wrapper">kodi dhool kelappum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi dhool kelappum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chidambarathil seeri varum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chidambarathil seeri varum "/>
</div>
<div class="lyrico-lyrics-wrapper">singam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan singam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan singam thana da"/>
</div>
</pre>
