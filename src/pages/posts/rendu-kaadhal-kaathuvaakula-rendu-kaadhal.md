---
title: "rendu kaadhal song lyrics"
album: "Kaathuvaakula Rendu Kaadhal"
artist: "Anirudh Ravichander"
lyricist: "Vignesh Shivan"
director: "Vignesh Shivan"
path: "/albums/kaathuvaakula-rendu-kaadhal-lyrics"
song: "Rendu Kaadhal"
image: ../../images/albumart/kaathuvaakula-rendu-kaadhal.jpg
date: 2022-04-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YlymYnShOLw"
type: "love"
singers:
  - Anirudh Ravichander
  - Shakthisree Gopalan
  - Aishwarya Suresh Bindra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaadhal Ondraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Ondraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Rendaanadheyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Rendaanadheyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Rendaagi Thundaanadheyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Rendaagi Thundaanadheyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalgal Thadumarii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal Thadumarii"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadam Maari Ponadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadam Maari Ponadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril En Kaadhalgal Pogudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril En Kaadhalgal Pogudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irandu Kandal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandu Kandal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhullaeyyy Ondraiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhullaeyyy Ondraiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilakaaa Sonnaaalll
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilakaaa Sonnaaalll"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyalavillaiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyalavillaiii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoduuuu Irundhavalll
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoduuuu Irundhavalll"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippodhu Illayaeeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu Illayaeeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingayaeeee Irundhaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingayaeeee Irundhaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Illaayeeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Illaayeeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoduuuu Irundhavalll
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoduuuu Irundhavalll"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippodhu Illayaeeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu Illayaeeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irugiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irugiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkudheyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkudheyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Piriya Pogiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Piriya Pogiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Oru Murai Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Oru Murai Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenaikkavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenaikkavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Udaiyakoodidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Udaiyakoodidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Oru Murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Oru Murai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraikkavilayeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraikkavilayeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Poigal Paesuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Poigal Paesuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Oru Murai Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Oru Murai Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenaikavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenaikavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Mudindhu Poi Vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Mudindhu Poi Vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Oru Murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Oru Murai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thonavillaiyeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thonavillaiyeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arthangal Thaedi Pogaadheyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arthangal Thaedi Pogaadheyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Alindhu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Alindhu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittuuu Pogaadheyyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittuuu Pogaadheyyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirum Uraindhu Pogummmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirum Uraindhu Pogummmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoduuuu Irundhavalll
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoduuuu Irundhavalll"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippodhu Illayaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu Illayaeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingayaeeee Irundhaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingayaeeee Irundhaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Illaayeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Illaayeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoduuuu Irundhavallll
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoduuuu Irundhavallll"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippodhu Illayaeeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippodhu Illayaeeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irugiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irugiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkudheyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkudheyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaai Malarvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai Malarvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pol Mudivadhu Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pol Mudivadhu Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engo Therivadhu Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo Therivadhu Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraivadhu Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraivadhu Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaai Malarvadhu Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai Malarvadhu Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivadhu Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivadhu Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Engo Therivadhu Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo Therivadhu Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraivadhu Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraivadhu Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varundha Koodathu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varundha Koodathu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Valigal Vendama Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigal Vendama Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Podhum Ini Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Podhum Ini Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Sandhika Vendama Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Sandhika Vendama Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Varundha Koodathu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varundha Koodathu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Ondraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Ondraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Rendaanadheyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Rendaanadheyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal"/>
</div>
</pre>
