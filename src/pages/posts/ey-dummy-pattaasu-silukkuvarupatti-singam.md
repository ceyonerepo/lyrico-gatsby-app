---
title: "ey dummy pattaasu song lyrics"
album: "Silukkuvarupatti Singam"
artist: "Leon James"
lyricist: "Madhan Karky"
director: "Chella Ayyavu"
path: "/albums/silukkuvarupatti-singam-lyrics"
song: "Ey Dummy Pattaasu"
image: ../../images/albumart/silukkuvarupatti-singam.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/srwlMebx-hs"
type: "happy"
singers:
  - Bamba Bakya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ei mudinja pudi mudinja pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei mudinja pudi mudinja pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja pudi mudinja pudi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja pudi mudinja pudi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Re eh eh eh eh eh eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re eh eh eh eh eh eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei mudinja pudi mudinja pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei mudinja pudi mudinja pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja pudi mudinja pudi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja pudi mudinja pudi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Re eh eh eh eh eh eh heii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re eh eh eh eh eh eh heii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan balli muttaai thaaren dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan balli muttaai thaaren dei"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sappikittae odu deii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sappikittae odu deii"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan peepee senji thaaren dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan peepee senji thaaren dei"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oothikittae aadu deii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oothikittae aadu deii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevar sanda rekkla race-uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevar sanda rekkla race-uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaman kaaran maasu maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman kaaran maasu maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Machaana paar jalli kattaa thullikittae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machaana paar jalli kattaa thullikittae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei dummy pattaasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei dummy pattaasae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei mudinja pudi mudinja pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei mudinja pudi mudinja pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja pudi mudinja pudi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja pudi mudinja pudi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Re eh eh eh eh eh eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re eh eh eh eh eh eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei mudinja pudi mudinja pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei mudinja pudi mudinja pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja pudi mudinja pudi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja pudi mudinja pudi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Re eh eh eh eh eh eh heii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re eh eh eh eh eh eh heii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei minuminukkum goli ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei minuminukkum goli ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thurathi vandhaa kozhi ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thurathi vandhaa kozhi ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pandh erinja kohli ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pandh erinja kohli ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudchaana gaali ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudchaana gaali ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paruppu vada theenthuchunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paruppu vada theenthuchunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan keera vada vaangi thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan keera vada vaangi thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thinuputtu thoonghu machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thinuputtu thoonghu machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan konjam thoongiyaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan konjam thoongiyaaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei mudinja pudi mudinja pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei mudinja pudi mudinja pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja pudi mudinja pudi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja pudi mudinja pudi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Re eh eh eh eh eh eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re eh eh eh eh eh eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei mudinja pudi mudinja pudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei mudinja pudi mudinja pudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja pudi mudinja pudi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja pudi mudinja pudi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Re eh eh eh eh eh eh heii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Re eh eh eh eh eh eh heii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan balli muttaai thaaren dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan balli muttaai thaaren dei"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sappikittae odu deii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sappikittae odu deii"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan peepee senji thaaren dei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan peepee senji thaaren dei"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oothikittae aadu deii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oothikittae aadu deii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sevar sanda rekkla race-uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevar sanda rekkla race-uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaman kaaran maasu maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman kaaran maasu maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Machaana paar jalli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machaana paar jalli "/>
</div>
<div class="lyrico-lyrics-wrapper">kattaa thullikittae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattaa thullikittae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei dummy pattaasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei dummy pattaasae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei dummy pattaasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei dummy pattaasae"/>
</div>
</pre>
