---
title: "yakka chakka song lyrics"
album: "Margandeyan"
artist: "Sundar C Babu"
lyricist: "Kavivarman"
director: "FEFSI Vijayan"
path: "/albums/markandeyan-lyrics"
song: "Yakka Chakka"
image: ../../images/albumart/markandeyan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XAUELod_JWA"
type: "happy"
singers:
  - Kavivarman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Charugu Charugu Charugu Charugu Charugu Charugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charugu Charugu Charugu Charugu Charugu Charugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Charugu Charugu Charugu Charugu Charugu Charugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charugu Charugu Charugu Charugu Charugu Charugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkachekka Ekkachekka Macham Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkachekka Ekkachekka Macham Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Vachaa Ekkuthappa Aagum Kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vachaa Ekkuthappa Aagum Kanakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkachekka Ekkachekka Macham Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkachekka Ekkachekka Macham Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Vachaa Ekkuthappa Aagum Kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vachaa Ekkuthappa Aagum Kanakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu Podu Thaa Paalu Edukkanum Nooru Thandaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu Podu Thaa Paalu Edukkanum Nooru Thandaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah Thaekupola Un Thoalu Redi Redi Intha Aandaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah Thaekupola Un Thoalu Redi Redi Intha Aandaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Charugu Charugu Charugu Charugu Charugu Charugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charugu Charugu Charugu Charugu Charugu Charugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Charugu Charugu Charugu Charugu Charugu Charugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charugu Charugu Charugu Charugu Charugu Charugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkachekka Ekkachekka Macham Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkachekka Ekkachekka Macham Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Vachaa Ekkuthappa Aagum Kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vachaa Ekkuthappa Aagum Kanakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugathula Nelaa Thunda Otti Vaikaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugathula Nelaa Thunda Otti Vaikaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkura Pura Renda Pothi Vaikkaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkura Pura Renda Pothi Vaikkaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhi Vizhum Kannathula Goli Aadaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhi Vizhum Kannathula Goli Aadaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Imaikkira Nerathula Goal'la Podaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Imaikkira Nerathula Goal'la Podaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuda Chuda Thaenu Kodukkura Un Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuda Chuda Thaenu Kodukkura Un Aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhakkithu Kaaman Valaivethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhakkithu Kaaman Valaivethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Athirasam Thinna Avasaram Ènna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athirasam Thinna Avasaram Ènna"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Thøøngura Thaamarai Neer Tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Thøøngura Thaamarai Neer Tharavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Charugu Charugu Charugu Charugu Charugu Charugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charugu Charugu Charugu Charugu Charugu Charugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Charugu Charugu Charugu Charugu Charugu Charugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charugu Charugu Charugu Charugu Charugu Charugu"/>
</div>
È<div class="lyrico-lyrics-wrapper">kkachekka Èkkachekka Macham Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kkachekka Èkkachekka Macham Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Vachaa Èkkuthappa Aagum Kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vachaa Èkkuthappa Aagum Kanakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudikiravarai Padappadakkuthu Ulle Ulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudikiravarai Padappadakkuthu Ulle Ulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagiyavudan Thavithavikkuthu Pulle Pulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagiyavudan Thavithavikkuthu Pulle Pulle"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi Kødukkura Pazhamirukkuthu Alla Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Kødukkura Pazhamirukkuthu Alla Alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadi Kadichathum Kirugirukkuthu Mella Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadi Kadichathum Kirugirukkuthu Mella Mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilu Kilu Thapputhandaa Pannaama Šuthaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilu Kilu Thapputhandaa Pannaama Šuthaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruttula Macham Ènnaathey Hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttula Macham Ènnaathey Hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Uriyila Paala Vechaa Pøønathaan Nikkaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uriyila Paala Vechaa Pøønathaan Nikkaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Thuli Micham Vaikkaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thuli Micham Vaikkaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Charugu Charugu Charugu Charugu Charugu Charugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charugu Charugu Charugu Charugu Charugu Charugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Charugu Charugu Charugu Charugu Charugu Charugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charugu Charugu Charugu Charugu Charugu Charugu"/>
</div>
È<div class="lyrico-lyrics-wrapper">kkachekka Èkkachekka Macham Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kkachekka Èkkachekka Macham Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Vachaa Èkkuthappa Aagum Kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vachaa Èkkuthappa Aagum Kanakku"/>
</div>
È<div class="lyrico-lyrics-wrapper">kkachekka Èkkachekka Macham Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kkachekka Èkkachekka Macham Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Vachaa Èkkuthappa Aagum Kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vachaa Èkkuthappa Aagum Kanakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pødu Pødu Thaa Paalu Èdukkanum Nøøru Thandaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pødu Pødu Thaa Paalu Èdukkanum Nøøru Thandaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah Thaekupøla Un Thøalu Redi Redi Intha Aandaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah Thaekupøla Un Thøalu Redi Redi Intha Aandaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
È<div class="lyrico-lyrics-wrapper">kkachekka Èkkachekka Macham Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kkachekka Èkkachekka Macham Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Vachaa Èkkuthappa Aagum Kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vachaa Èkkuthappa Aagum Kanakku"/>
</div>
</pre>
