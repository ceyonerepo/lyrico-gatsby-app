---
title: "phir shuru song lyrics"
album: "Maharshi"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Vamshi Paidipally"
path: "/albums/maharshi-lyrics"
song: "Phir Shuru"
image: ../../images/albumart/maharshi.jpg
date: 2019-05-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/a6PhZL44898"
type: "mass"
singers:
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yagasi pade kerataanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yagasi pade kerataanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapenaa yavadyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapenaa yavadyna"/>
</div>
<div class="lyrico-lyrics-wrapper">Merisi padey pidugulaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merisi padey pidugulaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapenaa yavadyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapenaa yavadyna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chadharangamlo chanakyudikey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadharangamlo chanakyudikey"/>
</div>
<div class="lyrico-lyrics-wrapper">Otami undha yenadyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otami undha yenadyna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhuradugese aalochanake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhuradugese aalochanake"/>
</div>
<div class="lyrico-lyrics-wrapper">Venakadugundha yennatikaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venakadugundha yennatikaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudigaalini kosi dhaarini theesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudigaalini kosi dhaarini theesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhuke pranam la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuke pranam la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phir shuru chal guru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir shuru chal guru"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir shuru chal guru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir shuru chal guru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velugakkada ledhani cheppe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velugakkada ledhani cheppe"/>
</div>
<div class="lyrico-lyrics-wrapper">Maate raa chikati ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate raa chikati ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisi annadhi lene ledhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisi annadhi lene ledhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraatam thodai unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraatam thodai unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Poratam mari nee vente
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poratam mari nee vente"/>
</div>
<div class="lyrico-lyrics-wrapper">Otamike chote ledhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otamike chote ledhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinukula nadumana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinukula nadumana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadavaka saage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadavaka saage"/>
</div>
<div class="lyrico-lyrics-wrapper">Arjuna vegam kshanamaagenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arjuna vegam kshanamaagenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalabadi porey nilakada theere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalabadi porey nilakada theere"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupani chaatelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupani chaatelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phir shuru chal guru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir shuru chal guru"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir shuru chal guru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir shuru chal guru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minuguru purugulu anuvanthaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minuguru purugulu anuvanthaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Adivini saitham veligencheiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivini saitham veligencheiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Chali cheemalu chiru chiguranthaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chali cheemalu chiru chiguranthaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalasarpamune gelichaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalasarpamune gelichaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalu renuvulanthe unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalu renuvulanthe unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningina rangulu ponginchaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningina rangulu ponginchaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkalu inthe pisaranthaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalu inthe pisaranthaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhikkulane shasinchaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhikkulane shasinchaiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kommala chaatuna koyila paate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommala chaatuna koyila paate"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekuva baataku pilupe kaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekuva baataku pilupe kaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni pidikili loni alikidi jagathiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni pidikili loni alikidi jagathiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Melakuva paatamla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melakuva paatamla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phir shuru chal guru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir shuru chal guru"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir shuru chal guru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir shuru chal guru"/>
</div>
</pre>
