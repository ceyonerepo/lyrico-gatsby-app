---
title: "kulirudha pulla song lyrics"
album: "Oththa Seruppu Size 7"
artist: "Santhosh Narayanan"
lyricist: "Vivek"
director: "R. Parthiepan"
path: "/albums/oththa-seruppu-size-7-lyrics"
song: "Kulirudha Pulla"
image: ../../images/albumart/oththa-seruppu-size-7.jpg
date: 2019-09-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ybfK2wb0ELo"
type: "sad"
singers:
  - Sid Sriram
  - Sangeetha Karuppiah
  - R. Parthiepan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oththa Usuru Unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Usuru Unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Niththam Kasiyum Kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niththam Kasiyum Kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanandh Thaniyaa Thoongura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanandh Thaniyaa Thoongura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulurudha Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulurudha Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannuppattu Keerumaennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannuppattu Keerumaennu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachirunthaen Maaru Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachirunthaen Maaru Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Keezhapatta Paatham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keezhapatta Paatham "/>
</div>
<div class="lyrico-lyrics-wrapper">Yeadhum Uruthutha Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeadhum Uruthutha Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaththadichaa Bayappaduva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththadichaa Bayappaduva "/>
</div>
<div class="lyrico-lyrics-wrapper">Maan Valaiyil Agappaduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maan Valaiyil Agappaduva"/>
</div>
<div class="lyrico-lyrics-wrapper">Poththi Vachi Paathukitan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poththi Vachi Paathukitan "/>
</div>
<div class="lyrico-lyrics-wrapper">Purinjitha Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purinjitha Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasang Konjam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasang Konjam "/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichiraen Kaathumadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichiraen Kaathumadal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadichiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadichiraen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unchirippa Gnyabagathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unchirippa Gnyabagathil "/>
</div>
<div class="lyrico-lyrics-wrapper">Adachikiraen Adhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adachikiraen Adhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattum Thirumbi Vaa Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattum Thirumbi Vaa Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbi Vaa Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbi Vaa Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkathula Ravikkai Thachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkathula Ravikkai Thachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththathula Jimikki Vachaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththathula Jimikki Vachaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhatho Poongothayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhatho Poongothayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chandhirana Tholuruchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandhirana Tholuruchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa Pakkam Thodu Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Pakkam Thodu Vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhatho Poongothayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhatho Poongothayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagala Paakadha Koondhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagala Paakadha Koondhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethi Chutti Muthamiduthay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethi Chutti Muthamiduthay"/>
</div>
<div class="lyrico-lyrics-wrapper">Irava Paakadha Dhaegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irava Paakadha Dhaegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Thugalena Minnal Vidudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Thugalena Minnal Vidudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Pota Verva 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Pota Verva "/>
</div>
<div class="lyrico-lyrics-wrapper">Magarandhame Mazhayaacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magarandhame Mazhayaacho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kochukittu Nee Nadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kochukittu Nee Nadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mochikittu Naan Kedandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mochikittu Naan Kedandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaachu Thembaavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaachu Thembaavani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Konjam Pesavittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Konjam Pesavittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thean Theratti Sethu Vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean Theratti Sethu Vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal Aachu Thembaavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal Aachu Thembaavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhuthu Kottoram Yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuthu Kottoram Yeri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagula Vandhu Vizhu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagula Vandhu Vizhu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavidha Maarbodu Modhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidha Maarbodu Modhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathu Valaivula Pithu Pidi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu Valaivula Pithu Pidi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Paatha Vaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paatha Vaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhuradiyaa Vela Poche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhuradiyaa Vela Poche"/>
</div>
</pre>
