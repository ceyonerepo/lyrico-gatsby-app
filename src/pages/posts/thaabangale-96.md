---
title: "thaabangale song lyrics"
album: "96"
artist: "Govind Vasantha"
lyricist: "Uma Devi"
director: "C Premkumar"
path: "/albums/96-lyrics"
song: "Thaabangale"
image: ../../images/albumart/96.jpg
date: 2018-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/I6KBcn3gvw0"
type: "melody"
singers:
  - Chinmayi
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaabangalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaabangalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Roobangalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roobangalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthae thoduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthae thoduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaginai chuduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaginai chuduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayagavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayagavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaattuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaattuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi vazhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi vazhi "/>
</div>
<div class="lyrico-lyrics-wrapper">mozhi vazhiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhi vazhiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathayai varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathayai varuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaabangalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaabangalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Roobangalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roobangalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthae thoduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthae thoduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaginai chuduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaginai chuduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayagavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayagavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaattuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaattuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi vazhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi vazhi "/>
</div>
<div class="lyrico-lyrics-wrapper">mozhi vazhiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhi vazhiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathayai varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathayai varuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam iravin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam iravin "/>
</div>
<div class="lyrico-lyrics-wrapper">puravi aagatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puravi aagatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Athae kanaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athae kanaa "/>
</div>
<div class="lyrico-lyrics-wrapper">athae vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athae vinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam nazhuvi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam nazhuvi "/>
</div>
<div class="lyrico-lyrics-wrapper">thazhuvi aadatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thazhuvi aadatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Athae nila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athae nila "/>
</div>
<div class="lyrico-lyrics-wrapper">aruginil varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aruginil varuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaabangalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaabangalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Roobangalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roobangalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthae thoduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthae thoduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaginai chuduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaginai chuduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayagavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayagavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaattuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaattuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi vazhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi vazhi "/>
</div>
<div class="lyrico-lyrics-wrapper">mozhi vazhiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhi vazhiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathayai varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathayai varuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan nanaithidum theeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nanaithidum theeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Peiyum nila neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peiyum nila neeya"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan anainthiduvena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan anainthiduvena"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalabananai thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalabananai thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal kanaakkal thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kanaakkal thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera ula naana podhatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera ula naana podhatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam vinaakkal thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam vinaakkal thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruginil vara manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginil vara manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugithan karaiyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugithan karaiyuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaabangalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaabangalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Roobangalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roobangalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paduthae thoduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduthae thoduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaginai chuduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaginai chuduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayagavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayagavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaattuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaattuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi vazhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi vazhi "/>
</div>
<div class="lyrico-lyrics-wrapper">mozhi vazhiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhi vazhiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathayai varuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathayai varuthae"/>
</div>
</pre>
