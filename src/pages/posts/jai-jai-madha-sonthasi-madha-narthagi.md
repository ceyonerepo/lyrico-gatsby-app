---
title: "jai jai madha sonthasi madha song lyrics"
album: "Narthagi"
artist: "G.V. Prakash Kumar"
lyricist: "Na. Muthukumar"
director: "G. Vijayapadma"
path: "/albums/narthagi-lyrics"
song: "Jai Jai Madha Sonthasi Madha"
image: ../../images/albumart/narthagi.jpg
date: 2011-05-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tL4ALGNY92E"
type: "happy"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poi vaa magalae poi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi vaa magalae poi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhupennaaga urumaari nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhupennaaga urumaari nee vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi vaa magalae poi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi vaa magalae poi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam maathaavin varam vaangi nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam maathaavin varam vaangi nee vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iththanai naalaai sumandhu vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iththanai naalaai sumandhu vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumaiyai irakki vaiththuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumaiyai irakki vaiththuvidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eththanai kanavugal neekkandaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththanai kanavugal neekkandaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam palikkum kilambividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam palikkum kilambividu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayammaa thunai irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayammaa thunai irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhimaargal udanirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhimaargal udanirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu piravi eduthuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu piravi eduthuvidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poopola malarndhuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poopola malarndhuvidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangam vairam chellamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangam vairam chellamena"/>
</div>
<div class="lyrico-lyrics-wrapper">Petra thaai unai azhaithiruppaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petra thaai unai azhaithiruppaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Angam maari vazhndhavudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angam maari vazhndhavudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalae kooda veruthirupaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalae kooda veruthirupaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhiyil iragaai midhakkum vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyil iragaai midhakkum vali"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyil irundhaal theriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyil irundhaal theriyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalai veruthu sumakkum vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalai veruthu sumakkum vali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil yaarukkum puriyaadhu….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil yaarukkum puriyaadhu…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sittukkuruvi polae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sittukkuruvi polae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu viduthalai aagindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu viduthalai aagindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutrum boomiyellaamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutrum boomiyellaamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhammaakka pogindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondhammaakka pogindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee neeyaagiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee neeyaagiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi ner aagivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi ner aagivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennae pennae kaathirundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae pennae kaathirundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam vandhadhu gummiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam vandhadhu gummiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam muzhuvadhum vetkathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam muzhuvadhum vetkathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaigal pesa paarkkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaigal pesa paarkkudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkum uravugal irukkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum uravugal irukkudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unpol ellaam sirikkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unpol ellaam sirikkudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Irakkumvaraikkum indha nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakkumvaraikkum indha nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil irukkum nallabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil irukkum nallabadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbam inbam inbamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam inbam inbamena"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbathilae moozhgividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbathilae moozhgividu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum konjam santhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum konjam santhosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthaal kooda vaangividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthaal kooda vaangividu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee neeyaagiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee neeyaagiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa ner aagi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa ner aagi vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poi vaa magalae poi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi vaa magalae poi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhupennaaga urumaari nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhupennaaga urumaari nee vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi vaa magalae poi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi vaa magalae poi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam maathaavin varam vaangi nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam maathaavin varam vaangi nee vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poi vaa magalae poi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi vaa magalae poi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhupennaaga urumaari nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhupennaaga urumaari nee vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi vaa magalae poi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi vaa magalae poi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam maathaavin varam vaangi nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam maathaavin varam vaangi nee vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iththanai naalaai sumandhu vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iththanai naalaai sumandhu vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumaiyai irakki vaiththuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumaiyai irakki vaiththuvidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eththanai kanavugal neekkandaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththanai kanavugal neekkandaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam palikkum kilambividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam palikkum kilambividu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayammaa thunai irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayammaa thunai irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhimaargal udanirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhimaargal udanirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu piravi eduthuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu piravi eduthuvidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poopola malarndhuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poopola malarndhuvidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poi vaa magalae poi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi vaa magalae poi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhupennaaga urumaari nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhupennaaga urumaari nee vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi vaa magalae poi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi vaa magalae poi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam maathaavin varam vaangi nee vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam maathaavin varam vaangi nee vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai jai maathaa sandhoshi maathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai jai maathaa sandhoshi maathaa"/>
</div>
</pre>
