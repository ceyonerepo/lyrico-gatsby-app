---
title: "kondaduda song lyrics"
album: "50 Roova"
artist: "VT. Bharathy - VT.Monish"
lyricist: "G. Panneerselvam"
director: "G. Panneerselvam"
path: "/albums/50-roova-lyrics"
song: "Kondaduda"
image: ../../images/albumart/50-roova.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2yPRdcOCiQw"
type: "happy"
singers:
  - Akhil
  - Mano Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kondadu da koothadu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondadu da koothadu da"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnapadi senjomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnapadi senjomada"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyathu nu sonnanga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyathu nu sonnanga da"/>
</div>
<div class="lyrico-lyrics-wrapper">mooku odanchu ninnanga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mooku odanchu ninnanga da"/>
</div>
<div class="lyrico-lyrics-wrapper">namma kotaiyila kodi yethuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma kotaiyila kodi yethuda"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba aatathula sruthi yethuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba aatathula sruthi yethuda"/>
</div>
<div class="lyrico-lyrics-wrapper">naadi narambula than veri yethuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadi narambula than veri yethuda"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnome sonnome sonnatha senjome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnome sonnome sonnatha senjome"/>
</div>
<div class="lyrico-lyrics-wrapper">verum koochal velaiku uthavathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verum koochal velaiku uthavathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethir neechal pottu karai yeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethir neechal pottu karai yeru"/>
</div>
<div class="lyrico-lyrics-wrapper">annaru sonnathu mannathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaru sonnathu mannathu"/>
</div>
<div class="lyrico-lyrics-wrapper">atha thooki keela eri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha thooki keela eri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kondadu da koothadu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondadu da koothadu da"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnapadi senjomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnapadi senjomada"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyathu nu sonnanga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyathu nu sonnanga da"/>
</div>
<div class="lyrico-lyrics-wrapper">mooku odanchu ninnanga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mooku odanchu ninnanga da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aathi nee enna thanga vayala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathi nee enna thanga vayala"/>
</div>
<div class="lyrico-lyrics-wrapper">adangi pogatha vanga puyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangi pogatha vanga puyala"/>
</div>
<div class="lyrico-lyrics-wrapper">aalam mogathe manthi railla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalam mogathe manthi railla"/>
</div>
<div class="lyrico-lyrics-wrapper">nan vendhu ponene off oil ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan vendhu ponene off oil ah"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna kannala mutta varaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna kannala mutta varaya"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli nikkama kitta variya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli nikkama kitta variya"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiya korthutu sutha variya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiya korthutu sutha variya"/>
</div>
<div class="lyrico-lyrics-wrapper">enakaga enakaga unna tharaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakaga enakaga unna tharaya"/>
</div>
<div class="lyrico-lyrics-wrapper">aalathula ne than nangooram patu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalathula ne than nangooram patu"/>
</div>
<div class="lyrico-lyrics-wrapper">asaiyama yen nikka vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asaiyama yen nikka vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">adimanasa ellam koodaram pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adimanasa ellam koodaram pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye ne than kutha vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye ne than kutha vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">vanavillin thesathuke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanavillin thesathuke "/>
</div>
<div class="lyrico-lyrics-wrapper">sonthagari ne than pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonthagari ne than pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">vannangala alli alli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannangala alli alli "/>
</div>
<div class="lyrico-lyrics-wrapper">koturaya nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koturaya nenjukulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kondadu da koothadu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondadu da koothadu da"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnapadi senjomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnapadi senjomada"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyathu nu sonnanga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyathu nu sonnanga da"/>
</div>
<div class="lyrico-lyrics-wrapper">mooku odanchu ninnanga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mooku odanchu ninnanga da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aale illama ticket edupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aale illama ticket edupom"/>
</div>
<div class="lyrico-lyrics-wrapper">ball eh podama vicket edupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ball eh podama vicket edupom"/>
</div>
<div class="lyrico-lyrics-wrapper">noole illama pattam viduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noole illama pattam viduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">innum than innum than kekuraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum than innum than kekuraya"/>
</div>
<div class="lyrico-lyrics-wrapper">naala rendala nanga kalipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naala rendala nanga kalipom"/>
</div>
<div class="lyrico-lyrics-wrapper">katha kaiyala nanga pidipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katha kaiyala nanga pidipom"/>
</div>
<div class="lyrico-lyrics-wrapper">engum epothum nanga jeyipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum epothum nanga jeyipom"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo ne ipo ne pakuraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo ne ipo ne pakuraya"/>
</div>
<div class="lyrico-lyrics-wrapper">adikira kathu epothum namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adikira kathu epothum namma"/>
</div>
<div class="lyrico-lyrics-wrapper">veeta suthi adikuthuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeta suthi adikuthuda"/>
</div>
<div class="lyrico-lyrics-wrapper">ala than vaika vantha kavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ala than vaika vantha kavala"/>
</div>
<div class="lyrico-lyrics-wrapper">namaloda senthu sirikuthu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namaloda senthu sirikuthu da"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha kuri vachathu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha kuri vachathu than"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam kooda thapa villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam kooda thapa villa"/>
</div>
<div class="lyrico-lyrics-wrapper">natpu kunu senjoim pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpu kunu senjoim pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">thappum kooda thape illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappum kooda thape illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kondadu da koothadu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondadu da koothadu da"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnapadi senjomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnapadi senjomada"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyathu nu sonnanga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyathu nu sonnanga da"/>
</div>
<div class="lyrico-lyrics-wrapper">mooku odanchu ninnanga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mooku odanchu ninnanga da"/>
</div>
<div class="lyrico-lyrics-wrapper">namma kotaiyila kodi yethuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma kotaiyila kodi yethuda"/>
</div>
<div class="lyrico-lyrics-wrapper">nanba aatathula sruthi yethuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanba aatathula sruthi yethuda"/>
</div>
<div class="lyrico-lyrics-wrapper">naadi narambula than veri yethuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadi narambula than veri yethuda"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnome sonnome sonnatha senjome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnome sonnome sonnatha senjome"/>
</div>
<div class="lyrico-lyrics-wrapper">verum koochal velaiku uthavathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verum koochal velaiku uthavathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethir neechal pottu karai yeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethir neechal pottu karai yeru"/>
</div>
<div class="lyrico-lyrics-wrapper">annaru sonnathu mannathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaru sonnathu mannathu"/>
</div>
<div class="lyrico-lyrics-wrapper">atha thooki keela eri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha thooki keela eri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kondadu da koothadu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondadu da koothadu da"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnapadi senjomada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnapadi senjomada"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyathu nu sonnanga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyathu nu sonnanga da"/>
</div>
<div class="lyrico-lyrics-wrapper">mooku odanchu ninnanga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mooku odanchu ninnanga da"/>
</div>
</pre>
