---
title: "leharaayi song lyrics"
album: "Most Eligible Bachelor"
artist: "Gopi Sundar"
lyricist: "Sri Mani"
director: "Bommarillu Bhaskar"
path: "/albums/most-eligible-bachelor-lyrics"
song: "Leharaayi"
image: ../../images/albumart/most-eligible-bachelor.jpg
date: 2021-10-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/J1KpuR2Khz8"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Leharaayi Leharaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leharaayi Leharaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Le Le Lele Le Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Le Le Lele Le Le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Leharaayi Leharaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leharaayi Leharaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Vechchanayye Oohalegiraayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Vechchanayye Oohalegiraayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lehar-Aayi Lehar-Aayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lehar-Aayi Lehar-Aayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Goru Vechchanaina Oosuladhiraayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goru Vechchanaina Oosuladhiraayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inni Naallu Entha Entha Vechaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni Naallu Entha Entha Vechaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaloney Daagi Unna Ammaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaloney Daagi Unna Ammaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonthamalley Cheruthuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonthamalley Cheruthuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranamanthaa Cheppaleni Haayi Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranamanthaa Cheppaleni Haayi Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Leharaayi Leharaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leharaayi Leharaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Vechchanayye Oohalegiraayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Vechchanayye Oohalegiraayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Leharaayi Leharaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leharaayi Leharaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Goru Vechchanaina Oosuladhiraayi Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goru Vechchanaina Oosuladhiraayi Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roju Chekkilitho Siggula Thaguvaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roju Chekkilitho Siggula Thaguvaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojaa Pedhavulatho Mudhdhula Godavaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojaa Pedhavulatho Mudhdhula Godavaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanta Gadhilo Mantalannee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanta Gadhilo Mantalannee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontilokey Omputhuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontilokey Omputhuntey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mari Ninnaa Monnaa Ontiga Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Ninnaa Monnaa Ontiga Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Eedey Nedey Leharaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedey Nedey Leharaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Leharaayi Leharaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leharaayi Leharaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Vechchanayye Oohalegiraayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Vechchanayye Oohalegiraayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Leharaayi Leharaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leharaayi Leharaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Goru Vechchanaina Oosuladhiraayi Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goru Vechchanaina Oosuladhiraayi Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velapaalalaney Marichey Sarasaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velapaalalaney Marichey Sarasaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedhi Vaaraaley Cheripey Cherasaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedhi Vaaraaley Cheripey Cherasaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Chanuvu Koncham Penchukuntoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chanuvu Koncham Penchukuntoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvu Baruvey Panchukuntoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvu Baruvey Panchukuntoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manalokam Maikam Ekam Avuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalokam Maikam Ekam Avuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekaanthaaley Leharaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekaanthaaley Leharaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Leharaayi Leharaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leharaayi Leharaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Vechchanayye Oohalegiraayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Vechchanayye Oohalegiraayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Leharaayi Leharaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leharaayi Leharaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Goru Vechchanaina Oosuladhiraayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goru Vechchanaina Oosuladhiraayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inni Naallu Entha Entha Vechaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni Naallu Entha Entha Vechaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaloney Daagi Unna Ammaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaloney Daagi Unna Ammaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonthamalley Cheruthuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonthamalley Cheruthuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranamanthaa Cheppaleni Haayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranamanthaa Cheppaleni Haayi"/>
</div>
</pre>
