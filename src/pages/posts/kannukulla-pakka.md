---
title: "kannukulla song lyrics"
album: "Pakka"
artist: "C. Sathya"
lyricist: "Yugabharathi"
director: "S.S. Surya"
path: "/albums/pakka-song-lyrics"
song: "Kannukulla"
image: ../../images/albumart/pakka.jpg
date: 2018-04-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/d8jNWbtfKuU"
type: "sad"
singers:
  - Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En kannukulla unna vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannukulla unna vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Novaama kaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Novaama kaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan avinji ponathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan avinji ponathenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooraarum paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooraarum paakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla thanni kinaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla thanni kinaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu nodiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu nodiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanji pattu thoonthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanji pattu thoonthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">En madiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En madiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazha vandha thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha vandha thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum vazhiyilaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum vazhiyilaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Verarundhu vizhunthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verarundhu vizhunthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kannukulla unna vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannukulla unna vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Novaama kaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Novaama kaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan avinji ponathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan avinji ponathenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooraarum paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooraarum paakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasayila un mugatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasayila un mugatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Arumba paathiruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumba paathiruppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesayila dhevadhaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesayila dhevadhaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumba paathiruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumba paathiruppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odi varum un nadaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi varum un nadaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellalaya paathiruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellalaya paathiruppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee utkaarum perazhagil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee utkaarum perazhagil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooviyatha paathiruppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooviyatha paathiruppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athanaiyum paarthavizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaiyum paarthavizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai irunthum kaal irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai irunthum kaal irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochi vida thonalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochi vida thonalayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey en kannukulla unna vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey en kannukulla unna vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Novaama kaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Novaama kaakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvaiyila pesiyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyila pesiyadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachirukken patthirama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachirukken patthirama"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathula nikkayilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathula nikkayilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkala nee sathiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkala nee sathiyama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna katti kaattulathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna katti kaattulathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittutiyae appadiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittutiyae appadiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu patta mannulathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu patta mannulathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanjathenna uthamiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanjathenna uthamiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathu jenmam vaazha thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu jenmam vaazha thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee porantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee porantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paala pola thirinji yendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paala pola thirinji yendi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattunu ver arundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu ver arundha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukulla unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh kannukulla unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh kannukulla unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukulla unna vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla unna vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Novaama kaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Novaama kaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan avinji ponathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan avinji ponathenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooraarum paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooraarum paakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla thanni kinaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla thanni kinaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu nodiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu nodiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanji pattu thoonthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanji pattu thoonthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">En madiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En madiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukulla aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla aaa aaa"/>
</div>
</pre>
