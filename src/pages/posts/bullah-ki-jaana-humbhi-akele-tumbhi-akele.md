---
title: "bullah ki jaana song lyrics"
album: "Hum Bhi Akele Tum Bhi Akele"
artist: "Oni-Adil"
lyricist: "	Bulleh Shah"
director: "Harish Vyas"
path: "/albums/humbhi-akele-tumbhi-akele-lyrics"
song: "Bullah Ki Jaana"
image: ../../images/albumart/humbhi-akele-tumbhi-akele.jpg
date: 2020-02-14
lang: hindi
youtubeLink: "https://www.youtube.com/embed/cofWH_QIQyE"
type: "Love"
singers:
  - Adil Rasheed
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Na main momin vich masita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na main momin vich masita"/>
</div>
<div class="lyrico-lyrics-wrapper">Na main vich ku fardiyan rita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na main vich ku fardiyan rita"/>
</div>
<div class="lyrico-lyrics-wrapper">Na main pakan vich palita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na main pakan vich palita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na vich shaadi nagaam naki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na vich shaadi nagaam naki"/>
</div>
<div class="lyrico-lyrics-wrapper">Na main vich paliki paaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na main vich paliki paaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Na main aapi naa main khaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na main aapi naa main khaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Na main moosa na firon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na main moosa na firon"/>
</div>
<div class="lyrico-lyrics-wrapper">Na main moosa na firon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na main moosa na firon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bullah ki jaana mein kaun?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullah ki jaana mein kaun?"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullah ki jaana mein kaun?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullah ki jaana mein kaun?"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullah ki jaana mein kaun?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullah ki jaana mein kaun?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na main arbi na lahori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na main arbi na lahori"/>
</div>
<div class="lyrico-lyrics-wrapper">Na main hindi shehar na gouri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na main hindi shehar na gouri"/>
</div>
<div class="lyrico-lyrics-wrapper">Na hindu na turk pisoyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na hindu na turk pisoyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Na main aatish na mein paun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na main aatish na mein paun"/>
</div>
<div class="lyrico-lyrics-wrapper">Na main aatish na main paun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na main aatish na main paun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bullah ki jaana mein kaun?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullah ki jaana mein kaun?"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullah ki jaana mein kaun?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullah ki jaana mein kaun?"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullah ki jaana mein kaun?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullah ki jaana mein kaun?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na main andar vaid kitaban
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na main andar vaid kitaban"/>
</div>
<div class="lyrico-lyrics-wrapper">Na vich bangaan naa sharaban
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na vich bangaan naa sharaban"/>
</div>
<div class="lyrico-lyrics-wrapper">Na vich randa masat kharaban
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na vich randa masat kharaban"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa vich jagan naa vich soun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa vich jagan naa vich soun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avval aakhir aap nu jana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avval aakhir aap nu jana"/>
</div>
<div class="lyrico-lyrics-wrapper">Na koyi dooja hor pahuchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na koyi dooja hor pahuchana"/>
</div>
<div class="lyrico-lyrics-wrapper">Maithon hor naa koyi siyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maithon hor naa koyi siyana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bullah ki jaana mein kaun?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullah ki jaana mein kaun?"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullah ki jaana mein kaun?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullah ki jaana mein kaun?"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullah ki jaana mein kaun?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullah ki jaana mein kaun?"/>
</div>
</pre>
