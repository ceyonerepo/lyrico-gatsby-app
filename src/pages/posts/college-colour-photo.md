---
title: "college song lyrics"
album: "Colour Photo"
artist: "Kaala Bhairava"
lyricist: "Sai Kiran"
director: "Vijay Kumar Konda"
path: "/albums/colour-photo-lyrics"
song: "College"
image: ../../images/albumart/colour-photo.jpg
date: 2020-10-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wFjFQqzj1Kw"
type: "happy"
singers:
  - Hema Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Cheppalantey chala kastam gaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppalantey chala kastam gaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenenno saradhalanni andanga eduroochayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenenno saradhalanni andanga eduroochayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anadhale panche allarlanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anadhale panche allarlanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Andinche snehalanni ee chote modhalayyayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andinche snehalanni ee chote modhalayyayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One by two tho dosthilello ichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One by two tho dosthilello ichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashallo cheyi andhinchi vennantey thodochindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashallo cheyi andhinchi vennantey thodochindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu enno chirunavvulni penchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu enno chirunavvulni penchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohalaku rekkalanichi nijamalle chupinchindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohalaku rekkalanichi nijamalle chupinchindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidi vidi adugulu vidi vidi manusulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidi vidi adugulu vidi vidi manusulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okatiga kalisina kathalivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatiga kalisina kathalivi"/>
</div>
<div class="lyrico-lyrics-wrapper">O oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenthenthaina aakashamlo thaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthenthaina aakashamlo thaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusuntundha kallara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusuntundha kallara"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi nimishanni manalaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi nimishanni manalaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Itittey allese bandham lera evarain kadhantara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itittey allese bandham lera evarain kadhantara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee sneham theerinthera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee sneham theerinthera"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidi vidi adugulu vidi vidi manusulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidi vidi adugulu vidi vidi manusulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okatiga kalisina kathalivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatiga kalisina kathalivi"/>
</div>
<div class="lyrico-lyrics-wrapper">O oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidi vidi adugulu vidi vidi manusulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidi vidi adugulu vidi vidi manusulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okatiga kalisina kathalivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatiga kalisina kathalivi"/>
</div>
<div class="lyrico-lyrics-wrapper">O oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O oh oh"/>
</div>
</pre>
