---
title: "chithiram pesuthadi song lyrics"
album: "Koottali"
artist: "Britto Michael"
lyricist: "Kurinchi Prabha"
director: "SK Mathi"
path: "/albums/koottali-song-lyrics"
song: "Chithiram Pesuthadi"
image: ../../images/albumart/koottali.jpg
date: 2018-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KTAgULw4p5Y"
type: "love"
singers:
  - Arvind Srinivas
  - Shakthi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">chithiram pesuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chithiram pesuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">nin chithiram pesuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nin chithiram pesuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">en kangalil eeram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kangalil eeram "/>
</div>
<div class="lyrico-lyrics-wrapper">yeno nan ariyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno nan ariyen"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan kadhalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan kadhalin"/>
</div>
<div class="lyrico-lyrics-wrapper">salaiyil pookalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="salaiyil pookalai"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kidanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kidanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chithiram pesuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chithiram pesuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">nin chithiram pesuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nin chithiram pesuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">undhan kadhalin saalaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan kadhalin saalaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">pookalai nan kidapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookalai nan kidapen"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan aadaiyin orathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan aadaiyin orathil"/>
</div>
<div class="lyrico-lyrics-wrapper">noolena nan irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noolena nan irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">kollaamal kolaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollaamal kolaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">mei endral killathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mei endral killathey"/>
</div>
<div class="lyrico-lyrics-wrapper">sollaamal sollathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollaamal sollathey"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhiyale thullathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhiyale thullathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenjam kavithaiyil mithakuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjam kavithaiyil mithakuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">konjum kadhalil yeri nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjum kadhalil yeri nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjam kavithaiyil mithakuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjam kavithaiyil mithakuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">konjum kadhalil yeri nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjum kadhalil yeri nindru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chithiram pesuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chithiram pesuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">nin chithiram pesuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nin chithiram pesuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyyaamal thallathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyyaamal thallathey"/>
</div>
<div class="lyrico-lyrics-wrapper">enai neengi sellathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai neengi sellathey"/>
</div>
<div class="lyrico-lyrics-wrapper">sollamal kollamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollamal kollamal"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirodu koyyathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirodu koyyathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatril asaikira thavaramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatril asaikira thavaramai"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan vizhi enai asikirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan vizhi enai asikirathey"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thinam thuyikira thai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thinam thuyikira thai"/>
</div>
<div class="lyrico-lyrics-wrapper">madiyai anuthinam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madiyai anuthinam "/>
</div>
<div class="lyrico-lyrics-wrapper">un udan nan irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un udan nan irupen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nenjam kavithaiyil mithakuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjam kavithaiyil mithakuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">konjum kadhalil yeri nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjum kadhalil yeri nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjam kavithaiyil mithakuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjam kavithaiyil mithakuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">konjum kadhalil yeri nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjum kadhalil yeri nindru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chithiram pesuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chithiram pesuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">nin chithiram pesuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nin chithiram pesuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannamma"/>
</div>
</pre>
