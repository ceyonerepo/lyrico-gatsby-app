---
title: "mazhai song lyrics"
album: "Yennanga Sir Unga Sattam"
artist: "Guna Balasubramanian"
lyricist: "M.S. Muthu"
director: "Prabhu Jeyaram"
path: "/albums/yennanga-sir-unga-sattam-lyrics"
song: "Mazhai"
image: ../../images/albumart/yennanga-sir-unga-sattam.jpg
date: 2021-10-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ky6m7xYdIMc"
type: "happy"
singers:
  - Guna Balasubramanian
  - Malvi Sundaresan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mazhai vishunthanthu thoolaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai vishunthanthu thoolaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudai paranthathu thaanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudai paranthathu thaanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adai mazhaiye adi manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adai mazhaiye adi manathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Amarnthidum azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amarnthidum azhage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi karuvizhi hooraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi karuvizhi hooraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagam thulaiththathu neraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagam thulaiththathu neraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Iragenave edai kurainthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iragenave edai kurainthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkiren uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkiren uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanaga neeyum aaginaai en udhayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanaga neeyum aaginaai en udhayame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvaaga neeyum maarinaai en idhayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvaaga neeyum maarinaai en idhayame"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayatha maayamaaginaai en manathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayatha maayamaaginaai en manathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeratha kadhal poosinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeratha kadhal poosinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai vizhunthathu thoolaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai vizhunthathu thoolaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudai paranthathu thanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudai paranthathu thanaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adai mazhaiye adimanathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adai mazhaiye adimanathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Amarnthidum azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amarnthidum azhage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi karuvizhi hooraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi karuvizhi hooraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Agam thulaiththathu neraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agam thulaiththathu neraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Iragenave edai kurainthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iragenave edai kurainthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakhiren uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakhiren uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeraga neeyum siru meenaaga naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeraga neeyum siru meenaaga naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam unnodu nen jodu sernthe irukaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam unnodu nen jodu sernthe irukaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theevaga neeyum thani poovaga naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theevaga neeyum thani poovaga naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enunnodu kannodu ninnai thulaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enunnodu kannodu ninnai thulaikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En mudhal mazhai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mudhal mazhai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">En manam vizhunthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manam vizhunthaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru muzhumutharkadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru muzhumutharkadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">En agam pooththathinge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En agam pooththathinge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kanaa nila neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanaa nila neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi thuyil kalaithaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi thuyil kalaithaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nija thanikadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nija thanikadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai atchi seikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai atchi seikirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyire neeye uyire neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire neeye uyire neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai vishunthanthu thoolaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai vishunthanthu thoolaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudai paranthathu thaanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudai paranthathu thaanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adai mazhaiye adi manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adai mazhaiye adi manathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Amarnthidum azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amarnthidum azhage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi karuvizhi hooraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi karuvizhi hooraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagam thulaiththathu neraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagam thulaiththathu neraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Iragenave edai kurainthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iragenave edai kurainthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkiren uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkiren uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanaga neeyum aaginaai en udhayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanaga neeyum aaginaai en udhayame"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvaaga neeyum maarinaai en idhayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvaaga neeyum maarinaai en idhayame"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayatha maayamaaginaai en manathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayatha maayamaaginaai en manathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeratha kadhal poosinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeratha kadhal poosinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai vizhunthathu thoolaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai vizhunthathu thoolaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudai paranthathu thanaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudai paranthathu thanaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adai mazhaiye adimanathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adai mazhaiye adimanathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Amarnthidum azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amarnthidum azhage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi karuvizhi hooraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi karuvizhi hooraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Agam thulaiththathu neraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agam thulaiththathu neraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Iragenave edai kurainthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iragenave edai kurainthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakhiren uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakhiren uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En mazhaiye neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mazhaiye neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiye neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiye neeye"/>
</div>
</pre>
