---
title: "siru siru vena song lyrics"
album: "Margandeyan"
artist: "Sundar C Babu"
lyricist: "Yugabharathy"
director: "FEFSI Vijayan"
path: "/albums/markandeyan-lyrics"
song: "Siru Siru Vena"
image: ../../images/albumart/markandeyan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/38Sqzvb8hAg"
type: "love"
singers:
  - M.K. Balaji
  - Vichitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Siru Siruvena Viru Viru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Siruvena Viru Viru "/>
</div>
<div class="lyrico-lyrics-wrapper">Ena Thazhuvidaamal Neengaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Thazhuvidaamal Neengaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu Sugamithu Purigira Vayathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Sugamithu Purigira Vayathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Iru Vizhi Thuru Thuruvena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Iru Vizhi Thuru Thuruvena "/>
</div>
<div class="lyrico-lyrics-wrapper">Urasidaamal Thoongaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasidaamal Thoongaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Ithayamum Inaigira Pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Ithayamum Inaigira Pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgal Naangum Poagum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal Naangum Poagum "/>
</div>
<div class="lyrico-lyrics-wrapper">Paathai Kaathal Oorai Seraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathai Kaathal Oorai Seraatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Naangum Kaanum Kaatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Naangum Kaanum Kaatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Poovaai Pagalum Iravum Malaraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Poovaai Pagalum Iravum Malaraatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Siruvena Viru Viru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Siruvena Viru Viru "/>
</div>
<div class="lyrico-lyrics-wrapper">Ena Thazhuvidaamal Neengaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Thazhuvidaamal Neengaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu Sugamithu Purigira Vayathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Sugamithu Purigira Vayathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Iru Vizhi Thuru Thuruvena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Iru Vizhi Thuru Thuruvena "/>
</div>
<div class="lyrico-lyrics-wrapper">Urasidaamal Thoongaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasidaamal Thoongaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Ithayamum Inaigira Pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Ithayamum Inaigira Pozhuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Enna Pesa Aethetho Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Enna Pesa Aethetho Aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidathil Naanum Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidathil Naanum Illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Chinna Chinna Paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Chinna Chinna Paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Alli Veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Alli Veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammidathil Naam Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammidathil Naam Illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam Bhoomi Naan Vendum Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam Bhoomi Naan Vendum Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhuvathum Nee Allavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhuvathum Nee Allavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalanthorum Endrendrum Vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalanthorum Endrendrum Vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Kathai Naeril Sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Kathai Naeril Sollavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Siruvena Viru Viru Ena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Siruvena Viru Viru Ena "/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhuvidaamal Neengaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhuvidaamal Neengaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu Šugamithu Purigira Vayathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Šugamithu Purigira Vayathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Iru Vizhi Thuru Thuruvena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Iru Vizhi Thuru Thuruvena "/>
</div>
<div class="lyrico-lyrics-wrapper">Urasidaamal Thøøngaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasidaamal Thøøngaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Ithayamum Inaigira Pøzhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Ithayamum Inaigira Pøzhuthu"/>
</div>
</pre>
