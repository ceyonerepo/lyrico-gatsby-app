---
title: "kanulo vennala nuvu song lyrics"
album: "Ajay Passayyadu"
artist: "Sahini Srinivas"
lyricist: "Pratap"
director: "Prem Bhagirath"
path: "/albums/ajay-passayyadu-lyrics"
song: "Kanulo Vennala Nuvu"
image: ../../images/albumart/ajay-passayyadu.jpg
date: 2019-01-04
lang: telugu
youtubeLink: "https://www.youtube.com/embed/4KJ20rY4WFA"
type: "sad"
singers:
  - Praveen Kumar Koppolu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kannulalo vennela neevamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannulalo vennela neevamma"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadu pommante etta cheppama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadu pommante etta cheppama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannulalo vennela neevamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannulalo vennela neevamma"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadu pommante etta cheppama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadu pommante etta cheppama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edala ninduga aase choopi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edala ninduga aase choopi"/>
</div>
<div class="lyrico-lyrics-wrapper">kalale repaave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalale repaave "/>
</div>
<div class="lyrico-lyrics-wrapper">chentakochi ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chentakochi ne"/>
</div>
<div class="lyrico-lyrics-wrapper">i love you chebithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you chebithe"/>
</div>
<div class="lyrico-lyrics-wrapper">kallanu chesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallanu chesave"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadu pommanamanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadu pommanamanave"/>
</div>
<div class="lyrico-lyrics-wrapper">entamma entamma entamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entamma entamma entamma"/>
</div>
<div class="lyrico-lyrics-wrapper">ela enduku chesavamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ela enduku chesavamma"/>
</div>
<div class="lyrico-lyrics-wrapper">maa ajaygadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa ajaygadi "/>
</div>
<div class="lyrico-lyrics-wrapper">love nu nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love nu nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">no anabokamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="no anabokamma"/>
</div>
<div class="lyrico-lyrics-wrapper">entamma entamma entamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entamma entamma entamma"/>
</div>
<div class="lyrico-lyrics-wrapper">ela enduku chesavamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ela enduku chesavamma"/>
</div>
<div class="lyrico-lyrics-wrapper">maa ajaygadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa ajaygadi "/>
</div>
<div class="lyrico-lyrics-wrapper">love nu nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love nu nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">no anabokamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="no anabokamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannulona nalusepadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannulona nalusepadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">kanule peekesthama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanule peekesthama"/>
</div>
<div class="lyrico-lyrics-wrapper">peekesthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peekesthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalulona mulle digite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalulona mulle digite"/>
</div>
<div class="lyrico-lyrics-wrapper">kaale narikesthama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaale narikesthama"/>
</div>
<div class="lyrico-lyrics-wrapper">maataleno cheppinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maataleno cheppinaave"/>
</div>
<div class="lyrico-lyrics-wrapper">naatakame chesinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naatakame chesinaave"/>
</div>
<div class="lyrico-lyrics-wrapper">thotalenno tippinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotalenno tippinaave"/>
</div>
<div class="lyrico-lyrics-wrapper">paatalenno paadinave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatalenno paadinave"/>
</div>
<div class="lyrico-lyrics-wrapper">ok anukuni vaddakuvasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ok anukuni vaddakuvasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">vaddani tosesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaddani tosesaave"/>
</div>
<div class="lyrico-lyrics-wrapper">gooba guyyani pinchaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gooba guyyani pinchaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">entamma entamma entamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entamma entamma entamma"/>
</div>
<div class="lyrico-lyrics-wrapper">ela enduku chesavamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ela enduku chesavamma"/>
</div>
<div class="lyrico-lyrics-wrapper">maa ajaygadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa ajaygadi "/>
</div>
<div class="lyrico-lyrics-wrapper">love nu nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love nu nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">no anabokamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="no anabokamma"/>
</div>
<div class="lyrico-lyrics-wrapper">entamma entamma entamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entamma entamma entamma"/>
</div>
<div class="lyrico-lyrics-wrapper">ela enduku chesavamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ela enduku chesavamma"/>
</div>
<div class="lyrico-lyrics-wrapper">maa ajaygadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa ajaygadi "/>
</div>
<div class="lyrico-lyrics-wrapper">love nu nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love nu nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">no anabokamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="no anabokamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aa divilo ni devudikaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa divilo ni devudikaina"/>
</div>
<div class="lyrico-lyrics-wrapper">manase karigenamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manase karigenamma"/>
</div>
<div class="lyrico-lyrics-wrapper">manase karigenamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manase karigenamma"/>
</div>
<div class="lyrico-lyrics-wrapper">karuguthundi ra babu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuguthundi ra babu"/>
</div>
<div class="lyrico-lyrics-wrapper">ee bhuviloni devathakemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee bhuviloni devathakemo"/>
</div>
<div class="lyrico-lyrics-wrapper">manase karuvenamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manase karuvenamma"/>
</div>
<div class="lyrico-lyrics-wrapper">voosulenno cheppinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="voosulenno cheppinaave"/>
</div>
<div class="lyrico-lyrics-wrapper">voohalenno repinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="voohalenno repinaave"/>
</div>
<div class="lyrico-lyrics-wrapper">vooru vaada thippinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vooru vaada thippinaave"/>
</div>
<div class="lyrico-lyrics-wrapper">voobilona dinchinaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="voobilona dinchinaave"/>
</div>
<div class="lyrico-lyrics-wrapper">ponilemmani daggarakosthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponilemmani daggarakosthe"/>
</div>
<div class="lyrico-lyrics-wrapper">pommanamannave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pommanamannave"/>
</div>
<div class="lyrico-lyrics-wrapper">eeda puncture chesaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeda puncture chesaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">entamma entamma entamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entamma entamma entamma"/>
</div>
<div class="lyrico-lyrics-wrapper">ela enduku chesavamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ela enduku chesavamma"/>
</div>
<div class="lyrico-lyrics-wrapper">maa ajaygadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa ajaygadi "/>
</div>
<div class="lyrico-lyrics-wrapper">love nu nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love nu nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">no anabokamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="no anabokamma"/>
</div>
<div class="lyrico-lyrics-wrapper">entamma entamma entamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entamma entamma entamma"/>
</div>
<div class="lyrico-lyrics-wrapper">ela enduku chesavamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ela enduku chesavamma"/>
</div>
<div class="lyrico-lyrics-wrapper">maa ajaygadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa ajaygadi "/>
</div>
<div class="lyrico-lyrics-wrapper">love nu nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love nu nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">no anabokamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="no anabokamma"/>
</div>
</pre>
