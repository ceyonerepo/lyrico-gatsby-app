---
title: "anandha koochal song lyrics"
album: "Kalavani 2"
artist: "Mani Amudhavan"
lyricist: "Mani Amuthavan"
director: "A. Sarkunam"
path: "/albums/kalavani-2-lyrics"
song: "Anandha Koochal"
image: ../../images/albumart/kalavani-2.jpg
date: 2019-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uwjJ4xOruLQ"
type: "love"
singers:
  - Mani Amuthavan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aanandha Koochal Poduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha Koochal Poduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Agayam Thandi Oduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agayam Thandi Oduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangatha Kathal Vanthu Nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangatha Kathal Vanthu Nan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaavonnu Thaavi Aduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavonnu Thaavi Aduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanandha Koochal Poduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha Koochal Poduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Agayam Thandi Oduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agayam Thandi Oduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangatha Kathal Vanthu Nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangatha Kathal Vanthu Nan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaavonnu Thaavi Aduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavonnu Thaavi Aduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeyichitten Nan Kathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeyichitten Nan Kathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeyikka Poren Therthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeyikka Poren Therthala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnalum Ava Sonna Paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnalum Ava Sonna Paru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thena Payuthu Kathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thena Payuthu Kathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanandha Koochal Poduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha Koochal Poduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Agayam Thandi Oduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agayam Thandi Oduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangatha Kathal Vanthu Nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangatha Kathal Vanthu Nan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaavonnu Thaavi Aduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavonnu Thaavi Aduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppo En Vasal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo En Vasal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolam Nee Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolam Nee Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppo Un Kathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo Un Kathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda Nan Aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda Nan Aada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeyichitten Nan Kathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeyichitten Nan Kathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeyikka Poren Therthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeyikka Poren Therthala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnalum Ava Sonna Paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnalum Ava Sonna Paru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thena Payuthu Kathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thena Payuthu Kathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanandha Koochal Poduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandha Koochal Poduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Agayam Thandi Oduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agayam Thandi Oduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaangatha Kathal Vanthu Nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaangatha Kathal Vanthu Nan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaavonnu Thaavi Aduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavonnu Thaavi Aduren"/>
</div>
</pre>
