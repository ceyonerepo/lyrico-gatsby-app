---
title: "ooh kanave song lyrics"
album: "Jarugandi"
artist: "Bobo Shashi"
lyricist: "AN Pitchumani"
director: "AN Pitchumani"
path: "/albums/jarugandi-lyrics"
song: "Ooh Kanave"
image: ../../images/albumart/jarugandi.jpg
date: 2018-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/g4UYI_LuC-g"
type: "happy"
singers:
  - Jesse Samuel 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ooh kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnum keetraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnum keetraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr ganamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr ganamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu sendraiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu sendraiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjodu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindraiyae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindraiyae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollal ennai kondrathaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollal ennai kondrathaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Engeyum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum nee endraanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum nee endraanathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu naanae naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu naanae naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannan thaniyanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannan thaniyanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu naanae naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu naanae naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannan thaniyanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannan thaniyanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sellaatha thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sellaatha thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooh selgindra neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh selgindra neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthaaiyadi thanthaaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaaiyadi thanthaaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Barangal yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barangal yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vesham indri Nesam kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham indri Nesam kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kandu kondenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kandu kondenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veril serum veyil polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veril serum veyil polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai sera vanthenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai sera vanthenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theerathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerathathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha naatkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha naatkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum indriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum indriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum indri thani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum indri thani "/>
</div>
<div class="lyrico-lyrics-wrapper">maaram aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaram aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae naanae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae naanae "/>
</div>
<div class="lyrico-lyrics-wrapper">naanae naanae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanae naanae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu naanae naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu naanae naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannan thaniyanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannan thaniyanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu naanae naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu naanae naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannan thaniyanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannan thaniyanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sellaatha thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sellaatha thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Selgindra neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selgindra neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthaaiyadi thanthaaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthaaiyadi thanthaaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Barangal yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barangal yen"/>
</div>
</pre>
