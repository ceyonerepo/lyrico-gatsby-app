---
title: "samajavaragamana song lyrics"
album: "Ala Vaikunthapurramuloo"
artist: "S. Thaman"
lyricist: "Sirivennela Seetharama Sastry"
director: "Trivikram Srinivas"
path: "/albums/ala-vaikunthapurramuloo-lyrics"
song: "Samajavaragamana"
image: ../../images/albumart/ala-vaikunthapurramuloo.jpg
date: 2020-01-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/OCg6BWlAXSw"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee kaallani pattuku vadalanannavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaallani pattuku vadalanannavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Choode naa kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choode naa kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa choopulanalla thokkuku vellaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa choopulanalla thokkuku vellaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dayaledha asalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dayaledha asalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kaallani pattuku vadalanannavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaallani pattuku vadalanannavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Choode naa kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choode naa kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa choopulanalla thokkuku vellaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa choopulanalla thokkuku vellaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dayaledha asalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dayaledha asalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kallaki kaavaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kallaki kaavaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasthaaye kaatukala naa kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasthaaye kaatukala naa kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nulumuthunte yerraga kandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nulumuthunte yerraga kandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindhene segalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindhene segalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa oopiri gaaliki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa oopiri gaaliki"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyyaalalooguthu unte mungurulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyyaalalooguthu unte mungurulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nettesthe ela nittoorchavatte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nettesthe ela nittoorchavatte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nishtoorapu vilavilalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nishtoorapu vilavilalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samajavaragamana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samajavaragamana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu choosi aaga galana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu choosi aaga galana"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu meeda vayasukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu meeda vayasukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adupu cheppa thaguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adupu cheppa thaguna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samajavaragamana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samajavaragamana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu choosi aaga galana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu choosi aaga galana"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu meeda vayasukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu meeda vayasukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adupu cheppa thaguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adupu cheppa thaguna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kaallani pattuku vadalanannavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaallani pattuku vadalanannavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Choode naa kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choode naa kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa choopulanalla thokkuku vellaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa choopulanalla thokkuku vellaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dayaledha asalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dayaledha asalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mallela maasama Manjula haasama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallela maasama Manjula haasama"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi malupulona yeduru padina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi malupulona yeduru padina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannela vanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannela vanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virisina pinchamaa Virula prapanchama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virisina pinchamaa Virula prapanchama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenni vanne chinnelante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenni vanne chinnelante"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennela vashama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennela vashama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey naa gaale thagilina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey naa gaale thagilina"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa neede thariminaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa neede thariminaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vulakavaa, palakavaa bhaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vulakavaa, palakavaa bhaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Entho brathimaalina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entho brathimaalina"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthena anganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthena anganaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhini meetu madhuramaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhini meetu madhuramaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Manavini vinumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manavini vinumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samajavaragamana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samajavaragamana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu choosi aaga galana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu choosi aaga galana"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu meeda vayasukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu meeda vayasukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adupu cheppa thaguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adupu cheppa thaguna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samajavaragamana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samajavaragamana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu choosi aaga galana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu choosi aaga galana"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu meeda vayasukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu meeda vayasukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adupu cheppa thaguna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adupu cheppa thaguna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kaallani pattuku vadalanannavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaallani pattuku vadalanannavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Choode naa kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choode naa kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa choopulanalla thokkuku vellaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa choopulanalla thokkuku vellaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dayaledha asalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dayaledha asalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kallaki kaavaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kallaki kaavaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasthaaye kaatukala naa kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasthaaye kaatukala naa kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nulumuthunte yerraga kandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nulumuthunte yerraga kandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindhene segalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindhene segalu"/>
</div>
</pre>
