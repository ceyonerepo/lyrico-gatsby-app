---
title: "dravida kone song lyrics"
album: "Tughlaq Durbar"
artist: "Govind Vasantha"
lyricist: "Karthik Netha"
director: "Balaji Tharaneetharan"
path: "/albums/tughlaq-durbar-song-lyrics"
song: "Dravida Kone"
image: ../../images/albumart/tughlaq-durbar.jpg
date: 2021-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ep77uLSvdkw"
type: "happy"
singers:
  - Hariharasudhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dhravida kone mudivilla muzhu vaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhravida kone mudivilla muzhu vaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanilam potrum thala thanga tamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanilam potrum thala thanga tamizha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aan ina thaaye ada anbaal oru theeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aan ina thaaye ada anbaal oru theeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiva en thalaiva ini neeye mudhalva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiva en thalaiva ini neeye mudhalva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhravida kone mudivilla muzhu vaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhravida kone mudivilla muzhu vaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanilam potrum thala thanga tamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanilam potrum thala thanga tamizha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aan ina thaaye ada anbaal oru theeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aan ina thaaye ada anbaal oru theeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiva en thalaiva ini neeye mudhalva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiva en thalaiva ini neeye mudhalva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey aa ee idhu thaan bhogi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey aa ee idhu thaan bhogi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor thirandadhu oor thirandadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor thirandadhu oor thirandadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa thalaivane naal pirandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa thalaivane naal pirandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhi bodhi needhi neethane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhi bodhi needhi neethane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivane nee nee nee porali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivane nee nee nee porali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingumai engeyumai nindraye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingumai engeyumai nindraye "/>
</div>
<div class="lyrico-lyrics-wrapper">kandaye mangalamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandaye mangalamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondanai dhavlathuvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondanai dhavlathuvai "/>
</div>
<div class="lyrico-lyrics-wrapper">vandhaye vendraye sandhanamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhaye vendraye sandhanamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandiyai ottandiyai maayandi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandiyai ottandiyai maayandi "/>
</div>
<div class="lyrico-lyrics-wrapper">ninnare aayandiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnare aayandiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bondiyai appaviyai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bondiyai appaviyai "/>
</div>
<div class="lyrico-lyrics-wrapper">annaru innaru komaaliyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaru innaru komaaliyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakkalapai iruppavare 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakkalapai iruppavare "/>
</div>
<div class="lyrico-lyrics-wrapper">palapalapai jolippavare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palapalapai jolippavare"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruppula vetti vetti moracharappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruppula vetti vetti moracharappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Washing-u la vallavaru kairasi ullavaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Washing-u la vallavaru kairasi ullavaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeyai thaayai peyai vaayai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyai thaayai peyai vaayai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nai kazhuvuna vai kazhuvuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nai kazhuvuna vai kazhuvuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ai kazhuvuna en thalaivana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ai kazhuvuna en thalaivana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada verai verai vaarai kondare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada verai verai vaarai kondare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangalam malangaattu mannare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangalam malangaattu mannare"/>
</div>
</pre>
