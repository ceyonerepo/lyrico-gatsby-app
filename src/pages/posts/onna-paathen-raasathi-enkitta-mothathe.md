---
title: "onna paathen raasathi song lyrics"
album: "Enkitta Mothathe"
artist: "Natarajan Sankaran"
lyricist: "Yugabharathi"
director: "Ramu Chellappa"
path: "/albums/enkitta-mothathe-lyrics"
song: "Onna Paathen Raasathi"
image: ../../images/albumart/enkitta-mothathe.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KxBUxPCwkgM"
type: "love"
singers:
  - D Imman
  - Vandana Srinivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Onna Paathen Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Paathen Raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungaadha Yemaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungaadha Yemaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Naanum Kaapathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Naanum Kaapathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Viduvena Kaimaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduvena Kaimaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polaadha Aalu Enna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polaadha Aalu Enna "/>
</div>
<div class="lyrico-lyrics-wrapper">Pooparikka Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooparikka Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallaaku Mele Nenja 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallaaku Mele Nenja "/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyala Thooki Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyala Thooki Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Killadi Neeyun Vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killadi Neeyun Vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Kerangadicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kerangadicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Lambaadiyaaga Yenna Naal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lambaadiyaaga Yenna Naal "/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhukka Naan Thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhukka Naan Thavichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Onna Nenchidhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Onna Nenchidhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Suthi Kedakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Suthi Kedakkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enna Nenaikadhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enna Nenaikadhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vathi Thavikkeren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vathi Thavikkeren"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Paathen Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Paathen Raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungaadha Yemaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungaadha Yemaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala Mudiya Yendi Seeva Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Mudiya Yendi Seeva Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogathilayun Powder Poosa Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogathilayun Powder Poosa Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kada Theruva Unna Paaka Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada Theruva Unna Paaka Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhav Idikil Kanna Pooka Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhav Idikil Kanna Pooka Vecha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhedho Raagam Paaduren Kirukkapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhedho Raagam Paaduren Kirukkapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum Unna Pugazhuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum Unna Pugazhuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolaaru Naanum Aaguren Unakku Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaaru Naanum Aaguren Unakku Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Naan Romba Puluguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Naan Romba Puluguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Onna Nenchidhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Onna Nenchidhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Suthi Kedakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Suthi Kedakkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enna Nenaikadhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enna Nenaikadhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vathi Thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vathi Thavikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Onna Nenchidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Onna Nenchidhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Suthi Kedakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Suthi Kedakkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enna Nenaikadhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enna Nenaikadhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vathi Thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vathi Thavikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Paathen Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Paathen Raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungaadha Yemaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungaadha Yemaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Color Colora Enna Milira Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color Colora Enna Milira Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavulayun Karagam Aada Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavulayun Karagam Aada Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadagadanu Manasa Maathi Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadagadanu Manasa Maathi Vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhi Sadaiya Kavidha Kekka Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhi Sadaiya Kavidha Kekka Vecha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedalam Pola Thaavuren Nerupillama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedalam Pola Thaavuren Nerupillama"/>
</div>
<div class="lyrico-lyrics-wrapper">Summave Patthi Eriyiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summave Patthi Eriyiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalala Thaalam Poduren Kanakillama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalala Thaalam Poduren Kanakillama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannala Kathi Sorugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala Kathi Sorugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Onna Nenchidhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Onna Nenchidhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Suthi Kedakkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Suthi Kedakkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enna Nenaikadhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enna Nenaikadhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vathi Thavikkeren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vathi Thavikkeren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Paathen Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Paathen Raasathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhungaadha Yemaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhungaadha Yemaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Naanum Kaapathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Naanum Kaapathi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaapathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaapathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Viduvena Kaimaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduvena Kaimaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaimaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaimaathi"/>
</div>
</pre>
