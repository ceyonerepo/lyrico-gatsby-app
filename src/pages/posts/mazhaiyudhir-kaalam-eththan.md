---
title: "mazhaiyudhir kaalam song lyrics"
album: "Eththan"
artist: "Taj Noor"
lyricist: "Na Muthukumar"
director: "L Suresh"
path: "/albums/eththan-lyrics"
song: "Mazhaiyudhir Kaalam"
image: ../../images/albumart/eththan.jpg
date: 2011-05-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dSDdqHPyqkI"
type: "love"
singers:
  - Vijay Yesudas
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mazhaiyudhir Kaalam Mazhaiyudhir Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyudhir Kaalam Mazhaiyudhir Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil therigirathe Manasukul etho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil therigirathe Manasukul etho"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukul etho minnal idikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukul etho minnal idikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paniyin thuligal vizhum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniyin thuligal vizhum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">ilaigal ariyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilaigal ariyaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnmugam parthu unnudan irundhaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnmugam parthu unnudan irundhaal "/>
</div>
<div class="lyrico-lyrics-wrapper">thooram kuraikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram kuraikirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varthai solla thudipathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varthai solla thudipathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un parvai sollkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un parvai sollkirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam polave oru mozhithaan yethadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam polave oru mozhithaan yethadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyudhir Kaalam Mazhaiyudhir Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyudhir Kaalam Mazhaiyudhir Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil therigirathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil therigirathe "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanayo payanangal vandhathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanayo payanangal vandhathu "/>
</div>
<div class="lyrico-lyrics-wrapper">ponathu ithupol yethumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponathu ithupol yethumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai vittu thalli nindru ennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vittu thalli nindru ennai "/>
</div>
<div class="lyrico-lyrics-wrapper">nane parkiren paravasam kurayavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane parkiren paravasam kurayavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">kathimel nadakum maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathimel nadakum maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kadhalil nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kadhalil nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">endha valigalum inithidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endha valigalum inithidume"/>
</div>
<div class="lyrico-lyrics-wrapper">Buthiyinai kulappum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buthiyinai kulappum"/>
</div>
<div class="lyrico-lyrics-wrapper">aanal manadhuku pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanal manadhuku pidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu marupadi kulapidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu marupadi kulapidume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal adhu pollaladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal adhu pollaladhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollamale kollum adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollamale kollum adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">koocham aacham illadhadhu haiyo haiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koocham aacham illadhadhu haiyo haiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyudhir Kaalam Mazhaiyudhir Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyudhir Kaalam Mazhaiyudhir Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil therigirathe Manasukul etho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil therigirathe Manasukul etho"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukul etho minnal idikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukul etho minnal idikirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnathoru punnagai ennenavo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnathoru punnagai ennenavo "/>
</div>
<div class="lyrico-lyrics-wrapper">pannudhu ennake puriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pannudhu ennake puriyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal vandha padagena pulangalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal vandha padagena pulangalum "/>
</div>
<div class="lyrico-lyrics-wrapper">thudikudhu karaye theriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudikudhu karaye theriyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil oru kanavu Vidindhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil oru kanavu Vidindhal "/>
</div>
<div class="lyrico-lyrics-wrapper">adhu kanidhida periyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu kanidhida periyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">aanal manam nambivida marukiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanal manam nambivida marukiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Renduketta vayasu medhuvaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Renduketta vayasu medhuvaai "/>
</div>
<div class="lyrico-lyrics-wrapper">oru thundilai poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thundilai poduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mansum adhil mattikitu mulikiradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mansum adhil mattikitu mulikiradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penne naalai enna nadakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne naalai enna nadakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ennai kanavidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ennai kanavidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Penne naalai enna nadakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne naalai enna nadakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyudhir Kaalam Mazhaiyudhir Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyudhir Kaalam Mazhaiyudhir Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil therigirathe Manasukul etho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil therigirathe Manasukul etho"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukul etho minnal idikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukul etho minnal idikirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varthai solla thudipathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varthai solla thudipathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un parvai sollkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un parvai sollkirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam polave oru mozhithaan yethadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam polave oru mozhithaan yethadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyudhir Kaalam Mazhaiyudhir Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyudhir Kaalam Mazhaiyudhir Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil therigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil therigirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyudhir Kaalam Mazhaiyudhir Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyudhir Kaalam Mazhaiyudhir Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil therigirathe Manasukul etho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil therigirathe Manasukul etho"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukul etho minnal idikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukul etho minnal idikirathe"/>
</div>
</pre>
