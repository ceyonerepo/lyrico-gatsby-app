---
title: "thiru thiru gananatha song lyrics"
album: "100 percentage Kadhal"
artist: "G.V. Prakash Kumar"
lyricist: "Mohanrajan"
director: "M.M. Chandramouli"
path: "/albums/100-percentage-kadhal-lyrics"
song: "Thiru Thiru Gananatha"
image: ../../images/albumart/100%-kadhal.jpg
date: 2019-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Yh2jMpUk7C4"
type: "happy"
singers:
  - Harini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thiru Thiru Gananaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiru Thiru Gananaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanathom Thom Thithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanathom Thom Thithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theechudar Vizhiyone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theechudar Vizhiyone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanathom Thom Thithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanathom Thom Thithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venbaavum Sempoovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venbaavum Sempoovum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Vaazhtha Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Vaazhtha Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thendou Thinaimaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendou Thinaimaavum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkkaaga Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkkaaga Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lesaai Lesaai Kannpaaraiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesaai Lesaai Kannpaaraiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marai Sonna Iraiyaalan Neethaanaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marai Sonna Iraiyaalan Neethaanaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lesaai Lesaai Kaappaatraiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesaai Lesaai Kaappaatraiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siram Thazhtha Varam Ennum Kodaiyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siram Thazhtha Varam Ennum Kodaiyaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puram Nookki Agam Kaakkum Maayakaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puram Nookki Agam Kaakkum Maayakaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanampookki Karam Korkkum Paasakaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanampookki Karam Korkkum Paasakaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idar Neekki Sudar Yetrum Kaavalkkaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idar Neekki Sudar Yetrum Kaavalkkaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugam Kaathu Sumai Pokkum Nesakaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam Kaathu Sumai Pokkum Nesakaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lesaai Lesaai Kanpaaraiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesaai Lesaai Kanpaaraiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marai Sonna Iraiyaalan Neethaanaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marai Sonna Iraiyaalan Neethaanaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nootrukku Noor Vaanga Aasai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nootrukku Noor Vaanga Aasai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pass Aagum Alavukku Thetrividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pass Aagum Alavukku Thetrividu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Scientist In IQ Va Thavai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scientist In IQ Va Thavai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koduthaalum Athai Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduthaalum Athai Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Quickaai Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Quickaai Kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Guindy Bridgeil En Banner
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guindy Bridgeil En Banner"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaikkum Marku Vendaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikkum Marku Vendaame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Last U Benchu Unnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Last U Benchu Unnaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Konjundu Munnera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Konjundu Munnera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Konjam Help Pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Konjam Help Pannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiru Thiru Gananaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiru Thiru Gananaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanathom Thom Thithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanathom Thom Thithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theechudar Vizhiyone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theechudar Vizhiyone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanathom Thom Thithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanathom Thom Thithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Computer Varum Munne Mouse Ai Vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Computer Varum Munne Mouse Ai Vaithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyaanda Vingyaani Neethaanappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaanda Vingyaani Neethaanappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammavum Appavum Ulagam Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammavum Appavum Ulagam Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rounds Vandha Einstein Neethaanappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rounds Vandha Einstein Neethaanappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bestu Marku Vendaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bestu Marku Vendaame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Justu Passu Pothumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Justu Passu Pothumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Restu Konjam Illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Restu Konjam Illaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Enakkaaga Nee Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Enakkaaga Nee Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hardworku Seivaaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hardworku Seivaaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiru Thiru Gananaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiru Thiru Gananaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanathom Thom Thithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanathom Thom Thithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theechudar Vizhiyone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theechudar Vizhiyone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanathom Thom Thithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanathom Thom Thithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venbaavum  Sempoovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venbaavum  Sempoovum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Vaazhtha Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Vaazhtha Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenodu Thinaimaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenodu Thinaimaavum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaga Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga Thaane"/>
</div>
</pre>
