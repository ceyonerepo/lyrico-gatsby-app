---
title: "aadyathe nokkil song lyrics"
album: "Kozhipporu"
artist: "Bijibal"
lyricist: "Vinayak Sasikumar"
director: "Jinoy Janardhanan - Jibit George"
path: "/albums/kozhipporu-lyrics"
song: "Aadyathe Nokkil"
image: ../../images/albumart/sufiyum-sujatayum.jpg
date: 2020-03-06
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/zOTM9V6qzSM"
type: "love"
singers:
  - Bijibal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aadyathe nokkil chanthakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadyathe nokkil chanthakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnetho vakkil nee ishtakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnetho vakkil nee ishtakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram tharilam tharaka chelullole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram tharilam tharaka chelullole"/>
</div>
<div class="lyrico-lyrics-wrapper">Eriyunnu kannil thoo mezhuthirikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eriyunnu kannil thoo mezhuthirikal"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheviyorthirunnu pala madhu mozhikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheviyorthirunnu pala madhu mozhikal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvodu thanu chernnu thaliradi njodikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvodu thanu chernnu thaliradi njodikal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pranaya nilavilay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pranaya nilavilay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Noorashakal izha chernnozhukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorashakal izha chernnozhukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonthennal thovunnu poombodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonthennal thovunnu poombodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dalamarmmaramaay shalabham varamay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dalamarmmaramaay shalabham varamay"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhakarnna vaasantham ariyunnu naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhakarnna vaasantham ariyunnu naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru tharala paragamay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru tharala paragamay"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadyathe nokkil nee jaalakkaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadyathe nokkil nee jaalakkaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadyathe nokkil nee ishtakkaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadyathe nokkil nee ishtakkaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram tharilam tharaka chelullole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram tharilam tharaka chelullole"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethormmathan ilaneerkkuliril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethormmathan ilaneerkkuliril"/>
</div>
<div class="lyrico-lyrics-wrapper">Moolathe moolunnu kaivalakal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolathe moolunnu kaivalakal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidarum chodikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidarum chodikal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mookarum kavilil oru saandhya saayoojyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mookarum kavilil oru saandhya saayoojyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalaadave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalaadave"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru hridayamithaalave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru hridayamithaalave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadyathe nokkil chanthakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadyathe nokkil chanthakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnetho vakkil nee ishtakkari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnetho vakkil nee ishtakkari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram tharilam tharaka chelullole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram tharilam tharaka chelullole"/>
</div>
<div class="lyrico-lyrics-wrapper">Eriyunnu kannil thoo mezhuthirikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eriyunnu kannil thoo mezhuthirikal"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheviyorthirunnu pala madhu mozhikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheviyorthirunnu pala madhu mozhikal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvodu thanu chernnu thaliradi njodikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvodu thanu chernnu thaliradi njodikal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pranaya nilavilay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pranaya nilavilay"/>
</div>
</pre>
