---
title: "kurisena song lyrics"
album: "Orey Bujjiga"
artist: "Anup Rubens"
lyricist: "Krishna Kanth"
director: "Vijay Kumar Konda"
path: "/albums/orey-bujjiga-lyrics"
song: "Kurisena"
image: ../../images/albumart/orey-bujjiga.jpg
date: 2020-10-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/N1X6hIVyb-w"
type: "love"
singers:
  - Armaan Malik
  - P Meghana
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kurisena kurisena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurisena kurisena"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholakari valapule mansuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholakari valapule mansuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Murisena murisena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murisena murisena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalaki kanulaki kalisena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalaki kanulaki kalisena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningilo thaarale jebulo dhoorena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningilo thaarale jebulo dhoorena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhehame meghamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhehame meghamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theluthunna samayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theluthunna samayana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilavilalale ninne chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilavilalale ninne chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranam Kavalandhi sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranam Kavalandhi sontham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pergindhe ishtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pergindhe ishtam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilavilalale Ninne chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilavilalale Ninne chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranam Kavalandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranam Kavalandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontham Perigindhe ishtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontham Perigindhe ishtam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurisena Kurisena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurisena Kurisena"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholakari valapule mansuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholakari valapule mansuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Murisena murisena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murisena murisena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalaki kanulaki kalisena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalaki kanulaki kalisena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oka varamu adhi Nannu nadipinadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka varamu adhi Nannu nadipinadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasithanamuku Thirigika tariminadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasithanamuku Thirigika tariminadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhavadiginadhi Neelo dhorikinadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavadiginadhi Neelo dhorikinadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkasari nannu neela nilipinadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkasari nannu neela nilipinadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chusthu chusthu nadhe lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthu chusthu nadhe lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho paate maare maikam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho paate maare maikam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddari gundela chappudulippudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddari gundela chappudulippudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayye yekam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayye yekam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilavilalale ninne chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilavilalale ninne chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranam Kavalandhi sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranam Kavalandhi sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pergindhe ishtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pergindhe ishtam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotha malupu idhi ninnu kalipinadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha malupu idhi ninnu kalipinadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu ekkadunte akkadike tariminadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu ekkadunte akkadike tariminadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni manasu idi… Ninne adiginadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni manasu idi… Ninne adiginadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkadunna pakkanunde talapu idi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkadunna pakkanunde talapu idi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninna monna baane unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna monna baane unna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niddura mottham padauthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niddura mottham padauthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve vache swapnam kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve vache swapnam kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veche unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veche unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurisena Kurisena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurisena Kurisena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholakari valapule mansuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholakari valapule mansuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Murisena murisena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murisena murisena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalaki kanulaki kalisena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalaki kanulaki kalisena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningilo thaarale jebulo dhoorena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningilo thaarale jebulo dhoorena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhehame meghamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhehame meghamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theluthunna samayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theluthunna samayana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilavilalale ninne chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilavilalale ninne chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranam Kavalandhi sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranam Kavalandhi sontham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pergindhe ishtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pergindhe ishtam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilavilalale ninne chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilavilalale ninne chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranam Kavalandhi sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranam Kavalandhi sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pergindhe ishtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pergindhe ishtam"/>
</div>
</pre>
