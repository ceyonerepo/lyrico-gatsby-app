---
title: "azhagaru vaaraaru song lyrics"
album: "Devarattam"
artist: "Nivas K. Prasanna"
lyricist: "Pa. Vijay"
director: "M. Muthaiah"
path: "/albums/devarattam-lyrics"
song: "Azhagaru Vaaraaru"
image: ../../images/albumart/devarattam.jpg
date: 2019-05-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wTJkGhAJWpE"
type: "happy"
singers:
  - V. M. Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ullankaalu kulunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullankaalu kulunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Udukka pada padakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udukka pada padakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimi thada thadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimi thada thadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu namma"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu namma"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchchi kidu kidunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi kidu kidunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullankaalu kulunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullankaalu kulunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Udukka pada padakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udukka pada padakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimi thada thadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimi thada thadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu namma"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu namma"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meesaiya murikkittu yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesaiya murikkittu yei"/>
</div>
<div class="lyrico-lyrics-wrapper">Vel kamba thookkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vel kamba thookkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesaiya murikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesaiya murikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vel kamba thookkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vel kamba thookkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthira mela yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthira mela yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaigai aaththa thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaigai aaththa thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu namma"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu namma"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kombu oothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombu oothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolava pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolava pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottuthadaa maththalaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottuthadaa maththalaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru sanam onnaa sendhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru sanam onnaa sendhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poduthadaa kondatatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poduthadaa kondatatha"/>
</div>
<div class="lyrico-lyrics-wrapper">epothumae kachcheri joora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothumae kachcheri joora"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakattura ooruthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakattura ooruthadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattu vedi vaanaththa kilichchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattu vedi vaanaththa kilichchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo poova thoovumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo poova thoovumadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththamum adhigamirukkum inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththamum adhigamirukkum inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththiyamum neranjurukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththiyamum neranjurukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Podraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Apdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apdi"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchchi kidu kidunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi kidu kidunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullankaalu kulunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullankaalu kulunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Udukka pada padakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udukka pada padakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimi thada thadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimi thada thadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu namma"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu namma"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchchi kidu kidunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi kidu kidunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullankaalu kulunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullankaalu kulunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Udukka pada padakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udukka pada padakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimi thada thadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimi thada thadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu namma"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu namma"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaru vaararu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaru vaararu"/>
</div>
</pre>
