---
title: "arey meriseley song lyrics"
album: "Ardha Shathabdam"
artist: "Nawfal Raja AIS"
lyricist: "Rahman"
director: "Rawindra Pulle"
path: "/albums/ardha-shathabdam-lyrics"
song: "Arey Meriseley Meriseley"
image: ../../images/albumart/ardha-shathabdam.jpg
date: 2021-03-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/vUpUv30qAkM"
type: "love"
singers:
  - Shankar mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mangalyam thanthunaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangalyam thanthunaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Mavajeevana hethunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavajeevana hethunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey meriseley meriseley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey meriseley meriseley"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulalo velugule kalalu siruluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulalo velugule kalalu siruluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatha kalisaley kalisaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatha kalisaley kalisaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru manasulu kalisaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru manasulu kalisaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugule okatiga kalisi nadavaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugule okatiga kalisi nadavaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa ningi merisndhi pandhiriga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa ningi merisndhi pandhiriga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee nela velasindi peetaluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nela velasindi peetaluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholi valape vadhuvai niliche…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi valape vadhuvai niliche…"/>
</div>
<div class="lyrico-lyrics-wrapper">Varude varamai raaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varude varamai raaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee jagame athithai murise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee jagame athithai murise"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasey manuvai poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasey manuvai poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika shwasalo shwasaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika shwasalo shwasaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalagalisina aashaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalagalisina aashaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Undipovaalilaa okarikokarugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undipovaalilaa okarikokarugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oka kala laaga karigenu dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka kala laaga karigenu dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika jatha cheri murisenu pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika jatha cheri murisenu pranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka shila laga nilichenu gaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka shila laga nilichenu gaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi gudilone tarigenu banam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi gudilone tarigenu banam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi kadaa ee hrudhayamulo odhigina prema bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi kadaa ee hrudhayamulo odhigina prema bandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka swaramai tadiminadhi tanuvuga raaga bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka swaramai tadiminadhi tanuvuga raaga bandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde ninda sandhademi techhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde ninda sandhademi techhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Undipoyinave pandagalle vachhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undipoyinave pandagalle vachhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnamalle vendi vennelalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnamalle vendi vennelalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu allukove rendu kallathoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu allukove rendu kallathoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarigi jarigi karige tholikari paruvapu jadiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarigi jarigi karige tholikari paruvapu jadiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedha pai palike thadi thakathakathaka thakadhimitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedha pai palike thadi thakathakathaka thakadhimitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika shwasalo shwasaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika shwasalo shwasaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalagalisina aashaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalagalisina aashaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Undipovaalilaa okarikokarugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undipovaalilaa okarikokarugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gelichinave ninu naa prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelichinave ninu naa prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilipinadhe lolona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilipinadhe lolona"/>
</div>
<div class="lyrico-lyrics-wrapper">Viduvanu ika ye janma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduvanu ika ye janma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathapaduthu raana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathapaduthu raana"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka needanai nadipinchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka needanai nadipinchana"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka pranamai brathikeyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka pranamai brathikeyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranayamule eduraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranayamule eduraina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedarani deepayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedarani deepayanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarigamalu chadavanivo katha mana prema kavyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarigamalu chadavanivo katha mana prema kavyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nenu paadukunna paata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nenu paadukunna paata"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangurangulunna gnapakala thota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangurangulunna gnapakala thota"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu nenu ekamaina chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nenu ekamaina chota"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbulantu leni chandamama kota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbulantu leni chandamama kota"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu naa sagamai jagamai udayapu tholi kiranamugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu naa sagamai jagamai udayapu tholi kiranamugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velugai thagile tholi chilipili thalukulu taragadhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velugai thagile tholi chilipili thalukulu taragadhura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ika shwasalo shwasaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika shwasalo shwasaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalagalisina aashaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalagalisina aashaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Undipovaalilaa okarikokarugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undipovaalilaa okarikokarugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa ningi merisndhi pandhiriga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa ningi merisndhi pandhiriga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee nela velasindi peetaluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nela velasindi peetaluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholi valape vadhuvai niliche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi valape vadhuvai niliche"/>
</div>
<div class="lyrico-lyrics-wrapper">Varude varamai raaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varude varamai raaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee jagame athithai murise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee jagame athithai murise"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasey manuvai poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasey manuvai poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika shwasalo shwasaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika shwasalo shwasaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalagalisina aashaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalagalisina aashaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Undipovaalilaa okarikokarugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undipovaalilaa okarikokarugaa"/>
</div>
</pre>
