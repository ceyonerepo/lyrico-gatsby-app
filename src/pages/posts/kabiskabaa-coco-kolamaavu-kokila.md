---
title: "kabiskabaa coco song lyrics"
album: "Kolamaavu Kokila"
artist: "Anirudh Ravichander"
lyricist: "Arunraja Kamaraj"
director: "Nelson Dilipkumar"
path: "/albums/kolamaavu-kokila-lyrics"
song: "Kabiskabaa CoCo"
image: ../../images/albumart/kolamaavu-kokila.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RED3lFvjxRA"
type: "happy"
singers:
  - Arunraja Kamaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tak tik digu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tak tik digu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tak tik taga dugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tak tik taga dugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tak tik digu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tak tik digu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tak tik taga dugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tak tik taga dugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tak tik digu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tak tik digu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tak tik taga dugu daa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tak tik taga dugu daa daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tak tik digu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tak tik digu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tak tik taga dugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tak tik taga dugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tak tik digu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tak tik digu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tak tik taga dugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tak tik taga dugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tak tik digu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tak tik digu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tak tik taga dugu daa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tak tik taga dugu daa daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juba juba juba juba jubhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juba juba juba juba jubhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Juba juba juba juba jubhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juba juba juba juba jubhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Juba juba joo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juba juba joo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juba juba joo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juba juba joo"/>
</div>
<div class="lyrico-lyrics-wrapper">Jubhaakilaapilaakilaapoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jubhaakilaapilaakilaapoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juba juba juba juba jubhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juba juba juba juba jubhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Juba juba juba juba jubhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juba juba juba juba jubhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Juba juba joo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juba juba joo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juba juba joo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juba juba joo"/>
</div>
<div class="lyrico-lyrics-wrapper">Jubhaakilaapilaakilaapoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jubhaakilaapilaakilaapoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabiskabaa Coco
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa Coco"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabiskabaa Coco
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa Coco"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jhum thaak jujum thaak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhum thaak jujum thaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhum jhum thaak jumjum thaak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhum jhum thaak jumjum thaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhum thaak jujum thaak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhum thaak jujum thaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhum jhum thaak jumjum thaak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhum jhum thaak jumjum thaak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jhum thaak jujum thaak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhum thaak jujum thaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhum jhum thaak jumjum thaak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhum jhum thaak jumjum thaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhum thaak jujum thaak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhum thaak jujum thaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhum jhum thaak jumjum thaak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhum jhum thaak jumjum thaak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tadangu dagu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadangu dagu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadangu digu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadangu digu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadangu dagu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadangu dagu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadangu digu diguTadangu dagu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadangu digu diguTadangu dagu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadangu digu digu daa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadangu digu digu daa daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tadangu dagu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadangu dagu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadangu digu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadangu digu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadangu dagu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadangu dagu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadangu digu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadangu digu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadangu dagu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadangu dagu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadangu digu digu daa daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadangu digu digu daa daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jigakku jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigakku jaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigakku jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigakku jaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigakku jaa Coco
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigakku jaa Coco"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jigakku jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigakku jaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigakku jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigakku jaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigakku jaa Coco
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigakku jaa Coco"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jigakku jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigakku jaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigakku jaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jigakku jaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tadaangu dagu digu daa  daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadaangu dagu digu daa  daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadaangu dagu digu daa  daa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadaangu dagu digu daa  daa "/>
</div>
<div class="lyrico-lyrics-wrapper">Tadaangu tagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadaangu tagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadaangu tagu dagu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadaangu tagu dagu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadaangu dagu digu daa  daa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadaangu dagu digu daa  daa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tadaangu dagu digu daa  daa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadaangu dagu digu daa  daa "/>
</div>
<div class="lyrico-lyrics-wrapper">Tadaangu dagu digu daa  daa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadaangu dagu digu daa  daa "/>
</div>
<div class="lyrico-lyrics-wrapper">Tadaangu tagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadaangu tagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadaangu tagu dagu digu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadaangu tagu dagu digu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadaangu dagu digu daa  daa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadaangu dagu digu daa  daa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Juba juba juba juba jubhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juba juba juba juba jubhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Juba juba juba juba jubhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juba juba juba juba jubhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Juba juba joo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juba juba joo"/>
</div>
<div class="lyrico-lyrics-wrapper">Juba juba joo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Juba juba joo"/>
</div>
<div class="lyrico-lyrics-wrapper">Jubhaakilaapilaakilaapoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jubhaakilaapilaakilaapoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabiskabaa Coco
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa Coco"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabiskabaa Coco
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa Coco"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabiskabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabiskabaa"/>
</div>
</pre>
