---
title: "comosava paris song lyrics"
album: "World Famous Lover"
artist: "Gopi Sundar"
lyricist: "Ramajogayya Sastry"
director: "Kranthi Madhav"
path: "/albums/world-famous-lover-lyrics"
song: "Comosava Paris"
image: ../../images/albumart/world-famous-lover.jpg
date: 2020-02-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/zd4WW-QDGAc"
type: "happy"
singers:
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yeah yeah yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah yeah yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu natho friendship chesthawa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu natho friendship chesthawa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu natho friendship chesthawa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu natho friendship chesthawa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come lets go rock it yo yo yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come lets go rock it yo yo yo"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s have some fun time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s have some fun time"/>
</div>
<div class="lyrico-lyrics-wrapper">Adrenaline upongindhi oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adrenaline upongindhi oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Like a firebird payikegarana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like a firebird payikegarana"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t stop nannevare apina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t stop nannevare apina"/>
</div>
<div class="lyrico-lyrics-wrapper">Full on attitude chupinchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full on attitude chupinchana"/>
</div>
<div class="lyrico-lyrics-wrapper">Na life nadhe anana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na life nadhe anana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wassup antu lokanni niladhistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wassup antu lokanni niladhistha"/>
</div>
<div class="lyrico-lyrics-wrapper">What is new today antu adugestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What is new today antu adugestha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhamayina anubhavalu vethukuthuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhamayina anubhavalu vethukuthuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Brand-new everyday ne gadipestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brand-new everyday ne gadipestha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Comosava Paris comosava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comosava Paris comosava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu natho friendship chesthawa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu natho friendship chesthawa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu natho friendship chesthawa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu natho friendship chesthawa"/>
</div>
<div class="lyrico-lyrics-wrapper">Come lets go rock it yo yo yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come lets go rock it yo yo yo"/>
</div>
</pre>
