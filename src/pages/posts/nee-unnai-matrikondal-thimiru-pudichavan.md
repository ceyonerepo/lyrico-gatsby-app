---
title: "nee unnai matrikondal song lyrics"
album: "Thimiru Pudichavan"
artist: "Vijay Antony"
lyricist: "Eknath"
director: "Ganeshaa"
path: "/albums/thimiru-pudichavan-lyrics"
song: "Nee Unnai Matrikondal"
image: ../../images/albumart/thimiru-pudichavan.jpg
date: 2018-11-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_bLVNoBlzxU"
type: "mass"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee unnai maatrikondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee unnai maatrikondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ennam thuimaikondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ennam thuimaikondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyaram koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram koodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochi viduvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochi viduvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muyarchi seithu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyarchi seithu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam ellaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam ellaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasatchiyai thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasatchiyai thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatti vai arugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatti vai arugil"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu enbathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu enbathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam seithidum seyalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam seithidum seyalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee unnai maatrikondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee unnai maatrikondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ennam thuimaikondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ennam thuimaikondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyaram koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram koodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellorum nallavan thaan pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellorum nallavan thaan pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozhal thaan aakkuthu thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozhal thaan aakkuthu thappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirunthida vaippugal vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirunthida vaippugal vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbavum maaruvaan top-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbavum maaruvaan top-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee seiyira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee seiyira"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaiyae kadavulada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaiyae kadavulada"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vetrikku thozhvi thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vetrikku thozhvi thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Soothiram da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soothiram da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thelivaga innikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thelivaga innikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Velai senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai senja"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum thadaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum thadaigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaikku thanduva da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaikku thanduva da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee unnai maatrikondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee unnai maatrikondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ennam thuimaikondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ennam thuimaikondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyaram koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram koodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbaalae eerkalam oorai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaalae eerkalam oorai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthaviyae vangalaam perai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthaviyae vangalaam perai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurai solli vazhathae endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurai solli vazhathae endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi meethu kai kaatta thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi meethu kai kaatta thondrum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhiyingu vazhkaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyingu vazhkaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaiyae da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaiyae da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhikamal siguchaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhikamal siguchaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nermaiyil nambikkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nermaiyil nambikkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechikka da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechikka da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini athu unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini athu unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkumae uchchiladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkumae uchchiladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee unnai maatrikondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee unnai maatrikondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ennam thuimaikondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ennam thuimaikondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyaram koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram koodum"/>
</div>
</pre>
