---
title: "ekkachekka kushiyo song lyrics"
album: "Yemaali"
artist: "Sam D. Raj"
lyricist: "Mohan Rajan"
director: "V.Z. Durai"
path: "/albums/yemaali-lyrics"
song: "Ekkachekka Kushiyo"
image: ../../images/albumart/yemaali.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gwTDhHfbvNU"
type: "love"
singers:
  - Cliffy
  - Hemambika
  - Velu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ekkachaka Kushiyo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkachaka Kushiyo "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Nee Serndhaaye Natpe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nee Serndhaaye Natpe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarparikum Express Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarparikum Express Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Kadhalil Moozhga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Kadhalil Moozhga "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Seidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Seidhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Eerthaai Nee Michame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Eerthaai Nee Michame "/>
</div>
<div class="lyrico-lyrics-wrapper">Illaamal Karaindhen Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaamal Karaindhen Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kasadara Kadhalai Kaatum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasadara Kadhalai Kaatum "/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Vizhigalai Virumbugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Vizhigalai Virumbugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalapada Unarvinai Ootum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalapada Unarvinai Ootum "/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Viralgalai Virumbugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Viralgalai Virumbugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Idam Porul Yevalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Idam Porul Yevalai "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandi Unai Ninaithen Oyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi Unai Ninaithen Oyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thedaamal Irukiren Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thedaamal Irukiren Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaindhadhum Therindhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaindhadhum Therindhume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Tholviyaa Thorkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Tholviyaa Thorkum "/>
</div>
<div class="lyrico-lyrics-wrapper">Vetriyaa En Nenjai Ketkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriyaa En Nenjai Ketkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravasamum Kuraiyumo Anudhinam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravasamum Kuraiyumo Anudhinam "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enai Anaikayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enai Anaikayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadum Vishamum Kasakumo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadum Vishamum Kasakumo "/>
</div>
<div class="lyrico-lyrics-wrapper">Aruginil Nee Dhinam Irukayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginil Nee Dhinam Irukayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Paarvai Veesu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Paarvai Veesu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thedinene Nee En Paadhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thedinene Nee En Paadhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Sugavali Thotu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Sugavali Thotu "/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithidu Naan Unn Meedhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithidu Naan Unn Meedhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Seidhuvittaai Endru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Seidhuvittaai Endru "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Naan Ketkindren Naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Naan Ketkindren Naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vendru Vittaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vendru Vittaai "/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Enakul Paarthene Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Enakul Paarthene Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siragai Izhandhaalume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai Izhandhaalume "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Paravai Enbome Podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Paravai Enbome Podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavia Pirindhaalume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavia Pirindhaalume "/>
</div>
<div class="lyrico-lyrics-wrapper">Oer Iravu Saagaadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oer Iravu Saagaadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu Karaidhaalume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu Karaidhaalume "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Idhayam Marakaadhu Vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Idhayam Marakaadhu Vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuraigal Veliyeriye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuraigal Veliyeriye "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal Kuraindhu Pogaadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Kuraindhu Pogaadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aimbulanum Adangumo Aruginil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aimbulanum Adangumo Aruginil "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Dhinam Irukayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhinam Irukayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiviralum Asarumo Anudhinam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiviralum Asarumo Anudhinam "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enai Anaikayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enai Anaikayil"/>
</div>
</pre>
