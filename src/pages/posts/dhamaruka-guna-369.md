---
title: "dhamaruka song lyrics"
album: "Guna 369"
artist: "Chaitan Bharadwaj"
lyricist: "Shubam Viswanath"
director: "Arjun Jandyala"
path: "/albums/guna-369-lyrics"
song: "Dhamaruka"
image: ../../images/albumart/guna-369.jpg
date: 2019-08-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/GXDdpvYdUZY"
type: "mass"
singers:
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dhamaruka Dhamaruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhamaruka Dhamaruka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhamaruka Baaje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhamaruka Baaje"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhamara Dhamara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhamara Dhamara"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum Dum Dum De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum Dum Dum De"/>
</div>
<div class="lyrico-lyrics-wrapper">Pralayam Vilayam Parugulu Pedithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pralayam Vilayam Parugulu Pedithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Balira Balira Ki Jaga Chorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balira Balira Ki Jaga Chorey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhamaruka Dhamaruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhamaruka Dhamaruka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhamaruka Baaje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhamaruka Baaje"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhamara Dhamara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhamara Dhamara"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum Dum Dum De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum Dum Dum De"/>
</div>
<div class="lyrico-lyrics-wrapper">Urumulu Merupulu Urkulu Pedithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urumulu Merupulu Urkulu Pedithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Balira Balira Asuaraakshukurey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balira Balira Asuaraakshukurey"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraathakaale Krushinchipoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraathakaale Krushinchipoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Trishoola Rudhrudu Thegabadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trishoola Rudhrudu Thegabadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Araachakale Harinchipova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Araachakale Harinchipova"/>
</div>
<div class="lyrico-lyrics-wrapper">Trinetrudinka Yagabadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trinetrudinka Yagabadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhuragathaale Dhahinchipova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuragathaale Dhahinchipova"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhayaamayudilo Sega Pudithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhayaamayudilo Sega Pudithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinaashanale Nashinchipova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaashanale Nashinchipova"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishaala Hrudhuyudu Pagapadithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishaala Hrudhuyudu Pagapadithey"/>
</div>
<div class="lyrico-lyrics-wrapper">Bali Bali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bali Bali"/>
</div>
<div class="lyrico-lyrics-wrapper">Bali Bali Bali Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bali Bali Bali Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Bali Bali Bala Bala Bali Bali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bali Bali Bala Bala Bali Bali"/>
</div>
<div class="lyrico-lyrics-wrapper">Bali Bali Bali Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bali Bali Bali Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Bali Bali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bali Bali"/>
</div>
<div class="lyrico-lyrics-wrapper">Bali Bali Bala Bala Bali Bali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bali Bali Bala Bala Bali Bali"/>
</div>
<div class="lyrico-lyrics-wrapper">Bali Bali Bali Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bali Bali Bali Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Bali Bali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bali Bali"/>
</div>
<div class="lyrico-lyrics-wrapper">Bali Bali Bala Bala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bali Bali Bala Bala"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhikkula Charanamidhii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhikkula Charanamidhii"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhurguna Haranamidhii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhurguna Haranamidhii"/>
</div>
<div class="lyrico-lyrics-wrapper">Jataa Jootude Paraakraminchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jataa Jootude Paraakraminchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Smashaana Kshetramidhii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smashaana Kshetramidhii"/>
</div>
<div class="lyrico-lyrics-wrapper">Nischala Pranavamidhii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nischala Pranavamidhii"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirbhaya Natanamidhii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirbhaya Natanamidhii"/>
</div>
<div class="lyrico-lyrics-wrapper">Halaahalaanne Skalinchanunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halaahalaanne Skalinchanunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheekshuni Rowdramidhii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheekshuni Rowdramidhii"/>
</div>
</pre>
