---
title: "meri rider rider song lyrics"
album: "Punya Paap"
artist: "Kanch - Stunnah Beatz"
lyricist: "DIVINE - Lisa Mishra"
director: "Gil Green"
path: "/albums/punya-paap-lyrics"
song: "Meri Rider Rider"
image: ../../images/albumart/punya-paap.jpg
date: 2020-12-03
lang: hindi
youtubeLink: "https://www.youtube.com/embed/voZv9trgIv0"
type: "happy"
singers:
  - DIVINE
  - Lisa Mishra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aa tu hi meri rider rider rider
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa tu hi meri rider rider rider"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi meri dost
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi meri dost"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm take you higher higher higher
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm take you higher higher higher"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakh mujhe close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakh mujhe close"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikal gaya main slums se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikal gaya main slums se"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bhi karun main sab mann se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bhi karun main sab mann se"/>
</div>
<div class="lyrico-lyrics-wrapper">Sab hard mere bande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab hard mere bande"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun khafa hai tu humse?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun khafa hai tu humse?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa tu hi meri rider rider rider
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa tu hi meri rider rider rider"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi meri dost
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi meri dost"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm take you higher higher higher
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm take you higher higher higher"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakh mujhe close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakh mujhe close"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaad aati mujhe neend mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaad aati mujhe neend mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Savera jab uthe tera bhai phir bhi feel mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savera jab uthe tera bhai phir bhi feel mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Khoobhsurti uske gene me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khoobhsurti uske gene me"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandi hawa nikalte paseene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandi hawa nikalte paseene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye asliyat ya dream hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye asliyat ya dream hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya mеra sirf naseeb hai(ha ha)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya mеra sirf naseeb hai(ha ha)"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye asliyat ya dream hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye asliyat ya dream hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya mera sirf naseeb hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya mera sirf naseeb hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aisi ladki jo khud karе grind
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisi ladki jo khud karе grind"/>
</div>
<div class="lyrico-lyrics-wrapper">Jab pooche usse time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab pooche usse time"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh bolti humara hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh bolti humara hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo check kare line
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo check kare line"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe puche if I'm fine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe puche if I'm fine"/>
</div>
<div class="lyrico-lyrics-wrapper">Puchha kya khaya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puchha kya khaya hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye hit & run wala koyi case nahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye hit & run wala koyi case nahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye police chor wala koyi chase nahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye police chor wala koyi chase nahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">We can never be friends, we can never just pretend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We can never be friends, we can never just pretend"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa tu hi meri rider rider rider
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa tu hi meri rider rider rider"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi meri dost
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi meri dost"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm take you higher higher higher
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm take you higher higher higher"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakh mujhe close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakh mujhe close"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa tu hi meri rider rider rider
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa tu hi meri rider rider rider"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi meri dost
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi meri dost"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm take you higher higher higher
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm take you higher higher higher"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakh mujhe close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakh mujhe close"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I know you want me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I know you want me"/>
</div>
<div class="lyrico-lyrics-wrapper">It's like every time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It's like every time"/>
</div>
<div class="lyrico-lyrics-wrapper">I walk by the room starts to spin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I walk by the room starts to spin"/>
</div>
<div class="lyrico-lyrics-wrapper">It's getting heavy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It's getting heavy"/>
</div>
<div class="lyrico-lyrics-wrapper">But I ain't comin over
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But I ain't comin over"/>
</div>
<div class="lyrico-lyrics-wrapper">Till you got that gin-gin-gin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Till you got that gin-gin-gin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunna mera dost tujhe pyaar karna roz
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunna mera dost tujhe pyaar karna roz"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi mera rider mujhe rakhna tujhe close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi mera rider mujhe rakhna tujhe close"/>
</div>
<div class="lyrico-lyrics-wrapper">Higher higher sirf hai tere saath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Higher higher sirf hai tere saath"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh doosre saare ladke hain pareshaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh doosre saare ladke hain pareshaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Independent mein karti mera tu tera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Independent mein karti mera tu tera"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum dono hai way up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum dono hai way up"/>
</div>
<div class="lyrico-lyrics-wrapper">Jisko jo kehna wo kahe na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jisko jo kehna wo kahe na"/>
</div>
<div class="lyrico-lyrics-wrapper">Lena na dena we way up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lena na dena we way up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu hi mera last tu hi pehla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi mera last tu hi pehla"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi mera raat tu savera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi mera raat tu savera"/>
</div>
<div class="lyrico-lyrics-wrapper">Parivaar lage jab tu haath pakde mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parivaar lage jab tu haath pakde mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal saath de tu mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal saath de tu mera"/>
</div>
<div class="lyrico-lyrics-wrapper">We way up we way up we way up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We way up we way up we way up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu hi mera rider rider rider
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi mera rider rider rider"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi mera dost
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi mera dost"/>
</div>
<div class="lyrico-lyrics-wrapper">I'll take you higher higher higher higher
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'll take you higher higher higher higher"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakh mujhe close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakh mujhe close"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu hi mera rider rider rider
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi mera rider rider rider"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu hi mera dost
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu hi mera dost"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm take you higher higher higher
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm take you higher higher higher"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakh mujhe close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakh mujhe close"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You can keep me close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You can keep me close"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm a rider riding witcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm a rider riding witcha"/>
</div>
<div class="lyrico-lyrics-wrapper">You can keep me close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You can keep me close"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm a rider riding way up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm a rider riding way up"/>
</div>
<div class="lyrico-lyrics-wrapper">You can keep me close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You can keep me close"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm a rider riding witcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm a rider riding witcha"/>
</div>
<div class="lyrico-lyrics-wrapper">You can keep me close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You can keep me close"/>
</div>
<div class="lyrico-lyrics-wrapper">I'm a rider riding way up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'm a rider riding way up"/>
</div>
</pre>
