---
title: "annana thaalaattum song lyrics"
album: "Maaran"
artist: "GV Prakash Kumar"
lyricist: "Vivek"
director: "Karthick Naren"
path: "/albums/maaran-lyrics"
song: "Annana Thaalaattum"
image: ../../images/albumart/maaran.jpg
date: 2022-03-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dviM9cRh8R0"
type: "sentiment"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Annana Thaalaattum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annana Thaalaattum "/>
</div>
<div class="lyrico-lyrics-wrapper">Annai Madi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai Madi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithira Poove En 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithira Poove En "/>
</div>
<div class="lyrico-lyrics-wrapper">Chellamadi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamadi Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannellam Neeyaagum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannellam Neeyaagum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Madhi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Madhi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalame Ponaalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalame Ponaalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Pillai Mozhi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillai Mozhi Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Simittum Kanna Paaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Simittum Kanna Paaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuduthu Vachen Naa Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuduthu Vachen Naa Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Thandha Vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Thandha Vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaku Mattum Dhan Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaku Mattum Dhan Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thonayinnu Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thonayinnu Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Solli Kooda Irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Kooda Irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nejathula En Thona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nejathula En Thona "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Dhaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Poo Thanndi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Poo Thanndi "/>
</div>
<div class="lyrico-lyrics-wrapper">En Rassathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Rassathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannula Lesa Kanneraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannula Lesa Kanneraa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kannu Thoovum Kadal Neeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannu Thoovum Kadal Neeraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Korayaadha Paasam Oru Aaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korayaadha Paasam Oru Aaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhenandhorum Yerum Pala Nooraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhenandhorum Yerum Pala Nooraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thitti Thitti Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thitti Thitti Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Nadichiupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Nadichiupen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thendral Adicha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral Adicha "/>
</div>
<div class="lyrico-lyrics-wrapper">Kuda Thudichirupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuda Thudichirupen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Vida Pala Neram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vida Pala Neram "/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Nenappen hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nenappen hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppul Kodiyoda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppul Kodiyoda "/>
</div>
<div class="lyrico-lyrics-wrapper">Otti Vandha Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti Vandha Poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennuyire Nee Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennuyire Nee Thaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annana Thaalaattum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annana Thaalaattum "/>
</div>
<div class="lyrico-lyrics-wrapper">Annai Madi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai Madi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithira Poove En 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithira Poove En "/>
</div>
<div class="lyrico-lyrics-wrapper">Chellamadi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamadi Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Annana Thaalattum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annana Thaalattum "/>
</div>
<div class="lyrico-lyrics-wrapper">Annai Madi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai Madi Nee"/>
</div>
</pre>
