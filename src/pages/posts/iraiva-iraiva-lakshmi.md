---
title: "iraiva iraiva song lyrics"
album: "Lakshmi"
artist: "Sam CS"
lyricist: "Madhan Karky"
director: "AL Vijay"
path: "/albums/lakshmi-lyrics"
song: "Iraiva Iraiva"
image: ../../images/albumart/lakshmi.jpg
date: 2018-08-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/49MPtLWcKgQ"
type: "happy"
singers:
  - Sam CS
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yei iraivanae iraivanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei iraivanae iraivanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Untham arul pozhivaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untham arul pozhivaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil nirainthae vazhivaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil nirainthae vazhivaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul nee nirainthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul nee nirainthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai vida yethai yethai naan ketpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai vida yethai yethai naan ketpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee varuvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee varuvaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarayo vaarayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarayo vaarayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkoru karamthara vaarayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkoru karamthara vaarayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarayo paarayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarayo paarayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkena vizhum thuli paarayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena vizhum thuli paarayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerayo theerayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerayo theerayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodiyinil nedunthuyar theerayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodiyinil nedunthuyar theerayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarayo thaarayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarayo thaarayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil eriporul thaarayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathil eriporul thaarayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadai neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadai neethanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En medai neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En medai neethanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul ennullae aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul ennullae aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam neethanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalgal neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgal neethanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaatrum neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaatrum neethanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul ennullae odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul ennullae odum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottam neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oottam neethanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulil vizhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulil vizhuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiyinil surundae azhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiyinil surundae azhuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruvanae unaiyae thozhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruvanae unaiyae thozhuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Karam thara udanae ezhuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karam thara udanae ezhuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisaikodu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisaikodu iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaithidu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaithidu iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaithidu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaithidu iraiva iraiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarayo vaarayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarayo vaarayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkoru karamthara vaarayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkoru karamthara vaarayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarayo paarayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarayo paarayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkena vizhum thuli paarayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena vizhum thuli paarayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadaigalae kidaiyaathae enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalae kidaiyaathae enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Niruthida theriyaathae enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niruthida theriyaathae enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaivathu ellamae unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaivathu ellamae unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ennullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ennullae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvarai nadanam un poruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvarai nadanam un poruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivenum nadanam un poruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivenum nadanam un poruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvarai manamengum neruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvarai manamengum neruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethanae en iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethanae en iraiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulaai irulaai irulathan pularaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulaai irulaai irulathan pularaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pularaai pularaai pularathan kathiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pularaai pularaai pularathan kathiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathiraai kathiraai kathirathan oliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathiraai kathiraai kathirathan oliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyaai oliyaai manadhinil niraivaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyaai oliyaai manadhinil niraivaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En paadham vaikindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paadham vaikindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththangal unthan sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththangal unthan sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannil pookkindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannil pookkindra"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbamgal unthan sondham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbamgal unthan sondham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En nenjil nee ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjil nee ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththangal unthan sondham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththangal unthan sondham"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaanum kaithattal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaanum kaithattal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvondrum unthan sondham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvondrum unthan sondham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisaikodu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisaikodu iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaithidu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaithidu iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaithidu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaithidu iraiva iraiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisaikodu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisaikodu iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaithidu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaithidu iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaithidu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaithidu iraiva iraiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisaikodu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisaikodu iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaithidu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaithidu iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaithidu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaithidu iraiva iraiva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisaikodu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisaikodu iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaithidu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaithidu iraiva iraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaithidu iraiva iraiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaithidu iraiva iraiva"/>
</div>
</pre>
