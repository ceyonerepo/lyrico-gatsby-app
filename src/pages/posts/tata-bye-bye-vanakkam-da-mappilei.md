---
title: "tata bye bye song lyrics"
album: "Vanakkam Da Mappilei"
artist: "G V Prakash Kumar"
lyricist: "Gaana Vinoth"
director: "M. Rajesh"
path: "/albums/vanakkam-da-mappilei-song-lyrics"
song: "Tata Bye Bye"
image: ../../images/albumart/vanakkam-da-mappilei.jpg
date: 2021-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/12csmYbMHp4"
type: "Love Sad"
singers:
  - Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En love oru top kilinja share auto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En love oru top kilinja share auto"/>
</div>
<div class="lyrico-lyrics-wrapper">En heart-u ippo gas-u pona beer-aatom beer-aatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En heart-u ippo gas-u pona beer-aatom beer-aatom"/>
</div>
<div class="lyrico-lyrics-wrapper">En love oru top kilinja share auto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En love oru top kilinja share auto"/>
</div>
<div class="lyrico-lyrics-wrapper">En heart-u ippo gas-u pona beer-aatom beer-aatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En heart-u ippo gas-u pona beer-aatom beer-aatom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeduthona vachithiyema natpula kaiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeduthona vachithiyema natpula kaiyu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyyo inime nee vena enakku tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyo inime nee vena enakku tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeduthona vachithiyema natpula kaiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeduthona vachithiyema natpula kaiyu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyyo inime nee vena enakku tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyo inime nee vena enakku tata bye bye-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollama kaldukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollama kaldukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadi un manners-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadi un manners-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiye kalaichu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiye kalaichu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Poduriye status-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poduriye status-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love-u’la thothavannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-u’la thothavannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayitendi famous-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayitendi famous-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthugula kuthtiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthugula kuthtiye"/>
</div>
<div class="lyrico-lyrics-wrapper">You too brutus-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You too brutus-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan thedi poiyi kadhal vanthuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thedi poiyi kadhal vanthuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nejama iruntha enakku halwa thanthuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nejama iruntha enakku halwa thanthuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam katchiyellam kanava pochudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam katchiyellam kanava pochudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattaayam panni vantha kadhal waste-u di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaayam panni vantha kadhal waste-u di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna suththunom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna suththunom"/>
</div>
<div class="lyrico-lyrics-wrapper">Duet paadunom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duet paadunom"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-ah jeikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-ah jeikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee solo aaganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee solo aaganum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That is life machi!!!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That is life machi!!!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alphonsa gilphonsa alphonsale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alphonsa gilphonsa alphonsale"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey gilphonsa alphonsa gilphonsale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey gilphonsa alphonsa gilphonsale"/>
</div>
<div class="lyrico-lyrics-wrapper">Alphonsa gilphonsa alphonsale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alphonsa gilphonsa alphonsale"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey gilphonsa alphonsa gilphonsale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey gilphonsa alphonsa gilphonsale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnu set-aachina wheelin panni kaattatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu set-aachina wheelin panni kaattatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Marunaal puttukuna hour-u cycle oottatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marunaal puttukuna hour-u cycle oottatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha manasunnu freeya eduthu neettadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha manasunnu freeya eduthu neettadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai mathikkum un friend vittu odatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai mathikkum un friend vittu odatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maratha valarthaaka nizhalu minjunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maratha valarthaaka nizhalu minjunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-ah valarthaaka adhu un end-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-ah valarthaaka adhu un end-u da"/>
</div>
<div class="lyrico-lyrics-wrapper">Saabam geebamlam time-u waste-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saabam geebamlam time-u waste-u da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla irumaanu vaazhthu paaduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla irumaanu vaazhthu paaduda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal vanthu yen kanna kattuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal vanthu yen kanna kattuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjan asanthathum ongi kottuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjan asanthathum ongi kottuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee case-u close-aayi!!!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee case-u close-aayi!!!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En love oru, love oru…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En love oru, love oru…"/>
</div>
<div class="lyrico-lyrics-wrapper">En love oru top kilinja share auto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En love oru top kilinja share auto"/>
</div>
<div class="lyrico-lyrics-wrapper">En heart-u ippo gas-u pona beer-aatom beer-aatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En heart-u ippo gas-u pona beer-aatom beer-aatom"/>
</div>
<div class="lyrico-lyrics-wrapper">En love oru top kilinja share auto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En love oru top kilinja share auto"/>
</div>
<div class="lyrico-lyrics-wrapper">En heart-u ippo gas-u pona beer-aatom beer-aatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En heart-u ippo gas-u pona beer-aatom beer-aatom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeduthona vachithiyema natpula kaiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeduthona vachithiyema natpula kaiyu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyyo inime nee vena enakku tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyo inime nee vena enakku tata bye bye-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey tata bye bye-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tata bye bye-u"/>
</div>
</pre>
