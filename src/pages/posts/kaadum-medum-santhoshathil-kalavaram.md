---
title: "kaadum medum song lyrics"
album: "Santhoshathil Kalavaram"
artist: "Sivanag"
lyricist: "Priyan"
director: "Kranthi Prasad"
path: "/albums/santhoshathil-kalavaram-lyrics"
song: "Kaadum Medum"
image: ../../images/albumart/santhoshathil-kalavaram.jpg
date: 2018-11-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EAORBSKXqyM"
type: "sad"
singers:
  - D Sathyaprakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kaadum medum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadum medum"/>
</div>
<div class="lyrico-lyrics-wrapper">munne neeluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne neeluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">idari oodum kaalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idari oodum kaalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">oointhe poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oointhe poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">vithi inge vilaiyaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithi inge vilaiyaada"/>
</div>
<div class="lyrico-lyrics-wrapper">karai enge veliyera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karai enge veliyera"/>
</div>
<div class="lyrico-lyrics-wrapper">thadumaari thadam maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadumaari thadam maari"/>
</div>
<div class="lyrico-lyrics-wrapper">vali thedum manathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali thedum manathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaadum medum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadum medum"/>
</div>
<div class="lyrico-lyrics-wrapper">munne neeluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne neeluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">idari oodum kaalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idari oodum kaalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">oointhe poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oointhe poguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naaku varandu pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naaku varandu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">kudika thanne venum viki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudika thanne venum viki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sooriyan maraiyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sooriyan maraiyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">irutile idhayam miralgirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irutile idhayam miralgirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">paathaigal thavaruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathaigal thavaruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kannile kanavugal kalaigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannile kanavugal kalaigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">megame kai kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megame kai kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">malaigalin maru puram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaigalin maru puram"/>
</div>
<div class="lyrico-lyrics-wrapper">kadathi vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadathi vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaname vali vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaname vali vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">engalai innum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalai innum "/>
</div>
<div class="lyrico-lyrics-wrapper">konjam vaala vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam vaala vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">manam inge siraiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam inge siraiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">udal inge ranamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal inge ranamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">payam koodi manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payam koodi manam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadi vidiyum manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi vidiyum manasu"/>
</div>
</pre>
