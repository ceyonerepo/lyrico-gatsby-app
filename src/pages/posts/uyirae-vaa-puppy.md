---
title: "uyirae vaa song lyrics"
album: "Puppy"
artist: "Dharan Kumar"
lyricist: "Mirchi Vijay"
director: "Nattu Dev"
path: "/albums/puppy-lyrics"
song: "Uyirae Vaa"
image: ../../images/albumart/puppy.jpg
date: 2019-10-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/e8shfRBT61M"
type: "sad"
singers:
  - Gautham Menon
  - Dharan Kumar
  - Alisha Thomas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru Nodiyinil Naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nodiyinil Naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilai Thavariyathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilai Thavariyathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedunaal Vaazhvae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedunaal Vaazhvae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulaai Maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulaai Maara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivugal Moththam Neeyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal Moththam Neeyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Nirambiyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Nirambiyathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhuvaai Medhuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuvaai Medhuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Perugiyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Perugiyathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakullae Adi Naan Indru Kaadhal Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakullae Adi Naan Indru Kaadhal Kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan Porul Indru Meiyaanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Porul Indru Meiyaanadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Vegu Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Vegu Thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaaga Nee Sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga Nee Sella"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae Oru Nodiyae Enai Kolgirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Oru Nodiyae Enai Kolgirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirae Vaa Kaathurindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae Vaa Kaathurindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaiyae Naan Edhir Paathurindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaiyae Naan Edhir Paathurindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae Vaa Kaathurippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae Vaa Kaathurippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppen Iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppen Iruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Urasa Urasa Ninaivu Urasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasa Urasa Ninaivu Urasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nodiyinil Naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nodiyinil Naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilai Thavariyathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilai Thavariyathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedunaal Vaazhvae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedunaal Vaazhvae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulaai Maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulaai Maara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sariyo Thavaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyo Thavaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera Vazhi Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Vazhi Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Idhuvo Illa Mudivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Idhuvo Illa Mudivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Theriyum Sariya Thavara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Theriyum Sariya Thavara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unna Kekala Nee Solluva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unna Kekala Nee Solluva"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Udalaai Neeyum Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Udalaai Neeyum Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Uyiraai Inainthomnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Uyiraai Inainthomnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluvanu Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluvanu Theriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadandhen Kalaindhen Tholaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandhen Kalaindhen Tholaindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Ninaivil Thavithen Sithaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Ninaivil Thavithen Sithaindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalai Ennul Sumandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalai Ennul Sumandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Ninaithen Unnai Ninaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Ninaithen Unnai Ninaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Unnai Niraithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Unnai Niraithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmm Sariyo Thavaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm Sariyo Thavaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Ver Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Ver Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvum Illai Mudivae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Illai Mudivae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sariya Thavara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariya Thavara"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Ketkalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Ketkalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Udalaai Ingu Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Udalaai Ingu Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Uyirai Inaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Uyirai Inaindhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirae Vaa Kaathurindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae Vaa Kaathurindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaiyae Naan Edhir Paathurindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaiyae Naan Edhir Paathurindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae Vaa Kaathurippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae Vaa Kaathurippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppen Iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppen Iruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Urasa Urasa Ninaivu Urasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasa Urasa Ninaivu Urasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nodiyinil Naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nodiyinil Naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilai Thavariyathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilai Thavariyathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedunaal Vaazhvae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedunaal Vaazhvae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulaai Maara Irulaai Maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulaai Maara Irulaai Maara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivugal Moththam Neeyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal Moththam Neeyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Nirambiyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Nirambiyathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Nirambiyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Nirambiyathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhuvaai Medhuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuvaai Medhuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Perugiyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Perugiyathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakullae Adi Naan Ingu Kaadhal Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakullae Adi Naan Ingu Kaadhal Kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan Porul Indru Meiyaanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Porul Indru Meiyaanadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Vegu Thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Vegu Thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaaga Nee Sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga Nee Sella"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae Oru Nodiyum Enai Kolgirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Oru Nodiyum Enai Kolgirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm Uyirae Vaa Kaathurindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Uyirae Vaa Kaathurindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaiyae Naan Edhir Paathurindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaiyae Naan Edhir Paathurindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae Vaa Kaathurippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae Vaa Kaathurippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Urasa Urasa Ninaivu Urasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasa Urasa Ninaivu Urasa"/>
</div>
</pre>
