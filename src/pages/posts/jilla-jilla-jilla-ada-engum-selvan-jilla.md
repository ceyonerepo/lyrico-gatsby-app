---
title: "jilla jilla jilla ada engum selvan song lyrics"
album: "Jilla"
artist: "D. Imman"
lyricist: "Viveka"
director: "Nesan"
path: "/albums/jilla-song-lyrics"
song: "Jilla Jilla Jilla Ada Engum Selvan"
image: ../../images/albumart/jilla.jpg
date: 2014-01-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fgnY5K5dHNU"
type: "Theme Song"
singers:
  - Anand
  - Deepak
  - Santhosh Hariharan
  - Shenbagaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jilla Jilla Jilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Jilla Jilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Engum Selvan Dhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Engum Selvan Dhilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilla Jilla Jilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Jilla Jilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Ethayum Velvan Dhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ethayum Velvan Dhilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veppam Neendhum Theppam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veppam Neendhum Theppam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena Vizhigal Kondavan Jilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Vizhigal Kondavan Jilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vengai Pola Paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vengai Pola Paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Vegam Kondavan Jilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Vegam Kondavan Jilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilla Jilla Jilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Jilla Jilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilla Jilla Jilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Jilla Jilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi Paarvai Paayum Mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Paarvai Paayum Mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilla Jilla Jilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Jilla Jilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagai Sitharum Sillu Silla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagai Sitharum Sillu Silla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anja Nenja Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anja Nenja Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan Per Dhan Inge Jilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Per Dhan Inge Jilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotta Boomi Adhirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Boomi Adhirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Thuya Veeran Jilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thuya Veeran Jilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Vizhi Aruginil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Vizhi Aruginil"/>
</div>
<div class="lyrico-lyrics-wrapper">Erimalai Vedikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erimalai Vedikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithathai Mudithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithathai Mudithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalum Thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalum Thudikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhumbugal Udaipada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhumbugal Udaipada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayamaigal Sitharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayamaigal Sitharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanudan Poorida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanudan Poorida"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukkum Udharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukkum Udharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhi Parithavanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhi Parithavanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruthiyil Nanaivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruthiyil Nanaivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhan Kedupavanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhan Kedupavanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudal Uruviduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudal Uruviduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Thaduthavanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Thaduthavanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathai Kuriduvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathai Kuriduvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vana Puli Sinathudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vana Puli Sinathudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Valam Dhinam Varuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valam Dhinam Varuvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilla Jilla Jilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Jilla Jilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ratham Engum Satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ratham Engum Satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilla Jilla Jilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Jilla Jilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Sitham Ellam Yutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sitham Ellam Yutham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pooti Endru Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooti Endru Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Eeti Aaga Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Eeti Aaga Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu Kaatru Mothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Kaatru Mothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum Thadaigal Ellam Saayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum Thadaigal Ellam Saayum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilla Jilla Jilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Jilla Jilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Jilla Jilla Jilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla Jilla Jilla"/>
</div>
</pre>
