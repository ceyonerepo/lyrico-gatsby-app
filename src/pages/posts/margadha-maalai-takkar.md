---
title: 'maragadha maalai song lyrics'
album: 'Takkar'
artist: 'Nivas K. Prasanna'
lyricist: 'Uma Devi'
director: 'Karthik G Krish'
path: '/albums/takkar-song-lyrics'
song: 'Maragadha Maalai'
image: ../../images/albumart/takkar.jpg
date: 2020-01-01
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/dJeoD5Sk0PA'
type: 'romantic'
singers: 
- Pradeep Kumar
- Vijay Yesudas 
- Chinmayi
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Maragatha maalai neram
<input type="checkbox" class="lyrico-select-lyric-line" value="Maragatha maalai neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamathaigal maindhu veezhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mamathaigal maindhu veezhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Magarandha serkai kaadhal dhaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Magarandha serkai kaadhal dhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Iravinil thotra theeyai
<input type="checkbox" class="lyrico-select-lyric-line" value="Iravinil thotra theeyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugida paarkum paarvai
<input type="checkbox" class="lyrico-select-lyric-line" value="Parugida paarkum paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhivathu kaadhal theertham dhaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhivathu kaadhal theertham dhaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaarthaigal thorkudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaarthaigal thorkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendalae tharum mozhi neeyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Theendalae tharum mozhi neeyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dhoorangal ketkuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhoorangal ketkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalin vazhithunai neeyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhalin vazhithunai neeyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hoo hoo hoo hoo hoo oo …oo hoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo hoo hoo hoo hoo oo …oo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hoo hoo hoo hoo oo …oo hoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo hoo hoo hoo hoo oo …oo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthidavaa….idhazh variya
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhuthidavaa….idhazh variya"/>
</div>
  <div class="lyrico-lyrics-wrapper">Idaiveli dhaan…
<input type="checkbox" class="lyrico-select-lyric-line" value="Idaiveli dhaan…"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen uyir valiya..aa..aa…aa..
<input type="checkbox" class="lyrico-select-lyric-line" value="Pen uyir valiya..aa..aa…aa.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Neer ketten
<input type="checkbox" class="lyrico-select-lyric-line" value="Neer ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyaaga vaan ketten
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhaiyaaga vaan ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavaaga nee endhan
<input type="checkbox" class="lyrico-select-lyric-line" value="Nilavaaga nee endhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavaaga thaedi vandhadhenna
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanavaaga thaedi vandhadhenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naan ketta
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamaaga nee vandhaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Varamaaga nee vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamaaga naam endrum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nijamaaga naam endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaaga kaalam serthadhenna
<input type="checkbox" class="lyrico-select-lyric-line" value="Uravaaga kaalam serthadhenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru vaanam udainthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru vaanam udainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru vaanam varuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Iru vaanam varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhi thoongum iravil
<input type="checkbox" class="lyrico-select-lyric-line" value="Ozhi thoongum iravil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkal poopadhenna
<input type="checkbox" class="lyrico-select-lyric-line" value="Pookkal poopadhenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mazhai yaavum vadinthum
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhai yaavum vadinthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marathooral varumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Marathooral varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru yaamam mudindhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru yaamam mudindhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodal thorpadhenna
<input type="checkbox" class="lyrico-select-lyric-line" value="Oodal thorpadhenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nadhi neeyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadhi neeyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli naanaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thuli naanaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Kalandhingae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalandhingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal aaguthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal aaguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ezhuthidavaa….aa…aa…aa…aa..aa..
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhuthidavaa….aa…aa…aa…aa..aa.."/>
</div>
  <div class="lyrico-lyrics-wrapper">Idhazh variya
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhazh variya"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiveli dhaan…
<input type="checkbox" class="lyrico-select-lyric-line" value="Idaiveli dhaan…"/>
</div>
  <div class="lyrico-lyrics-wrapper">Pen uyir valiya…..aa…aa…aa…aa..aa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Pen uyir valiya…..aa…aa…aa…aa..aa…"/>
</div>
</pre>