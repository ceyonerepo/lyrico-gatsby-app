---
title: "sethaanda sekaru song lyrics"
album: "Pandrikku Nandri Solli"
artist: "Suren Vikhash"
lyricist: "Vignesh Selvaraj"
director: "Bala Aran"
path: "/albums/pandrikku-nandri-solli-lyrics"
song: "Sethaanda Sekaru"
image: ../../images/albumart/pandrikku-nandri-solli.jpg
date: 2022-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cGX1FmoMg8k"
type: "happy"
singers:
  - Maria Kavita Thomas
  - Kids Chorus 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">oru kaatula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kaatula "/>
</div>
<div class="lyrico-lyrics-wrapper">kara metula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kara metula"/>
</div>
<div class="lyrico-lyrics-wrapper">nari kuzhiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nari kuzhiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">puli pasiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puli pasiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">nari nadunguthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nari nadunguthu "/>
</div>
<div class="lyrico-lyrics-wrapper">puli kiranguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puli kiranguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kita nerunguthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kita nerunguthu "/>
</div>
<div class="lyrico-lyrics-wrapper">kadika thayanguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadika thayanguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru kaatula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kaatula "/>
</div>
<div class="lyrico-lyrics-wrapper">kara metula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kara metula"/>
</div>
<div class="lyrico-lyrics-wrapper">nari kuzhiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nari kuzhiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">puli pasiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puli pasiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">nari nadunguthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nari nadunguthu "/>
</div>
<div class="lyrico-lyrics-wrapper">puli kiranguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puli kiranguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kita nerunguthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kita nerunguthu "/>
</div>
<div class="lyrico-lyrics-wrapper">kadika thayanguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadika thayanguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sethaanda sekaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethaanda sekaru"/>
</div>
<div class="lyrico-lyrics-wrapper">nikkama odiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikkama odiru"/>
</div>
<div class="lyrico-lyrics-wrapper">sethaanda sekaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethaanda sekaru"/>
</div>
<div class="lyrico-lyrics-wrapper">nikkama odiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikkama odiru"/>
</div>
<div class="lyrico-lyrics-wrapper">odiru hey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odiru hey "/>
</div>
<div class="lyrico-lyrics-wrapper">ka ka ka kadichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ka ka ka kadichu"/>
</div>
<div class="lyrico-lyrics-wrapper">ei ei ei ei idichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ei ei ei ei idichu"/>
</div>
<div class="lyrico-lyrics-wrapper">pop pop pop pop poruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pop pop pop pop poruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">ki ki ki ki kizhuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ki ki ki ki kizhuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">ka ka ka kadichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ka ka ka kadichu"/>
</div>
<div class="lyrico-lyrics-wrapper">ei ei ei ei idichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ei ei ei ei idichu"/>
</div>
<div class="lyrico-lyrics-wrapper">pop pop pop pop poruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pop pop pop pop poruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">karadiye kaari thupiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karadiye kaari thupiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">karadiye kaari thupiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karadiye kaari thupiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">aiming pandra just u miss u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiming pandra just u miss u"/>
</div>
<div class="lyrico-lyrics-wrapper">bullatiilla vethu vettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bullatiilla vethu vettu"/>
</div>
<div class="lyrico-lyrics-wrapper">innum iruntha eduthu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum iruntha eduthu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">karadiye kaari thupiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karadiye kaari thupiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">aiming pandra just u miss u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiming pandra just u miss u"/>
</div>
<div class="lyrico-lyrics-wrapper">bullatiilla vethu vettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bullatiilla vethu vettu"/>
</div>
<div class="lyrico-lyrics-wrapper">innum iruntha eduthu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum iruntha eduthu kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna suda variya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna suda variya "/>
</div>
<div class="lyrico-lyrics-wrapper">illa thoda variya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa thoda variya"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kola veriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kola veriya"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kolla variya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kolla variya"/>
</div>
<div class="lyrico-lyrics-wrapper">un palla udappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un palla udappen"/>
</div>
<div class="lyrico-lyrics-wrapper">un nakka aruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nakka aruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">nee bayam ariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee bayam ariyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oru paayum puliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru paayum puliya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna suda variya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna suda variya "/>
</div>
<div class="lyrico-lyrics-wrapper">illa thoda variya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa thoda variya"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kola veriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kola veriya"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kolla variya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kolla variya"/>
</div>
<div class="lyrico-lyrics-wrapper">un palla udappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un palla udappen"/>
</div>
<div class="lyrico-lyrics-wrapper">un nakka aruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nakka aruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">nee bayam ariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee bayam ariyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oru paayum puliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru paayum puliya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sethaanda sekaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethaanda sekaru"/>
</div>
<div class="lyrico-lyrics-wrapper">nikkaama odiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikkaama odiru"/>
</div>
<div class="lyrico-lyrics-wrapper">sethaanda sekaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethaanda sekaru"/>
</div>
<div class="lyrico-lyrics-wrapper">nikkaama odiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikkaama odiru"/>
</div>
<div class="lyrico-lyrics-wrapper">odiru hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odiru hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ka ka ka kadichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ka ka ka kadichu"/>
</div>
<div class="lyrico-lyrics-wrapper">ei ei ei ei idichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ei ei ei ei idichu"/>
</div>
<div class="lyrico-lyrics-wrapper">pop pop pop pop poruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pop pop pop pop poruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">ki ki ki ki kizhuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ki ki ki ki kizhuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">ka ka ka kadichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ka ka ka kadichu"/>
</div>
<div class="lyrico-lyrics-wrapper">ei ei ei ei idichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ei ei ei ei idichu"/>
</div>
<div class="lyrico-lyrics-wrapper">pop pop pop pop poruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pop pop pop pop poruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">karadiye kaari thupiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karadiye kaari thupiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">karadiye kaari thupiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karadiye kaari thupiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">aiming pandra just u miss u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiming pandra just u miss u"/>
</div>
<div class="lyrico-lyrics-wrapper">bullatiilla vethu vettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bullatiilla vethu vettu"/>
</div>
<div class="lyrico-lyrics-wrapper">innum iruntha eduthu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum iruntha eduthu kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">karadiye karadiye hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karadiye karadiye hey hey"/>
</div>
</pre>
