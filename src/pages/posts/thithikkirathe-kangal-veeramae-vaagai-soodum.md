---
title: "thithikkirathe kangal song lyrics"
album: "Veeramae Vaagai Soodum"
artist: "Yuvan Shankar Raja"
lyricist: "Vivek"
director: "Thu Pa Saravanan"
path: "/albums/veeramae-vaagai-soodum-lyrics"
song: "Thithikkirathe Kangal"
image: ../../images/albumart/veeramae-vaagai-soodum.jpg
date: 2022-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/cfF81jw__gw"
type: "love"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thithikkirathe kangal ethanayo pengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithikkirathe kangal ethanayo pengal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil unnai mattum kandaal uyir poraadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil unnai mattum kandaal uyir poraadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thodarum sorkal pathikirathe parkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thodarum sorkal pathikirathe parkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaan un thaakkam entral manam ennaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaan un thaakkam entral manam ennaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru nal naan kai pidipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru nal naan kai pidipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru yositha kaalam athuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru yositha kaalam athuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mel naan konda kaadhaluke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mel naan konda kaadhaluke"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalam maalaigal sootiyathinke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam maalaigal sootiyathinke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un viral koorthu vanthidum dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un viral koorthu vanthidum dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilai ariyaamal naan nadapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilai ariyaamal naan nadapen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugam paarthu en vizhi moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugam paarthu en vizhi moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paala yugam kuda naan kadapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paala yugam kuda naan kadapen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalaiyil poorvaikul unthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaiyil poorvaikul unthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna mugaam kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna mugaam kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ennaiye killi dhinam viyapene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ennaiye killi dhinam viyapene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pakkathil ne irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pakkathil ne irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Evan evan vaazha ooru muthathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan evan vaazha ooru muthathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandri solli naanum siripene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandri solli naanum siripene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marainthe naan kathai pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marainthe naan kathai pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Marathin arukil nedu kuraal ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marathin arukil nedu kuraal ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai vaangi alaikamal ineme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai vaangi alaikamal ineme"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaigal naam thadam yerkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaigal naam thadam yerkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un ottrai chithiram machcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ottrai chithiram machcham"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravagum vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravagum vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumpaagi koththira kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumpaagi koththira kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarum kooppai minnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarum kooppai minnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un viral koorthu vanthidum dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un viral koorthu vanthidum dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilai ariyaamal naan nadapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilai ariyaamal naan nadapen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugam paarthu en vizhi moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugam paarthu en vizhi moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala yugam kuda naan kadapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala yugam kuda naan kadapen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thithikkirathe kangal ethanayo pengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thithikkirathe kangal ethanayo pengal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil unnai mattum kandaal uyir poraadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil unnai mattum kandaal uyir poraadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thodarum sorkal pathikirathe parkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thodarum sorkal pathikirathe parkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaan un thaakkam entral manam ennaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu thaan un thaakkam entral manam ennaagum"/>
</div>
</pre>
