---
title: "hello nanbaa song lyrics"
album: "Maharaja"
artist: "D. Imman"
lyricist: "Pa. Vijay"
director: "D. Manoharan"
path: "/albums/maharaja-lyrics"
song: "Hello Nanbaa"
image: ../../images/albumart/maharaja.jpg
date: 2011-12-30
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hello hello hello nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello hello hello nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">style-leesh style-leesh en nadaithaamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="style-leesh style-leesh en nadaithaamba"/>
</div>
<div class="lyrico-lyrics-wrapper">helo helo helo nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="helo helo helo nanbaa"/>
</div>
20-20 <div class="lyrico-lyrics-wrapper">en udaithaambaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en udaithaambaa"/>
</div>
<div class="lyrico-lyrics-wrapper">koadambaakkam pakkam poanaa naan thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koadambaakkam pakkam poanaa naan thaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">booth hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="booth hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooda heeroyin thaaney treen pishaavil yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda heeroyin thaaney treen pishaavil yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">poraamai vendaamey poattikku vaa nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poraamai vendaamey poattikku vaa nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aanaalum naathaan velven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanaalum naathaan velven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hello hello hello nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello hello hello nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaalibathin vaanavilthaan vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaalibathin vaanavilthaan vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hello hello hello nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hello hello hello nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un thiramaikku help thaan No-ppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thiramaikku help thaan No-ppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hello hello hello nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello hello hello nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">style-leesh style-leesh en nadaithaamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="style-leesh style-leesh en nadaithaamba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennudan maarning break first
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennudan maarning break first"/>
</div>
<div class="lyrico-lyrics-wrapper">saappida vendum endrey solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saappida vendum endrey solli"/>
</div>
<div class="lyrico-lyrics-wrapper">imsai seiyum dheebigaa padukoaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imsai seiyum dheebigaa padukoaney"/>
</div>
<div class="lyrico-lyrics-wrapper">bissnece deeling ellaam vaithukkolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bissnece deeling ellaam vaithukkolla"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan veettin Gate-in munney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan veettin Gate-in munney"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu nirkkum nam aththil gates thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu nirkkum nam aththil gates thaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No daam skrilsin poal azhagum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No daam skrilsin poal azhagum illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam sutrum vaalibanum naan thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam sutrum vaalibanum naan thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">indha ulagai vella vandhavanum naan thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha ulagai vella vandhavanum naan thaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hello hello hello nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello hello hello nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">style-leesh style-leesh en nadaithaamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="style-leesh style-leesh en nadaithaamba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum 20-20 Cricket paarkkapoanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum 20-20 Cricket paarkkapoanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai rasigar koottam sutri vandhu moithidumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai rasigar koottam sutri vandhu moithidumey"/>
</div>
<div class="lyrico-lyrics-wrapper">ada mis veldil thaan daily daily miss call thandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada mis veldil thaan daily daily miss call thandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">I love you solli aiyo taarchal seigiraaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love you solli aiyo taarchal seigiraaley"/>
</div>
</pre>
