---
title: "thamizh anthem song lyrics"
album: "LKG"
artist: "Leon James"
lyricist: "Pa. Vijay"
director: "K. R. Prabhu"
path: "/albums/lkg-lyrics"
song: "Thamizh Anthem"
image: ../../images/albumart/lkg.jpg
date: 2019-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Wa8hTKe1n-I"
type: "happy"
singers:
  - Sid Sriram
  - Chinmayi
  - P. Susheela
  - L. R. Eswari
  - Vani Jayaram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uyire Unnai Thamizh Yenbatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Unnai Thamizh Yenbatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhe Unnai Uyir Yenbatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhe Unnai Uyir Yenbatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyil Mei Maranthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyil Mei Maranthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthil Uyir Mei Kalanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthil Uyir Mei Kalanthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumari Kandam Muthal Andam Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumari Kandam Muthal Andam Varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan Niranithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan Niranithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesathane Aasai Muzhaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesathane Aasai Muzhaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesi Paarthal Meesai Muzhaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesi Paarthal Meesai Muzhaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhkaapiyarin Kaigalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhkaapiyarin Kaigalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhe Kaninee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhe Kaninee"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Ezhuthunile Aayuthathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Ezhuthunile Aayuthathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthum Mozhi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthum Mozhi Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizh Magane Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh Magane Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Nimirnthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Nimirnthu Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh Magale Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh Magale Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharani Vella Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharani Vella Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizh Magane Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh Magane Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunivodu Vaada Thuyar Theerka Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunivodu Vaada Thuyar Theerka Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Nimirnthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Nimirnthu Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Panivodu Vaada Padaikondu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panivodu Vaada Padaikondu Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh Magale Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh Magale Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunivodu Vaada Thuyar Theerka Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunivodu Vaada Thuyar Theerka Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharani Vella Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharani Vella Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panivodu Vaada Padaikondu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panivodu Vaada Padaikondu Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niraarun Kadaludutta Nilamadanthai Kezhilozhugum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraarun Kadaludutta Nilamadanthai Kezhilozhugum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraarum Vathanamena Thigal Paradha Kandamithil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraarum Vathanamena Thigal Paradha Kandamithil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thekkanamum Athisirantha Dhravida Nal Thirunaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thekkanamum Athisirantha Dhravida Nal Thirunaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakka Siru Pirainuthalum Tharithanaru Thilagamume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakka Siru Pirainuthalum Tharithanaru Thilagamume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththilaga Vaasanai Pol Anaithulagum Inbamura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththilaga Vaasanai Pol Anaithulagum Inbamura"/>
</div>
<div class="lyrico-lyrics-wrapper">Yettiseiyum Pugazh Manakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yettiseiyum Pugazh Manakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthaperun Thamizhnange Thamizhnange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthaperun Thamizhnange Thamizhnange"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Sirilamai Thiram Viyanthu Seyal Maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sirilamai Thiram Viyanthu Seyal Maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhthuthume Vazhthuthume Vazhthuthume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhthuthume Vazhthuthume Vazhthuthume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eerayiram Aandagiyum Thamizh Tharaniyai Aalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerayiram Aandagiyum Thamizh Tharaniyai Aalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Ulagengilum Paraisatriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Ulagengilum Paraisatriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Inam Engal Nizham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Inam Engal Nizham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallanaiyil Patta Katrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallanaiyil Patta Katrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaveriyil Kodi Yetrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaveriyil Kodi Yetrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Olaisuvadukul Ulla Ariviyal Parpotrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olaisuvadukul Ulla Ariviyal Parpotrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Singa Inam Endrum Seerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singa Inam Endrum Seerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jallikattu Sonna Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jallikattu Sonna Veeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeram Mattumella Kadhal Solla Solla Thean Oorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram Mattumella Kadhal Solla Solla Thean Oorum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaiyam Athan Mudiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaiyam Athan Mudiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Peyar Yezhuthiya Thamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peyar Yezhuthiya Thamizhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil Entha Uyirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Entha Uyirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendru Uthavidum Manithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendru Uthavidum Manithan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizhan Thamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhan Thamizhan"/>
</div>
<div class="lyrico-lyrics-wrapper">ThamizhanThamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ThamizhanThamizhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizh Magane Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh Magane Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunivodu Vaada Thuyar Theerka Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunivodu Vaada Thuyar Theerka Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Nimirnthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Nimirnthu Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Panivodu Vaada Padaikondu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panivodu Vaada Padaikondu Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh Magale Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh Magale Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunivodu Vaada Thuyar Theerka Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunivodu Vaada Thuyar Theerka Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharani Vella Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharani Vella Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panivodu Vaada Padaikondu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panivodu Vaada Padaikondu Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizh Magane Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh Magane Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Nimirnthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Nimirnthu Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh Magale Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh Magale Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharani Vella Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharani Vella Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thamizh Magane Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh Magane Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Nimirnthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Nimirnthu Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizh Magale Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizh Magale Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharani Vella Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharani Vella Vaa"/>
</div>
</pre>
