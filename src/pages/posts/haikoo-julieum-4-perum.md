---
title: "haikoo song lyrics"
album: "Julieum 4 Perum"
artist: "Raghu Sravan Kumar"
lyricist: "Madhan Raj"
director: "R V Satheesh"
path: "/albums/julieum-4-perum-lyrics"
song: "Haikoo"
image: ../../images/albumart/julieum-4-perum.jpg
date: 2017-04-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DUnZbv8ahRU"
type: "happy"
singers:
  -	Akshaya Ramnath
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haiku Neeye En 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiku Neeye En "/>
</div>
<div class="lyrico-lyrics-wrapper">Haiku Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiku Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Facebook Kule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Facebook Kule "/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundha Like'um Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundha Like'um Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiku Neeye En 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiku Neeye En "/>
</div>
<div class="lyrico-lyrics-wrapper">Haiku Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiku Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Facebook Kule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Facebook Kule "/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundha Like'um Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundha Like'um Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whatsapp'in Status 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whatsapp'in Status "/>
</div>
<div class="lyrico-lyrics-wrapper">Pola Unnai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Unnai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Paarpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Paarpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Yarukum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Yarukum "/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyamal Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyamal Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Button'itu Kaapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Button'itu Kaapen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whatsapp'in Status 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whatsapp'in Status "/>
</div>
<div class="lyrico-lyrics-wrapper">Pola Unnai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Unnai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Paarpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Paarpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Yarukum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Yarukum "/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyamal Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyamal Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Button'itu Kaapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Button'itu Kaapen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haiku Neeye En 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiku Neeye En "/>
</div>
<div class="lyrico-lyrics-wrapper">Haiku Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiku Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Facebook Kule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Facebook Kule "/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundha Like'um Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundha Like'um Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kayirai Kondu Manjal Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayirai Kondu Manjal Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnudan Nadandhaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudan Nadandhaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirakudhu Motu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirakudhu Motu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parvai Indri Irul Poruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parvai Indri Irul Poruka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Munne Sellum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Munne Sellum "/>
</div>
<div class="lyrico-lyrics-wrapper">Bakiyam Sothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakiyam Sothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnudane Irundhaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudane Irundhaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimai Theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai Theriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Velayatin Seyalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Velayatin Seyalil "/>
</div>
<div class="lyrico-lyrics-wrapper">En Kavalaigal Irukadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kavalaigal Irukadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Nambi Sila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Nambi Sila "/>
</div>
<div class="lyrico-lyrics-wrapper">Veedugal Ingu Thoongudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedugal Ingu Thoongudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Nambi Sila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Nambi Sila "/>
</div>
<div class="lyrico-lyrics-wrapper">Veedugal Ingu Thoongudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedugal Ingu Thoongudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haiku Neeye En 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiku Neeye En "/>
</div>
<div class="lyrico-lyrics-wrapper">Haiku Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiku Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Facebook Kule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Facebook Kule "/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundha Like'um Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundha Like'um Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Methai Mele 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Methai Mele "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennudan Thoongum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennudan Thoongum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyanai Vendam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyanai Vendam "/>
</div>
<div class="lyrico-lyrics-wrapper">Julie Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Julie Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulika Veikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulika Veikum "/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Nanaiven 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Nanaiven "/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin Mazhayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin Mazhayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuzhandhaiyudan Vilayadum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhandhaiyudan Vilayadum "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Kuzhandhai Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Kuzhandhai Dhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Kurumbuku Vaal Aatum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Kurumbuku Vaal Aatum "/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Manamum Dhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Manamum Dhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Tholil Thooki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Tholil Thooki "/>
</div>
<div class="lyrico-lyrics-wrapper">Kondu Ulagam Sutruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu Ulagam Sutruven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandri Ninaipadhaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandri Ninaipadhaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Unakum Silaigal Veipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakum Silaigal Veipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haiku Neeye En 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiku Neeye En "/>
</div>
<div class="lyrico-lyrics-wrapper">Haiku Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiku Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Facebook Kule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Facebook Kule "/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundha Like'um Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundha Like'um Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whatsapp'in Status 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whatsapp'in Status "/>
</div>
<div class="lyrico-lyrics-wrapper">Pola Unnai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Unnai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Paarpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Paarpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Yarukum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Yarukum "/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyamal Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyamal Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Button'itu Kaapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Button'itu Kaapen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whatsapp'in Status 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whatsapp'in Status "/>
</div>
<div class="lyrico-lyrics-wrapper">Pola Unnai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Unnai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Paarpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Paarpen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Yarukum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Yarukum "/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyamal Naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyamal Naan "/>
</div>
<div class="lyrico-lyrics-wrapper">Button'itu Kaapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Button'itu Kaapen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haiku Neeye En 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiku Neeye En "/>
</div>
<div class="lyrico-lyrics-wrapper">Haiku Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiku Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Facebook Kule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Facebook Kule "/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundha Like'um Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundha Like'um Neeye"/>
</div>
</pre>
