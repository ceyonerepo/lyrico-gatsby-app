---
title: "theemai thaan vellum song lyrics"
album: "Thani Oruvan"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "M. Raja"
path: "/albums/thani-oruvan-lyrics"
song: "Theemai Thaan Vellum"
image: ../../images/albumart/thani-oruvan.jpg
date: 2015-08-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tkql_yvuSK0"
type: "Mass"
singers:
  - Arvind Swamy
  - Hiphop Tamizha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nallavanukku Nallathu Seirathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallavanukku Nallathu Seirathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verum Aasai Thaan Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Aasai Thaan Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kettavanukku Kettathu Seirathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettavanukku Kettathu Seirathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peraasai Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peraasai Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaikkum Aasaikku Peraasaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaikkum Aasaikku Peraasaikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadakkura Porla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakkura Porla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeikkarathu Peraasaithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeikkarathu Peraasaithan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theemai Dhaan Vellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemai Dhaan Vellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Ninaithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ninaithaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theemai Thaan Vellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemai Thaan Vellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evan Thaduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan Thaduthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manitha Uruvathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manitha Uruvathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaindhidum Mirugam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaindhidum Mirugam Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manidha Mirugangalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidha Mirugangalukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kadavul Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kadavul Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manitha Uruvathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manitha Uruvathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaindhidum Mirugam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaindhidum Mirugam Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manidha Mirugangalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidha Mirugangalukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kadavul Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kadavul Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velichathula Irukkaravantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichathula Irukkaravantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irutta Paathu Bayapaduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutta Paathu Bayapaduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Iruttualiye Vaazhravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Iruttualiye Vaazhravan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I’m not bad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m not bad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Just Evil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Evil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yevana Irundhaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevana Irundhaal Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemanaai Irundhaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemanaai Irundhaal Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sivana Irudhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivana Irudhaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku Samamaai Amaiven Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Samamaai Amaiven Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panama Irundhaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panama Irundhaal Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pinamaai Irundhaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinamaai Irundhaal Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Uyirodu Irundhidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Uyirodu Irundhidave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yevanaiyum Unavaai Unben Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevanaiyum Unavaai Unben Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manitha Uruvathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manitha Uruvathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaindhidum Mirugam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaindhidum Mirugam Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manidha Mirugangalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidha Mirugangalukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kadavul Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kadavul Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmai Jeikkarathuukku Thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Jeikkarathuukku Thaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadharam Thevai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadharam Thevai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poi Jeikkarathukku Kolappame Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi Jeikkarathukku Kolappame Pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soodhaai irundhaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodhaai irundhaal Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Theetha Irundhaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Theetha Irundhaal Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaathaai Irundhaalum Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaathaai Irundhaalum Enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodhaai Amaindhidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodhaai Amaindhidume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhoologam Athai Vendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoologam Athai Vendru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhala Paathaalam Varai Sendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhala Paathaalam Varai Sendru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolaagalamaaga Endhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaagalamaaga Endhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatchi Purinthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatchi Purinthiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theemai Thaan Vellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemai Thaan Vellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Ninaithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ninaithaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theemai Than Vellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemai Than Vellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evan Thaduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan Thaduthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">The Name is Sidharth Abhimanyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Name is Sidharth Abhimanyu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Good Luck
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Good Luck"/>
</div>
</pre>
