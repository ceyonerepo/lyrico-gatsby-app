---
title: "saalakaara song lyrics"
album: "Pakkiri"
artist: "Amit Trivedi"
lyricist: "Madhan Karky"
director: "Ken Scott"
path: "/albums/pakkiri-lyrics"
song: "Saalakaara"
image: ../../images/albumart/pakkiri.jpg
date: 2019-06-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QufFKI64TJc"
type: "happy"
singers:
  - Anthony Daasan
  - R. Venkataraman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Saalakkaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalakkaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannkatti Vitha Kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannkatti Vitha Kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalakkaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalakkaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathathaan Kaasaa Maathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathathaan Kaasaa Maathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalakkaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalakkaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Megatha Choosaa Maathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megatha Choosaa Maathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalakkaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalakkaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalakkaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalakkaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatta Ellaam Maasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatta Ellaam Maasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatta Maattaan Lesula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatta Maattaan Lesula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottam Paaru Chase-ula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottam Paaru Chase-ula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulimaari Urumaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulimaari Urumaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saalakkaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalakkaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanathil Cycle Viduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathil Cycle Viduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalakkaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalakkaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai Muzhungi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Muzhungi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppam Viduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppam Viduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalakkaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalakkaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Tavusara Unakke Vippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Tavusara Unakke Vippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalakkaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalakkaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machaan Maaya Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machaan Maaya Kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippaan Di On Munnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippaan Di On Munnaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaa Povum Pinnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaa Povum Pinnaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Fakkeeru Killaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fakkeeru Killaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koottan Seppaan Koothaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottan Seppaan Koothaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattuvaan Dee Kaathaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattuvaan Dee Kaathaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yechu Poavaan Aathaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yechu Poavaan Aathaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Fakkeeru Killaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fakkeeru Killaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pacha Ila Kasakkuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Ila Kasakkuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Keechu Kili Porakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keechu Kili Porakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekka Rendu Kirukkuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekka Rendu Kirukkuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacha Kili Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Kili Parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Chekka Cheva Mookka Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chekka Cheva Mookka Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Parsa Thorakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Parsa Thorakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorakkum Thorakkum Thorakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorakkum Thorakkum Thorakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkaru Aalu Fakkeeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaru Aalu Fakkeeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikkunaa Needhaan Jokkeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikkunaa Needhaan Jokkeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gili Gili Jee Boom Baa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili Gili Jee Boom Baa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Saamiyaara Varuvom Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Saamiyaara Varuvom Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Saalai Pathu Nadungumdaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Saalai Pathu Nadungumdaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipoo Aarum Illa Thadangal Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipoo Aarum Illa Thadangal Daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetti Vetti Vettiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti Vetti Vettiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Vitha Illa Kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Vitha Illa Kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Dhoolu Paa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Dhoolu Paa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga Pocket Kulla Login
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Pocket Kulla Login"/>
</div>
<div class="lyrico-lyrics-wrapper">Panni Ella Data Clean Up Baa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panni Ella Data Clean Up Baa"/>
</div>
<div class="lyrico-lyrics-wrapper">Phone-u Motham Ella Sothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phone-u Motham Ella Sothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuru Thuru Naanga Take Up Paa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thuru Naanga Take Up Paa"/>
</div>
<div class="lyrico-lyrics-wrapper">Wake Up Paa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wake Up Paa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Paar Kaasu Panam Engappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Paar Kaasu Panam Engappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Aasa Kaasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Aasa Kaasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Kanakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhana Money Illa Enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhana Money Illa Enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadasi Kooku Thaan Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadasi Kooku Thaan Unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Suruttu Suruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suruttu Suruttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandavan Thaan Thunaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavan Thaan Thunaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Amukku Amukku Amukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amukku Amukku Amukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aatta Ellaam Maasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatta Ellaam Maasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatta Maattaan Lesula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatta Maattaan Lesula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottam Paaru Chase-ula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottam Paaru Chase-ula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulimaari Urumaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulimaari Urumaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Grab It Grab It Rob
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Grab It Grab It Rob"/>
</div>
<div class="lyrico-lyrics-wrapper">Its Okay Its Okay Its Okay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Its Okay Its Okay Its Okay"/>
</div>
<div class="lyrico-lyrics-wrapper">Its Okay Its Okay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Its Okay Its Okay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saalakkaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalakkaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalakkaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalakkaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalakkaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalakkaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalakkaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalakkaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fakkeeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fakkeeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fakkeeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fakkeeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fakkeeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fakkeeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fakkeeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fakkeeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fakkeeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fakkeeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fakkeeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fakkeeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fakkeeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fakkeeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Fakkeeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fakkeeru"/>
</div>
</pre>
