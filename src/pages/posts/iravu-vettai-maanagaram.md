---
title: "iravu vettai song lyrics"
album: "Maanagaram"
artist: "Javed Riaz"
lyricist: "Lalithanand"
director: "Lokesh Kanagaraj"
path: "/albums/maanagaram-lyrics"
song: "Iravu Vettai"
image: ../../images/albumart/maanagaram.jpg
date: 2017-03-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lcYeN2WfHVU"
type: "mass"
singers:
  -	Suraj Jagan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Iravu vettai aaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu vettai aaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraiena vazhvai kekkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraiena vazhvai kekkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukku kangal koodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukku kangal koodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Immaikuthae padhai thediyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Immaikuthae padhai thediyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookam illamal thurathuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookam illamal thurathuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooku kairaagi miratuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooku kairaagi miratuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu nagaram managaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu nagaram managaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Atharukku nooru mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharukku nooru mugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saama saama saama pozhudhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saama saama saama pozhudhilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathurangha aatam irulilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathurangha aatam irulilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gama gama vaasam panathilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gama gama vaasam panathilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu vettai aaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu vettai aaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraiena vazhvai kekkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraiena vazhvai kekkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukku kangal koodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukku kangal koodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Immaikuthae padhai thediyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Immaikuthae padhai thediyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Punnagai minnidum munn iravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagai minnidum munn iravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu pudhu vannam tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu pudhu vannam tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennavo bothai varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavo bothai varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnani kondathu pinn iravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnani kondathu pinn iravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhirgalai kattru tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhirgalai kattru tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottai thirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottai thirakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagarama naragama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarama naragama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru dhinam virkum thunbaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru dhinam virkum thunbaththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Marudhinam virkum inbaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudhinam virkum inbaththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodaruma vidiyumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodaruma vidiyumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engo thondridum engo serndhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo thondridum engo serndhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ella padhaigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ella padhaigalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ella oorgalum ondrai serndhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ella oorgalum ondrai serndhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu than maanagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu than maanagaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu vettai aaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu vettai aaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraiena vazhvai kekkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraiena vazhvai kekkuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravukku kangal koodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukku kangal koodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Immaikuthae padhai thediyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Immaikuthae padhai thediyae"/>
</div>
</pre>
