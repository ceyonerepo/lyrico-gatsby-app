---
title: "mannaarkudi song lyrics"
album: "Galatta Kalyaanam"
artist: "A R Rahman "
lyricist: "Madhan Karky"
director: "Aanand L Rai"
path: "/albums/galatta-kalyaanam-song-lyrics"
song: "Mannaarkudi"
image: ../../images/albumart/galatta-kalyaanam.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8SruPgvLu6w"
type: "happy"
singers:
  - Mano
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Athiradi makka makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiradi makka makka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannarkudi kaaren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannarkudi kaaren da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakka thina thakka thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakka thina thakka thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannarkudi kaaren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannarkudi kaaren da"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiradi makka makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiradi makka makka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannarkudi kaaren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannarkudi kaaren da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakka thina thakka thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakka thina thakka thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannarkudi kaaren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannarkudi kaaren da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjai ellam alla varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai ellam alla varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanjavuru theru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjavuru theru da"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru per boogam vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru per boogam vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadungama paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadungama paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kadhai kettukkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kadhai kettukkada"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluratha pottukkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluratha pottukkada"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasathi rasa thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasathi rasa thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Adicha century da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adicha century da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannarkudi da kudi mannarkudi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannarkudi da kudi mannarkudi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudi mannarkudi da adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudi mannarkudi da adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adicha century da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adicha century da"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannarkudi da makka mannarkudi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannarkudi da makka mannarkudi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekka mannarkudi da pakka century da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekka mannarkudi da pakka century da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athiradi makka makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiradi makka makka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannarkudi kaaren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannarkudi kaaren da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakka thina thakka thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakka thina thakka thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannarkudi kaaren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannarkudi kaaren da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thananaana thananaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananaana thananaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Papa paaapa papa paapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papa paaapa papa paapa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellame saaram saaram avan jothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellame saaram saaram avan jothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey makka hey makka makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey makka hey makka makka"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey makka makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey makka makka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh jathagannu sollucha ullam kai sollucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh jathagannu sollucha ullam kai sollucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanoda pathaiya thaan maariccha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanoda pathaiya thaan maariccha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayajalam panna manasellam allikittan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayajalam panna manasellam allikittan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kooda koottam ellam sernthucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kooda koottam ellam sernthucha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram viththai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram viththai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil avan vachirukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil avan vachirukkan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooril ulla anba ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooril ulla anba ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil avan vachirukkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil avan vachirukkan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadolla nalla adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadolla nalla adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Anna ooru mannarkudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna ooru mannarkudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannarkudi da kudi mannarkudi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannarkudi da kudi mannarkudi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudi mannarkudi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudi mannarkudi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi adicha century da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi adicha century da"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannarkudi da makka mannarkudi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannarkudi da makka mannarkudi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekka mannarkudi da pakka century da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekka mannarkudi da pakka century da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athiradi makka makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiradi makka makka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannarkudi kaaren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannarkudi kaaren da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakka thina thakka thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakka thina thakka thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannarkudi kaaren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannarkudi kaaren da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjai ellam alla varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai ellam alla varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanjavuru theru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjavuru theru da"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru per boogam vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru per boogam vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadungama paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadungama paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kadhai kettukkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kadhai kettukkada"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluratha pottukkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluratha pottukkada"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasathi rasa thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasathi rasa thaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannarkudi da kudi mannarkudi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannarkudi da kudi mannarkudi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi mannarkudi da adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi mannarkudi da adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannarkudi da makka mannarkudi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannarkudi da makka mannarkudi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekka mannarkudi da pakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekka mannarkudi da pakka"/>
</div>
</pre>
