---
title: "penne song lyrics"
album: "2 States"
artist: "Jakes Bejoy"
lyricist: "Joe Paul"
director: "Jacky S. Kumar"
path: "/albums/2-states-lyrics"
song: "Penne"
image: ../../images/albumart/2-states.jpg
date: 2020-03-06
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/hgsk91vSbnA"
type: "happy"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maanin Mizhiyulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanin Mizhiyulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanachiriyulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanachiriyulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Laavinnazhakivalarennariyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laavinnazhakivalarennariyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paari Vanne Vazhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paari Vanne Vazhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadi Nadhiyarike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Nadhiyarike"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meghacheruvile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meghacheruvile"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal Valachu Njaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Valachu Njaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullilorukkiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullilorukkiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoniyirakkidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoniyirakkidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mohicha Raavin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohicha Raavin"/>
</div>
<div class="lyrico-lyrics-wrapper">Oolathilengo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oolathilengo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadithuzhanjeedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadithuzhanjeedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyennuyirile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyennuyirile"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyennuyarile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyennuyarile"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Thirayaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Thirayaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Peythidukille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peythidukille"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadariyatheyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadariyatheyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattariyatheyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattariyatheyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Raamazha Thenmazhayaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamazha Thenmazhayaay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venal Puzhayude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venal Puzhayude"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerathinnee Nammal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerathinnee Nammal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooval Virichurangaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooval Virichurangaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arikilaay Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arikilaay Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Alasayaay Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alasayaay Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyaavum Taazhvaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaavum Taazhvaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Njaanetho Vaarmegham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Njaanetho Vaarmegham"/>
</div>
<div class="lyrico-lyrics-wrapper">Melaake Moodunno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melaake Moodunno"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaneram Theerolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaneram Theerolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nin Mizhiyennoru Vennadhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nin Mizhiyennoru Vennadhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjala Ninnukayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjala Ninnukayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovithalaayathilinnineeyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovithalaayathilinnineeyen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamana Neendhukayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamana Neendhukayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hridhyamandhramozhuki Melle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hridhyamandhramozhuki Melle"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumoru Theeraay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumoru Theeraay"/>
</div>
</pre>
