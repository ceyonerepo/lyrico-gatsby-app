---
title: 'hey amigo song lyrics'
album: 'Kaappaan'
artist: 'Harris Jayaraj'
lyricist: 'Vairamuthu'
director: 'K V Anand'
path: '/albums/kaappaan-song-lyrics'
song: 'Hey Amigo'
image: ../../images/albumart/kaappaan.jpg
date: 2019-09-20
lang: tamil
singers: 
- Leslie Lewis
- Jonita Gandhi
youtubeLink: "https://www.youtube.com/embed/MwtKJG_87fw"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Hey mi amigo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey mi amigo"/>
</div>
<div class="lyrico-lyrics-wrapper">Whisko…kisko..
<input type="checkbox" class="lyrico-select-lyric-line" value="Whisko…kisko.."/>
</div>
<div class="lyrico-lyrics-wrapper">Let the honey flow..
<input type="checkbox" class="lyrico-select-lyric-line" value="Let the honey flow.."/>
</div>
<div class="lyrico-lyrics-wrapper">Naan santiago..never..ever..
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan santiago..never..ever.."/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t let me go…
<input type="checkbox" class="lyrico-select-lyric-line" value="Don’t let me go…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Eh amadoo.. vaa vaa ennodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Eh amadoo.. vaa vaa ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Solando..nee en sweet hearto
<input type="checkbox" class="lyrico-select-lyric-line" value="Solando..nee en sweet hearto"/>
</div>
<div class="lyrico-lyrics-wrapper">Momento..nee paarka paarka paravasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Momento..nee paarka paarka paravasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo bailando…vandhaadum serndhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oo bailando…vandhaadum serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ailento… moochodu moochu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ailento… moochodu moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Libido…un paarvaikkkullae pazharasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Libido…un paarvaikkkullae pazharasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa en gajaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa en gajaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai kolla naan vandhenaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Kollai kolla naan vandhenaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pon savaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee pon savaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmanjal manjam adi needhaana..
<input type="checkbox" class="lyrico-select-lyric-line" value="Mmanjal manjam adi needhaana.."/>
</div>
  <div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Atlantickin aazham…
<input type="checkbox" class="lyrico-select-lyric-line" value="Atlantickin aazham…"/>
</div>
<div class="lyrico-lyrics-wrapper">Arabic kadalin neelam
<input type="checkbox" class="lyrico-select-lyric-line" value="Arabic kadalin neelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvae undhan kangal
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhuvae undhan kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Penn kuruviyae…..
<input type="checkbox" class="lyrico-select-lyric-line" value="Penn kuruviyae….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idaiyin thullal podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Idaiyin thullal podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">En idhayam nettriyil yerum
<input type="checkbox" class="lyrico-select-lyric-line" value="En idhayam nettriyil yerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaa thisaiyum maarum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaa thisaiyum maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">En arasaiyae..heyy heyyy…
<input type="checkbox" class="lyrico-select-lyric-line" value="En arasaiyae..heyy heyyy…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thuzhaavum …un paarvai nenjil
<input type="checkbox" class="lyrico-select-lyric-line" value="Thuzhaavum …un paarvai nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaavum….. adhil thoatru pogum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulaavum….. adhil thoatru pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavum… nee ulagin ettaaam adhisayam
<input type="checkbox" class="lyrico-select-lyric-line" value="Nilavum… nee ulagin ettaaam adhisayam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Woahu wohh huuu
<input type="checkbox" class="lyrico-select-lyric-line" value="Woahu wohh huuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vinodham… adi unnaal egurudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vinodham… adi unnaal egurudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivegam …nee kollum aangal anaegam
<input type="checkbox" class="lyrico-select-lyric-line" value="Vivegam …nee kollum aangal anaegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kollai azhagu kolaikalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kollai azhagu kolaikalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">La..vaa..hey..ah…hmm…aaa..
<input type="checkbox" class="lyrico-select-lyric-line" value="La..vaa..hey..ah…hmm…aaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Thari ….
<input type="checkbox" class="lyrico-select-lyric-line" value="Thari …."/>
</div>
<div class="lyrico-lyrics-wrapper">Doo ..hey ..oo.. naa…aa..hey ohoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Doo ..hey ..oo.. naa…aa..hey ohoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un kannaal kannaal sonnaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kannaal kannaal sonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugam kittaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Sugam kittaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyaal kaiyaal meiyaal thodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiyaal kaiyaal meiyaal thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiyaal poiyaal paiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Poiyaal poiyaal paiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavi sollaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavi sollaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbam thunbam rendum kodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Inbam thunbam rendum kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti pehchu pesi
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti pehchu pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Penmai nirayaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Penmai nirayaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennazhagellaam veppam kolla
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennazhagellaam veppam kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerukki vidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nerukki vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai udaikaamal
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai udaikaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir inaikaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyir inaikaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vanmurai indri
<input type="checkbox" class="lyrico-select-lyric-line" value="Un vanmurai indri"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan iravu kazhiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Endhan iravu kazhiyaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannodu kangal kandalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannodu kangal kandalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhaadhu melum keezhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Podhaadhu melum keezhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyodu kaigal seraamal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiyodu kaigal seraamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeradhu endhan mogam
<input type="checkbox" class="lyrico-select-lyric-line" value="Theeradhu endhan mogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkarai vendum endraal
<input type="checkbox" class="lyrico-select-lyric-line" value="Sakkarai vendum endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumbu chakkai aaga vendum
<input type="checkbox" class="lyrico-select-lyric-line" value="Karumbu chakkai aaga vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ooohooo …
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooohooo …"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaagaadhu nee thalli sendraal
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaagaadhu nee thalli sendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaadhu en aasai peiyoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Adaadhu en aasai peiyoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaadhu en moga kaatil muzhugidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidaadhu en moga kaatil muzhugidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeey…adaadhu en jodi poovo
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeey…adaadhu en jodi poovo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudaathu nee thalli chella
<input type="checkbox" class="lyrico-select-lyric-line" value="Sudaathu nee thalli chella"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaathu nee kodikaiyaal thodu thodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidaathu nee kodikaiyaal thodu thodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Eh amadoo.. vaa vaa ennodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Eh amadoo.. vaa vaa ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Solando..nee en sweet hearto
<input type="checkbox" class="lyrico-select-lyric-line" value="Solando..nee en sweet hearto"/>
</div>
<div class="lyrico-lyrics-wrapper">Momento..nee paarka paarka paravasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Momento..nee paarka paarka paravasam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Heyy eyyyy….
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy eyyyy…."/>
</div>
  <div class="lyrico-lyrics-wrapper">Oo bailando…vandhaadum serndhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oo bailando…vandhaadum serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ailento… moochodu moochu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ailento… moochodu moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Libido…un paarvaikkkullae pazharasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Libido…un paarvaikkkullae pazharasam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Lelelele…
<input type="checkbox" class="lyrico-select-lyric-line" value="Lelelele…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Amigo
<input type="checkbox" class="lyrico-select-lyric-line" value="Amigo"/>
</div>
  <div class="lyrico-lyrics-wrapper">Eh amadoo.. vaa vaa ennodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Eh amadoo.. vaa vaa ennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Solando..nee en sweet hearto
<input type="checkbox" class="lyrico-select-lyric-line" value="Solando..nee en sweet hearto"/>
</div>
  <div class="lyrico-lyrics-wrapper">Eh Bilando
<input type="checkbox" class="lyrico-select-lyric-line" value="Eh Bilando"/>
</div>
  <div class="lyrico-lyrics-wrapper">Momento..nee paarka paarka paravasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Momento..nee paarka paarka paravasam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Heyyyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyyyy"/>
</div>
  <div class="lyrico-lyrics-wrapper">Oo bailando…vandhaadum serndhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oo bailando…vandhaadum serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ailento… moochodu moochu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ailento… moochodu moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Libido…un paarvaikkkullae pazharasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Libido…un paarvaikkkullae pazharasam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hoo ooo ooo lets go
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo ooo lets go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oo bailando…vandhaadum serndhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oo bailando…vandhaadum serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ailento… moochodu moochu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ailento… moochodu moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Libido…un paarvaikkkullae pazharasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Libido…un paarvaikkkullae pazharasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh amadoo..
<input type="checkbox" class="lyrico-select-lyric-line" value="Eh amadoo.."/>
</div>
</pre>