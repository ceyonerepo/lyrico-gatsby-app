---
title: "ramuloo ramulaa song lyrics"
album: "Ala Vaikunthapurramuloo"
artist: "S. Thaman"
lyricist: "Kasarla Shyam"
director: "Trivikram Srinivas"
path: "/albums/ala-vaikunthapurramuloo-lyrics"
song: "Ramuloo Ramulaa"
image: ../../images/albumart/ala-vaikunthapurramuloo.jpg
date: 2020-01-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Bg8Yb9zGYyA"
type: "love"
singers:
  - Anurag Kulkarni
  - Mangli Satyavati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bantu ganiki twenty-two
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bantu ganiki twenty-two"/>
</div>
<div class="lyrico-lyrics-wrapper">Basthila masthu cut-outu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basthila masthu cut-outu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bacchagandla batch undedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bacchagandla batch undedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vocchinamante suttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vocchinamante suttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kick ye chaalaka o nightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick ye chaalaka o nightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekki dokku bullettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekki dokku bullettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhu sandhula mandhu kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhu sandhula mandhu kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukuthante routeu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukuthante routeu"/>
</div>
<div class="lyrico-lyrics-wrapper">Silk cheera kattukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silk cheera kattukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Child beer merisinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Child beer merisinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Potlam kattina biryaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potlam kattina biryaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bottu billa pettinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottu billa pettinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bangla meedha nilusonundhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bangla meedha nilusonundhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">O sandhamama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O sandhamama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sukkadhaagaka chakkar occhero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukkadhaagaka chakkar occhero"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemandham maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemandham maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinka lekka dhunkuthuntero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinka lekka dhunkuthuntero"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa chandamama junkhee jari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa chandamama junkhee jari"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikkukundhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikkukundhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa dhilluku mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa dhilluku mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaagam jesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaagam jesindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa paanam theesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa paanam theesindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaagam jesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaagam jesindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa paanam theesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa paanam theesindhiro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaagam jesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaagam jesindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa paanam theesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa paanam theesindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaagam jesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaagam jesindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa paanam theesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa paanam theesindhiro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye thamalapaake yesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thamalapaake yesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammaga vaasana vosthave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammaga vaasana vosthave"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerraga pandina buggalu rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerraga pandina buggalu rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaadhikosthaaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadhikosthaaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey puvvula angi yesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey puvvula angi yesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundi nuvvai poosthaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundi nuvvai poosthaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandukunna gundelo dhoori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandukunna gundelo dhoori"/>
</div>
<div class="lyrico-lyrics-wrapper">Gulle jesthave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gulle jesthave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey inti mundhu light
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey inti mundhu light"/>
</div>
<div class="lyrico-lyrics-wrapper">Minuku minuku mantaante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minuku minuku mantaante"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu kannu kottinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu kannu kottinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siggu puttindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siggu puttindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Seera kongu thalupu saatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seera kongu thalupu saatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkukuntaante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkukuntaante"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu laaginattu vollu jhallumantaandhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu laaginattu vollu jhallumantaandhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nagaswaram oodhuthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaswaram oodhuthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Naagupaamu ooginattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naagupaamu ooginattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yentabadi vasthunna nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yentabadi vasthunna nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee patta golusu sappudintu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee patta golusu sappudintu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattanatte thiruguthunnave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattanatte thiruguthunnave"/>
</div>
<div class="lyrico-lyrics-wrapper">O sandhamaava pakkaki poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O sandhamaava pakkaki poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thongi soosthave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thongi soosthave"/>
</div>
<div class="lyrico-lyrics-wrapper">Em tekkura maava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em tekkura maava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaagam jesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaagam jesindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa paanam theesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa paanam theesindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaagam jesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaagam jesindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa paanam theesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa paanam theesindhiro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaagam jesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaagam jesindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa paanam theesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa paanam theesindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaagam jesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaagam jesindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa paanam theesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa paanam theesindhiro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramuloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannaagam jesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannaagam jesindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramuloo ramulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramuloo ramulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa paanam theesindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa paanam theesindhiro"/>
</div>
</pre>
