---
title: "rama rama enna idhu song lyrics"
album: "Sollividava"
artist: "Jassie Gift"
lyricist: "Viveka"
director: "Arjun Sarja"
path: "/albums/sollividava-lyrics"
song: "Rama Rama Enna Idhu"
image: ../../images/albumart/sollividava.jpg
date: 2018-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MMVY9TA4RTk"
type: "love"
singers:
  - Haricharan
  - Anitha Karthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raama raama enna idhu raama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raama raama enna idhu raama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda padi poduvalae draama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda padi poduvalae draama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu rendum kaathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu rendum kaathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana mosa maana vaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana mosa maana vaayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aama aama vaada kitta maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama aama vaada kitta maama"/>
</div>
<div class="lyrico-lyrics-wrapper">Onghi vitta poiyuduva coma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onghi vitta poiyuduva coma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaliaana mel maadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaliaana mel maadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana pecha paaru aathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana pecha paaru aathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goiyaala romba aaduralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goiyaala romba aaduralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyaama sanda poduraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyaama sanda poduraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada poda eruma maadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada poda eruma maadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeans potta karuvaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeans potta karuvaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raama raama enna idhu raama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raama raama enna idhu raama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">TRP rating jaasthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TRP rating jaasthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga channel di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga channel di"/>
</div>
<div class="lyrico-lyrics-wrapper">Fake news-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fake news-u da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla ellam kaasuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla ellam kaasuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ara loosu kitta vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ara loosu kitta vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatikittenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatikittenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaan loosudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaan loosudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala paathu pesuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala paathu pesuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dettol oothi thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dettol oothi thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya nee kaluvukka di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya nee kaluvukka di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta varthai pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta varthai pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaikaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaikaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kora maasathil porandhavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kora maasathil porandhavanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ko ko korangha pola valarndhavanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ko ko korangha pola valarndhavanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadadha daa un thol uruchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadadha daa un thol uruchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thongha poduven da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thongha poduven da"/>
</div>
<div class="lyrico-lyrics-wrapper">Chumma poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chumma poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raama raama enna idhu raama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raama raama enna idhu raama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appata pottu thandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appata pottu thandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna pannuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pannuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Uncle dhaanae di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uncle dhaanae di"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesi mingle aaven di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesi mingle aaven di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaala vittu odhacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaala vittu odhacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna pannuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pannuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Senju paaru di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senju paaru di"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu gym-mu body di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu gym-mu body di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un perula love letter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un perula love letter"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eluthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eluthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru paati kitta tharuvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru paati kitta tharuvenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey paatikku oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey paatikku oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paethi irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paethi irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona enna paathu sirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona enna paathu sirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru noola vitu aala pidipennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru noola vitu aala pidipennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly dhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly dhaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raama raama enna idhu raama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raama raama enna idhu raama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda padi poduvanae draama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda padi poduvanae draama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaliaana mel maadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaliaana mel maadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana pecha paaru aathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana pecha paaru aathadi"/>
</div>
</pre>
