---
title: "nee chitram choosi song lyrics"
album: "Love Story"
artist: "Pawan Ch"
lyricist: "Mittapalli Surender"
director: "Sekhar Kammula"
path: "/albums/love-story-lyrics"
song: "Nee Chitram Choosi"
image: ../../images/albumart/love-story.jpg
date: 2021-09-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/dc-fOyYjtk8"
type: "love"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chitram choosi naa chittam chediri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitram choosi naa chittam chediri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne chitthuruvaithi rayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne chitthuruvaithi rayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Inchu inchulona ponchi unna eedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inchu inchulona ponchi unna eedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nine enchukundi rayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nine enchukundi rayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitram choosi naa chittam chediri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitram choosi naa chittam chediri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne chitthuruvaithi rayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne chitthuruvaithi rayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Inchu inchulona ponchi unna eedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inchu inchulona ponchi unna eedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nine enchukundi rayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nine enchukundi rayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa inti mundu roju vese muggu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa inti mundu roju vese muggu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gunde meedhane vesukundhunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gunde meedhane vesukundhunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorellu aa chotu naake ivvurayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorellu aa chotu naake ivvurayyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee daariloni gandharagolale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee daariloni gandharagolale"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangala vayidyalugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangala vayidyalugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttu vinipisthunna ee allarlevo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttu vinipisthunna ee allarlevo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana pelli mantralugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana pelli mantralugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atu vaipu neevu ee vaipu nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atu vaipu neevu ee vaipu nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veseti adugule yedu adugulani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veseti adugule yedu adugulani"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedu janmalaki ekamai podama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedu janmalaki ekamai podama"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha chitram prema vintha veelunama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha chitram prema vintha veelunama"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasindhi manaku prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasindhi manaku prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu naalo daachi nannu neelo vidichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu naalo daachi nannu neelo vidichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellipomantondhi prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellipomantondhi prema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee kaalam kanna oka kshanamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee kaalam kanna oka kshanamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne gelichi vasthanani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne gelichi vasthanani"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeli meghalanni pallakiga malachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeli meghalanni pallakiga malachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu ooregisthanani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu ooregisthanani"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakshamantha mana premalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakshamantha mana premalona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye cheektaina kshanakalamantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye cheektaina kshanakalamantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nuduta thilakamai nilichipovalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nuduta thilakamai nilichipovalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha chitram prema vintha veelunama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha chitram prema vintha veelunama"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasindhi manaku prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasindhi manaku prema"/>
</div>
</pre>
