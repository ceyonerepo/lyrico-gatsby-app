---
title: "vaadi vaadi vaadi song lyrics"
album: "Nagesh Thiraiyarangam"
artist: "Srikanth Deva"
lyricist: "Murugan Manthiram"
director: "Mohamad Issack"
path: "/albums/nagesh-thiraiyarangam-lyrics"
song: "Vaadi Vaadi Vaadi"
image: ../../images/albumart/nagesh-thiraiyarangam.jpg
date: 2018-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eBEv4OM0WFI"
type: "happy"
singers:
  - Gana Bala
  - Anitha Karthikeyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vaadi vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi vaadi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en sangi mangi lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sangi mangi lady"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kabaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kabaliya"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">vachen vella thaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachen vella thaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaadi vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi vaadi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en sangi mangi lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sangi mangi lady"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kabaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kabaliya"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">vachen vella thaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachen vella thaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ambala nenjile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambala nenjile"/>
</div>
<div class="lyrico-lyrics-wrapper">asaya pinchila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asaya pinchila"/>
</div>
<div class="lyrico-lyrics-wrapper">oothum pombala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothum pombala"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye pavi anjala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye pavi anjala"/>
</div>
<div class="lyrico-lyrics-wrapper">aaradi minnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaradi minnala"/>
</div>
<div class="lyrico-lyrics-wrapper">mudunee annala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudunee annala"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum thappala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum thappala"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye oorum thapala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye oorum thapala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">engala minji nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engala minji nee"/>
</div>
<div class="lyrico-lyrics-wrapper">ennatha pannuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennatha pannuva"/>
</div>
<div class="lyrico-lyrics-wrapper">paavi ambala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavi ambala"/>
</div>
<div class="lyrico-lyrics-wrapper">nee than pathi pombala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee than pathi pombala"/>
</div>
<div class="lyrico-lyrics-wrapper">kudiyai kedukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudiyai kedukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kudiyai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudiyai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum ambala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum ambala"/>
</div>
<div class="lyrico-lyrics-wrapper">evanum inga velangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evanum inga velangala"/>
</div>
<div class="lyrico-lyrics-wrapper">cutting cutting cutting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cutting cutting cutting"/>
</div>
<div class="lyrico-lyrics-wrapper">cutting cutting daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cutting cutting daa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee rava kudicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee rava kudicha"/>
</div>
<div class="lyrico-lyrics-wrapper">yemanoda dating daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yemanoda dating daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaadi vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi vaadi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en sangi mangi lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sangi mangi lady"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadi vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi vaadi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en sangi mangi lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sangi mangi lady"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kabaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kabaliya"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">vachen vella thaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachen vella thaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pombalainga illayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pombalainga illayina"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyila thunbamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyila thunbamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">sami kooda thevai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sami kooda thevai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">aamaa aamaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aamaa aamaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ambalainga kombukalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambalainga kombukalai"/>
</div>
<div class="lyrico-lyrics-wrapper">seevurathu pombala daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seevurathu pombala daa"/>
</div>
<div class="lyrico-lyrics-wrapper">boomikava samiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomikava samiyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poda poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">peya kattunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peya kattunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee nambi vazhalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nambi vazhalam"/>
</div>
<div class="lyrico-lyrics-wrapper">oru ponna kattuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru ponna kattuna"/>
</div>
<div class="lyrico-lyrics-wrapper">thenam vembi sagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenam vembi sagalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaali enbathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaali enbathe"/>
</div>
<div class="lyrico-lyrics-wrapper">oru thooku kayirudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thooku kayirudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">atha therinjum katura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha therinjum katura"/>
</div>
<div class="lyrico-lyrics-wrapper">ava theivam thanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava theivam thanadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poosai enaku vena dee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poosai enaku vena dee"/>
</div>
<div class="lyrico-lyrics-wrapper">ponga sorum venande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponga sorum venande"/>
</div>
<div class="lyrico-lyrics-wrapper">aalai vitta pothun dee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalai vitta pothun dee"/>
</div>
<div class="lyrico-lyrics-wrapper">nee podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee podi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">china mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="china mama"/>
</div>
<div class="lyrico-lyrics-wrapper">yei china mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei china mama"/>
</div>
<div class="lyrico-lyrics-wrapper">yei china mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei china mama"/>
</div>
<div class="lyrico-lyrics-wrapper">enga pora daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga pora daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muttu santhu mohini di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muttu santhu mohini di"/>
</div>
<div class="lyrico-lyrics-wrapper">mooncha pathu vanthendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mooncha pathu vanthendi"/>
</div>
<div class="lyrico-lyrics-wrapper">muttalaa aanendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muttalaa aanendi"/>
</div>
<div class="lyrico-lyrics-wrapper">podi podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podi podi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thuppu ketta dumaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuppu ketta dumaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kappadikum kumaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kappadikum kumaru"/>
</div>
<div class="lyrico-lyrics-wrapper">onnakatti pejaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnakatti pejaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">poda poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pacha kadaikum than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha kadaikum than"/>
</div>
<div class="lyrico-lyrics-wrapper">ee sales reppu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee sales reppu di"/>
</div>
<div class="lyrico-lyrics-wrapper">onna davadichathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna davadichathu"/>
</div>
<div class="lyrico-lyrics-wrapper">athu enga thapu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu enga thapu di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kollu maaku daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollu maaku daa"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kokku maaku daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kokku maaku daa"/>
</div>
<div class="lyrico-lyrics-wrapper">naan rettu pesuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan rettu pesuda"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kaalu thoosudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kaalu thoosudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vambu sandaiyum venan dee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambu sandaiyum venan dee"/>
</div>
<div class="lyrico-lyrics-wrapper">sombu sandaiyum venan dee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sombu sandaiyum venan dee"/>
</div>
<div class="lyrico-lyrics-wrapper">kadaya iluthu sathittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadaya iluthu sathittu"/>
</div>
<div class="lyrico-lyrics-wrapper">polan dee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="polan dee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaadi vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi vaadi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en sangi mangi lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sangi mangi lady"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kabaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kabaliya"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">vachen vella thaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachen vella thaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ambala nenjile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambala nenjile"/>
</div>
<div class="lyrico-lyrics-wrapper">asaya pinchila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="asaya pinchila"/>
</div>
<div class="lyrico-lyrics-wrapper">oothum pombala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothum pombala"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye pavi anjala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye pavi anjala"/>
</div>
<div class="lyrico-lyrics-wrapper">aaradi minnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaradi minnala"/>
</div>
<div class="lyrico-lyrics-wrapper">mudunee annala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudunee annala"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum thappala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum thappala"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye oorum thapala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye oorum thapala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">engala minji nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engala minji nee"/>
</div>
<div class="lyrico-lyrics-wrapper">ennatha pannuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennatha pannuva"/>
</div>
<div class="lyrico-lyrics-wrapper">paavi ambala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavi ambala"/>
</div>
<div class="lyrico-lyrics-wrapper">nee than pathi pombala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee than pathi pombala"/>
</div>
<div class="lyrico-lyrics-wrapper">kudiyai kedukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudiyai kedukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kudiyai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudiyai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum ambala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum ambala"/>
</div>
<div class="lyrico-lyrics-wrapper">evanum inga velangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evanum inga velangala"/>
</div>
<div class="lyrico-lyrics-wrapper">cutting cutting cutting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cutting cutting cutting"/>
</div>
<div class="lyrico-lyrics-wrapper">cutting cutting daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cutting cutting daa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee rava kudicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee rava kudicha"/>
</div>
<div class="lyrico-lyrics-wrapper">yemanoda dating daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yemanoda dating daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaadi vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi vaadi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en sangi mangi lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sangi mangi lady"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kabaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kabaliya"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">vachen vella thaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachen vella thaadi"/>
</div>
</pre>
