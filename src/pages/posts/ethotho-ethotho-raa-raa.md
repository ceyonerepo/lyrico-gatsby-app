---
title: "ethotho ethotho song lyrics"
album: "Raa Raa"
artist: "Srikanth Deva"
lyricist: "Na. Muthukumar"
director: "Sandilya"
path: "/albums/raa-raa-lyrics"
song: "Ethotho Ethotho"
image: ../../images/albumart/raa-raa.jpg
date: 2011-10-07
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yedhedho yedhedho kanavai tharugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho yedhedho kanavai tharugindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">adi ennulley ennulley kuzhappam tharugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi ennulley ennulley kuzhappam tharugindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhedho kanavai tharugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho kanavai tharugindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">adi ennulley ennulley kuzhappam tharugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi ennulley ennulley kuzhappam tharugindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu natpaa kaadhalaa natpaa kaadhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu natpaa kaadhalaa natpaa kaadhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sutrida vaikkindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutrida vaikkindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">adi rekkaiyindri ennai engo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi rekkaiyindri ennai engo"/>
</div>
<div class="lyrico-lyrics-wrapper">parandhida vaikkindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parandhida vaikkindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu natpaa kaadhalaa natpaa kaadhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu natpaa kaadhalaa natpaa kaadhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sutrida vaikkindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutrida vaikkindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">en vayadhin Oram penney vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vayadhin Oram penney vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">poochedi vaikkindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poochedi vaikkindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">idhazhoaram hei yeraalam vaarthaigal thoandrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhazhoaram hei yeraalam vaarthaigal thoandrum"/>
</div>
<div class="lyrico-lyrics-wrapper">irundhaalum en nenjudan nenjudan mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irundhaalum en nenjudan nenjudan mounam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho kanavai tharugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho kanavai tharugindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">adi ennulley ennulley kuzhappam tharugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi ennulley ennulley kuzhappam tharugindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu natpaa kaadhalaa natpaa kaadhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu natpaa kaadhalaa natpaa kaadhalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoorathil nindraalum pakkathil therigindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorathil nindraalum pakkathil therigindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkathil nindraalum thoorathil therigindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkathil nindraalum thoorathil therigindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjikkul sila neram thooralga tharugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjikkul sila neram thooralga tharugindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">thooralga thandhaalum karppathil vidugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooralga thandhaalum karppathil vidugindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">enna solla penney idhu yedho pudhu yekkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna solla penney idhu yedho pudhu yekkam"/>
</div>
<div class="lyrico-lyrics-wrapper">kannam vittuppochu sila naalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannam vittuppochu sila naalaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho kanavai tharugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho kanavai tharugindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">adi ennulley ennulley kuzhappam tharugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi ennulley ennulley kuzhappam tharugindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu natpaa kaadhalaa natpaa kaadhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu natpaa kaadhalaa natpaa kaadhalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veettukkul irundhaalum nenjikkul nuzhaigindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettukkul irundhaalum nenjikkul nuzhaigindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">veliye naan poanaalum ulleyum varugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veliye naan poanaalum ulleyum varugindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">peye en soottaipoal ul nenjil sudugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peye en soottaipoal ul nenjil sudugindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">suttaalum nenjikkulley thiththippai tharugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suttaalum nenjikkulley thiththippai tharugindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaththikkuchi poaley thala mela oru baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaththikkuchi poaley thala mela oru baaram"/>
</div>
<div class="lyrico-lyrics-wrapper">naththai koottam poala sonneney sila neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naththai koottam poala sonneney sila neram"/>
</div>
</pre>
