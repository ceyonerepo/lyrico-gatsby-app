---
title: "arasiyal kedi song lyrics"
album: "Tughlaq Durbar"
artist: "Govind Vasantha"
lyricist: "Karthik Netha"
director: "Balaji Tharaneetharan"
path: "/albums/tughlaq-durbar-song-lyrics"
song: "Arasiyal Kedi"
image: ../../images/albumart/tughlaq-durbar.jpg
date: 2021-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CDYpdt4nuOw"
type: "love"
singers:
  - SidVoc
  - Bhuvana Ananth
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Arasiyal kedi athanaiyum pooli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arasiyal kedi athanaiyum pooli"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachapulla poola sirikkiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachapulla poola sirikkiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedachavan bangaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedachavan bangaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta paiyan singaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta paiyan singaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikka vaachu aappu adikkiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikka vaachu aappu adikkiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karithundu veesi kandathaiyum pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karithundu veesi kandathaiyum pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandaikulla oosi yerakkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaikulla oosi yerakkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhukaiya maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhukaiya maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangamena kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamena kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Bongu bongu bongu viduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bongu bongu bongu viduran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hero hero hero hero karukkaa murukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero hero hero hero karukkaa murukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalu deal-u mavandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalu deal-u mavandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maala mela maala pottu asarum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maala mela maala pottu asarum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala vaarum yemandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala vaarum yemandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agulavuttu attu appa kooda sikkuchinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agulavuttu attu appa kooda sikkuchinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Igulavuttu yethu sikkavudu sikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Igulavuttu yethu sikkavudu sikkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulu dabba attu thokka vaara mukkuchinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulu dabba attu thokka vaara mukkuchinna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragudu raththam aadu pagachukatha singatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragudu raththam aadu pagachukatha singatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karanttu kambimela kaala vachutte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karanttu kambimela kaala vachutte"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthala vaayikulla kaiya vuttutte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthala vaayikulla kaiya vuttutte"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagada aattathula pagudu ellam pekkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagada aattathula pagudu ellam pekkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Saguni veshathula vachu vachu seiyuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saguni veshathula vachu vachu seiyuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo malaiya kaathula suthuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo malaiya kaathula suthuvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadapaaraiyaa muthukulaa kuthuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadapaaraiyaa muthukulaa kuthuvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru quateru bottlela koduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru quateru bottlela koduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaa vaangidum kathakaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaa vaangidum kathakaaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhu moosani sothula maraipan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhu moosani sothula maraipan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemakathakan kaariyam mudippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemakathakan kaariyam mudippan"/>
</div>
<div class="lyrico-lyrics-wrapper">Padupathakaan yerimalai kozhampula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padupathakaan yerimalai kozhampula"/>
</div>
<div class="lyrico-lyrics-wrapper">Soupu poduvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soupu poduvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hero hero hero hero karukka murukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero hero hero hero karukka murukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalu deal-u mavandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalu deal-u mavandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalavaari kaalavaari idamtha pudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalavaari kaalavaari idamtha pudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Molla maari ivandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molla maari ivandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkaanathu kottaiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkaanathu kottaiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavi adhu onnu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavi adhu onnu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu medaiyil poranthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu medaiyil poranthavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanda singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanda singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nara narampula naadiyil ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nara narampula naadiyil ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adam pudikkuthu kottaiya aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adam pudikkuthu kottaiya aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Etha pudunganum pudunguven neata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etha pudunganum pudunguven neata"/>
</div>
</pre>
