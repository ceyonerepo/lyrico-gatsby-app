---
title: "canteen song lyrics"
album: "Dear Comrade"
artist: "Justin Prabhakaran"
lyricist: "Rehman"
director: "Bharat Kamma"
path: "/albums/dear-comrade-lyrics"
song: "Canteen"
image: ../../images/albumart/dear-comrade.jpg
date: 2019-07-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/rTCXFddUgsQ"
type: "happy"
singers:
  - Karthik Rodriguez
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">College Canteen-U Anteney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Canteen-U Anteney"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Pakshulaku Heaven-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Pakshulaku Heaven-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Teenage Lovevu Ku Tea Coffeelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teenage Lovevu Ku Tea Coffeelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhinche Colorful Lu Station-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhinche Colorful Lu Station-U"/>
</div>
<div class="lyrico-lyrics-wrapper">College Canteen-U Anteney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Canteen-U Anteney"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Pakshulaku Heaven-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Pakshulaku Heaven-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Teenage Lovevu Ku Tea Coffeelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teenage Lovevu Ku Tea Coffeelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhinche Colorful Lu Station-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhinche Colorful Lu Station-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Corner-U Table Pai Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Corner-U Table Pai Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallallo Kalletti Thaagesthu Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallallo Kalletti Thaagesthu Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Pass-U Chestharu Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Pass-U Chestharu Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Lona Pass Aypotharu Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Lona Pass Aypotharu Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Atu Choodu Aa Kallajodu Sinnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atu Choodu Aa Kallajodu Sinnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Atu Choodu Aa Kallajodu Sinnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atu Choodu Aa Kallajodu Sinnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Mundhu Aa Neeli Kalla Sinnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Mundhu Aa Neeli Kalla Sinnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Mundhu Aa Neeli Kalla Sinnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Mundhu Aa Neeli Kalla Sinnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadi Madhyalo Okkate Okka Cool Drink
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadi Madhyalo Okkate Okka Cool Drink"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Ye Ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Ye Ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadi Madhyalo Okkate Okka Cool Drinku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadi Madhyalo Okkate Okka Cool Drinku"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnadhante Bandi Pattalekki Natte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnadhante Bandi Pattalekki Natte"/>
</div>
<div class="lyrico-lyrics-wrapper">College Canteen-U Anteney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College Canteen-U Anteney"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Pakshulaku Heaven-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Pakshulaku Heaven-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Teenage Lovevu Ku Tea Coffeelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teenage Lovevu Ku Tea Coffeelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhinche Colorful Lu Station-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhinche Colorful Lu Station-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhilindhi Nee Naava Jagartha Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhilindhi Nee Naava Jagartha Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhilindhi Nee Naava Jagartha Mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhilindhi Nee Naava Jagartha Mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Samudhramantha Lothaindhi Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samudhramantha Lothaindhi Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Samudhramantha Lothaindhi Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samudhramantha Lothaindhi Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Alalenni Yedhuraina Nee Dhaari Lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alalenni Yedhuraina Nee Dhaari Lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhaloddhu Aa Cheiyi Yedhi Yemyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhaloddhu Aa Cheiyi Yedhi Yemyna"/>
</div>
</pre>
