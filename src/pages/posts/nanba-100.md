---
title: "nanba song lyrics"
album: "100"
artist: "Sam C.S."
lyricist: "Sam. C. S"
director: "Sam Anton"
path: "/albums/100-lyrics"
song: "Nanba"
image: ../../images/albumart/100.jpg
date: 2019-05-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pq9nac7Iw9w"
type: "sad"
singers:
  - Sam. C.S
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vizhi Verkkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Verkkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Aaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Aaguren"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nanba Neeyum Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nanba Neeyum Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Vittu Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Vittu Pona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavaragumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavaragumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seriyagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seriyagumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nanba Neeyum Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nanba Neeyum Yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani Aakki Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Aakki Pona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Senjathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Senjathellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyaayam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Naal Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellarukkum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarukkum Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangaama Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangaama Poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangaama Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangaama Poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Thol Kudukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thol Kudukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Yaar Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Yaar Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kooda Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kooda Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Poda Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Poda Poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali Theerthu Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Theerthu Poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyiraa Iruppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiraa Iruppaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyira Koduppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyira Koduppaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Kadavul Padaicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Kadavul Padaicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravil Idhu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravil Idhu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirparppu Illathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirparppu Illathathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi Verkkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Verkkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Aaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Aaguren"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nanba Neeyum Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nanba Neeyum Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Vittu Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Vittu Pona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavaragumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavaragumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seriyagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seriyagumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nanba Neeyum Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nanba Neeyum Yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani Aakki Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Aakki Pona"/>
</div>
</pre>
