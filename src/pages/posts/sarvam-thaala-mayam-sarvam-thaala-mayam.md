---
title: "sarvam thaala mayam song lyrics"
album: "Sarvam Thaala Mayam"
artist: "A. R. Rahman"
lyricist: "Madhan Karky"
director: "Rajiv Menon"
path: "/albums/sarvam-thaala-mayam-lyrics"
song: "Sarvam Thaala Mayam"
image: ../../images/albumart/sarvam-thaala-mayam.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bDorKQg8Uyc"
type: "happy"
singers:
  - Haricharan
  - Arjun Chandy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Karuvukkul Pookkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvukkul Pookkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottrai Thudipodu Thondangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottrai Thudipodu Thondangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karavangal Theerkkum Ottrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karavangal Theerkkum Ottrai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudipodu Adangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudipodu Adangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karumegam Muttum Bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumegam Muttum Bodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnengum Oilkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnengum Oilkka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhi Kai Thattum Bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhi Kai Thattum Bodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karai Engum Olikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Engum Olikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalaum Uyrium
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalaum Uyrium"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadattume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadattume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarvam Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarvam Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaalam Indri Yedhu Nayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalam Indri Yedhu Nayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Rekkai Ethir Kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Rekkai Ethir Kaatril"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idum Thaalam Kaelaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idum Thaalam Kaelaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sara Saravena Illaigal Podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara Saravena Illaigal Podum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illanthaalam Kelaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illanthaalam Kelaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karvangal Theerkkum Ottrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karvangal Theerkkum Ottrai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudipodu Adangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudipodu Adangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karumegam Muttum Bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumegam Muttum Bodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vin Engum Olikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vin Engum Olikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhi Kai Thattum Bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhi Kai Thattum Bodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karai Engum Olikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Engum Olikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalum Uyirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalum Uyirum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadattume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadattume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarvam Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarvam Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaalam Indri Yedhu Nayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalam Indri Yedhu Nayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erumbugal Padai Eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumbugal Padai Eduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorndhey Varum Thaala Charam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorndhey Varum Thaala Charam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arumbugal Dhinam Udaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumbugal Dhinam Udaithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaeni Thirudum Pookkalin Udhiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeni Thirudum Pookkalin Udhiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharai Melaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharai Melaththil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Olikkindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Olikkindra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaname Adhiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaname Adhiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karainthidum Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karainthidum Maname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeyil Moolum Thaalam Kettutidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyil Moolum Thaalam Kettutidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeym Naanum Kaalathin Thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeym Naanum Kaalathin Thaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undaanom Mei Thaalathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaanom Mei Thaalathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhgindrom Poi Thaalathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhgindrom Poi Thaalathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thai Thai Thai Thai Thaalathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thai Thai Thai Thai Thaalathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thai Thai Thai Thaaka Thimi Thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thai Thai Thai Thaaka Thimi Thakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarvam Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarvam Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaalam Indri Yethu Nayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalam Indri Yethu Nayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuvukkul Pookkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvukkul Pookkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottrai Thudipodu Thondangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottrai Thudipodu Thondangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karvangal Theerkkum Ottrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karvangal Theerkkum Ottrai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudipodu Adangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudipodu Adangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karumegam Muttum Bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karumegam Muttum Bodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnengum Olikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnengum Olikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhi Kai Thattum Bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhi Kai Thattum Bodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karai Engum Olikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai Engum Olikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalum Uyirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalum Uyirum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadattume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadattume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarvam Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarvam Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaalam Indri Yedhu Nayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalam Indri Yedhu Nayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Rekkai Ethir Kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Rekkai Ethir Kaatril"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhum Thaalam Kelaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhum Thaalam Kelaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarvam Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarvam Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh Sarvam Thaala Mayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Sarvam Thaala Mayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaalam Indri Yedhu Nayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalam Indri Yedhu Nayam"/>
</div>
</pre>
