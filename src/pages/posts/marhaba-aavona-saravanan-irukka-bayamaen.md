---
title: "marhaba aavona song lyrics"
album: "Saravanan Irukka Bayamaen"
artist: "D Imman"
lyricist: "Yugabharathi"
director: "Ezhil"
path: "/albums/saravanan-irukka-bayamaen-lyrics"
song: "Marhaba Aavona"
image: ../../images/albumart/saravanan-irukka-bayamaen.jpg
date: 2017-05-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ppZyWLm7sb4"
type: "love"
singers:
  -	Shreya Ghoshal
  - Aditya Gadhvi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maruhabaa aavo naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruhabaa aavo naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathilae nee thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathilae nee thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyumae naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyumae naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thullinenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thullinenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulli maanaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli maanaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maruhabaa aavo naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruhabaa aavo naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathilae nee thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathilae nee thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyumae naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyumae naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thullinenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thullinenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulli maanaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli maanaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennila piraiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennila piraiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthathae tharaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathae tharaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yega iraivanin kodai neeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yega iraivanin kodai neeyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennila thisaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennila thisaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan yezhisaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan yezhisaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maada vilakkathan oli neeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maada vilakkathan oli neeyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil kanaa kanaa kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil kanaa kanaa kanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaal kalainthenae naan aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaal kalainthenae naan aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maruhabaa aavo naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruhabaa aavo naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathilae nee thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathilae nee thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyumae naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyumae naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thullinenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thullinenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulli maanaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli maanaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annai madiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai madiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedum kuzhanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum kuzhanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandavudan thaaviduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandavudan thaaviduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha kathai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha kathai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai manamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai manamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un azhagai yenthiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un azhagai yenthiduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha sugam sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha sugam sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithamum thodara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithamum thodara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennangal yengiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennangal yengiduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam poru poru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam poru poru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena nee nagarnthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena nee nagarnthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennullam thengiduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennullam thengiduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaivae suduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivae suduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamae keduthae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamae keduthae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maruhabaa aavo naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruhabaa aavo naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathilae nee thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathilae nee thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyumae naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyumae naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thullinenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thullinenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulli maanaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli maanaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaa aa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aa aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velli salangai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli salangai"/>
</div>
<div class="lyrico-lyrics-wrapper">Osai marakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osai marakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkkapada nee sirithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkkapada nee sirithaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum nedu naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum nedu naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanngal unai paarthirunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanngal unai paarthirunthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manjal nila nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal nila nila"/>
</div>
<div class="lyrico-lyrics-wrapper">Masakkai adaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masakkai adaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sollai kettirunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sollai kettirunthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anthi pagal pagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthi pagal pagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ravikkai aniyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravikkai aniyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaiyil naan irunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaiyil naan irunthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai nee adainthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai nee adainthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvae peru naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvae peru naal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maruhabaa aavo naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruhabaa aavo naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathilae nee thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathilae nee thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyumae naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyumae naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thullinenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thullinenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulli maanaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli maanaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maruhabaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruhabaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavoooo naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavoooo naa"/>
</div>
</pre>
