---
title: "ekka chakkam aaguthae song lyrics"
album: "Tamil Rockers"
artist: "Premgi Amaren"
lyricist: "Gangai Amaren"
director: "Barani Jayapal"
path: "/albums/tamil-rockers-lyrics"
song: "Ekka Chakkam Aaguthae"
image: ../../images/albumart/tamil-rockers.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6DFmWe535rk"
type: "happy"
singers:
  - Premgi Amaren
  - Swagatha Krishnan
  - Hema Ambika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ekka chekkam aaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekka chekkam aaguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">en pakkam sorgam pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pakkam sorgam pole"/>
</div>
<div class="lyrico-lyrics-wrapper">varum accham vittu ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varum accham vittu ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ini uchcham uchcham mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini uchcham uchcham mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ekka chekkam aaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekka chekkam aaguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">en pakkam sorgam pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en pakkam sorgam pole"/>
</div>
<div class="lyrico-lyrics-wrapper">varum accham vittu ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varum accham vittu ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ini uchcham uchcham mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini uchcham uchcham mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suthiyodu suthi aerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthiyodu suthi aerum"/>
</div>
<div class="lyrico-lyrics-wrapper">ini jamuku jamuku jigu jaalao
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini jamuku jamuku jigu jaalao"/>
</div>
<div class="lyrico-lyrics-wrapper">radhi odu vilayadu oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="radhi odu vilayadu oru"/>
</div>
<div class="lyrico-lyrics-wrapper">raja yogam indha neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raja yogam indha neram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aa aan aa aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa aan aa aan"/>
</div>
<div class="lyrico-lyrics-wrapper">aahaan aahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aahaan aahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">aa aan aa aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa aan aa aan"/>
</div>
<div class="lyrico-lyrics-wrapper">aahaan aahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aahaan aahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">aa aan aa aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa aan aa aan"/>
</div>
<div class="lyrico-lyrics-wrapper">aahaan aahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aahaan aahaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">lakum kikum thokum thokum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lakum kikum thokum thokum"/>
</div>
<div class="lyrico-lyrics-wrapper">engum etti pakum pakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum etti pakum pakum"/>
</div>
<div class="lyrico-lyrics-wrapper">elathaiyum ethi kepom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elathaiyum ethi kepom"/>
</div>
<div class="lyrico-lyrics-wrapper">mathathae maathi papom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathathae maathi papom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mupatanum aattam pottan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mupatanum aattam pottan"/>
</div>
<div class="lyrico-lyrics-wrapper">en paatanum aadi theethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en paatanum aadi theethan"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo ini en pole dan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo ini en pole dan"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan peranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan peranum"/>
</div>
<div class="lyrico-lyrics-wrapper">muthalayum kadhal serntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthalayum kadhal serntha"/>
</div>
<div class="lyrico-lyrics-wrapper">epothume onnae onnuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epothume onnae onnuu"/>
</div>
<div class="lyrico-lyrics-wrapper">amma appa attam poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amma appa attam poda"/>
</div>
<div class="lyrico-lyrics-wrapper">nan than moraponnuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan than moraponnuu"/>
</div>
<div class="lyrico-lyrics-wrapper">kotha dha dha kothi theeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotha dha dha kothi theeru"/>
</div>
<div class="lyrico-lyrics-wrapper">katta dha dha katti cheru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta dha dha katti cheru"/>
</div>
<div class="lyrico-lyrics-wrapper">ettadhadhae etti aeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettadhadhae etti aeru"/>
</div>
<div class="lyrico-lyrics-wrapper">kettorukkum nanmae kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kettorukkum nanmae kooru"/>
</div>
<div class="lyrico-lyrics-wrapper">ellathukkum mela toppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellathukkum mela toppu"/>
</div>
<div class="lyrico-lyrics-wrapper">istam pola pottu thaaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="istam pola pottu thaaku"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthaattama kuthu pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthaattama kuthu pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu kittu aadi kaatuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu kittu aadi kaatuu"/>
</div>
<div class="lyrico-lyrics-wrapper">ippo vitta eppo varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippo vitta eppo varum"/>
</div>
<div class="lyrico-lyrics-wrapper">entha neram dhaannn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha neram dhaannn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">lakum kikum thokum thokum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lakum kikum thokum thokum"/>
</div>
<div class="lyrico-lyrics-wrapper">engum etti pakum pakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum etti pakum pakum"/>
</div>
<div class="lyrico-lyrics-wrapper">elathaiyum ethi kepom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elathaiyum ethi kepom"/>
</div>
<div class="lyrico-lyrics-wrapper">mathathae maathi papom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathathae maathi papom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannu vechu kaiya vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu vechu kaiya vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">ennennavo aagi pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennennavo aagi pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">unna suthi ponnu puthihi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna suthi ponnu puthihi"/>
</div>
<div class="lyrico-lyrics-wrapper">onna senthaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna senthaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">un vayasu moonu aaruna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vayasu moonu aaruna"/>
</div>
<div class="lyrico-lyrics-wrapper">en vayasu nalu aaramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vayasu nalu aaramma"/>
</div>
<div class="lyrico-lyrics-wrapper">rendum ipo sendha dhunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendum ipo sendha dhunna"/>
</div>
<div class="lyrico-lyrics-wrapper">romba dhoolamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba dhoolamma"/>
</div>
<div class="lyrico-lyrics-wrapper">thanniyila kolam podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanniyila kolam podu"/>
</div>
<div class="lyrico-lyrics-wrapper">aadi kaathil deebam yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadi kaathil deebam yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">aagayathil kotta kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagayathil kotta kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">andharathil aattam podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andharathil aattam podu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey aandavana kootivandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey aandavana kootivandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">avara angae kaaval podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avara angae kaaval podu"/>
</div>
<div class="lyrico-lyrics-wrapper">athanaiyum nadakum yemma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athanaiyum nadakum yemma"/>
</div>
<div class="lyrico-lyrics-wrapper">aada vecha kedaikum amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aada vecha kedaikum amma"/>
</div>
<div class="lyrico-lyrics-wrapper">hey apparomma epporamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey apparomma epporamma"/>
</div>
<div class="lyrico-lyrics-wrapper">namma aatchi dan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma aatchi dan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">lakum kikum thokum thokum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lakum kikum thokum thokum"/>
</div>
<div class="lyrico-lyrics-wrapper">engum etti pakum pakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum etti pakum pakum"/>
</div>
<div class="lyrico-lyrics-wrapper">elathaiyum ethi kepom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elathaiyum ethi kepom"/>
</div>
<div class="lyrico-lyrics-wrapper">mathathae maathi papom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathathae maathi papom"/>
</div>
</pre>
