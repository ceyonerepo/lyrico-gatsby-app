---
title: "punya paap punya paap song lyrics"
album: "Punya Paap"
artist: "iLL Wayno"
lyricist: "DIVINE"
director: "Abhay Raha"
path: "/albums/punya-paap-lyrics"
song: "Punya Paap Punya Paap"
image: ../../images/albumart/punya-paap.jpg
date: 2020-12-03
lang: hindi
youtubeLink: "https://www.youtube.com/embed/4wTTAWjNlDM"
type: "happy"
singers:
  - DIVINE
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jabse paida tab se kiya humne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jabse paida tab se kiya humne"/>
</div>
<div class="lyrico-lyrics-wrapper">Jabse paida tab se kiya humne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jabse paida tab se kiya humne"/>
</div>
<div class="lyrico-lyrics-wrapper">Jabse paida tab se kiya humne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jabse paida tab se kiya humne"/>
</div>
<div class="lyrico-lyrics-wrapper">Jabse paida tab se kiya humne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jabse paida tab se kiya humne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jabse paida tab se kiya humne punya paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jabse paida tab se kiya humne punya paap"/>
</div>
<div class="lyrico-lyrics-wrapper">Jitna mila dugna diya kiya punya paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jitna mila dugna diya kiya punya paap"/>
</div>
<div class="lyrico-lyrics-wrapper">Khoon pasina chauda seena karna punya aaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khoon pasina chauda seena karna punya aaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum na darte paap karke seekhe punya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum na darte paap karke seekhe punya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Punya paap punya paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punya paap punya paap"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun hai khaas kaun hai sath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun hai khaas kaun hai sath"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaale baadal kala baarish
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaale baadal kala baarish"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya hai dharm kya hai jaat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya hai dharm kya hai jaat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaun banaya kiska hath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun banaya kiska hath"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarfira yeh sarfarosh sarvanash
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarfira yeh sarfarosh sarvanash"/>
</div>
<div class="lyrico-lyrics-wrapper">Kisko chunta desh ka labh ya khudka swarth
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisko chunta desh ka labh ya khudka swarth"/>
</div>
<div class="lyrico-lyrics-wrapper">Manavata par kar prakash
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manavata par kar prakash"/>
</div>
<div class="lyrico-lyrics-wrapper">Nahi samajhta mujhko dharm jaat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nahi samajhta mujhko dharm jaat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu daddy pe tha main maggi pe tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu daddy pe tha main maggi pe tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu skinny mein hai main baggy mein tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu skinny mein hai main baggy mein tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh rap aur hip-hop mera planning nahi tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh rap aur hip-hop mera planning nahi tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Main paise kamaane ke planing mein tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main paise kamaane ke planing mein tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ganda ya saaf mangta anaaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganda ya saaf mangta anaaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Jab doodh nahi ghar pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab doodh nahi ghar pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Tab kuch nahi aata hai yaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tab kuch nahi aata hai yaad"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya hai punya aur kya hai paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya hai punya aur kya hai paap"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jabse paida tab se kiya humne punya paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jabse paida tab se kiya humne punya paap"/>
</div>
<div class="lyrico-lyrics-wrapper">Jitna mila dugna diya kiya punya paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jitna mila dugna diya kiya punya paap"/>
</div>
<div class="lyrico-lyrics-wrapper">Khoon pasina chauda seena karna punya aaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khoon pasina chauda seena karna punya aaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum na darte paap karke seekhe punya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum na darte paap karke seekhe punya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Punya paap punya paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punya paap punya paap"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal hai punya aaj hai paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal hai punya aaj hai paap"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaale kapde kaali raat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaale kapde kaali raat"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaun hai sath lambi khaat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaun hai sath lambi khaat"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaadi chalti ghoomte hath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaadi chalti ghoomte hath"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehnat zyada luck de sath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehnat zyada luck de sath"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deal jab karte patte khaas
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deal jab karte patte khaas"/>
</div>
<div class="lyrico-lyrics-wrapper">Suffer karte bante laash
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suffer karte bante laash"/>
</div>
<div class="lyrico-lyrics-wrapper">Girne ka mera hai intezaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girne ka mera hai intezaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahenge zindabad jab tak intekal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahenge zindabad jab tak intekal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unleashed yeh sher mere daant pe rakth hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unleashed yeh sher mere daant pe rakth hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jalan hai bimari jo yeh sath mein rakhte hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalan hai bimari jo yeh sath mein rakhte hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Isliye covid se pehle hum fasle rakthe hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isliye covid se pehle hum fasle rakthe hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Beimani ki wajah badalte takht hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beimani ki wajah badalte takht hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chhapri hai tu tere gaane saste hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhapri hai tu tere gaane saste hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo lad nahi sakte woh gaali bakte hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo lad nahi sakte woh gaali bakte hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Moses banjate jab paani sar pe hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moses banjate jab paani sar pe hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaali ko mante yahan andh bhakt hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaali ko mante yahan andh bhakt hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan bhagwan mein vishwas insaan dhokebaaz
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan bhagwan mein vishwas insaan dhokebaaz"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek number rahenge jab tak bante nahi laash
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek number rahenge jab tak bante nahi laash"/>
</div>
<div class="lyrico-lyrics-wrapper">Divine mera spirit hum bante nahi laash
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Divine mera spirit hum bante nahi laash"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum takht pe baithe tu karte reh naach
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum takht pe baithe tu karte reh naach"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere ginti ke din hain hisaab mein le saans
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere ginti ke din hain hisaab mein le saans"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum O G jiyenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum O G jiyenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jabse paida tab se kiya humne punya paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jabse paida tab se kiya humne punya paap"/>
</div>
<div class="lyrico-lyrics-wrapper">Jitna mila dugna diya kiya punya paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jitna mila dugna diya kiya punya paap"/>
</div>
<div class="lyrico-lyrics-wrapper">Khoon pasina chauda seena karna punya aaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khoon pasina chauda seena karna punya aaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum na darte paap karke seekhe punya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum na darte paap karke seekhe punya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jee le beta teri aani wali maut
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee le beta teri aani wali maut"/>
</div>
<div class="lyrico-lyrics-wrapper">Jitna kar le tu punya ya paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jitna kar le tu punya ya paap"/>
</div>
<div class="lyrico-lyrics-wrapper">Band pinjre se udde hum aazad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Band pinjre se udde hum aazad"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud parosne pe aata mujhe swaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud parosne pe aata mujhe swaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dusri album nahi meri dusri yeh kitaab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dusri album nahi meri dusri yeh kitaab"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum bille yeh billi sirf raaste de kaat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum bille yeh billi sirf raaste de kaat"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirf darte hum bhagwan se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirf darte hum bhagwan se"/>
</div>
<div class="lyrico-lyrics-wrapper">Karte punya aur paap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karte punya aur paap"/>
</div>
</pre>
