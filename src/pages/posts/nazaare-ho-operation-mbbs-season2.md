---
title: "nazaare ho song lyrics"
album: "operation MBBD season 2"
artist: "Karthik Rao"
lyricist: "Durgesh Singh"
director: "Karthik Rao"
path: "/albums/operation-mbbs-season2-lyrics"
song: "Nazaare Ho"
image: ../../images/albumart/operation-mbbs-season2.jpg
date: 2021-03-22
lang: hindi
youtubeLink: "https://www.youtube.com/embed/V0CVnfqsLCE"
type: "Mass"
singers:
  - Karthik Rao
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ho dasturon ko dhata bata ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho dasturon ko dhata bata ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud ke mann ke pata bataake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud ke mann ke pata bataake"/>
</div>
<div class="lyrico-lyrics-wrapper">Window seat kari hai offer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Window seat kari hai offer"/>
</div>
<div class="lyrico-lyrics-wrapper">Qismat ne phir paas betha ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Qismat ne phir paas betha ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho nazaare ho, nazaare ho..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho nazaare ho, nazaare ho.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ho nazaare ho, nazaare ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho nazaare ho, nazaare ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uddne ki chahat thhi kacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uddne ki chahat thhi kacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Par girne ki fitrat thi sacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par girne ki fitrat thi sacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Khone ke mausam mein yaaron
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khone ke mausam mein yaaron"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuch paane ki aadat hai acchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuch paane ki aadat hai acchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho neend ko aankh di humne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho neend ko aankh di humne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ummeedon ka saath nibhake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ummeedon ka saath nibhake"/>
</div>
<div class="lyrico-lyrics-wrapper">Window seat kari hai offer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Window seat kari hai offer"/>
</div>
<div class="lyrico-lyrics-wrapper">Qismat ne phir paas betha ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Qismat ne phir paas betha ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho nazaare ho, nazaare ho..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho nazaare ho, nazaare ho.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ho nazaare ho, nazaare ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho nazaare ho, nazaare ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tune maine sabne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tune maine sabne"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekhe thhe aune paune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekhe thhe aune paune"/>
</div>
<div class="lyrico-lyrics-wrapper">Aage hoga kya pata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aage hoga kya pata"/>
</div>
<div class="lyrico-lyrics-wrapper">Log baag sab ek raag hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Log baag sab ek raag hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Apni hi dhun tu suna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni hi dhun tu suna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho sakht chhaton pe ugaa hai bargad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho sakht chhaton pe ugaa hai bargad"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekho sabkuch tod taadke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekho sabkuch tod taadke"/>
</div>
<div class="lyrico-lyrics-wrapper">Window seat kari hai offer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Window seat kari hai offer"/>
</div>
<div class="lyrico-lyrics-wrapper">Qismat ne phir paas betha ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Qismat ne phir paas betha ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho nazaare ho, nazaare ho..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho nazaare ho, nazaare ho.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ho nazaare ho, nazaare ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho nazaare ho, nazaare ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuch aadha hoga adhoora hoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuch aadha hoga adhoora hoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo apne mann ka woh poora hoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo apne mann ka woh poora hoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Param ke paani mein bhatkati naavon ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Param ke paani mein bhatkati naavon ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyi na koyi kinaara hoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyi na koyi kinaara hoga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho jakdi muthhi khol hawa mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jakdi muthhi khol hawa mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Baat banegi baat baat mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baat banegi baat baat mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Soch saach ke kyun hai jeena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soch saach ke kyun hai jeena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun jeena hai inch naap ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun jeena hai inch naap ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho nazaare ho, nazaare ho..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho nazaare ho, nazaare ho.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ho nazaare ho, nazaare ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho nazaare ho, nazaare ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho dasturon ko dhata bata ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho dasturon ko dhata bata ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud ke mann ke pata bataake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud ke mann ke pata bataake"/>
</div>
<div class="lyrico-lyrics-wrapper">Window seat kari hai offer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Window seat kari hai offer"/>
</div>
<div class="lyrico-lyrics-wrapper">Qismat ne phir paas betha ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Qismat ne phir paas betha ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho nazaare ho, nazaare ho..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho nazaare ho, nazaare ho.."/>
</div>
</pre>
