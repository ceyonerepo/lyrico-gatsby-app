---
title: "daakko daakko meka song lyrics"
album: "Pushpa The Rise"
artist: "Devi Sri Prasad"
lyricist: "Chandrabose"
director: "Sukumar"
path: "/albums/pushpa-the-rise-lyrics"
song: "Daakko Daakko Meka"
image: ../../images/albumart/pushpa-the-rise.jpg
date: 2021-12-17
lang: telugu
youtubeLink: "https://www.youtube.com/embed/irsLfoFb-W0"
type: "mass"
singers:
  - Shivam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thandhane thanananenaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhane thanananenaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhane thanananenaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhane thanananenaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanane thandhinaane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanane thandhinaane "/>
</div>
<div class="lyrico-lyrics-wrapper">thanane thanananenaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanane thanananenaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluthuru thintadhi aaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthuru thintadhi aaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluthuru thintadhi aaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthuru thintadhi aaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakunu thintadhi meka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakunu thintadhi meka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakunu thintadhi meka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakunu thintadhi meka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekanu thintadhi puli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekanu thintadhi puli"/>
</div>
<div class="lyrico-lyrics-wrapper">Mekanu thintadhi puli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mekanu thintadhi puli"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi kadara aakali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi kadara aakali"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi kadara aakali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi kadara aakali"/>
</div>
<div class="lyrico-lyrics-wrapper">Puline thintadhi chaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puline thintadhi chaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaabuni thintadhi kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaabuni thintadhi kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalanni thintadu kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalanni thintadu kaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi maha aakali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi maha aakali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetadedhi okati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetadedhi okati"/>
</div>
<div class="lyrico-lyrics-wrapper">Parigetthedhi inkokati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parigetthedhi inkokati"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorikindha idhi sasthadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorikindha idhi sasthadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorakkapothe adhi sasthadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorakkapothe adhi sasthadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka jeeviki aakalesindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka jeeviki aakalesindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Inko jeeviki aayuvu mudindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inko jeeviki aayuvu mudindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey daakko daakko meka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey daakko daakko meka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulochhi korukuddhi peeka huy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulochhi korukuddhi peeka huy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chepaku purugu era
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chepaku purugu era"/>
</div>
<div class="lyrico-lyrics-wrapper">Pittaki nukalu era
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pittaki nukalu era"/>
</div>
<div class="lyrico-lyrics-wrapper">Kukkaki mamsam mukka era
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kukkaki mamsam mukka era"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangamma thalli jaathara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangamma thalli jaathara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollu pottella kothara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollu pottella kothara"/>
</div>
<div class="lyrico-lyrics-wrapper">Katthiki netthuti puthara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katthiki netthuti puthara"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathakaina tappadhu era
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathakaina tappadhu era"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi lokam talaraathara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi lokam talaraathara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Emarapatuga unnava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emarapatuga unnava"/>
</div>
<div class="lyrico-lyrics-wrapper">Erake chikkesthavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erake chikkesthavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Erane minge aakalunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erane minge aakalunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikkada bathikuntavu ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikkada bathikuntavu ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaale kadupu sudadhuro neethi nyayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaale kadupu sudadhuro neethi nyayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Balam unnodidhera ikkada ista rajyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balam unnodidhera ikkada ista rajyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey daakko daakko meka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey daakko daakko meka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulochhi korukuddhi peeka huy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulochhi korukuddhi peeka huy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adigithe puttadhu aruvu aruvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigithe puttadhu aruvu aruvu"/>
</div>
<div class="lyrico-lyrics-wrapper">brathimalithe brathuke baruvu baruvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="brathimalithe brathuke baruvu baruvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottara undadhu karuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottara undadhu karuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Devudikaina dhebbe guruvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devudikaina dhebbe guruvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannulu chese melu tammudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannulu chese melu tammudu "/>
</div>
<div class="lyrico-lyrics-wrapper">kuda cheyyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuda cheyyadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baddhudu cheppe paatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baddhudu cheppe paatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Baddhudu kuda cheppade hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baddhudu kuda cheppade hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaggedhe le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaggedhe le"/>
</div>
</pre>
