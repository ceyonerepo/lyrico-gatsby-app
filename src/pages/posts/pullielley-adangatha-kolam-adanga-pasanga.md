---
title: "pullielley adangatha kolam song lyrics"
album: "Adanga Pasanga"
artist: "AK Alldern"
lyricist: "N Jaigar Devadass"
director: "R. Selvanathan"
path: "/albums/adanga-pasanga-lyrics"
song: "Pullielley Adangatha Kolam"
image: ../../images/albumart/adanga-pasanga.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/x9idVyp7gws"
type: "happy"
singers:
  - Abhay
  - Deepak
  - Ramkumar
  - Famish
  - Sushanth 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">pullielley adangatha kolam neenga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullielley adangatha kolam neenga "/>
</div>
<div class="lyrico-lyrics-wrapper">adipattu kilunchu pona melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adipattu kilunchu pona melam"/>
</div>
<div class="lyrico-lyrics-wrapper">pullielley adangatha kolam neenga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullielley adangatha kolam neenga "/>
</div>
<div class="lyrico-lyrics-wrapper">adipattu kilunchu pona melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adipattu kilunchu pona melam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">venumada venumada kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venumada venumada kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">venumada venumada kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venumada venumada kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">vambu thumbula than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambu thumbula than"/>
</div>
<div class="lyrico-lyrics-wrapper">pirakumada mothal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirakumada mothal"/>
</div>
<div class="lyrico-lyrics-wrapper">vambu thumbula than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambu thumbula than"/>
</div>
<div class="lyrico-lyrics-wrapper">pirakumada mothal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirakumada mothal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pullielley adangatha kolam neenga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullielley adangatha kolam neenga "/>
</div>
<div class="lyrico-lyrics-wrapper">adipattu kilunchu pona melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adipattu kilunchu pona melam"/>
</div>
<div class="lyrico-lyrics-wrapper">neenga adipattu kilunchu pona melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga adipattu kilunchu pona melam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey patti thotti paatu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey patti thotti paatu "/>
</div>
<div class="lyrico-lyrics-wrapper">padi aatam poteengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padi aatam poteengal"/>
</div>
<div class="lyrico-lyrics-wrapper">vetti pechu pesi pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetti pechu pesi pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">veena poneenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veena poneenga"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey patti thotti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey patti thotti "/>
</div>
<div class="lyrico-lyrics-wrapper">marupadi aatam poteengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marupadi aatam poteengal"/>
</div>
<div class="lyrico-lyrics-wrapper">vetti pechu pesi pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetti pechu pesi pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">veena poneenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veena poneenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">billavum neenga illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="billavum neenga illa"/>
</div>
<div class="lyrico-lyrics-wrapper">hey billavum neenga illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey billavum neenga illa"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey jillavum neenga illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey jillavum neenga illa"/>
</div>
<div class="lyrico-lyrics-wrapper">villavum neenga illa odunju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="villavum neenga illa odunju"/>
</div>
<div class="lyrico-lyrics-wrapper">pona villu thane neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pona villu thane neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">odunju pona villu thane neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odunju pona villu thane neenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pullielley adangatha kolam neenga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullielley adangatha kolam neenga "/>
</div>
<div class="lyrico-lyrics-wrapper">adipattu kilunchu pona melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adipattu kilunchu pona melam"/>
</div>
<div class="lyrico-lyrics-wrapper">pullielley adangatha kolam neenga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullielley adangatha kolam neenga "/>
</div>
<div class="lyrico-lyrics-wrapper">adipattu kilunchu pona melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adipattu kilunchu pona melam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aruva aasa valakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aruva aasa valakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vidala pasanga naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidala pasanga naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">thiruvodu aentha vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruvodu aentha vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvodu ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvodu ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">aruva aasa valakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aruva aasa valakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vidala pasanga naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidala pasanga naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">thiruvodu aentha vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruvodu aentha vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvodu ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvodu ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">super star neenga illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="super star neenga illa"/>
</div>
<div class="lyrico-lyrics-wrapper">power star neenga illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="power star neenga illa"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey super star neenga illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey super star neenga illa"/>
</div>
<div class="lyrico-lyrics-wrapper">power star neenga illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="power star neenga illa"/>
</div>
<div class="lyrico-lyrics-wrapper">pinju pona naaru thane neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinju pona naaru thane neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">pinju pona naaru thane neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinju pona naaru thane neenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pullielley adangatha kolam neenga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullielley adangatha kolam neenga "/>
</div>
<div class="lyrico-lyrics-wrapper">adipattu kilunchu pona melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adipattu kilunchu pona melam"/>
</div>
<div class="lyrico-lyrics-wrapper">neenga adipattu kilunchu pona melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga adipattu kilunchu pona melam"/>
</div>
<div class="lyrico-lyrics-wrapper">neenga adipattu kilunchu pona melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga adipattu kilunchu pona melam"/>
</div>
<div class="lyrico-lyrics-wrapper">neenga adipattu kilunchu pona melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga adipattu kilunchu pona melam"/>
</div>
</pre>
