---
title: "un pirivu song lyrics"
album: "Sila Nerangalil Sila Manidhargal"
artist: "Radhan"
lyricist: "RJ Vijay"
director: "Vishal Venkat"
path: "/albums/sila-nerangalil-sila-manidhargal-lyrics"
song: "Un Pirivu"
image: ../../images/albumart/sila-nerangalil-sila-manidhargal.jpg
date: 2022-01-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MdAl_CmhiAk"
type: "sad"
singers:
  - Sarath Santhosh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Un pirivu un maraivu un thadangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pirivu un maraivu un thadangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini theda paadhai illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini theda paadhai illaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaniyaa yaar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaniyaa yaar "/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiyaa ver izhandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiyaa ver izhandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovukku naatkkal neelumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovukku naatkkal neelumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholile thookki nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholile thookki nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan uyaram kaattinaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan uyaram kaattinaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Osai kottida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osai kottida"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoorathil isai meettinaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoorathil isai meettinaay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarthaiyum vaazhkkaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaiyum vaazhkkaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugaipadam kadhai sollumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugaipadam kadhai sollumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam athai paarthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam athai paarthida"/>
</div>
<div class="lyrico-lyrics-wrapper">En manam thaan thaangumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manam thaan thaangumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un pirivu un maraivu un thadangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pirivu un maraivu un thadangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini theda paadhai illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini theda paadhai illaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pirivu un maraivu un thadangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pirivu un maraivu un thadangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini theda paadhai illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini theda paadhai illaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaniyaa yaar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaniyaa yaar "/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiyaa ver izhandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiyaa ver izhandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovukku naatkkal neelumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovukku naatkkal neelumo"/>
</div>
</pre>
