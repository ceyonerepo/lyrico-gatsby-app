---
title: "idhe kadha nee katha song lyrics"
album: "Maharshi"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Vamshi Paidipally"
path: "/albums/maharshi-lyrics"
song: "Idhe Kadha Nee Katha"
image: ../../images/albumart/maharshi.jpg
date: 2019-05-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/G7Fxzz-NH_s"
type: "mass"
singers:
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhe katha idhe katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhe katha idhe katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muginpu lenidai sadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muginpu lenidai sadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhe katha idhe katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhe katha idhe katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muginpu lenidai sadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muginpu lenidai sadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kanti reppalanchuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kanti reppalanchuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Manassu nindi pongina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manassu nindi pongina"/>
</div>
<div class="lyrico-lyrics-wrapper">O neeti binduve kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O neeti binduve kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu vethukuthunna sampada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu vethukuthunna sampada"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkokka gnaapakaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkokka gnaapakaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhella aayuvundhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhella aayuvundhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkenni mundhu vecheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkenni mundhu vecheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanni vethukuthu padha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanni vethukuthu padha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manushyulandhu nee katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushyulandhu nee katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maharshi laaga saagadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maharshi laaga saagadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manushyulandhu nee katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushyulandhu nee katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maharshi laaga saagadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maharshi laaga saagadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhe katha idhe katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhe katha idhe katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muginpu lenidai sadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muginpu lenidai sadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhe katha idhe katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhe katha idhe katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Muginpu lenidai sadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muginpu lenidai sadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niswaartham entha goppadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niswaartham entha goppadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee padhamu rujuvu kattadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee padhamu rujuvu kattadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraalu laksha ompadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraalu laksha ompadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiraaksharalu raayada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiraaksharalu raayada"/>
</div>
<div class="lyrico-lyrics-wrapper">Niseedhi entha chinnadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niseedhi entha chinnadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kanti choopu cheppadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kanti choopu cheppadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee loni velugu panchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee loni velugu panchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishaala ningi chaaladhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishaala ningi chaaladhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manushyulandhu nee katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushyulandhu nee katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maharshi laaga saagadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maharshi laaga saagadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manushyulandhu nee katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushyulandhu nee katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maharshi laaga saagadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maharshi laaga saagadha"/>
</div>
</pre>
