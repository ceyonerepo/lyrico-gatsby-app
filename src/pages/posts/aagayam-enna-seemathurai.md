---
title: "aagayam enna song lyrics"
album: "Seemathurai"
artist: "Jose Franklin"
lyricist: "Veenai Mainthan"
director: "Santhosh Thiyagarajan"
path: "/albums/seemathurai-lyrics"
song: "Aagayam Enna"
image: ../../images/albumart/seemathurai.jpg
date: 2018-12-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/w-8Yzv4Egko"
type: "love"
singers:
  - Shreya Ghoshal
  - Swetha Mohan
  - KG Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aagayam enna paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagayam enna paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">niram maaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niram maaruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">athanaala anthi veyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athanaala anthi veyilu"/>
</div>
<div class="lyrico-lyrics-wrapper">karai yeruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karai yeruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nelavukku kaathirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nelavukku kaathirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">iravaanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravaanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nesama enna nesama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesama enna nesama "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu manam kekkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu manam kekkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">yen manasu ena paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen manasu ena paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">yelanama sirikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelanama sirikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">yen mugamum enake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen mugamum enake"/>
</div>
<div class="lyrico-lyrics-wrapper">than puthusa poche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than puthusa poche"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayavittu thayodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayavittu thayodum"/>
</div>
<div class="lyrico-lyrics-wrapper">pesa onnum thonalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesa onnum thonalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarukkum theriyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarukkum theriyama"/>
</div>
<div class="lyrico-lyrics-wrapper">sirikka pakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirikka pakkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">antha vanam puthusa than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha vanam puthusa than"/>
</div>
<div class="lyrico-lyrics-wrapper">intha boomi paakathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha boomi paakathu"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir saral theendama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir saral theendama"/>
</div>
<div class="lyrico-lyrics-wrapper">buvi yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="buvi yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">adi vandi thadamattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi vandi thadamattom"/>
</div>
<div class="lyrico-lyrics-wrapper">nenju kuda varum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenju kuda varum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">atha thandi poga than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha thandi poga than"/>
</div>
<div class="lyrico-lyrics-wrapper">gathi yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gathi yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannala pakum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala pakum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">kan pesume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan pesume"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanatha neram ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanatha neram ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaai pesume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaai pesume"/>
</div>
<div class="lyrico-lyrics-wrapper">paathangal patta idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathangal patta idam"/>
</div>
<div class="lyrico-lyrics-wrapper">poo pookume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo pookume"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna moodi enna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna moodi enna "/>
</div>
<div class="lyrico-lyrics-wrapper">pakka manam yengume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakka manam yengume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paruvam thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruvam thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">muthalam vetkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthalam vetkam"/>
</div>
<div class="lyrico-lyrics-wrapper">maravaathu vazhnaalellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maravaathu vazhnaalellam"/>
</div>
<div class="lyrico-lyrics-wrapper">paruvangalin naduvala than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruvangalin naduvala than"/>
</div>
<div class="lyrico-lyrics-wrapper">una thottu potta iduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="una thottu potta iduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu kala vellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kala vellam"/>
</div>
<div class="lyrico-lyrics-wrapper">thekki vacha paaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thekki vacha paaram"/>
</div>
<div class="lyrico-lyrics-wrapper">itha maathi vaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itha maathi vaika"/>
</div>
<div class="lyrico-lyrics-wrapper">yarukku than koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarukku than koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">adi yarumilla boomiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi yarumilla boomiya"/>
</div>
<div class="lyrico-lyrics-wrapper">than nenachu paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than nenachu paru"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir thondri vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir thondri vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">karanatha kooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karanatha kooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thera poda enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thera poda enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">theriya villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriya villa"/>
</div>
<div class="lyrico-lyrics-wrapper">uravana unna thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravana unna thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">maruthani poosama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maruthani poosama"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannam sevakkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannam sevakkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ena mathi ponathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena mathi ponathe "/>
</div>
<div class="lyrico-lyrics-wrapper">un nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nesam"/>
</div>
<div class="lyrico-lyrics-wrapper">raa kozhi pola than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raa kozhi pola than"/>
</div>
<div class="lyrico-lyrics-wrapper">naa muzhichu kedakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa muzhichu kedakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">ada vidiyum pothellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada vidiyum pothellam"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniyaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">sariyana jodiyinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sariyana jodiyinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">oore pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">namakkaga senthu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namakkaga senthu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhthum koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhthum koora"/>
</div>
<div class="lyrico-lyrics-wrapper">kanatha kaatchi ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanatha kaatchi ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">nesama poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesama poga"/>
</div>
<div class="lyrico-lyrics-wrapper">vera enna ketka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera enna ketka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">saami kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saami kitta"/>
</div>
</pre>
