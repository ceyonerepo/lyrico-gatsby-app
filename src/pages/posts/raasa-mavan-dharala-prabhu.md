---
title: 'raasa mavan song lyrics'
album: 'Dharala Prabhu'
artist: 'Madley Blues'
lyricist: 'Super Subu'
director: 'Krishna Marimuthu'
path: '/albums/dharala-prabhu-song-lyrics'
song: 'Raasa Mavan'
image: ../../images/albumart/dharala-prabhu.jpg
date: 2020-03-13
lang: tamil
singers: 
- Alexander Babu
- Harish Venkat
youtubeLink: 'https://www.youtube.com/embed/AIkqqYKoiyo'
type: 'happy'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Mama mama maattikkitaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama mama maattikkitaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Raasa raasa raasa mavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Raasa raasa raasa mavan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Mama mama mama mama maattikkitaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama mama mama mama maattikkitaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Raasa raasa raasa mavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Raasa raasa raasa mavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mama mama mama mama maattikkitaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama mama mama mama maattikkitaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Raasa mavan raasa mavan raasa mavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Raasa mavan raasa mavan raasa mavan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Mama mama maattikkitaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama mama maattikkitaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Raasa mavan raasa mavan raasa mavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Raasa mavan raasa mavan raasa mavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ivandhaan manmadhano
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivandhaan manmadhano"/>
</div>
<div class="lyrico-lyrics-wrapper">Kora theerka porandhae vandhavano
<input type="checkbox" class="lyrico-select-lyric-line" value="Kora theerka porandhae vandhavano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaniya nindhavano thalaraadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniya nindhavano thalaraadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangaa king ivano
<input type="checkbox" class="lyrico-select-lyric-line" value="Adangaa king ivano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ivana paathadhumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivana paathadhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bell adichu bulb-um erinjiruchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Bell adichu bulb-um erinjiruchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaathula mudi asaiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaathula mudi asaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaya raasaa paattum ketturuchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ilaya raasaa paattum ketturuchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idhuvum oru vidha
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhuvum oru vidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Loves-u dhaanae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Loves-u dhaanae…."/>
</div>
  <div class="lyrico-lyrics-wrapper">Mama mama mama maattikkitaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama mama mama maattikkitaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ok sonna mavusu dhaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ok sonna mavusu dhaanae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Raasa mavan raasa mavan ((<div class="lyrico-lyrics-wrapper">2) times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Raasa mavan raasa mavan ((<div class="lyrico-lyrics-wrapper">2) times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ivandhaan manmadhano
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivandhaan manmadhano"/>
</div>
<div class="lyrico-lyrics-wrapper">Kora theerka porandhae vandhavano
<input type="checkbox" class="lyrico-select-lyric-line" value="Kora theerka porandhae vandhavano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mama mama maattikkitaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama mama maattikkitaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Raasa raasa raasa mavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Raasa raasa raasa mavan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Mama mama mama mama maattikkitaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama mama mama mama maattikkitaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Raasa raasa raasa mavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Raasa raasa raasa mavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mama mama mama mama maattikkitaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama mama mama mama maattikkitaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Raasa mavan raasa mavan raasa mavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Raasa mavan raasa mavan raasa mavan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Mama mama maattikkitaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama mama maattikkitaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Raasa mavan raasa mavan raasa mavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Raasa mavan raasa mavan raasa mavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaayila vada suttae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaayila vada suttae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandiyila vandhavaasi vandhaachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandiyila vandhavaasi vandhaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaayila vada suttae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaayila vada suttae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandiyila vandhavaasi vandhaachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandiyila vandhavaasi vandhaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Petrol illaama nikkaiyila
<input type="checkbox" class="lyrico-select-lyric-line" value="Petrol illaama nikkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Bunk-eh kadachaachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Bunk-eh kadachaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Super man-u sikkuvaanaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Super man-u sikkuvaanaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Sik sik sik sikkuvaanaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Sik sik sik sikkuvaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettakaaran vitturuvaana…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vettakaaran vitturuvaana…"/>
</div>
<div class="lyrico-lyrics-wrapper">Vitturuvaana vitturuvaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Vitturuvaana vitturuvaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Malaya katta mudiyirukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Malaya katta mudiyirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaya virichaa kadal onakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Valaya virichaa kadal onakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ivandhaan manmadhano
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivandhaan manmadhano"/>
</div>
<div class="lyrico-lyrics-wrapper">Kora theerka porandhae vandhavano
<input type="checkbox" class="lyrico-select-lyric-line" value="Kora theerka porandhae vandhavano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaniya nindhavano thalaraadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniya nindhavano thalaraadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangaa king ivano
<input type="checkbox" class="lyrico-select-lyric-line" value="Adangaa king ivano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ivana paathadhumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivana paathadhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bell adichu bulb-um erinjiruchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Bell adichu bulb-um erinjiruchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaathula mudi asaiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaathula mudi asaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaya raasaa paattum ketturuchae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ilaya raasaa paattum ketturuchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idhuvum oru vidha
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhuvum oru vidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Loves-u dhaanae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Loves-u dhaanae…."/>
</div>
  <div class="lyrico-lyrics-wrapper">Mama mama mama maattikkitaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama mama mama maattikkitaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ok sonna mavusu dhaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ok sonna mavusu dhaanae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Raasa mavan raasa mavan ((<div class="lyrico-lyrics-wrapper">2) times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Raasa mavan raasa mavan ((<div class="lyrico-lyrics-wrapper">2) times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mama mama maattikkitaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama mama maattikkitaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Raasa raasa raasa mavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Raasa raasa raasa mavan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Mama mama mama mama maattikkitaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama mama mama mama maattikkitaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Raasa raasa raasa mavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Raasa raasa raasa mavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mama mama mama mama maattikkitaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama mama mama mama maattikkitaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Raasa mavan raasa mavan raasa mavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Raasa mavan raasa mavan raasa mavan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Mama mama maattikkitaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mama mama maattikkitaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Raasa mavan raasa mavan raasa mavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Raasa mavan raasa mavan raasa mavan"/>
</div>
</pre>