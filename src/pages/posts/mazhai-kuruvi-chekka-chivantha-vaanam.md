---
title: "mazhai kuruvi song lyrics"
album: "Chekka Chivantha Vaanam"
artist: "AR Rahman"
lyricist: "Vairamuthu"
director: "Mani Ratnam"
path: "/albums/chekka-chivantha-vaanam-lyrics"
song: "Mazhai Kuruvi"
image: ../../images/albumart/chekka-chivantha-vaanam.jpg
date: 2018-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oNpVIgJrKo8"
type: "happy"
singers:
  - AR Rahman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neela mazhai saaral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela mazhai saaral"/>
</div>
<div class="lyrico-lyrics-wrapper">Thendral nesavu nadathum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral nesavu nadathum idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela mazhai saaral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela mazhai saaral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam kunivathilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam kunivathilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannai thoduvathilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai thoduvathilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal arinthurunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal arinthurunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanam urainthuvarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanam urainthuvarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mouna thiruveliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mouna thiruveliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru gnyaanam valarthurinthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru gnyaanam valarthurinthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam erithirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam erithirunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan iyarkayil thilaithirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan iyarkayil thilaithirunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chittu kuruvi ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu kuruvi ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sneha paarvai kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sneha paarvai kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vatta paaraiyin mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta paaraiyin mel"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai vaa vaa endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vaa vaa endrathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kichu kich endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichu kich endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vaa endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vaa endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechu ethumindri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechu ethumindri"/>
</div>
<div class="lyrico-lyrics-wrapper">Priyamaa endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyamaa endrathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kichu kich endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichu kich endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vaa endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vaa endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechu ethumindri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechu ethumindri"/>
</div>
<div class="lyrico-lyrics-wrapper">Priyamaa endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyamaa endrathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otrai siru kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai siru kuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadathum ooranga nadagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadathum ooranga nadagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Satrae thilaithurunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satrae thilaithurunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kichu kich endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichu kich endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vaa endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vaa endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechu ethumindri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechu ethumindri"/>
</div>
<div class="lyrico-lyrics-wrapper">Priyamaa endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyamaa endrathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru naal kanavil ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal kanavil ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peratti per uravo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peratti per uravo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar varavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar varavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kan thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kan thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanthidum kaattro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanthidum kaattro"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai kanavil naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai kanavil naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkkum paatto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkkum paatto"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu uravo illai parivoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu uravo illai parivoooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee la mazhai saaral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee la mazhai saaral"/>
</div>
<div class="lyrico-lyrics-wrapper">Naananana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naananana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nananana na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nananana na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagai asaithapadi paranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagai asaithapadi paranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagaayam koththiyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam koththiyathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai uthari vittu sattrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai uthari vittu sattrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarae paranthathuvae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarae paranthathuvae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kichu kich endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichu kich endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vaa endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vaa endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechu ethumindri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechu ethumindri"/>
</div>
<div class="lyrico-lyrics-wrapper">Priyamaa endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyamaa endrathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kichu kich endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichu kich endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vaa endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vaa endrathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechu ethumindri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechu ethumindri"/>
</div>
<div class="lyrico-lyrics-wrapper">Priyamaa endrathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyamaa endrathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugilil sara sara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugilil sara sara"/>
</div>
<div class="lyrico-lyrics-wrapper">Saravendru kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saravendru kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi vanthu pada pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi vanthu pada pada"/>
</div>
<div class="lyrico-lyrics-wrapper">Padavendru veezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padavendru veezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai vanthu sada sada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai vanthu sada sada"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadavendru sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadavendru sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Adai mazhai kaattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adai mazhai kaattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudayillai mooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudayillai mooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanaveli mannil nazhuvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaveli mannil nazhuvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunthathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthathenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisaiyellam mazhaiyil karainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisaiyellam mazhaiyil karainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholainthathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholainthathenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chittu chirukuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu chirukuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Parantha thisaiyum theriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parantha thisaiyum theriyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu pirinthuvitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu pirinthuvitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirintha vethanai sumanthirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirintha vethanai sumanthirunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittu pirinthen pirinthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu pirinthen pirinthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir nanainthen nanainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir nanainthen nanainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha siru kuruvi ippothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha siru kuruvi ippothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alainthu thuyarpadumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alainthu thuyarpadumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyarpadumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyarpadumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha mazhai sumanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha mazhai sumanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athan rekkai vazhithidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athan rekkai vazhithidumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhithidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhithidumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril anneram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril anneram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaiyae veru kathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaiyae veru kathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottai maranthuvittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottai maranthuvittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruvi kummiyadithathu kaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruvi kummiyadithathu kaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sottum mazhai sinthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottum mazhai sinthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha sugathil nanaiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha sugathil nanaiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ettu ponavanai enni enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ettu ponavanai enni enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhuthathu kaan azhuthathu kaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuthathu kaan azhuthathu kaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril anneram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril anneram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaiyae veru kathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathaiyae veru kathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottai maranthuvittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottai maranthuvittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruvi kummiyadithathu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruvi kummiyadithathu thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sottum mazhai sinthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottum mazhai sinthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha sugathil nanaiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha sugathil nanaiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ettu ponavanai enni enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ettu ponavanai enni enni"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhuthathu kaan azhuthathu kaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuthathu kaan azhuthathu kaan"/>
</div>
</pre>
