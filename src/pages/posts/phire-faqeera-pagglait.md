---
title: "phire faqeera song lyrics"
album: "Pagglait"
artist: "Arijit Singh"
lyricist: "Neelesh Misra"
director: "Umesh Bist"
path: "/albums/pagglait-lyrics"
song: "Phire Faqeera"
image: ../../images/albumart/pagglait.jpg
date: 2021-03-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/N--SB72jLFY"
type: "Motivational"
singers:
  - Amrita Singh
  - Raja Kumari
  - Arijit Singh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mann hai kalandar, mann hai jogi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann hai kalandar, mann hai jogi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann jo chahe, mann ki hogi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann jo chahe, mann ki hogi"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir kyun mann ne jhooth ko pooja?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir kyun mann ne jhooth ko pooja?"/>
</div>
<div class="lyrico-lyrics-wrapper">Dukh takleefein saari bhogi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dukh takleefein saari bhogi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabhi sadiyaan, kabhi lamha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi sadiyaan, kabhi lamha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabhi mushqil, kabhi aasaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi mushqil, kabhi aasaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabhi roothi, kabhi jhooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi roothi, kabhi jhooti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabhi poori, kabhi tooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi poori, kabhi tooti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabhi jugnu, kabhi tara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi jugnu, kabhi tara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabhi kam kam, kabhi saara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi kam kam, kabhi saara"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai bole zingdagi meetha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai bole zingdagi meetha"/>
</div>
<div class="lyrico-lyrics-wrapper">Par iska swaad hai khara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par iska swaad hai khara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratti ratti masha masha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratti ratti masha masha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tinka tinka tooti aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tinka tinka tooti aasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhole hai hum, samajh na paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhole hai hum, samajh na paaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Duniya ka yeh khel tamasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duniya ka yeh khel tamasha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratti ratti masha masha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratti ratti masha masha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tinka tinka tooti aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tinka tinka tooti aasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhole hai hum, samajh na paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhole hai hum, samajh na paaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Duniya ka yeh khel tamasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duniya ka yeh khel tamasha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagal paagal pagal paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal paagal pagal paagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal, phire faqeera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal, phire faqeera"/>
</div>
<div class="lyrico-lyrics-wrapper">Rait mein dekho dhoondhe heera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rait mein dekho dhoondhe heera"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal paagal pagal paagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal paagal pagal paagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal pagal, phire faqeera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal pagal, phire faqeera"/>
</div>
<div class="lyrico-lyrics-wrapper">Jogi koyi mantar jantar,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jogi koyi mantar jantar,"/>
</div>
<div class="lyrico-lyrics-wrapper">Jogi koyi mantar jantar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jogi koyi mantar jantar"/>
</div>
<div class="lyrico-lyrics-wrapper">Phoonke toh kam hove peera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phoonke toh kam hove peera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagal pagal pagal pagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal pagal pagal pagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal pagal phire faqeera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal pagal phire faqeera"/>
</div>
<div class="lyrico-lyrics-wrapper">Aansu chandi, dard hai sona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aansu chandi, dard hai sona"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoop ka tukda, yaad ka kona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoop ka tukda, yaad ka kona"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal pagal pagal pagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal pagal pagal pagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal pagal phire faqeera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal pagal phire faqeera"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann kathputali, mann hai khilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann kathputali, mann hai khilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Le phirta rishton ka bichona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le phirta rishton ka bichona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aankhon main yeh jhaank raha hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon main yeh jhaank raha hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann ka gadariyaan haankh raha hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann ka gadariyaan haankh raha hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Amrit becha karta tha yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amrit becha karta tha yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Zahar ki pudiyaan faankh raha hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zahar ki pudiyaan faankh raha hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phenkh mukhauta, chehara dekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phenkh mukhauta, chehara dekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhes hai badala dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhes hai badala dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Toote lafzon ko joda hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toote lafzon ko joda hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavita kar lega kabira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavita kar lega kabira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phire faqeera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phire faqeera"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal phire faqeera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal phire faqeera"/>
</div>
<div class="lyrico-lyrics-wrapper">Rait mein dekho dhoondhe heera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rait mein dekho dhoondhe heera"/>
</div>
<div class="lyrico-lyrics-wrapper">Jogi koi mantar jantar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jogi koi mantar jantar"/>
</div>
<div class="lyrico-lyrics-wrapper">Phoonke toh kam hove peera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phoonke toh kam hove peera"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main rangrez, kumhaar bhi main hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main rangrez, kumhaar bhi main hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Nafrat main hun pyaar bhi main hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nafrat main hun pyaar bhi main hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhmein saara satya basa hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhmein saara satya basa hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann hun, main sansaar bhi hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann hun, main sansaar bhi hun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Punarjanma ki rasma karenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punarjanma ki rasma karenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Rooh apni bhasma karenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rooh apni bhasma karenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Marghat mein bhi phool khilenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marghat mein bhi phool khilenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Aisa koyi tilasma karenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisa koyi tilasma karenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratti ratti masha masha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratti ratti masha masha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tinka tinka tooti aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tinka tinka tooti aasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhole hai hum, samajh na paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhole hai hum, samajh na paaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Duniya ka yeh khel tamasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duniya ka yeh khel tamasha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratti ratti masha masha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratti ratti masha masha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tinka tinka tooti aasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tinka tinka tooti aasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhole hai hum, samajh na paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhole hai hum, samajh na paaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Duniya ka yeh khel tamasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duniya ka yeh khel tamasha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagal pagal pagal pagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal pagal pagal pagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal, phire faqeera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal, phire faqeera"/>
</div>
<div class="lyrico-lyrics-wrapper">Rait mein dekho dhoondhe heera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rait mein dekho dhoondhe heera"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal pagal pagal pagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal pagal pagal pagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal pagal, phire faqeera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal pagal, phire faqeera"/>
</div>
<div class="lyrico-lyrics-wrapper">Jogi koyi mantar jantar,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jogi koyi mantar jantar,"/>
</div>
<div class="lyrico-lyrics-wrapper">Jogi koyi mantar jantar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jogi koyi mantar jantar"/>
</div>
<div class="lyrico-lyrics-wrapper">Phoonke toh kam hove peera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phoonke toh kam hove peera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagal pagal pagal pagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal pagal pagal pagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal pagal, phire faqeera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal pagal, phire faqeera"/>
</div>
<div class="lyrico-lyrics-wrapper">Aansu chandi, dard hai sona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aansu chandi, dard hai sona"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoop ka tukda, yaad ka kona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoop ka tukda, yaad ka kona"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal pagal pagal pagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal pagal pagal pagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal pagal, phire faqeera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal pagal, phire faqeera"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann kathputali, mann hai khilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann kathputali, mann hai khilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Le phirta rishton ka bichona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le phirta rishton ka bichona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khud ko toda, khud ko banaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud ko toda, khud ko banaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud ko ranga khud ko sajaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud ko ranga khud ko sajaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud ko sancha phod diya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud ko sancha phod diya hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud ko phir se jod liya hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud ko phir se jod liya hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phenkh mukhauta, chehara dekha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phenkh mukhauta, chehara dekha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhes hai badala dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhes hai badala dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Toote lafzon ko joda hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toote lafzon ko joda hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavita kar lega kabira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavita kar lega kabira"/>
</div>
<div class="lyrico-lyrics-wrapper">Phire faqeera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phire faqeera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagal phire faqeera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal phire faqeera"/>
</div>
<div class="lyrico-lyrics-wrapper">Rait mein dekho dhoondhe heera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rait mein dekho dhoondhe heera"/>
</div>
<div class="lyrico-lyrics-wrapper">Jogi koyi mantar jantar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jogi koyi mantar jantar"/>
</div>
<div class="lyrico-lyrics-wrapper">Phoonke toh kam hove peera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phoonke toh kam hove peera"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal, pagal!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal, pagal!"/>
</div>
</pre>
