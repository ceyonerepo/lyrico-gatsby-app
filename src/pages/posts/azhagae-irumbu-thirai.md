---
title: "azhagae song lyrics"
album: "Irumbu Thirai"
artist: "Yuvan Shankar Raja"
lyricist: "Vivek"
director: "P.S. Mithran"
path: "/albums/irumbu-thirai-song-lyrics"
song: "Azhagae"
image: ../../images/albumart/irumbu-thirai.jpg
date: 2018-05-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ut8FGRsCZI8"
type: "love"
singers:
  - Arun Kamath
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhigiraai arugae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhigiraai arugae"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgalil siragae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgalil siragae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaithu ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaithu ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaatril aadinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaatril aadinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Olivizhum mezhugae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olivizhum mezhugae"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaiyil un iragae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaiyil un iragae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudi ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudi ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmoodi kaadhal naan aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodi kaadhal naan aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee veesidum siru moochai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee veesidum siru moochai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulvaanginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulvaanginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Malar aanen un madi veeznhdhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar aanen un madi veeznhdhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yendhum anbil vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yendhum anbil vaazhgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhigiraai arugae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhigiraai arugae"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgalil siragae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgalil siragae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaithu ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaithu ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaatril aadinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaatril aadinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yayee yai yaye yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yayee yai yaye yaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yayee yai yaye yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yayee yai yaye yaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yayee yai yaye yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yayee yai yaye yaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yayaee yayaee yaya ya ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yayaee yayaee yaya ya ya ya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoosippikul ottikollum muthu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoosippikul ottikollum muthu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittukul ottikollum anbu paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittukul ottikollum anbu paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyilil veezhndhu vitta thuli pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyilil veezhndhu vitta thuli pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kadai vizhi kanalil kaaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kadai vizhi kanalil kaaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinda thindaadi veenaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinda thindaadi veenaven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kondaadi thaenaaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kondaadi thaenaaven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna kannaadi naanaaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna kannaadi naanaaven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nil en munnaadi nee yaaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nil en munnaadi nee yaaven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhigiraai arugae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhigiraai arugae"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgalil siragae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgalil siragae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaithu ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaithu ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaatril aadinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaatril aadinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Olivizhum mezhugae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olivizhum mezhugae"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaiyil un iragae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaiyil un iragae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudi ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudi ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmoodi kaadhal naan aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmoodi kaadhal naan aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee veesidum siru moochai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee veesidum siru moochai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulvaanginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulvaanginen"/>
</div>
<div class="lyrico-lyrics-wrapper">Malar aanen un madi veeznhdhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar aanen un madi veeznhdhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yendhum anbil vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yendhum anbil vaazhgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhigiraai arugae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhigiraai arugae"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgalil siragae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgalil siragae"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaithu ponaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaithu ponaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaatril aadinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaatril aadinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yayee yai yaye yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yayee yai yaye yaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yayee yai yaye yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yayee yai yaye yaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yayee yai yaye yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yayee yai yaye yaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yayaee yayaee yaya ya ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yayaee yayaee yaya ya ya ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yayaee yayaee yaya ya ya ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yayaee yayaee yaya ya ya ya"/>
</div>
</pre>
