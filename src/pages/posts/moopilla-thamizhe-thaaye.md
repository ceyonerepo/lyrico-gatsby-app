---
title: "Moopilla Thamizhe Thaaye"
album: "Majja"
artist: "AR Rahman"
lyricist: "Thamarai"
director: "Amith Krishnan"
path: "/albums/moopilla-thamizhe-thaaye-song-lyrics"
song: "Enjoy Enjaami"
image: ../../images/albumart/moopilla-thamizhe-thaaye.jpg
date: 2022-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JDYiJGTOFHU"
type: "album"
singers:
  - A. R. Rahman
  - Saindhavi Prakash
  - Khatija Rahman
  - A. R. Ameen
  - Amina Rafiq
  - Gabriella Sellus
  - Poovaiyar
  - Rakshita Suresh
  - Niranjana Ramanan
  - Aparna Harikumar
  - Nakul Abhyankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">puyal thaandiye vidiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyal thaandiye vidiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhu vaanil vidiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu vaanil vidiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">poobalame vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poobalame vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thamizhe vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhe vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dharaniyana thamizhe vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharaniyana thamizhe vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vizhinthom munnam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhinthom munnam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">yezhunthom eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yezhunthom eppothum"/>
</div>
<div class="lyrico-lyrics-wrapper">pirinthom munnam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirinthom munnam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">inainthom eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inainthom eppothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dhisaiyettum thamizhe yettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhisaiyettum thamizhe yettum"/>
</div>
<div class="lyrico-lyrics-wrapper">thittithom murasum kottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittithom murasum kottum"/>
</div>
<div class="lyrico-lyrics-wrapper">madhi natupum vaanai muttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhi natupum vaanai muttum"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai muththai kadalil sottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai muththai kadalil sottum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dhisaiyettum thamizhe yettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhisaiyettum thamizhe yettum"/>
</div>
<div class="lyrico-lyrics-wrapper">thittithom murasum kottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thittithom murasum kottum"/>
</div>
<div class="lyrico-lyrics-wrapper">madhi natupum vaanai muttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhi natupum vaanai muttum"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai muththai kadalil sottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai muththai kadalil sottum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">agam endral anbai konjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="agam endral anbai konjum"/>
</div>
<div class="lyrico-lyrics-wrapper">puram endral porai pongum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puram endral porai pongum"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaiyindri kaatril engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaiyindri kaatril engum"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaiyindri kaatril engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaiyindri kaatril engum"/>
</div>
<div class="lyrico-lyrics-wrapper">thamizhendru sange muzhangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhendru sange muzhangum"/>
</div>
<div class="lyrico-lyrics-wrapper">thadiyindri kaatril engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadiyindri kaatril engum"/>
</div>
<div class="lyrico-lyrics-wrapper">thamizhendru sange muzhangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhendru sange muzhangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">urangatha pillaikellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urangatha pillaikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalatti thamizhe karaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalatti thamizhe karaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">pasiyendru yaarum vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasiyendru yaarum vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">pagagi amutham pozhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagagi amutham pozhiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kodai vallal ezhuvar vandhar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodai vallal ezhuvar vandhar"/>
</div>
<div class="lyrico-lyrics-wrapper">kodai endral uyirm thandhar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodai endral uyirm thandhar"/>
</div>
<div class="lyrico-lyrics-wrapper">padai kondu pagaivar vandhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padai kondu pagaivar vandhal"/>
</div>
<div class="lyrico-lyrics-wrapper">pala paadam katru sendrar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala paadam katru sendrar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">moovendar sabaiyil nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moovendar sabaiyil nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">muthamizhin pulavar vendrar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthamizhin pulavar vendrar"/>
</div>
<div class="lyrico-lyrics-wrapper">paavendhar endre kandal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavendhar endre kandal"/>
</div>
<div class="lyrico-lyrics-wrapper">paaralam mannar paninthar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaralam mannar paninthar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annaikkum annai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaikkum annai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">adivaanil udhayam neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adivaanil udhayam neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">munnaikkum munnai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnaikkum munnai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">moopilla thamizhe thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moopilla thamizhe thaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annaikkum annai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaikkum annai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">adivaanil udhayam neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adivaanil udhayam neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">munnaikkum munnai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnaikkum munnai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">moopilla thamizhe thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moopilla thamizhe thaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">udhirnthom munnam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udhirnthom munnam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">malarnthom eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malarnthom eppothum"/>
</div>
<div class="lyrico-lyrics-wrapper">kidanthom munnam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidanthom munnam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">kilaithom eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilaithom eppothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaninthom munnam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaninthom munnam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">erinthom eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erinthom eppothum"/>
</div>
<div class="lyrico-lyrics-wrapper">tholainthom munnam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholainthom munnam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">pinainthom eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinainthom eppothum"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhunthom munnam naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhunthom munnam naam"/>
</div>
<div class="lyrico-lyrics-wrapper">ezhunthom eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ezhunthom eppothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annaikkum annai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaikkum annai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">adivaanil udhayam neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adivaanil udhayam neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">munnaikkum munnai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnaikkum munnai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">moopilla thamizhe thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moopilla thamizhe thaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thamizhenral moovagai endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhenral moovagai endre"/>
</div>
<div class="lyrico-lyrics-wrapper">andandai arinthom andru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andandai arinthom andru"/>
</div>
<div class="lyrico-lyrics-wrapper">iyazh nadagam isaiyum sernthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyazh nadagam isaiyum sernthal"/>
</div>
<div class="lyrico-lyrics-wrapper">manam kollai kollum endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam kollai kollum endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalangal pogum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangal pogum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">mozhi sernthu munnal ponal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhi sernthu munnal ponal"/>
</div>
<div class="lyrico-lyrics-wrapper">azhivindri thodarum endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhivindri thodarum endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">amudhagi pozhiyum engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amudhagi pozhiyum engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vinyana thamizhai ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinyana thamizhai ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">vanigathin thamizhai ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanigathin thamizhai ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">iniyathin noolai kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iniyathin noolai kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">inaiyum thamizh ulagai pandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inaiyum thamizh ulagai pandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mai achchil munne vandhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mai achchil munne vandhom"/>
</div>
<div class="lyrico-lyrics-wrapper">tattachil thaniye nindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tattachil thaniye nindrom"/>
</div>
<div class="lyrico-lyrics-wrapper">kaninikkul porunthi kondom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaninikkul porunthi kondom"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaiketra maari kolvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaiketra maari kolvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnipai gavanam kondom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnipai gavanam kondom"/>
</div>
<div class="lyrico-lyrics-wrapper">ul vaangi maari selvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ul vaangi maari selvom"/>
</div>
<div class="lyrico-lyrics-wrapper">pin vaangum peche illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pin vaangum peche illai"/>
</div>
<div class="lyrico-lyrics-wrapper">munnokki sendre velvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnokki sendre velvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pudhu nutpam nenje ethuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu nutpam nenje ethuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaal vaikkum munne thamizhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaal vaikkum munne thamizhum"/>
</div>
<div class="lyrico-lyrics-wrapper">ayudham kollum azhagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayudham kollum azhagai"/>
</div>
<div class="lyrico-lyrics-wrapper">aadaigal aniyum pudhithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadaigal aniyum pudhithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">engengum sodai poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engengum sodai poga"/>
</div>
<div class="lyrico-lyrics-wrapper">en arumai thamizhe vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en arumai thamizhe vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">varungala pillaigal vaazhvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varungala pillaigal vaazhvil"/>
</div>
<div class="lyrico-lyrics-wrapper">valam ponga vaa vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valam ponga vaa vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annaikkum annai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaikkum annai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">adivaanil udhayam neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adivaanil udhayam neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">munnaikkum munnai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnaikkum munnai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">moopilla thamizhe thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moopilla thamizhe thaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">palangala perumai pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palangala perumai pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">padithanda vannam poosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padithanda vannam poosi"/>
</div>
<div class="lyrico-lyrics-wrapper">sirai vaikka paarpaar thamizhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirai vaikka paarpaar thamizhe"/>
</div>
<div class="lyrico-lyrics-wrapper">nee seeri vaa vaa veliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee seeri vaa vaa veliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vai sollil veerar ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vai sollil veerar ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">vadikaata paduvar veettil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadikaata paduvar veettil"/>
</div>
<div class="lyrico-lyrics-wrapper">sollukkul siranthathu endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollukkul siranthathu endral"/>
</div>
<div class="lyrico-lyrics-wrapper">seyal endre sol sol sol sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seyal endre sol sol sol sol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sendruduvom ettuthikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sendruduvom ettuthikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ayal naatu palgalai pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayal naatu palgalai pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">iru kangai thamizhukku amaippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru kangai thamizhukku amaippom"/>
</div>
<div class="lyrico-lyrics-wrapper">oor koodi therai izhuppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor koodi therai izhuppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mozhiyillai endral inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhiyillai endral inge"/>
</div>
<div class="lyrico-lyrics-wrapper">idamillai endre arivai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idamillai endre arivai"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhuthukol thamizha munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhuthukol thamizha munne"/>
</div>
<div class="lyrico-lyrics-wrapper">pinathu kol thamizhal unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinathu kol thamizhal unnai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thamizhengal uyire endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhengal uyire endru"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinathorum solvom nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinathorum solvom nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">unaiyendri yaarai kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaiyendri yaarai kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">uyarvoma ulagil indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyarvoma ulagil indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annaikkum annai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaikkum annai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">adivaanil udhayam neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adivaanil udhayam neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">munnaikkum munnai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnaikkum munnai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">moopilla thamizhe thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moopilla thamizhe thaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annaikkum annai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaikkum annai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">adivaanil udhayam neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adivaanil udhayam neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">munnaikkum munnai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnaikkum munnai neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">moopilla thamizhe thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moopilla thamizhe thaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puyal thandiye vidiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyal thandiye vidiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhu vaanil vidiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu vaanil vidiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">poobalame vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poobalame vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thamizhe vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhe vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dharaniyana thamizhe vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharaniyana thamizhe vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thamizhe vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhe vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dharaniyana thamizhe vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dharaniyana thamizhe vaa"/>
</div>
</pre>
