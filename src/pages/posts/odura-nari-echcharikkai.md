---
title: "odura nari song lyrics"
album: "Echcharikkai"
artist: "Sundaramurthy KS"
lyricist: "GKB"
director: "Sarjun KM"
path: "/albums/echcharikkai-lyrics"
song: "Odura Nari"
image: ../../images/albumart/echcharikkai.jpg
date: 2018-08-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vMn4HewP0to"
type: "mass"
singers:
  - Sri Radha Bharath
  - Vijaynarain
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mei athu poiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei athu poiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiyaaga vaalattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyaaga vaalattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi athu thee pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi athu thee pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiyodu serattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyodu serattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee athu poiyaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee athu poiyaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiyaaga maarattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyaaga maarattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha oru irulukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha oru irulukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyalgal pirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalgal pirakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalai keelai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai keelai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirakkum maanidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirakkum maanidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai kanam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai kanam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum yenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum yenadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu thiriyum poigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu thiriyum poigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum oru thugaladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum oru thugaladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odura nariyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odura nariyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nari kulla nari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nari kulla nari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulla nari mudiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulla nari mudiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mudi vella mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mudi vella mudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odura nariyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odura nariyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nari kulla nari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nari kulla nari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulla nari mudiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulla nari mudiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mudi vella mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mudi vella mudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odura nariyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odura nariyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nari kulla nari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nari kulla nari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulla nari mudiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulla nari mudiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mudi vella mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mudi vella mudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odura nariyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odura nariyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nari kulla nari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nari kulla nari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulla nari mudiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulla nari mudiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mudi vella mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mudi vella mudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velichangal viraigaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichangal viraigaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Natchathiram thoovalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natchathiram thoovalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravugal velipada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravugal velipada"/>
</div>
<div class="lyrico-lyrics-wrapper">Minminigal sitharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minminigal sitharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkena pirakkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena pirakkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru nodiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru nodiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vittu unnai vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vittu unnai vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalathil karaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalathil karaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porkalamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porkalamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagarum naatkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarum naatkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthu pogutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthu pogutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnerum paathaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnerum paathaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum thadaikalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum thadaikalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaandadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaandadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odura nariyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odura nariyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nari kulla nari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nari kulla nari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulla nari mudiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulla nari mudiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mudi vella mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mudi vella mudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odura nariyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odura nariyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nari kulla nari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nari kulla nari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulla nari mudiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulla nari mudiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mudi vella mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mudi vella mudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odura nariyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odura nariyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nari kulla nari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nari kulla nari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulla nari mudiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulla nari mudiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mudi vella mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mudi vella mudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odura nariyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odura nariyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nari kulla nari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nari kulla nari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulla nari mudiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulla nari mudiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mudi vella mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mudi vella mudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mei athu poiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei athu poiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiyaaga vaalattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyaaga vaalattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi athu thee pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi athu thee pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiyodu serattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyodu serattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee athu poiyaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee athu poiyaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiyaaga maarattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyaaga maarattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha oru irulukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha oru irulukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyalgal pirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyalgal pirakkum"/>
</div>
</pre>
