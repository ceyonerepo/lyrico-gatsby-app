---
title: "kannum kannum pesa pesa song lyrics"
album: "Thalaivii"
artist: "G.V. Prakash Kumar"
lyricist: "Madhan Karky"
director: "A.L. Vijay"
path: "/albums/thalaivii-song-lyrics"
song: "Kannum Kannum Pesa Pesa"
image: ../../images/albumart/thalaivii.jpg
date: 2021-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2jTe9yeNpyE"
type: "happy"
singers:
  - Saindhavi Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dhina Dhina Dheera Dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Dhina Dheera Dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhina Dhina Dheera Dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Dhina Dheera Dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannai Aazhum Vantha Mannava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai Aazhum Vantha Mannava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhina Dhina Dheera Dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Dhina Dheera Dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhina Dhina Dheera Dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Dhina Dheera Dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Vaanil Konjam Minnava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Vaanil Konjam Minnava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Pesa Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Pesa Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Yeno Mounamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Yeno Mounamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Nindru Theenda Theenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Nindru Theenda Theenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal Ingu Oomaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal Ingu Oomaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Man Meedhu Vandha Vaanam Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man Meedhu Vandha Vaanam Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnum Neela Vannane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnum Neela Vannane"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaaranangal Koora Vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaaranangal Koora Vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai Sollu Kannane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Sollu Kannane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhina Dhina Dheera Dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Dhina Dheera Dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhina Dhina Dheera Dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Dhina Dheera Dheera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerile Vennila Pol 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerile Vennila Pol "/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Un Kannila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Un Kannila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vimbama Sondhama Thunbam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vimbama Sondhama Thunbam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thointha Inbama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thointha Inbama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnin Meengal Paarpadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnin Meengal Paarpadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanai Neelam Neenguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanai Neelam Neenguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Endru Sonnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Endru Sonnal"/>
</div>
<div class="lyrico-lyrics-wrapper">Radhai Nenjam Thaanguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radhai Nenjam Thaanguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Pesa Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Pesa Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Yeno Mounamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Yeno Mounamaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Man Meedhu Vandha Vaanam Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man Meedhu Vandha Vaanam Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnum Neela Vannane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnum Neela Vannane"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaaranangal Koora Vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaaranangal Koora Vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai Sollu Kannane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Sollu Kannane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhina Dhina Dheera Dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Dhina Dheera Dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhina Dhina Dheera Dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Dhina Dheera Dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannai Aazhum Vantha Mannava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai Aazhum Vantha Mannava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhina Dhina Dheera Dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Dhina Dheera Dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhina Dhina Dheera Dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhina Dhina Dheera Dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Vaanil Konjam Minnava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Vaanil Konjam Minnava"/>
</div>
</pre>
