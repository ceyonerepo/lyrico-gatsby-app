---
title: "charitraney likhinchara song lyrics"
album: "A1 Express"
artist: "Hiphop Tamizha"
lyricist: "Kittu Vissapragada"
director: "Dennis Jeevan Kanukolanu"
path: "/albums/a1-express-lyrics"
song: "charitraney Likhinchara - Cheekatine Cheelchukune"
image: ../../images/albumart/a1-express.jpg
date: 2021-03-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Nglop0DI2tA"
type: "happy"
singers:
  - Hema Chandran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Cheekatine cheelchukune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekatine cheelchukune"/>
</div>
<div class="lyrico-lyrics-wrapper">vekuva rekha idhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vekuva rekha idhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Geethalane dhaatukune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geethalane dhaatukune"/>
</div>
<div class="lyrico-lyrics-wrapper">daagina aata idhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daagina aata idhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelone modhalaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelone modhalaina"/>
</div>
<div class="lyrico-lyrics-wrapper">samaram idhi kaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samaram idhi kaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee valle mugisela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee valle mugisela"/>
</div>
<div class="lyrico-lyrics-wrapper">aduge padaraada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduge padaraada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaraathe maarche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaraathe maarche"/>
</div>
<div class="lyrico-lyrics-wrapper">daari idhe kadaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daari idhe kadaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee yuddhame neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee yuddhame neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">kada pada pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kada pada pada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gelavali emaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelavali emaina"/>
</div>
<div class="lyrico-lyrics-wrapper">charithraney likinchara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="charithraney likinchara"/>
</div>
<div class="lyrico-lyrics-wrapper">Pada pada gelavali emaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada pada gelavali emaina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee yuddhame neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee yuddhame neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">kada pada pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kada pada pada"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelavali emaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelavali emaina"/>
</div>
<div class="lyrico-lyrics-wrapper">charithraney likinchara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="charithraney likinchara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pada pada gelavali emaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada pada gelavali emaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelone modhalaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelone modhalaina"/>
</div>
<div class="lyrico-lyrics-wrapper">samaram idhi kaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samaram idhi kaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee valle mugisela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee valle mugisela"/>
</div>
<div class="lyrico-lyrics-wrapper">aduge padaraada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduge padaraada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee yuddhame neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee yuddhame neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">kada pada pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kada pada pada"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelavali emaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelavali emaina"/>
</div>
<div class="lyrico-lyrics-wrapper">charithraney likinchara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="charithraney likinchara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pada pada gelavali emaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada pada gelavali emaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelone modhalaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelone modhalaina"/>
</div>
<div class="lyrico-lyrics-wrapper">samaram idhi kaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samaram idhi kaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee valle mugisela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee valle mugisela"/>
</div>
<div class="lyrico-lyrics-wrapper">aduge padaraada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduge padaraada"/>
</div>
</pre>
