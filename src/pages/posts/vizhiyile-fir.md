---
title: "vizhiyile song lyrics"
album: "FIR"
artist: "Ashwath"
lyricist: "Bagavathy PK"
director: "Manu Anand"
path: "/albums/fir-lyrics"
song: "Vizhiyile"
image: ../../images/albumart/fir.jpg
date: 2022-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7p05ih6cxYg"
type: "happy"
singers:
  - Sathyaprakash
  - Mahita Mahesh
  - Suganth Shekar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Lika dedhe maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lika dedhe maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Lika dedhe maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lika dedhe maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Fanaa tujhe pe maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fanaa tujhe pe maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Like dedhe maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like dedhe maula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyilae virindhadhen vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyilae virindhadhen vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivilae paravaiyaagireno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivilae paravaiyaagireno"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragai virithadavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai virithadavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyaram…parandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram…parandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam virigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam virigiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai manadhinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai manadhinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil pizhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil pizhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai vayadhinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai vayadhinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil alai sottum isaiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil alai sottum isaiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Neendhi paarkkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendhi paarkkava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrai vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrai vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai varugayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai varugayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanai thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanai thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyin aruginil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyin aruginil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottrai nodi neendu virigaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottrai nodi neendu virigaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Neendhi theerkkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendhi theerkkava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaththi thaavi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaththi thaavi "/>
</div>
<div class="lyrico-lyrics-wrapper">theeraadha thimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeraadha thimira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagadhai siraiyida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagadhai siraiyida "/>
</div>
<div class="lyrico-lyrics-wrapper">thudithidum vayadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudithidum vayadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi koovi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi koovi "/>
</div>
<div class="lyrico-lyrics-wrapper">kondadu sadhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondadu sadhira"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru oru nodiyilum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru oru nodiyilum "/>
</div>
<div class="lyrico-lyrics-wrapper">piranthidu pudhidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piranthidu pudhidhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaththi thaavi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaththi thaavi "/>
</div>
<div class="lyrico-lyrics-wrapper">theeraadha thimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeraadha thimira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagadhai siraiyida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagadhai siraiyida "/>
</div>
<div class="lyrico-lyrics-wrapper">thudithidum vayadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudithidum vayadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi koovi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi koovi "/>
</div>
<div class="lyrico-lyrics-wrapper">kondadu sadhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondadu sadhira"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru oru nodiyilum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru oru nodiyilum "/>
</div>
<div class="lyrico-lyrics-wrapper">piranthidu pudhidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piranthidu pudhidhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa aa aa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa aa aa "/>
</div>
<div class="lyrico-lyrics-wrapper">Krishna nee beganae baaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna nee beganae baaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Krishna nee beganaekrishna aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna nee beganaekrishna aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattampoochi polaagi vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattampoochi polaagi vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Siluvaigal udaithidu siragugal urava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siluvaigal udaithidu siragugal urava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamoochi kannodu kanava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoochi kannodu kanava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaimayin ragasiyam vidai kondu varava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaimayin ragasiyam vidai kondu varava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa aaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lika dedhe maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lika dedhe maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Lika dedhe maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lika dedhe maula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Krishna beganae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna beganae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fanaa tujhe pe maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fanaa tujhe pe maula"/>
</div>
<div class="lyrico-lyrics-wrapper">Like dedhe maula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like dedhe maula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyaram Vizhiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram Vizhiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhen Virindhadhen en vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhen Virindhadhen en vaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Virigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virigirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravayaaagireno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravayaaagireno"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyilae virindhadhen en vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyilae virindhadhen en vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivilae paravaiyaagireno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivilae paravaiyaagireno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattampoochi polaagi vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattampoochi polaagi vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Siluvaigal udaithidu siragugal urava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siluvaigal udaithidu siragugal urava"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaththi thaavi theeraadha thimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaththi thaavi theeraadha thimira"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru oru nodiyilum piranthidu pudhidhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru oru nodiyilum piranthidu pudhidhaai"/>
</div>
</pre>
