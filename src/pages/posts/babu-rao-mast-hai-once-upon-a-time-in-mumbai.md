---
title: "babu rao mast hai song lyrics"
album: "Once Upon a Time in Mumbai"
artist: "Amitabh Bhattacharya"
lyricist: "Irshad Kamil"
director: "Milan Luthria"
path: "/albums/once-upon-a-time-in-mumbai-lyrics"
song: "Babu rao mast hai"
image: ../../images/albumart/once-upon-a-time-in-mumbai.jpg
date: 2010-07-30
lang: hindi
youtubeLink: "https://www.youtube.com/embed/UwkOleuVykw"
type: "happy"
singers:
  - Mika Singh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Akha ye shehar bada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akha ye shehar bada"/>
</div>
<div class="lyrico-lyrics-wrapper">Apni jaib mein pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni jaib mein pada"/>
</div>
<div class="lyrico-lyrics-wrapper">Khulla raaz hai bas kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khulla raaz hai bas kya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ungli ye trigger pe hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungli ye trigger pe hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Daring bhi jigar mein hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daring bhi jigar mein hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Apni thaath hai bas kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni thaath hai bas kya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O apna time hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O apna time hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aish cash bhi zabardast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aish cash bhi zabardast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Underground har cheez ka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Underground har cheez ka "/>
</div>
<div class="lyrico-lyrics-wrapper">Yahan bandobast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahan bandobast hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baburao mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baburao mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mast hai mast mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mast hai mast mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Baburao mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baburao mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mast hai mast mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mast hai mast mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Baburao mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baburao mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mast hai mast mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mast hai mast mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Baburao mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baburao mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mast hai mast mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mast hai mast mast hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O kal tak toh chaar sikke hi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O kal tak toh chaar sikke hi "/>
</div>
<div class="lyrico-lyrics-wrapper">Jeb mein khankhana rahe the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeb mein khankhana rahe the"/>
</div>
<div class="lyrico-lyrics-wrapper">Noto ke bistar aaj hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noto ke bistar aaj hain"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaale dhande ke bazar mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaale dhande ke bazar mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bikta kabhi nahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bikta kabhi nahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Apne sar pe wo taaz hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apne sar pe wo taaz hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kal tak toh char sikke hi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal tak toh char sikke hi "/>
</div>
<div class="lyrico-lyrics-wrapper">Jeb mein khankhana rahe the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeb mein khankhana rahe the"/>
</div>
<div class="lyrico-lyrics-wrapper">Noto ke bistar aaj hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noto ke bistar aaj hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaale dhande ke bazar mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaale dhande ke bazar mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bikta kabhi nahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bikta kabhi nahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Apne sar pe wo taaz hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apne sar pe wo taaz hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roke jo koi agar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roke jo koi agar"/>
</div>
<div class="lyrico-lyrics-wrapper">Seedha thok de udhar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seedha thok de udhar"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhe foot gaad de bas kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhe foot gaad de bas kya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ungli ye trigger pe hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungli ye trigger pe hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Daring bhi jigar mein hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daring bhi jigar mein hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Apni thaath hai bas kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni thaath hai bas kya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae black white mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae black white mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Wrong right sab adjust hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wrong right sab adjust hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Underground har cheez ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Underground har cheez ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yahan bandobast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahan bandobast hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Babu rao mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu rao mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mast hai mast mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mast hai mast mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Babu rao mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu rao mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mast hai mast mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mast hai mast mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Babu rao mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu rao mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mast hai mast mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mast hai mast mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Babu rao mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu rao mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mast hai mast mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mast hai mast mast hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O duniya ye matlabi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O duniya ye matlabi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Badi kameeni jagah hai yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badi kameeni jagah hai yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Maange kuch milta hai kahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maange kuch milta hai kahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo chahiye wo collar pakad ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo chahiye wo collar pakad ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum toh nikaalte hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum toh nikaalte hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Watt apna chalta hai yahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watt apna chalta hai yahan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O duniya ye matlabi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O duniya ye matlabi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Badi kameeni jaga hai yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badi kameeni jaga hai yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Maange kuch milta hai kahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maange kuch milta hai kahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo chahiye wo collar pakad ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo chahiye wo collar pakad ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum toh nikaalte hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum toh nikaalte hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Watt apna chalta hai yahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watt apna chalta hai yahan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O apne pair ke tale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O apne pair ke tale"/>
</div>
<div class="lyrico-lyrics-wrapper">System saara ye chale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="System saara ye chale"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee le phaad ke bas kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee le phaad ke bas kya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ungli ye trigger pe hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungli ye trigger pe hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Daring bhi jigar mein hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daring bhi jigar mein hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Apni thath hai bas kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni thath hai bas kya"/>
</div>
<div class="lyrico-lyrics-wrapper">O beech line mein hai sabi khade 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O beech line mein hai sabi khade "/>
</div>
<div class="lyrico-lyrics-wrapper">Apun first hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apun first hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Underground har cheez ka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Underground har cheez ka "/>
</div>
<div class="lyrico-lyrics-wrapper">Yahan bandobast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahan bandobast hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Babu rao mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu rao mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mast hai mast mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mast hai mast mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Babu rao mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu rao mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mast hai mast mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mast hai mast mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Babu rao mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu rao mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mast hai mast mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mast hai mast mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Babu rao mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu rao mast hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mast hai mast mast hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mast hai mast mast hai"/>
</div>
</pre>
