---
title: "pesatha mozhiye song lyrics"
album: "Kombu Vatcha Singamda"
artist: "Dhibu Ninan Thomas"
lyricist: "Arunraja Kamaraj"
director: "S.R. Prabhakaran"
path: "/albums/kombu-vatcha-singamda-lyrics"
song: "Pesatha Mozhiye"
image: ../../images/albumart/kombu-vatcha-singamda.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5GQQHhcgJKg"
type: "love"
singers:
  - Chinmayi
  - K.S. Harisankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesaadha mozhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaadha mozhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhiyaadha paniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhiyaadha paniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pularaadha poonjolaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pularaadha poonjolaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verodu enaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verodu enaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saikkindra vizhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saikkindra vizhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu vilayaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu vilayaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan aruginil vanthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan aruginil vanthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi idikudhu anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi idikudhu anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna nilavena naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna nilavena naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai vilungidum theeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vilungidum theeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aaa aaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaa aaa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesaadha mozhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaadha mozhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhiyaadha paniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhiyaadha paniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pularaadha poonjolaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pularaadha poonjolaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Da ni sa ga ma da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da ni sa ga ma da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma da ma da ni ni ni ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma da ma da ni ni ni ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Da ma ga ma da ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da ma ga ma da ni sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga ma da ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga ma da ni sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma da ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma da ni sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma da ni ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma da ni ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa sa sa ni da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa sa ni da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma da ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma da ni sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga ga sa ni da ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga ga sa ni da ni sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaa aaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa aaa "/>
</div>
<div class="lyrico-lyrics-wrapper">aaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma da ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma da ni sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma da ni ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma da ni ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa sa sa ni da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa sa ni da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma da ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma da ni sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga ga sa ni da ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga ga sa ni da ni sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai kadanthidum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kadanthidum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam kudhikudhu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam kudhikudhu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooradhu vizhi eerppu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooradhu vizhi eerppu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal vidugindra thoodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal vidugindra thoodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti izhukudhu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti izhukudhu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsaara poongaatru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaara poongaatru nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaradhanai naan aagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradhanai naan aagida"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae varamaai varugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae varamaai varugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjae nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjae nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannangal naan aagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannangal naan aagida"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae uyiril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae uyiril"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli alli ennai poosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli alli ennai poosa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm mmm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm mm mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mm mm mm mm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm mm mm mm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mm mm mm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm mm mm mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa aa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaa aaaaaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaa aaaaaaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaa aaaaaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaa aaaaaaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaa aaaaaaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaa aaaaaaa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan madiyinil naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan madiyinil naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thangi thavaznthadhu podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangi thavaznthadhu podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee moottum uyir kaatru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee moottum uyir kaatru nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa aaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa aaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil thuvangidum thaedal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil thuvangidum thaedal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennil mudigindra thooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennil mudigindra thooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeradha idhazh vetkai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeradha idhazh vetkai nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovae poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovae poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonthottam thaandaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonthottam thaandaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae manamaai varugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae manamaai varugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelaamal mudiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelaamal mudiyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae arugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae arugil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella mella ennai koosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mella ennai koosa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesaadha mozhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaadha mozhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhiyaadha paniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhiyaadha paniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pularaadha poonjolaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pularaadha poonjolaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verodu enaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verodu enaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saikkindra vizhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saikkindra vizhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu vilayaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu vilayaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan aruginil vanthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan aruginil vanthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi idikudhu anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi idikudhu anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna nilavena naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna nilavena naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai vilungidum theeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vilungidum theeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa aa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa aa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa aa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa aa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jum jum jum jum jum jum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jum jum jum jum jum jum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaa aaa"/>
</div>
</pre>
