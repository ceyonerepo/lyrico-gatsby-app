---
title: "aasai peraasai song lyrics"
album: "Thani Oruvan"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "M. Raja"
path: "/albums/thani-oruvan-lyrics"
song: "Aasai Peraasai"
image: ../../images/albumart/thani-oruvan.jpg
date: 2015-08-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pjxBAjg8eMQ"
type: "Motivational"
singers:
  - Jayam Ravi
  - Hiphop Tamizha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennaikkum aasaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaikkum aasaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Peraasaikkum nadakkura por la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peraasaikkum nadakkura por la"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeikkurathu peraasaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeikkurathu peraasaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku nallathu seirathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku nallathu seirathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai illa peraasai (Dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai illa peraasai (Dialogue)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irutta veraturathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutta veraturathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Suriyan thaeva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suriyan thaeva illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae oru theekuchi pothum (Dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae oru theekuchi pothum (Dialogue)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mumbai taj attack pannumbothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mumbai taj attack pannumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ajmal kasabkku vayasu 18
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajmal kasabkku vayasu 18"/>
</div>
<div class="lyrico-lyrics-wrapper">Delhi rape case la involve aanavanukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Delhi rape case la involve aanavanukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu 17
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu 17"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaikku cityla irukkura koolipadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaikku cityla irukkura koolipadai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mollamari capemaari athanaperukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mollamari capemaari athanaperukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu 18… 19 (Dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu 18… 19 (Dialogue)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kettavan thappa seirathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettavan thappa seirathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappa kathukkirathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappa kathukkirathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu neram kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu neram kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethaiyumae paarkurathulla (Dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethaiyumae paarkurathulla (Dialogue)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aana nallavan mattum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana nallavan mattum thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallathu seirathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallathu seirathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam neram kaaranamnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam neram kaaranamnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaathukkum kaathittu irukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaathukkum kaathittu irukkaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirunthu kaathirunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirunthu kaathirunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam kadanthu poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam kadanthu poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatukulla maranju vaazhura poraliyavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatukulla maranju vaazhura poraliyavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa naattukkulla sagichu vaazhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa naattukkulla sagichu vaazhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemaaliyaavo irunthuttu irukkan (Dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaaliyaavo irunthuttu irukkan (Dialogue)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku poralikkaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku poralikkaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nokkamum irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nokkamum irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Police kaana athigaramum irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police kaana athigaramum irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan police uniform potta poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan police uniform potta poraali"/>
</div>
<div class="lyrico-lyrics-wrapper">Mithran IPS (Dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mithran IPS (Dialogue)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I will meet you very very soon 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I will meet you very very soon "/>
</div>
(<div class="lyrico-lyrics-wrapper">Dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dialogue)"/>
</div>
</pre>
