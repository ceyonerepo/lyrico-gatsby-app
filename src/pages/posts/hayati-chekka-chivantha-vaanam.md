---
title: "hayati song lyrics"
album: "Chekka Chivantha Vaanam"
artist: "AR Rahman"
lyricist: "Shiv"
director: "Mani Ratnam"
path: "/albums/chekka-chivantha-vaanam-lyrics"
song: "Hayati"
image: ../../images/albumart/chekka-chivantha-vaanam.jpg
date: 2018-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dP8DdszDd2s"
type: "mass"
singers:
  - Shiv
  - Mayssa Karaa
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hayati eee eee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati eee eee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati "/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati ee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Spit too quick
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spit too quick"/>
</div>
<div class="lyrico-lyrics-wrapper">But i hit my stride
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But i hit my stride"/>
</div>
<div class="lyrico-lyrics-wrapper">Flow fluid
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flow fluid"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m a wavy guy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m a wavy guy"/>
</div>
<div class="lyrico-lyrics-wrapper">Full moon bright
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full moon bright"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m ma tip the tides
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m ma tip the tides"/>
</div>
<div class="lyrico-lyrics-wrapper">If you had a bad night
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you had a bad night"/>
</div>
<div class="lyrico-lyrics-wrapper">I could flip the vibe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I could flip the vibe"/>
</div>
<div class="lyrico-lyrics-wrapper">Out in LA
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Out in LA"/>
</div>
<div class="lyrico-lyrics-wrapper">In a coupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In a coupe"/>
</div>
<div class="lyrico-lyrics-wrapper">When i ride
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When i ride"/>
</div>
<div class="lyrico-lyrics-wrapper">When i’m on the east coast
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When i’m on the east coast"/>
</div>
<div class="lyrico-lyrics-wrapper">Got New York pride
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Got New York pride"/>
</div>
<div class="lyrico-lyrics-wrapper">When i’m on the greece coast
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When i’m on the greece coast"/>
</div>
<div class="lyrico-lyrics-wrapper">See white all sides
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="See white all sides"/>
</div>
<div class="lyrico-lyrics-wrapper">In the far east
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In the far east"/>
</div>
<div class="lyrico-lyrics-wrapper">Cover of the Mumbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cover of the Mumbai"/>
</div>
<div class="lyrico-lyrics-wrapper">Times times times
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Times times times"/>
</div>
<div class="lyrico-lyrics-wrapper">Times times times times
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Times times times times"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okay challenges are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okay challenges are"/>
</div>
<div class="lyrico-lyrics-wrapper">Welcome to this game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome to this game"/>
</div>
<div class="lyrico-lyrics-wrapper">I am the don
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am the don"/>
</div>
<div class="lyrico-lyrics-wrapper">Any languages
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Any languages"/>
</div>
<div class="lyrico-lyrics-wrapper">I blend-em
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I blend-em"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola vanakkam salam hanji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola vanakkam salam hanji"/>
</div>
<div class="lyrico-lyrics-wrapper">Hello bonjour chao aloha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello bonjour chao aloha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahlan wa sahlan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahlan wa sahlan"/>
</div>
<div class="lyrico-lyrics-wrapper">No matter the vernacular
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No matter the vernacular"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m always talking strong
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m always talking strong"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayati eee eee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati eee eee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati ee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayati hayati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati hayati"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati hayati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati hayati"/>
</div>
<div class="lyrico-lyrics-wrapper">Living my low life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Living my low life"/>
</div>
<div class="lyrico-lyrics-wrapper">And ain’t low key
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And ain’t low key"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati hayati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati hayati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">And I never take time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I never take time"/>
</div>
<div class="lyrico-lyrics-wrapper">When i elevate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When i elevate"/>
</div>
<div class="lyrico-lyrics-wrapper">And I never take time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I never take time"/>
</div>
<div class="lyrico-lyrics-wrapper">To celebrate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To celebrate"/>
</div>
<div class="lyrico-lyrics-wrapper">But I take time 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But I take time "/>
</div>
<div class="lyrico-lyrics-wrapper">when I meditate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="when I meditate"/>
</div>
<div class="lyrico-lyrics-wrapper">And I never waste time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I never waste time"/>
</div>
<div class="lyrico-lyrics-wrapper">When I’m celebrating
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When I’m celebrating"/>
</div>
<div class="lyrico-lyrics-wrapper">Crazy and deranged
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crazy and deranged"/>
</div>
<div class="lyrico-lyrics-wrapper">But the parties are insane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But the parties are insane"/>
</div>
<div class="lyrico-lyrics-wrapper">We are living in the fast lane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We are living in the fast lane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Money ain’t a thing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Money ain’t a thing"/>
</div>
<div class="lyrico-lyrics-wrapper">No money ain’t a thing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No money ain’t a thing"/>
</div>
<div class="lyrico-lyrics-wrapper">And we spend it at a fast rate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And we spend it at a fast rate"/>
</div>
<div class="lyrico-lyrics-wrapper">Am never tame on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Am never tame on"/>
</div>
<div class="lyrico-lyrics-wrapper">Top of the game and
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Top of the game and"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m leaving you in last place
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m leaving you in last place"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Spit heat like a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spit heat like a"/>
</div>
<div class="lyrico-lyrics-wrapper">Dragon flame of the brain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dragon flame of the brain"/>
</div>
<div class="lyrico-lyrics-wrapper">And it’ll leave you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And it’ll leave you"/>
</div>
<div class="lyrico-lyrics-wrapper">With a bad taste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="With a bad taste"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah we live fast paced life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah we live fast paced life"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayati hayati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati hayati"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okay challenges are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okay challenges are"/>
</div>
<div class="lyrico-lyrics-wrapper">Welcome to this game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome to this game"/>
</div>
<div class="lyrico-lyrics-wrapper">I am the don
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am the don"/>
</div>
<div class="lyrico-lyrics-wrapper">Any languages
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Any languages"/>
</div>
<div class="lyrico-lyrics-wrapper">I blend-em
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I blend-em"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola vanakkam salam hanji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola vanakkam salam hanji"/>
</div>
<div class="lyrico-lyrics-wrapper">Hello bonjour chao aloha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello bonjour chao aloha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahlan wa sahlan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahlan wa sahlan"/>
</div>
<div class="lyrico-lyrics-wrapper">No matter the vernacular
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No matter the vernacular"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m always talking strong
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m always talking strong"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayati eee eee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati eee eee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati "/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati ee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hayati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati haya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati haya"/>
</div>
<div class="lyrico-lyrics-wrapper">Party night one till
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party night one till"/>
</div>
<div class="lyrico-lyrics-wrapper">All night till
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All night till"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati haya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati haya"/>
</div>
<div class="lyrico-lyrics-wrapper">One two three party three
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One two three party three"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati haya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati haya"/>
</div>
<div class="lyrico-lyrics-wrapper">Living my life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Living my life"/>
</div>
<div class="lyrico-lyrics-wrapper">Ain’t low key
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ain’t low key"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayati haya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayati haya"/>
</div>
</pre>
