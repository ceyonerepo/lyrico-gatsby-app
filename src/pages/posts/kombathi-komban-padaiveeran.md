---
title: "kombathi komban song lyrics"
album: "Padaiveeran"
artist: "Karthik Raja"
lyricist: "Mohan Rajan"
director: "Dhana Sekaran"
path: "/albums/padaiveeran-lyrics"
song: "Kombathi Komban"
image: ../../images/albumart/padaiveeran.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/aS_k5qmD6hQ"
type: "mass"
singers:
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kombaathi kombanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombaathi kombanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kombu seevi vanthaanadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombu seevi vanthaanadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaatha aattam onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaatha aattam onna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi paarkka vanthaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi paarkka vanthaanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa da maplaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa da maplaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattaang kattaang kulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaang kattaang kulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karukaattaang kulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karukaattaang kulam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yettu thisa sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yettu thisa sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu enga aadukalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu enga aadukalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotta thotta sudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta thotta sudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodung kaathaa sudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodung kaathaa sudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti thotti ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti thotti ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaa vanthu pathikkollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaa vanthu pathikkollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga ooru manukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ooru manukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukuthadaa varalaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukuthadaa varalaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha chinnamannoru kaarankitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha chinnamannoru kaarankitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaandaa thagaraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaandaa thagaraaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeratha udambaathaan valathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeratha udambaathaan valathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Than maanatha uyiraathaan nenappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Than maanatha uyiraathaan nenappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasatha manasaara kodupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasatha manasaara kodupom"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga kovatha yedapottaa polappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga kovatha yedapottaa polappom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandiyaru aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandiyaru aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu thaatiyaru koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu thaatiyaru koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru singam puli pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru singam puli pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga poduvonda nottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga poduvonda nottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaravaara aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaravaara aattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu kaarasaara koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu kaarasaara koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada yaaru enna sonnapothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada yaaru enna sonnapothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga ketka maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga ketka maattom"/>
</div>
</pre>
