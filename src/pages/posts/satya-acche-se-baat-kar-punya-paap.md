---
title: "satya - acche se baat kar song lyrics"
album: "Punya Paap"
artist: "Karan Kanchan"
lyricist: "DIVINE"
director: "Gil Green"
path: "/albums/punya-paap-lyrics"
song: "Satya - Acche Se Baat Kar"
image: ../../images/albumart/punya-paap.jpg
date: 2020-12-03
lang: hindi
youtubeLink: "https://www.youtube.com/embed/rO4DbBjaWJY"
type: "happy"
singers:
  - DIVINE
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">You know who the fu*k it is haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You know who the fu*k it is haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Divine haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Divine haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gully gang haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gully gang haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum log chill karenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum log chill karenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Apun drill karenge aah..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apun drill karenge aah.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha!"/>
</div>
<div class="lyrico-lyrics-wrapper">Acche se baat kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acche se baat kar"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhote acche se baat kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhote acche se baat kar"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chulhe mein chingari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chulhe mein chingari"/>
</div>
<div class="lyrico-lyrics-wrapper">Sar pe hai chhat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sar pe hai chhat"/>
</div>
<div class="lyrico-lyrics-wrapper">Badan pe kapde tab zindagi mast
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badan pe kapde tab zindagi mast"/>
</div>
<div class="lyrico-lyrics-wrapper">Sab samundar par se bole whats up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab samundar par se bole whats up"/>
</div>
<div class="lyrico-lyrics-wrapper">Dm mein ladkiyan likhti hai khat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dm mein ladkiyan likhti hai khat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gully gang chhaati thok ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gully gang chhaati thok ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu suraj pancholi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu suraj pancholi"/>
</div>
<div class="lyrico-lyrics-wrapper">Main chandra bose hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main chandra bose hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Shanti failana hum mantra bolte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shanti failana hum mantra bolte"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu ro dega agar apun santra khol de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu ro dega agar apun santra khol de"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patra tod ke nikle ISRO ki tarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patra tod ke nikle ISRO ki tarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhaap ke nahi kahin dusron ki tarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhaap ke nahi kahin dusron ki tarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Vastav hai kala meri lifestyle ki tarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vastav hai kala meri lifestyle ki tarah"/>
</div>
<div class="lyrico-lyrics-wrapper">Rolly range hoon main chakri ki tarah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rolly range hoon main chakri ki tarah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nange pair hoon jo signal pe khada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nange pair hoon jo signal pe khada"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivaji rajya se swagat pe maza
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivaji rajya se swagat pe maza"/>
</div>
<div class="lyrico-lyrics-wrapper">Badalon ke upar mera swag apsara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badalon ke upar mera swag apsara"/>
</div>
<div class="lyrico-lyrics-wrapper">Spicy hai sab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spicy hai sab"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirf chocolate hai color haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirf chocolate hai color haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goli maar bheje mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli maar bheje mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Bheja shor karta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bheja shor karta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poore desh bhar mein charcha hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poore desh bhar mein charcha hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bole apun karta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bole apun karta hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goli maar bheje mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli maar bheje mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Bheja shor karta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bheja shor karta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poore desh bhar mein charcha hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poore desh bhar mein charcha hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bole apun karta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bole apun karta hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sir ka ladka karta dharta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sir ka ladka karta dharta"/>
</div>
<div class="lyrico-lyrics-wrapper">Saat ko lekar main sath mein chalta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saat ko lekar main sath mein chalta"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum bahar jab nikle kuch karna padta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum bahar jab nikle kuch karna padta"/>
</div>
15 <div class="lyrico-lyrics-wrapper">peti mere show ka lagta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peti mere show ka lagta"/>
</div>
30 <div class="lyrico-lyrics-wrapper">peti mere verse ka lagta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peti mere verse ka lagta"/>
</div>
<div class="lyrico-lyrics-wrapper">Topi nikale to church ka ladka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Topi nikale to church ka ladka"/>
</div>
<div class="lyrico-lyrics-wrapper">Topi pehna de phir karte lafda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Topi pehna de phir karte lafda"/>
</div>
<div class="lyrico-lyrics-wrapper">Load nahi lete apun duniya bhar ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Load nahi lete apun duniya bhar ka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nange pair se seedha lounge mein chair
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nange pair se seedha lounge mein chair"/>
</div>
<div class="lyrico-lyrics-wrapper">Paper ke pakad mein duniya bhel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paper ke pakad mein duniya bhel"/>
</div>
<div class="lyrico-lyrics-wrapper">Chase karte hum sirf gandhi ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chase karte hum sirf gandhi ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladkiyan fail badhiyan mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladkiyan fail badhiyan mel"/>
</div>
<div class="lyrico-lyrics-wrapper">Matlab saat figure deal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matlab saat figure deal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khud mein ek brand to kyun karun free
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud mein ek brand to kyun karun free"/>
</div>
<div class="lyrico-lyrics-wrapper">Zarurat nahi wahan bante nahi g
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zarurat nahi wahan bante nahi g"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandana bandana gang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandana bandana gang"/>
</div>
59 <div class="lyrico-lyrics-wrapper">bombay se bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bombay se bhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goli maar bheje mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli maar bheje mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Bheja shor karta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bheja shor karta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poore desh bhar mein charcha hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poore desh bhar mein charcha hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bole apun karta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bole apun karta hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goli maar bheje mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goli maar bheje mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Bheja shor karta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bheja shor karta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poore desh bhar mein charcha hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poore desh bhar mein charcha hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bole apun karta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bole apun karta hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jhoom jhoom jhoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhoom jhoom jhoom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera bhai ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera bhai ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhoom jhoom mera bhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhoom jhoom mera bhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhoom jhoom mera bhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhoom jhoom mera bhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha! Ha!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha! Ha!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Drill drill drill karenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drill drill drill karenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum log chill karenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum log chill karenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Apun drill karenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apun drill karenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Drill drill
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drill drill"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karan kanchan beat pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karan kanchan beat pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Punya paap! Punya paap!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punya paap! Punya paap!"/>
</div>
</pre>
