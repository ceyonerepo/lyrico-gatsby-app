---
title: "theeradha theedalgal song lyrics"
album: "Koothan"
artist: "Balz G"
lyricist: "MK Balaji"
director: "Venky AL"
path: "/albums/koothan-lyrics"
song: "Theeradha Theedalgal"
image: ../../images/albumart/koothan.jpg
date: 2018-10-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4RN1eT-gM3w"
type: "love"
singers:
  - Padmalatha
  - MK Balaji
  - Kamalaja 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">theeratha theendalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeratha theendalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">theetidu theeyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetidu theeyava"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongatha thooralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongatha thooralgal"/>
</div>
<div class="lyrico-lyrics-wrapper">thoovidu thoodhuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoovidu thoodhuva"/>
</div>
<div class="lyrico-lyrics-wrapper">pennaga naan indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennaga naan indru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhdhida kaaranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhdhida kaaranam"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai sernthidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai sernthidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vidiyaatha iravinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyaatha iravinai"/>
</div>
<div class="lyrico-lyrics-wrapper">thandhidu maayava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandhidu maayava"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyaadha porinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyaadha porinai"/>
</div>
<div class="lyrico-lyrics-wrapper">vendridu maaveera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendridu maaveera"/>
</div>
<div class="lyrico-lyrics-wrapper">vadiyadha vellamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadiyadha vellamai"/>
</div>
<div class="lyrico-lyrics-wrapper">endrumey ennule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrumey ennule"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum thengidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum thengidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mugathirai neeyum kizhithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugathirai neeyum kizhithu"/>
</div>
<div class="lyrico-lyrics-wrapper">anaithida vendum thudithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaithida vendum thudithu"/>
</div>
<div class="lyrico-lyrics-wrapper">sila nodi neram kalithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila nodi neram kalithu"/>
</div>
<div class="lyrico-lyrics-wrapper">vedithidu venpaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedithidu venpaniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vilagida neyum maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilagida neyum maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyalai thandi kadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyalai thandi kadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sila mani neram uranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila mani neram uranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">urasidu ulagalage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urasidu ulagalage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">theeratha theendalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeratha theendalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">theetidu theeyava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theetidu theeyava"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongatha thooralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongatha thooralgal"/>
</div>
<div class="lyrico-lyrics-wrapper">thoovidu thoodhuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoovidu thoodhuva"/>
</div>
<div class="lyrico-lyrics-wrapper">pennaga naan indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennaga naan indru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhdhida kaaranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhdhida kaaranam"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai sernthidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai sernthidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannakuli kaayum munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannakuli kaayum munne"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal mazhaiyal thaakathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal mazhaiyal thaakathe"/>
</div>
<div class="lyrico-lyrics-wrapper">paayum nathiyin vegam thannai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paayum nathiyin vegam thannai "/>
</div>
<div class="lyrico-lyrics-wrapper">mannin anaiyal mudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannin anaiyal mudathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">konji konji pesi ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konji konji pesi ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">kotukulle thallathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotukulle thallathey"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam nanum imsai seidhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam nanum imsai seidhal"/>
</div>
<div class="lyrico-lyrics-wrapper">illai endru sollathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai endru sollathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">idam naanum thanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idam naanum thanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ellaigal meeruvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ellaigal meeruvai"/>
</div>
<div class="lyrico-lyrics-wrapper">en viyarvai thuliyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en viyarvai thuliyai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee mariduvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mariduvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sellathu thisaigal athai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sellathu thisaigal athai"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi naan theenduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi naan theenduven"/>
</div>
<div class="lyrico-lyrics-wrapper">kaarayam aagiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaarayam aagiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thooram nindru kannil theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram nindru kannil theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanal neerum nee thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanal neerum nee thane"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanal nerai kannukulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanal nerai kannukulle"/>
</div>
<div class="lyrico-lyrics-wrapper">kadathi selbaval naan thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadathi selbaval naan thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">varudam thorum kathal solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varudam thorum kathal solla"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu kidapaval naan thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu kidapaval naan thane"/>
</div>
<div class="lyrico-lyrics-wrapper">varuvai varuvai ena naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuvai varuvai ena naan"/>
</div>
<div class="lyrico-lyrics-wrapper">engum paruva malaiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum paruva malaiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">nee thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanavai kanavai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavai kanavai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">iravodu tholainthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravodu tholainthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">nijamaai nijamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijamaai nijamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">enai anaipaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai anaipaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">nizhalai nizhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhalai nizhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">un koodave nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un koodave nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">nithamum kaathirupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nithamum kaathirupen"/>
</div>
</pre>
