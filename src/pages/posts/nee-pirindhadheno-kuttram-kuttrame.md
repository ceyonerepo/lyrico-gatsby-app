---
title: "nee pirindhadheno song lyrics"
album: "Kuttram Kuttrame"
artist: "Ajesh"
lyricist: "Viveka"
director: "Suseenthiran"
path: "/albums/kuttram-kuttrame-lyrics"
song: "Nee Pirindhadheno"
image: ../../images/albumart/kuttram-kuttrame.jpg
date: 2022-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CoeMF3-I6lg"
type: "sad"
singers:
  - Ajesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanave Ninaive En Niraive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave Ninaive En Niraive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ennai Pirindhadheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Pirindhadheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Dhinam Karaindhadheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Dhinam Karaindhadheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Irundhaal Podhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Irundhaal Podhume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Kanave Ninaive En Niraive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Kanave Ninaive En Niraive"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiye Oh Unaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiye Oh Unaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thedugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thedugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pona Thadangal Meedhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pona Thadangal Meedhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Dhinam Nadakkirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Dhinam Nadakkirene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhale Nee Vaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhale Nee Vaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Theigiren Va Nee Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Theigiren Va Nee Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruginile Nee Irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginile Nee Irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhum Thevai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhum Thevai Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paadalin Osaiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadalin Osaiye "/>
</div>
<div class="lyrico-lyrics-wrapper">Meendum Vaaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Vaaraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ponadhum Ponadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ponadhum Ponadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin Eeram Thevaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin Eeram Thevaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanave Ninaive En Niraive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave Ninaive En Niraive"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiye Unaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiye Unaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thedugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thedugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pona Thadangal Meedhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pona Thadangal Meedhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Dhinam Nadakkirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Dhinam Nadakkirene"/>
</div>
</pre>
