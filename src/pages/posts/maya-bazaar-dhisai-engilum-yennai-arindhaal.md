---
title: "maya bazaar - dhisai engilum song lyrics"
album: "Yennai Arindhaal"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Gautham Vasudev Menon"
path: "/albums/yennai-arindhaal-lyrics"
song: "Maya Bazaar - Dhisai Engilum"
image: ../../images/albumart/yennai-arindhaal.jpg
date: 2015-02-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/q0oIpVIV4lo"
type: "Mass"
singers:
  - Aalaap Raju
  - Priya Subramanian
  - Velmurugan
  - Krishna Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kada Kada Kada Kadodkajan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada Kada Kada Kadodkajan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Para Para Para Paraakraman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Para Para Para Paraakraman"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kada Kada Kada Kadodkajan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada Kada Kada Kadodkajan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Para Para Para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Para Para Para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Para Para Para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Para Para Para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhisai Engilum En Kodi Parakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai Engilum En Kodi Parakkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhuvanam Muzhuthum En Kural Olikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhuvanam Muzhuthum En Kural Olikkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethirigal Iru Paadham Paniyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirigal Iru Paadham Paniyanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saranaagadhi Tharum Varai Nadunganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saranaagadhi Tharum Varai Nadunganum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhi Bhalavaan Bheemanin Maganada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Bhalavaan Bheemanin Maganada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aram Tharugira Dharumanin Uravadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aram Tharugira Dharumanin Uravadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saga Muthavarin Sangadam Theerpavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saga Muthavarin Sangadam Theerpavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sagalarum Thozhum Mannavan Thaan Ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagalarum Thozhum Mannavan Thaan Ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kada Kada Kada Kadodkajan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada Kada Kada Kadodkajan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Para Para Para Paraakraman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Para Para Para Paraakraman"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kada Kada Kada Kadodkajan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada Kada Kada Kadodkajan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Para Para Para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Para Para Para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Para Para Para
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Para Para Para"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe En Aaruyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe En Aaruyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amuthey Nee Vaa Arugey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amuthey Nee Vaa Arugey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan Mugilodum Iravodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Mugilodum Iravodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyaadum Vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadum Vennila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi Meethu Vizhi Veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Meethu Vizhi Veesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyaadum Pen Nilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadum Pen Nilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanne Unnai Piriyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Unnai Piriyen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Ennaalum Naan Maraven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Ennaalum Naan Maraven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe En Aaruyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe En Aaruyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhen Naan Un Arugey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhen Naan Un Arugey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Pagalodum Iravodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Pagalodum Iravodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi Kaanum En Kanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Kaanum En Kanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imai Meethum Thuyil Meethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai Meethum Thuyil Meethum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadamaadum Un Ula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadamaadum Un Ula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Unnai Piriyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Unnai Piriyen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ennaalum Naan Maraven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ennaalum Naan Maraven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Nee Enakku Venumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Nee Enakku Venumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangame Thangam Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Thangam Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Vechcha Kannu Vaangavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Vechcha Kannu Vaangavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangame Thangam Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Thangam Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Muththu Mani Alli Varavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Muththu Mani Alli Varavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Aagaasaththa Seeraa Tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Aagaasaththa Seeraa Tharavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Nee Enakku Venumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Nee Enakku Venumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangame Thangam Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Thangam Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Vechcha Kannu Vaangavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Vechcha Kannu Vaangavilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangame Thangam Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Thangam Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mukkodi Devargalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukkodi Devargalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mummoorthi Deviyarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummoorthi Deviyarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru Ulamaara Manathaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Ulamaara Manathaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varam Thanthu Vaazhthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam Thanthu Vaazhthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagaala Pugal Sooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagaala Pugal Sooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Isai Paadi Potrida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai Paadi Potrida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manamakkal Vaazhiyave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamakkal Vaazhiyave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Kurai indri Vaazhiyave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Kurai indri Vaazhiyave"/>
</div>
</pre>
