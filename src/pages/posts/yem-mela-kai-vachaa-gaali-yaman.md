---
title: "yem mela kai vachaa gaali song lyrics"
album: "Yaman"
artist: "Vijay Antony"
lyricist: "Annamalai"
director: "Jeeva Shankar"
path: "/albums/yaman-lyrics"
song: "Yem Mela Kai Vachaa Gaali"
image: ../../images/albumart/yaman.jpg
date: 2017-02-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DQamy41prn0"
type: "mass"
singers:
  - Hemachandra
  - Chetan
  - Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Make Way Here Comes The King
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make Way Here Comes The King"/>
</div>
<div class="lyrico-lyrics-wrapper">With Those Classy Shiny Bling Bling Thing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="With Those Classy Shiny Bling Bling Thing"/>
</div>
<div class="lyrico-lyrics-wrapper">Moving Like A Wrecking Machine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moving Like A Wrecking Machine"/>
</div>
<div class="lyrico-lyrics-wrapper">Get A Life He is The Best Listen Up Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get A Life He is The Best Listen Up Now"/>
</div>
<div class="lyrico-lyrics-wrapper">Let's Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let's Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Mela Kai Vacha Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Mela Kai Vacha Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhirunda Onnoda Thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhirunda Onnoda Thaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yem Mela Kai Vecha Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yem Mela Kai Vecha Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Maganey Andhirundha Onnoda Thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maganey Andhirundha Onnoda Thaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelu Maram Setha Naarkali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelu Maram Setha Naarkali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Setha Edam Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Setha Edam Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakkali Thakkalai Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkali Thakkalai Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandathey Yen Veli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandathey Yen Veli"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinjurundha Onjoli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinjurundha Onjoli"/>
</div>
<div class="lyrico-lyrics-wrapper">Duppaaley Puppaaley Doli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duppaaley Puppaaley Doli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelu Maram Setha Naarkali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelu Maram Setha Naarkali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Setha Edam Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Setha Edam Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakkali Thakkalai Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkali Thakkalai Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandathey Yen Veli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandathey Yen Veli"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinjurundha Onjoli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinjurundha Onjoli"/>
</div>
<div class="lyrico-lyrics-wrapper">Duppaaley Puppaaley Doli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duppaaley Puppaaley Doli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Mela Kai Vacha Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Mela Kai Vacha Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhirunda Onnoda Thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhirunda Onnoda Thaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yem Mela Kai Vecha Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yem Mela Kai Vecha Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Maganey Andhirundha Onnoda Thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maganey Andhirundha Onnoda Thaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotram Kandu Edai Podaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotram Kandu Edai Podaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Vitta Sevulu Kizhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Vitta Sevulu Kizhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oaram Poada Dae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oaram Poada Dae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Patta Adiyellam Padikattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Patta Adiyellam Padikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeikathan Vandhen Naan Porapattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeikathan Vandhen Naan Porapattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Missagi Pogathu Naan Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Missagi Pogathu Naan Thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambi Enpakkam Nee Senthaa Sari Hittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi Enpakkam Nee Senthaa Sari Hittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thoonga En Kaithan Thalagaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thoonga En Kaithan Thalagaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkunu Irukkuthu Thani Baani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkunu Irukkuthu Thani Baani"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhilluthaan Munnera Mudhal Yeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhilluthaan Munnera Mudhal Yeni"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vandikku Naanethaan Kadaiyaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vandikku Naanethaan Kadaiyaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Sindhum Vervaiya Kadalakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sindhum Vervaiya Kadalakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhula Kappala Otttuvendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhula Kappala Otttuvendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan patta Kashtaththa Eruvaaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan patta Kashtaththa Eruvaaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Uruvaaki Kaattuvendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Uruvaaki Kaattuvendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Mela Kai Vacha Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Mela Kai Vacha Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhirunda Onnoda Thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhirunda Onnoda Thaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yem Mela Kai Vecha Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yem Mela Kai Vecha Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Maganey Andhirundha Onnoda Thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maganey Andhirundha Onnoda Thaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Vegaththa Thadai Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Vegaththa Thadai Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukka Nee Mudiyaathu Nadai Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukka Nee Mudiyaathu Nadai Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Pogum Vegam Than Rocket-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pogum Vegam Than Rocket-u"/>
</div>
<div class="lyrico-lyrics-wrapper">AAndha Aayam Thanda En Target-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AAndha Aayam Thanda En Target-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Naan Yaraiyum Thodamatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Naan Yaraiyum Thodamatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Pottini Vandhakka Vidamaaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Pottini Vandhakka Vidamaaten"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasukku Eppothum Vizhamaaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasukku Eppothum Vizhamaaten"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambi Nee Kaattum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi Nee Kaattum "/>
</div>
<div class="lyrico-lyrics-wrapper">Paasaththa Edai Potten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasaththa Edai Potten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vambellam Engitta Vachikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambellam Engitta Vachikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Puthooru Maavukattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Puthooru Maavukattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nalladha Yezhaikku Senjiputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nalladha Yezhaikku Senjiputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vananguven Kaalathottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vananguven Kaalathottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Mela Kai Vacha Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Mela Kai Vacha Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhirunda Onnoda Thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhirunda Onnoda Thaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yem Mela Kai Vecha Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yem Mela Kai Vecha Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Maganey Andhirundha Onnoda Thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maganey Andhirundha Onnoda Thaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelu Maram Setha Naarkali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelu Maram Setha Naarkali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Setha Edam Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Setha Edam Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakkali Thakkalai Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkali Thakkalai Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandathey Yen Veli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandathey Yen Veli"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinjurundha Onjoli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinjurundha Onjoli"/>
</div>
<div class="lyrico-lyrics-wrapper">Duppaaley Puppaaley Doli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duppaaley Puppaaley Doli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Mela Kai Vacha Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Mela Kai Vacha Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhirunda Onnoda Thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhirunda Onnoda Thaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Yem Mela Kai Vecha Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yem Mela Kai Vecha Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Maganey Andhirundha Onnoda Thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maganey Andhirundha Onnoda Thaali"/>
</div>
</pre>
