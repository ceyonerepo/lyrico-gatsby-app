---
title: 'vanthe mataram lyrics'
album: 'Bhoomi'
artist: 'D Imman'
lyricist: 'Madhan Karky'
director: 'Lakshman'
path: '/albums/bhoomi-song-lyrics'
song: 'Vanthe Mataram'
image: ../../images/albumart/bhoomi.jpg
date: 2020-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UYoHzRmPXgs"
type: 'Motivational'
singers: 
- Ananya Bhat
- Sabesh Manmathan 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannai Kaakkave Nenjin Maavuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai Kaakkave Nenjin Maavuram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahimsai Kondu Ezhunthu Nindru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahimsai Kondu Ezhunthu Nindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Agilam Athira Vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agilam Athira Vendume"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivin Theeyil Aayudham Theetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivin Theeyil Aayudham Theetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvum Mudiyum Endrume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvum Mudiyum Endrume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbaa Alai Kadalena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaa Alai Kadalena"/>
</div>
<div class="lyrico-lyrics-wrapper">Panbaa Perum Mazhaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panbaa Perum Mazhaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbaa Unai Anaithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbaa Unai Anaithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Intha India
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Intha India"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Yemathuyirena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Yemathuyirena"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeram Ethu Ethuvena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram Ethu Ethuvena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paar Thaa Yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paar Thaa Yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannai Kaakkave Nenjin Maavuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai Kaakkave Nenjin Maavuram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru Koodume Sodhanai Karam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru Koodume Sodhanai Karam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannai Kaakkave Nenjin Maavuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai Kaakkave Nenjin Maavuram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasai Veesi Emmai Vaangida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasai Veesi Emmai Vaangida"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal Bommai Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal Bommai Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theengai Theengai Kandu Thoongida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theengai Theengai Kandu Thoongida"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal Katrathillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal Katrathillaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi Kodi Vetrumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Kodi Vetrumai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engalukkul Kondumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalukkul Kondumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Desam Kaakkave Ondravom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desam Kaakkave Ondravom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalaikaaga Veedhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaikaaga Veedhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Needhi Ketta Bhoomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhi Ketta Bhoomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Soorayadinal Theeyavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorayadinal Theeyavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aattral Kadal Azhaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattral Kadal Azhaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Seetram Erimazhaiyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seetram Erimazhaiyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum Kalai Kalaikkidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum Kalai Kalaikkidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Intha India
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Intha India"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Yemathuyirena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Yemathuyirena"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeram Ethu Ethuvena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram Ethu Ethuvena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paarthaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paarthaayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannai Kaakkave Nenjin Maavuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannai Kaakkave Nenjin Maavuram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vande Mataram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vande Mataram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru Koodume Sodhanai Karam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru Koodume Sodhanai Karam"/>
</div>
</pre>