---
title: "shuru karo song lyrics"
album: "Maestro"
artist: "Mahati Swara Sagar"
lyricist: "Shreemani"
director: "Merlapaka Gandhi"
path: "/albums/maestro-lyrics"
song: "Shuru Karo"
image: ../../images/albumart/maestro.jpg
date: 2021-09-17
lang: telugu
youtubeLink: "https://www.youtube.com/embed/XNdwA8Zm7bw"
type: "happy"
singers:
  - L.V. Revanth
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hello mic testing 123
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello mic testing 123"/>
</div>
<div class="lyrico-lyrics-wrapper">Please lisen to a story
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Please lisen to a story"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shuru karo shuru karo guruji story
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shuru karo shuru karo guruji story"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kathemito baadhemito cheppey oo sari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kathemito baadhemito cheppey oo sari"/>
</div>
<div class="lyrico-lyrics-wrapper">Start music
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Start music"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaha hey oho turrrr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha hey oho turrrr"/>
</div>
<div class="lyrico-lyrics-wrapper">Emito remito thelvani savari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emito remito thelvani savari"/>
</div>
<div class="lyrico-lyrics-wrapper">Inni rangulelaa choosindhiro bholo atthiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni rangulelaa choosindhiro bholo atthiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey so happy life yee saafigundedhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey so happy life yee saafigundedhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Coffeetho kalise sofialaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coffeetho kalise sofialaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Music ye praanam antu aa velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Music ye praanam antu aa velaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Magicaa modale love feel ye sufila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magicaa modale love feel ye sufila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maestro maestro I’m a maestro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maestro maestro I’m a maestro"/>
</div>
<div class="lyrico-lyrics-wrapper">Lovely life yee prathi roju lover tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lovely life yee prathi roju lover tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Maestro maestro I’m a maestro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maestro maestro I’m a maestro"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly life yee jaabilli flowertho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly life yee jaabilli flowertho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shuru karo shuru karo guruji story
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shuru karo shuru karo guruji story"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kathemito baadhemito cheppey oo sari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kathemito baadhemito cheppey oo sari"/>
</div>
<div class="lyrico-lyrics-wrapper">Emito remito thelvani savari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emito remito thelvani savari"/>
</div>
<div class="lyrico-lyrics-wrapper">Inni rangulelaa choosindhiro bholo atthiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni rangulelaa choosindhiro bholo atthiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peppygaa sexygaa undedhayyo naa tuneu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peppygaa sexygaa undedhayyo naa tuneu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okadochhi gimme show annte temptayyaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okadochhi gimme show annte temptayyaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha hey oho turr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha hey oho turr"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bigshot biwi ke birthday gift naa plan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bigshot biwi ke birthday gift naa plan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellaanu time ke fullugaa bookke ayyaany
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaanu time ke fullugaa bookke ayyaany"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi wife aa vaammo kaane kaadhantanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi wife aa vaammo kaane kaadhantanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peekalne tenche nife ye betteranta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peekalne tenche nife ye betteranta"/>
</div>
<div class="lyrico-lyrics-wrapper">Glamourne poosi chesina granideu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Glamourne poosi chesina granideu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ladylaa maari vasthondhi naa sideu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladylaa maari vasthondhi naa sideu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maestro maestro I’m a maestro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maestro maestro I’m a maestro"/>
</div>
<div class="lyrico-lyrics-wrapper">Life show maari modalaye killer show
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life show maari modalaye killer show"/>
</div>
<div class="lyrico-lyrics-wrapper">Maestro maestro I’m a maestro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maestro maestro I’m a maestro"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy show ayyindhi horror show
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy show ayyindhi horror show"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nextemito restemito chepey oo saru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nextemito restemito chepey oo saru"/>
</div>
<div class="lyrico-lyrics-wrapper">High tension lo padesilaa champaku masteru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="High tension lo padesilaa champaku masteru"/>
</div>
<div class="lyrico-lyrics-wrapper">Producer oo director oo phone yee chesharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Producer oo director oo phone yee chesharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa twistemito hotstar lo chseymannaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa twistemito hotstar lo chseymannaru"/>
</div>
</pre>
