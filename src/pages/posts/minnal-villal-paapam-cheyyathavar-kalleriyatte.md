---
title: "minnal villaal song lyrics"
album: "Paapam Cheyyathavar Kalleriyatte"
artist: "Prashant Pillai"
lyricist: "Anu Elizabeth"
director: "Shambhu Purushothaman"
path: "/albums/paapam-cheyyathavar-kalleriyatte-lyrics"
song: "Minnal Villaal"
image: ../../images/albumart/paapam-cheyyathavar-kalleriyatte.jpg
date: 2020-02-21
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/FFwBqxUklUw"
type: "love"
singers:
  - Sreekanth Hariharan
  - Preeti Pillai
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Minnal Villaal Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Villaal Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollaathinnee Nottamkondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaathinnee Nottamkondene"/>
</div>
<div class="lyrico-lyrics-wrapper">Thennal Kayyal Moodum Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thennal Kayyal Moodum Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuliraarnnu Nee Ninnathenthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuliraarnnu Nee Ninnathenthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathiyenam Chiri Viriye Muzhukidumen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathiyenam Chiri Viriye Muzhukidumen"/>
</div>
<div class="lyrico-lyrics-wrapper">Alakalile Mohangalaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alakalile Mohangalaay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithuvare Njaan Kaanaathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuvare Njaan Kaanaathen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullin Paathi Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullin Paathi Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Iniyakale Poyeedaam Nin Kaikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyakale Poyeedaam Nin Kaikal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayaathe Kannoram Neeyonnaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayaathe Kannoram Neeyonnaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Doorangal Njan Poyeedaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doorangal Njan Poyeedaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninne Kandal Ithal Virinju Mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Kandal Ithal Virinju Mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinne Mekham Pole Chirakellaame Neettidum Maanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinne Mekham Pole Chirakellaame Neettidum Maanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthumakalaal Pathivukalil Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthumakalaal Pathivukalil Oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamaay Oru Swaramaay Naam Maaridan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamaay Oru Swaramaay Naam Maaridan"/>
</div>
</pre>
