---
title: "venmathi venmathiye nillu song lyrics"
album: "Minnale"
artist: "Harris Jayaraj"
lyricist: "Late Vaaliba Vaali"
director: "Gautham Menon"
path: "/albums/minnale-lyrics"
song: "Venmathi Venmathiye Nillu"
image: ../../images/albumart/minnale.jpg
date: 2001-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3SMTGbhojpk"
type: "sad"
singers:
  - Roop Kumar Rathod
  - Tippu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Venmathi Venmathiye Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venmathi Venmathiye Nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vaanukka Megathukka Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaanukka Megathukka Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venmathi Venmathiye Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venmathi Venmathiye Nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vaanukka Megathukka Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaanukka Megathukka Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanamthaan Unnudaiya Ishtam Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamthaan Unnudaiya Ishtam Endraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megathukillai Oru Nashtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathukillai Oru Nashtam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Indrodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Indrodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Marappene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Marappene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Marappene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Marappene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaale Nenjil Pootha Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaale Nenjil Pootha Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melum Melum Thunbam Thunbam Vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melum Melum Thunbam Thunbam Vendam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venmathi Venmathiye Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venmathi Venmathiye Nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vaanukka Megathukka Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaanukka Megathukka Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanamthaan Unnudaiya Ishtam Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamthaan Unnudaiya Ishtam Endraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megathukillai Oru Nashtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathukillai Oru Nashtam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Indrodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Indrodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Marappene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Marappene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Marappene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Marappene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaale Nenjil Pootha Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaale Nenjil Pootha Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melum Melum Thunbam Thunbam Vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melum Melum Thunbam Thunbam Vendam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jannalin Vazhi Vanthu Vizhunthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannalin Vazhi Vanthu Vizhunthathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minnalin Oli Athil Therinthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnalin Oli Athil Therinthathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagu Devathai Athisaiya Mugame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Devathai Athisaiya Mugame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee Pori Ezha Iru Vizhigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Pori Ezha Iru Vizhigalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theekuchi Ena Ennai Urasida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theekuchi Ena Ennai Urasida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi Pookkalaai Malarnthathu Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Pookkalaai Malarnthathu Maname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Azhagai Paada Oru Mozhi Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Azhagai Paada Oru Mozhi Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alanthu Paarkka Pala Vizhi illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alanthu Paarkka Pala Vizhi illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ena iruntha Pothum Aval Enathillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena iruntha Pothum Aval Enathillaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maranthu Po En Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthu Po En Maname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Venmathi Venmathiye Nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Venmathi Venmathiye Nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vaanukka Megathukka Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaanukka Megathukka Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanamthaan Unnudaiya Ishtam Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamthaan Unnudaiya Ishtam Endraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megathukillai Oru Nashtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathukillai Oru Nashtam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Indrodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Indrodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Marappene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Marappene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Marappene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Marappene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaale Nenjil Pootha Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaale Nenjil Pootha Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melum Melum Thunbam Thunbam Vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melum Melum Thunbam Thunbam Vendam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anju Naal Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anju Naal Varai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Pozhinthathu Aasaiyin Mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Pozhinthathu Aasaiyin Mazhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athil Nanainthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Nanainthathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooru Jenmangal Ninaivinil Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Jenmangal Ninaivinil Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Pol Entha Naal Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Pol Entha Naal Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Urugiya Antha Naal Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Urugiya Antha Naal Sugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athai Ninaikkaiyil Raththa Naalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Ninaikkaiyil Raththa Naalangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raathiri Vedikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiri Vedikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Nimisam Kooda Enai Piriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nimisam Kooda Enai Piriyavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vivaram Yethum Aval Ariyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivaram Yethum Aval Ariyavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ena Iruntha Pothum Aval Enathillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Iruntha Pothum Aval Enathillaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maranthu Po En Maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthu Po En Maname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Indrodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Indrodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Marappene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Marappene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Marappene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Marappene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaale Nenjil Pootha Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaale Nenjil Pootha Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melum Melum Thunbam Thunbam Vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melum Melum Thunbam Thunbam Vendam"/>
</div>
</pre>
