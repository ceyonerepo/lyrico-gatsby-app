---
title: "rey idhe nee jaaga song lyrics"
album: "Manasuku Nachindi"
artist: "Radhan"
lyricist: "Ananta Sriram"
director: "Manjula Ghattamaneni"
path: "/albums/manasuku-nachindi-lyrics"
song: "Rey Idhe Nee Jaaga"
image: ../../images/albumart/manasuku-nachindi.jpg
date: 2018-02-16
lang: telugu
youtubeLink: "https://www.youtube.com/embed/TANq__t2UJM"
type: "happy"
singers:
  -	Yazin Nizar
  - Ramya NSK
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rey Ide Nee Jaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rey Ide Nee Jaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkada Evaru Ninnapalerugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkada Evaru Ninnapalerugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Kshanam Saradaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Kshanam Saradaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Undara Yedee Neekaddukadugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undara Yedee Neekaddukadugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Prathee Galini 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathee Galini "/>
</div>
<div class="lyrico-lyrics-wrapper">Prathee Gantanee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathee Gantanee "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvee Marchukoo Ela Neeku Veeluga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvee Marchukoo Ela Neeku Veeluga "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Gathamuni Marachi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gathamuni Marachi "/>
</div>
<div class="lyrico-lyrics-wrapper">Gadipe Samayame Yedurayethe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadipe Samayame Yedurayethe "/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Samayamu Malli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Samayamu Malli "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Veedanantu Brathimaalaalanthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Veedanantu Brathimaalaalanthe "/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Pongindi Naalo Ullasam Ullasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Pongindi Naalo Ullasam Ullasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Valai Allindi Nanne Santhosham Santhosham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valai Allindi Nanne Santhosham Santhosham "/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Pongindi Naalo Ullasam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Pongindi Naalo Ullasam "/>
</div>
<div class="lyrico-lyrics-wrapper">Valai Allindi Nanne Santhosham Santhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valai Allindi Nanne Santhosham Santhosham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arichesei Adduledura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arichesei Adduledura "/>
</div>
<div class="lyrico-lyrics-wrapper">Adige Peddollu Leruraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adige Peddollu Leruraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Dorelesay Thappuleduraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorelesay Thappuleduraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Dulipeste Dummu Poddiraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dulipeste Dummu Poddiraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Akasam Andee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akasam Andee "/>
</div>
<div class="lyrico-lyrics-wrapper">Avakasam Chetikuntee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avakasam Chetikuntee "/>
</div>
<div class="lyrico-lyrics-wrapper">Inka Ee Nela Painee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka Ee Nela Painee "/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Mulanundi Gollu Gillukokuraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Mulanundi Gollu Gillukokuraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Pongindi Naalo Ullasam Ullasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Pongindi Naalo Ullasam Ullasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Valai Allindi Nanne Santhosham Santhosham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valai Allindi Nanne Santhosham Santhosham "/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Pongindi Naalo Ullasam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Pongindi Naalo Ullasam "/>
</div>
<div class="lyrico-lyrics-wrapper">Valai Allindi Nanne Santhosham Santhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valai Allindi Nanne Santhosham Santhosham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veligincha Kotha Navvuni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veligincha Kotha Navvuni "/>
</div>
<div class="lyrico-lyrics-wrapper">Velivesa Patha Maatani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velivesa Patha Maatani "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidilinche Hayi Rekkani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidilinche Hayi Rekkani "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidichesa Chittigootini 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidichesa Chittigootini "/>
</div>
<div class="lyrico-lyrics-wrapper">Snehamtho Ninde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Snehamtho Ninde "/>
</div>
<div class="lyrico-lyrics-wrapper">Lokamlo Putta Malli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokamlo Putta Malli "/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandham Lo Munche 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandham Lo Munche "/>
</div>
<div class="lyrico-lyrics-wrapper">Sandramlo Ooda Laga Teelana Mari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandramlo Ooda Laga Teelana Mari "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai Pongindi Naalo Ullasam Ullasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Pongindi Naalo Ullasam Ullasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Valai Allindi Nanne Santhosham Santhosham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valai Allindi Nanne Santhosham Santhosham "/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Pongindi Naalo Ullasam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Pongindi Naalo Ullasam "/>
</div>
<div class="lyrico-lyrics-wrapper">Valai Allindi Nanne Santhosham Santhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valai Allindi Nanne Santhosham Santhosham"/>
</div>
</pre>
