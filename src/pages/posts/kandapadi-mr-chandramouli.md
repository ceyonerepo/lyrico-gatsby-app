---
title: "kandapadi song lyrics"
album: "Mr Chandramouli"
artist: "Sam CS"
lyricist: "Logan"
director: "Thiru"
path: "/albums/mr-chandramouli-lyrics"
song: "Kandapadi"
image: ../../images/albumart/mr-chandramouli.jpg
date: 2018-07-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ri-Y5H3cm5M"
type: "happy"
singers:
  - Sam CS
  - Guna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaana naa naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana naa naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana na nana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana na nana naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandapadi kandapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandapadi kandapadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkam enna killuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkam enna killuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothi pothi vecha enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothi pothi vecha enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Laavagama nee thinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laavagama nee thinna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkaraiyaa seendi puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkaraiyaa seendi puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakkanaiya pora pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakkanaiya pora pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu vittu pona pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu vittu pona pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukulla nee ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulla nee ninna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekkachakka aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkachakka aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonduthu en meesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonduthu en meesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga enna vittutu nee pora pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga enna vittutu nee pora pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pathi pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pathi pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa oru baashai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa oru baashai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathula neeyum vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathula neeyum vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikiren vedikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikiren vedikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhikiren sirikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhikiren sirikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cherry pazhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherry pazhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevakkum udhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevakkum udhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paakum pothu enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paakum pothu enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyilil kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyilil kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuliru thoonam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuliru thoonam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nizhala kadakum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nizhala kadakum pothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rayilu thadama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayilu thadama"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisi varaikum poga venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisi varaikum poga venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuru piriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuru piriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum venum hmm mmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum venum hmm mmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandapadi kandapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandapadi kandapadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkam enna killuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkam enna killuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothi pothi vecha enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothi pothi vecha enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Laavagama nee thinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laavagama nee thinna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkaraiyaa seendi puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkaraiyaa seendi puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakkanaiya pora pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakkanaiya pora pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu vittu pona pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu vittu pona pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla nee ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla nee ninna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinusu dhinusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinusu dhinusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil varuva nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil varuva nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thedi thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thedi thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu kalanju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu kalanju"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthu paathathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthu paathathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambil siragu kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambil siragu kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavithai ezhutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithai ezhutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthai varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthai varuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuthu varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuthu varuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthusu puthusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthusu puthusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai tharuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai tharuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuthu tharuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuthu tharuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu mulusa unnai thooki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu mulusa unnai thooki"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakka thudikum mm mmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakka thudikum mm mmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandapadi kandapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandapadi kandapadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkam enna killuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkam enna killuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothi pothi vecha enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothi pothi vecha enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Laavagama nee thinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laavagama nee thinna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkaraiyaa seendi puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkaraiyaa seendi puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vakkanaiya pora pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakkanaiya pora pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu vittu pona pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu vittu pona pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla nee ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla nee ninna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekkachakka aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkachakka aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonduthu en meesai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonduthu en meesai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga enna vittutu nee pora pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga enna vittutu nee pora pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pathi pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pathi pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa oru baashai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa oru baashai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkathula neeyum vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathula neeyum vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikiren vedikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikiren vedikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhikiren sirikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhikiren sirikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaana naa naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana naa naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana na nana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana na nana naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana naa naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana naa naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana na nana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana na nana naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana na nana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana na nana naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana na nana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana na nana naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana na nana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana na nana naa"/>
</div>
</pre>
