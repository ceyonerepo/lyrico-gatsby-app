---
title: "ye kannulu song lyrics"
album: "Ardha Shathabdam"
artist: "Nawfal Raja AIS"
lyricist: "Rahman"
director: "Rawindra Pulle"
path: "/albums/ardha-shathabdam-lyrics"
song: "Ye Kannulu Choodani"
image: ../../images/albumart/ardha-shathabdam.jpg
date: 2021-03-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/53F2H1a1Iv8"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye kannulu chudani chithrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kannulu chudani chithrame"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthunnadhi nedu naa praname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthunnadhi nedu naa praname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye kannulu chudani chithrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kannulu chudani chithrame"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthunnadhi nedu naa praname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthunnadhi nedu naa praname"/>
</div>
<div class="lyrico-lyrics-wrapper">Okate kshaname premane swaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okate kshaname premane swaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhalo manamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalo manamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhigeti nuvvane varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhigeti nuvvane varam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhuke ee nela navvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhuke ee nela navvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolu poosele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolu poosele"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalulanni ninnu thaaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalulanni ninnu thaaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandhamaayale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandhamaayale"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhamaina oohalenno oosuladele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhamaina oohalenno oosuladele"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthuleni sambaraana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthuleni sambaraana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyaloopele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyaloopele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye kannulu chudani chithrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kannulu chudani chithrame"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthunnadhi nedu naa praname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthunnadhi nedu naa praname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha dhachukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha dhachukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongi pothu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongi pothu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha aasalenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha aasalenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitti gundelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti gundelona"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaarikaasthu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaarikaasthu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu choosthu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu choosthu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu choodagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu choodagane"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaagipothu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaagipothu unna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnu thalachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu thalachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravasami parugulane theese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravasami parugulane theese"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa manasu o velluvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa manasu o velluvala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana lolona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana lolona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhuke ee nela navvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhuke ee nela navvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolu poosele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolu poosele"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalulanni ninnu thaaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalulanni ninnu thaaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandhamaayale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandhamaayale"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhamaina oohalenno oosuladele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhamaina oohalenno oosuladele"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthuleni sambaraana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthuleni sambaraana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyaloopele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyaloopele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye kannulu chudani chithrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kannulu chudani chithrame"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthunnadhi nedu naa praname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthunnadhi nedu naa praname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaahh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaahh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranguladhukunna sandhepodhulaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranguladhukunna sandhepodhulaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu navvuthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu navvuthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhivvelendhukantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhivvelendhukantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Reppaleyakunda rendu kallaninda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppaleyakunda rendu kallaninda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindu punnamalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindu punnamalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu nimpukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu nimpukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevarikidhi teliyadule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevarikidhi teliyadule"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukidhi madhuramule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukidhi madhuramule"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo ne murise o vekuvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo ne murise o vekuvala"/>
</div>
<div class="lyrico-lyrics-wrapper">Velugai unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velugai unna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhuke ee nela navvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhuke ee nela navvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolu poosele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolu poosele"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalulanni ninnu thaaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalulanni ninnu thaaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandhamaayale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandhamaayale"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhamaina oohalenno oosuladele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhamaina oohalenno oosuladele"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthuleni sambaraana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthuleni sambaraana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyaloopele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyaloopele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye kannulu chudani chithrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kannulu chudani chithrame"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthunnadhi nedu naa praname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthunnadhi nedu naa praname"/>
</div>
</pre>
