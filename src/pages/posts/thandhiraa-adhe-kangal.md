---
title: "thandhiraa song lyrics"
album: "Adhe Kangal"
artist: "Ghibran"
lyricist: "S N Anuradha"
director: "Rohin Venkatesan"
path: "/albums/adhe-kangal-lyrics"
song: "Thandhiraa"
image: ../../images/albumart/adhe-kangal.jpg
date: 2017-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bJY05rgzoQE"
type: "love"
singers:
  - Rajan Chelliah
  - Sree Ganesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adiye nee kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye nee kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutti kaatteri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti kaatteri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaadi dhegathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi dhegathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattaaru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaaru nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiye nee kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye nee kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutti kaatteri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti kaatteri"/>
</div>
<div class="lyrico-lyrics-wrapper">Alattaamal vilayaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alattaamal vilayaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Killaadi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killaadi nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karu vandu kannukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karu vandu kannukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vandi poithaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vandi poithaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Amul baby makeup-il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amul baby makeup-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Anakonda nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anakonda nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandhiraa thandhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhiraa thandhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhiraa thandhiraa thandhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhiraa thandhiraa thandhiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaana sadhigaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana sadhigaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangaa pidaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangaa pidaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Sidharaamal pandhaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidharaamal pandhaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru marmam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru marmam nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhagaana sadhigaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhagaana sadhigaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangaa pidaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangaa pidaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Barbie doll getup-il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barbie doll getup-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachchondhi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachchondhi nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasu tholil pulithaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasu tholil pulithaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Padiyaadha maanthaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padiyaadha maanthaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veli vesham ellaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli vesham ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Velicham aachae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham aachae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandhiraa thandhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhiraa thandhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhiraa thandhiraa thandhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhiraa thandhiraa thandhiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaana oru aabaththae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana oru aabaththae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagu vilagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagu vilagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinaiyum ivalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaiyum ivalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhi vegam pen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi vegam pen"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhithaanae puyalthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhithaanae puyalthaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiya kodiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiya kodiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Analum ivalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analum ivalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandhiraa thandhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhiraa thandhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhiraa thandhiraa thandhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhiraa thandhiraa thandhiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiye nee kalavaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye nee kalavaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutti kaatteri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti kaatteri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaadi dhegathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi dhegathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattaaru nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattaaru nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karu vandu kannukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karu vandu kannukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vandi poithaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vandi poithaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Amul baby makeup-il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amul baby makeup-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Anakonda nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anakonda nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandhiraa thandhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhiraa thandhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhiraa thandhiraa thandhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhiraa thandhiraa thandhiraa"/>
</div>
</pre>
