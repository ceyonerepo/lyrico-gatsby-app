---
title: "chatu matu chupulanni song lyrics"
album: "Manasuku Nachindi"
artist: "Radhan"
lyricist: "Ananta Sriram"
director: "Manjula Ghattamaneni"
path: "/albums/manasuku-nachindi-lyrics"
song: "Chatu Matu Chupulanni"
image: ../../images/albumart/manasuku-nachindi.jpg
date: 2018-02-16
lang: telugu
youtubeLink: "https://www.youtube.com/embed/1yXN6nqdy2U"
type: "happy"
singers:
  -	Srinivas Raghunathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chatumatu Chupulanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chatumatu Chupulanni "/>
</div>
<div class="lyrico-lyrics-wrapper">Leneleedu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leneleedu "/>
</div>
<div class="lyrico-lyrics-wrapper">Atapaata Chindulanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atapaata Chindulanni "/>
</div>
<div class="lyrico-lyrics-wrapper">Petti Chuudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petti Chuudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Andamaina Rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andamaina Rani"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyavela Boni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyavela Boni "/>
</div>
<div class="lyrico-lyrics-wrapper">Akatayi Paapalanni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akatayi Paapalanni "/>
</div>
<div class="lyrico-lyrics-wrapper">Rechhipoyenee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rechhipoyenee "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chil Chil Chil Chilling 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chil Chil Chil Chilling "/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Weather Come Edar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Weather Come Edar "/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Ha Ha Just 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Ha Ha Just "/>
</div>
<div class="lyrico-lyrics-wrapper">Like A Feather Humsafar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like A Feather Humsafar "/>
</div>
<div class="lyrico-lyrics-wrapper">Neela Mida Kaalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela Mida Kaalu "/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyi Undakudaadoyi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyi Undakudaadoyi "/>
</div>
<div class="lyrico-lyrics-wrapper">Galilona Telipoyi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galilona Telipoyi "/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi Padavoyi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Padavoyi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ashaalotulo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ashaalotulo "/>
</div>
<div class="lyrico-lyrics-wrapper">Hushaarendukoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hushaarendukoo "/>
</div>
<div class="lyrico-lyrics-wrapper">Vesukoo Vesukoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesukoo Vesukoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalilo Thelipoo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalilo Thelipoo "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jarigededoo Thelusa Ledo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarigededoo Thelusa Ledo "/>
</div>
<div class="lyrico-lyrics-wrapper">Maimarchindemo Praanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maimarchindemo Praanam "/>
</div>
<div class="lyrico-lyrics-wrapper">Emouthunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emouthunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Nilo Unna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilo Unna "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Manasuku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Manasuku "/>
</div>
<div class="lyrico-lyrics-wrapper">Thelisu Ee Kshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelisu Ee Kshanam"/>
</div>
</pre>
