---
title: "dil kyun yeh mere song lyrics"
album: "Kites"
artist: "Rajesh Roshan"
lyricist: "Nasir Faraaz"
director: "Anurag Basu"
path: "/albums/kites-lyrics"
song: "Dil Kyun Yeh Mere"
image: ../../images/albumart/kites.jpg
date: 2010-05-21
lang: hindi
youtubeLink: "https://www.youtube.com/embed/maKDIvUVkQo"
type: "love"
singers:
  - K.K
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">dil kyu yeh mera shor kare, dil kyu yeh mera shor kare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dil kyu yeh mera shor kare, dil kyu yeh mera shor kare"/>
</div>
<div class="lyrico-lyrics-wrapper">idhar nahi udhar nahi teri ore chale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhar nahi udhar nahi teri ore chale"/>
</div>
<div class="lyrico-lyrics-wrapper">dil kyu yeh mera shor kare, dil kyu yeh mera shor kare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dil kyu yeh mera shor kare, dil kyu yeh mera shor kare"/>
</div>
<div class="lyrico-lyrics-wrapper">idhar nahi udhar nahi teri ore chale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhar nahi udhar nahi teri ore chale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">zara der mein yeh kya ho gaya nazar milte hi kaha kho gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zara der mein yeh kya ho gaya nazar milte hi kaha kho gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">zara der mein yeh kya ho gaya nazar milte hi kaha kho gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="zara der mein yeh kya ho gaya nazar milte hi kaha kho gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">bheed mein logo ki woh hai waha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bheed mein logo ki woh hai waha"/>
</div>
<div class="lyrico-lyrics-wrapper">aur pyaar ke mele mein akela itna hu main yaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aur pyaar ke mele mein akela itna hu main yaha"/>
</div>
<div class="lyrico-lyrics-wrapper">dil kyu yeh mera shor kare, dil kyu yeh mera shor kare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dil kyu yeh mera shor kare, dil kyu yeh mera shor kare"/>
</div>
<div class="lyrico-lyrics-wrapper">idhar nahi udhar nahi teri ore chale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhar nahi udhar nahi teri ore chale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">shuru ho gayi kahani meri mere dil ne baat na maani meri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shuru ho gayi kahani meri mere dil ne baat na maani meri"/>
</div>
<div class="lyrico-lyrics-wrapper">shuru ho gayi kahani meri mere dil ne baat na maani meri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shuru ho gayi kahani meri mere dil ne baat na maani meri"/>
</div>
<div class="lyrico-lyrics-wrapper">hadd se bhi aagey yeh guzar hi gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hadd se bhi aagey yeh guzar hi gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">khud bhi pareshaan hua mujhko bhi yeh kar gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khud bhi pareshaan hua mujhko bhi yeh kar gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">dil kyu yeh mera shor kare, dil kyu yeh mera shor kare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dil kyu yeh mera shor kare, dil kyu yeh mera shor kare"/>
</div>
<div class="lyrico-lyrics-wrapper">idhar nahi udhar nahi teri ore chale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhar nahi udhar nahi teri ore chale"/>
</div>
</pre>
