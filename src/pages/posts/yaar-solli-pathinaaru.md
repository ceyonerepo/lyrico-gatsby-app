---
title: "yaar solli song lyrics"
album: "Pathinaaru"
artist: "Yuvan Shankar Raja"
lyricist: "Snehan"
director: "Sabapathy - Dekshinamurthy"
path: "/albums/pathinaaru-lyrics"
song: "Yaar Solli"
image: ../../images/albumart/pathinaaru.jpg
date: 2011-01-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/axQ0SsHqHyk"
type: "sad"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaar Solli Kaathal Varuvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Solli Kaathal Varuvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Solli Kaathal Povathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Solli Kaathal Povathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaruku Adimai Intha Kaathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaruku Adimai Intha Kaathal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aen Intha Kalam Nagaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aen Intha Kalam Nagaruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aen Intha Kaathal Thagaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aen Intha Kaathal Thagaruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aen Intha Maarupatta Thaedal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aen Intha Maarupatta Thaedal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithayangal Ilayum Tharunam Therinthal Solvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithayangal Ilayum Tharunam Therinthal Solvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaimoodi Irunthaaley Velicham Varuma Solvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaimoodi Irunthaaley Velicham Varuma Solvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Muzhuka Kaathal Iruka Engu Odi Olikirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Muzhuka Kaathal Iruka Engu Odi Olikirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Thanda Vazhiye Illai Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Thanda Vazhiye Illai Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Ingae Thavaru Endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Ingae Thavaru Endral"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul Kooda Thavaruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Kooda Thavaruthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Indri Kadavul Illai Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Indri Kadavul Illai Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Nee Aaen Vathaikirai Karanangal Theriyamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Nee Aaen Vathaikirai Karanangal Theriyamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Thanae Meendum Unnai Meetu Edukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Thanae Meendum Unnai Meetu Edukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Nee Aaen Verukirai En Nilai Puriyamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nee Aaen Verukirai En Nilai Puriyamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Unnai Mounamaga Azhugavaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Unnai Mounamaga Azhugavaikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaethi Poal Kaathalai Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaethi Poal Kaathalai Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilithuvida Mudiyaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilithuvida Mudiyaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Agaayathai Ullangaiyil Maraithu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agaayathai Ullangaiyil Maraithu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaika Mudiyaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaika Mudiyaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathaluku Maatru Ethuvum Illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathaluku Maatru Ethuvum Illayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadai Pola Kalati Poda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadai Pola Kalati Poda "/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyavillai Unnai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyavillai Unnai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai Pola Ennakul Ullai Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Pola Ennakul Ullai Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Meeri Unnai Ethuvum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Meeri Unnai Ethuvum "/>
</div>
<div class="lyrico-lyrics-wrapper">Seithidathu Kaathal Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seithidathu Kaathal Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathalaithaan Nambukinraen Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathalaithaan Nambukinraen Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithayathil Nee Kaathalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithayathil Nee Kaathalai "/>
</div>
<div class="lyrico-lyrics-wrapper">Pooti Vaika Mudiyaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooti Vaika Mudiyaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Savi Enathu Karathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savi Enathu Karathil "/>
</div>
<div class="lyrico-lyrics-wrapper">Iruku Purinthu Kolvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruku Purinthu Kolvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vai Vazhi Nee Ennai Thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vai Vazhi Nee Ennai Thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaendam Endru Sonnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaendam Endru Sonnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Orunaal Unthan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Orunaal Unthan "/>
</div>
<div class="lyrico-lyrics-wrapper">Manamae Kondru Vidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamae Kondru Vidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neyum Nanum Saernthey Seithoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neyum Nanum Saernthey Seithoam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Ennum Sirpathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Ennum Sirpathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirpam Vendam Endre Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirpam Vendam Endre Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodanginai Yuththathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodanginai Yuththathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Enna Nyayam Neeyae Soladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Enna Nyayam Neeyae Soladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum 100 Thalaimuraigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum 100 Thalaimuraigal "/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Mannil Vazhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Mannil Vazhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Andrum Intha Kaathal Irukum Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andrum Intha Kaathal Irukum Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirgal Janitha Nodiyil Irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirgal Janitha Nodiyil Irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Ingae Vazhuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Ingae Vazhuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Indri Uyirgal Aethu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Indri Uyirgal Aethu Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Solli Kaathal Varuvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Solli Kaathal Varuvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Solli Kaathal Povathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Solli Kaathal Povathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaruku Adimai Intha Kaathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaruku Adimai Intha Kaathal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aen Intha Kalam Nagaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aen Intha Kalam Nagaruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aen Intha Kaathal Thagaruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aen Intha Kaathal Thagaruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aen Intha Maarupatta Thaedal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aen Intha Maarupatta Thaedal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithayangal Ilayum Tharunam Therinthal Solvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithayangal Ilayum Tharunam Therinthal Solvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaimoodi Irunthaaley Velicham Varuma Solvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaimoodi Irunthaaley Velicham Varuma Solvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Muzhuka Kaathal Iruka Engu Odi Olikirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Muzhuka Kaathal Iruka Engu Odi Olikirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Thanda Vazhiye Illai Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Thanda Vazhiye Illai Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Ingae Thavaru Endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Ingae Thavaru Endral"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavul Kooda Thavaruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Kooda Thavaruthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathal Indri Kadavul Illai Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathal Indri Kadavul Illai Vaa"/>
</div>
</pre>
