---
title: "nee raaka kosam song lyrics"
album: "Yatra"
artist: "K"
lyricist: "Sirivennela Sitaramasastri"
director: "Mahi V Raghav"
path: "/albums/yatra-lyrics"
song: "Nee Raaka Kosam"
image: ../../images/albumart/yatra.jpg
date: 2019-02-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/bJLIRnH6rUA"
type: "happy"
singers:
  - Shankar Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee raaka kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee raaka kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">vethike choopulouthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethike choopulouthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">maa poddu podupaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa poddu podupaa"/>
</div>
<div class="lyrico-lyrics-wrapper">jayahoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jayahoo"/>
</div>
<div class="lyrico-lyrics-wrapper">nee venta nithyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee venta nithyam"/>
</div>
<div class="lyrico-lyrics-wrapper">nadiche sainyamouthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadiche sainyamouthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">maa gelupu malupaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa gelupu malupaa"/>
</div>
<div class="lyrico-lyrics-wrapper">jayahoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jayahoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">evaruu lerani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaruu lerani"/>
</div>
<div class="lyrico-lyrics-wrapper">evaruu raarani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaruu raarani"/>
</div>
<div class="lyrico-lyrics-wrapper">nammisthunna maa talaraatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammisthunna maa talaraatha"/>
</div>
<div class="lyrico-lyrics-wrapper">thaane thappani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaane thappani"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvunnaavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvunnaavani"/>
</div>
<div class="lyrico-lyrics-wrapper">satyam nammani adhinethaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satyam nammani adhinethaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee raaka kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee raaka kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">vethike choopulouthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethike choopulouthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">maa poddu podupaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa poddu podupaa"/>
</div>
<div class="lyrico-lyrics-wrapper">jayahoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jayahoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maa gadapalonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa gadapalonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kiranam okkatainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiranam okkatainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaloolanede eppudainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaloolanede eppudainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">maa gundelonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa gundelonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">gudiloo deepamainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gudiloo deepamainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee gurthupode ikapainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee gurthupode ikapainaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">madame thippani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madame thippani"/>
</div>
<div class="lyrico-lyrics-wrapper">mahaa yathragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mahaa yathragaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mundhadugaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mundhadugaina"/>
</div>
<div class="lyrico-lyrics-wrapper">oh jananethaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh jananethaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">modaipoinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modaipoinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">maalo aashaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalo aashaki"/>
</div>
<div class="lyrico-lyrics-wrapper">maarakaindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarakaindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">nee cheyuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee cheyuthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee raaka kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee raaka kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">vethike choopulouthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethike choopulouthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">maa poddu podupaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa poddu podupaa"/>
</div>
<div class="lyrico-lyrics-wrapper">jayahoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jayahoo"/>
</div>
<div class="lyrico-lyrics-wrapper">nee venta nithyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee venta nithyam"/>
</div>
<div class="lyrico-lyrics-wrapper">nadiche sainyamouthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadiche sainyamouthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">maa gelupu malupaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa gelupu malupaa"/>
</div>
<div class="lyrico-lyrics-wrapper">jayahoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jayahoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">evaruu lerani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaruu lerani"/>
</div>
<div class="lyrico-lyrics-wrapper">evaruu raarani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaruu raarani"/>
</div>
<div class="lyrico-lyrics-wrapper">nammisthunna maa talaraatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nammisthunna maa talaraatha"/>
</div>
<div class="lyrico-lyrics-wrapper">thaane thappani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaane thappani"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvunnaavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvunnaavani"/>
</div>
<div class="lyrico-lyrics-wrapper">satyam nammani adhinethaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satyam nammani adhinethaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee raaka kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee raaka kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">vethike choopulouthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethike choopulouthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">maa poddu podupaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa poddu podupaa"/>
</div>
<div class="lyrico-lyrics-wrapper">jayahoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jayahoo"/>
</div>
</pre>
