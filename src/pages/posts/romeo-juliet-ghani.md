---
title: "romeo juliet song lyrics"
album: "Ghani"
artist: "S. Thaman"
lyricist: "Raghuram"
director: "Kiran Korrapati"
path: "/albums/ghani-lyrics"
song: "Romeo Juliet"
image: ../../images/albumart/ghani.jpg
date: 2021-12-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/O5GsA-qdgAQ"
type: "happy"
singers:
  - Aditi Shankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pie Pie Sweetie Pie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pie Pie Sweetie Pie"/>
</div>
<div class="lyrico-lyrics-wrapper">Romeo Ki Juliet Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romeo Ki Juliet Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Radio Ki Satellite Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radio Ki Satellite Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lab Dab Heart Beat Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lab Dab Heart Beat Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Undiponaa Neeku Nenilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undiponaa Neeku Nenilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Choopulemo Chocolate Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopulemo Chocolate Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvulemo Magnet Laa Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvulemo Magnet Laa Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachhinaavu Anni Velalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachhinaavu Anni Velalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Masthugunna Chandamamalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masthugunna Chandamamalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Newton Cheppina Soothramedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Newton Cheppina Soothramedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundene Laagena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundene Laagena"/>
</div>
<div class="lyrico-lyrics-wrapper">U-Turn Thirige Needalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U-Turn Thirige Needalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ventane Saaganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ventane Saaganaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veturilaa Nandurilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veturilaa Nandurilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varninchamante Neepai Preme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varninchamante Neepai Preme"/>
</div>
<div class="lyrico-lyrics-wrapper">Baashalanni Chaalave Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baashalanni Chaalave Mari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romeo Ki Juliet Laa Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romeo Ki Juliet Laa Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Radio Ki Satellite Laa Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Radio Ki Satellite Laa Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lab Dab Heart Beat Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lab Dab Heart Beat Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Undiponaa Neeku Nenilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undiponaa Neeku Nenilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Meghame Vaanalaa Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Meghame Vaanalaa Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kosame Cheragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kosame Cheragaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandame Adugule Vesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandame Adugule Vesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Sonthame Avvagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Sonthame Avvagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppudaina Naaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudaina Naaku "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Ninna Dhaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Ninna Dhaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachhanaina Nachhaledhu Inthalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachhanaina Nachhaledhu Inthalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopire Ooyalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopire Ooyalai "/>
</div>
<div class="lyrico-lyrics-wrapper">Ooguthundhi Unnapaatugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooguthundhi Unnapaatugaa"/>
</div>
</pre>
