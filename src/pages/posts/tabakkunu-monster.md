---
title: "tabakkunu song lyrics"
album: "Monster"
artist: "Justin Prabhakaran"
lyricist: "Yugabharathi"
director: "Nelson Venkatesan"
path: "/albums/monster-lyrics"
song: "Tabakkunu"
image: ../../images/albumart/monster.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/W8NV5wdBofE"
type: "happy"
singers:
  - Offir J. Rock
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tabakkunu Thaavi Thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tabakkunu Thaavi Thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Keezha Paanju Paanju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Keezha Paanju Paanju"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaakuvome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakuvome"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Labakkunu Mondhu Mondhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labakkunu Mondhu Mondhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam Veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetta Naanga Thedi Poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetta Naanga Thedi Poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkuvome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkuvome"/>
</div>
<div class="lyrico-lyrics-wrapper">Saraak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasi Thaangatha Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Thaangatha Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedham Samaikkaadha Koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedham Samaikkaadha Koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhanjoraana Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhanjoraana Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusi Paakaama Vida Maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusi Paakaama Vida Maattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valuuu Sema Valuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valuuu Sema Valuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaadum Kadha Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadum Kadha Kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Reelu Pudhu Reelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reelu Pudhu Reelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Odum Pala Naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Odum Pala Naalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pipe Pu Mela Yeruvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pipe Pu Mela Yeruvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Paper Plate Ta Keeruvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paper Plate Ta Keeruvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Spidermanu Vamsam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spidermanu Vamsam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Late Tu Nightu Kooduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Late Tu Nightu Kooduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Steppu Pottu Aaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Steppu Pottu Aaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Micheal Jackson Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micheal Jackson Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Amsam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Amsam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjara Pettikulla Poondhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjara Pettikulla Poondhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lavanga Pattaigala Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lavanga Pattaigala Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lavatta Nikkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lavatta Nikkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Cooker Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Cooker Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dishyum Dishyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dishyum Dishyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakkaram Suththaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkaram Suththaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakku Vandiga Mathiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakku Vandiga Mathiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuzhanju Thaavura Kattathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuzhanju Thaavura Kattathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Ore Bhayam Meow Meow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Ore Bhayam Meow Meow"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiruttu Thittam Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruttu Thittam Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">GST Illa Adhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="GST Illa Adhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanga Senjidum Unga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanga Senjidum Unga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattam Sattam Sattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattam Sattam Sattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhukkum Anjiyathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhukkum Anjiyathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaridamum Kenjiyathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaridamum Kenjiyathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Engala Kolludhu Unga System
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engala Kolludhu Unga System"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varapporennu Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varapporennu Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli Verupeththa Mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Verupeththa Mattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum Pechaave Pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Pechaave Pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vada Vaayala Suda Mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vada Vaayala Suda Mattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valuuu Sema Valuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valuuu Sema Valuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaadum Kadha Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadum Kadha Kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Reelu… Pudhu Reelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reelu… Pudhu Reelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Odum Pala Naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Odum Pala Naalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tabakkunu Thaavi Thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tabakkunu Thaavi Thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Keezha Paanju Paanju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Keezha Paanju Paanju"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaakuvome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakuvome"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaak"/>
</div>
<div class="lyrico-lyrics-wrapper">Labakkunu Mondhu Mondhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labakkunu Mondhu Mondhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam Veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetta Naanga Thedi Poyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetta Naanga Thedi Poyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkuvome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkuvome"/>
</div>
<div class="lyrico-lyrics-wrapper">Saraak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasi Thaangatha Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Thaangatha Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedham Samaikkaadha Koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedham Samaikkaadha Koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhanjoraana Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhanjoraana Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusi Paakaama Vida Maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusi Paakaama Vida Maattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valuuu Sema Valuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valuuu Sema Valuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaadum Kadha Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadum Kadha Kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Reelu… Pudhu Reelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reelu… Pudhu Reelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Odum Pala Naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Odum Pala Naalu"/>
</div>
</pre>
