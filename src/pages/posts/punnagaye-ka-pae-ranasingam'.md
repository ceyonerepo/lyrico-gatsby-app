---
title: "punnagaiye lyrics"
album: "Ka Pae Ranasingam"
artist: "Ghibran"
lyricist: "Vairamuthu"
director: "P Virumandi"
path: "/albums/ka-pae-ranasingam-lyrics"
song: "Punnagaiye"
image: ../../images/albumart/ka-pae-ranasingam.jpg
date: 2020-10-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YMSNYc-DoPI"
type: "motivational"
singers:
  - Sundarayyar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chrous  Punnagaiyae…..
<input type="checkbox" class="lyrico-select-lyric-line" value="Chrous  Punnagaiyae….."/>
</div>
<div class="lyrico-lyrics-wrapper">Siru poongkiliyae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Siru poongkiliyae…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chrous  Punnagaiyae puyalaai maarum podhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Chrous  Punnagaiyae puyalaai maarum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru poongkiliyae puliyaai neeyum maaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Siru poongkiliyae puliyaai neeyum maaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chrous  Puli nee….nee…
<input type="checkbox" class="lyrico-select-lyric-line" value="Chrous  Puli nee….nee…"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal nee….nee…
<input type="checkbox" class="lyrico-select-lyric-line" value="Puyal nee….nee…"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiya…..yaa….aa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidhiya…..yaa….aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhiya….yaa…aa
<input type="checkbox" class="lyrico-select-lyric-line" value="Sadhiya….yaa…aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chrous  Adhigaarathin meedhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Chrous  Adhigaarathin meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae neeyum modhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adiyae neeyum modhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru vaer muttum podhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Siru vaer muttum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumpaarai thaangaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Karumpaarai thaangaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chrous  Punnagaiyae puyalaai maarum podhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Chrous  Punnagaiyae puyalaai maarum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru poongkiliyae puliyaai neeyum maaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Siru poongkiliyae puliyaai neeyum maaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chrous  Aha….aaa….aa…aaha….ahaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Chrous  Aha….aaa….aa…aaha….ahaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha ahaa ahhaa haa….aaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aha ahaa ahhaa haa….aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha….aaa….aa…aaha….ahaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Aha….aaa….aa…aaha….ahaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha ahaa ahhaa haa….aaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aha ahaa ahhaa haa….aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa….aaa…aa….aa….aa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaa….aaa…aa….aa….aa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thunai onnu illamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thunai onnu illamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa usuru alai modhuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oththa usuru alai modhuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eriyura kaatukullae
<input type="checkbox" class="lyrico-select-lyric-line" value="Eriyura kaatukullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru erumbu vazhitheduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru erumbu vazhitheduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pattikaattu ponnu sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pattikaattu ponnu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parliament-il ketkuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Parliament-il ketkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu singam muyala kaakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaattu singam muyala kaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaditham kodukkuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaditham kodukkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kanji potta kaakki sattai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanji potta kaakki sattai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaithariya paakkuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaithariya paakkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Velu naachi vamsakkaari
<input type="checkbox" class="lyrico-select-lyric-line" value="Velu naachi vamsakkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeram poguma….
<input type="checkbox" class="lyrico-select-lyric-line" value="Veeram poguma…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thuninja thee idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thuninja thee idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuninju eriyuma…
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuninju eriyuma…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Punnagaiyae puyalaai maarum podhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Punnagaiyae puyalaai maarum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru poongkiliyae puliyaai neeyum maaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Siru poongkiliyae puliyaai neeyum maaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chrous  Puli nee….nee…
<input type="checkbox" class="lyrico-select-lyric-line" value="Chrous  Puli nee….nee…"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal nee….nee…
<input type="checkbox" class="lyrico-select-lyric-line" value="Puyal nee….nee…"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiya…..yaa….aa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidhiya…..yaa….aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhiya….yaa…aa
<input type="checkbox" class="lyrico-select-lyric-line" value="Sadhiya….yaa…aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adhigaarathin meedhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhigaarathin meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae neeyum modhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adiyae neeyum modhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru vaer muttum podhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Siru vaer muttum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumpaarai thaangaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Karumpaarai thaangaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">and Chrous  Punnagaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="and Chrous  Punnagaiyae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Puyalaai maarum podhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Puyalaai maarum podhu"/>
</div>
 <div class="lyrico-lyrics-wrapper">and Chrous  Siru poongkiliyae
<input type="checkbox" class="lyrico-select-lyric-line" value="and Chrous  Siru poongkiliyae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Puliyaai neeyum maaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Puliyaai neeyum maaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ilakkai adaiyamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ilakkai adaiyamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival idhayam adangathadaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ival idhayam adangathadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilangu udaiyamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vilangu udaiyamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival valaiyal udaiyathadaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ival valaiyal udaiyathadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ivalum kooda Indhiya raththam
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivalum kooda Indhiya raththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arasamaippu solluthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Arasamaippu solluthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee erumbum vaazha thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ee erumbum vaazha thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi ulladhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhoomi ulladhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Arungampullum nanaiya thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Arungampullum nanaiya thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha megam peiyudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha megam peiyudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhudha jaadhi meela thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhudha jaadhi meela thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Arasu ulladhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Arasu ulladhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhudha pillaithaan paal kudikkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhudha pillaithaan paal kudikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chrous  Punnagaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Chrous  Punnagaiyae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Puyalaai maarum podhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Puyalaai maarum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chrous  Siru poongkiliyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Chrous  Siru poongkiliyae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Puliyaai neeyum maaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Puliyaai neeyum maaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">and Chrous  Puli nee….nee…
<input type="checkbox" class="lyrico-select-lyric-line" value="and Chrous  Puli nee….nee…"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal nee….nee…
<input type="checkbox" class="lyrico-select-lyric-line" value="Puyal nee….nee…"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhiya…..yaa….aa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidhiya…..yaa….aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhiya….yaa…aa
<input type="checkbox" class="lyrico-select-lyric-line" value="Sadhiya….yaa…aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adhigaarathin meedhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhigaarathin meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae neeyum modhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adiyae neeyum modhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru vaer muttum podhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Siru vaer muttum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karumpaarai thaangaadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Karumpaarai thaangaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">and Chrous  Punnagaiyae puyalaai maarum podhu
<input type="checkbox" class="lyrico-select-lyric-line" value="and Chrous  Punnagaiyae puyalaai maarum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru poongkiliyae puliyaai neeyum maaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Siru poongkiliyae puliyaai neeyum maaru"/>
</div>
</pre>
