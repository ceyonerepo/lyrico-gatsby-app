---
title: "cable raja song lyrics"
album: "Vaanam"
artist: "Yuvan Shankar Raja"
lyricist: "Abhishek - Lawrence"
director: "Krish"
path: "/albums/vaanam-lyrics"
song: "Cable Raja"
image: ../../images/albumart/vaanam.jpg
date: 2011-04-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lIfm_qjRySk"
type: "happy"
singers:
  - Abhishek
  - Lawrence
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kootam Podaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootam Podaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettai Inidhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettai Inidhaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatram Pudidhanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatram Pudidhanadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kootam Podaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootam Podaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettai Inidhaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettai Inidhaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatram Pudidhanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatram Pudidhanadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Vaada En Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vaada En Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhakka Bajji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhakka Bajji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onodamba Picchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onodamba Picchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poda Pora Bajji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda Pora Bajji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Vaada En Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vaada En Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalaka Bajji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalaka Bajji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onodamba Picchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onodamba Picchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poda Pora Bajji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda Pora Bajji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooo Vallavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo Vallavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Manmadhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Manmadhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Kettavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Kettavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Vaaliban
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Vaaliban"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Vs Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Vs Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Vs Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Vs Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Vs Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Vs Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Cable Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Cable Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Vs Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Vs Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Vs Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Vs Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Vs Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Vs Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Cable Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Cable Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetti Puduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti Puduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Ketta Payanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Ketta Payanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patti Thoti Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti Thoti Ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Chella Payanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Chella Payanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandha Vaayila Vacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Vaayila Vacchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thandha Odamba Vacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandha Odamba Vacchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seta Verala Vecchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seta Verala Vecchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetta Vaalanga Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetta Vaalanga Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erraki Kuthey Egiri Kuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erraki Kuthey Egiri Kuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethiril Nikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiril Nikkaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala Thookaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Thookaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thazhaiku Pidikaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhaiku Pidikaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vambula Nikkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambula Nikkaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erraki Kuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erraki Kuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Vaada En Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vaada En Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalakka Bajji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalakka Bajji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onodamba Picchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onodamba Picchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poda Pora Bajji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda Pora Bajji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Vaada En Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vaada En Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhakka Bajji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhakka Bajji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onodamba Picchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onodamba Picchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poda Pora Bajji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda Pora Bajji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bajji Bajji Bajji Bajji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajji Bajji Bajji Bajji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vada Vada Vada Vadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vada Vada Vada Vadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Vaada En Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vaada En Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhakka Bajji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhakka Bajji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Odamba Picchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Odamba Picchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pichi Pichi Pichi Pichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichi Pichi Pichi Pichi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Pichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Pichi"/>
</div>
</pre>
