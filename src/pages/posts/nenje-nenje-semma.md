---
title: "nenje nenje song lyrics"
album: "Semma"
artist: "G.V. Prakash Kumar"
lyricist: "Yugabharathi"
director: "Vallikanthan"
path: "/albums/semma-lyrics"
song: "Nenje Nenje"
image: ../../images/albumart/semma.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dizWd_ga6Kk"
type: "love"
singers:
  - Siddharth Mahadevan
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nenje Nenje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenje Nenje"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkama Odudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkama Odudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Enge Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Enge Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Endre Thedudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endre Thedudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unapaathe Na Eraganen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unapaathe Na Eraganen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalakaalu Puriyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalakaalu Puriyama"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenje Nenje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenje Nenje"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkama Odudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkama Odudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Enge Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Enge Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Endre Thedudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endre Thedudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unapaathe Na Eraganen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unapaathe Na Eraganen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalakaalu Puriyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalakaalu Puriyama"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oore Mela Paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oore Mela Paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athayum Thaandi Dhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athayum Thaandi Dhane"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjankuzhi Medaakudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjankuzhi Medaakudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pechula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pechula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammavume Kaadakudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammavume Kaadakudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Veechila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Veechila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum Ketidaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Ketidaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral Poodum Megampola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooral Poodum Megampola"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Vaara Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Vaara Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendhu Vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendhu Vaazha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjaama Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaama Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponjaadhiya Uravaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponjaadhiya Uravaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaralayum Aagadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaralayum Aagadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadapoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadapoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenchirudee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenchirudee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oodha Kathumkuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodha Kathumkuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Modha Thaanguvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modha Thaanguvene"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammavayum Thaalattudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammavayum Thaalattudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaasana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaasana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathazhaiya Kannadiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathazhaiya Kannadiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Maathuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maathuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjal Pola Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal Pola Aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onjidama Aatampotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onjidama Aatampotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkam Thungidatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkam Thungidatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Na"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Nee Vilaiyaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Nee Vilaiyaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mupodhume Sandhoshamtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mupodhume Sandhoshamtham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadapoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadapoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna Thodu Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Thodu Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenje Nenje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenje Nenje"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkama Odudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkama Odudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Endre Thedudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Endre Thedudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unapaathe Na Eraganen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unapaathe Na Eraganen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalakaalu Puriyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalakaalu Puriyama"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthene"/>
</div>
</pre>
