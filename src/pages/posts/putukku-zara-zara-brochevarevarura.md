---
title: "putukku zara zara song lyrics"
album: "Brochevarevarura"
artist: "Vivek Sagar"
lyricist: "Hasith Goli"
director: "Vivek Athreya"
path: "/albums/brochevarevarura-lyrics"
song: "Putukku Zara Zara"
image: ../../images/albumart/brochevarevarura.jpg
date: 2019-06-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/HpnuumG5IE8"
type: "happy"
singers:
  - Anthony Daasan
  - Balaji Dake
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Daam Daam Daam Daam Debba Soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daam Daam Daam Daam Debba Soodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raju Gari Tupaki Soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raju Gari Tupaki Soodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aru Peddavi Pululu Soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aru Peddavi Pululu Soodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothu Pothu Poluge Palike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothu Pothu Poluge Palike"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaase Gaddilo Kamju Palikera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaase Gaddilo Kamju Palikera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooka Gaddilo Udumu Palikera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooka Gaddilo Udumu Palikera"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakshmarayudi Kota Mundara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakshmarayudi Kota Mundara"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannendu Metla Kinnera Palikera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannendu Metla Kinnera Palikera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Putukku Zara Zara Zaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putukku Zara Zara Zaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Tickettu Thoorupu Dikkele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Tickettu Thoorupu Dikkele"/>
</div>
<div class="lyrico-lyrics-wrapper">Putukku Zara Zara Zaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putukku Zara Zara Zaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Tickettu Thoorupu Dikkele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Tickettu Thoorupu Dikkele"/>
</div>
<div class="lyrico-lyrics-wrapper">Atekkithe Padamarale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atekkithe Padamarale"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Itongi Dandam Pettaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Itongi Dandam Pettaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Atekkithe Padamarale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atekkithe Padamarale"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Pettale Itu Pettale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Pettale Itu Pettale"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Vaada Maeda Goda Kaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Vaada Maeda Goda Kaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethakaale Arey Ethakaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethakaale Arey Ethakaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Vaada Veeda Theda Leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Vaada Veeda Theda Leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adagaale Ehe Adagaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagaale Ehe Adagaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponira Vadiley Ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponira Vadiley Ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaa Ginjukuntaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaa Ginjukuntaave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaneeraa Kanapadadhante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaneeraa Kanapadadhante"/>
</div>
<div class="lyrico-lyrics-wrapper">Maree Mondikesthave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maree Mondikesthave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Sinna Sesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Sinna Sesthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhu Gondhi Soosthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhu Gondhi Soosthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthu Sikkakunnadaa Mastaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthu Sikkakunnadaa Mastaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Anthe Sikkugunnadhe Mee Theeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Anthe Sikkugunnadhe Mee Theeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baajala Daabulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baajala Daabulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaja Vaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja Vaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Bejaarayyindi Le Meelo Joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bejaarayyindi Le Meelo Joru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa Bhoothaddle Etti Suttu Soodaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Bhoothaddle Etti Suttu Soodaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Timeu Geemulanni Lekkhe Eyyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Timeu Geemulanni Lekkhe Eyyale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Bhoothaddle Etti Suttu Soodaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Bhoothaddle Etti Suttu Soodaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Timeu Geemulanni Lekkhe Eyyale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Timeu Geemulanni Lekkhe Eyyale"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadharangam Raajugaadi Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadharangam Raajugaadi Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Gadiko Adugu Eyyabaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Gadiko Adugu Eyyabaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Onte Gurrale Ente Unnaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onte Gurrale Ente Unnaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Seri Jalledeyyalanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Seri Jalledeyyalanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Putukku Zara Zara Zaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putukku Zara Zara Zaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Tickettu Thoorupu Dikkele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Tickettu Thoorupu Dikkele"/>
</div>
<div class="lyrico-lyrics-wrapper">Putukku Zara Zara Zaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putukku Zara Zara Zaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Tickettu Thoorupu Dikkele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Tickettu Thoorupu Dikkele"/>
</div>
<div class="lyrico-lyrics-wrapper">Atekkithe Padamarale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atekkithe Padamarale"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Itongi Dandam Pettaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Itongi Dandam Pettaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Atekkithe Padamarale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Atekkithe Padamarale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikadandale Itu Pettale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikadandale Itu Pettale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baajala Daabulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baajala Daabulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaja Vaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja Vaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Bejaarayyindi Le Meelo Joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bejaarayyindi Le Meelo Joru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooo Baajala Daabulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo Baajala Daabulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaja Vaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja Vaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Bejaarayyindi Le Meelo Joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Bejaarayyindi Le Meelo Joru"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Poyindi Thelake Meelo Poru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Poyindi Thelake Meelo Poru"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi Kaadante Teeradhe Mee Kangaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi Kaadante Teeradhe Mee Kangaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Vaada Maeda Goda Kaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Vaada Maeda Goda Kaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethakaale Arey Ethakaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethakaale Arey Ethakaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Vaada Veeda Theda Leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Vaada Veeda Theda Leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adagaale Ehe Adagaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adagaale Ehe Adagaale"/>
</div>
</pre>
