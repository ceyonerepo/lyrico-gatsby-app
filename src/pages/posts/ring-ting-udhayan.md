---
title: "ring ting song lyrics"
album: "Udhayan"
artist: "Manikanth Kadri"
lyricist: "Vaali - Yugabharathi - Annamalai - Surya - Muthamil"
director: "Chaplin"
path: "/albums/udhayan-lyrics"
song: "Ring Ting"
image: ../../images/albumart/udhayan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZBchmyPNowo"
type: "happy"
singers:
  - Baba Sehgal
  - Suchitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey ring tring ring ring ting ting ting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ring tring ring ring ting ting ting"/>
</div>
<div class="lyrico-lyrics-wrapper">ring ting ringi tikkidi ting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ring ting ringi tikkidi ting"/>
</div>
<div class="lyrico-lyrics-wrapper">ring ting alley paappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ring ting alley paappa"/>
</div>
<div class="lyrico-lyrics-wrapper">ring ting alley kaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ring ting alley kaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaramaachinaa thanni kudinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaramaachinaa thanni kudinga"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayamaachina aaravaiyinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayamaachina aaravaiyinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaramaachinaa thanni kudinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaramaachinaa thanni kudinga"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayamaachina aaravaiyinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayamaachina aaravaiyinga"/>
</div>
<div class="lyrico-lyrics-wrapper">kappal maela thaan namma yerallaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kappal maela thaan namma yerallaam"/>
</div>
<div class="lyrico-lyrics-wrapper">naamma melathaan kappal yerumaa hOi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naamma melathaan kappal yerumaa hOi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalaiyiley odambu thechi kulichikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalaiyiley odambu thechi kulichikko"/>
</div>
<div class="lyrico-lyrics-wrapper">saalaiyiley poothirukkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saalaiyiley poothirukkum "/>
</div>
<div class="lyrico-lyrics-wrapper">natchathiram jolikkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natchathiram jolikkudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">tring tring tring
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tring tring tring"/>
</div>
<div class="lyrico-lyrics-wrapper">ailekkaa taappey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ailekkaa taappey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Michamillaa ponnazhagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Michamillaa ponnazhagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">panje varume kili onnu parakkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panje varume kili onnu parakkudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga vela ippo rumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga vela ippo rumba"/>
</div>
<div class="lyrico-lyrics-wrapper">thaavi thaavi yerikkittey irukkudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaavi thaavi yerikkittey irukkudhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaiya virichaa kaasu panam kottaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiya virichaa kaasu panam kottaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhukkum meengal vaathukku sikkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhukkum meengal vaathukku sikkaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal neendhum meenukkum thoondilgal ingundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal neendhum meenukkum thoondilgal ingundu"/>
</div>
<div class="lyrico-lyrics-wrapper">tring tring tring allaakkaa thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tring tring tring allaakkaa thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaramaachinaa thanni kudinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaramaachinaa thanni kudinga"/>
</div>
<div class="lyrico-lyrics-wrapper">kaayamaachina aaravaiyinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaayamaachina aaravaiyinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarkkaadhadha paarkkaadhadhapoal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkaadhadha paarkkaadhadhapoal"/>
</div>
<div class="lyrico-lyrics-wrapper">engalukku riskkuthara muzhikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalukku riskkuthara muzhikkidhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaarumey paarthidaadha sorkka boomi onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaarumey paarthidaadha sorkka boomi onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">suthikkittu irukkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthikkittu irukkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellai manasu vannamaaga maarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai manasu vannamaaga maarudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallathanamaa pinnaala Odudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallathanamaa pinnaala Odudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullaasa vaanathin uchikki poayittoam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaasa vaanathin uchikki poayittoam"/>
</div>
<div class="lyrico-lyrics-wrapper">tring tring tring allekkaathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tring tring tring allekkaathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey ring tring ring ring ting ting ting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ring tring ring ring ting ting ting"/>
</div>
<div class="lyrico-lyrics-wrapper">ring ting ringi tikkidi ting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ring ting ringi tikkidi ting"/>
</div>
<div class="lyrico-lyrics-wrapper">ring ting alley paappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ring ting alley paappa"/>
</div>
<div class="lyrico-lyrics-wrapper">ring ting alley kaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ring ting alley kaatha"/>
</div>
</pre>
