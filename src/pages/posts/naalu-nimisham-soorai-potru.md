---
title: "naalu nimisham song lyrics"
album: "Soorarai Pottru"
artist: "G V Prakash Kumar"
lyricist: "Maya Mahalingam"
director: "Sudha Kongara"
path: "/albums/soorarai-pottru-song-lyrics"
song: "Naalu Nimisham"
image: ../../images/albumart/soorarai-potru.jpg
date: 2020-10-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OrHYZ3q6dNw"
type: "sad"
singers:
  - Krishnaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">usurey
<input type="checkbox" class="lyrico-select-lyric-line" value="usurey"/>
</div>
<div class="lyrico-lyrics-wrapper">usurey
<input type="checkbox" class="lyrico-select-lyric-line" value="usurey"/>
</div>
<div class="lyrico-lyrics-wrapper">usurey
<input type="checkbox" class="lyrico-select-lyric-line" value="usurey"/>
</div>
<div class="lyrico-lyrics-wrapper">Minmini poochiya pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Minmini poochiya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee minnitu poriyaedi
<input type="checkbox" class="lyrico-select-lyric-line" value="nee minnitu poriyaedi"/>
</div>
<div class="lyrico-lyrics-wrapper">andha kaanangkuruviya pola
<input type="checkbox" class="lyrico-select-lyric-line" value="andha kaanangkuruviya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kaanaama poniyadi
<input type="checkbox" class="lyrico-select-lyric-line" value="nee kaanaama poniyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vepamarathu nizhala
<input type="checkbox" class="lyrico-select-lyric-line" value="vepamarathu nizhala"/>
</div>
<div class="lyrico-lyrics-wrapper">nee veroonda venumadi
<input type="checkbox" class="lyrico-select-lyric-line" value="nee veroonda venumadi"/>
</div>
<div class="lyrico-lyrics-wrapper">un nizhalin arumai therinchae
<input type="checkbox" class="lyrico-select-lyric-line" value="un nizhalin arumai therinchae"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thalai saaika venumadi
<input type="checkbox" class="lyrico-select-lyric-line" value="nan thalai saaika venumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">patharrai poi
<input type="checkbox" class="lyrico-select-lyric-line" value="patharrai poi"/>
</div>
<div class="lyrico-lyrics-wrapper">sarugaa aachu
<input type="checkbox" class="lyrico-select-lyric-line" value="sarugaa aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">enn manasu
<input type="checkbox" class="lyrico-select-lyric-line" value="enn manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee oda neera
<input type="checkbox" class="lyrico-select-lyric-line" value="nee oda neera"/>
</div>
<div class="lyrico-lyrics-wrapper">odi vandhu
<input type="checkbox" class="lyrico-select-lyric-line" value="odi vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">usurucku usurutuu
<input type="checkbox" class="lyrico-select-lyric-line" value="usurucku usurutuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">patharrai poi
<input type="checkbox" class="lyrico-select-lyric-line" value="patharrai poi"/>
</div>
<div class="lyrico-lyrics-wrapper">sarugaa aachu
<input type="checkbox" class="lyrico-select-lyric-line" value="sarugaa aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">enn manasu
<input type="checkbox" class="lyrico-select-lyric-line" value="enn manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee oda neera
<input type="checkbox" class="lyrico-select-lyric-line" value="nee oda neera"/>
</div>
<div class="lyrico-lyrics-wrapper">odi vandhu
<input type="checkbox" class="lyrico-select-lyric-line" value="odi vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">usurucku usurutuu
<input type="checkbox" class="lyrico-select-lyric-line" value="usurucku usurutuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">usurey
<input type="checkbox" class="lyrico-select-lyric-line" value="usurey"/>
</div>
<div class="lyrico-lyrics-wrapper">usurey
<input type="checkbox" class="lyrico-select-lyric-line" value="usurey"/>
</div>
<div class="lyrico-lyrics-wrapper">usurey
<input type="checkbox" class="lyrico-select-lyric-line" value="usurey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naalu nimisham
<input type="checkbox" class="lyrico-select-lyric-line" value="naalu nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">unna kanaama
<input type="checkbox" class="lyrico-select-lyric-line" value="unna kanaama"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kanaama
<input type="checkbox" class="lyrico-select-lyric-line" value="nee kanaama"/>
</div>
<div class="lyrico-lyrics-wrapper">en naadi narambu
<input type="checkbox" class="lyrico-select-lyric-line" value="en naadi narambu"/>
</div>
<div class="lyrico-lyrics-wrapper">surungi ponenae
<input type="checkbox" class="lyrico-select-lyric-line" value="surungi ponenae"/>
</div>
<div class="lyrico-lyrics-wrapper">naa ponenae
<input type="checkbox" class="lyrico-select-lyric-line" value="naa ponenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kann irundhum
<input type="checkbox" class="lyrico-select-lyric-line" value="kann irundhum"/>
</div>
<div class="lyrico-lyrics-wrapper">kabothi aanenae
<input type="checkbox" class="lyrico-select-lyric-line" value="kabothi aanenae"/>
</div>
<div class="lyrico-lyrics-wrapper">naa aanenae
<input type="checkbox" class="lyrico-select-lyric-line" value="naa aanenae"/>
</div>
<div class="lyrico-lyrics-wrapper">unna paartha podhu
<input type="checkbox" class="lyrico-select-lyric-line" value="unna paartha podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan oomaiyai ponenae
<input type="checkbox" class="lyrico-select-lyric-line" value="naan oomaiyai ponenae"/>
</div>
<div class="lyrico-lyrics-wrapper">naa ponenae
<input type="checkbox" class="lyrico-select-lyric-line" value="naa ponenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">silayai ninnenae
<input type="checkbox" class="lyrico-select-lyric-line" value="silayai ninnenae"/>
</div>
<div class="lyrico-lyrics-wrapper">thavamai ninnenae
<input type="checkbox" class="lyrico-select-lyric-line" value="thavamai ninnenae"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnenae
<input type="checkbox" class="lyrico-select-lyric-line" value="ninnenae"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnenae
<input type="checkbox" class="lyrico-select-lyric-line" value="ninnenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naalu nimisham
<input type="checkbox" class="lyrico-select-lyric-line" value="naalu nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">adi naalu nimisham
<input type="checkbox" class="lyrico-select-lyric-line" value="adi naalu nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">unna kanaama
<input type="checkbox" class="lyrico-select-lyric-line" value="unna kanaama"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kanaama
<input type="checkbox" class="lyrico-select-lyric-line" value="nee kanaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en naadi narambu
<input type="checkbox" class="lyrico-select-lyric-line" value="en naadi narambu"/>
</div>
<div class="lyrico-lyrics-wrapper">surungi ponenae
<input type="checkbox" class="lyrico-select-lyric-line" value="surungi ponenae"/>
</div>
<div class="lyrico-lyrics-wrapper">naa ponenae
<input type="checkbox" class="lyrico-select-lyric-line" value="naa ponenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">usurey
<input type="checkbox" class="lyrico-select-lyric-line" value="usurey"/>
</div>
<div class="lyrico-lyrics-wrapper">usurey
<input type="checkbox" class="lyrico-select-lyric-line" value="usurey"/>
</div>
<div class="lyrico-lyrics-wrapper">usurey
<input type="checkbox" class="lyrico-select-lyric-line" value="usurey"/>
</div>
<div class="lyrico-lyrics-wrapper">usurey
<input type="checkbox" class="lyrico-select-lyric-line" value="usurey"/>
</div>
<div class="lyrico-lyrics-wrapper">usurey
<input type="checkbox" class="lyrico-select-lyric-line" value="usurey"/>
</div>
</pre>
