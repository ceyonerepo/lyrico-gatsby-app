---
title: "hey menina song lyrics"
album: "Manmadhudu 2"
artist: "Chaitan Bharadwaj"
lyricist: "Subham Viswanadh"
director: "Rahul Ravindran"
path: "/albums/manmadhudu-2-lyrics"
song: "Hey Menina"
image: ../../images/albumart/manmadhudu-2.jpg
date: 2019-08-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/7B1kk8z8VI0"
type: "love"
singers:
  - Chaithan Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">I see you wanna love Menina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I see you wanna love Menina"/>
</div>
<div class="lyrico-lyrics-wrapper">I see you wanna love Menina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I see you wanna love Menina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I don’t fall in love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I don’t fall in love"/>
</div>
<div class="lyrico-lyrics-wrapper">I only make love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I only make love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee maayalo thelindhi nenega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee maayalo thelindhi nenega"/>
</div>
<div class="lyrico-lyrics-wrapper">Vundali reyantha matthuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vundali reyantha matthuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Premenichesey raa andhaanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premenichesey raa andhaanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa dhaarilo vaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa dhaarilo vaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandham andhe saradaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandham andhe saradaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Neela undaalle rojantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neela undaalle rojantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Epudu one way lone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epudu one way lone"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam chaala bagundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam chaala bagundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho nene untaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho nene untaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhuke naa runway lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhuke naa runway lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaru naatho raavaddhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaru naatho raavaddhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho nene anthegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho nene anthegaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Crazy as he seems
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crazy as he seems"/>
</div>
<div class="lyrico-lyrics-wrapper">Love is bursting at the seams
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love is bursting at the seams"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart is asking what’s the scene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart is asking what’s the scene"/>
</div>
<div class="lyrico-lyrics-wrapper">All the places he has been to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All the places he has been to"/>
</div>
<div class="lyrico-lyrics-wrapper">Life he has been through
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life he has been through"/>
</div>
<div class="lyrico-lyrics-wrapper">Now he’s keen to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Now he’s keen to"/>
</div>
<div class="lyrico-lyrics-wrapper">Keep it simple always cheerful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keep it simple always cheerful"/>
</div>
<div class="lyrico-lyrics-wrapper">Way to cool to handle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Way to cool to handle"/>
</div>
<div class="lyrico-lyrics-wrapper">What he can do is what yo can’t do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What he can do is what yo can’t do"/>
</div>
<div class="lyrico-lyrics-wrapper">You can’t hold a candle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You can’t hold a candle"/>
</div>
<div class="lyrico-lyrics-wrapper">Take up the mantle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take up the mantle"/>
</div>
<div class="lyrico-lyrics-wrapper">Now you can’t tell the love giver
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Now you can’t tell the love giver"/>
</div>
<div class="lyrico-lyrics-wrapper">Never the one to quiver
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Never the one to quiver"/>
</div>
<div class="lyrico-lyrics-wrapper">Whatever will be etched forever
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whatever will be etched forever"/>
</div>
<div class="lyrico-lyrics-wrapper">Wah re wah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wah re wah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Epudu one way lone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epudu one way lone"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam chaala bagundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam chaala bagundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho nene untaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho nene untaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhuke naa runway lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhuke naa runway lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaru naatho raavaddhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaru naatho raavaddhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho nene anthegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho nene anthegaa"/>
</div>
</pre>
