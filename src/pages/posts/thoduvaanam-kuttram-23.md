---
title: "thoduvaanam song lyrics"
album: "Kuttram 23"
artist: "Vishal Chandrasekhar"
lyricist: "Viveka"
director: "Arivazhagan Venkatachalam"
path: "/albums/kuttram-23-lyrics"
song: "Thoduvaanam"
image: ../../images/albumart/kuttram-23.jpg
date: 2017-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/za7isRNikIE"
type: "sad"
singers:
  - Sinduri Vishal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thoduvaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoduvaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nettruvarai adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nettruvarai adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholai dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholai dhooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi pookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi pookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan nenjoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan nenjoram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoduvaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoduvaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nettruvarai adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nettruvarai adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholai dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholai dhooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi pookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi pookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan nenjoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan nenjoram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkum moligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkum moligal"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhu unarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhu unarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooraadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooraadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avizha thudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avizha thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu malarin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu malarin"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaivum sugamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaivum sugamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avanin nagala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanin nagala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhu nagala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhu nagala"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarin mugamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarin mugamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoduvaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoduvaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nettruvarai adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nettruvarai adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholai dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholai dhooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi pookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi pookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan nenjoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan nenjoram"/>
</div>
</pre>
