---
title: "venamppa venam song lyrics"
album: "Arasakulam"
artist: "Velan Sakadevan"
lyricist: "Pavalan"
director: "Kumaramaran"
path: "/albums/arasakulam-lyrics"
song: "Venamppa Venam"
image: ../../images/albumart/arasakulam.jpg
date: 2017-03-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FBeatDT3bYo"
type: "sad"
singers:
  - Velan Sagadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">venampa venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venampa venam"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kadhal venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kadhal venam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal venam"/>
</div>
<div class="lyrico-lyrics-wrapper">venampa venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venampa venam"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kadhal venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kadhal venam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal venam"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnapa konnapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnapa konnapa"/>
</div>
<div class="lyrico-lyrics-wrapper">usurukulla valikuthu pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usurukulla valikuthu pa"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnapa konnapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnapa konnapa"/>
</div>
<div class="lyrico-lyrics-wrapper">usurukulla valikuthu pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usurukulla valikuthu pa"/>
</div>
<div class="lyrico-lyrics-wrapper">uriya nenachen pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uriya nenachen pa"/>
</div>
<div class="lyrico-lyrics-wrapper">urava thudichen pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urava thudichen pa"/>
</div>
<div class="lyrico-lyrics-wrapper">poiya kathalichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poiya kathalichu"/>
</div>
<div class="lyrico-lyrics-wrapper">meiya maranthapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meiya maranthapa"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyayo nenachale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyayo nenachale"/>
</div>
<div class="lyrico-lyrics-wrapper">labakunu kuthuchale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="labakunu kuthuchale"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyayo nenachale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyayo nenachale"/>
</div>
<div class="lyrico-lyrics-wrapper">labakunu kuthuchale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="labakunu kuthuchale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">venampa venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venampa venam"/>
</div>
<div class="lyrico-lyrics-wrapper">venampa venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venampa venam"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kadhal venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kadhal venam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal venam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathala visam munu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathala visam munu"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnathu poi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnathu poi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">methuva methu methuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="methuva methu methuva"/>
</div>
<div class="lyrico-lyrics-wrapper">aala athu kollumapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala athu kollumapa"/>
</div>
<div class="lyrico-lyrics-wrapper">methuva methu methuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="methuva methu methuva"/>
</div>
<div class="lyrico-lyrics-wrapper">aala athu kollumapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala athu kollumapa"/>
</div>
<div class="lyrico-lyrics-wrapper">enakathu venam pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakathu venam pa"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalika thonavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalika thonavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalicha evanukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalicha evanukum"/>
</div>
<div class="lyrico-lyrics-wrapper">nimmathi kidachathu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimmathi kidachathu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">rasa magan rasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasa magan rasa"/>
</div>
<div class="lyrico-lyrics-wrapper">ada solli tharen kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada solli tharen kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal oru dosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal oru dosa"/>
</div>
<div class="lyrico-lyrics-wrapper">atha pakuvama kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha pakuvama kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">meesa naraikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meesa naraikum"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa naraikuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa naraikuma"/>
</div>
<div class="lyrico-lyrics-wrapper">nee than verutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee than verutha"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal verukuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal verukuma"/>
</div>
<div class="lyrico-lyrics-wrapper">manasa payala padachathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasa payala padachathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethuku kathal seiya than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuku kathal seiya than"/>
</div>
<div class="lyrico-lyrics-wrapper">kavuntha thalaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavuntha thalaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">nimithalana vaanam theriyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimithalana vaanam theriyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">appa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">venampa venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venampa venam"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kadhal venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kadhal venam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal venam"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnapa konnapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnapa konnapa"/>
</div>
<div class="lyrico-lyrics-wrapper">usurukulla valikuthu pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usurukulla valikuthu pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">venam pa venam pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venam pa venam pa"/>
</div>
<div class="lyrico-lyrics-wrapper">appa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aayiram poiya solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram poiya solli"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyanam senjukalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam senjukalam"/>
</div>
<div class="lyrico-lyrics-wrapper">poiyana kathal la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poiyana kathal la"/>
</div>
<div class="lyrico-lyrics-wrapper">nan yen matikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan yen matikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">poiyana kathal la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poiyana kathal la"/>
</div>
<div class="lyrico-lyrics-wrapper">nan yen matikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan yen matikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalula vilunthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalula vilunthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">karai vanthu senthuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karai vanthu senthuduven"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalula vilunthena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalula vilunthena"/>
</div>
<div class="lyrico-lyrics-wrapper">polambiye sethuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="polambiye sethuduven"/>
</div>
<div class="lyrico-lyrics-wrapper">aathi choodi sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathi choodi sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">ada ovaiyaaru kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada ovaiyaaru kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalika solli paatu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalika solli paatu "/>
</div>
<div class="lyrico-lyrics-wrapper">eluthi vachanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eluthi vachanga"/>
</div>
<div class="lyrico-lyrics-wrapper">patha vaalkai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha vaalkai "/>
</div>
<div class="lyrico-lyrics-wrapper">romba chinnathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba chinnathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ella usurum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ella usurum "/>
</div>
<div class="lyrico-lyrics-wrapper">kathal pannuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal pannuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pulla kutti petha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulla kutti petha"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnum kathal theeruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnum kathal theeruma"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyil vachu sutta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyil vachu sutta "/>
</div>
<div class="lyrico-lyrics-wrapper">kooda kathal poguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda kathal poguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">venampa venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venampa venam"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kadhal venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kadhal venam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal venam"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnapa konnapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnapa konnapa"/>
</div>
<div class="lyrico-lyrics-wrapper">usurukulla valikuthu pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usurukulla valikuthu pa"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnapa konnapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnapa konnapa"/>
</div>
<div class="lyrico-lyrics-wrapper">usurukulla valikuthu pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usurukulla valikuthu pa"/>
</div>
<div class="lyrico-lyrics-wrapper">uriya nenachen pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uriya nenachen pa"/>
</div>
<div class="lyrico-lyrics-wrapper">urava thudichen pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urava thudichen pa"/>
</div>
<div class="lyrico-lyrics-wrapper">poiya kathalichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poiya kathalichu"/>
</div>
<div class="lyrico-lyrics-wrapper">meiya maranthapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meiya maranthapa"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyayo nenachale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyayo nenachale"/>
</div>
<div class="lyrico-lyrics-wrapper">labakunu kuthuchale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="labakunu kuthuchale"/>
</div>
<div class="lyrico-lyrics-wrapper">aiyayo nenachale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyayo nenachale"/>
</div>
<div class="lyrico-lyrics-wrapper">labakunu kuthuchale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="labakunu kuthuchale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">venampa venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venampa venam"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kadhal venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kadhal venam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal venam"/>
</div>
</pre>
