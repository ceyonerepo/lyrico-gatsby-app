---
title: "prema idhi song lyrics"
album: "Sashi"
artist: "Arun Chiluveru"
lyricist: "Kalyan Chakravrthy"
director: "Srinivas Naidu Nadikatla"
path: "/albums/sashi-lyrics"
song: "Prema Idhi Prema"
image: ../../images/albumart/sashi.jpg
date: 2021-03-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/96RejRvmoHg"
type: "love"
singers:
  - Ishaq Vali
  - Bolt
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Prema idhi prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema idhi prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu ounanna kadhanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu ounanna kadhanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prema idhi prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema idhi prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mayallone unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mayallone unna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prema idhi prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema idhi prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaraounanna kadhanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaraounanna kadhanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prema idhi prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema idhi prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mayallone unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mayallone unna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalo dhyase neeva neeva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo dhyase neeva neeva"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolo oose neeva oh…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolo oose neeva oh…"/>
</div>
<div class="lyrico-lyrics-wrapper">Paade kanne neeva neeva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paade kanne neeva neeva"/>
</div>
<div class="lyrico-lyrics-wrapper">Aade minne neeva neeva oh…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aade minne neeva neeva oh…"/>
</div>
<div class="lyrico-lyrics-wrapper">Bugolamantha nee valle nee valle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bugolamantha nee valle nee valle"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagishilu poose nee vallena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagishilu poose nee vallena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee paala puntha naavalee naa valle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee paala puntha naavalee naa valle"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagumomu chere neevallena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagumomu chere neevallena"/>
</div>
<div class="lyrico-lyrics-wrapper">Panche ee prema peduthundhe o coma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panche ee prema peduthundhe o coma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bashedhaina bavam inthe rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bashedhaina bavam inthe rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Penche prema edhamunchenamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penche prema edhamunchenamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuremaina nivuraipodhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuremaina nivuraipodhamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prema idhi prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema idhi prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Evarounanna kadhanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evarounanna kadhanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prema idhi prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema idhi prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mayallone unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mayallone unna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prema idhi prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema idhi prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Evarounanna kadhanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evarounanna kadhanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prema idhi prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema idhi prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mayallone unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mayallone unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaare kannu neeva neeva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaare kannu neeva neeva"/>
</div>
<div class="lyrico-lyrics-wrapper">Mire minne neeva oh…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mire minne neeva oh…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaara theeram neeva neeva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaara theeram neeva neeva"/>
</div>
<div class="lyrico-lyrics-wrapper">Karagaram neeva oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karagaram neeva oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prema idhi prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema idhi prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mayallone unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mayallone unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhuram baram neeva neeva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuram baram neeva neeva"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaari dhaapu Neeva oh…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaari dhaapu Neeva oh…"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam vedham neeva neeva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam vedham neeva neeva"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhi antham neeva oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhi antham neeva oh"/>
</div>
</pre>
