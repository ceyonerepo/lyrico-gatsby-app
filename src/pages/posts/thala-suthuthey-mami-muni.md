---
title: "thala suthuthey mami song lyrics"
album: "Muni"
artist: "Bharathwaj"
lyricist: "Pa. Vijay - Raghava Lawrence"
director: "Raghava Lawrence"
path: "/albums/muni-lyrics"
song: "Thala Suthuthey Mami"
image: ../../images/albumart/muni.jpg
date: 2007-03-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7jfnvVjcK7A"
type: "Love"
singers:
  - Bharathwaj
  - Kavitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oo….. maami……………
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo….. maami……………"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo……………. maami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo……………. maami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo….. maama……………
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo….. maama……………"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo……………. maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo……………. maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala suththuthae maami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala suththuthae maami"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku thala suththuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku thala suththuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppaaduthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppaaduthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku iduppaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku iduppaaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala suththuthae maami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala suththuthae maami"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku thala suththuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku thala suththuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppaaduthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppaaduthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku iduppaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku iduppaaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanjavuru bomma pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjavuru bomma pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambu suththuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambu suththuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala kaalu puriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala kaalu puriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha udambu aaduthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha udambu aaduthadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ey chellakutty ey bujjukutti…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey chellakutty ey bujjukutti….."/>
</div>
<div class="lyrico-lyrics-wrapper">Ey chellakutty ey bujjukutti…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey chellakutty ey bujjukutti….."/>
</div>
<div class="lyrico-lyrics-wrapper">Ey chellakutty ammukutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey chellakutty ammukutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjukutti…..pappukutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjukutti…..pappukutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutti kuttiyooo hei saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti kuttiyooo hei saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala suththuthae maami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala suththuthae maami"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku thala suththuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku thala suththuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh maami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh maami"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppaaduthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppaaduthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku iduppaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku iduppaaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh maamaa ohh maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh maamaa ohh maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamaa maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaa maamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odambu romba soodaa irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambu romba soodaa irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu paarthu kaaichala erukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu paarthu kaaichala erukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnappaiyaa ye chinnappaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnappaiyaa ye chinnappaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotta udanae kaaichal ponathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta udanae kaaichal ponathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattipudichi kacha mocha nadakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattipudichi kacha mocha nadakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna ponnae ye chinna ponnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna ponnae ye chinna ponnae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittu vittu varuvathu kaaichal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu vittu varuvathu kaaichal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada vidaama adippathu neechal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada vidaama adippathu neechal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli thalli viduvathu oonjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli thalli viduvathu oonjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kitta vanthu mayakkura angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kitta vanthu mayakkura angel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada ennennamo sollura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ennennamo sollura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennennamo seiyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo seiyura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta izhuththu orasippaakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta izhuththu orasippaakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Orasunaathaandi paththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasunaathaandi paththum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala suththuthae maami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala suththuthae maami"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku thala suththuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku thala suththuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iduppaaduthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppaaduthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku iduppaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku iduppaaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udambu paathaa chinnathaa irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambu paathaa chinnathaa irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannure vela perusaa irukkku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannure vela perusaa irukkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruttupaiyaa ye ye Thiruttupaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruttupaiyaa ye ye Thiruttupaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odambukkula onnum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambukkula onnum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiya sonnaa romba tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiya sonnaa romba tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruttu pennae ye thiruttu pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruttu pennae ye thiruttu pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada ellaam enakku theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ellaam enakku theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ellaam enakku puriyum puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ellaam enakku puriyum puriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennadi unakku theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadi unakku theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi ennadi unakku puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi ennadi unakku puriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada podaa podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada podaa podaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Peiyaa alaiyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peiyaa alaiyura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukkulla yetho kanakkiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkulla yetho kanakkiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennathu peiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathu peiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivallukkeppadi theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivallukkeppadi theriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala suththuthae maami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala suththuthae maami"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku thala suththuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku thala suththuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iduppaaduthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppaaduthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku iduppaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku iduppaaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala suththuthae maami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala suththuthae maami"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku thala suththuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku thala suththuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iduppaaduthae maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppaaduthae maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku iduppaaduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku iduppaaduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanjavuru bomma pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjavuru bomma pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambu suththuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambu suththuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala kaalu puriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala kaalu puriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha udambu aaduthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha udambu aaduthadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ey chellakutty ey bujjukutti…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey chellakutty ey bujjukutti….."/>
</div>
<div class="lyrico-lyrics-wrapper">Ey chellakutty ey bujjukutti…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey chellakutty ey bujjukutti….."/>
</div>
<div class="lyrico-lyrics-wrapper">Ey chellakutty ammukutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey chellakutty ammukutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjukutti…..pappukutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjukutti…..pappukutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutti kuttiyooo hei saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti kuttiyooo hei saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahaha"/>
</div>
</pre>
