---
title: "yaar ival song lyrics"
album: "Mangai Maanvizhi Ambugal"
artist: "Thameem Ansari"
lyricist: "Vno - Arungopal"
director: "Vino"
path: "/albums/mangai-maanvizhi-ambugal-lyrics"
song: "Yaar Ival"
image: ../../images/albumart/mangai-maanvizhi-ambugal.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1hef_-ZSa4U"
type: "love"
singers:
  - Sathyaprakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaar ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalil vazhi solli pogiral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalil vazhi solli pogiral"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisai ettum maranthae pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai ettum maranthae pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaranthidum mayilin thogaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaranthidum mayilin thogaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Malar ena vazhnthida yenginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar ena vazhnthida yenginen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Mun jenmathin kanmani ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mun jenmathin kanmani ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival than aval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival than aval"/>
</div>
<div class="lyrico-lyrics-wrapper">In jenmathin muthal penmaniyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In jenmathin muthal penmaniyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhalinil sinugalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhalinil sinugalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorandu vaazhven adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorandu vaazhven adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgalil nagangalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgalil nagangalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhnthaalum podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhnthaalum podhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalil vazhi solli pogiral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalil vazhi solli pogiral"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisai ettum maranthae pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai ettum maranthae pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pa dha ni dha pa dha ni dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa dha ni dha pa dha ni dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa ppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa ppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saa saa ni dha ma pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saa saa ni dha ma pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa dha ni dha pa dha ni dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa dha ni dha pa dha ni dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa ppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa ppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saa saa ni dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saa saa ni dha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pa dha ni dha pa dha ni dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa dha ni dha pa dha ni dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa ppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa ppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saa saa ni dha ma pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saa saa ni dha ma pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa dha ni dha pa dha ni dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa dha ni dha pa dha ni dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa ppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa ppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saa saa ni dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saa saa ni dha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaa aaa ahaaaaaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaa ahaaaaaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaaaaaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru naal dhan unai parthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal dhan unai parthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvae neeyae aanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvae neeyae aanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaikaamal unnai kappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaikaamal unnai kappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae uravaai aanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae uravaai aanal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa aaaa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa aaaa aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru naal dhan unai parthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal dhan unai parthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvae neeyae aanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvae neeyae aanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaikaamal unnai kappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaikaamal unnai kappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae uravaai aanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirae uravaai aanal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pavai parvaiyai paarthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavai parvaiyai paarthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam nindru dhaan thudithidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam nindru dhaan thudithidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mendum paarvai illai yeanil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mendum paarvai illai yeanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikum idhayamum nindridumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikum idhayamum nindridumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum pathaiyil polintha thuralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum pathaiyil polintha thuralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muluvathum nanaipai saralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muluvathum nanaipai saralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendum kaathalai virumbi thandhidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendum kaathalai virumbi thandhidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvena sumapen malaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvena sumapen malaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ival"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirana theentha nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirana theentha nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiranana thirananaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiranana thirananaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thira nana nana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thira nana nana nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru naal dhan unnai parthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal dhan unnai parthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thira thira thira thira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thira thira thira thira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvae neeyae aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvae neeyae aanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiranana thiranana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiranana thiranana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiranana thiranana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiranana thiranana"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendalil"/>
</div>
</pre>
