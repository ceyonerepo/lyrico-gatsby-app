---
title: "yela yela song lyrics"
album: "Sakala Kala Vallabhudu"
artist: "Ajay Patnaik"
lyricist: "Subash Natayan"
director: "Shiva Ganesh"
path: "/albums/sakala-kala-vallabhudu-lyrics"
song: "Yela Yela"
image: ../../images/albumart/sakala-kala-vallabhudu.jpg
date: 2019-02-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/a0kQFJjL1Iw"
type: "love"
singers:
  - Dhananjay Bhattacharya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanya Rasi Kanye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanya Rasi Kanye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Lifeloki Vaccheraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Lifeloki Vaccheraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Love Chaptaree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Love Chaptaree"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhalayeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalayeraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andamante Needey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andamante Needey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pedari Navvu Chaaleeeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pedari Navvu Chaaleeeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokamantha Verey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokamantha Verey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Choosthe Manasu Jaaree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Choosthe Manasu Jaaree"/>
</div>
<div class="lyrico-lyrics-wrapper">Andamante Needey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andamante Needey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pedari Navvu Chaaleeeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pedari Navvu Chaaleeeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokamantha Verey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokamantha Verey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Choosthe Manasu Jaaree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Choosthe Manasu Jaaree"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa Neeli Mabbulloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa Neeli Mabbulloo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeti Chukka Nuvvee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeti Chukka Nuvvee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelaa Paiki Vachchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelaa Paiki Vachchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gunde Thadipinaavee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gunde Thadipinaavee"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosi Choosi Mantramesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosi Choosi Mantramesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Premalona Dimpinaavee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premalona Dimpinaavee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yela Yela Yela Yela Cheliyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela Yela Yela Yela Cheliyaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pranamantha Laaginaavee Sakhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pranamantha Laaginaavee Sakhiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Yela Yela Yela YelaCheliyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Yela Yela Yela YelaCheliyaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pranamantha Laaginaavee Sakhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pranamantha Laaginaavee Sakhiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennelakainaa Chaliputtinchee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennelakainaa Chaliputtinchee"/>
</div>
<div class="lyrico-lyrics-wrapper">Sannani Nadume Needegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannani Nadume Needegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Suryunikainaa Segalettesee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suryunikainaa Segalettesee"/>
</div>
<div class="lyrico-lyrics-wrapper">Surukku Choope Needegaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surukku Choope Needegaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkala Merupu Neelo Thaluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkala Merupu Neelo Thaluku"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyaku Podupu Neelo Kuluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyaku Podupu Neelo Kuluku"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkala Merupu Neelo Thaluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkala Merupu Neelo Thaluku"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyaku Podupu Neelo Kuluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyaku Podupu Neelo Kuluku"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosi Choosi Mantramesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosi Choosi Mantramesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pramlona Dimpinavee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pramlona Dimpinavee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yela Yela Yela Yela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela Yela Yela Yela"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliyaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pranamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pranamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Laaginaavee Sakhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laaginaavee Sakhiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Yela Yela Yela Yela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Yela Yela Yela Yela"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliyaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pranamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pranamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Laaginaavee Sakhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laaginaavee Sakhiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanya Raasi Muddula Gummaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanya Raasi Muddula Gummaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakai Nuvve Vachchavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakai Nuvve Vachchavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Choosthuntee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Choosthuntee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagadhu Manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagadhu Manase"/>
</div>
<div class="lyrico-lyrics-wrapper">Andam Kaani Thinnavaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andam Kaani Thinnavaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chelime Chestha Thodai Vasthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chelime Chestha Thodai Vasthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Roju Ninne Follow Chesthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roju Ninne Follow Chesthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chelime Chestha Thodai Vasthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chelime Chestha Thodai Vasthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Roju Ninne Follow Chesthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roju Ninne Follow Chesthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosi Choosi Mantramesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosi Choosi Mantramesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Premalona Dimpinaavee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premalona Dimpinaavee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yela Yela Yela Yela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela Yela Yela Yela"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliyaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pranamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pranamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Laaginaavee Sakhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laaginaavee Sakhiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Yela Yela Yela Yela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Yela Yela Yela Yela"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliyaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pranamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pranamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Laaginaavee Sakhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laaginaavee Sakhiyaa"/>
</div>
</pre>
