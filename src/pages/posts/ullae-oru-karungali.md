---
title: "ullae oru song lyrics"
album: "Karungali"
artist: "Srikanth Deva"
lyricist: "Ilaya Kamban"
director: "Kalanjiyam"
path: "/albums/karungali-lyrics"
song: "Ullae Oru"
image: ../../images/albumart/karungali.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/q8aLJ-PJdiU"
type: "terror"
singers:
  - Sathyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En uyirai koduthavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirai koduthavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">urakkam parithavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urakkam parithavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu neethaanadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu neethaanadi "/>
</div>
<div class="lyrico-lyrics-wrapper">en kanavu neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanavu neethaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">enai konjam konjamaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai konjam konjamaai "/>
</div>
<div class="lyrico-lyrics-wrapper">kola pirandhavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola pirandhavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu neethaanadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu neethaanadi "/>
</div>
<div class="lyrico-lyrics-wrapper">en kanavu neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanavu neethaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulley oru mirugam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulley oru mirugam "/>
</div>
<div class="lyrico-lyrics-wrapper">urangaamal adhu serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urangaamal adhu serum"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai adhu viratti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai adhu viratti "/>
</div>
<div class="lyrico-lyrics-wrapper">thindraaley pasi aarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thindraaley pasi aarum"/>
</div>
<div class="lyrico-lyrics-wrapper">moochukkaatril theeppidikka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochukkaatril theeppidikka "/>
</div>
<div class="lyrico-lyrics-wrapper">moththa idhayamum karugudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moththa idhayamum karugudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhugu thandil moagam kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhugu thandil moagam kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">latcham paambugal neliyudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="latcham paambugal neliyudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulley oru mirugam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulley oru mirugam "/>
</div>
<div class="lyrico-lyrics-wrapper">urangaamal adhu serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urangaamal adhu serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhu azhagu ennai vadhaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu azhagu ennai vadhaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyirai naar naaraai kizhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyirai naar naaraai kizhikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">raththam un ninaivaal kodhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raththam un ninaivaal kodhikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">en narambil yevuganai vedikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en narambil yevuganai vedikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagaal ennai vadhai seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagaal ennai vadhai seigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">thavanaiyil ena kolaiseigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavanaiyil ena kolaiseigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">indrey unai kaiyil kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indrey unai kaiyil kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">illai enai thookkilidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai enai thookkilidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En uyirai koduthavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirai koduthavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">urakkam parithavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urakkam parithavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu neethaanadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu neethaanadi "/>
</div>
<div class="lyrico-lyrics-wrapper">en kanavu neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanavu neethaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">enai konjam konjamaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai konjam konjamaai "/>
</div>
<div class="lyrico-lyrics-wrapper">kola pirandhavaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola pirandhavaley"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavu neethaanadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavu neethaanadi "/>
</div>
<div class="lyrico-lyrics-wrapper">en kanavu neethaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kanavu neethaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulley oru mirugam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulley oru mirugam "/>
</div>
<div class="lyrico-lyrics-wrapper">urangaamal adhu serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urangaamal adhu serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooriyanil padhunginaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyanil padhunginaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">angum seemics poala nan varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angum seemics poala nan varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul vandhu thaduthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul vandhu thaduthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">adi unnai adaindhu uyirviduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi unnai adaindhu uyirviduven"/>
</div>
<div class="lyrico-lyrics-wrapper">noagaamaley enai koigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noagaamaley enai koigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">nuraiyeeralil thuvaikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuraiyeeralil thuvaikkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">boogambamaai vandhaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boogambamaai vandhaayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">tharaimattamaai sarindhenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharaimattamaai sarindhenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulley oru mirugam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulley oru mirugam "/>
</div>
<div class="lyrico-lyrics-wrapper">urangaamal adhu serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urangaamal adhu serum"/>
</div>
</pre>
