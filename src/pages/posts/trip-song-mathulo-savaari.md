---
title: "trip song (mathulo) song lyrics"
album: "Savaari"
artist: "Shekar Chandra"
lyricist: "Shekhar Chandra"
director: "Saahith Mothkuri"
path: "/albums/savaari-lyrics"
song: "Trip Song (Mathulo)"
image: ../../images/albumart/savaari.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Kct7N4cZD8M"
type: "happy"
singers:
  - Shekhar Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mathu Lo Thelipodhama Eeee Velalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathu Lo Thelipodhama Eeee Velalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalila Oogi Podhama Aakasham Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalila Oogi Podhama Aakasham Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho Nenu Natho Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Nenu Natho Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pareshanlu Inka Emi Levu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pareshanlu Inka Emi Levu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manalninka Evvaru Aapaleru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalninka Evvaru Aapaleru"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Upside Down Gunnadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Upside Down Gunnadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Aaaaari Poya Poya Po Po Poya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Aaaaari Poya Poya Po Po Poya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Jaaari Poya Poya Pooooooya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Jaaari Poya Poya Pooooooya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Aaaaari Poya Poya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Aaaaari Poya Poya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Nenu Vadhilipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Nenu Vadhilipoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Jaaari Poya Poya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Jaaari Poya Poya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thapassulona Munigipoyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thapassulona Munigipoyaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooyeeee Kothagunnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyeeee Kothagunnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyeeee Masthugunnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyeeee Masthugunnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyeeee Sakkagunnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyeeee Sakkagunnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Ga Ekkuthunnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Ga Ekkuthunnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Sari Gunji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Sari Gunji"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendo Sari Laagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendo Sari Laagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodo Sari O
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodo Sari O"/>
</div>
<div class="lyrico-lyrics-wrapper">Uff Uff Uffmantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uff Uff Uffmantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalgo Saari Arre I Am Sorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalgo Saari Arre I Am Sorry"/>
</div>
<div class="lyrico-lyrics-wrapper">Swargam Lo Swimming E
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swargam Lo Swimming E"/>
</div>
<div class="lyrico-lyrics-wrapper">Abbabbabbabbaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abbabbabbabbaba"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Baaga Undho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Baaga Undho"/>
</div>
<div class="lyrico-lyrics-wrapper">Abbabbabbabbaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abbabbabbabbaba"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Baaga Undhoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Baaga Undhoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Abbabbabbabbaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abbabbabbabbaba"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Baaga Undho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Baaga Undho"/>
</div>
<div class="lyrico-lyrics-wrapper">Abbabbabbabbaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abbabbabbabbaba"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Entha Baaga Undho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Entha Baaga Undho"/>
</div>
<div class="lyrico-lyrics-wrapper">This Pilla Is So Hot Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This Pilla Is So Hot Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Smoking Pot Bloody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smoking Pot Bloody"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Her A Lot Shady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Her A Lot Shady"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My God
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My God"/>
</div>
<div class="lyrico-lyrics-wrapper">I Wanna Tie You Knot Lady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Wanna Tie You Knot Lady"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedatha Spot Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedatha Spot Baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Netthi Meedha Pedatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netthi Meedha Pedatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dot Dot Dot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dot Dot Dot"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathu Lo Thelipodhama Eeee Velalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathu Lo Thelipodhama Eeee Velalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalila Oogi Podhama Aakasham Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalila Oogi Podhama Aakasham Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho Nenu Natho Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Nenu Natho Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pareshanlu Inka Emi Levu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pareshanlu Inka Emi Levu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manalninka Evvaru Aapaleru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalninka Evvaru Aapaleru"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Upside Down Gunnadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Upside Down Gunnadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Aaaaari Poya Poya Po Po Poya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Aaaaari Poya Poya Po Po Poya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Jaaari Poya Poya Pooooooya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Jaaari Poya Poya Pooooooya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Aaaaari Poya Poya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Aaaaari Poya Poya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Nenu Vadhilipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Nenu Vadhilipoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Jaaari Poya Poya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Jaaari Poya Poya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thapassulona Munigipoyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thapassulona Munigipoyaaa"/>
</div>
</pre>
