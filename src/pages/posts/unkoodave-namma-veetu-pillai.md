---
title: "unkoodave lyrics"
album: "namma veetu pillai"
artist: "D Imman"
lyricist: "GKB"
director: "pandiraj"
path: "/albums/namma-veetu-pillai-song-lyrics"
song: "unkoodave"
image: ../../images/albumart/namma-veetu-pillai.jpg
date: 2019-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2ggLEMPvgdU"
type: "sentiment"
singers:
  - Sid sriram
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaaga naan irukkanum eppodhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga naan irukkanum eppodhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai pola naan kaakkanum eppodhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai pola naan kaakkanum eppodhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En vaazhka varamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhka varamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada neeyum porandhaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada neeyum porandhaaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirae uravaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyirae uravaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjil karainjaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjil karainjaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasi thookaththa marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi thookaththa marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi paasaththa pozhinjaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi paasaththa pozhinjaaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhenam un mugam paarthu pookum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhenam un mugam paarthu pookum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu vidiyalum thandhaaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu vidiyalum thandhaaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee enakku saami indha boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enakku saami indha boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ellam nee thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ellam nee thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sirippu pothum nee kettaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sirippu pothum nee kettaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En usura thaarenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En usura thaarenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaaga naan irukkanum eppodhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga naan irukkanum eppodhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai pola naan kaakkanum eppodhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai pola naan kaakkanum eppodhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaaga naan irukkanum eppodhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga naan irukkanum eppodhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai pola naan kaakkanum eppodhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai pola naan kaakkanum eppodhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa aaa aaa aaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa aaa aaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koodaavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodaavae porakkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anju viralgala korthu naama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anju viralgala korthu naama"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu viralaa aanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu viralaa aanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakolathil paarthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakolathil paarthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha chinna ponna kaanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha chinna ponna kaanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila naalil nee en thaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila naalil nee en thaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila naalil nee en seiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila naalil nee en seiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee madimel ssayum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee madimel ssayum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha vaanam virikkum paayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha vaanam virikkum paayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppodhumae enkooda thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhumae enkooda thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naetru nenachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naetru nenachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo nee pogum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo nee pogum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethu polachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethu polachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thaanae kolasaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaanae kolasaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru varamum thaayen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru varamum thaayen"/>
</div>
<div class="lyrico-lyrics-wrapper">Magalaaga porappennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magalaaga porappennu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee solli poyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee solli poyen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaaga naan irukkanum eppodhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga naan irukkanum eppodhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai pola naan kaakkanum eppodhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai pola naan kaakkanum eppodhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaaga naan irukkanum eppodhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga naan irukkanum eppodhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koodavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodavae porakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai pola naan kaakkanum eppodhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai pola naan kaakkanum eppodhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa aaa aaa aaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa aaa aaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un koodaavae porakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koodaavae porakkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo ooo ooo hoo oo oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo ooo ooo hoo oo oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hoo hooo ooo oooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo hoo hooo ooo oooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Read more at https//naalyrics.com/unkoodave-porakkanum-lyrics/
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Read more at https//naalyrics.com/unkoodave-porakkanum-lyrics/"/>
</div>
</pre>
