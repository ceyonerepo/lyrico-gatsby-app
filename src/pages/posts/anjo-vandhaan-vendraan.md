---
title: "anjo song lyrics"
album: "Vandhaan Vendraan"
artist: "Thaman"
lyricist: "Madhan Karky"
director: "R. Kannan"
path: "/albums/vandhaan-vendraan-lyrics"
song: "Anjo"
image: ../../images/albumart/vandhaan-vendraan.jpg
date: 2011-09-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/yUvGjdZuMlY"
type: "happy"
singers:
  - Devan Ekambaram
  - Rahul Nambiar
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Min nilavival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Min nilavival"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru ven olithiral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru ven olithiral"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal simitidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal simitidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha newyork silaiyival hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha newyork silaiyival hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjo lovely anjo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjo lovely anjo"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheesy anjoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheesy anjoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjo oru panithuli pathumaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjo oru panithuli pathumaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjo oru puthuvagai puthumaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjo oru puthuvagai puthumaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjo oru puthuvagai puthumaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjo oru puthuvagai puthumaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ival thumbiyo oru thumbaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival thumbiyo oru thumbaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hohoo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hohoo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Jean topsilae oru jhansiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jean topsilae oru jhansiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hohoo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hohoo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Catvakkidum oru saetaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Catvakkidum oru saetaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hohoo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hohoo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai pootilaa poo muttaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai pootilaa poo muttaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooo ooo ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooo ooo ooo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey veenai melae ponai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey veenai melae ponai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi melae vanthaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi melae vanthaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mounam illa naanam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mounam illa naanam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonam kondae vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonam kondae vandhaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruvaayai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruvaayai"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiravaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiravaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Noo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival amaithikku nobel thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival amaithikku nobel thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagamae mudivedukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagamae mudivedukum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey anjo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey anjo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vinmeenin pinjo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vinmeenin pinjo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey anjo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey anjo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee konjum men panjo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konjum men panjo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ival vizhikkooril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival vizhikkooril"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinandhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinandhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhipadum nooru irudhayamAadai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhipadum nooru irudhayamAadai "/>
</div>
<div class="lyrico-lyrics-wrapper">mothi ooril paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothi ooril paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avathiyil alaigiratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avathiyil alaigiratho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Needle idai model polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needle idai model polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eedillamal vanthaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedillamal vanthaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hoo hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo hoo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Iphone il ila paadal ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iphone il ila paadal ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhil paada vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil paada vanthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neptune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neptune"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Endralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endralum"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandora"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendralum"/>
</div>
<div class="lyrico-lyrics-wrapper">Noo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival alagukku nigar ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival alagukku nigar ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Agilathil uyir ilaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agilathil uyir ilaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjo oru puthuvagai puthumaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjo oru puthuvagai puthumaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hebrew latin kavithaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hebrew latin kavithaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyebrow mozhiyodu thotridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyebrow mozhiyodu thotridum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ballae flamingo nadanangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ballae flamingo nadanangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavai vizhiyodu thotridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavai vizhiyodu thotridum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Waltz and jazz ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waltz and jazz ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival pechil thotridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival pechil thotridum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sengaanthal aambal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sengaanthal aambal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodha roja nochi poovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodha roja nochi poovum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival moochil thotridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival moochil thotridum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh angel angel anjo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh angel angel anjo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey anjo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey anjo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vinmeenin pinjo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vinmeenin pinjo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey anjo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey anjo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee konjum men panjo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee konjum men panjo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Min nilavival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Min nilavival"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru ven olithiral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru ven olithiral"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal simitidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal simitidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha newyork silaiyival hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha newyork silaiyival hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjo oru puthuvagai puthumaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjo oru puthuvagai puthumaiyo"/>
</div>
</pre>
