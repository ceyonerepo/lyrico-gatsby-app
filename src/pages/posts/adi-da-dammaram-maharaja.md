---
title: "adi da dammaram song lyrics"
album: "Maharaja"
artist: "D. Imman"
lyricist: "Viveka"
director: "D. Manoharan"
path: "/albums/maharaja-lyrics"
song: "Adi Da Dammaram"
image: ../../images/albumart/maharaja.jpg
date: 2011-12-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3IrezvZ5FU8"
type: "love"
singers:
  - D. Imman
  - Roshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adida dammaaram Adida dammaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adida dammaaram Adida dammaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">damakku damma damakku damma damma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="damakku damma damakku damma damma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Damakku damma Damakku damma damma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damakku damma Damakku damma damma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei thangathula sedhukki vacha bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei thangathula sedhukki vacha bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">un vizhiyilathaan tholaichuputtom enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vizhiyilathaan tholaichuputtom enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei adida dammaaram Adida dammaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei adida dammaaram Adida dammaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">damakku damma damakku damma damma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="damakku damma damakku damma damma"/>
</div>
<div class="lyrico-lyrics-wrapper">nee nadandhaa oiyaaram nee asanja singaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee nadandhaa oiyaaram nee asanja singaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">damakku damma damakku damma damma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="damakku damma damakku damma damma"/>
</div>
<div class="lyrico-lyrics-wrapper">hei sumugamaana savagaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei sumugamaana savagaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thaaren naan thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thaaren naan thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">onakketha mavaraasan naan vaaren naan vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onakketha mavaraasan naan vaaren naan vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">adidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poadu adida dammaaram Adida dammaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poadu adida dammaaram Adida dammaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee nadandhaa oiyaaram nee asanja singaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nadandhaa oiyaaram nee asanja singaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">damakku damma damakku damma damma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="damakku damma damakku damma damma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan maarboaram saanjikkalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan maarboaram saanjikkalaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sugam vaangikkalaam konjam sollivittu poamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugam vaangikkalaam konjam sollivittu poamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey moochaaga vaazhndhukkalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey moochaaga vaazhndhukkalaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">onnaa serndhu poagalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnaa serndhu poagalaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam kannakkaatti poamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam kannakkaatti poamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanthaan unnoada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthaan unnoada"/>
</div>
<div class="lyrico-lyrics-wrapper">neethaan en naattu magaraani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethaan en naattu magaraani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum vaikkaadhey thalaivaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum vaikkaadhey thalaivaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">sinnam vaikkathaan vaa vaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinnam vaikkathaan vaa vaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">un paarvaipattu en aasaitheru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvaipattu en aasaitheru"/>
</div>
<div class="lyrico-lyrics-wrapper">velloattam paarkkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velloattam paarkkudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adida dammaaram Adida dammaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adida dammaaram Adida dammaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">damakku damma damakku damma damma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="damakku damma damakku damma damma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan kan rendum raakkettuthaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan kan rendum raakkettuthaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna moadhittu poanaa summa saathiduven venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna moadhittu poanaa summa saathiduven venaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanney nee ennai suththittupoanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanney nee ennai suththittupoanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">summa paarthirupenaa poi koabamellaam venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa paarthirupenaa poi koabamellaam venaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan ennoada palagaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan ennoada palagaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu rendumthaan sema kaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu rendumthaan sema kaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnappaarthaaley joramaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnappaarthaaley joramaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kai pattaaley gunamaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kai pattaaley gunamaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">M un vaarthai kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="M un vaarthai kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">en neththipottu vekkathil sinungudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en neththipottu vekkathil sinungudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adida dammaaram Adida dammaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adida dammaaram Adida dammaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">damakku damma damakku damma damma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="damakku damma damakku damma damma"/>
</div>
</pre>
