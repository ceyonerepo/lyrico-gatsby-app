---
title: "marmamaai song lyrics"
album: "Thittam Poattu Thirudura Kootam"
artist: "Ashwath"
lyricist: "Niranjan Bharathi"
director: "Sudhar"
path: "/albums/thittam-poattu-thirudura-kootam-lyrics"
song: "Marmamaai"
image: ../../images/albumart/thittam-poattu-thirudura-kootam.jpg
date: 2019-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ul6REVWJChs"
type: "love"
singers:
  - Balram Iyer
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">marmamaai yedho nenjin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marmamaai yedho nenjin"/>
</div>
<div class="lyrico-lyrics-wrapper">oram vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oram vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">nuzhaiyudhe unnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuzhaiyudhe unnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">arugile undhan vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arugile undhan vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakkudhe thannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakkudhe thannaale"/>
</div>
<div class="lyrico-lyrics-wrapper">kannodu kangal modhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannodu kangal modhi"/>
</div>
<div class="lyrico-lyrics-wrapper">vaarthaigal kaanaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaarthaigal kaanaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">ponaal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponaal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjodu nenjam sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjodu nenjam sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">mounangal baasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounangal baasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">aanaal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanaal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">indha nilai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha nilai "/>
</div>
<div class="lyrico-lyrics-wrapper">podhum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">vendum vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendum vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">poove innum innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poove innum innum"/>
</div>
<div class="lyrico-lyrics-wrapper">kollum kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollum kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai theendum inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai theendum inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkam vandhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkam vandhal"/>
</div>
<div class="lyrico-lyrics-wrapper">ennil vetkam vetkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennil vetkam vetkam"/>
</div>
<div class="lyrico-lyrics-wrapper">sokkum bodhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokkum bodhe "/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam sikkum sikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam sikkum sikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">marmamaai yedho nenjin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marmamaai yedho nenjin"/>
</div>
<div class="lyrico-lyrics-wrapper">oram vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oram vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">nuzhaiyudhe unnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuzhaiyudhe unnaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chinna chinna vizhigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna chinna vizhigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">pinni pinni ennai ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinni pinni ennai ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaidhu seigiraaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaidhu seigiraaye"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivinil pala pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivinil pala pala"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugal vara vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugal vara vara"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakkam tharugiraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakkam tharugiraye"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagiya neram anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagiya neram anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">andri yaar than inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andri yaar than inge"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan naanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan naanam"/>
</div>
<div class="lyrico-lyrics-wrapper">vidumurai kondal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidumurai kondal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kannam rendai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannam rendai"/>
</div>
<div class="lyrico-lyrics-wrapper">oru murai thandhal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru murai thandhal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">indhal nilai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indhal nilai "/>
</div>
<div class="lyrico-lyrics-wrapper">podhum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">dhik dhik endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhik dhik endru"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu ullam thulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu ullam thulla"/>
</div>
<div class="lyrico-lyrics-wrapper">thithikaadho dhegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thithikaadho dhegam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">solla solla satham indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solla solla satham indri"/>
</div>
<div class="lyrico-lyrics-wrapper">mella mella idhayathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mella mella idhayathai"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudi kolgiraaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudi kolgiraaye"/>
</div>
<div class="lyrico-lyrics-wrapper">thuru thuru vizhigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuru thuru vizhigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">siru siru kalavaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru siru kalavaram"/>
</div>
<div class="lyrico-lyrics-wrapper">nooru tharugiraaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru tharugiraaye"/>
</div>
<div class="lyrico-lyrics-wrapper">mutham sindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mutham sindha"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan sattam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan sattam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">sitham motham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sitham motham"/>
</div>
<div class="lyrico-lyrics-wrapper">vella thittam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vella thittam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">en paadhai un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en paadhai un "/>
</div>
<div class="lyrico-lyrics-wrapper">iru kan kaatume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru kan kaatume"/>
</div>
<div class="lyrico-lyrics-wrapper">en thevai un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thevai un"/>
</div>
<div class="lyrico-lyrics-wrapper">idhal than sollume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhal than sollume"/>
</div>
<div class="lyrico-lyrics-wrapper">indha nilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha nilai"/>
</div>
<div class="lyrico-lyrics-wrapper">pothum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothum pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">vendum vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendum vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">poove innum innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poove innum innum"/>
</div>
<div class="lyrico-lyrics-wrapper">kollum kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollum kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai theendum inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai theendum inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkam vandhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkam vandhal"/>
</div>
<div class="lyrico-lyrics-wrapper">ennil vetkam vetkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennil vetkam vetkam"/>
</div>
<div class="lyrico-lyrics-wrapper">sokkum bodhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokkum bodhe"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam sikkum sikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam sikkum sikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">marmamaai yedho nenjin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marmamaai yedho nenjin"/>
</div>
<div class="lyrico-lyrics-wrapper">oram vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oram vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">nuzhaiyudhe unnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuzhaiyudhe unnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">arugile undhan vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arugile undhan vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakkudhe thannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakkudhe thannaale"/>
</div>
<div class="lyrico-lyrics-wrapper">kannodu kangal modhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannodu kangal modhi"/>
</div>
<div class="lyrico-lyrics-wrapper">vaarthaigal kaanaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaarthaigal kaanaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">ponaal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponaal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjodu nenjam sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjodu nenjam sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">mounangal baasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounangal baasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">aanaal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanaal enna"/>
</div>
</pre>
