---
title: "thavam seyyum karai melay song lyrics"
album: "Theeni"
artist: "Rajesh Murugesan"
lyricist: "Ko Sesha"
director: "Ani.I.V.Sasi"
path: "/albums/theeni-lyrics"
song: "Thavam Seyyum Karai Melay"
image: ../../images/albumart/theeni.jpg
date: 2021-02-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/twW6JJvZ-mY"
type: "Love"
singers:
  - Rajesh Murugesan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thavam seyyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavam seyyum "/>
</div>
<div class="lyrico-lyrics-wrapper">karai melay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karai melay"/>
</div>
<div class="lyrico-lyrics-wrapper">varam serkkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam serkkum "/>
</div>
<div class="lyrico-lyrics-wrapper">alai polay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alai polay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en vaalvodu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalvodu "/>
</div>
<div class="lyrico-lyrics-wrapper">nee kidaithaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kidaithaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">un suvaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un suvaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">en suvaasamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en suvaasamey"/>
</div>
<div class="lyrico-lyrics-wrapper">nodiyellam yugamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nodiyellam yugamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">ini yugamellam namathaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini yugamellam namathaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">mun jenmangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mun jenmangal"/>
</div>
<div class="lyrico-lyrics-wrapper">varum kan munney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varum kan munney"/>
</div>
<div class="lyrico-lyrics-wrapper">naan varugiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan varugiraen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai seravayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai seravayae"/>
</div>
<div class="lyrico-lyrics-wrapper">manasellam neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasellam neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyum sugamum ontrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyum sugamum ontrae"/>
</div>
<div class="lyrico-lyrics-wrapper">madi saaykiraen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madi saaykiraen"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirum unathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirum unathey"/>
</div>
<div class="lyrico-lyrics-wrapper">unathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thavam seyyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavam seyyum "/>
</div>
<div class="lyrico-lyrics-wrapper">karai melay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karai melay"/>
</div>
<div class="lyrico-lyrics-wrapper">varam serkkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam serkkum "/>
</div>
<div class="lyrico-lyrics-wrapper">alai polay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alai polay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thavam seyyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavam seyyum "/>
</div>
<div class="lyrico-lyrics-wrapper">karai melay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karai melay"/>
</div>
<div class="lyrico-lyrics-wrapper">varam serkkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam serkkum "/>
</div>
<div class="lyrico-lyrics-wrapper">alai polay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alai polay"/>
</div>
</pre>
