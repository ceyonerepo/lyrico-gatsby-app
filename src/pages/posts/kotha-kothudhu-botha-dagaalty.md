---
title: "kotha kothudhu botha song lyrics"
album: "Dagaalty"
artist: "Vijay Narain"
lyricist: "Subu"
director: "Vijay Anand"
path: "/albums/dagaalty-song-lyrics"
song: "Kotha Kothudhu Botha"
image: ../../images/albumart/dagaalty.jpg
date: 2020-01-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/I3djMmwAuPw"
type: "Enjoy"
singers:
  - Santhosh Narayanan
  - Govind Vasantha
  - Vijaynarain
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Koththa Koththudhu Boadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththa Koththudhu Boadha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paadha Ini Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadha Ini Mela Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththa Koththudhu Boadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththa Koththudhu Boadha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paadha Ini Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadha Ini Mela Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththa Kaththudhu Koottam Pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththa Kaththudhu Koottam Pinnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pacha Pasun Thoottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Pasun Thoottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Moottam Adhil Aattam Paattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Moottam Adhil Aattam Paattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Poyi Serum Theriyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Poyi Serum Theriyadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaakkuren Porula Thookkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakkuren Porula Thookkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyil Sekkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyil Sekkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Vazhiyila Pazhigala Kuzhigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Vazhiyila Pazhigala Kuzhigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagida Paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagida Paakkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theduren Thirumbi Paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theduren Thirumbi Paakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Illa Oduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Illa Oduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaamadhamaa Nyaanam Porandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaamadhamaa Nyaanam Porandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Palakkura Porula Kandaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Palakkura Porula Kandaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Pal Izhichu Tholaikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pal Izhichu Tholaikkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnukkum Porulukkum Edaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnukkum Porulukkum Edaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thavikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thavikkiriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththa Koththudhu Boadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththa Koththudhu Boadha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paadha Ini Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadha Ini Mela Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththa Kaththudhu Koottam Pinnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththa Kaththudhu Koottam Pinnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pacha Pasun Thoottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Pasun Thoottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Moottam Adhil Aattam Paattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Moottam Adhil Aattam Paattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Poyi Serum Theriyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Poyi Serum Theriyadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottamale Seramale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottamale Seramale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal Manasa Irundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal Manasa Irundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Appaviya Ponna Kanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appaviya Ponna Kanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Peiyum Erangum Perumbaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peiyum Erangum Perumbaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkalunnu Vanthakka Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkalunnu Vanthakka Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachondhiyum Niram Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachondhiyum Niram Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dagaalty Nee Kaasa Kanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagaalty Nee Kaasa Kanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaruviye Dhinam Dhoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaruviye Dhinam Dhoram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puriyudhu Da Maame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyudhu Da Maame"/>
</div>
<div class="lyrico-lyrics-wrapper">Right Edhu Wrong Edhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right Edhu Wrong Edhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Velangudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velangudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Sila Pala Karunthungalaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Sila Pala Karunthungalaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakaththa Nee Kalaikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakaththa Nee Kalaikkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkula Thoongura Lion-ah Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkula Thoongura Lion-ah Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ezhuppuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ezhuppuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththa Koththudhu Boadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththa Koththudhu Boadha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paadha Ini Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadha Ini Mela Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththa Kaththudhu Koottam Pinnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththa Kaththudhu Koottam Pinnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pacha Pasun Thoottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha Pasun Thoottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Moottam Adhil Aattam Paattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Moottam Adhil Aattam Paattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Poyi Serum Theriyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Poyi Serum Theriyadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelinjicha Unmai Velangicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelinjicha Unmai Velangicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla Erangicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Erangicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Varundhura Manasukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Varundhura Manasukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Virundhula Marundhiila Purinjichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virundhula Marundhiila Purinjichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaya Patta En Manasoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaya Patta En Manasoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Velaga Paathirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Velaga Paathirunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumba Varum Varaiyila Karaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumba Varum Varaiyila Karaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kaathiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kaathiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Putham Pudhu Paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putham Pudhu Paadha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paadha Ini Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadha Ini Mela Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattam Kattum Aattam Ini Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattam Kattum Aattam Ini Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli Adikkum Neram Ini Veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Adikkum Neram Ini Veeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Yerum Yerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Yerum Yerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Beram Pesa Yaarum Kidaiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beram Pesa Yaarum Kidaiyathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththa Koththudhu Boadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththa Koththudhu Boadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Putham Pudhu Paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putham Pudhu Paadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Boadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Koththa Koththudhu Boadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththa Koththudhu Boadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Putham Pudhu Paadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putham Pudhu Paadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Mela"/>
</div>
</pre>
