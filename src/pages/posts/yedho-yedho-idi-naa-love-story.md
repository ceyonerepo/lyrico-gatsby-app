---
title: "yedho yedho song lyrics"
album: "Idi Naa Love Story"
artist: "Srinath Vijay"
lyricist: "Gopi - Ramesh"
director: "Ramesh Gopi"
path: "/albums/idi-naa-love-story-lyrics"
song: "Yedho Yedho"
image: ../../images/albumart/idi-naa-love-story.jpg
date: 2018-02-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/j0gvv3TBoUw"
type: "love"
singers:
  -	Naresh Iyer
  - Priya Himesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yedho Yedho Cheyyakamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Yedho Cheyyakamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaga Naaku O Muddhe Ivvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaga Naaku O Muddhe Ivvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Yedho Jaruguthu Vundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Yedho Jaruguthu Vundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthu Nanne Thakkithe Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthu Nanne Thakkithe Nuvve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kalle Naake Ishtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalle Naake Ishtam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pedhavula Navve Ishtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pedhavula Navve Ishtam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Chese Allari Ishtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Chese Allari Ishtam"/>
</div>
<div class="lyrico-lyrics-wrapper">Crazy Thelusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crazy Thelusa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Maatalu Yentho Ishtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maatalu Yentho Ishtam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Choose Choopulu Ishtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Choose Choopulu Ishtam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Choope Preme Ishtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Choope Preme Ishtam"/>
</div>
<div class="lyrico-lyrics-wrapper">Praanam Thelusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praanam Thelusa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Yedho Cheyyakamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Yedho Cheyyakamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaga Naaku O Muddhe Ivvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaga Naaku O Muddhe Ivvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Yedho Jaruguthu Vundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Yedho Jaruguthu Vundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthu Nanne Thakkithe Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthu Nanne Thakkithe Nuvve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muddhutho Nee Pedhavula Paine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhutho Nee Pedhavula Paine"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopana Naa Premane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopana Naa Premane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siggutho Nee Muddhuku Badhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siggutho Nee Muddhuku Badhule"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherana Nee Kougililo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherana Nee Kougililo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inthandham Yedhuta Vundaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthandham Yedhuta Vundaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasemo Oorukodhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasemo Oorukodhuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalasyam Yendhuko Ika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalasyam Yendhuko Ika"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthaa Nee Sonthame Kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthaa Nee Sonthame Kadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaggaragaane Nuvvunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaggaragaane Nuvvunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemi Thochadhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemi Thochadhuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Yedho Cheyyakamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Yedho Cheyyakamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaga Naaku O Muddhe Ivvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaga Naaku O Muddhe Ivvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Yedho Jaruguthu Vundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Yedho Jaruguthu Vundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthu Nanne Thakkithe Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthu Nanne Thakkithe Nuvve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Botthigaa Naa Maate Vinava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Botthigaa Naa Maate Vinava"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapava Nee Allarine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapava Nee Allarine"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chilipigaa Nee Nadumunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipigaa Nee Nadumunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Muddhaadana O Saaraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Muddhaadana O Saaraina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu Chese Chilipi Panulaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Chese Chilipi Panulaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Naa Manasu Korene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Naa Manasu Korene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Korika Naaku Thelusule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Korika Naaku Thelusule"/>
</div>
<div class="lyrico-lyrics-wrapper">Thvaragaa Naa Odiki Cherave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thvaragaa Naa Odiki Cherave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanne Nuvvu Choosthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Nuvvu Choosthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigge Aagadhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigge Aagadhuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Yedho Cheyyakamundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Yedho Cheyyakamundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaga Naaku O Muddhe Ivvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaga Naaku O Muddhe Ivvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Yedho Jaruguthu Vundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Yedho Jaruguthu Vundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosthu Nanne Thakkithe Nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosthu Nanne Thakkithe Nuvve"/>
</div>
</pre>
