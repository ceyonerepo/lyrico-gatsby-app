---
title: "urimayai meetpom song lyrics"
album: "Kaala"
artist: "Santhosh Narayanan"
lyricist: "Arivu"
director: "Pa. Ranjith"
path: "/albums/kaala-lyrics"
song: "Urimayai Meetpom"
image: ../../images/albumart/kaala.jpg
date: 2018-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MdFq3-TQZy8"
type: "mass"
singers:
  - Ananthu
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Urimaiyai meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyai meetpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooooooooomm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooooooooomm"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaiyai meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyai meetpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaiyaieee meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyaieee meetpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaiyai meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyai meetpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaiyai meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyai meetpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooooo ooooooooomm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooo ooooooooomm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar vachathu yaar vachathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar vachathu yaar vachathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sattamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sattamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vaazhvenbathum saavenbathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vaazhvenbathum saavenbathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelam mattumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelam mattumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar vachathu yaar vachathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar vachathu yaar vachathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sattamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sattamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vaazhvenbathum saavenbathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vaazhvenbathum saavenbathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelam mattumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelam mattumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy eeyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy eeyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Evano vanthu vethachan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evano vanthu vethachan"/>
</div>
<div class="lyrico-lyrics-wrapper">Atho evano vanthu aruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atho evano vanthu aruthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba karutha atha valathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba karutha atha valathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan pasiyaa thudichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan pasiyaa thudichaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moraiyaa thala moraiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moraiyaa thala moraiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi vazhiyaa ina mozhiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi vazhiyaa ina mozhiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada pirindhae kidandhavanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada pirindhae kidandhavanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa nimirnthaan nimirnthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa nimirnthaan nimirnthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai urimai urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai urimai urimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai urimai urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai urimai urimai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy eyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy eyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Idiya oru puyala vanthu erangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idiya oru puyala vanthu erangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma padaiyum atha thadutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma padaiyum atha thadutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha karaiyum ini udaiyum udaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha karaiyum ini udaiyum udaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vithaiya chinna vithaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithaiya chinna vithaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu vizhundhom siru thuliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu vizhundhom siru thuliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathai kizhindhae mella ezhunthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathai kizhindhae mella ezhunthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum mazhaiya mazhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum mazhaiya mazhaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aahaapuzhuthi udaiyaai anindhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaapuzhuthi udaiyaai anindhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Viyarvai nediyaal kunindhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viyarvai nediyaal kunindhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruthi vazhiya varainthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruthi vazhiya varainthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuthaan ulagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuthaan ulagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaavidiyum vidiyum endrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaavidiyum vidiyum endrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulil kidanthom indrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulil kidanthom indrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oliyai thiranthom ondraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyai thiranthom ondraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhae serndhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhae serndhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai urimai urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai urimai urimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai urimai urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai urimai urimai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar vachathu yaar vachathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar vachathu yaar vachathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sattamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sattamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vaazhvenbathum saavenbathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vaazhvenbathum saavenbathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelam mattumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelam mattumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal urimai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalavum vazhi illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalavum vazhi illada"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimaiyum nee illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimaiyum nee illada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadulaam meedulaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadulaam meedulaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulachathu un nilam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulachathu un nilam da"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangi vazhndhaaka mudiyathamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangi vazhndhaaka mudiyathamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaiya vaangama uyir poguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiya vaangama uyir poguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhundhu vaada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhu vaada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirthu nee kelu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirthu nee kelu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayandha agaathu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayandha agaathu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Inamae un koodathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inamae un koodathan"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelatha meettukkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelatha meettukkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelamaa maathikkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelamaa maathikkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaaga iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaaga iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini namma kaalamthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini namma kaalamthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai urimai urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai urimai urimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai urimai urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai urimai urimai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannangala theettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannangala theettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu vaanavilin koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu vaanavilin koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada sondham ethu ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada sondham ethu ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha vinmeen-eh kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha vinmeen-eh kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathavillatha koottil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathavillatha koottil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal yeralam undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal yeralam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaikku ondraagum veettil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaikku ondraagum veettil"/>
</div>
<div class="lyrico-lyrics-wrapper">Viduthalai eppothum undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduthalai eppothum undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athigaram thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athigaram thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenappa maathi kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenappa maathi kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaikkum kaigalukkae naadu naadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaikkum kaigalukkae naadu naadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adakkum kaalamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakkum kaalamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkum veli illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkum veli illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedithu poraadalam bayamae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedithu poraadalam bayamae illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai urimai urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai urimai urimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilamae enga urimai urimai urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamae enga urimai urimai urimai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar vachathu yaar vachathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar vachathu yaar vachathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sattamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sattamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vaazhvenbathum saavenbathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vaazhvenbathum saavenbathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelam mattumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelam mattumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urimaiyai meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyai meetpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaiyai meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyai meetpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaiyai meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyai meetpom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar vachathu yaar vachathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar vachathu yaar vachathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sattamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sattamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vaazhvenbathum saavenbathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vaazhvenbathum saavenbathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelam mattumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelam mattumada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urimaiee e
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiee e"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetpom meetpom meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetpom meetpom meetpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaiyai meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyai meetpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimayai meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimayai meetpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Meetpom meetpom meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meetpom meetpom meetpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaiyai meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyai meetpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaiyai meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyai meetpom"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaiyai meetpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyai meetpom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar vachathu yaar vachathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar vachathu yaar vachathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sattamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sattamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vaazhvenbathum saavenbathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vaazhvenbathum saavenbathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelam mattumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelam mattumada"/>
</div>
</pre>
