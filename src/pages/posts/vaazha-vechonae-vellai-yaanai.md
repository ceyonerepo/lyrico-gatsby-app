---
title: "vaazha vechonae song lyrics"
album: "Vellai Yaanai"
artist: "Santhosh Narayanan"
lyricist: "Arivu"
director: "Subramaniam Siva"
path: "/albums/vellai-yaanai-lyrics"
song: "Vaazha Vechonae"
image: ../../images/albumart/vellai-yaanai.jpg
date: 2021-07-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FxwZpb1vi18"
type: "sad"
singers:
  - Vijaynarain
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vellaiyila vegum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellaiyila vegum"/>
</div>
<div class="lyrico-lyrics-wrapper">pattadathu vaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattadathu vaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">pathala thooruthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathala thooruthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">poo vedhachonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo vedhachonae"/>
</div>
<div class="lyrico-lyrics-wrapper">adha kutha vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adha kutha vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">kelumaiya koodazhichonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelumaiya koodazhichonae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mannarachu poosunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannarachu poosunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">punnu sugamagum ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnu sugamagum ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">santhathiku serthu vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhathiku serthu vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">seerazhichone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seerazhichone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nattavidha paal kudikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nattavidha paal kudikum"/>
</div>
<div class="lyrico-lyrics-wrapper">aathoda enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathoda enga"/>
</div>
<div class="lyrico-lyrics-wrapper">pacharisi koni ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacharisi koni ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">moolga vitome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moolga vitome"/>
</div>
<div class="lyrico-lyrics-wrapper">indha mannupuzhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha mannupuzhu"/>
</div>
<div class="lyrico-lyrics-wrapper">mugamchulikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugamchulikka"/>
</div>
<div class="lyrico-lyrics-wrapper">marundhu vechone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marundhu vechone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaazha vechonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazha vechonae"/>
</div>
<div class="lyrico-lyrics-wrapper">thennanthaazha vechonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thennanthaazha vechonae"/>
</div>
<div class="lyrico-lyrics-wrapper">inga kaalvayithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga kaalvayithu"/>
</div>
<div class="lyrico-lyrics-wrapper">koozhuku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koozhuku than"/>
</div>
<div class="lyrico-lyrics-wrapper">maaraduchonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaraduchonae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">por adichonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="por adichonae"/>
</div>
<div class="lyrico-lyrics-wrapper">alli neer yerachonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli neer yerachonae"/>
</div>
<div class="lyrico-lyrics-wrapper">ippo oor thirumbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippo oor thirumbi"/>
</div>
<div class="lyrico-lyrics-wrapper">pogamattum usuru vechonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogamattum usuru vechonae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yezhaiyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yezhaiyellam"/>
</div>
<div class="lyrico-lyrics-wrapper">kodutha vervaiyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodutha vervaiyellam"/>
</div>
<div class="lyrico-lyrics-wrapper">kanaku paarthathuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaku paarthathuna"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam or sadham than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam or sadham than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yezhaiyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yezhaiyellam"/>
</div>
<div class="lyrico-lyrics-wrapper">kodutha vervaiyellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodutha vervaiyellam"/>
</div>
<div class="lyrico-lyrics-wrapper">kanaku paarthathuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaku paarthathuna"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam or sadham than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam or sadham than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pasi theerthavan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasi theerthavan than"/>
</div>
<div class="lyrico-lyrics-wrapper">vali kettu vanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali kettu vanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhavum sathirundhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhavum sathirundhana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaazha vechonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazha vechonae"/>
</div>
<div class="lyrico-lyrics-wrapper">thennanthaazha vechonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thennanthaazha vechonae"/>
</div>
<div class="lyrico-lyrics-wrapper">inga kalvayithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga kalvayithu"/>
</div>
<div class="lyrico-lyrics-wrapper">koozhuku than maaradichonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koozhuku than maaradichonae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">por adichonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="por adichonae"/>
</div>
<div class="lyrico-lyrics-wrapper">alli neer yerachonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli neer yerachonae"/>
</div>
<div class="lyrico-lyrics-wrapper">ippo oor thirumbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippo oor thirumbi"/>
</div>
<div class="lyrico-lyrics-wrapper">pogamatum usuru vechonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogamatum usuru vechonae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh oh oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">nelatha keeri vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nelatha keeri vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">vedicha paadhamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedicha paadhamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">varappa kaanudhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varappa kaanudhaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaanamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">karanja kaagamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karanja kaagamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">varanja kolamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varanja kolamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">maranju ponadhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranju ponadhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">valartha oora ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valartha oora ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">vittu vera ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittu vera ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">pirinja vedhana thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirinja vedhana thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaazha vechonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazha vechonae"/>
</div>
<div class="lyrico-lyrics-wrapper">thennanthaazha vechonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thennanthaazha vechonae"/>
</div>
<div class="lyrico-lyrics-wrapper">inga kaalvayithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga kaalvayithu"/>
</div>
<div class="lyrico-lyrics-wrapper">koozhuku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koozhuku than"/>
</div>
<div class="lyrico-lyrics-wrapper">maaraduchonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaraduchonae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">por adichonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="por adichonae"/>
</div>
<div class="lyrico-lyrics-wrapper">alli neer yerachonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli neer yerachonae"/>
</div>
<div class="lyrico-lyrics-wrapper">ippo oor thirumbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippo oor thirumbi"/>
</div>
<div class="lyrico-lyrics-wrapper">pogamattum usuru vechonae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogamattum usuru vechonae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ohohoh ohohoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohohoh ohohoh"/>
</div>
<div class="lyrico-lyrics-wrapper">ohohoh ohohoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohohoh ohohoh"/>
</div>
<div class="lyrico-lyrics-wrapper">ohohoh ohohoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohohoh ohohoh"/>
</div>
<div class="lyrico-lyrics-wrapper">ohohoh ohohooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohohoh ohohooh"/>
</div>
</pre>
