---
title: "entha chuda chakande song lyrics"
album: "Raahu"
artist: "Praveen Lakkaraju"
lyricist: "Subbu Vedula"
director: "Subbu Vedula"
path: "/albums/raahu-lyrics"
song: "Entha Chuda Chakande"
image: ../../images/albumart/raahu.jpg
date: 2020-02-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/oK2eAReUSDg"
type: "love"
singers:
  - Anurag Kulkarni
  - Sahithi Chaganti
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Entha chuda chakkande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha chuda chakkande"/>
</div>
<div class="lyrico-lyrics-wrapper">Bugga sotta aaratale daachipettinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bugga sotta aaratale daachipettinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalle chaal chala cheddave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalle chaal chala cheddave"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigge leka kanne pilla olle gillaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigge leka kanne pilla olle gillaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neetho pakke panchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho pakke panchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanna jaaji kondamalli guppu mannaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanna jaaji kondamalli guppu mannaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake intha andame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake intha andame"/>
</div>
<div class="lyrico-lyrics-wrapper">vinnapaale petti nuvvu petti puttave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnapaale petti nuvvu petti puttave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha chuda chakkande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha chuda chakkande"/>
</div>
<div class="lyrico-lyrics-wrapper">Bugga sotta aaratale daachipettinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bugga sotta aaratale daachipettinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalle chaal chala cheddave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalle chaal chala cheddave"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigge leka kanne pilla olle gillaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigge leka kanne pilla olle gillaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pichakinche raa entho mathekkinche raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichakinche raa entho mathekkinche raa"/>
</div>
<div class="lyrico-lyrics-wrapper">konte korita sontha gunde cheppale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konte korita sontha gunde cheppale"/>
</div>
<div class="lyrico-lyrics-wrapper">mukkuku mukke raa adi mudduku mudhe raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mukkuku mukke raa adi mudduku mudhe raa"/>
</div>
<div class="lyrico-lyrics-wrapper">patte mancham pai kadhalenno raayali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patte mancham pai kadhalenno raayali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannula kankshala erupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula kankshala erupe"/>
</div>
<div class="lyrico-lyrics-wrapper">ika niddura dooramu raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ika niddura dooramu raa"/>
</div>
<div class="lyrico-lyrics-wrapper">kuduruga undani pedave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuduruga undani pedave"/>
</div>
<div class="lyrico-lyrics-wrapper">nara naramuna sega impaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nara naramuna sega impaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha chuda chakkande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha chuda chakkande"/>
</div>
<div class="lyrico-lyrics-wrapper">Bugga sotta aaratale daachipettinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bugga sotta aaratale daachipettinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalle chaal chala cheddave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalle chaal chala cheddave"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigge leka kanne pilla olle gillaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigge leka kanne pilla olle gillaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo Mudhu gunnave ika hadde daatale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo Mudhu gunnave ika hadde daatale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kantireppalu nannu daachipettale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kantireppalu nannu daachipettale"/>
</div>
<div class="lyrico-lyrics-wrapper">Repu maapullo nee preme nindale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repu maapullo nee preme nindale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Neeli kallalo kalalenno chudale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Neeli kallalo kalalenno chudale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nudutana kumkuma pasupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nudutana kumkuma pasupe"/>
</div>
<div class="lyrico-lyrics-wrapper">ika iddaru okate kadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ika iddaru okate kadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aaa chedirina mallela kurula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaa chedirina mallela kurula"/>
</div>
<div class="lyrico-lyrics-wrapper">kala kalamulu vinapada laa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kala kalamulu vinapada laa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha chuda chakkande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha chuda chakkande"/>
</div>
<div class="lyrico-lyrics-wrapper">Bugga sotta aaratale daachipettinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bugga sotta aaratale daachipettinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalle chaal chala cheddave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalle chaal chala cheddave"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigge leka kanne pilla olle gillaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigge leka kanne pilla olle gillaye"/>
</div>
</pre>
