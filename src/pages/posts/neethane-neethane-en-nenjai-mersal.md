---
title: "neethane neethane en nenjai song lyrics"
album: "Mersal"
artist: "A.R. Rahman"
lyricist: "Vivek"
director: "Atlee"
path: "/albums/mersal-lyrics"
song: "Neethane Neethane En Nenjai"
image: ../../images/albumart/mersal.jpg
date: 2017-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fWajtP80g54"
type: "Love"
singers:
  - A R Rahman
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neethane Neethane En 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethane Neethane En "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Thattum Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Thattum Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaai Udaindhaen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai Udaindhaen "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae Artham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae Artham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethanae Neethanae En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethanae Neethanae En"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Thattum Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Thattum Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaai Udaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai Udaindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Artham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Artham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Maalai Vaanam Moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maalai Vaanam Moththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irul Poosi Kollum Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul Poosi Kollum Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Neeyum Naanum Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Neeyum Naanum Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Kavithaiyo Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Kavithaiyo Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethanae Neethanae En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethanae Neethanae En"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Thedum Inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Thedum Inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirin Thiraiyil Un Paal Pimbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Thiraiyil Un Paal Pimbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Kadhal Katril Patrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kadhal Katril Patrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Vaanin Kaadhil Ettum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Vaanin Kaadhil Ettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Kaiyil Maatri Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Kaiyil Maatri Kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon Thingal Vizhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon Thingal Vizhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yachey Yachey Yachey Yachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yachey Yachey Yachey Yachey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yachey Yachey Yachey Yachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yachey Yachey Yachey Yachey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yachey Yachey Yachey Yachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yachey Yachey Yachey Yachey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli Maiyyal Undaachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli Maiyyal Undaachey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yachey Yachey Yachey Yachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yachey Yachey Yachey Yachey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yachey Yachey Yachey Yachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yachey Yachey Yachey Yachey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yachey Yachey Yachey Yachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yachey Yachey Yachey Yachey"/>
</div>
<div class="lyrico-lyrics-wrapper">Avai Maiyam Kondachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avai Maiyam Kondachey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethane Neethane En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethane Neethane En"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Thattum Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Thattum Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaai Udaindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai Udaindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Maalai Vaanam Moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maalai Vaanam Moththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irul Poosi Kollum Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul Poosi Kollum Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Neeyum Naanum Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Neeyum Naanum Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Kavithaiyo Ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Kavithaiyo Ohh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaley Yaaley Yaaley Yaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaley Yaaley Yaaley Yaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaley Yaaley Yaaley Yaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaley Yaaley Yaaley Yaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaley Yaaley Yaaley Yaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaley Yaaley Yaaley Yaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aasai Sollaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aasai Sollaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaley Yaaley Yaaley Yaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaley Yaaley Yaaley Yaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaley Yaaley Yaaley Yaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaley Yaaley Yaaley Yaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaley Yaaley Yaaley Yaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaley Yaaley Yaaley Yaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhageri Selvaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhageri Selvaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethanae Neethanae En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethanae Neethanae En"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Thattum Satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Thattum Satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Alagaai Udainthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alagaai Udainthaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae Artham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae Artham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Maalai Vaanam Motham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maalai Vaanam Motham"/>
</div>
<div class="lyrico-lyrics-wrapper">Irul Poosi Kollum Satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul Poosi Kollum Satham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Neeyum Naanum Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Neeyum Naanum Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Kavithaiyo Ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Kavithaiyo Ohh"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethane Neethane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethane Neethane"/>
</div>
</pre>
