---
title: "theeranadhi song lyrics"
album: "Maara"
artist: "Ghibran"
lyricist: "Thamarai"
director: "Dhilip Kumar"
path: "/albums/maara-song-lyrics"
song: "Theeranadhi"
image: ../../images/albumart/maara.jpg
date: 2021-01-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fyFwLwx-HaA"
type: "love"
singers:
  - Padmalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Eththanai naal aagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththanai naal aagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamellam thaagamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamellam thaagamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipadiyae pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipadiyae pogumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan uyirea theerumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan uyirea theerumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeraanadhi theeraanadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraanadhi theeraanadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedalkalo theeraathini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalkalo theeraathini"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangiyae varuguthu en vaasal vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangiyae varuguthu en vaasal vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaivatho nagarvatho vaazhvin vithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaivatho nagarvatho vaazhvin vithi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaasam illamal vivaatham seiyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaasam illamal vivaatham seiyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ingu yaen nindrean kooraai saghi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ingu yaen nindrean kooraai saghi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeraanadhi theeraanadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraanadhi theeraanadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedalkalo theeraathini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalkalo theeraathini"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangiyae varuguthu en vaasal vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangiyae varuguthu en vaasal vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaivatho nagarvatho vaazhvin vithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaivatho nagarvatho vaazhvin vithi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eththanai naal aagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththanai naal aagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamellam thaagamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamellam thaagamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipadiyae pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipadiyae pogumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan uyirea theerumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan uyirea theerumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagumo thaagamopogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagumo thaagamopogumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmm aruginil naan irunthean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm aruginil naan irunthean"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaivinil ne irunthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivinil ne irunthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru kai neetukirean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru kai neetukirean"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirinil vaaraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirinil vaaraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum kaanatha ragasiya kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum kaanatha ragasiya kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanum neeyumthaan inaiththidum paalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanum neeyumthaan inaiththidum paalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedum meenaai neeril naan irunthean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum meenaai neeril naan irunthean"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaivil ne irunthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivil ne irunthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeraanadhi theeraanadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraanadhi theeraanadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedalkaloo theeraathini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalkaloo theeraathini"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangiyae varuguthu en vaasal vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangiyae varuguthu en vaasal vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaivatho nagarvatho vaazhvin vithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaivatho nagarvatho vaazhvin vithi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathanimaiyin thoorigaiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathanimaiyin thoorigaiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Panimalar naan varainthean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panimalar naan varainthean"/>
</div>
<div class="lyrico-lyrics-wrapper">Maraivin suriyanaal karainthidalaanean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraivin suriyanaal karainthidalaanean"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro kaanum or kanavinil naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro kaanum or kanavinil naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraa kaaloda nuzhainthathum yaenaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraa kaaloda nuzhainthathum yaenaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanal poley neeril naan nanainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanal poley neeril naan nanainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nathiyaai maarugindrean
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nathiyaai maarugindrean"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeraanadhi theeraanadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraanadhi theeraanadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedalkaloo theeraathini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalkaloo theeraathini"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangiyae varuguthu en vaasal vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangiyae varuguthu en vaasal vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaivatho nagarvatho vaazhvin vithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaivatho nagarvatho vaazhvin vithi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaasam illamal vivatham seiyamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaasam illamal vivatham seiyamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nan ingu yaen nindrean kooraai saghi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan ingu yaen nindrean kooraai saghi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eththanai naal aagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththanai naal aagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamellam thaagamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamellam thaagamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipadiyae pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipadiyae pogumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan uyirea theerumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan uyirea theerumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeraanadhi theeraanadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraanadhi theeraanadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedalkaloo theeraathini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalkaloo theeraathini"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraanadhi theeraanadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraanadhi theeraanadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedalkaloo theeraathini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalkaloo theeraathini"/>
</div>
</pre>
