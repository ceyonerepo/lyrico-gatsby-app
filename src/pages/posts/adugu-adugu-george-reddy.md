---
title: "adugu adugu song lyrics"
album: "George Reddy"
artist: "Suresh Bobbili"
lyricist: "MEGH-uh-WATT"
director: "Jeevan Reddy"
path: "/albums/george-reddy-lyrics"
song: "Adugu Adugu"
image: ../../images/albumart/george-reddy.jpg
date: 2019-11-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/8Gp7A7AP32w"
type: "mass"
singers:
  - Revanth
  - MEGH-uh-WATT
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Heyy raise up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy raise up"/>
</div>
<div class="lyrico-lyrics-wrapper">go get up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="go get up"/>
</div>
<div class="lyrico-lyrics-wrapper">move forward
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="move forward"/>
</div>
<div class="lyrico-lyrics-wrapper">hey fighter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey fighter"/>
</div>
<div class="lyrico-lyrics-wrapper">neelo ne vundhi ra 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelo ne vundhi ra "/>
</div>
<div class="lyrico-lyrics-wrapper">prashninchey ah staminaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prashninchey ah staminaa"/>
</div>
<div class="lyrico-lyrics-wrapper">No stopping you right 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No stopping you right "/>
</div>
<div class="lyrico-lyrics-wrapper">now you are the power house
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="now you are the power house"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvey ra andariki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvey ra andariki "/>
</div>
<div class="lyrico-lyrics-wrapper">oraade ah warrior
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oraade ah warrior"/>
</div>
<div class="lyrico-lyrics-wrapper">ohhh adugu adugu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohhh adugu adugu "/>
</div>
<div class="lyrico-lyrics-wrapper">maa prathi adugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa prathi adugu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee venakale maa parugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee venakale maa parugu"/>
</div>
<div class="lyrico-lyrics-wrapper">adugu adugu ma madhini adugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugu adugu ma madhini adugu"/>
</div>
<div class="lyrico-lyrics-wrapper">anthaa nuveyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anthaa nuveyy"/>
</div>
<div class="lyrico-lyrics-wrapper">cheekati darlua veluturu vai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheekati darlua veluturu vai"/>
</div>
<div class="lyrico-lyrics-wrapper">velugandinchey suryudivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velugandinchey suryudivi"/>
</div>
<div class="lyrico-lyrics-wrapper">maalo okadiga nuv odhigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalo okadiga nuv odhigi"/>
</div>
<div class="lyrico-lyrics-wrapper">nadipisthaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadipisthaave"/>
</div>
<div class="lyrico-lyrics-wrapper">now puli let's now 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="now puli let's now "/>
</div>
<div class="lyrico-lyrics-wrapper">ventabadi take its the crown
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ventabadi take its the crown"/>
</div>
<div class="lyrico-lyrics-wrapper">you are the one
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you are the one"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvey maaku yepudu gunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvey maaku yepudu gunde"/>
</div>
<div class="lyrico-lyrics-wrapper">dhairyam neetho vunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhairyam neetho vunte "/>
</div>
<div class="lyrico-lyrics-wrapper">memu moveavthaamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="memu moveavthaamu"/>
</div>
<div class="lyrico-lyrics-wrapper">He's the waiter on 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He's the waiter on "/>
</div>
<div class="lyrico-lyrics-wrapper">the scale of waiter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="the scale of waiter"/>
</div>
<div class="lyrico-lyrics-wrapper">he is staabale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="he is staabale"/>
</div>
<div class="lyrico-lyrics-wrapper">he is the free lace ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="he is the free lace ya"/>
</div>
<div class="lyrico-lyrics-wrapper">he is the best yet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="he is the best yet"/>
</div>
<div class="lyrico-lyrics-wrapper">don't you get yet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="don't you get yet"/>
</div>
<div class="lyrico-lyrics-wrapper">ohhohoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohhohoo"/>
</div>
<div class="lyrico-lyrics-wrapper">heyy adugu adugu ma prathi adugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heyy adugu adugu ma prathi adugu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee venakaale maa parugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee venakaale maa parugu"/>
</div>
<div class="lyrico-lyrics-wrapper">adugu adugu maa madhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adugu adugu maa madhi "/>
</div>
<div class="lyrico-lyrics-wrapper">ni adugu anthaaa nuvveyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ni adugu anthaaa nuvveyy"/>
</div>
<div class="lyrico-lyrics-wrapper">cheekati daarula veluturi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheekati daarula veluturi "/>
</div>
<div class="lyrico-lyrics-wrapper">vai velugandinchey suryudivi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vai velugandinchey suryudivi"/>
</div>
<div class="lyrico-lyrics-wrapper">maalo okadiga nuv 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalo okadiga nuv "/>
</div>
<div class="lyrico-lyrics-wrapper">odhigi nadipisthaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odhigi nadipisthaave"/>
</div>
</pre>
