---
title: "uyire en uyire song lyrics"
album: "Arasakulam"
artist: "Velan Sakadevan"
lyricist: "Padmavathi"
director: "Kumaramaran"
path: "/albums/arasakulam-lyrics"
song: "Uyire En Uyire"
image: ../../images/albumart/arasakulam.jpg
date: 2017-03-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/P1bZEY6S8ak"
type: "love"
singers:
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">uyire en uyire ada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire en uyire ada "/>
</div>
<div class="lyrico-lyrics-wrapper">thaniya thavichen nane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniya thavichen nane"/>
</div>
<div class="lyrico-lyrics-wrapper">unake ada unake en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unake ada unake en"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaya solven thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaya solven thane"/>
</div>
<div class="lyrico-lyrics-wrapper">nadu saamam thoongum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadu saamam thoongum"/>
</div>
<div class="lyrico-lyrics-wrapper">nerathil ada kathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerathil ada kathil"/>
</div>
<div class="lyrico-lyrics-wrapper">oru padal ketka pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru padal ketka pidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadigaram eluppum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadigaram eluppum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">athai nanum anaikamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai nanum anaikamal"/>
</div>
<div class="lyrico-lyrics-wrapper">thoonga pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoonga pidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">enakenna pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakenna pidikum"/>
</div>
<div class="lyrico-lyrics-wrapper">ena yaarum ketathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena yaarum ketathu"/>
</div>
<div class="lyrico-lyrics-wrapper">illai en uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai en uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">malai mele nan yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai mele nan yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">oo vena solvene un peyarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo vena solvene un peyarai"/>
</div>
<div class="lyrico-lyrics-wrapper">silu silu malaiyum thoora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silu silu malaiyum thoora"/>
</div>
<div class="lyrico-lyrics-wrapper">suda suda nanainthida vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suda suda nanainthida vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thuli thuli saaral veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuli thuli saaral veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">pani athil karanthida vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani athil karanthida vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vilunthida vaa ulagaithi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilunthida vaa ulagaithi "/>
</div>
<div class="lyrico-lyrics-wrapper">nee maranthida vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee maranthida vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyire en uyire ada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire en uyire ada "/>
</div>
<div class="lyrico-lyrics-wrapper">thaniya thavichen nane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniya thavichen nane"/>
</div>
<div class="lyrico-lyrics-wrapper">unake ada unake en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unake ada unake en"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaya solven thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaya solven thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kula saami kovilula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kula saami kovilula"/>
</div>
<div class="lyrico-lyrics-wrapper">sabai ellam koodum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sabai ellam koodum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">maatu vandi naan otanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatu vandi naan otanum"/>
</div>
<div class="lyrico-lyrics-wrapper">alagaga nethi mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagaga nethi mele"/>
</div>
<div class="lyrico-lyrics-wrapper">puruvathai seera vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puruvathai seera vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannadi naan pakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannadi naan pakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirathil parakum ratimai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirathil parakum ratimai"/>
</div>
<div class="lyrico-lyrics-wrapper">thuyarathil sirikanum sathiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyarathil sirikanum sathiyama"/>
</div>
<div class="lyrico-lyrics-wrapper">unakathu ulukuthu kulunguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakathu ulukuthu kulunguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">valayathu koodanuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valayathu koodanuma"/>
</div>
<div class="lyrico-lyrics-wrapper">kinathula kuthikanum ragasiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinathula kuthikanum ragasiyama"/>
</div>
<div class="lyrico-lyrics-wrapper">athukula kulikanum puthu sugamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athukula kulikanum puthu sugamai"/>
</div>
<div class="lyrico-lyrics-wrapper">paduchathum pidichathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paduchathum pidichathu"/>
</div>
<div class="lyrico-lyrics-wrapper">rasichatha atha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasichatha atha solla"/>
</div>
<div class="lyrico-lyrics-wrapper">naal pothuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naal pothuma"/>
</div>
<div class="lyrico-lyrics-wrapper">pournami naal iravil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pournami naal iravil"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal vilikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal vilikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavukku thunaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavukku thunaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanil vasikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanil vasikanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyire en uyire ada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire en uyire ada "/>
</div>
<div class="lyrico-lyrics-wrapper">thaniya thavichen nane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniya thavichen nane"/>
</div>
<div class="lyrico-lyrics-wrapper">unake ada unake en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unake ada unake en"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaya solven thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaya solven thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alamari oorathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alamari oorathula"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam nanum olichu vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam nanum olichu vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu roopai nottu sekanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu roopai nottu sekanum"/>
</div>
<div class="lyrico-lyrics-wrapper">muthal muthala neeyum vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthal muthala neeyum vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">kungumatha pathu kitte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kungumatha pathu kitte"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalnaala naan valanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalnaala naan valanum"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu varai enakillai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu varai enakillai "/>
</div>
<div class="lyrico-lyrics-wrapper">oru thunaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thunaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">irivanum kuduthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irivanum kuduthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku inaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku inaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">vidugathu vidiayena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidugathu vidiayena"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kidaithathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kidaithathu "/>
</div>
<div class="lyrico-lyrics-wrapper">oru varama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru varama"/>
</div>
<div class="lyrico-lyrics-wrapper">thavikuthu thinaruthu puriyalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavikuthu thinaruthu puriyalaya"/>
</div>
<div class="lyrico-lyrics-wrapper">uthatula vaarthaiyum varavilaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthatula vaarthaiyum varavilaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavula nenachathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula nenachathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nesathula nadanthathidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesathula nadanthathidum"/>
</div>
<div class="lyrico-lyrics-wrapper">naal varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naal varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">onnukula onna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnukula onna "/>
</div>
<div class="lyrico-lyrics-wrapper">naama seranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naama seranum"/>
</div>
<div class="lyrico-lyrics-wrapper">atha maama aasaia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha maama aasaia"/>
</div>
<div class="lyrico-lyrics-wrapper">namma vaalthanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma vaalthanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyire en uyire ada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire en uyire ada "/>
</div>
<div class="lyrico-lyrics-wrapper">thaniya thavichen nane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniya thavichen nane"/>
</div>
<div class="lyrico-lyrics-wrapper">unake ada unake en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unake ada unake en"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaya solven thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaya solven thane"/>
</div>
</pre>
