---
title: "karambakudi kanaga song lyrics"
album: "Udanpirappe"
artist: "D. Imman"
lyricist: "Snehan"
director: "Era. Saravanan"
path: "/albums/udanpirappe-lyrics"
song: "Karambakudi Kanaga"
image: ../../images/albumart/udanpirappe.jpg
date: 2021-10-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6LRp_5_fxaY"
type: "happy"
singers:
  - Nithyasree Mahadevan
  - Jeyamoorthi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kotta katti valthavaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kotta katti valthavaga"/>
</div>
<div class="lyrico-lyrics-wrapper">kodi nanma seithavaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi nanma seithavaga"/>
</div>
<div class="lyrico-lyrics-wrapper">patta kekka varagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta kekka varagada"/>
</div>
<div class="lyrico-lyrics-wrapper">pathu menja poragada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu menja poragada"/>
</div>
<div class="lyrico-lyrics-wrapper">intha mannoda makkaloda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha mannoda makkaloda"/>
</div>
<div class="lyrico-lyrics-wrapper">kalai solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalai solluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">antha magarasan thondamana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha magarasan thondamana"/>
</div>
<div class="lyrico-lyrics-wrapper">vanangi kolluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanangi kolluvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karambakudi kanaga kanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karambakudi kanaga kanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">karambakudi kanaga kanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karambakudi kanaga kanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">aaduna boomi banthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaduna boomi banthum"/>
</div>
<div class="lyrico-lyrics-wrapper">aadumaiya thalakeela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadumaiya thalakeela"/>
</div>
<div class="lyrico-lyrics-wrapper">karambakudi kanaga kanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karambakudi kanaga kanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">karambakudi kanaga kanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karambakudi kanaga kanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">puthukotta jillavuke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthukotta jillavuke "/>
</div>
<div class="lyrico-lyrics-wrapper">ennala than kumba mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennala than kumba mela"/>
</div>
<div class="lyrico-lyrics-wrapper">angali yarum illa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angali yarum illa "/>
</div>
<div class="lyrico-lyrics-wrapper">pagaiyali yarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagaiyali yarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">ellarukume sontham naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellarukume sontham naan"/>
</div>
<div class="lyrico-lyrics-wrapper">oor kannu pattalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor kannu pattalume"/>
</div>
<div class="lyrico-lyrics-wrapper">per kettu ponathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="per kettu ponathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">eppavumo nan thangam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppavumo nan thangam than"/>
</div>
<div class="lyrico-lyrics-wrapper">ennoda kaal silambil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda kaal silambil"/>
</div>
<div class="lyrico-lyrics-wrapper">pesuvale kalaivaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesuvale kalaivaani"/>
</div>
<div class="lyrico-lyrics-wrapper">evanukum vaiya maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evanukum vaiya maten"/>
</div>
<div class="lyrico-lyrics-wrapper">paniya maten poda nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniya maten poda nee "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karambakudi kanaga kanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karambakudi kanaga kanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">karambakudi kanaga kanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karambakudi kanaga kanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">aaduna boomi banthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaduna boomi banthum"/>
</div>
<div class="lyrico-lyrics-wrapper">aadumaiya thalakeela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadumaiya thalakeela"/>
</div>
<div class="lyrico-lyrics-wrapper">karambakudi kanaga kanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karambakudi kanaga kanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">karambakudi kanaga kanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karambakudi kanaga kanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">puthukotta jillavuke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthukotta jillavuke "/>
</div>
<div class="lyrico-lyrics-wrapper">ennala than kumba mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennala than kumba mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vandara kulaal alagi sathangai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandara kulaal alagi sathangai "/>
</div>
<div class="lyrico-lyrics-wrapper">thandora pottu illukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandora pottu illukum"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu thaviya thaviya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu thaviya thaviya"/>
</div>
<div class="lyrico-lyrics-wrapper">thaviya thavikutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaviya thavikutha"/>
</div>
<div class="lyrico-lyrics-wrapper">vayasu ulaya ulaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayasu ulaya ulaya"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaya kothikutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaya kothikutha"/>
</div>
<div class="lyrico-lyrics-wrapper">thintada vaikum siruki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thintada vaikum siruki"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannu kandara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannu kandara"/>
</div>
<div class="lyrico-lyrics-wrapper">kalangu adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangu adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thoda than thoda than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thoda than thoda than"/>
</div>
<div class="lyrico-lyrics-wrapper">thoda than thudikutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoda than thudikutha"/>
</div>
<div class="lyrico-lyrics-wrapper">moochu sudutha sudutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochu sudutha sudutha"/>
</div>
<div class="lyrico-lyrics-wrapper">sudutha vedikutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudutha vedikutha"/>
</div>
<div class="lyrico-lyrics-wrapper">aada vaikum intha aata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aada vaikum intha aata"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaiku nan maharani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaiku nan maharani"/>
</div>
<div class="lyrico-lyrics-wrapper">sambalama un kai thatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sambalama un kai thatta"/>
</div>
<div class="lyrico-lyrics-wrapper">mattume thada nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mattume thada nee"/>
</div>
<div class="lyrico-lyrics-wrapper">medaiyile yeri aadaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="medaiyile yeri aadaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum menagaya minniduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum menagaya minniduven"/>
</div>
<div class="lyrico-lyrics-wrapper">kaal silamba kalati veetukula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaal silamba kalati veetukula"/>
</div>
<div class="lyrico-lyrics-wrapper">pona kan kalangi ninuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pona kan kalangi ninuduven"/>
</div>
<div class="lyrico-lyrics-wrapper">aalala kandaniyum thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalala kandaniyum thala"/>
</div>
<div class="lyrico-lyrics-wrapper">sutha vaipen nanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutha vaipen nanu"/>
</div>
<div class="lyrico-lyrics-wrapper">un aagasam boomikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aagasam boomikum"/>
</div>
<div class="lyrico-lyrics-wrapper">sachiyaga nipen naanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sachiyaga nipen naanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karambakudi kanaga kanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karambakudi kanaga kanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">karambakudi kanaga kanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karambakudi kanaga kanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">aaduna boomi banthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaduna boomi banthum"/>
</div>
<div class="lyrico-lyrics-wrapper">aadumaiya thalakeela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadumaiya thalakeela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennoda kaal silambil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda kaal silambil"/>
</div>
<div class="lyrico-lyrics-wrapper">pesuvale kalaivaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesuvale kalaivaani"/>
</div>
<div class="lyrico-lyrics-wrapper">evanukum vaiya maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evanukum vaiya maten"/>
</div>
<div class="lyrico-lyrics-wrapper">paniya maten poda nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniya maten poda nee"/>
</div>
</pre>
