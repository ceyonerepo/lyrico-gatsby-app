---
title: "naan ini kaatril song lyrics"
album: "Yaakkai"
artist: "Yuvan Shankar Raja"
lyricist: "Pa Vijay"
director: "Kuzhandai Velappan"
path: "/albums/yaakkai-lyrics"
song: "Naan Ini Kaatril"
image: ../../images/albumart/yaakkai.jpg
date: 2017-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HoeyUcm0U1A"
type: "love"
singers:
  -	Yuvan Shankar Raja
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan ini kaatril nadakka pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ini kaatril nadakka pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodavae un kaigal korthu kolgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodavae un kaigal korthu kolgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha prabanjam thaandiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha prabanjam thaandiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru payanam pogalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru payanam pogalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil moochu kooda thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil moochu kooda thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham ondril serndhu sellalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham ondril serndhu sellalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Midhandhu midhandhu vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhandhu midhandhu vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil nadandhu nadandhu sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil nadandhu nadandhu sendraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Asandhu asandhu nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asandhu asandhu nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo alandhu alandhu kondraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo alandhu alandhu kondraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un porvai irutilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un porvai irutilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan tholaindhu pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan tholaindhu pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru jadai seiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru jadai seiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Un patha suvatil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un patha suvatil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoosi pola padigiren madigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoosi pola padigiren madigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melliya saaralum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melliya saaralum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjalaai veiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjalaai veiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhadhu pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhadhu pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan vetkamum kobamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan vetkamum kobamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theththupal keeralum konjaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theththupal keeralum konjaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalum illadha kaamamum illadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalum illadha kaamamum illadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr nodi Orr nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr nodi Orr nodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paar sutri paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar sutri paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai pol ini yaarada kaadhalipaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai pol ini yaarada kaadhalipaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee endhan puththagam mellisai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee endhan puththagam mellisai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulnuni theypirai yaavilum neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulnuni theypirai yaavilum neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattilum nee kovilum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattilum nee kovilum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaai nadi aagidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai nadi aagidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhiyum naan thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhiyum naan thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bharathi pol aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharathi pol aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Paithiyam pol aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paithiyam pol aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnal naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Midhandhu midhandhu vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhandhu midhandhu vandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil nadandhu nadandhu sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil nadandhu nadandhu sendraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Asandhu asandhu nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asandhu asandhu nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo nelindhu valaindhu kondraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo nelindhu valaindhu kondraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un koonthal irutilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koonthal irutilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un porvai irutilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un porvai irutilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan tholaindhu pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan tholaindhu pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan tholaindhu pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan tholaindhu pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru jadai seiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru jadai seiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru jadai seiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru jadai seiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Un patha suvatil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un patha suvatil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoosi pola padigiren madigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoosi pola padigiren madigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anthi malaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthi malaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachai thalirgal nanaitha vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai thalirgal nanaitha vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan udalil sila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan udalil sila"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagudhi athilae veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagudhi athilae veesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enthan irudhi moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan irudhi moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudindhu kangal moodum tharunamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudindhu kangal moodum tharunamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu uruvam kaatumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu uruvam kaatumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda ninaivu naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda ninaivu naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril midhakkum isai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril midhakkum isai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan kaadhil nulaindhu kolven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan kaadhil nulaindhu kolven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattil kidakkum ilai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattil kidakkum ilai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">En koondhal kalaithu selvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En koondhal kalaithu selvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha boomi podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha boomi podhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha boomi podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha boomi podhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum veeru venduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum veeru venduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum veeru venduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum veeru venduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paartha paarvaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paartha paarvaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paartha paarvaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paartha paarvaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu kaalaveliyil kaatru pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu kaalaveliyil kaatru pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakumae midhakumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakumae midhakumae"/>
</div>
</pre>
