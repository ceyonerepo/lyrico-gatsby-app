---
title: "un kannukulle gunah vachu song lyrics"
album: "Singam II"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari Gopalakrishnan"
path: "/albums/singam-2-lyrics"
song: "Un Kannukulle Gun ah Vachu"
image: ../../images/albumart/singam-2.jpg
date: 2013-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2BXk5qto6RY"
type: "Love"
singers:
  - Javed Ali
  - Priya Himesh
  - Kunal Ganjawala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Solamugai Solamugai Solamugaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solamugai Solamugai Solamugaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solamugai Solamugai Solamugaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solamugai Solamugai Solamugaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solamugai Solamugai Solamugaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solamugai Solamugai Solamugaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solamugai Solamugai Solamugaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solamugai Solamugai Solamugaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boys And Girls Wanna Get Up And Dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boys And Girls Wanna Get Up And Dance"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come On Raise You Glass And Say Whats Up Whats
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On Raise You Glass And Say Whats Up Whats"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Up Whats Up Hey Scream And Shout Maama Let It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Up Whats Up Hey Scream And Shout Maama Let It"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Out Loud Maama Bring The Roof Down Singing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Out Loud Maama Bring The Roof Down Singing"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whats Up Whats Up Whats Up hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whats Up Whats Up Whats Up hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannukkulle Gun Ah Vachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannukkulle Gun Ah Vachu "/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Sudaadha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Sudaadha "/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaakki Satta Collar Ah Thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaakki Satta Collar Ah Thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thooki Vidaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooki Vidaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pattampoochi Kitta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pattampoochi Kitta "/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Vattamitta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Vattamitta "/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu Rasi Kattam Katta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu Rasi Kattam Katta "/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa Padaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Padaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Dettol Oothi Sutham Senja Vennilavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Dettol Oothi Sutham Senja Vennilavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnukittu Enna Inga Yedho Seiyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnukittu Enna Inga Yedho Seiyaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Aayiram Aasaiya Vachirunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Aayiram Aasaiya Vachirunthum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Vaatuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vaatuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannukulle Kannukkulle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannukulle Kannukkulle "/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannukkulle Gun Ah Vachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannukkulle Gun Ah Vachu "/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Sudaadha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Sudaadha "/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaaki Satta Collar Ah Thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaaki Satta Collar Ah Thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thooki Vidaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooki Vidaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Yeah Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boys And Girls Wanna Get Up And Dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boys And Girls Wanna Get Up And Dance"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come On Raise You Glass And Say Whats Up Whats
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come On Raise You Glass And Say Whats Up Whats"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Up Whats Up Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Up Whats Up Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vekkathula Sirichaale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vekkathula Sirichaale "/>
</div>
<div class="lyrico-lyrics-wrapper">Western Music Kku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Western Music Kku "/>
</div>
<div class="lyrico-lyrics-wrapper">Verum Kaalil Nadanthaale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Kaalil Nadanthaale "/>
</div>
<div class="lyrico-lyrics-wrapper">Luck U Boomikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luck U Boomikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Dhoorathula Irunthaale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhoorathula Irunthaale "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaichal Maenikku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaichal Maenikku "/>
</div>
<div class="lyrico-lyrics-wrapper">Aruge Nee Vandhalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruge Nee Vandhalae "/>
</div>
<div class="lyrico-lyrics-wrapper">Energy Tonic U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Energy Tonic U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Corrupt Aana Computer Ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Corrupt Aana Computer Ah "/>
</div>
<div class="lyrico-lyrics-wrapper">Maari Ponene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari Ponene "/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kai Patta Immediate Ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kai Patta Immediate Ah "/>
</div>
<div class="lyrico-lyrics-wrapper">Restart Aavenayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Restart Aavenayee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Straberry Baby Ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Straberry Baby Ah "/>
</div>
<div class="lyrico-lyrics-wrapper">Robbery Panna Paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robbery Panna Paakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannukulle Kannukkulle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannukulle Kannukkulle "/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannukkulle Gun Ah Vachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannukkulle Gun Ah Vachu "/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Sudaadha Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Sudaadha Un"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaki Satta Collar Ah Thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki Satta Collar Ah Thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thooki Vidaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooki Vidaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Ekka Chakka Azhagoda Thiriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ekka Chakka Azhagoda Thiriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salvaare Unnakaaga Varalam Di 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salvaare Unnakaaga Varalam Di "/>
</div>
<div class="lyrico-lyrics-wrapper">Moondram World War Eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moondram World War Eh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Dada Unnudaiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Dada Unnudaiya "/>
</div>
<div class="lyrico-lyrics-wrapper">Nadipukku Tharalaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadipukku Tharalaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Oscare Nee Vaikum Ice Il Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oscare Nee Vaikum Ice Il Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marandhen En Pere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhen En Pere"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neee Mooku Mela Kova Patta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neee Mooku Mela Kova Patta "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaam Sivakkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaam Sivakkum "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Vanavillil Oru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Vanavillil Oru "/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paakura Paarvayil Pathu Kilo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakura Paarvayil Pathu Kilo "/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kootura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kootura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kannukulle Kannukkulle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannukulle Kannukkulle "/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannukkulle Gun Ah Vachu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannukkulle Gun Ah Vachu "/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Sudaadha Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Sudaadha Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaki Satta Collar Ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki Satta Collar Ah "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan Thooki Vidaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Thooki Vidaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Yeah Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Onnam Classu Ponna Pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Onnam Classu Ponna Pola "/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Pannaadha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Pannaadha "/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unna Thaana Thedi Vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unna Thaana Thedi Vanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli Oodaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Oodaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solamugai Solamugai Solamugaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solamugai Solamugai Solamugaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solamugai Solamugai Solamugaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solamugai Solamugai Solamugaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solamugai Solamugai Solamugaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solamugai Solamugai Solamugaiyo"/>
</div>
</pre>
