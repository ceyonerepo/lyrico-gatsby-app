---
title: 'megathoodham lyrics'
album: 'Airaa'
artist: 'Sundaramurthy K.S.'
lyricist: 'Thamarai'
director: 'Sarjun KM'
path: '/albums/airaa-song-lyrics'
song: 'Megathoodham'
image: ../../images/albumart/airaa.jpg
date: 2019-03-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CWjR0ddXUDo"
type: 'happy'
singers: 
- Padmapriya Raghavan 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Megathoodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathoodham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megathoodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathoodham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paada Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanal Aagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanal Aagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarigai Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarigai Kanavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaagam Theerkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaagam Theerkumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodaiyin Nilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodaiyin Nilavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaivile Velichcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivile Velichcham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimaiyil Urugum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyil Urugum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anicham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anicham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavuthaan Ithuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavuthaan Ithuvum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalainthidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ena Nenjil Nenjil Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Nenjil Nenjil Dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuthey Achcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuthey Achcham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megathoodham Paada Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathoodham Paada Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Menimeethu Saaral Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menimeethu Saaral Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Thaasan Kaana Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Thaasan Kaana Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanavil Varum Vaazhvil Meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavil Varum Vaazhvil Meendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megathoodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathoodham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megathoodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathoodham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paada Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam Eluthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Eluthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril Veesiya Naadagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Veesiya Naadagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha Kaatre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Kaatre"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum Inaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Inaithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arangam Yetrum Kaaviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arangam Yetrum Kaaviyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deva Mullai Pookkum Kollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deva Mullai Pookkum Kollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondathey En Veettin Ellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondathey En Veettin Ellai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Nee Maravaathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nee Maravaathiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal Kaatrilum Piriyaathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Kaatrilum Piriyaathiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megathootham Paada Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathootham Paada Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Menimeethu Saaral Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menimeethu Saaral Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali Thaasan Kaana Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali Thaasan Kaana Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanavil Varum Vaazhvil Meendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavil Varum Vaazhvil Meendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thumbai Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumbai Pole"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuya Azhagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuya Azhagai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnidam Thaan Kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam Thaan Kaangiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kai Neetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kai Neetti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenthi Anaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthi Anaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Enni Yenginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Enni Yenginen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">intha Vaarthai Ketkum Pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha Vaarthai Ketkum Pothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eeram Oorum Kannin Meethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeram Oorum Kannin Meethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavaiyin intha Eeram Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaiyin intha Eeram Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karu Megamaai Uru Maaruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karu Megamaai Uru Maaruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanal Aagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanal Aagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarigai Kanavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarigai Kanavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaagam Theerkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaagam Theerkumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodaiyin Nilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodaiyin Nilavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaivile Velichcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivile Velichcham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimaiyil Urugum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyil Urugum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anicham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anicham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavuthaan Ithuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavuthaan Ithuvum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalainthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalainthidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ena Nenjil Nenjil Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Nenjil Nenjil Dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuthey Achcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuthey Achcham"/>
</div>
</pre>