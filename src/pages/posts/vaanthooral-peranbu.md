---
title: "vaanthooral song lyrics"
album: "Peranbu"
artist: "Yuvan Shankar Raja"
lyricist: "Vairamuthu"
director: "Ram"
path: "/albums/peranbu-lyrics"
song: "Vaanthooral"
image: ../../images/albumart/peranbu.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DA2mBYEtsSA"
type: "sad"
singers:
  - Sriram Parthasarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaanthooral en tholgal meley vaaladum naaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanthooral en tholgal meley vaaladum naaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Per irutil en kankal meethey minnum minminiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per irutil en kankal meethey minnum minminiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhaiye purati potalum vinnai paaruthuthan mulaigum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhaiye purati potalum vinnai paaruthuthan mulaigum"/>
</div>
<div class="lyrico-lyrics-wrapper">Peranbinal vaalvin konalgal nerpadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peranbinal vaalvin konalgal nerpadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paruvam kadanthu ponalum arugam pul saagathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvam kadanthu ponalum arugam pul saagathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ore thooralil mothamai pachaiyay maaridium
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore thooralil mothamai pachaiyay maaridium"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanthooral en tholgal meley vaaladum naaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanthooral en tholgal meley vaaladum naaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Per irutil en kankal meethey minnum minminiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per irutil en kankal meethey minnum minminiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engey sentru veelvathu entru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engey sentru veelvathu entru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinthum mazhai arinthathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinthum mazhai arinthathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha pantham yaarvasam entru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha pantham yaarvasam entru"/>
</div>
<div class="lyrico-lyrics-wrapper">Theadum uyire therinthathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theadum uyire therinthathu illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megam atra vaanthin kiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam atra vaanthin kiley"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagam utra paravaiye pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagam utra paravaiye pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethum atru parantha pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethum atru parantha pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadum thunaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadum thunaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irapatham kaatril irunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irapatham kaatril irunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorathil kaadu therinthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorathil kaadu therinthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakam thaney neernilai entru pesum kuralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakam thaney neernilai entru pesum kuralgal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanthooral en tholgal meley vaaladum naaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanthooral en tholgal meley vaaladum naaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mella thana sollum maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella thana sollum maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollithaney sogam theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollithaney sogam theerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalum aasai ullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalum aasai ullum"/>
</div>
<div class="lyrico-lyrics-wrapper">perugey vaalgai entrum inigum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perugey vaalgai entrum inigum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unam patta jeevan ethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unam patta jeevan ethum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattiniyal saavathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattiniyal saavathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engo sellum erumpu kooda irai kodugum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo sellum erumpu kooda irai kodugum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megam madum vaanam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam madum vaanam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theagam madum vaalgai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theagam madum vaalgai illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanthooral en tholgal meley vaaladum naaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanthooral en tholgal meley vaaladum naaley"/>
</div>
<div class="lyrico-lyrics-wrapper">Per irutil en kankal meethey minnum minminiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Per irutil en kankal meethey minnum minminiye"/>
</div>
</pre>
