---
title: "veezhaadhae song lyrics"
album: "Mudhal Nee Mudivum Nee"
artist: "Darbuka Siva"
lyricist: "Keerthi"
director: "Darbuka Siva"
path: "/albums/mudhal-nee-mudivum-nee-lyrics"
song: "Veezhaadhae"
image: ../../images/albumart/mudhal-nee-mudivum-nee.jpg
date: 2022-01-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/v62sFekg9qc"
type: "happy"
singers:
  - Abhay Jodhpurkar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Veezhadhae manamae en manamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhadhae manamae en manamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai neengadhae kanamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai neengadhae kanamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarathil enai nee yetrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarathil enai nee yetrinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanilae naan pogayil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanilae naan pogayil "/>
</div>
<div class="lyrico-lyrics-wrapper">megamaai vaazhgaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megamaai vaazhgaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaindhae vizhi thuliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaindhae vizhi thuliyaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veezhadhae manamae en manamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhadhae manamae en manamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhadhae manamae en manamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhadhae manamae en manamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhai seiyaadhae en manamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhai seiyaadhae en manamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalai keezhzai eriyum kelvigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai keezhzai eriyum kelvigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram naan paarkkaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram naan paarkkaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">naan vidai ketkkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan vidai ketkkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaindhae thugal thugalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaindhae thugal thugalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhadhae manamae en manamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhadhae manamae en manamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan seidha thavara un thavara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan seidha thavara un thavara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai indri idhayam nindradho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai indri idhayam nindradho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana chuvar ellaam kodu varaindhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana chuvar ellaam kodu varaindhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirithaayae rasithaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithaayae rasithaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oviyam alla keeral endraayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyam alla keeral endraayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Virinthidum keeralil un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virinthidum keeralil un "/>
</div>
<div class="lyrico-lyrics-wrapper">erigira thoorigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erigira thoorigai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan muzhudhum pugaiyinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan muzhudhum pugaiyinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal yaavumae kariyena maarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal yaavumae kariyena maarudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nillaai en kaalamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillaai en kaalamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avasaramaaga nee enai vittu pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaramaaga nee enai vittu pogiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupadi thodangi munnurai sollavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupadi thodangi munnurai sollavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivurai ezhudhavo muyalugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivurai ezhudhavo muyalugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaaa hoo oooo oooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaaa hoo oooo oooo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marubadi thodangi munn urai sollavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marubadi thodangi munn urai sollavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivurai ezhudhavae muyalugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivurai ezhudhavae muyalugiren"/>
</div>
</pre>
