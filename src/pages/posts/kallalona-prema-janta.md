---
title: "kallalona song lyrics"
album: "Prema Janta"
artist: "Nikhilesh Thogari"
lyricist: "Nikhilesh Thogari"
director: "Nikhilesh Thogari"
path: "/albums/prema-janta-lyrics"
song: "Kallalona"
image: ../../images/albumart/prema-janta.jpg
date: 2019-06-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Sx57OWIhQTI"
type: "love"
singers:
  - Lahari Ambati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kallallona enno kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallallona enno kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu chustunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu chustunte"/>
</div>
<div class="lyrico-lyrics-wrapper">oohallona evo kathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oohallona evo kathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu vasthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu vasthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">varasaayye varevaranee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varasaayye varevaranee"/>
</div>
<div class="lyrico-lyrics-wrapper">adiganu naa manasuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiganu naa manasuni"/>
</div>
<div class="lyrico-lyrics-wrapper">eduraite nuvvenani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduraite nuvvenani"/>
</div>
<div class="lyrico-lyrics-wrapper">kangaru pedutunnadee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangaru pedutunnadee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kallallona enno kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallallona enno kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu chustunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu chustunte"/>
</div>
<div class="lyrico-lyrics-wrapper">oohallona evo kathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oohallona evo kathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu vasthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu vasthunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chetiki gajulu vestunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chetiki gajulu vestunna"/>
</div>
<div class="lyrico-lyrics-wrapper">kaliki muvvalu kodutunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaliki muvvalu kodutunna"/>
</div>
<div class="lyrico-lyrics-wrapper">kantiki katuka pedutunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kantiki katuka pedutunna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kosam chinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kosam chinna"/>
</div>
<div class="lyrico-lyrics-wrapper">meniki vompulu pudutunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meniki vompulu pudutunna"/>
</div>
<div class="lyrico-lyrics-wrapper">navvuki siggulu postunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="navvuki siggulu postunna"/>
</div>
<div class="lyrico-lyrics-wrapper">nadakaki natyam nerpaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadakaki natyam nerpaina"/>
</div>
<div class="lyrico-lyrics-wrapper">andhanga rana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhanga rana"/>
</div>
<div class="lyrico-lyrics-wrapper">adham lo nanu nenu chustunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adham lo nanu nenu chustunna"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhochchi na disti teestuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhochchi na disti teestuna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kosame intha mustabavutuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kosame intha mustabavutuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">galilo rathalu rastunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="galilo rathalu rastunna"/>
</div>
<div class="lyrico-lyrics-wrapper">nelanu kalitho geesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nelanu kalitho geesthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">reyiki rangulu vestunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="reyiki rangulu vestunna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee valle kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee valle kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">velaku nidhura potunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velaku nidhura potunna"/>
</div>
<div class="lyrico-lyrics-wrapper">nidhura lo nee kala kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nidhura lo nee kala kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">kalanu evaraina kadiliste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalanu evaraina kadiliste"/>
</div>
<div class="lyrico-lyrics-wrapper">godave padutunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="godave padutunna"/>
</div>
<div class="lyrico-lyrics-wrapper">vadhantu pasi vayasu antunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadhantu pasi vayasu antunna"/>
</div>
<div class="lyrico-lyrics-wrapper">sidhanga nee vaipu vastunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sidhanga nee vaipu vastunna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kosame intha alochistunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kosame intha alochistunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kallallona enno kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallallona enno kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu chustunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu chustunte"/>
</div>
<div class="lyrico-lyrics-wrapper">oohallona evo kathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oohallona evo kathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu vasthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu vasthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">varasaayye varevaranee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varasaayye varevaranee"/>
</div>
<div class="lyrico-lyrics-wrapper">adiganu naa manasuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiganu naa manasuni"/>
</div>
<div class="lyrico-lyrics-wrapper">eduraite nuvvenani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eduraite nuvvenani"/>
</div>
<div class="lyrico-lyrics-wrapper">kangaru pedutunnadee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangaru pedutunnadee"/>
</div>
</pre>
