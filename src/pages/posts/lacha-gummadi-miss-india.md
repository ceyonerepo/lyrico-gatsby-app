---
title: "lacha gummadi song lyrics"
album: "Miss India"
artist: "S. Thaman"
lyricist: "Kalyan Chakravarthi"
director: "Narendra Nath"
path: "/albums/miss-india-lyrics"
song: "Lacha Gummadi"
image: ../../images/albumart/miss-india.jpg
date: 2020-11-04
lang: telugu
youtubeLink: "https://www.youtube.com/embed/kecLQrg_ZTs"
type: "happy"
singers:
  - Srivardhini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pachi pachi matti jaaley puttukoche ee vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachi pachi matti jaaley puttukoche ee vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaddi pocha gajje katti dhunukuladey ee vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaddi pocha gajje katti dhunukuladey ee vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Gattu dhaati palley theti paate katti ponkamlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gattu dhaati palley theti paate katti ponkamlaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaleni polikalona paduchu navve thummedhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaleni polikalona paduchu navve thummedhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa lacha gummadi gummadi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa lacha gummadi gummadi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">O gogula gongadi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O gogula gongadi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee kinnera koppuna sanna jaaji navvera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee kinnera koppuna sanna jaaji navvera"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa lacha gummadi gummadi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa lacha gummadi gummadi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedu mallela andhamu ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedu mallela andhamu ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee ompula kuppaki mannu minnu kannera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee ompula kuppaki mannu minnu kannera"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaga anaga raga madhe avaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaga anaga raga madhe avaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinaga thinaga chedhaina theepiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinaga thinaga chedhaina theepiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaga kanaga karnale kalaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaga kanaga karnale kalaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinaga vinaga vivarmidhe gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaga vinaga vivarmidhe gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi sithakoka chilakamma o gongali puruganta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi sithakoka chilakamma o gongali puruganta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu nuvve marchukomannadhanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu nuvve marchukomannadhanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Gana shilpalu evaina oka nadu shilalanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gana shilpalu evaina oka nadu shilalanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yochanalanni aarambalanta...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yochanalanni aarambalanta..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maa lacha gummadi gummadi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa lacha gummadi gummadi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">O gogula gongadi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O gogula gongadi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee kinnera koppuna sanna jaaji navvera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee kinnera koppuna sanna jaaji navvera"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa lacha gummadi gummadi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa lacha gummadi gummadi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedu mallela andhamu ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedu mallela andhamu ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee ompula kuppaki mannu minnu kannera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee ompula kuppaki mannu minnu kannera"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaga anaga raga madhe avaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaga anaga raga madhe avaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinaga thinaga chedhaina theepiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinaga thinaga chedhaina theepiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaga kanaga karnale kalaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaga kanaga karnale kalaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinaga vinaga vivarmidhe gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaga vinaga vivarmidhe gaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvu chuse lokamlo prathi chota nuvvele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu chuse lokamlo prathi chota nuvvele"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhurayye kannile kantundhi nee kalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhurayye kannile kantundhi nee kalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali vaane chusi gaalincheddhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali vaane chusi gaalincheddhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nela vaale navvulu chese hungama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nela vaale navvulu chese hungama"/>
</div>
<div class="lyrico-lyrics-wrapper">Roju poosey thurupulone kandhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roju poosey thurupulone kandhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi puta puttey vidhey needhamma…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi puta puttey vidhey needhamma…"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa lacha gummadi gummadi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa lacha gummadi gummadi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">O gogula gongadi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O gogula gongadi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee kinnera koppuna sanna jaaji navvera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee kinnera koppuna sanna jaaji navvera"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa lacha gummadi gummadi raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa lacha gummadi gummadi raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedu mallela andhamu ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedu mallela andhamu ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee ompula kuppaki mannu minnu kannera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee ompula kuppaki mannu minnu kannera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachi pachi matti jaaley puttukoche ee vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachi pachi matti jaaley puttukoche ee vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaddi pocha gajje katti dhunukuladey ee vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaddi pocha gajje katti dhunukuladey ee vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Gattu dhaati palley theti paate katti ponkamlaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gattu dhaati palley theti paate katti ponkamlaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaleni polikalona paduchu navve thummedhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaleni polikalona paduchu navve thummedhala"/>
</div>
</pre>
