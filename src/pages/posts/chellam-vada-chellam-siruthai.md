---
title: "chellam vada chellam song lyrics"
album: "Siruthai"
artist: "Vidyasagar"
lyricist: "Na. Muthukumar"
director: "Siva"
path: "/albums/siruthai-lyrics"
song: "Chellam Vada Chellam"
image: ../../images/albumart/siruthai.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vH49WC3EH2A"
type: "love"
singers:
  - Udit Narayan
  - Roshan
  - Surmukhi Raman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chellam Indha Chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellam Indha Chellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Venum Kaelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Venum Kaelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Tharren Kaelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Tharren Kaelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellam Vaada Chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellam Vaada Chellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaera Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaera Enna Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan Enakku Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Enakku Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Neeyum Kettaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Neeyum Kettaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththangalai Naan Ketpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththangalai Naan Ketpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththangal Thandhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththangal Thandhaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moththam Nee Ketpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththam Nee Ketpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Love My Love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Sweety Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Sweety Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You The Only One Only One
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You The Only One Only One"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">In My World
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In My World"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Would Be My Diamond
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Would Be My Diamond"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Whity Pearl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Whity Pearl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Love My Love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Are My Lady Bird
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Lady Bird"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellam Indha Chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellam Indha Chellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Venum Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Venum Kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Tharen Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Tharen Kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellam Vaadaa Chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellam Vaadaa Chellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Enna Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needhaan Enakku Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhaan Enakku Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaivil Irundhu Un Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivil Irundhu Un Paarvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasa Kolludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Kolludhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhaiya Thudippu Ippodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaiya Thudippu Ippodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Egiri Nikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egiri Nikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arugil Irukku Un Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil Irukku Un Paarvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayasu Kolludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu Kolludhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anaikka Cholli En Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaikka Cholli En Nenjil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuruvi Solludhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruvi Solludhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kitta Kitta Vaa Vaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Kitta Vaa Vaanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Vandhu Kai Neetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Vandhu Kai Neetta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli Thalli Poponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Thalli Poponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetkam Vaal Neetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkam Vaal Neetta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Love My Love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Sweety Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Sweety Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You The Only One Only One
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You The Only One Only One"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">In My World
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In My World"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Would Be My Diamond
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Would Be My Diamond"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Whity Pearl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Whity Pearl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Love My Love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Are My Lady Bird
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Lady Bird"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirichi Sirichi Nee Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichi Sirichi Nee Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sidhara Vaikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidhara Vaikkira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asanji Asanji Alagaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asanji Asanji Alagaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhara Vakkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhara Vakkira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izhuththu Pidichi Nee Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuththu Pidichi Nee Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sinunga Vaikkiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinunga Vaikkiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kazhuththu Narambu Pooppookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuththu Narambu Pooppookka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keranga Vaikkira Keranga Vaikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keranga Vaikkira Keranga Vaikkira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Sonnaal Endhan Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sonnaal Endhan Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyakkatti Nirkkaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyakkatti Nirkkaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neekkonjin Pesumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekkonjin Pesumbodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukkulla Verkkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkulla Verkkaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Love My Love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Sweety Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Sweety Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You The Only One Only One
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You The Only One Only One"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">In My World
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In My World"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Would Be My Diamond
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Would Be My Diamond"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Whity Pearl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Whity Pearl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Love My Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Love My Love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Are My Lady Bird
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Lady Bird"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellam Intha Chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellam Intha Chellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Vaenum Kaelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vaenum Kaelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam Tharaen Kaelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Tharaen Kaelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellam Vaadaa Chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellam Vaadaa Chellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vera Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera Enna Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neethaan Enakku Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Enakku Venum"/>
</div>
</pre>
