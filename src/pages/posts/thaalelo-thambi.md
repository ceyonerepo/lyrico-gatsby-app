---
title: "thaalelo song lyrics"
album: "Thambi"
artist: "Govind Vasantha"
lyricist: "Karthik Netha"
director: "Jeethu Joseph"
path: "/albums/thambi-lyrics"
song: "Thaalelo"
image: ../../images/albumart/thambi.jpg
date: 2019-12-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YvcUvSacgRo"
type: "love"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaalaeloo paaduthae kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalaeloo paaduthae kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo polae thoonguthae naanamae oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo polae thoonguthae naanamae oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhedho aaguthae maarbilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho aaguthae maarbilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan melae pogudhae aadalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan melae pogudhae aadalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee enai yedho seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enai yedho seigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigal kannin neer neekki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigal kannin neer neekki"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo vaithu paarkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo vaithu paarkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un bimbathin seendalgalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un bimbathin seendalgalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaangindra yaavum vaasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaangindra yaavum vaasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idho idho kaana meiyaagudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho idho kaana meiyaagudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poga koodatha thoorangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga koodatha thoorangal "/>
</div>
<div class="lyrico-lyrics-wrapper">poonalumae Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poonalumae Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Maara koodamal premaigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maara koodamal premaigal "/>
</div>
<div class="lyrico-lyrics-wrapper">poo pookkumae vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo pookkumae vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketka koodatha vaarthaigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketka koodatha vaarthaigal "/>
</div>
<div class="lyrico-lyrics-wrapper">kettalumae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kettalumae ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhil nee pesum mounangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil nee pesum mounangal "/>
</div>
<div class="lyrico-lyrics-wrapper">kaapattrumae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaapattrumae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooramal nee poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooramal nee poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazham naan yenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazham naan yenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Saambal aagatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saambal aagatha "/>
</div>
<div class="lyrico-lyrics-wrapper">kanneeril vaazhnthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanneeril vaazhnthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezh ezhelu kaalangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezh ezhelu kaalangal "/>
</div>
<div class="lyrico-lyrics-wrapper">theernthu poonalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theernthu poonalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanmai chuttodu alaivenae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanmai chuttodu alaivenae "/>
</div>
<div class="lyrico-lyrics-wrapper">unai thedi ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai thedi ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeee mazhai kaattilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeee mazhai kaattilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarum asiriree nee eee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarum asiriree nee eee"/>
</div>
</pre>
