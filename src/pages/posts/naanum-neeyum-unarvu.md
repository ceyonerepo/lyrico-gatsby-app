---
title: "naanum neeyum song lyrics"
album: "Unarvu"
artist: "Nakul Abhyankar"
lyricist: "Vadivarasu"
director: "Subu Venkat"
path: "/albums/unarvu-lyrics"
song: "Naanum Neeyum"
image: ../../images/albumart/unarvu.jpg
date: 2019-07-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7aHPCi6HYqQ"
type: "melody"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naanum Neeyum Yaaro Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Neeyum Yaaro Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Sirippil Endhan Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Sirippil Endhan Kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unarvu En Unarvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvu En Unarvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvu En Unarvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvu En Unarvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvu En Unarvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvu En Unarvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvu En Unarvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvu En Unarvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum Neeyum Yaaro Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Neeyum Yaaro Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Sirippil Endhan Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Sirippil Endhan Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam Kooda Saalai Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Kooda Saalai Aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbi Seidhaal Inikkum Edhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbi Seidhaal Inikkum Edhuvum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum Mannil Baaram Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Mannil Baaram Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum Inge Muyandraal Mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Inge Muyandraal Mudiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unarvu En Unarvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvu En Unarvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvu En Unarvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvu En Unarvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhage Azhage Yedhilum Azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage Azhage Yedhilum Azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Naamum Yendralayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Naamum Yendralayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhage Azhage Yedhilum Azhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage Azhage Yedhilum Azhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Naamum Yendralayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Naamum Yendralayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaikkindra Ellaam Nadappathu Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaikkindra Ellaam Nadappathu Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadappathil Kooda Ninaippathu Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadappathil Kooda Ninaippathu Illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaippathu Yedhuvum Nadakkadhadhu Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaippathu Yedhuvum Nadakkadhadhu Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum Kaanum Kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum Kaanum Kanavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manamae Kavalai Illaa Manidhar Undaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamae Kavalai Illaa Manidhar Undaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyyilum Mazhaiyum Paniyum Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyyilum Mazhaiyum Paniyum Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella Mella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhaippai Kandu Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippai Kandu Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhthumae Vanangumae En Naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhthumae Vanangumae En Naalum"/>
</div>
</pre>
