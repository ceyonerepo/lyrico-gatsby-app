---
title: "anthem of harmony song lyrics"
album: "FIR"
artist: "Ashwath"
lyricist: "Mashook Rahman"
director: "Manu Anand"
path: "/albums/fir-lyrics"
song: "Anthem Of Harmony"
image: ../../images/albumart/fir.jpg
date: 2022-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FjbhHYNKaII"
type: "anthem"
singers:
  - Naresh Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nenjodu nenjodu vanjamum nanjada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu nenjodu vanjamum nanjada"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiyaga meiyaga unmaiyum nanjada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyaga meiyaga unmaiyum nanjada"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai serndha amilam nanjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai serndha amilam nanjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagai nerndha manidham nanjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagai nerndha manidham nanjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adar nanje padarum nanje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adar nanje padarum nanje"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangidayo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangidayo nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Idar thaangum maanudame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idar thaangum maanudame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthidayo nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthidayo nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal aalum bhoomiyin mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal aalum bhoomiyin mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai aaadum agathigal naame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai aaadum agathigal naame"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhilum yen pirivinaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhilum yen pirivinaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Por ellam punidham endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Por ellam punidham endral"/>
</div>
<div class="lyrico-lyrics-wrapper">Amaidhikkor aalayam yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amaidhikkor aalayam yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Inam kaakka nara baliyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inam kaakka nara baliyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuva nee idhuva naam engu pogirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuva nee idhuva naam engu pogirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai illa iruloda maaya pogirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai illa iruloda maaya pogirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhama nee madhama naan kaanal maanida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhama nee madhama naan kaanal maanida"/>
</div>
<div class="lyrico-lyrics-wrapper">Inavaadham enum saabam innum yenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inavaadham enum saabam innum yenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjodu nenjodu vanjamum nanjada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu nenjodu vanjamum nanjada"/>
</div>
<div class="lyrico-lyrics-wrapper">Inavaadham madhavaadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inavaadham madhavaadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Sazhithu nee meelada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sazhithu nee meelada"/>
</div>
</pre>
