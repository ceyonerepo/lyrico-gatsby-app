---
title: "nyaayam thaana song lyrics"
album: "180"
artist: "Sharreth"
lyricist: "Viveka"
director: "Jayendra Panchapakesan"
path: "/albums/180-lyrics"
song: "Nyaayam Thaana"
image: ../../images/albumart/180.jpg
date: 2011-06-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WNDTK6sx3j8"
type: "sad"
singers:
  - Sharreth
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nyalamey Un Poar Nyayam Thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyalamey Un Poar Nyayam Thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalamae Un Peyar Kaayam Thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamae Un Peyar Kaayam Thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro Yaar Yoaaro Venpugai Aavaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaar Yoaaro Venpugai Aavaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poi Pola Yaavum Purandoduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi Pola Yaavum Purandoduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyalame Un Poar Nyayam Thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyalame Un Poar Nyayam Thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalame Un Peyar Kaayam Thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalame Un Peyar Kaayam Thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aethetho Nenjukkul Aasai Aasaithaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aethetho Nenjukkul Aasai Aasaithaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaamae Manmaele Maayai Maayaithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamae Manmaele Maayai Maayaithaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvaiyae Vetri Kollave Yaarundu Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvaiyae Vetri Kollave Yaarundu Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naerukku Neraai Nijam Mothuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naerukku Neraai Nijam Mothuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalame Thee Thaan Thoovalaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalame Thee Thaan Thoovalaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaavumey Maayai Aagalaamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavumey Maayai Aagalaamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saerthathu Ellaame Veen Veenthaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saerthathu Ellaame Veen Veenthaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthathu Ellaamae Pogum Pogumthaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthathu Ellaamae Pogum Pogumthaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pookkindra Poovellam Vaadiyae Theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkindra Poovellam Vaadiyae Theerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ul Naakku Kooda Kaaigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ul Naakku Kooda Kaaigirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyalamey Un Poar Nyayam Thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyalamey Un Poar Nyayam Thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalamae Un Peyar Kaayam Thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamae Un Peyar Kaayam Thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro Yaar Yoaaro Venpugai Aavaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Yaar Yoaaro Venpugai Aavaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poi Pola Yaavum Purandoduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi Pola Yaavum Purandoduthae"/>
</div>
</pre>
