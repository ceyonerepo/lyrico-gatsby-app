---
title: "yedho maayam enna song lyrics"
album: "Dagaalty"
artist: "Vijay Narain"
lyricist: "Subu"
director: "Vijay Anand"
path: "/albums/dagaalty-song-lyrics"
song: "Yedho Maayam Enna"
image: ../../images/albumart/dagaalty.jpg
date: 2020-01-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3wTjX2YmjCM"
type: "Love"
singers:
  - Dhee
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yedho Maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thoduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thoduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkam Lesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkam Lesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi Viduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi Viduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valaiyum Nedunchaalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyum Nedunchaalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhir Pooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhir Pooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaiye Illa Kudukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaiye Illa Kudukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyum Mani Kaalamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyum Mani Kaalamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Araiyum Edhir Kaathula Seragu Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Araiyum Edhir Kaathula Seragu Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Achudhu En Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achudhu En Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thoduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thoduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla Sirippula Kerangurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla Sirippula Kerangurane"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Kovatha Rasikkirane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Kovatha Rasikkirane"/>
</div>
<div class="lyrico-lyrics-wrapper">Padangaattum Ennoda Kanavula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padangaattum Ennoda Kanavula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadu Maattram Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadu Maattram Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandha Nookkatha Marakkurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Nookkatha Marakkurane"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakoottula Sarukkurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakoottula Sarukkurane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkethum Ullaara Pudhusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkethum Ullaara Pudhusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraattaam Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraattaam Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniye Varusham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniye Varusham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Kadanthene Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Kadanthene Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaikkunnu Oru Thevai Varlaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaikkunnu Oru Thevai Varlaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanava Nenache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanava Nenache"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkaa Alanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkaa Alanjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaname Nee Kaattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaname Nee Kaattura"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakka Thayangurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakka Thayangurane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kadhai Enna Theriyalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kadhai Enna Theriyalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pechum Puriyalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pechum Puriyalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vazhiyedhu Kekalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vazhiyedhu Kekalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhum Pudikkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum Pudikkudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalla Sirippula Kerangurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla Sirippula Kerangurane"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Kovatha Rasikkirane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Kovatha Rasikkirane"/>
</div>
<div class="lyrico-lyrics-wrapper">Padangaattum Ennoda Kanavula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padangaattum Ennoda Kanavula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadu Maattram Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadu Maattram Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandha Nookkatha Marakkurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha Nookkatha Marakkurane"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakoottula Sarukkurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakoottula Sarukkurane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirukkethum Ullaara Pudhusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukkethum Ullaara Pudhusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraattaam Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraattaam Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thoduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thoduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkam Lesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkam Lesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththi Viduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththi Viduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valaiyum Nedunchaalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyum Nedunchaalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhir Pooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhir Pooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Araiyum Edhir Kaathula Seragu Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Araiyum Edhir Kaathula Seragu Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Achudhu En Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achudhu En Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedho Maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Thoduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thoduthe"/>
</div>
</pre>
