---
title: "ishtam pole song lyrics"
album: "Ayudha Porattam"
artist: "Nandhan Raj - S.S. Athreya"
lyricist: "Andal Priyadarshini"
director: "Jai Akash"
path: "/albums/ayudha-porattam-lyrics"
song: "Ishtam Pole"
image: ../../images/albumart/ayudha-porattam.jpg
date: 2011-09-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uPX7ue6bRZU"
type: "happy"
singers:
  - Charulatha Mani
  - Nandhan Raj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ishttam poaley thottukko vaseendhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishttam poaley thottukko vaseendhara"/>
</div>
<div class="lyrico-lyrics-wrapper">enga venaa ottikko vaseendhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga venaa ottikko vaseendhara"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu kerangai paaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu kerangai paaradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">munnum pinnum therudaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnum pinnum therudaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjam theenda vaasthoondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjam theenda vaasthoondaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee enna venum seiya vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee enna venum seiya vaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">muththam thandhu kolludaa neeye OhO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muththam thandhu kolludaa neeye OhO"/>
</div>
<div class="lyrico-lyrics-wrapper">thaam thoom vetkam ennai theendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaam thoom vetkam ennai theendum"/>
</div>
<div class="lyrico-lyrics-wrapper">boadhaiyin roagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boadhaiyin roagam"/>
</div>
<div class="lyrico-lyrics-wrapper">nam nam ushnam throgam thoondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam nam ushnam throgam thoondu"/>
</div>
<div class="lyrico-lyrics-wrapper">manmadha mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manmadha mayakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishttam poaley thottukko vaseendhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishttam poaley thottukko vaseendhara"/>
</div>
<div class="lyrico-lyrics-wrapper">enga venaa ottikko vaseendhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga venaa ottikko vaseendhara"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu kerangai paaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu kerangai paaradaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Othikkoadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othikkoadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan penmai thirakka vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan penmai thirakka vaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai meniyil thookki vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai meniyil thookki vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ichi ichi vaiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ichi ichi vaiyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un sevai kaama thevan aasiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sevai kaama thevan aasiye"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam konjam killudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam konjam killudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">moaga baashai solludhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moaga baashai solludhey"/>
</div>
<div class="lyrico-lyrics-wrapper">ichu ichu moottathaan vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ichu ichu moottathaan vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">poorva jenma raasiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poorva jenma raasiye"/>
</div>
<div class="lyrico-lyrics-wrapper">irukku enneramey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukku enneramey"/>
</div>
<div class="lyrico-lyrics-wrapper">adhai theervai sollumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhai theervai sollumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu ucham muththaattavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu ucham muththaattavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai vinnil oottavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai vinnil oottavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaam thoom vetkam ennai theendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaam thoom vetkam ennai theendum"/>
</div>
<div class="lyrico-lyrics-wrapper">boadhaiyin roagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boadhaiyin roagam"/>
</div>
<div class="lyrico-lyrics-wrapper">nam nam ushnam throgam thoondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam nam ushnam throgam thoondu"/>
</div>
<div class="lyrico-lyrics-wrapper">manmadha mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manmadha mayakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishttam poaley thottukko vaseendhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishttam poaley thottukko vaseendhara"/>
</div>
<div class="lyrico-lyrics-wrapper">enga venaa ottikko vaseendhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga venaa ottikko vaseendhara"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu kerangai paaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu kerangai paaradaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machcham eththini ennudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machcham eththini ennudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai yedho pannudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai yedho pannudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">serthukkaamal ketkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serthukkaamal ketkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">angey viththai nooru ketkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angey viththai nooru ketkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">onnum onnum rendu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum onnum rendu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum naanum onnu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum naanum onnu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">solla thaan kashttamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solla thaan kashttamey"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan vaasamey ishttamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan vaasamey ishttamey"/>
</div>
<div class="lyrico-lyrics-wrapper">ini kaadhal thedudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini kaadhal thedudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhu paadhai poadudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhu paadhai poadudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal onney onnudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal onney onnudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aasai thoasai thinnudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasai thoasai thinnudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moththamaai moththamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththamaai moththamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai naaney vettaiyaaduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai naaney vettaiyaaduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennaam vennaam innum pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennaam vennaam innum pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">neram aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishttam poaley thottukko vaseendhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishttam poaley thottukko vaseendhara"/>
</div>
<div class="lyrico-lyrics-wrapper">enga venaa ottikko vaseendhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga venaa ottikko vaseendhara"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu kerangai paaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu kerangai paaradaa"/>
</div>
</pre>
