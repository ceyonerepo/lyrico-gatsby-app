---
title: "chinna ponnu song lyrics"
album: "Mambattiyan"
artist: "S Thaman"
lyricist: "Vairamuthu"
director: "Thiagarajan"
path: "/albums/mambattiyan-lyrics"
song: "Chinna Ponnu"
image: ../../images/albumart/mambattiyan.jpg
date: 2011-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ygz3vqJN-6w"
type: "love"
singers:
  - Harish Raghavendra
  - Shaila
  - Sanjeev Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chinnapponnu Saela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnapponnu Saela"/>
</div>
<div class="lyrico-lyrics-wrapper">Senbagappoo Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senbagappoo Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnapponnu Saela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnapponnu Saela"/>
</div>
<div class="lyrico-lyrics-wrapper">Senbagappoo Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senbagappoo Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae Maaraappu Mayilae Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae Maaraappu Mayilae Poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Venaam Veeraappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Venaam Veeraappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnapponnu Saela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnapponnu Saela"/>
</div>
<div class="lyrico-lyrics-wrapper">Senbagappoo Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senbagappoo Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnapponnu Saela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnapponnu Saela"/>
</div>
<div class="lyrico-lyrics-wrapper">Senbagappoo Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senbagappoo Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyae Maaraappu Arugae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyae Maaraappu Arugae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaa Venaam Veeraappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaa Venaam Veeraappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pogum Vazhiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pogum Vazhiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan Pogum En Sela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Pogum En Sela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pogum Vazhithedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pogum Vazhithedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvenae Pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvenae Pinnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhitheriyaadha Aaru Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhitheriyaadha Aaru Idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idha Nambiththaanaa Oduvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha Nambiththaanaa Oduvadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Vellam Serum Bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Vellam Serum Bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyenna Paadhai Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyenna Paadhai Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatraagi Veesum Bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatraagi Veesum Bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisai Enna Dhesam Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai Enna Dhesam Enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasa Thaal Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Thaal Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilae Nee Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilae Nee Poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaam Velaiyaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaam Velaiyaattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnapponnu Saela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnapponnu Saela"/>
</div>
<div class="lyrico-lyrics-wrapper">Senbagappoo Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senbagappoo Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyae Maaraappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyae Maaraappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayilae Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayilae Poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Venaam Veeraappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Venaam Veeraappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Mela Nee Aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mela Nee Aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaalum Thappilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaalum Thappilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalum Kuyilukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum Kuyilukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindraada Kokkilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindraada Kokkilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thandha Thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thandha Thaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinji Vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinji Vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Nambithaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Nambithaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Olichi Vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olichi Vachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaappu Venaam Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaappu Venaam Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Poochoodum Kaalam Valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poochoodum Kaalam Valla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thoonga Paayum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thoonga Paayum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhaa Gnyaayam Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhaa Gnyaayam Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venaam Pooppaadu Arugae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaam Pooppaadu Arugae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaa Rosaa Poochoodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaa Rosaa Poochoodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnapponnu Saela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnapponnu Saela"/>
</div>
<div class="lyrico-lyrics-wrapper">Senbagappoo Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senbagappoo Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Lalaa Lalaa Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalaa Lalaa Laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Laa Laaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laa Laaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lalaa Lalaa Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lalaa Lalaa Laa"/>
</div>
</pre>
