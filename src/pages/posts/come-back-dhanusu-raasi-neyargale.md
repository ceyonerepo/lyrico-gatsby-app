---
title: "come back song lyrics"
album: "Dhanusu Raasi Neyargale"
artist: "Ghibran"
lyricist: "Chandru"
director: "Sanjay Bharathi"
path: "/albums/dhanusu-raasi-neyargale-lyrics"
song: "Come Back"
image: ../../images/albumart/dhanusu-raasi-neyargale.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4ft27RtKmfs"
type: "happy"
singers:
  - Gold Devaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ponaa Pottum Ponaa Pottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaa Pottum Ponaa Pottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadaa Nee Rajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadaa Nee Rajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa Neeyo Thedi Poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa Neeyo Thedi Poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannaadha Thaaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannaadha Thaaja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponaa Pottum Ponaa Pottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaa Pottum Ponaa Pottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadaa Nee Rajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadaa Nee Rajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanaa Neeyo Thedi Poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanaa Neeyo Thedi Poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannaadha Thaaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannaadha Thaaja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukudaa Vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukudaa Vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Kannu Munnaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kannu Munnaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhukuda Kalangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhukuda Kalangura"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam Kaattum Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam Kaattum Kannaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vida Sigaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vida Sigaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Uyaram Illadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Uyaram Illadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraga Neeyum Virichi Putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraga Neeyum Virichi Putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhudhaan Ellaidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhudhaan Ellaidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come Back Come Back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Come Back"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Back Maamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Maamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Back Come Back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Come Back"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Back Maamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Maamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come Back Come Back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Come Back"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Back Maamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Maamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Back Come Back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Come Back"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Back Maamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Maamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerukkul Neendhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerukkul Neendhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppena Vandhidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppena Vandhidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perukku Vaazhvathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perukku Vaazhvathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkai Illadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai Illadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponadhai Kaalathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponadhai Kaalathin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakkena Kollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakkena Kollada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanaththai Vetti Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaththai Vetti Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Theettada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Theettada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Nondi Saakku Nee Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Nondi Saakku Nee Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandi Poga Nee Ennadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandi Poga Nee Ennadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandi Illaa Sakkaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandi Illaa Sakkaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkai Venaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai Venaanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come Back Come Back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Come Back"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Back Maamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Maamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Back Come Back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Come Back"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Back Maamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Maamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come Back Come Back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Come Back"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Back Maamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Maamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Back Come Back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Come Back"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Back Maamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Back Maamu"/>
</div>
</pre>
