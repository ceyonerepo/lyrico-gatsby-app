---
title: "naa kanulu song lyrics"
album: "Rang De"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/rang-de-lyrics"
song: "Naa Kanulu Yepudu"
image: ../../images/albumart/rang-de.jpg
date: 2021-03-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/r1w-5ybQRC0"
type: "happy"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naa kanulu yepudu kanane kanani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kanulu yepudu kanane kanani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhavulepudu anane anani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavulepudu anane anani"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudayam yepudu vinane vinani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudayam yepudu vinane vinani"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayalo theluthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayalo theluthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa manusu thalupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa manusu thalupe"/>
</div>
<div class="lyrico-lyrics-wrapper">Terachi terachi velugu therale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terachi terachi velugu therale"/>
</div>
<div class="lyrico-lyrics-wrapper">Parachi parachi kalalu nijami yedhuta nilichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parachi parachi kalalu nijami yedhuta nilichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichene ee kshananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichene ee kshananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedhu pai theepilaa reyi pai rangulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedhu pai theepilaa reyi pai rangulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelapai ningilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelapai ningilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Guppedu gundeku pandaga ee velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppedu gundeku pandaga ee velaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kanulu yepudu kanane kanani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kanulu yepudu kanane kanani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhavulepudu anane anani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavulepudu anane anani"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudayam yepudu vinane vinani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudayam yepudu vinane vinani"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayalo theluthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayalo theluthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa manusu thalupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa manusu thalupe"/>
</div>
<div class="lyrico-lyrics-wrapper">Terachi terachi velugu therale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terachi terachi velugu therale"/>
</div>
<div class="lyrico-lyrics-wrapper">Parachi parachi kalalu nijami yedhuta nilichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parachi parachi kalalu nijami yedhuta nilichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichene ee kshananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichene ee kshananaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yepudu leni ee santhoshanni dachalante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yepudu leni ee santhoshanni dachalante"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi chaalo ledo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi chaalo ledo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yepudu raani ee anandhanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yepudu raani ee anandhanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondhe hakkey naakundho ledho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondhe hakkey naakundho ledho"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa anelaa naadhanelaa o prapancham naakivala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa anelaa naadhanelaa o prapancham naakivala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonthamai andhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonthamai andhene"/>
</div>
<div class="lyrico-lyrics-wrapper">Guppedu gundeku pandaga ee velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppedu gundeku pandaga ee velaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kanulu yepudu kanane kanani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kanulu yepudu kanane kanani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhavulepudu anane anani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavulepudu anane anani"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudayam yepudu vinane vinani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudayam yepudu vinane vinani"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayalo theluthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayalo theluthunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanne nene kalisano emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne nene kalisano emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake nene telisano emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake nene telisano emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo nanne chushano emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo nanne chushano emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naala nene maarano emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naala nene maarano emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gathamlo nee kathentho nee gathamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gathamlo nee kathentho nee gathamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kathanthe o kshanam penchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kathanthe o kshanam penchina"/>
</div>
<div class="lyrico-lyrics-wrapper">guppedu gundeku pandaga aa vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guppedu gundeku pandaga aa vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kanulu yepudu kanane kanani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kanulu yepudu kanane kanani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhavulepudu anane anani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavulepudu anane anani"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudayam yepudu vinane vinani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudayam yepudu vinane vinani"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayalo theluthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayalo theluthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa manusu thalupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa manusu thalupe"/>
</div>
<div class="lyrico-lyrics-wrapper">Terachi terachi velugu therale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terachi terachi velugu therale"/>
</div>
<div class="lyrico-lyrics-wrapper">Parachi parachi kalalu nijami yedhuta nilichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parachi parachi kalalu nijami yedhuta nilichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilichene ee kshananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilichene ee kshananaa"/>
</div>
</pre>
