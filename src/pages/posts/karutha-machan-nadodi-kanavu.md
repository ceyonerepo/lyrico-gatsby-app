---
title: "karutha machan song lyrics"
album: "Nadodi Kanavu"
artist: "Sabesh Murali"
lyricist: "Sirgazhi Sirpi"
director: "Veera Selva"
path: "/albums/nadodi-kanavu-lyrics"
song: "Karutha Machan"
image: ../../images/albumart/nadodi-kanavu.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/mdB6nyCGulI"
type: "love"
singers:
  - Sathya Prakash
  - Malavika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">karutha machan mayakki puttan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karutha machan mayakki puttan"/>
</div>
<div class="lyrico-lyrics-wrapper">adi manasa keduthu putan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi manasa keduthu putan"/>
</div>
<div class="lyrico-lyrics-wrapper">sevatha pulla kavuthu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevatha pulla kavuthu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">vasiyam panni valachu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasiyam panni valachu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">kundu mani pola un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kundu mani pola un "/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla irunthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla irunthene"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnale kaanala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnale kaanala"/>
</div>
<div class="lyrico-lyrics-wrapper">vilunthene thanaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilunthene thanaale"/>
</div>
<div class="lyrico-lyrics-wrapper">koo koo koo koo koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koo koo koo koo koo"/>
</div>
<div class="lyrico-lyrics-wrapper">koo koo koo koo koo koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koo koo koo koo koo koo"/>
</div>
<div class="lyrico-lyrics-wrapper">koo koo koo koo koo koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koo koo koo koo koo koo"/>
</div>
<div class="lyrico-lyrics-wrapper">koo koo koo koo koo koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koo koo koo koo koo koo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paravaiyai naan irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravaiyai naan irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">panjaaram nee aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panjaaram nee aana"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhikunju pola ena adikiraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhikunju pola ena adikiraye"/>
</div>
<div class="lyrico-lyrics-wrapper">veliyila nee pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veliyila nee pona"/>
</div>
<div class="lyrico-lyrics-wrapper">parunthu thaan thookume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parunthu thaan thookume"/>
</div>
<div class="lyrico-lyrics-wrapper">athuku thaan naan unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athuku thaan naan unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kaakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kaakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhusoliyaal kollai aducha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhusoliyaal kollai aducha"/>
</div>
<div class="lyrico-lyrics-wrapper">ithayathile vella adicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayathile vella adicha"/>
</div>
<div class="lyrico-lyrics-wrapper">kathorathu kammal rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathorathu kammal rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">un pera than solluthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pera than solluthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">methu methuva naan tholaigirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="methu methuva naan tholaigirene"/>
</div>
<div class="lyrico-lyrics-wrapper">sogam sogamaa naan karaigirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sogam sogamaa naan karaigirene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uththu nee paarthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uththu nee paarthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">pattunu poothene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattunu poothene"/>
</div>
<div class="lyrico-lyrics-wrapper">thottu nee paarkathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu nee paarkathan"/>
</div>
<div class="lyrico-lyrics-wrapper">naan yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan yenguren"/>
</div>
<div class="lyrico-lyrics-wrapper">koodave naan varava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodave naan varava"/>
</div>
<div class="lyrico-lyrics-wrapper">koochatha naan thodava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koochatha naan thodava"/>
</div>
<div class="lyrico-lyrics-wrapper">kovathula nee paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovathula nee paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">naan thenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thenguren"/>
</div>
<div class="lyrico-lyrics-wrapper">adai malaiyaai vanthu vilava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adai malaiyaai vanthu vilava"/>
</div>
<div class="lyrico-lyrics-wrapper">idaiveliyai kondru vidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaiveliyai kondru vidava"/>
</div>
<div class="lyrico-lyrics-wrapper">pullu kattu aattu kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullu kattu aattu kutti"/>
</div>
<div class="lyrico-lyrics-wrapper">mallu katti meyuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mallu katti meyuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">alai alaiyai vanthu adikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alai alaiyai vanthu adikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">madai thirapai thinam thavikirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madai thirapai thinam thavikirene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karutha machan mayakki puttan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karutha machan mayakki puttan"/>
</div>
<div class="lyrico-lyrics-wrapper">adi manasa keduthu putan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi manasa keduthu putan"/>
</div>
<div class="lyrico-lyrics-wrapper">sevatha pulla kavuthu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sevatha pulla kavuthu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">vasiyam panni valachu putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasiyam panni valachu putta"/>
</div>
<div class="lyrico-lyrics-wrapper">kundu mani pola un 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kundu mani pola un "/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla irunthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla irunthene"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnale kaanala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnale kaanala"/>
</div>
<div class="lyrico-lyrics-wrapper">vilunthene thanaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilunthene thanaale"/>
</div>
<div class="lyrico-lyrics-wrapper">koo koo koo koo koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koo koo koo koo koo"/>
</div>
<div class="lyrico-lyrics-wrapper">koo koo koo koo koo koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koo koo koo koo koo koo"/>
</div>
<div class="lyrico-lyrics-wrapper">koo koo koo koo koo koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koo koo koo koo koo koo"/>
</div>
<div class="lyrico-lyrics-wrapper">koo koo koo koo koo koo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koo koo koo koo koo koo"/>
</div>
</pre>
