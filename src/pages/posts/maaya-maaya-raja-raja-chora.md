---
title: "maaya maaya song lyrics"
album: "Raja Raja Chora"
artist: "Vivek Sagar"
lyricist: "Sanapati Bharadwaj Patrudu"
director: "Hasith Goli"
path: "/albums/raja-raja-chora-lyrics"
song: "Maaya Maaya"
image: ../../images/albumart/raja-raja-chora.jpg
date: 2021-08-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/qBf9Yj2JQZ0"
type: "happy"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ekuvalo Maaya Seekatilo Maaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekuvalo Maaya Seekatilo Maaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Soopulalo Maaya Oosulalo Maaya Aeyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soopulalo Maaya Oosulalo Maaya Aeyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekuvalo Maaya Seekatilo Maaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekuvalo Maaya Seekatilo Maaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Soopulalo Maaya Oosulalo Maaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soopulalo Maaya Oosulalo Maaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaya Maaya Veera Saaho Dheera Shoora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Maaya Veera Saaho Dheera Shoora"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninginela Poora Swaaha Chesaaveraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninginela Poora Swaaha Chesaaveraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya Maaya Veera Saaho Dheera Shoora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Maaya Veera Saaho Dheera Shoora"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninginela Poora Swaaha Chesaaveraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninginela Poora Swaaha Chesaaveraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aey Addadde Kottavathaaram Etthesaare Meeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aey Addadde Kottavathaaram Etthesaare Meeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaleri Kinchitthainaa Shankinchetivaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaleri Kinchitthainaa Shankinchetivaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Chitraala Jantharu Mantharu Pettenu Tecchesaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Chitraala Jantharu Mantharu Pettenu Tecchesaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathakatti Chitraalenno Choopinchesthunnaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathakatti Chitraalenno Choopinchesthunnaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnarachham Meeriddharoka Achhu Hallullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnarachham Meeriddharoka Achhu Hallullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Cheppandayyaa Ee Hechhulika Tagginchedhellaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Cheppandayyaa Ee Hechhulika Tagginchedhellaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fly High Two Wings Leni Vayassulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fly High Two Wings Leni Vayassulai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sky Mottham Chuttaaroi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sky Mottham Chuttaaroi"/>
</div>
<div class="lyrico-lyrics-wrapper">Fly High… Two Light Weight Manassulaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fly High… Two Light Weight Manassulaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sky Mottham Chuttaaroi Ettaaroi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sky Mottham Chuttaaroi Ettaaroi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekuvalona Maaya Seekatilonu Maaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekuvalona Maaya Seekatilonu Maaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Oosulu Chaanaa Maaya Soopulu Koodaa Maaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosulu Chaanaa Maaya Soopulu Koodaa Maaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekuvalona Maaya Seekatilonu Maaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekuvalona Maaya Seekatilonu Maaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Oosanthaa Chaanaa Maaya Soopulu Koodaa Maaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosanthaa Chaanaa Maaya Soopulu Koodaa Maaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya Maaya Maaya Maaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Maaya Maaya Maaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inthalo Enduko Vinthaga Intha Ullaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthalo Enduko Vinthaga Intha Ullaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhile Pondhulo Jantagaa Konte Sallaapam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhile Pondhulo Jantagaa Konte Sallaapam"/>
</div>
<div class="lyrico-lyrics-wrapper">Edha Golagola Eela Vese Ee Haayilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edha Golagola Eela Vese Ee Haayilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaalipadi Jaavaleela Jolaali Paade Ee Gaalilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaalipadi Jaavaleela Jolaali Paade Ee Gaalilo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam Tharinchelaa Ee Velaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Tharinchelaa Ee Velaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Vere Prapanchaale Cheraalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vere Prapanchaale Cheraalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Matthulo Etthulo Kotthagaa Undhi Yavvaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthulo Etthulo Kotthagaa Undhi Yavvaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddhele Vaddhule Addugaa Undhi Bandaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddhele Vaddhule Addugaa Undhi Bandaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ehe Dhorakani Doragaaru Giri Dhaatenanta Joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ehe Dhorakani Doragaaru Giri Dhaatenanta Joru"/>
</div>
<div class="lyrico-lyrics-wrapper">Giri Dhaatenanta Joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giri Dhaatenanta Joru"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Iru Padavala Poru Chesenu Horaahoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Iru Padavala Poru Chesenu Horaahoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesenu HoraaHoru Inanante Thedde Jaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesenu HoraaHoru Inanante Thedde Jaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unte Achham Meeriddharoka Achhu Hallullaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unte Achham Meeriddharoka Achhu Hallullaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Cheppandayyaa Ee Chikkulani Thappinchedhellaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Cheppandayyaa Ee Chikkulani Thappinchedhellaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fly High Two Wings Leni Vayassulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fly High Two Wings Leni Vayassulai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sky Mottham Chuttaaroi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sky Mottham Chuttaaroi"/>
</div>
<div class="lyrico-lyrics-wrapper">Fly High Two Light Weight Manassulaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fly High Two Light Weight Manassulaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sky Mottham Chuttaaroi Ettaaroi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sky Mottham Chuttaaroi Ettaaroi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aey Addadde Kottavathaaram Etthesaare Meeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aey Addadde Kottavathaaram Etthesaare Meeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaleri Kinchitthainaa Shankinchetivaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaleri Kinchitthainaa Shankinchetivaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya Maaya Veera Saaho Dheera Shoora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Maaya Veera Saaho Dheera Shoora"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninginela Poora Swaaha Chesaaveraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninginela Poora Swaaha Chesaaveraa"/>
</div>
</pre>
