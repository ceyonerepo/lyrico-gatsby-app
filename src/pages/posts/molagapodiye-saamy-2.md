---
title: "molagapodiye song lyrics"
album: "Saamy 2"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari"
path: "/albums/saamy-2-song-lyrics"
song: "Molagapodiye"
image: ../../images/albumart/saamy-2.jpg
date: 2018-09-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ck-utgwMx8o"
type: "Love"
singers:
  - Sanjith Hegde
  - Rita Thyagarajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Molagapodiyae molagapodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molagapodiyae molagapodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En inippaana molagapodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En inippaana molagapodiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dumku chikku dum….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumku chikku dum…."/>
</div>
<div class="lyrico-lyrics-wrapper">Chikku dum….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikku dum…."/>
</div>
<div class="lyrico-lyrics-wrapper">Chikku dum….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikku dum…."/>
</div>
<div class="lyrico-lyrics-wrapper">Chikku dum…. ((3) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikku dum…. ((3) times)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dumku chikku dum….((4) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumku chikku dum….((4) times)"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kaadhalicha pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaadhalicha pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikitta pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikitta pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaram onnum koraiyalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaram onnum koraiyalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Molagapodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molagapodiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dumku chikku dum….((4) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumku chikku dum….((4) times)"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaipudicha yogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaipudicha yogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeruthadi vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeruthadi vegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai innum maraiyalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai innum maraiyalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulasi chediyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulasi chediyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dumku chikku dum….((4) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumku chikku dum….((4) times)"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi eppavumae neethan venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi eppavumae neethan venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi unnudaiya nizhalthan naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi unnudaiya nizhalthan naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kovathula kooda kovapazham pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kovathula kooda kovapazham pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai sundi izhukuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai sundi izhukuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Molagapodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molagapodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Molagapodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molagapodiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kaadhalicha pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaadhalicha pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattikitta pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikitta pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeram onnum koraiyalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram onnum koraiyalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai vediyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai vediyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dumku chikku dum….((4) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dumku chikku dum….((4) times)"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nethiyila poosum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nethiyila poosum"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhanathin vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhanathin vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai vittu pogalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai vittu pogalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokku podiyae..haan haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokku podiyae..haan haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokku podiyae…ae…hae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokku podiyae…ae…hae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannananna nanna nanna naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannananna nanna nanna naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana nanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannananna nanna nanna naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannananna nanna nanna naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaana naana thana nana naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaana naana thana nana naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaana naana thana nana naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaana naana thana nana naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana naana naaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana naana naaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa…oorukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa…oorukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnavidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnavidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Perazhagi yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perazhagi yaarumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootivaippen en kannukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootivaippen en kannukulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meesaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa mudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pootukava modhirama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootukava modhirama"/>
</div>
<div class="lyrico-lyrics-wrapper">En viralula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En viralula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan kannala izhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan kannala izhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyala anaichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyala anaichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsaara muththam kodukava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaara muththam kodukava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada ennoda elumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ennoda elumbum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda sathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda sathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnakki uyiraa padaikkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnakki uyiraa padaikkava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Molagapodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molagapodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Molagapodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molagapodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Molagapodiyae..ae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molagapodiyae..ae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Molagapodiyae…ae…..ae….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molagapodiyae…ae…..ae…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei…hei hei…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei…hei hei…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooralula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooralula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooralula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooralula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nanainja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nanainja"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nanainja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nanainja"/>
</div>
<div class="lyrico-lyrics-wrapper">Selaiyila naan thuvatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selaiyila naan thuvatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Serthu unnai kattikava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthu unnai kattikava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nadantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nadantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nadantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nadantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jalli kallu kuthu munnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalli kallu kuthu munnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poova vazhiyil thoovidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poova vazhiyil thoovidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda uduppum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda uduppum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda uduppum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda uduppum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaaga vachu rasikava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaaga vachu rasikava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda thaniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda thaniya"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaaga amarnthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaaga amarnthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaana thananaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaana thananaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Molagapodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molagapodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Molagapodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molagapodiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Molagapodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molagapodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Molagapodiyae…aee….ae….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molagapodiyae…aee….ae…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei hei hei"/>
</div>
</pre>
