---
title: "cheeni chillaallee song lyrics"
album: "Sketch"
artist: "S. Thaman"
lyricist: "Vivek"
director: "Vijay Chandar"
path: "/albums/sketch-lyrics"
song: "Cheeni Chillaallee"
image: ../../images/albumart/sketch.jpg
date: 2018-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xa8tHL8PmyY"
type: "love"
singers:
  - Shweta Mohan
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thendral Thindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral Thindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Cheeni Chillaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Cheeni Chillaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooche Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooche Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Theni Kannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Theni Kannaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thendral Thindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral Thindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Seeni Chillaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Seeni Chillaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooche Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooche Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Theni Kannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Theni Kannaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mutha Thuli Yendha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mutha Thuli Yendha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallam Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallam Aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellai Idai Paarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai Idai Paarka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kullam Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kullam Aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai Vazhai Pechil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai Vazhai Pechil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellam Aanen Ammu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellam Aanen Ammu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kodi Pengal Perazhagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kodi Pengal Perazhagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Serthu Vaithen Oridam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthu Vaithen Oridam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serthu Unnil Paathi Illai Paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthu Unnil Paathi Illai Paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Maya Pookkal Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maya Pookkal Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondu Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thaadi Kulle Moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thaadi Kulle Moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondu Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondu Po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Ketkum Podhu Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ketkum Podhu Kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaasam Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaasam Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enake Thanthu Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enake Thanthu Po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thendral Thindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral Thindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Cheeni Chillaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Cheeni Chillaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooche Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooche Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Theni Kannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Theni Kannaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marabanu Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marabanu Varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Magarantha Mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magarantha Mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaai Thoovudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaai Thoovudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanadhu Vazhi Idayathin Oli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanadhu Vazhi Idayathin Oli"/>
</div>
<div class="lyrico-lyrics-wrapper">Isayayi Thaavudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isayayi Thaavudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kaadhil Nizhal En Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaadhil Nizhal En Maari"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaalam Adhil Elai Paari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaalam Adhil Elai Paari"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Per Mazhaidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Per Mazhaidhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Natkuripil Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natkuripil Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kooda Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kooda Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Vaazhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Vaazhava"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaadhal Kooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaadhal Kooti"/>
</div>
<div class="lyrico-lyrics-wrapper">Penmai Aalava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penmai Aalava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Theeti Chellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Theeti Chellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Penmai Aalava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penmai Aalava"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Kooda Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Kooda Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Seravaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Seravaa"/>
</div>
</pre>
