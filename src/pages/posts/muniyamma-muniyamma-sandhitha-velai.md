---
title: "muniyamma muniyamma song lyrics"
album: "Sandhitha Velai"
artist: "Deva"
lyricist: "Ponniyin Selvan"
director: "Ravichandran"
path: "/albums/sandhitha-velai-lyrics"
song: "Muniyamma Muniyamma"
image: ../../images/albumart/sandhitha-velai.jpg
date: 2000-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3fIUGPM9j9Q"
type: "happy"
singers:
  - Sabesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Munima munima munima munima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munima munima munima munima"/>
</div>
<div class="lyrico-lyrics-wrapper">Munima munima munima munima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munima munima munima munima"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munima munima munima munima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munima munima munima munima"/>
</div>
<div class="lyrico-lyrics-wrapper">Munima munima munima munima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munima munima munima munima"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kovapadathae munima kovapadathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovapadathae munima kovapadathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovapadathae munima kovapadathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovapadathae munima kovapadathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Showa thatcha pudavaiyathaan idupula katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Showa thatcha pudavaiyathaan idupula katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodambakkam poralae oru pombala kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodambakkam poralae oru pombala kutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Showa thatcha pudavaiyathaan idupula katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Showa thatcha pudavaiyathaan idupula katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodambakkam poralae oru pombala kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodambakkam poralae oru pombala kutti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava banarassu patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava banarassu patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa kanchipuram chitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa kanchipuram chitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kaiya koncha thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kaiya koncha thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">En kanathula vittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanathula vittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovapadathae munima kovapadathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovapadathae munima kovapadathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovapadathae munima kovapadathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovapadathae munima kovapadathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munima munima munima munima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munima munima munima munima"/>
</div>
<div class="lyrico-lyrics-wrapper">Munima aaama ma ma ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munima aaama ma ma ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Munima munima munima munima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munima munima munima munima"/>
</div>
<div class="lyrico-lyrics-wrapper">Munima aaama ma ma ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munima aaama ma ma ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ambatur pakkam ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambatur pakkam ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aunty nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aunty nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooku kuthi parkaillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooku kuthi parkaillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooku nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooku nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake panni nadakum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake panni nadakum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Walk nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Walk nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava sollu nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava sollu nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiaiyo lollu nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiaiyo lollu nalla illae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amijikarai pakkam ponna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amijikarai pakkam ponna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekka nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekka nalla illae"/>
</div>
 <div class="lyrico-lyrics-wrapper">Nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka sutta sukka roti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka sutta sukka roti"/>
</div>
<div class="lyrico-lyrics-wrapper">Palluku nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palluku nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirika solli parthu puten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirika solli parthu puten"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirippu nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirippu nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava lip nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava lip nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiaiyo hip nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiaiyo hip nalla illae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ice vaanga pockettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice vaanga pockettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summer season marketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summer season marketu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thachi thaaren jacketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thachi thaaren jacketu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu tharen locket tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu tharen locket tu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koruku pettai kuthu gummangkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koruku pettai kuthu gummangkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava bigulu vitta figure motham pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava bigulu vitta figure motham pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koruku pettai kuthu gummangkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koruku pettai kuthu gummangkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava bigulu vitta figure motham pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava bigulu vitta figure motham pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovapadathae munima kovapadathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovapadathae munima kovapadathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovapadathae munima kovapadathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovapadathae munima kovapadathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munima munima munima munima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munima munima munima munima"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poondhamalli athai ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poondhamalli athai ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rosy nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rosy nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottivaakkam kumari ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottivaakkam kumari ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gowri nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gowri nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sowri vechu pinnal potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sowri vechu pinnal potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jadaiyum nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadaiyum nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava appan nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava appan nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaiku ribbon nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaiku ribbon nalla illae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simha rasi ponnu kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simha rasi ponnu kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Silpaans nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silpaans nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adutha veetu kudiyirukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha veetu kudiyirukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Alphonse nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alphonse nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattil pottu thoongi partha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattil pottu thoongi partha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava lookum nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava lookum nalla illae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammamma kickum nalla illae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammamma kickum nalla illae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa minuma vaa endren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa minuma vaa endren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkapattu vandhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkapattu vandhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha munima tha endren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha munima tha endren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayagi thaan ponaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayagi thaan ponaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalicha godu andha godu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalicha godu andha godu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namala serthu veppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namala serthu veppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhool rombo dhool
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhool rombo dhool"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalicha godu andha godu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalicha godu andha godu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namala serthu veppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namala serthu veppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhool rombo dhool
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhool rombo dhool"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Showa thatcha pudavaiyathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Showa thatcha pudavaiyathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idupula katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idupula katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodambakkam poralae oru pombala kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodambakkam poralae oru pombala kutti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava banarassu patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava banarassu patta"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa kanchipuram chitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa kanchipuram chitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kaiya koncha thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kaiya koncha thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">En kanathula vittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanathula vittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovapadathae munima kovapadathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovapadathae munima kovapadathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovapadathae munima kovapadathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovapadathae munima kovapadathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munima munima munima munima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munima munima munima munima"/>
</div>
</pre>
