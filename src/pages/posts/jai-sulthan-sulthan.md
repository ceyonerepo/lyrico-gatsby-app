---
title: "jai sulthan song lyrics"
album: "Sulthan"
artist: "vivek - mervin"
lyricist: "viveka"
director: "Bakkiyaraj Kannan"
path: "/albums/sulthan-song-lyrics"
song: "Jai Sulthan"
image: ../../images/albumart/sulthan.jpg
date: 2020-04-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4QC-xQwldcQ"
type: "celebration"
singers:
  - Anirudh Ravichander
  - Junior Nithya
  - Gana Guna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jai Sultan! Jai Sultan!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Sultan! Jai Sultan!"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandaiyila Kiyatha Sattaiyilla Kumare,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaiyila Kiyatha Sattaiyilla Kumare,"/>
</div>
<div class="lyrico-lyrics-wrapper">Manda Renda Povata Sanda Romba Sumare,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manda Renda Povata Sanda Romba Sumare,"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Dada Mare Hey Douloth Mare,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Dada Mare Hey Douloth Mare,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Anba Kottum En Annam Mare Ayo,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Anba Kottum En Annam Mare Ayo,"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Dada Mare Hey Douloth Mare,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Dada Mare Hey Douloth Mare,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Anba Kottum En Annam Mare,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Anba Kottum En Annam Mare,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Mathanalu Kithanrathea Sandhega Case-u,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Mathanalu Kithanrathea Sandhega Case-u,"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Mansanunga Ellarume Comedy Peseu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Mansanunga Ellarume Comedy Peseu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vayakatti Vaiththa Katti Sekkadha Kaase,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vayakatti Vaiththa Katti Sekkadha Kaase,"/>
</div>
<div class="lyrico-lyrics-wrapper">Soru Thunnumpothu Vikkichina Ellam Close,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soru Thunnumpothu Vikkichina Ellam Close,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va Sulthan Va Sulthan Va Sulthan Va Sulthan Va,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Sulthan Va Sulthan Va Sulthan Va Sulthan Va,"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakunudhan Tharava Tharava Usura Tharava,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakunudhan Tharava Tharava Usura Tharava,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakurye Sulthan Va Sulthan Va Sulthan Va Sulthan Va,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakurye Sulthan Va Sulthan Va Sulthan Va Sulthan Va,"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakunudhan Tharava Tharava Usura Tharava,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakunudhan Tharava Tharava Usura Tharava,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Dada Mare Hey Douloth Mare,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Dada Mare Hey Douloth Mare,"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Anba Kottum En Annam Mare,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Anba Kottum En Annam Mare,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Nikkal Hey Kundhal Hey Nikkal Hey Kundhal,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nikkal Hey Kundhal Hey Nikkal Hey Kundhal,"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Nikkal Kundhal Nikkal Kundhal,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Nikkal Kundhal Nikkal Kundhal,"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkal Kundhal Nikkal Kundhal,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkal Kundhal Nikkal Kundhal,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooril Romba Peru Moonja Uthoo Paru,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooril Romba Peru Moonja Uthoo Paru,"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooram Ootu Irupom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooram Ootu Irupom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Moratu Piece Yellam Oorutu Kattaiyala,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moratu Piece Yellam Oorutu Kattaiyala,"/>
</div>
<div class="lyrico-lyrics-wrapper">Perati Perati Yedupom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perati Perati Yedupom,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokka Matikanu Soli Paka Pona,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka Matikanu Soli Paka Pona,"/>
</div>
<div class="lyrico-lyrics-wrapper">Thokka Ala Pudipom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokka Ala Pudipom,"/>
</div>
<div class="lyrico-lyrics-wrapper">Beeda Pota Mery Vaya Koyapitu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beeda Pota Mery Vaya Koyapitu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Tata Kati Varuvom,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tata Kati Varuvom,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samavam Seum Velayellam,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samavam Seum Velayellam,"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjaru Varam Othi Podu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaru Varam Othi Podu,"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambuku Yarum Vandhalum Kuda,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambuku Yarum Vandhalum Kuda,"/>
</div>
<div class="lyrico-lyrics-wrapper">Valalar Pola Vanakam Podu,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valalar Pola Vanakam Podu,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va Sulthan Va Sulthan Va Sulthan Va Sulthan Va,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Sulthan Va Sulthan Va Sulthan Va Sulthan Va,"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakunudhan Tharava Tharava Usura Tharava,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakunudhan Tharava Tharava Usura Tharava,"/>
</div>
<div class="lyrico-lyrics-wrapper">Are Va Va Sulthan Va Sulthan Va Sulthan Va Sulthan Va,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Va Va Sulthan Va Sulthan Va Sulthan Va Sulthan Va,"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakunudhan Tharava Tharava Usura Tharava.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakunudhan Tharava Tharava Usura Tharava."/>
</div>
</pre>
