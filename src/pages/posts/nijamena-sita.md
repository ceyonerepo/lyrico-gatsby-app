---
title: "nijamena song lyrics"
album: "Sita"
artist: "Anoop Rubens"
lyricist: "Lakshmi Bhupal"
director: "Teja"
path: "/albums/sita-lyrics"
song: "Nijamena"
image: ../../images/albumart/sita.jpg
date: 2019-05-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/OHb76_wcVak"
type: "sad"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Evaradi Evaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaradi Evaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedha gadilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedha gadilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalapula thalupulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalapula thalupulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Therachinadi Nijamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therachinadi Nijamenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vethike praaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethike praaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedurainadaa alisainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedurainadaa alisainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai Veedani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai Veedani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudi Padinadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudi Padinadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alanai manasanchuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alanai manasanchuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Istamgaa Thalavanchanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Istamgaa Thalavanchanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosam Neekosam Vechinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosam Neekosam Vechinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee praanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamenaa nijamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamenaa nijamenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethike praaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethike praaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedurainadaa alisainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedurainadaa alisainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai Veedani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai Veedani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudi Padinadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudi Padinadaa"/>
</div>
</pre>
