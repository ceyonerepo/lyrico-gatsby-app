---
title: "merise merise song lyrics"
album: "SR Kalyanamandapam"
artist: "Chaitan Bharadwaj"
lyricist: "Bhaskarabhatla Ravi Kumar"
director: "Sridhar Gade"
path: "/albums/sr-kalyanamandapam-lyrics"
song: "Merise Merise"
image: ../../images/albumart/sr-kalyanamandapam.jpg
date: 2021-08-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/mc_1oG9OFZA"
type: "happy"
singers:
  - Chaitan Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Merise merise merise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merise merise merise"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanule tholigaa merise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanule tholigaa merise"/>
</div>
<div class="lyrico-lyrics-wrapper">Arerey kalale edhute vaalenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arerey kalale edhute vaalenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase manase manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase manase manase"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanalo thanuga murise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanalo thanuga murise"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurise aanandhamlo thadisenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurise aanandhamlo thadisenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee kshanam edhaki vinabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee kshanam edhaki vinabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholi gelupe piliche pilupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi gelupe piliche pilupe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhivaraku erugani merupula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhivaraku erugani merupula"/>
</div>
<div class="lyrico-lyrics-wrapper">Malupu ipude ipude ipude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malupu ipude ipude ipude"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitranga chusthu unte neruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitranga chusthu unte neruga"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaitrale varaalu theesene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaitrale varaalu theesene"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttura sumalu pusene hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttura sumalu pusene hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thonikasale kalale kalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thonikasale kalale kalale"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhanga bandhalalle velalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhanga bandhalalle velalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaakitlo vasanthamochhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaakitlo vasanthamochhene"/>
</div>
<div class="lyrico-lyrics-wrapper">Dositlo varaali nimpene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dositlo varaali nimpene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinchani pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchani pranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukonidhi saayam ayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukonidhi saayam ayina"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugu atu vaipe kadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugu atu vaipe kadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinchanedhaanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchanedhaanam "/>
</div>
<div class="lyrico-lyrics-wrapper">chupinchene theeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chupinchene theeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanuke manasu atu vaipe kadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanuke manasu atu vaipe kadhile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okaritho okaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okaritho okaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vodipadi tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vodipadi tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavaraku veediponi kalyana yogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavaraku veediponi kalyana yogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naluguru kalise kalividi samayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naluguru kalise kalividi samayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee haayi chirakaala gnapakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee haayi chirakaala gnapakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatha kalipe guname alavataithe balame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatha kalipe guname alavataithe balame"/>
</div>
<div class="lyrico-lyrics-wrapper">Panilo padithe niluvuna paravashame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panilo padithe niluvuna paravashame"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhadiga janame ontariga maname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhadiga janame ontariga maname"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhilo medhile teliyani kalavarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhilo medhile teliyani kalavarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinchanedhaanam chupinchene theeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchanedhaanam chupinchene theeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanuke manasu atu vaipe kadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanuke manasu atu vaipe kadhile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varasalu kalipi manasulu thelipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varasalu kalipi manasulu thelipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Arumerikala podaleni sangathyamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumerikala podaleni sangathyamena"/>
</div>
<div class="lyrico-lyrics-wrapper">Alasata marichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alasata marichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Atu itu thirigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atu itu thirigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorantha vinipinche veduka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorantha vinipinche veduka"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhi okate adige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhi okate adige"/>
</div>
<div class="lyrico-lyrics-wrapper">valavanne vinadhe asale thanadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valavanne vinadhe asale thanadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhagani pasi thaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhagani pasi thaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana kosam taruchu parigeduthu paduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana kosam taruchu parigeduthu paduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanu korinadhi vethukuta avasarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanu korinadhi vethukuta avasarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinchani pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchani pranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukonidhi saayam ayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukonidhi saayam ayina"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugu atu vaipe kadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugu atu vaipe kadhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipinchanedhaanam chupinchene theeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipinchanedhaanam chupinchene theeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanuke manasu atu vaipe kadhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanuke manasu atu vaipe kadhile"/>
</div>
</pre>
