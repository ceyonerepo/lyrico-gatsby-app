---
title: "netri kungumam song lyrics"
album: "Kaalakkoothu"
artist: "Justin Prabhakaran"
lyricist: "M Nagarajan"
director: "M. Nagarajan"
path: "/albums/kaalakkoothu-lyrics"
song: "Netri Kungumam"
image: ../../images/albumart/kaalakkoothu.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DcOcCMJchI4"
type: "love"
singers:
  - V.V. Prasanna
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Netri kungumam nee sooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri kungumam nee sooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Eththanai thavam naan seitheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththanai thavam naan seitheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottrai thingal unai sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottrai thingal unai sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingoru piravi edutheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingoru piravi edutheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonil oorum uyirenavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonil oorum uyirenavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu unakkulae vaazhven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu unakkulae vaazhven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kadhal thanthavan neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kadhal thanthavan neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyiril kalandhavan thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyiril kalandhavan thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thunaiyaai varuven naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thunaiyaai varuven naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un manathai vendraval naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un manathai vendraval naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nizhalaai thodarbaval thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nizhalaai thodarbaval thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhivin thedal neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhivin thedal neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaikengiya boomiyum naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaikengiya boomiyum naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthal mazhayaai nanaithaval neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal mazhayaai nanaithaval neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un anbil karainthae ponen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un anbil karainthae ponen "/>
</div>
<div class="lyrico-lyrics-wrapper">hmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir eeramaagha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir eeramaagha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam oramaagha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam oramaagha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thanthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thanthaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkullae maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkullae maatram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kaadhal thanthaval neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal thanthaval neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyiril kalandhaval thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyiril kalandhaval thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thunaiyaai varuven naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thunaiyaai varuven naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netri kungumam nee sooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri kungumam nee sooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Eththanai thavam naan seitheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththanai thavam naan seitheno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottrai thingal unai sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottrai thingal unai sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingoru piravi edutheno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingoru piravi edutheno"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonil oorum uyirenavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonil oorum uyirenavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu unakkulae vaazhven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu unakkulae vaazhven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaivinil eriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivinil eriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai vilakkoliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai vilakkoliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Therinthidum un mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therinthidum un mugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaiyinil thavazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyinil thavazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilai ena naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilai ena naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Midhanthidum en manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhanthidum en manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyil nanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyil nanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhalai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhalai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjam aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjam aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Angum ingumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angum ingumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaiyaikodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyaikodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivan neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivan neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnalae pongum inbame aeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnalae pongum inbame aeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En thanimayil unnai thanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thanimayil unnai thanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaagavae neeyum vanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaagavae neeyum vanthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru idhayangal sernthidum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru idhayangal sernthidum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgam engum kaadhal sangeetham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgam engum kaadhal sangeetham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa sa ri ma pa pa dha ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa ri ma pa pa dha ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ri ri ma ga ri sa ni dha ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ri ri ma ga ri sa ni dha ni sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa sa ri ma pa pa dha ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa ri ma pa pa dha ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ri ri ma ga ri sa ni dha ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ri ri ma ga ri sa ni dha ni sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa ri ri ma ma ma ri ma pha dha ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ri ri ma ma ma ri ma pha dha ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa sa sa pa ni dha ma ga ri sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa sa sa pa ni dha ma ga ri sa"/>
</div>
</pre>
