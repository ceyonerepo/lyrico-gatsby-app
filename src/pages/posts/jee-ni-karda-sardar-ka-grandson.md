---
title: "jee ni karda song lyrics"
album: "Sardar Ka Grandson"
artist: "Tanishk Bagchi - Manak-E"
lyricist: "Tanishk Bagchi"
director: "Kaashvie Nair"
path: "/albums/sardar-ka-grandson-lyrics"
song: "Jee Ni Karda"
image: ../../images/albumart/sardar-ka-grandson.jpg
date: 2021-05-18
lang: hindi
youtubeLink: "https://www.youtube.com/embed/xvgN1hMOAWU"
type: "Celebration"
singers:
  - Jass Manak
  - Manak-E
  - Nikhita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ajj tu mainu kol bitha le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajj tu mainu kol bitha le"/>
</div>
<div class="lyrico-lyrics-wrapper">Kol bitha le seene la le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kol bitha le seene la le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ajj tu mainu kol bitha le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajj tu mainu kol bitha le"/>
</div>
<div class="lyrico-lyrics-wrapper">Kol bitha le seene la le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kol bitha le seene la le"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar tere bin mainu lagda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar tere bin mainu lagda"/>
</div>
<div class="lyrico-lyrics-wrapper">Main tan mar jaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main tan mar jaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaye main tan mar jaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaye main tan mar jaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hunn taithon taithon taithon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunn taithon taithon taithon"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunn taithon mera door jaan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunn taithon mera door jaan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee ni karda ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee ni karda ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunn taithon mera door jaan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunn taithon mera door jaan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee ni karda ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee ni karda ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunn taithon mera door jaan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunn taithon mera door jaan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee ni karda ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee ni karda ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunn taithon mera door jaan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunn taithon mera door jaan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee ni karda ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee ni karda ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Now check this out!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Now check this out!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tere husna de charche ne bahle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tere husna de charche ne bahle"/>
</div>
<div class="lyrico-lyrics-wrapper">Har passe gallan teriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har passe gallan teriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere husna de charche ne bahle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere husna de charche ne bahle"/>
</div>
<div class="lyrico-lyrics-wrapper">Har passe gallan teriyan haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har passe gallan teriyan haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni har passe gallan teriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni har passe gallan teriyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Taithon taithon taithon taithon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taithon taithon taithon taithon"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunn taithon mera door jaan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunn taithon mera door jaan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee ni karda ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee ni karda ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunn taithon mera door jaan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunn taithon mera door jaan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee ni karda ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee ni karda ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hunn taithon mera door jaan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunn taithon mera door jaan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee ni karda ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee ni karda ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunn taithon mera door jaan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunn taithon mera door jaan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee ni karda ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee ni karda ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunn taithon mera door jaan nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunn taithon mera door jaan nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jee ni karda ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee ni karda ni"/>
</div>
</pre>
