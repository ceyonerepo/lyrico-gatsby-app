---
title: "silukkuvarupatti singam song lyrics"
album: "Silukkuvarupatti Singam"
artist: "Leon James"
lyricist: "V7H"
director: "Chella Ayyavu"
path: "/albums/silukkuvarupatti-singam-lyrics"
song: "Silukkuvarupatti Singam"
image: ../../images/albumart/silukkuvarupatti-singam.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JQ8LC7ufyE8"
type: "title track"
singers:
  - Vaikom Vijayalakshmi
  - Sargam Choir
  - V7H
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Heyy heyy heyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy heyy heyy heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti ee singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti ee singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti ee singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti ee singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silukkuvaru silukkuvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvaru silukkuvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvaru silukkuvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvaru silukkuvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvaru silukkuvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvaru silukkuvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvaru silukku silukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvaru silukku silukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukku silukku silukku silukkuvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukku silukku silukku silukkuvaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy heyy heyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy heyy heyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy heyy heyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy heyy heyy heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engooru hero pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engooru hero pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum kidaiyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum kidaiyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanoda veera kadhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanoda veera kadhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonna mudiyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna mudiyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichaanaa nee gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichaanaa nee gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkavum maattaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkavum maattaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhichaa nee thakkali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhichaa nee thakkali"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhikkavum maataanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhikkavum maataanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thayiriathula thaan machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayiriathula thaan machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhi puli aamaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhi puli aamaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhunga theriyum aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhunga theriyum aana"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaya theriyaathaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaya theriyaathaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Supermanu soupu vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Supermanu soupu vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suppi suppi kudipanaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suppi suppi kudipanaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Baahubaliyin ellu peran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baahubaliyin ellu peran"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru paaruuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru paaruuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I’m a super indian cop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m a super indian cop"/>
</div>
<div class="lyrico-lyrics-wrapper">Who’s used to hiphop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who’s used to hiphop"/>
</div>
<div class="lyrico-lyrics-wrapper">I don’t play with the words
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I don’t play with the words"/>
</div>
<div class="lyrico-lyrics-wrapper">I just play with swords
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I just play with swords"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sippin on some tea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sippin on some tea"/>
</div>
<div class="lyrico-lyrics-wrapper">I am having fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am having fun"/>
</div>
<div class="lyrico-lyrics-wrapper">Anyday would prefer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anyday would prefer"/>
</div>
<div class="lyrico-lyrics-wrapper">Bun to a gun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bun to a gun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wearing kaakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wearing kaakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaying with a thuppakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaying with a thuppakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Please don’t get confused
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Please don’t get confused"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m not thala or thalapathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m not thala or thalapathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Stop it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stop it"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m feeling all shy baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m feeling all shy baby"/>
</div>
<div class="lyrico-lyrics-wrapper">People say that
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="People say that"/>
</div>
<div class="lyrico-lyrics-wrapper">Evananga rendu peroda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evananga rendu peroda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavai nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chilling or cooling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilling or cooling"/>
</div>
<div class="lyrico-lyrics-wrapper">Like imma kingIn this city
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like imma kingIn this city"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m just very cute
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m just very cute"/>
</div>
<div class="lyrico-lyrics-wrapper">So these girls
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So these girls"/>
</div>
<div class="lyrico-lyrics-wrapper">Call me cutie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Call me cutie"/>
</div>
<div class="lyrico-lyrics-wrapper">But my mommy says that
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But my mommy says that"/>
</div>
<div class="lyrico-lyrics-wrapper">I am a bit of Tamil beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a bit of Tamil beauty"/>
</div>
<div class="lyrico-lyrics-wrapper">Though I’m a strict cop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Though I’m a strict cop"/>
</div>
<div class="lyrico-lyrics-wrapper">Am a little naughty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Am a little naughty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I don’t talk to thugs
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I don’t talk to thugs"/>
</div>
<div class="lyrico-lyrics-wrapper">My gun does the talking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My gun does the talking"/>
</div>
<div class="lyrico-lyrics-wrapper">If you mess with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you mess with me"/>
</div>
<div class="lyrico-lyrics-wrapper">You would be like broken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You would be like broken"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Though I’m a gangster
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Though I’m a gangster"/>
</div>
<div class="lyrico-lyrics-wrapper">Man I’m just joking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man I’m just joking"/>
</div>
<div class="lyrico-lyrics-wrapper">Take a chill pill
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take a chill pill"/>
</div>
<div class="lyrico-lyrics-wrapper">There’s no reason
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="There’s no reason"/>
</div>
<div class="lyrico-lyrics-wrapper">To be chokin’
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To be chokin’"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti ee singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti ee singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti ee singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti ee singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vambuthumbu pomattaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambuthumbu pomattaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandha vamba kuda ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha vamba kuda ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru poyi naalai vanu solvaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru poyi naalai vanu solvaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakki sattaiya pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakki sattaiya pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandhi thaatha vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandhi thaatha vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avar ippadi thaan irupaarunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avar ippadi thaan irupaarunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parungadiyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parungadiyoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvai neruppaamaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai neruppaamaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana onnum eriyaathae machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana onnum eriyaathae machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam rail thaan aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam rail thaan aama"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam engine oodathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam engine oodathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Spider manu kaima panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spider manu kaima panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ittulikku thottupaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ittulikku thottupaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jetulikku thambhi kaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jetulikku thambhi kaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru paaruu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru paaruu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti ee singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti ee singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti ee singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti ee singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silukkuvaru silukkuvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvaru silukkuvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvaru silukkuvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvaru silukkuvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvarupatti singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvarupatti singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvaru silukkuvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvaru silukkuvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukkuvaru silukku silukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukkuvaru silukku silukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukku silukku silukku silukkuvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukku silukku silukku silukkuvaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy heyy heyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy heyy heyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy heyy heyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy heyy heyy heyy"/>
</div>
</pre>
