---
title: "po indru neeyaga song lyrics"
album: "Vellaiilla Pattadhari"
artist: "Anirudh Ravichander"
lyricist: "Dhanush"
director: "Velraj"
path: "/albums/vellaiilla-pattadhari-lyrics"
song: "Po Indru Neeyaga"
image: ../../images/albumart/vellaiilla-pattadhari.jpg
date: 2014-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xSrgt6QG0nE"
type: "Love"
singers:
  - Anirudh Ravichander
  - Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Po Indru Neeyaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Indru Neeyaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Naalai Naamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Naalai Naamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Paakkamale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paakkamale "/>
</div>
<div class="lyrico-lyrics-wrapper">Onnum Pesamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum Pesamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Seraamale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Seraamale "/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Koothaaduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Koothaaduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Po Indru Neeyaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Indru Neeyaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Naalai Naamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Naalai Naamaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniyaave Irundhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaave Irundhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Veruppaagi Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruppaagi Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhathaala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhathaala "/>
</div>
<div class="lyrico-lyrics-wrapper">En Sogam Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sogam Pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumoochu Vitten 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumoochu Vitten "/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaana Moochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodaana Moochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaasam Pattu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaasam Pattu "/>
</div>
<div class="lyrico-lyrics-wrapper">Jelathosam Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jelathosam Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Medhuva Medhuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuva Medhuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pesum Pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pesum Pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugama Sugama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugama Sugama "/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kekkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kekkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Saara Kaaththu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Saara Kaaththu "/>
</div>
<div class="lyrico-lyrics-wrapper">En Pakkam Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pakkam Paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethamaaga Venaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethamaaga Venaandi"/>
</div>
 <div class="lyrico-lyrics-wrapper">Oru Saathu Saathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Saathu Saathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Po Indru Neeyaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Indru Neeyaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Naalai Naamaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Naalai Naamaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Paakkamale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paakkamale "/>
</div>
<div class="lyrico-lyrics-wrapper">Onnum Pesamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum Pesamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Seraamale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Seraamale "/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Koothaaduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Koothaaduthey"/>
</div>
</pre>
