---
title: "sha la la sha la la song lyrics"
album: "Gilli"
artist: "Vidyasagar"
lyricist: "Pa. Vijay"
director: "Dharani"
path: "/albums/gilli-lyrics"
song: "Sha la la Sha la la"
image: ../../images/albumart/gilli.jpg
date: 2004-04-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LEJLjmpYhIs"
type: "happy"
singers:
  - Sunidhi Chauhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sha La La Sha La La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sha La La Sha La La"/>
</div>
<div class="lyrico-lyrics-wrapper">Rettai Vaal Vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rettai Vaal Vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Pol Chuttippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Pol Chuttippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Bhoomiyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Bhoomiyilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Se Se Se Sevvandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Se Se Se Sevvandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thozhi Saamandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thozhi Saamandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetrikku Eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetrikku Eppothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaane Mundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaane Mundhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottum Aruvi Vi Vi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum Aruvi Vi Vi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thazhuvi Vi Vi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thazhuvi Vi Vi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Kolla Aasaikalvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Kolla Aasaikalvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Varuvaanoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Varuvaanoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sha La La Sha La La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sha La La Sha La La"/>
</div>
<div class="lyrico-lyrics-wrapper">Rettai Vaal Vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rettai Vaal Vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Pol Chuttippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Pol Chuttippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Bhoomiyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Bhoomiyilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marangale Marangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marangale Marangale"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai Kaalil Iruppathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai Kaalil Iruppathen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennavoo Ennavoo Thavamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavoo Ennavoo Thavamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadhigale Nadhigale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhigale Nadhigale"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththam Pottu Thaan Nadappathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththam Pottu Thaan Nadappathen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalgalin Viralgale Kolusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgalin Viralgale Kolusaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhaarathi Pola Thalaipaagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaarathi Pola Thalaipaagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattiyathe Theekuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattiyathe Theekuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppillaamal Pugai Varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppillaamal Pugai Varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhisayamaana Neerveezhchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhisayamaana Neerveezhchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idayai Aati Nadayai Aati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idayai Aati Nadayai Aati"/>
</div>
<div class="lyrico-lyrics-wrapper">Odum Rayile Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum Rayile Sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatiyamaa Hey Naatiyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatiyamaa Hey Naatiyamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaai Mugam Paartha Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Mugam Paartha Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaavani Potta Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavani Potta Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Marakkumaa Marakkumaa Nenje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marakkumaa Marakkumaa Nenje"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaithuli Rasithathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaithuli Rasithathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pani Thuli Rusithathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pani Thuli Rusithathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyuma Karaiyuma Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyuma Karaiyuma Kannil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hyder Kaala Veeranthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hyder Kaala Veeranthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthirai Yeri Varuvaanoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthirai Yeri Varuvaanoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaval Thaandi Ennai Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaval Thaandi Ennai Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadathi Kondu Povaanoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadathi Kondu Povaanoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukkul Mudhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkul Mudhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukkul Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkul Varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Semikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Semikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaravanoo Yaaravanoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaravanoo Yaaravanoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sha La La Sha La La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sha La La Sha La La"/>
</div>
<div class="lyrico-lyrics-wrapper">Rettai Vaal Vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rettai Vaal Vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Pol Chuttippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Pol Chuttippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Bhoomiyilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Bhoomiyilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottum Aruvi Vi Vi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottum Aruvi Vi Vi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thazhuvi Vi Vi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thazhuvi Vi Vi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli Kolla Aasaikalvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Kolla Aasaikalvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Varuvaanoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Varuvaanoo"/>
</div>
</pre>
