---
title: "Nalona song lyrics"
album: "Manmadhudu 2"
artist: "Chaitan Bharadwaj"
lyricist: "Subham Viswanadh"
director: "Rahul Ravindran"
path: "/albums/manmadhudu-2-lyrics"
song: "Nalona"
image: ../../images/albumart/manmadhudu-2.jpg
date: 2019-08-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/MPvV5ECvVBg"
type: "love"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naalona nee venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalona nee venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Premai nedu poochenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premai nedu poochenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pedhavullo paatai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pedhavullo paatai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegise keratamavuthundhi hrudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegise keratamavuthundhi hrudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedalo ninnu chesindhi padilam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedalo ninnu chesindhi padilam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne chusthu karigindhi le kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne chusthu karigindhi le kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagale vennelaindhi bhuvanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagale vennelaindhi bhuvanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalale marichi chusthodhi nayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalale marichi chusthodhi nayanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo kalisi ranandhi naa praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo kalisi ranandhi naa praanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prema Ee vintha kotthagundhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Ee vintha kotthagundhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo nee jathalo ee velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo nee jathalo ee velaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Ee haayi ponguthundhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Ee haayi ponguthundhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo naa jathakai ravela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo naa jathakai ravela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvveleni hrudayala pralayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvveleni hrudayala pralayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho rani janminka narakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho rani janminka narakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekai adugulesthondhi naa paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekai adugulesthondhi naa paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruvam urakelesthunna tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvam urakelesthunna tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaha cheppalenantha madhuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha cheppalenantha madhuram"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasaa chesi povaddhule gaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasaa chesi povaddhule gaayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelambarame vaale nee kallalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelambarame vaale nee kallalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarale ninnu varninchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarale ninnu varninchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunkuma puvvai virise edha nandhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunkuma puvvai virise edha nandhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennelai nuvvu varshinchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennelai nuvvu varshinchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Parimalavadha ninu thaake gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parimalavadha ninu thaake gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhalaya vinavaa swaase neevai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalaya vinavaa swaase neevai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagadapu kaluva ninu chere dhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagadapu kaluva ninu chere dhaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhamula kerukaa thodai rava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhamula kerukaa thodai rava"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalona nee venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalona nee venaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogasulu paarijaathamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogasulu paarijaathamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pilupulu suprabhathamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pilupulu suprabhathamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vainam chusthe ruthuvulalo nayagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vainam chusthe ruthuvulalo nayagaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Rupam chusthe madhuvula jalapaatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rupam chusthe madhuvula jalapaatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Na manasika raase neekai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na manasika raase neekai"/>
</div>
<div class="lyrico-lyrics-wrapper">Valapula mrudhu kavyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valapula mrudhu kavyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo mounam palikenu hindholam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo mounam palikenu hindholam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kalayika pondhe vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kalayika pondhe vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaladhu ika kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaladhu ika kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalona nee venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalona nee venaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yegise keratamavuthundhi hrudhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegise keratamavuthundhi hrudhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedalo ninnu chesindhi padilam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedalo ninnu chesindhi padilam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne chusthu karigindhi le kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne chusthu karigindhi le kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagale vennelaindhi bhuvanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagale vennelaindhi bhuvanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalale marichi chusthodhi nayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalale marichi chusthodhi nayanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo kalisi ranandhi naa praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo kalisi ranandhi naa praanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalona nee venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalona nee venaa"/>
</div>
</pre>
