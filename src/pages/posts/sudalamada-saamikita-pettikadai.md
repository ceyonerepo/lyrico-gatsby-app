---
title: "sudalamada saamikitta song lyrics"
album: "Pettikadai"
artist: "Mariya Manohar"
lyricist: "Na. Muthukumar"
director: "Esakki Karvannan"
path: "/albums/pettikadai-lyrics"
song: "Sudalamada Saamikitta"
image: ../../images/albumart/pettikadai.jpg
date: 2019-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/29GVqjeNbHg"
type: "love"
singers:
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sudala mada sami kitta Sudam ethi vendi kitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudala mada sami kitta Sudam ethi vendi kitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenthikkitta karanathe unak sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenthikkitta karanathe unak sollavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurapattu vanki enakk thaali kattava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurapattu vanki enakk thaali kattava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathiyama orakkamille Soruthanni erankaville
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiyama orakkamille Soruthanni erankaville"/>
</div>
<div class="lyrico-lyrics-wrapper">En manassa thirudiputte Thiruppi kodukka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manassa thirudiputte Thiruppi kodukka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En usura uyilu ezhidhi Onak kodukkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En usura uyilu ezhidhi Onak kodukkava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neth vacha rosa chedi Pooth nikkuth munnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neth vacha rosa chedi Pooth nikkuth munnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathirikka raasa neeyum Mala kattu kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathirikka raasa neeyum Mala kattu kannala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kolussu satham kettu Enna anaikka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kolussu satham kettu Enna anaikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekka chakka mutham koduth Katti pudikkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekka chakka mutham koduth Katti pudikkavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudala mada sami kitta Sudam ethi vendi kitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudala mada sami kitta Sudam ethi vendi kitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenthikkitta karanathe unak sollavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenthikkitta karanathe unak sollavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurapattu vanki enakk thaali kattava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurapattu vanki enakk thaali kattava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madiyila thaan saanjikittu Kadha pesa aasappatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyila thaan saanjikittu Kadha pesa aasappatten"/>
</div>
<div class="lyrico-lyrics-wrapper">En masakkaik nee Maanthoppae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En masakkaik nee Maanthoppae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaankithara aasappatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaankithara aasappatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En thaavaniyil thoolikatti Thaalatta aasappatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thaavaniyil thoolikatti Thaalatta aasappatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">On meesa kuthi melodhattil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On meesa kuthi melodhattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam vara aasappatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam vara aasappatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">On pathu viral patta idam Padharippoka aasappatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On pathu viral patta idam Padharippoka aasappatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham madam naanam ellam Sedharippoka aasappatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham madam naanam ellam Sedharippoka aasappatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suthi suthi on mohanthaan Varuthe maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi suthi on mohanthaan Varuthe maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna nambi poothirikken Naanthaan aamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna nambi poothirikken Naanthaan aamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thenedukka thaamadha Vaa vaa maamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenedukka thaamadha Vaa vaa maamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenilavu nelaviladhan Aama aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenilavu nelaviladhan Aama aama"/>
</div>
</pre>
