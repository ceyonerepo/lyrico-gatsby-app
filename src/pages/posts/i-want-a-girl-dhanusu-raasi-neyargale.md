---
title: "i want a girl song lyrics"
album: "Dhanusu Raasi Neyargale"
artist: "Ghibran"
lyricist: "Ku. Karthik"
director: "Sanjay Bharathi"
path: "/albums/dhanusu-raasi-neyargale-lyrics"
song: "I Want A Girl"
image: ../../images/albumart/dhanusu-raasi-neyargale.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TAI-gcH3-Fw"
type: "happy"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aganakanaka Asathura Azhagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aganakanaka Asathura Azhagula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izhukkura Parakkura Padminiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkura Parakkura Padminiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakalakalaka Thuru Thuru Pechula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakalakalaka Thuru Thuru Pechula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sinungura Kulungura Sanginiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinungura Kulungura Sanginiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Damadamanaka Adikkira Koothula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damadamanaka Adikkira Koothula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alappara Kudukkara Hasthiniyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alappara Kudukkara Hasthiniyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakathakanaka Jolikkura Colorula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakathakanaka Jolikkura Colorula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alukkura Kulukkura Sithriniyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alukkura Kulukkura Sithriniyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkena Oru Angel Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena Oru Angel Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedaikkaravara Searching Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaikkaravara Searching Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adampudikkuthu Adimanam Thudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adampudikkuthu Adimanam Thudikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaru Yaru Enna Peru Theriyalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaru Yaru Enna Peru Theriyalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikkuthu Ulla Turbo Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkuthu Ulla Turbo Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudikkanum Oru Virgo Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikkanum Oru Virgo Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padapadakkuthu Nenjukulla Vedikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padapadakkuthu Nenjukulla Vedikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha Ooru Ithuvara Paakkalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Ooru Ithuvara Paakkalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I want a Girl for Marriage u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I want a Girl for Marriage u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava illaama En Lifefu Romba Damage u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava illaama En Lifefu Romba Damage u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I want a Girl for Marriage u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I want a Girl for Marriage u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava illaama En Lifefu Romba Damage u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava illaama En Lifefu Romba Damage u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponga Boss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponga Boss"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Guruve Uchchathukkuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Guruve Uchchathukkuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Iruntha Kanni Thedi Varuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Iruntha Kanni Thedi Varuvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Varavum Neram Kaalam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Varavum Neram Kaalam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanavu Pootta Theranthu Vidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanavu Pootta Theranthu Vidavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyaiyo Raasi Kattam Enna Suththuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaiyo Raasi Kattam Enna Suththuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natchathiratha Thedi Alaiya Vakkithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natchathiratha Thedi Alaiya Vakkithey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aganakanaka Asathura Azhagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aganakanaka Asathura Azhagula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izhukkura Parakkura Padminiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkura Parakkura Padminiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sakalakalaka Thuru Thuru Pechula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakalakalaka Thuru Thuru Pechula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sinungura Kulungura Sanginiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinungura Kulungura Sanginiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Damadamanaka Adikkira Koothula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damadamanaka Adikkira Koothula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alappara Kudukkara Hasthiniyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alappara Kudukkara Hasthiniyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakathakanaka Jolikkura Colorula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakathakanaka Jolikkura Colorula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alukkura Kulukkura Sithriniyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alukkura Kulukkura Sithriniyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkena Oru Angel Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena Oru Angel Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedaikkaravara Searching Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedaikkaravara Searching Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adampudikkuthu Adimanam Thudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adampudikkuthu Adimanam Thudikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaru Yaru Enna Peru Theriyalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaru Yaru Enna Peru Theriyalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikkuthu Ulla Turbo Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkuthu Ulla Turbo Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudikkanum Oru Virgo Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikkanum Oru Virgo Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padapadakkuthu Nenjukulla Vedikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padapadakkuthu Nenjukulla Vedikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha Ooru Ithuvara Paakkalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Ooru Ithuvara Paakkalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I want a Girl for Marriage u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I want a Girl for Marriage u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava illaama En Lifefu Romba Damage u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava illaama En Lifefu Romba Damage u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I want a Girl for Marriage u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I want a Girl for Marriage u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava illaama En Lifefu Romba Damage u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava illaama En Lifefu Romba Damage u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I want a Girl for Marriage u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I want a Girl for Marriage u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava illaama En Lifefu Romba Damage u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava illaama En Lifefu Romba Damage u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I want a Girl for Marriage u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I want a Girl for Marriage u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava illaama En Lifefu Romba Damage u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava illaama En Lifefu Romba Damage u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Ponga Boss
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ponga Boss"/>
</div>
</pre>
