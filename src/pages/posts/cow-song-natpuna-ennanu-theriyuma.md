---
title: "cow song song lyrics"
album: "Natpuna Ennanu Theriyuma"
artist: "Dharan"
lyricist: "Mirchi Vijay"
director: "Shiva Arvind"
path: "/albums/natpuna-ennanu-theriyuma-lyrics"
song: "Cow Song"
image: ../../images/albumart/natpuna-ennanu-theriyuma.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4RxCAGjXUBM"
type: "friendship"
singers:
  - Simbu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tharathalanu Nenacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharathalanu Nenacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullaigala Parthiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullaigala Parthiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Urupudumanu Ketta Pillainge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urupudumanu Ketta Pillainge"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pannuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pannuthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharathalanu Nenacha Pullainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharathalanu Nenacha Pullainga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala Thukkuthu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Thukkuthu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Urupudumanu Ketta Pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urupudumanu Ketta Pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oore Parkutha Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oore Parkutha Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sontha Bantham Onna Serntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha Bantham Onna Serntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Villangatha Vaari Vekkithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Villangatha Vaari Vekkithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpu Onnu Than Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpu Onnu Than Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Mela Thukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Mela Thukkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gappula Aapu Vekkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gappula Aapu Vekkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Appana Neeyum Parthiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appana Neeyum Parthiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenda Ennai Petha-nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda Ennai Petha-nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattaiya Pudichi Kettiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattaiya Pudichi Kettiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aala Vetta Vanthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Vetta Vanthavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Ribbonna Than Vetturan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Ribbonna Than Vetturan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jodi Pura Onna Sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jodi Pura Onna Sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaliya Than Katturan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaliya Than Katturan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadu Paal Kudukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadu Paal Kudukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure Miss Call Kudukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure Miss Call Kudukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasanga Paasathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga Paasathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal Payasam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal Payasam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama Karan Ponnu Kudupan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Karan Ponnu Kudupan"/>
</div>
<div class="lyrico-lyrics-wrapper">Appan Karan Uyira Eduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan Karan Uyira Eduppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpu Than Eppothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpu Than Eppothume"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Kudukum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Kudukum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadu Paal Kudukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadu Paal Kudukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure Miss Call Kudukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure Miss Call Kudukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasanga Paasathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga Paasathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal Payasam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal Payasam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama Karan Ponnu Kudupan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Karan Ponnu Kudupan"/>
</div>
<div class="lyrico-lyrics-wrapper">Appan Karan Uyira Eduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan Karan Uyira Eduppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpu Than Eppothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpu Than Eppothume"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Kudukum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Kudukum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpuna Ennanu Theriyumma Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpuna Ennanu Theriyumma Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanban Na Ennanu Theriyuma Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban Na Ennanu Theriyuma Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Suriya Da Deva Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suriya Da Deva Da"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kuda Padicha Raj Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kuda Padicha Raj Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani Nelson, Jai Satish Yuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mani Nelson, Jai Satish Yuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ram Gandhi Kumar Krishna Vel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Gandhi Kumar Krishna Vel"/>
</div>
<div class="lyrico-lyrics-wrapper">Correct Ah Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Correct Ah Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Ennai Vuttiye Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ennai Vuttiye Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thilagakka Neeghalum Vangho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thilagakka Neeghalum Vangho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpuna Ennanu Theriyumma Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpuna Ennanu Theriyumma Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatttt Sago
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatttt Sago"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sowkarpettai Paan Beeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sowkarpettai Paan Beeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Meals Saptu Pottu Koda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Meals Saptu Pottu Koda"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-ula Than Oru Problem Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-ula Than Oru Problem Vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Eppothume Phone A Friendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Eppothume Phone A Friendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Option Undu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Option Undu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singa Kutty Moonuum Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singa Kutty Moonuum Ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Silakaati Scene Nu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silakaati Scene Nu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Area-la Mathavan Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Area-la Mathavan Kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Complete Ta Kaatu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Complete Ta Kaatu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chessula Kudhurai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chessula Kudhurai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasanga Thavi Oduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga Thavi Oduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Opponent Ellarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Opponent Ellarume"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhunga Edam Theduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhunga Edam Theduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karnan Color Xerox Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karnan Color Xerox Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutthi Mutthi Parthiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutthi Mutthi Parthiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Pa Friendu Treat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Pa Friendu Treat"/>
</div>
<div class="lyrico-lyrics-wrapper">Open Na Nee Kettiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Open Na Nee Kettiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Mattum Kaati Putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Mattum Kaati Putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nathar Muthar Adikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nathar Muthar Adikiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenda Machan Neeyum Ah Ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda Machan Neeyum Ah Ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudutha Kaasukku Mela Nadikiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudutha Kaasukku Mela Nadikiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadu Paal Kudukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadu Paal Kudukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure Miss Call Kudukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure Miss Call Kudukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasanga Paasathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga Paasathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal Payasam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal Payasam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama Karan Ponnu Kudupan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Karan Ponnu Kudupan"/>
</div>
<div class="lyrico-lyrics-wrapper">Appan Karan Uyira Eduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan Karan Uyira Eduppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpu Than Eppothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpu Than Eppothume"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Kudukum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Kudukum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadu Paal Kudukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadu Paal Kudukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure Miss Call Kudukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure Miss Call Kudukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasanga Paasathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga Paasathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal Payasam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal Payasam Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama Karan Ponnu Kudupan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Karan Ponnu Kudupan"/>
</div>
<div class="lyrico-lyrics-wrapper">Appan Karan Uyira Eduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appan Karan Uyira Eduppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpu Than Eppothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpu Than Eppothume"/>
</div>
<div class="lyrico-lyrics-wrapper">Thol Kudukum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thol Kudukum Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Matha Pitha Guru Deivam-nu Sonanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matha Pitha Guru Deivam-nu Sonanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaranam Thoda Than Nanbana Vittanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaranam Thoda Than Nanbana Vittanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuppamthu Villain-nu Solluvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuppamthu Villain-nu Solluvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaan Thooki Nirutha Nenaikka Veippa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaan Thooki Nirutha Nenaikka Veippa Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katthu Kudukum Guru Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katthu Kudukum Guru Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuda Nirukkum Deivam Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuda Nirukkum Deivam Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Serntha Thale Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Serntha Thale Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Peru Nanban Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Peru Nanban Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpuna Ennanu Theriyumma Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpuna Ennanu Theriyumma Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum Nanban Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum Nanban Da"/>
</div>
</pre>
