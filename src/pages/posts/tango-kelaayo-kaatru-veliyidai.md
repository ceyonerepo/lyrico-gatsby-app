---
title: "tango kelaayo song lyrics"
album: "Kaatru Veliyidai"
artist: "A R Rahman"
lyricist: "Vairamuthu"
director: "Mani Ratnam"
path: "/albums/kaatru-veliyidai-lyrics"
song: "Tango Kelaayo"
image: ../../images/albumart/kaatru-veliyidai.jpg
date: 2017-04-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-6VYh3fVXlc"
type: "love"
singers:
  -	Haricharan
  - Diwakar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kelaayo kelaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelaayo kelaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sempoovae kelaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sempoovae kelaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manradum en ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manradum en ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai pirindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pirindhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pirindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pirindhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vaazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vaazhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andril paravai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andril paravai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Andril paravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andril paravai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee ennai marandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ennai marandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatru kadharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatru kadharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalin melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalin melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottagam nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottagam nadakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh nee ennai marandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh nee ennai marandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatru kadharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatru kadharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalin melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalin melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottagam nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottagam nadakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh nee ennai piriyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh nee ennai piriyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh nee ennai maravaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh nee ennai maravaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu ponaal etti pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu ponaal etti pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinmeenellaam kotti pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinmeenellaam kotti pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelaayo kelaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelaayo kelaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sempoovae kelaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sempoovae kelaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manradum en ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manradum en ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaraayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai pirindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pirindhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pirindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pirindhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vaazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vaazhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andril paravai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andril paravai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Andril paravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andril paravai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kuraigal edhu kandaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kuraigal edhu kandaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesuvadhu kaadhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesuvadhu kaadhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Penuvadhu kaamamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penuvadhu kaamamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyam enna poliyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyam enna poliyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen pennae idaiveli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen pennae idaiveli"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirindhaai pirindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirindhaai pirindhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Marandhaai marandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhaai marandhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai unnai pirindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai unnai pirindhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vaazhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vaazhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andril paravai paravai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andril paravai paravai naan"/>
</div>
</pre>
