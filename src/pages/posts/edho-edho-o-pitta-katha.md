---
title: "edho edho song lyrics"
album: "O Pitta Katha"
artist: "Pravin Lakkaraju"
lyricist: "Sreejo"
director: "Chendu Muddhu"
path: "/albums/o-pitta-katha-lyrics"
song: "Edho Edho"
image: ../../images/albumart/o-pitta-katha.jpg
date: 2020-03-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/QsUUeVTIJaM"
type: "love"
singers:
  - Sweekar Agasthi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">edho edho edho edho edho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edho edho edho edho edho "/>
</div>
<div class="lyrico-lyrics-wrapper">edho edho edho edho edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edho edho edho edho edho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edo nannu thaki nattu undi maya edo edo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edo nannu thaki nattu undi maya edo edo"/>
</div>
<div class="lyrico-lyrics-wrapper">andamaina mataki ardham edo uhalu vache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andamaina mataki ardham edo uhalu vache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">guttu bhavana edo gundelalo nindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guttu bhavana edo gundelalo nindi"/>
</div>
<div class="lyrico-lyrics-wrapper">uppena ayithe nadi nilo kadho nive edo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppena ayithe nadi nilo kadho nive edo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edo nannu thaki nattu undi maya edo edo edo edo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edo nannu thaki nattu undi maya edo edo edo edo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanula la lo merise kala edo valu jadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanula la lo merise kala edo valu jadai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">volikki kala edo pilupula chilipi chanuvu’edo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="volikki kala edo pilupula chilipi chanuvu’edo"/>
</div>
<div class="lyrico-lyrics-wrapper">cheppenedo sayankalam saradaga edo nitho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheppenedo sayankalam saradaga edo nitho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">untu teliyade kalam edo ontarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="untu teliyade kalam edo ontarai"/>
</div>
<div class="lyrico-lyrics-wrapper">nimisham inka ledo adi ado ido edo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimisham inka ledo adi ado ido edo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ara kshanam ni jada ledo ayomayam madhiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ara kshanam ni jada ledo ayomayam madhiki"/>
</div>
<div class="lyrico-lyrics-wrapper">kudhiredo telusuna kathaku malupu edo gelupu edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudhiredo telusuna kathaku malupu edo gelupu edho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oppukova yadhala madana edo chinnadaina telupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oppukova yadhala madana edo chinnadaina telupu"/>
</div>
<div class="lyrico-lyrics-wrapper">rujuvu edo niku nijam teluso teliyado chelimido
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rujuvu edo niku nijam teluso teliyado chelimido"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edo nannu thaki nattu undi maya edo edo edo edo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edo nannu thaki nattu undi maya edo edo edo edo"/>
</div>
</pre>
