---
title: "taan addi song lyrics"
album: "Ayngaran"
artist: "G.V. Prakash Kumar"
lyricist: "G. Rokesh"
director: "Ravi Arasu"
path: "/albums/ayngaran-lyrics"
song: "Taan Addi"
image: ../../images/albumart/ayngaran.jpg
date: 2022-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tpyicHP5J6Y"
type: "happy"
singers:
  - Rajaganapathy
  - Anthony Daasan
  - V.M. Mahalingam
  - Sonu Nigam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Heyy Heyy Heyy Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Heyy Heyy Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy Heyy Heyy Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Heyy Heyy Heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Taan Addi Kirkaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taan Addi Kirkaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Urtaladi Portaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urtaladi Portaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Taan Addi Kirkaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taan Addi Kirkaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Urtaladi Portaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urtaladi Portaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Attakathi Aalentta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attakathi Aalentta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vazha Vena Violent-Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vazha Vena Violent-Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Erangi Yeru Silent-Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangi Yeru Silent-Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Machaan Getha Kaattu Talent-Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machaan Getha Kaattu Talent-Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattam Thattravana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattam Thattravana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandukkaama Vaada Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukkaama Vaada Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Paper-Ah Rocketaakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paper-Ah Rocketaakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Oru Vinyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Oru Vinyani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirusa Nanba Pudhusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirusa Nanba Pudhusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thiramaiya Kaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thiramaiya Kaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru Varum Perusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru Varum Perusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethu Machaan Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethu Machaan Paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nenachatha Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nenachatha Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagatha Maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagatha Maathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maappu Un Yosanaiyum Toppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maappu Un Yosanaiyum Toppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maappu Nee Aruviyalu Thoppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maappu Nee Aruviyalu Thoppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maappu Nee Thotta Maarum Shape-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maappu Nee Thotta Maarum Shape-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Maappu Nee Enaikkumae Sharppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maappu Nee Enaikkumae Sharppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Heyy Heyy Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Heyy Heyy Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy Heyy Heyy Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Heyy Heyy Heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poranthu Vazhanthathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poranthu Vazhanthathellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Patti Thottiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patti Thottiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">World-Ah Maathiduven Creativity-La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="World-Ah Maathiduven Creativity-La"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela Yeri Varen Sondha Budhiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Yeri Varen Sondha Budhiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaiya Otta Maatten Kundu Sattiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiya Otta Maatten Kundu Sattiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Eppavumae Vachukkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Eppavumae Vachukkada"/>
</div>
<div class="lyrico-lyrics-wrapper">Suru Suruppa Moolaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suru Suruppa Moolaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Single-Ah Nee Senjiduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single-Ah Nee Senjiduva"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu Peru Veliaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu Peru Veliaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalathukkum Yetha Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalathukkum Yetha Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathikanum Aalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathikanum Aalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Vara Vaikkanum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vara Vaikkanum Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholukku Thaan Maalaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholukku Thaan Maalaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mersal Mechanic-Ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mersal Mechanic-Ku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinakku Dhinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinakku Dhinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Talent Techinic-Ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talent Techinic-Ku"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinakku Dhinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinakku Dhinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Science-Ah Silent-Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Science-Ah Silent-Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathukunum Machaan Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathukunum Machaan Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Taan Addi Kirkaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taan Addi Kirkaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Urtaladi Portaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urtaladi Portaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Taan Addi Kirkaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taan Addi Kirkaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Urtaladi Portaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urtaladi Portaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eettiya Paayanum Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eettiya Paayanum Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatha Kizhichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha Kizhichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Potti Potta Thaanda Nalla Valarchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti Potta Thaanda Nalla Valarchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa Irukkatha Naala Kazhichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Irukkatha Naala Kazhichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti Modhikittu Vaada Molachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutti Modhikittu Vaada Molachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thurumba Ellam Onna Sertha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thurumba Ellam Onna Sertha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoona Kooda Aakalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoona Kooda Aakalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Waste-Ah Pona Porula Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waste-Ah Pona Porula Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">World-U Record Pannalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="World-U Record Pannalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dupe Ellaam Nambi Vazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dupe Ellaam Nambi Vazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tube Light-U Naan Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tube Light-U Naan Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Challenge Panni Vittuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challenge Panni Vittuduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Satellite Vaanula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satellite Vaanula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Vachaa Done-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Vachaa Done-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinakku Dhinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinakku Dhinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenacha Ellaam Win-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenacha Ellaam Win-U"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinakku Dhinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinakku Dhinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduppen Odaama Ninnu Kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduppen Odaama Ninnu Kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru Run-Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Run-Nu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Taan Addi Kirkaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taan Addi Kirkaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Urtaladi Portaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urtaladi Portaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Taan Addi Kirkaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taan Addi Kirkaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Urtaladi Portaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urtaladi Portaladi"/>
</div>
</pre>
