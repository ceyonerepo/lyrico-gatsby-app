---
title: "neramtha vanthanthe song lyrics"
album: "Pattinapakkam"
artist: "Ishaan Dev"
lyricist: "Vel Murugan"
director: "Jayadev"
path: "/albums/pattinapakkam-lyrics"
song: "Neramtha Vanthanthe"
image: ../../images/albumart/pattinapakkam.jpg
date: 2018-11-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/77PZmFu_v5s"
type: "happy"
singers:
  - Abhay Jodhpurkar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neram thaan vanthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram thaan vanthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvaai ennoduthaan kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvaai ennoduthaan kondaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam thaan aarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam thaan aarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhuva nenjodu thaan thindaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuva nenjodu thaan thindaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalanthaan maarumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalanthaan maarumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhuva nammodu thaan panpaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhuva nammodu thaan panpaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookamum ponadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookamum ponadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum vandha naalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum vandha naalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongha venum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongha venum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha iravil pennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha iravil pennodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram thaan vanthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram thaan vanthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvaai ennoduthaan kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvaai ennoduthaan kondaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam thaan aarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam thaan aarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhuva nenjodu thaan thindaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuva nenjodu thaan thindaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalanthaan maarumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalanthaan maarumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhuva nammodu thaan panpaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhuva nammodu thaan panpaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oomaiyaai vaazhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomaiyaai vaazhgiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ulagam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ulagam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam paarkkadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam paarkkadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oonjalaai aadiththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjalaai aadiththaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookku kaiyuraai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookku kaiyuraai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Seendi paarkkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seendi paarkkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu varai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu varai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasadhi paarthathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasadhi paarthathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal murai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal murai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasadhi paarkkum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasadhi paarkkum pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvarai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvarai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Osaram paarkka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osaram paarkka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadandhu poiyiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadandhu poiyiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram thaan vanthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram thaan vanthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvaai ennoduthaan kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvaai ennoduthaan kondaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam thaan aarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam thaan aarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhuva nenjodu thaan thindaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuva nenjodu thaan thindaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram thaan vanthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram thaan vanthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvaai ennoduthaan kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvaai ennoduthaan kondaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam thaan aarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam thaan aarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhuva nenjodu thaan thindaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuva nenjodu thaan thindaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalanthaan maarumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalanthaan maarumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhuva nammodu thaan panpaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhuva nammodu thaan panpaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookamum ponadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookamum ponadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum vandha naalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum vandha naalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongha venum naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongha venum naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha iravil pennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha iravil pennodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram thaan vanthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram thaan vanthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvaai ennoduthaan kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvaai ennoduthaan kondaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam thaan aarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam thaan aarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhuva nenjodu thaan thindaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuva nenjodu thaan thindaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalanthaan maarumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalanthaan maarumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhuva nammodu thaan panpaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhuva nammodu thaan panpaadu"/>
</div>
</pre>
