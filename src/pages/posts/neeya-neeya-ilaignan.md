---
title: "neeya neeya song lyrics"
album: "Ilaignan"
artist: "Vidyasagar"
lyricist: "Pa. Vijay"
director: "Suresh Krishna"
path: "/albums/ilaignan-lyrics"
song: "Neeya Neeya"
image: ../../images/albumart/ilaignan.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KE9XXPWCfQE"
type: "motivational"
singers:
  - Tippu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neeyaa Neeyaa Neeyaethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaa Neeyaa Neeyaethaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaa Theeyaa Theeyaethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaa Theeyaa Theeyaethaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaa Neeyaa Neeyaethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaa Neeyaa Neeyaethaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutrum Sudarum Neethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutrum Sudarum Neethaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyaa Neeyaa Neeyaethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaa Neeyaa Neeyaethaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaa Theeyaa Theeyaethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaa Theeyaa Theeyaethaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaa Neeyaa Neeyaethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaa Neeyaa Neeyaethaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutrum Sudarum Neethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutrum Sudarum Neethaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyum Vidiyum Endru Mudiyu Mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyum Vidiyum Endru Mudiyu Mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Nimirum Nimirum Ilaignan Neeye Thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Nimirum Nimirum Ilaignan Neeye Thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimai Adimai Varkkam Urimai Urimai Ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimai Adimai Varkkam Urimai Urimai Ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthavum Uthavum Ilaignan Neeyethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthavum Uthavum Ilaignan Neeyethaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyaa Neeyaa Neeyaethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaa Neeyaa Neeyaethaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaa Theeyaa Theeyaethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaa Theeyaa Theeyaethaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaa Neeyaa Neeyaethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaa Neeyaa Neeyaethaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutrum Sudarum Neethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutrum Sudarum Neethaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanai Kaalam Ethanai Pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Kaalam Ethanai Pozhuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oomaigalaagi Uzhalvatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomaigalaagi Uzhalvatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaippavan Kanneer Urinjidum Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippavan Kanneer Urinjidum Ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamaai Ingu Suzhalvatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaai Ingu Suzhalvatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Aezhaigal Nenjin Elumbugal Meethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aezhaigal Nenjin Elumbugal Meethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanavakkoottam Vaazhvatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavakkoottam Vaazhvatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaraiyil Sellum Karumbu Kai Poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraiyil Sellum Karumbu Kai Poala"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaippavar Moochu Poavatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippavar Moochu Poavatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppaai Nee Neruppaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppaai Nee Neruppaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aen Pøruppaai Thadaiyaruppaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aen Pøruppaai Thadaiyaruppaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppaai Nee Neruppaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppaai Nee Neruppaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aen Pøruppaai Thadaiyaruppaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aen Pøruppaai Thadaiyaruppaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai Thøandrattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai Thøandrattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbar Kai Šerattum Naalaigal Vidiyattum Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbar Kai Šerattum Naalaigal Vidiyattum Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaa Neeyaa Neeyaethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaa Neeyaa Neeyaethaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaa Theeyaa Theeyaethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaa Theeyaa Theeyaethaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaa Neeyaa Neeyaethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaa Neeyaa Neeyaethaanaa"/>
</div>
Š<div class="lyrico-lyrics-wrapper">utrum Šudarum Neethaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="utrum Šudarum Neethaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyae Neeyae Neeyaethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae Neeyae Neeyaethaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyae Theeyae Theeyaethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyae Theeyae Theeyaethaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae Neeyae Neeyaethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae Neeyae Neeyaethaane"/>
</div>
Š<div class="lyrico-lyrics-wrapper">utrum Šudarum Neethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="utrum Šudarum Neethaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyum Vidiyum Èndru Mudiyu Mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyum Vidiyum Èndru Mudiyu Mudiyum"/>
</div>
È<div class="lyrico-lyrics-wrapper">ndru Nimirum Nimirum Ilaignan Neeye Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ndru Nimirum Nimirum Ilaignan Neeye Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimai Adimai Varkkam Urimai Urimai Ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimai Adimai Varkkam Urimai Urimai Ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthavum Uthavum Ilaignan Neeyethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthavum Uthavum Ilaignan Neeyethaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyae Neeyae Neeyaethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae Neeyae Neeyaethaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyae Theeyae Theeyaethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyae Theeyae Theeyaethaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae Neeyae Neeyaethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae Neeyae Neeyaethaane"/>
</div>
Š<div class="lyrico-lyrics-wrapper">utrum Šudarum Neethaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="utrum Šudarum Neethaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yutham Varuthu Varuthu Aayutham Kaiyil Tharuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yutham Varuthu Varuthu Aayutham Kaiyil Tharuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thegam Èzhuthu Vetkai Èzhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegam Èzhuthu Vetkai Èzhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pøarkkalam Pøagumbøzhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pøarkkalam Pøagumbøzhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Irugu Theeyai Parugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Irugu Theeyai Parugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Røuthiram Nenjil Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Røuthiram Nenjil Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham Vilagu Vidiyum Piragu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham Vilagu Vidiyum Piragu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayamae Vaazhvin Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayamae Vaazhvin Azhagu"/>
</div>
È<div class="lyrico-lyrics-wrapper">thirppaai Padai Èthirppaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirppaai Padai Èthirppaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalai Èduppaai Athil Køduppaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalai Èduppaai Athil Køduppaai"/>
</div>
È<div class="lyrico-lyrics-wrapper">thirppaai Padai Èthirppaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirppaai Padai Èthirppaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalai Èduppaai Athil Køduppaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalai Èduppaai Athil Køduppaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraiyattum Širayellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraiyattum Širayellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarattum Širakellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarattum Širakellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravattum Paravattum Thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravattum Paravattum Thee"/>
</div>
</pre>
