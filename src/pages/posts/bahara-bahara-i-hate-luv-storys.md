---
title: "bahara bahara song lyrics"
album: "I Hate Luv Storys"
artist: "Vishal Shekhar"
lyricist: "Kumaar"
director: "Punit Malhotra"
path: "/albums/i-hate-luv-storys-lyrics"
song: "Bahara Bahara"
image: ../../images/albumart/i-hate-luv-storys.jpg
date: 2010-07-02
lang: hindi
youtubeLink: "https://www.youtube.com/embed/7N74i_rAfFE"
type: "love"
singers:
  - Shreya Ghoshal
  - Sona Mohapatra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">O tora saajan aayo tore des
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O tora saajan aayo tore des"/>
</div>
<div class="lyrico-lyrics-wrapper">Badli badara, badla saawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badli badara, badla saawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Badla jag ne bhes re..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badla jag ne bhes re.."/>
</div>
<div class="lyrico-lyrics-wrapper">Tora saajan aayo tore des..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tora saajan aayo tore des.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soi soi palkon pe chal ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soi soi palkon pe chal ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere sapnon ki khidki pe aa gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere sapnon ki khidki pe aa gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aate-jaate phir mere dil ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aate-jaate phir mere dil ke"/>
</div>
<div class="lyrico-lyrics-wrapper">In hathon mein woh khat pakda gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In hathon mein woh khat pakda gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar ka.. lafzon mein rang hai pyaar ka..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar ka.. lafzon mein rang hai pyaar ka.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bahara bahara hua dil pehli baar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahara bahara hua dil pehli baar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahara bahara ke chain to hua faraar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahara bahara ke chain to hua faraar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahara bahara hua dil pehli-pehli baar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahara bahara hua dil pehli-pehli baar ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O tora saajan aayo tore des
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O tora saajan aayo tore des"/>
</div>
<div class="lyrico-lyrics-wrapper">Badli badara badla saawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badli badara badla saawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Badla jag ne bhes re..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badla jag ne bhes re.."/>
</div>
<div class="lyrico-lyrics-wrapper">Tora saajan aayo tore des..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tora saajan aayo tore des.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Woh... kabhi dikhe jameen pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh... kabhi dikhe jameen pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabhi woh chaand pe..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi woh chaand pe.."/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh... nazar kahe use yahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh... nazar kahe use yahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Main rakh loon bandh ke ek saans mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main rakh loon bandh ke ek saans mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadkanon ke paas mein, haan paas mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhadkanon ke paas mein, haan paas mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghar banaayein haaye bhoolein yeh jahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghar banaayein haaye bhoolein yeh jahan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bahara bahara hua dil pehli baar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahara bahara hua dil pehli baar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahara bahara ke chain to hua faraar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahara bahara ke chain to hua faraar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahara bahara hua dil pehli-pehli baar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahara bahara hua dil pehli-pehli baar ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pritam tore o re saawariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pritam tore o re saawariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Payal jaise chhanke bijuriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payal jaise chhanke bijuriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Chham chham naache tan pe badariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chham chham naache tan pe badariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho ho.. o.. ho.. o..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ho.. o.. ho.. o.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baadaliya tu barse ghana, barse ghana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadaliya tu barse ghana, barse ghana"/>
</div>
<div class="lyrico-lyrics-wrapper">Barse ghana, barse ghana, barse ghana..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barse ghana, barse ghana, barse ghana.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jo... yeh badliyan woh chhed de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo... yeh badliyan woh chhed de"/>
</div>
<div class="lyrico-lyrics-wrapper">To chhalke baarishein..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To chhalke baarishein.."/>
</div>
<div class="lyrico-lyrics-wrapper">Woh... de aahatein kareeb se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh... de aahatein kareeb se"/>
</div>
<div class="lyrico-lyrics-wrapper">To bole khwaahishein ke aaj-kal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To bole khwaahishein ke aaj-kal"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagi har ek pal, har ek pal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi har ek pal, har ek pal"/>
</div>
<div class="lyrico-lyrics-wrapper">Se chaahe haaye jiska dil hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Se chaahe haaye jiska dil hua"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bahara bahara hua dil pehli baar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahara bahara hua dil pehli baar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahara bahara ke chain to hua faraar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahara bahara ke chain to hua faraar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahara bahara hua dil pehli-pehli baar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahara bahara hua dil pehli-pehli baar ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soi soi palkon pe chal ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soi soi palkon pe chal ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere sapnon ki khidki pe aa gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere sapnon ki khidki pe aa gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aate-jaate phir mere dil ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aate-jaate phir mere dil ke"/>
</div>
<div class="lyrico-lyrics-wrapper">In hathon mein woh khat pakda gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In hathon mein woh khat pakda gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar ka.. lafzon mein rang hai pyaar ka..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar ka.. lafzon mein rang hai pyaar ka.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bahara bahara hua dil pehli baar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahara bahara hua dil pehli baar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahara bahara ke chain to hua faraar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahara bahara ke chain to hua faraar ve"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahara bahara hua dil pehli-pehli baar ve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahara bahara hua dil pehli-pehli baar ve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O tora saajan aayo tore des
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O tora saajan aayo tore des"/>
</div>
<div class="lyrico-lyrics-wrapper">Badli badara, badla saawan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badli badara, badla saawan"/>
</div>
<div class="lyrico-lyrics-wrapper">Badla jag ne bhes re..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badla jag ne bhes re.."/>
</div>
<div class="lyrico-lyrics-wrapper">Tora saajan aayo tore des
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tora saajan aayo tore des"/>
</div>
</pre>
