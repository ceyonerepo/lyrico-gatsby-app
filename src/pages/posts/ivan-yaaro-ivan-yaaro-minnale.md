---
title: "ivan yaaro ivan yaaro song lyrics"
album: "Minnale"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Gautham Menon"
path: "/albums/minnale-lyrics"
song: "Ivan Yaaro Ivan Yaaro"
image: ../../images/albumart/minnale.jpg
date: 2001-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qt3Fxnq1h0c"
type: "love"
singers:
  - Unni Krishnan
  - Harini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Verenna Verenna Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna Verenna Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Murai Sonnaal Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Murai Sonnaal Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavaiyum Undhan Kaal Midhiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavaiyum Undhan Kaal Midhiyaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaippene Vaippene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaippene Vaippene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollavum Kooda Vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollavum Kooda Vendam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannimaithaale Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannimaithaale Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelvigal Indri Uyiraiyum Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvigal Indri Uyiraiyum Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharuvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Mounam Mounam Mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Mounam Mounam Mounam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mounam Yen Mounam Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam Yen Mounam Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verenna Vendum Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna Vendum Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seigiren Seigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seigiren Seigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Yaro Ivan Yaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaro Ivan Yaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthathu Yetharkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathu Yetharkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikindraan Rasikkindraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikindraan Rasikkindraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakke Enakkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakke Enakkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaachu Enakke Theriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaachu Enakke Theriyavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Moochin Kaaichal Kuraiyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moochin Kaaichal Kuraiyavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Enna Idhu Enna Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Enna Idhu Enna Idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Maatti Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Maatti Konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Pidikkiratha Pidikkalaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pidikkiratha Pidikkalaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaridam Kettu Solven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaridam Kettu Solven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Yaro Ivan Yaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaro Ivan Yaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthathu Yetharkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathu Yetharkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikindraan Rasikkindraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikindraan Rasikkindraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakke Enakkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakke Enakkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaachu Enakke Theriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaachu Enakke Theriyavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Moochin Kaaichal Kuraiyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moochin Kaaichal Kuraiyavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Enna Idhu Enna Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Enna Idhu Enna Idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Maatti Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Maatti Konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Pidikkiratha Pidikkalaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pidikkiratha Pidikkalaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaridam Kettu Solven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaridam Kettu Solven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottathil Ulla Thottathil Ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottathil Ulla Thottathil Ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pookkal Ellaame Vanna Pookkal Ellaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkal Ellaame Vanna Pookkal Ellaame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaiyai Thiruppi Paarkum Aanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyai Thiruppi Paarkum Aanaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhaithathu Unnaithaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaithathu Unnaithaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naano Azhaithathu Unnaithaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naano Azhaithathu Unnaithaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenje Nenje Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenje Nenje Unnai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulle Vaithathu Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulle Vaithathu Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Varum Paadhai Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Varum Paadhai Engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Iru Ullangai Thaangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Iru Ullangai Thaangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Yaro Ivan Yaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaro Ivan Yaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthathu Yetharkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathu Yetharkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikindraan Rasikkindraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikindraan Rasikkindraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakke Enakkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakke Enakkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaachu Enakke Theriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaachu Enakke Theriyavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Moochin Kaaichal Kuraiyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moochin Kaaichal Kuraiyavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Enna Idhu Enna Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Enna Idhu Enna Idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippadi Maatti Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Maatti Konden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Pidikkiratha Pidikkalaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pidikkiratha Pidikkalaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaridam Kettu Solven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaridam Kettu Solven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalgalin Koluse Kaalgalin Koluse
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalgalin Koluse Kaalgalin Koluse"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kobam Varugirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobam Varugirathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmel Kobam Varugirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmel Kobam Varugirathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Antha Idathil Sinungida Thudithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Antha Idathil Sinungida Thudithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vanthu Keduthaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vanthu Keduthaaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavi Nee Vanthu Keduthaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavi Nee Vanthu Keduthaaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno Yeno Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno Yeno Ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarkka Seidhaai Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkka Seidhaai Unnai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Unnai Kaanaaththaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unnai Kaanaaththaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yugamthorum Kaathu Kidanthenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yugamthorum Kaathu Kidanthenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Yaro Ivan Yaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaro Ivan Yaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthathu Yetharkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathu Yetharkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikindraan Rasikkindraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikindraan Rasikkindraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanthaane Naanthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanthaane Naanthaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthen Unakkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthen Unakkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirkindren Rasikindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirkindren Rasikindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakke Unakkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakke Unakkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaachu Enakke Theriyalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaachu Enakke Theriyalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Moochin Kaaichal Kuraiyalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Moochin Kaaichal Kuraiyalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Enna Idhu Enna Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Enna Idhu Enna Idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennidam Pesividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidam Pesividu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Pidichirukka Pidikkalaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Pidichirukka Pidikkalaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Murai Sollividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Murai Sollividu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Oru Murai Sollividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Oru Murai Sollividu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Oru Murai Sollividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Oru Murai Sollividu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Oru Murai Sollividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Oru Murai Sollividu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollividu Sollividu Sollividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollividu Sollividu Sollividu"/>
</div>
</pre>
