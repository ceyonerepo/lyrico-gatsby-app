---
title: "sarileru neekevvaru anthem song lyrics"
album: "Sarileru Neekevvaru"
artist: "Devi Sri Prasad"
lyricist: "Devi Sri Prasad"
director: "Anil Ravipudi"
path: "/albums/sarileru-neekevvaru-lyrics"
song: "Sarileru Neekevvaru Anthem"
image: ../../images/albumart/sarileru-neekevvaru.jpg
date: 2020-01-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/12TBkQvBQ8s"
type: "anthem"
singers:
  - Shankar Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bhaga bhaga bhaga bhaga mande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaga bhaga bhaga bhaga mande"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippul varshamochchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippul varshamochchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Jana gana mana antune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana gana mana antune"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhuke waade sainikudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuke waade sainikudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phela phela phela phelamantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phela phela phela phelamantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Machchu tufaanu vacchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machchu tufaanu vacchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Venakaduge ledhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venakaduge ledhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaate waade sainikudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaate waade sainikudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhada dhada dhada dhadamantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhada dhada dhada dhadamantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thutaale dhusukocchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thutaale dhusukocchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana gundenu addu petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana gundenu addu petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Aape waade sainikudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aape waade sainikudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaranayudhalu yennedhuraina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaranayudhalu yennedhuraina"/>
</div>
<div class="lyrico-lyrics-wrapper">Prananni yedhuru pampevadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prananni yedhuru pampevadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okade okadu vaade sainikudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okade okadu vaade sainikudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarileru neekevvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarileru neekevvaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvelle rahadhaariki johaaru ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvelle rahadhaariki johaaru ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarileru neekevvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarileru neekevvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yanaleni tyaganiki nuvve maaru peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yanaleni tyaganiki nuvve maaru peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarileru neekevvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarileru neekevvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvelle rahadhaariki johaaru ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvelle rahadhaariki johaaru ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarileru neekevvaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarileru neekevvaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yanaleni tyaganiki nuvve maaru peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yanaleni tyaganiki nuvve maaru peru"/>
</div>
</pre>
