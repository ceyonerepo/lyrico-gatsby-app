---
title: "apdi paakadhadi song lyrics"
album: "Jada"
artist: "Sam CS"
lyricist: "Sam CS"
director: "Jada Kumaran"
path: "/albums/jada-lyrics"
song: "Apdi Paakadhadi"
image: ../../images/albumart/jada.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/azdRRrFVsQw"
type: "love"
singers:
  - Anirudh Ravichander
  - Swagatha S. Krishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Appdi Paakadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appdi Paakadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedipennae Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedipennae Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Paakaamalae Thudipeanaehae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paakaamalae Thudipeanaehae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Yen Kannu Munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Yen Kannu Munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Vandhavandha Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Vandhavandha Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaeno Sukkunooraaki Enna Konna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaeno Sukkunooraaki Enna Konna"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Konna Enna Konna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Konna Enna Konna"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae Murai Pazhaga Thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Murai Pazhaga Thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Zen Nilai Adha Kadaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zen Nilai Adha Kadaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Yen Kannu Munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Yen Kannu Munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaeno Sukkunooraaki Enna Konna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaeno Sukkunooraaki Enna Konna"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae Murai Pazhaga Thudikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Murai Pazhaga Thudikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Zen Nilai Adha Kadaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zen Nilai Adha Kadaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appdi Paakadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appdi Paakadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedipennae Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedipennae Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakaamalae Thudipeanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakaamalae Thudipeanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appidi Paakadhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appidi Paakadhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedippennae Unnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedippennae Unnal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakamalae Thavippenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakamalae Thavippenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appdi Paakadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appdi Paakadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedipennae Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedipennae Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakaamalae Thudipeanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakaamalae Thudipeanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakathadi Enna Neeyum Kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakathadi Enna Neeyum Kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedippen Thudippen Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedippen Thudippen Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Azhaikkirena Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Azhaikkirena Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Thodargiraai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thodargiraai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan Vara Yen Uraigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan Vara Yen Uraigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Niraindhidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Niraindhidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yee Azhagiyae Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yee Azhagiyae Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkaamal Sikkiten Unkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkaamal Sikkiten Unkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththangal Veikkathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththangal Veikkathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkam Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkam Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Azhaikkirena Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Azhaikkirena Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkaamal Sikkiten Unkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkaamal Sikkiten Unkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththangal Veikkathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththangal Veikkathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkam Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkam Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appidi Paakadhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appidi Paakadhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedippennae Unnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedippennae Unnal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakamalae Thavippenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakamalae Thavippenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Appdi Paakadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appdi Paakadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedipennae Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedipennae Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakaamalae Thudipeanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakaamalae Thudipeanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appdi Paakadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appdi Paakadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedipennae Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedipennae Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakaamalum Thudipeanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakaamalum Thudipeanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudipeanae Thudipeanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudipeanae Thudipeanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Paakamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paakamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vedippenae Thudippennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vedippenae Thudippennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedippenae Thudippennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedippenae Thudippennae"/>
</div>
</pre>
