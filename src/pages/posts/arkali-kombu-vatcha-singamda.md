---
title: "arkali song lyrics"
album: "Kombu Vatcha Singamda"
artist: "Dhibu Ninan Thomas"
lyricist: "GKB"
director: "S.R. Prabhakaran"
path: "/albums/kombu-vatcha-singamda-lyrics"
song: "Arkali"
image: ../../images/albumart/kombu-vatcha-singamda.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Wqvbm66yX1A"
type: "love"
singers:
  - Sathya Prakash
  - Ala B Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aarkali Aarkali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarkali Aarkali"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarumo Kannimaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarumo Kannimaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhiyin Aazhamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhiyin Aazhamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalin Uyirthaezhudhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalin Uyirthaezhudhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee En Perum Velvigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee En Perum Velvigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Pozhivai Uyirum Nanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Pozhivai Uyirum Nanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamo Sila Maatrangal Nigazhthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamo Sila Maatrangal Nigazhthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkai Uraiyum Nimidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkai Uraiyum Nimidam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poongkatrae Poongkatrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongkatrae Poongkatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pularugirai Pudhumalarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pularugirai Pudhumalarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Melliravil Sudum Panithuliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melliravil Sudum Panithuliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiridaiyil Nee Urulugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiridaiyil Nee Urulugiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalilaiyil Pudhu Thoranagal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalilaiyil Pudhu Thoranagal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Pudhirgal Purigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Pudhirgal Purigiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaridamum Naan Thootrathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaridamum Naan Thootrathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Unnidam Nigazhgiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Unnidam Nigazhgiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jenmangal Niraindhavan Neethaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jenmangal Niraindhavan Neethaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannangal Kanindhavan Neethaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannangal Kanindhavan Neethaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae Anuvin Men Niraiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Anuvin Men Niraiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnakkaga Naannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnakkaga Naannadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarkali Aarkali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarkali Aarkali"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarumo Kannimaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarumo Kannimaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhiyin Aazhamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhiyin Aazhamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalin Uyirthaezhudhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalin Uyirthaezhudhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee En Perum Velvigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee En Perum Velvigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Pozhivai Uyirum Nanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Pozhivai Uyirum Nanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamo Sila Maatrangal Nigazhthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamo Sila Maatrangal Nigazhthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkai Uraiyum Nimidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkai Uraiyum Nimidam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poongkatrae Poongkatrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongkatrae Poongkatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pularugirai Pudhumalarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pularugirai Pudhumalarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Melliravil Sudum Panithuliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melliravil Sudum Panithuliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiridaiyil Nee Urulugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiridaiyil Nee Urulugiraai"/>
</div>
</pre>
