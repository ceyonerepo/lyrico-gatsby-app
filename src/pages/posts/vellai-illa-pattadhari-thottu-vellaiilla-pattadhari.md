---
title: "vellai illa pattadhari thottu song lyrics"
album: "Vellaiilla Pattadhari"
artist: "Anirudh Ravichander"
lyricist: "Dhanush"
director: "Velraj"
path: "/albums/vellaiilla-pattadhari-lyrics"
song: "Vellai Illa Pattadhari Thottu"
image: ../../images/albumart/vellaiilla-pattadhari.jpg
date: 2014-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qcnqkr7oeNg"
type: "Mass Intro"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Velai Illa Pattadhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai Illa Pattadhaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottu Paaththa Shock Adikkum Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Paaththa Shock Adikkum Vera Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velai Illa Pattadhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai Illa Pattadhaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottu Paaththa Shock Adikkum Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Paaththa Shock Adikkum Vera Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru Mudhal Colorgal Thookattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Mudhal Colorgal Thookattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalamellaam Maarattum Tholodu Thol Serada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamellaam Maarattum Tholodu Thol Serada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholvi Ini Vervaigal Oothattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholvi Ini Vervaigal Oothattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vega Ellai Meerattum Mun Nokki Nee Odadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vega Ellai Meerattum Mun Nokki Nee Odadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vip Tea Kadai Raja Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vip Tea Kadai Raja Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vip Naalaiya India Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vip Naalaiya India Thaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vip Puriyaatha Saritharam Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vip Puriyaatha Saritharam Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vip Aama Thimiruthaan Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vip Aama Thimiruthaan Ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vip Tea Kadai Raja Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vip Tea Kadai Raja Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vip Naalaiya India Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vip Naalaiya India Thaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vip Puriyaatha Saritharam Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vip Puriyaatha Saritharam Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vip Aama Thimiruthaan Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vip Aama Thimiruthaan Ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai Athai Udai Puthu Sarithiram Padai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Athai Udai Puthu Sarithiram Padai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Namathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vali Athai Ozhi Puthu Vazhi Pirandhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Athai Ozhi Puthu Vazhi Pirandhidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatram Uruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatram Uruthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai Athai Udai Puthu Sarithiram Padai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai Athai Udai Puthu Sarithiram Padai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Namathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Namathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vali Athai Ozhi Puthu Vazhi Pirandhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Athai Ozhi Puthu Vazhi Pirandhidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maatram Uruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatram Uruthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooraga Padai Nooraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooraga Padai Nooraaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotta Idamellaam Thoolaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Idamellaam Thoolaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neraaga Vazhi Neraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neraaga Vazhi Neraaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellalaam Kaalam Maaraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellalaam Kaalam Maaraaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru Mudhal Colorgal Thookattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Mudhal Colorgal Thookattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalamellaam Maarattum Tholodu Thol Serada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamellaam Maarattum Tholodu Thol Serada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholvi Ini Vervaigal Oothattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholvi Ini Vervaigal Oothattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vega Ellai Meerattum Mun Nokki Nee Odada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vega Ellai Meerattum Mun Nokki Nee Odada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velai Illa Pattadhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai Illa Pattadhaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottu Paaththa Shock Adikkum Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Paaththa Shock Adikkum Vera Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velai Illa Pattadhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai Illa Pattadhaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottu Paaththa Shock Adikkum Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Paaththa Shock Adikkum Vera Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velai Illa Pattadhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai Illa Pattadhaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottu Paaththa Shock Adikkum Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Paaththa Shock Adikkum Vera Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velai Illa Pattadhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velai Illa Pattadhaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottu Paaththa Shock Adikkum Vera Maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Paaththa Shock Adikkum Vera Maari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tea Kadai Raja Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tea Kadai Raja Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalaiya India Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaiya India Thaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puriyaatha Saritharam Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaatha Saritharam Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aama Thimiruthaan Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama Thimiruthaan Ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tea Kadai Raja Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tea Kadai Raja Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalaiya India Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaiya India Thaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puriyaatha Saritharam Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaatha Saritharam Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aama Thimiruthaan Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama Thimiruthaan Ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vip Tea Kadai Raja Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vip Tea Kadai Raja Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vip Naalaiya India Thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vip Naalaiya India Thaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vip Puriyaatha Saritharam Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vip Puriyaatha Saritharam Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vip Aama Thimiruthaan Ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vip Aama Thimiruthaan Ponga"/>
</div>
</pre>
