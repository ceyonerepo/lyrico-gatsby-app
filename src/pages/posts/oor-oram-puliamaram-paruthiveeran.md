---
title: "oor oram puliamaram song lyrics"
album: "Paruthiveeran"
artist: "Yuvan Shankar Raja"
lyricist: "Snehan"
director: "Ameer"
path: "/albums/paruthiveeran-lyrics"
song: "Oor Oram Puliamaram"
image: ../../images/albumart/paruthiveeran.jpg
date: 2007-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1hyZUkORs7k"
type: "Enjoyment"
singers:
  - Kala
  - Lakshmi
  - Pandi
  - Madurai S
  - Saroja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oororam puliyamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oororam puliyamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Olippivitta salasalangai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olippivitta salasalangai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oororam puliyamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oororam puliyamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Plippivitta salasalangai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Plippivitta salasalangai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pirandha madurai-yila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pirandha madurai-yila"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalukkaalu naattaamaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalukkaalu naattaamaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pirandha madurai-yila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pirandha madurai-yila"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalukkaalu naattaamaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalukkaalu naattaamaiyaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooduinamae kooduinamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooduinamae kooduinamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poottu vandi kaalapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottu vandi kaalapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooduinamae kooduinamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooduinamae kooduinamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poottu vandi kaalapole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottu vandi kaalapole"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattuinamae maattuinamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattuinamae maattuinamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naarappaya kaiyimela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naarappaya kaiyimela"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattuinamae maattuinamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattuinamae maattuinamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naarappaya kaiyimela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naarappaya kaiyimela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadarinja azhagigalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadarinja azhagigalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga enga jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga enga jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungala kattikkava vachikava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungala kattikkava vachikava"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollipudungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollipudungadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththarippoo ravikka potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththarippoo ravikka potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna paingkili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna paingkili"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththarippoo ravikka potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththarippoo ravikka potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna paingkili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna paingkili"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna quarter-ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna quarter-ku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorugaya thottukkavaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorugaya thottukkavaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna quarter-ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna quarter-ku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorugaya thottukkavaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorugaya thottukkavaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuththunna ipdi thaan kuththanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththunna ipdi thaan kuththanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalillatha kaattukkulla payalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalillatha kaattukkulla payalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ravusu pannum chinna thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravusu pannum chinna thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Night ellaam aattampottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night ellaam aattampottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku kaalu rendum noguthadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku kaalu rendum noguthadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalu rendum noguthadaa.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalu rendum noguthadaa."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi raavellaam aattampottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi raavellaam aattampottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku kaalu rendum ippo nonthalenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku kaalu rendum ippo nonthalenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaa paruvamulla paiyan kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaa paruvamulla paiyan kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum paasaangu pannaathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum paasaangu pannaathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannaathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannaathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paruvamulla paiyan kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvamulla paiyan kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum paasanga pannavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum paasanga pannavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasanga pannurendu neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasanga pannurendu neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arivu kettu pesathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivu kettu pesathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee arivu kettu pesathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee arivu kettu pesathada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi maadi mela maadi vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi maadi mela maadi vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaralavu jennal vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaralavu jennal vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Apdi podu chiththappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apdi podu chiththappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etti etti parththaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti etti parththaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eravapondati neethaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eravapondati neethaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaahaaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaahaaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi kaatharuntha mooli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi kaatharuntha mooli"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna kattuvendi thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kattuvendi thaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi kaatharuntha mooli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi kaatharuntha mooli"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna kattuvendi thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kattuvendi thaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada intha paattu padikathadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada intha paattu padikathadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku vekkam aaguthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku vekkam aaguthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Amaa ama amaaamooi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amaa ama amaaamooi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Posagetta payalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Posagetta payalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku pondati kekkuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku pondati kekkuthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Neththiyilae…ae…ae…ae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neththiyilae…ae…ae…ae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamoooooiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamoooooiii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethiyilae pottu vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethiyilae pottu vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panja varanum selai katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panja varanum selai katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethiyilae pottu vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethiyilae pottu vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Panja varanum selai katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panja varanum selai katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathiyana veyilukkullae oththai veliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathiyana veyilukkullae oththai veliyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathiyana veyilukkullae oththai veliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathiyana veyilukkullae oththai veliyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee manasu veruthu pora karanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee manasu veruthu pora karanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkum theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkum theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee manasu veruthu pora karanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee manasu veruthu pora karanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkum theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkum theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konaang-krap vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konaang-krap vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthikaalu vetti katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthikaalu vetti katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Konaang-krap vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konaang-krap vetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthikaalu vetti katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthikaalu vetti katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasa kaatti mosam senja aambala neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasa kaatti mosam senja aambala neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasa kaatti mosam senja aambala neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasa kaatti mosam senja aambala neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungala arunjirunthu namburathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungala arunjirunthu namburathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungala arunjirunthu namburathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungala arunjirunthu namburathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyae alli…eeee…..ee….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae alli…eeee…..ee…."/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaan aahaan ahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaan aahaan ahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamoooooiii…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamoooooiii…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alli mayirurathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli mayirurathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadharaama kondaiyittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadharaama kondaiyittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli mayirurathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli mayirurathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadharaama kondaiyittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadharaama kondaiyittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulli maanai pola thulli pogum vazhiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli maanai pola thulli pogum vazhiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulli maanai pola thulli pogum vazhiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulli maanai pola thulli pogum vazhiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungala purinjikitta manasu summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungala purinjikitta manasu summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukka mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukka mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungala purinjikitta manasu summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungala purinjikitta manasu summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukka mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukka mudiyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poda poda podippayalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda poda podippayalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththi ketta madappayalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththi ketta madappayalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poda poda podippayalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda poda podippayalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththi ketta madappayalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththi ketta madappayalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eenangetta chinnappaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eenangetta chinnappaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennennamo pesuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo pesuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum enakkum sandai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum enakkum sandai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo udayappoguthu mandai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo udayappoguthu mandai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada unakkum enakkum sandai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada unakkum enakkum sandai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo udayappoguthu mandai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo udayappoguthu mandai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi en kuttapillai annakili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi en kuttapillai annakili"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vandhu sethi kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vandhu sethi kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttapillai annakili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttapillai annakili"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vanthu sethi kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vanthu sethi kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruppudatha naanga iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruppudatha naanga iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruppu varaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruppu varaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruppudatha naanga iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruppudatha naanga iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruppu varaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruppu varaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engala purinjikitta manasu summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engala purinjikitta manasu summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukka vidaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukka vidaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engala purinjikitta manasu summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engala purinjikitta manasu summa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukka vidaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukka vidaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna naayanakkaararae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna naayanakkaararae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa vedikka paathirukkigalae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa vedikka paathirukkigalae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayila vachchi oothavendiyathuthaanae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayila vachchi oothavendiyathuthaanae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga oothuriyala illa naan oothavaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga oothuriyala illa naan oothavaa…"/>
</div>
</pre>
