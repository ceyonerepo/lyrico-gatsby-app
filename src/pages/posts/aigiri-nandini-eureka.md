---
title: "aigiri nandini song lyrics"
album: "Eureka"
artist: "Naresh Kumaran"
lyricist: "Ramanjaneyulu"
director: "Karteek Anand"
path: "/albums/eureka-lyrics"
song: "Aigiri Nandini"
image: ../../images/albumart/eureka.jpg
date: 2020-03-13
lang: telugu
youtubeLink: "https://www.youtube.com/embed/0gbNNeP7Ee0"
type: "mass"
singers:
  - Rahul Sipligunj 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gaale Geetha Geesthe Aagadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaale Geetha Geesthe Aagadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Neede Musugeste Dagadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neede Musugeste Dagadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Satyam Cheripeste Swapnamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satyam Cheripeste Swapnamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari Podhule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari Podhule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram Chesindhe Evvaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Chesindhe Evvaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Sakshyam Undhe Nivurulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Sakshyam Undhe Nivurulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anveshanane Nippu Kosame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anveshanane Nippu Kosame"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Daroolu Jalledeyyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Daroolu Jalledeyyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi Nelalanni Ponchi Choodaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi Nelalanni Ponchi Choodaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaku Ralina Chappudu Ayyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaku Ralina Chappudu Ayyina"/>
</div>
<div class="lyrico-lyrics-wrapper">Shatruwunna Jada Gurthu Pattna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shatruwunna Jada Gurthu Pattna"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekate Wo Uchhulaga Maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekate Wo Uchhulaga Maare"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekuve Aa Dhikkulonaa Daage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekuve Aa Dhikkulonaa Daage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edurayye Ee Prashnalennonede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edurayye Ee Prashnalennonede"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedhistha Kammina Chikkulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedhistha Kammina Chikkulanni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aigiri Nandini Nandhitha Medhini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aigiri Nandini Nandhitha Medhini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishva Vinodhini Nandanuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishva Vinodhini Nandanuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Giriwara Vindhya Sirodhi Nivasini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giriwara Vindhya Sirodhi Nivasini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishnu Vilasini Jishnu Nuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishnu Vilasini Jishnu Nuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhawathi Hey Sithi Kanda Kudumbini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhawathi Hey Sithi Kanda Kudumbini"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoori Kudumbini Bhoori Kuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoori Kudumbini Bhoori Kuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya Jaya He Mahishasur Mardini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Jaya He Mahishasur Mardini"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramya Kapardini Shail Suthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramya Kapardini Shail Suthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayuvanndhe Harinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayuvanndhe Harinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Viyuhamevvaro Rachinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viyuhamevvaro Rachinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaali Lekane Vadhinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaali Lekane Vadhinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Gali Dhadi Lona Deepamaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gali Dhadi Lona Deepamaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Praname Theesene Kutra Panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praname Theesene Kutra Panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhuko Dhehamaina Matti Cheyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhuko Dhehamaina Matti Cheyu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dwesham Endukosam Oopiree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dwesham Endukosam Oopiree"/>
</div>
<div class="lyrico-lyrics-wrapper">Theesina Cheyi Daagenekkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theesina Cheyi Daagenekkada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yamudu Veyu Pashamalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamudu Veyu Pashamalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Raaka Thadhyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Raaka Thadhyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekate Vo Uchhulaga Maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekate Vo Uchhulaga Maare"/>
</div>
<div class="lyrico-lyrics-wrapper">Wekuve Aa Dhikkulona Daage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wekuve Aa Dhikkulona Daage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ganthale Ne Vipputhaan Nede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganthale Ne Vipputhaan Nede"/>
</div>
<div class="lyrico-lyrics-wrapper">Choosela Lokame Kanthulewo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choosela Lokame Kanthulewo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aigiri Nandini Nandhitha Medhini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aigiri Nandini Nandhitha Medhini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishva Vinodhini Nandanuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishva Vinodhini Nandanuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Giriwara Vindhya Sirodhi Nivasini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giriwara Vindhya Sirodhi Nivasini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishnu Vilasini Jishnu Nuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishnu Vilasini Jishnu Nuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhawathi Hey Sithi Kanda Kudumbini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhawathi Hey Sithi Kanda Kudumbini"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoori Kudumbini Bhoori Kuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoori Kudumbini Bhoori Kuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya Jaya He Mahishasur Mardini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya Jaya He Mahishasur Mardini"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramya Kapardini Shail Suthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramya Kapardini Shail Suthe"/>
</div>
</pre>
