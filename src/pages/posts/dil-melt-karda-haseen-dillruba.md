---
title: "dil melt karda song lyrics"
album: "Haseen Dillruba"
artist: "Amit Trivedi"
lyricist: "Varun Grover"
director: "Vinil Mathew"
path: "/albums/haseen-dillruba-lyrics"
song: "Dil Melt Karda"
image: ../../images/albumart/haseen-dillruba.jpg
date: 2021-07-02
lang: hindi
youtubeLink: "https://www.youtube.com/embed/MNOar14DJYI"
type: "love"
singers:
  - Navraj Hans
  - Nikhita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaye Haye Haye Haye Haye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaye Haye Haye Haye Haye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaye Haye Haye Haye Haye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaye Haye Haye Haye Haye"/>
</div>
<div class="lyrico-lyrics-wrapper">Oye Hoye Hoye Hoye Hoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oye Hoye Hoye Hoye Hoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaye Haye Haye Haye Haye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaye Haye Haye Haye Haye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aane Jane Wale Saare Takne Lage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aane Jane Wale Saare Takne Lage"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Tera Nind Mein Hum Bakne Lage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Tera Nind Mein Hum Bakne Lage"/>
</div>
<div class="lyrico-lyrics-wrapper">Haye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aane Jane Wale Saare Takne Lage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aane Jane Wale Saare Takne Lage"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Tera Nind Mein Hum Bakne Lage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Tera Nind Mein Hum Bakne Lage"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann Mein Ferraro Rocher Foot Gaya Ji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann Mein Ferraro Rocher Foot Gaya Ji"/>
</div>
<div class="lyrico-lyrics-wrapper">Armaan Ke Jugnu Bhadkne Lage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Armaan Ke Jugnu Bhadkne Lage"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhadkne Lage Haye Bhadkne Lage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhadkne Lage Haye Bhadkne Lage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nayan Mile Toh Fire Jale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayan Mile Toh Fire Jale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aisi Fire Jale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisi Fire Jale"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil Melt Melt Karda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil Melt Melt Karda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Nayan Mile Toh Fire Jale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nayan Mile Toh Fire Jale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aisi Fire Jale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisi Fire Jale"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil Melt Melt Karda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil Melt Melt Karda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Confusion!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confusion!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Thoda Sa Yeh Naughty Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thoda Sa Yeh Naughty Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda Sa Bhola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda Sa Bhola"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Munda Thoda Sa Bhola La La La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Munda Thoda Sa Bhola La La La"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoda Sa Toh Mera Bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda Sa Toh Mera Bhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tann Mann Hai Dola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tann Mann Hai Dola"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Dola Tann Mann Hai Dola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Dola Tann Mann Hai Dola"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Waise Dil Pe Lagi Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waise Dil Pe Lagi Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aake Chot Yeh Kahin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aake Chot Yeh Kahin"/>
</div>
<div class="lyrico-lyrics-wrapper">Par Hoon Confuse Hai Yeh Banda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par Hoon Confuse Hai Yeh Banda"/>
</div>
<div class="lyrico-lyrics-wrapper">Hot Hai Ya Nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot Hai Ya Nahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hot Hai Ya Nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot Hai Ya Nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hot Hai Ya Nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot Hai Ya Nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hot Hai Ya Nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot Hai Ya Nahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhar Chale Kabhi Udhar Chale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhar Chale Kabhi Udhar Chale"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud Se Hi Ladd Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud Se Hi Ladd Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhan Chhan Dil Wajda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhan Chhan Dil Wajda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Nayan Mile Toh Fire Jale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Nayan Mile Toh Fire Jale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aisi Fire Jale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisi Fire Jale"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil Melt Melt Karda Ji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil Melt Melt Karda Ji"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hai Bandi Sidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai Bandi Sidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Main Banda Tedha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main Banda Tedha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh Jhooth Bhatera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh Jhooth Bhatera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Ho Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Ho Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hai Banda Bhola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai Banda Bhola"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur Bandi Shatir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur Bandi Shatir"/>
</div>
<div class="lyrico-lyrics-wrapper">Is Baat Ki Khatir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is Baat Ki Khatir"/>
</div>
<div class="lyrico-lyrics-wrapper">No No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho Nayan Mile Toh Fire Jale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Nayan Mile Toh Fire Jale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aisi Fire Jale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisi Fire Jale"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil Melt Melt Karda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil Melt Melt Karda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nayan Mile Toh Fire Jale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayan Mile Toh Fire Jale"/>
</div>
<div class="lyrico-lyrics-wrapper">Aisi Fire Jale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisi Fire Jale"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil Melt Melt Karda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil Melt Melt Karda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dil Melt Melt Karda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil Melt Melt Karda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Dil Melt Melt Karda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Dil Melt Melt Karda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Light Kisne Bujha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Light Kisne Bujha Di"/>
</div>
</pre>
