---
title: "salaam aharasa song lyrics"
album: "Badri"
artist: "Ramana Gogula"
lyricist: "Palani Bharathi"
director: "P. A. Arun Prasad"
path: "/albums/badri-lyrics"
song: "Salaam Maharasa"
image: ../../images/albumart/badri.jpg
date: 2001-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pOLlksDfxxw"
type: "happy"
singers:
  - Devan Ekambaram
  - Priya Himesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Salaam Maharaasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaam Maharaasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaam Maharaasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaam Maharaasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga Vayasukku Thaan Aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Vayasukku Thaan Aaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungha Kaithattu Thaane En Thuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungha Kaithattu Thaane En Thuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu Thillana Naan Poduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu Thillana Naan Poduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Manam Vittu Enna Paarattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Manam Vittu Enna Paarattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Pambarama Naan Suththuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Pambarama Naan Suththuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Thiranaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Thiranaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha Thillaaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha Thillaaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mohanaambal Nee Thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mohanaambal Nee Thaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nimirnthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nimirnthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Kuninjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Kuninjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Vedikudhadi Oosi Vedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Vedikudhadi Oosi Vedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Suthiyethu Konjam Kushiyethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Suthiyethu Konjam Kushiyethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Hotellaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Hotellaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vangi Tharen Un Koothukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangi Tharen Un Koothukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Pappa Una Partha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Pappa Una Partha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Kabadi Aadudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Kabadi Aadudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Iduppil Ulla Madippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Iduppil Ulla Madippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kasakki Podudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kasakki Podudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rombha Deepa Unga Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rombha Deepa Unga Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Aazham Paarkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Aazham Paarkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga Moochu Onnu Serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Moochu Onnu Serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Sooduyethuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Sooduyethuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandrangal Veikka Naanga Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandrangal Veikka Naanga Ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Thirappu Vizhavil Kalanthukkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thirappu Vizhavil Kalanthukkadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Pappa Una Partha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Pappa Una Partha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Kabadi Aadudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Kabadi Aadudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Iduppil Ulla Madippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Iduppil Ulla Madippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Kasakki Podudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Kasakki Podudhu"/>
</div>
</pre>
