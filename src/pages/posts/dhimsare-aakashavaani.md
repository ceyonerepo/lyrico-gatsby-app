---
title: "dhimsare song lyrics"
album: "Aakashavaani"
artist: "Kaala Bhairava"
lyricist: "Ananth Sriram"
director: "Ashwin Gangaraju"
path: "/albums/aakashavaani-lyrics"
song: "Dhimsare"
image: ../../images/albumart/aakashavaani.jpg
date: 2021-09-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/7w53UXKKBns"
type: "happy"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dappuleyro Giddado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappuleyro Giddado"/>
</div>
<div class="lyrico-lyrics-wrapper">Daruvuleyro Biddado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daruvuleyro Biddado"/>
</div>
<div class="lyrico-lyrics-wrapper">Aey Sinnaa Peddaa Sindhelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aey Sinnaa Peddaa Sindhelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sukkaleyro Subbado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukkaleyro Subbado"/>
</div>
<div class="lyrico-lyrics-wrapper">Surakaleyro Abbado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surakaleyro Abbado"/>
</div>
<div class="lyrico-lyrics-wrapper">Aey Konda Konaa Oogelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aey Konda Konaa Oogelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gudemlo Kodi Kootheyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gudemlo Kodi Kootheyyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello Lady Lechelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello Lady Lechelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gullona Saami Thodiyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gullona Saami Thodiyyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gudisello Deepaaleligelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gudisello Deepaaleligelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Dhimsare Dhimsare Yaare  Yaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Dhimsare Dhimsare Yaare  Yaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhimsare Dhimsaa Dhimsaa Huyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimsare Dhimsaa Dhimsaa Huyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhimsare Dhimsare Yaare  Yaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimsare Dhimsare Yaare  Yaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhimsare Dhimsaa Dhimsaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimsare Dhimsaa Dhimsaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Huyy Huyy Aeyy Yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huyy Huyy Aeyy Yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Ranguleyro Rangado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Ranguleyro Rangado"/>
</div>
<div class="lyrico-lyrics-wrapper">Rachhaseyro Singado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rachhaseyro Singado"/>
</div>
<div class="lyrico-lyrics-wrapper">Aey Ningi Nela Kaliselaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aey Ningi Nela Kaliselaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mukkaleyro Mellado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukkaleyro Mellado"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulakaleyro Pillado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulakaleyro Pillado"/>
</div>
<div class="lyrico-lyrics-wrapper">Aey, Vaagu Vankaa Pongelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aey, Vaagu Vankaa Pongelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gudemlo Kodi Kootheyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gudemlo Kodi Kootheyyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello Lady Lechelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello Lady Lechelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gullona Saami Thodiyyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gullona Saami Thodiyyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Gudisello Deepaaleligelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gudisello Deepaaleligelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa AaAa Aa Aa Oo OoOo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa AaAa Aa Aa Oo OoOo Oo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">YeYe Saamee Saamee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="YeYe Saamee Saamee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Punyam Sesaamo Ye Mannem Needhaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Punyam Sesaamo Ye Mannem Needhaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Illu Naa Goodu Antha Nee Needaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Illu Naa Goodu Antha Nee Needaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Santodi Paanaale Neelisaayante Nee Maaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santodi Paanaale Neelisaayante Nee Maaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palike Daivam Mundunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palike Daivam Mundunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Palledainaa Malleraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palledainaa Malleraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaache Daivam Entunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaache Daivam Entunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Neederaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Neederaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanainaa Madisai Maataadalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanainaa Madisai Maataadalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Sentha Seri Oogelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Sentha Seri Oogelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanainaa Varasai Aataadaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanainaa Varasai Aataadaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Sinthalannee Teerelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Sinthalannee Teerelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Dhimsare Dhimsare Yaare  Yaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Dhimsare Dhimsare Yaare  Yaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhimsare Dhimsaa Dhimsaa Huyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimsare Dhimsaa Dhimsaa Huyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhimsare Dhimsare Yaare  Yaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimsare Dhimsare Yaare  Yaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhimsare Dhimsaa Dhimsaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimsare Dhimsaa Dhimsaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Dhimsare Dhimsare Dhimsare Dhimsare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Dhimsare Dhimsare Dhimsare Dhimsare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhimsare Dhimsare Dhimsare Dhimsare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimsare Dhimsare Dhimsare Dhimsare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhimsare Dhimsare Dhimsare Dhimsare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimsare Dhimsare Dhimsare Dhimsare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhimsare Dhimsaa Dhimsaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhimsare Dhimsaa Dhimsaa"/>
</div>
</pre>
