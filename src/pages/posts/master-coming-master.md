---
title: "master coming song lyrics"
album: "Master"
artist: "Anirudh Ravichander"
lyricist: "Sri Sai Kiran"
director: "Lokesh Kanakaraj"
path: "/albums/master-lyrics"
song: "Master Coming"
image: ../../images/albumart/master.jpg
date: 2021-01-13
lang: telugu
youtubeLink: "https://www.youtube.com/embed/qItykoFeqNE"
type: "mass"
singers:
  - Gaana Balachander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rey entra idhi dabba beatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rey entra idhi dabba beatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthakara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthakara"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah ah maja pa maja pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aah ah maja pa maja pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Itra itra itra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itra itra itra"/>
</div>
<div class="lyrico-lyrics-wrapper">Randira randira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Randira randira"/>
</div>
<div class="lyrico-lyrics-wrapper">Master coming chudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Master coming chudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey chudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Duppa duppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duppa duppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Anna vandha atom bombu dappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna vandha atom bombu dappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilupilupilu pilami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilupilupilu pilami"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilupilupilu ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilupilupilu ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilupilupilu pilami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilupilupilu pilami"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilupilupilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilupilupilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilupilupilu pilami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilupilupilu pilami"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilupilupilu pilami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilupilupilu pilami"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilupilupilu pilupilupilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilupilupilu pilupilupilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Master coming chudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Master coming chudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorraga katte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorraga katte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey tarukkula tripleu vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey tarukkula tripleu vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Salipila silpi thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salipila silpi thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thogurula thagara vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thogurula thagara vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Paguru aguruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paguru aguruthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silkila silki vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silkila silki vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilpula salttu thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilpula salttu thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Bijilila bilpi vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bijilila bilpi vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jettak jarukkun thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jettak jarukkun thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeye, aiyooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeye, aiyooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Line kattu line kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Line kattu line kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Itukku dumaa Itukku dumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itukku dumaa Itukku dumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaa modhalu pettu modhalu pettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaa modhalu pettu modhalu pettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anna vandhaa atom bombu dhammu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna vandhaa atom bombu dhammu"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaa modhalu pettu modhalu pettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaa modhalu pettu modhalu pettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Master coming chudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Master coming chudu"/>
</div>
</pre>
