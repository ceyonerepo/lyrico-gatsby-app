---
title: "chuttipnnae chuttipennae song lyrics"
album: "Uchithanai Muharnthaal"
artist: "D. Imman"
lyricist: "Kathirmozhi"
director: "Pugazhendhi Thangaraj"
path: "/albums/uchithanai-muharnthaal-lyrics"
song: "Chuttipnnae Chuttipennae"
image: ../../images/albumart/uchithanai-muharnthaal.jpg
date: 2011-12-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MXrIm1gXvtY"
type: "happy"
singers:
  - SuVi
  - Durga Viswanath
  - Harini Ravi
  - Shekkinath Shawn
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">chutti penne chutti penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chutti penne chutti penne"/>
</div>
<div class="lyrico-lyrics-wrapper">oru kattu kaval katruku illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kattu kaval katruku illai"/>
</div>
<div class="lyrico-lyrics-wrapper">sittu pola paranthu sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sittu pola paranthu sella"/>
</div>
<div class="lyrico-lyrics-wrapper">antha vaanam kooda thooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha vaanam kooda thooram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">thuru thuru pechu kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuru thuru pechu kari"/>
</div>
<div class="lyrico-lyrics-wrapper">viru viru veechu kari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viru viru veechu kari"/>
</div>
<div class="lyrico-lyrics-wrapper">thiru thiru endru mulikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiru thiru endru mulikathe"/>
</div>
<div class="lyrico-lyrics-wrapper">sada sada vaanam pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sada sada vaanam pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">sala sala odai pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sala sala odai pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">pada pada pechu salikathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pada pada pechu salikathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">podi podi rosakari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podi podi rosakari "/>
</div>
<div class="lyrico-lyrics-wrapper">nee thedi vantha pasakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thedi vantha pasakari"/>
</div>
<div class="lyrico-lyrics-wrapper">podi podi rosakari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podi podi rosakari "/>
</div>
<div class="lyrico-lyrics-wrapper">nee thedi vantha pasakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thedi vantha pasakari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chutti penne chutti penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chutti penne chutti penne"/>
</div>
<div class="lyrico-lyrics-wrapper">oru kattu kaval katruku illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kattu kaval katruku illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalum illaiye aatru thaniku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalum illaiye aatru thaniku"/>
</div>
<div class="lyrico-lyrics-wrapper">salangai kati vitatharunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="salangai kati vitatharunga"/>
</div>
<div class="lyrico-lyrics-wrapper">kayam patathum moongil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kayam patathum moongil"/>
</div>
<div class="lyrico-lyrics-wrapper">kuchiku saranam solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuchiku saranam solli"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthau aarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthau aarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">nalaiye unthan kaiyil adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalaiye unthan kaiyil adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">poovai theendum poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovai theendum poo"/>
</div>
<div class="lyrico-lyrics-wrapper">thayin mannai mutham iduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayin mannai mutham iduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">saavai kanum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saavai kanum pothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">villuku intha boomi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="villuku intha boomi "/>
</div>
<div class="lyrico-lyrics-wrapper">baaram aavathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baaram aavathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">moodi vaitha pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moodi vaitha pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">anbai mitta sorvathilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbai mitta sorvathilai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">podi podi rosakari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podi podi rosakari "/>
</div>
<div class="lyrico-lyrics-wrapper">nee thedi vantha pasakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thedi vantha pasakari"/>
</div>
<div class="lyrico-lyrics-wrapper">podi podi rosakari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podi podi rosakari "/>
</div>
<div class="lyrico-lyrics-wrapper">nee thedi vantha pasakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thedi vantha pasakari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chutti penne chutti penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chutti penne chutti penne"/>
</div>
<div class="lyrico-lyrics-wrapper">oru kattu kaval katruku illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kattu kaval katruku illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saarai saaraiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saarai saaraiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">thooral poduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooral poduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">oonjal katti aadi padalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oonjal katti aadi padalam"/>
</div>
<div class="lyrico-lyrics-wrapper">minathangathum engu sendratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minathangathum engu sendratho"/>
</div>
<div class="lyrico-lyrics-wrapper">oodi vanga thedi parkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodi vanga thedi parkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">ondre parisu thayin anaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondre parisu thayin anaipu"/>
</div>
<div class="lyrico-lyrics-wrapper">rendum thanthai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendum thanthai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">chella sandaiyai pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chella sandaiyai pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">thavithom unnal indru thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavithom unnal indru thane"/>
</div>
<div class="lyrico-lyrics-wrapper">saaral mothi sayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaral mothi sayam "/>
</div>
<div class="lyrico-lyrics-wrapper">poovil povathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovil povathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyin thinnum yavarukum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyin thinnum yavarukum "/>
</div>
<div class="lyrico-lyrics-wrapper">vaipathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaipathu illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">podi podi rosakari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podi podi rosakari "/>
</div>
<div class="lyrico-lyrics-wrapper">nee thedi vantha pasakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thedi vantha pasakari"/>
</div>
<div class="lyrico-lyrics-wrapper">podi podi rosakari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podi podi rosakari "/>
</div>
<div class="lyrico-lyrics-wrapper">nee thedi vantha pasakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thedi vantha pasakari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chutti penne chutti penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chutti penne chutti penne"/>
</div>
<div class="lyrico-lyrics-wrapper">chutti penne chutti penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chutti penne chutti penne"/>
</div>
</pre>
