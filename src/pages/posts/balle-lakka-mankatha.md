---
title: "balle lakka song lyrics"
album: "Mankatha"
artist: "Yuvan Shankar Raja"
lyricist: "Gangai Amaran"
director: "Venkat Prabhu"
path: "/albums/mankatha-lyrics"
song: "Balle Lakka"
image: ../../images/albumart/mankatha.jpg
date: 2011-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r7MlejG7WsI"
type: "happy"
singers:
  - Karthik
  - Vijay Yesudas
  - Anusha Durai Dhayanidhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhu Enga Balle Lakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enga Balle Lakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mee Kelu Kokka Makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee Kelu Kokka Makka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">New Type-u Naakku Mukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="New Type-u Naakku Mukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadiko Kicka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadiko Kicka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Abhi Sabbo Acha Acha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abhi Sabbo Acha Acha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandavan Kan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavan Kan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vachaan Vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachaan Vachaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Da Dhool Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Da Dhool Machan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machan Vaangiko Extra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Vaangiko Extra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soodhilla Soodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodhilla Soodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Suthamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Suthamaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagellam Poondhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagellam Poondhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adha Oruti Podappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Oruti Podappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappu Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappaagadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappaagadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Satham Pottu Sollappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satham Pottu Sollappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annan Sonna Vaartha Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan Sonna Vaartha Kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya Korthu Allappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Korthu Allappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Thaana Kidaikuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Thaana Kidaikuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Santhosam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeya Eduthuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeya Eduthuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakaagathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaagathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veena Polambuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena Polambuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyaathappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyaathappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilakka Yethuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilakka Yethuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruttathappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttathappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudiyaathadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaathadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padiyaathadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padiyaathadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethum Kidaiyadhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethum Kidaiyadhada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuthaan Kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaan Kanakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyaathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyaathathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnum Puriyaathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum Puriyaathathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Athuthaanada Irutto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Athuthaanada Irutto"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thittam Potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittam Potu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatti Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kitta Serum Thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Serum Thannaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vattam Pottu Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam Pottu Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podu Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laabam Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laabam Munnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veena Thoonguthu Pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena Thoonguthu Pala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kidithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidithaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atha Mulika Vaipavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Mulika Vaipavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Killadi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killadi Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olachi Vazhave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olachi Vazhave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venaamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaamada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirar Ulaipil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirar Ulaipil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhvadhe Varam Thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvadhe Varam Thaanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasapadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasapadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alave Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alave Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aambalaiku Athuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aambalaiku Athuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kovapadu Kuraiye Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovapadu Kuraiye Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pombalaiku Athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pombalaiku Athu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaan Poruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan Poruppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thittam Potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittam Potu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatti Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kitta Serum Thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Serum Thannaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vattam Pottu Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam Pottu Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podu Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laabam Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laabam Munnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Enga Balle Lakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Enga Balle Lakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kelu Kokka Makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kelu Kokka Makka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">New Type u Naakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="New Type u Naakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mukka Aadiko Kicka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukka Aadiko Kicka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Abhi Sabbo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abhi Sabbo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Acha Acha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acha Acha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandavan Kan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavan Kan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vachaan Vachaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachaan Vachaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Da Dhool Machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Da Dhool Machan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaangiko Extra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangiko Extra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soodhilla Soodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodhilla Soodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Suthamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Suthamaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagellam Poondhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagellam Poondhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adha Oruti Podappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Oruti Podappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappu Kooda Thappagadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Kooda Thappagadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Satham Pottu Sollappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satham Pottu Sollappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Sonna Vaartha Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Sonna Vaartha Kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya Korthu Allappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Korthu Allappa"/>
</div>
</pre>
