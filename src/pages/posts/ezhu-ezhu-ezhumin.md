---
title: "ezhu ezhu song lyrics"
album: "Ezhumin"
artist: "Ganesh Chandrasekaran"
lyricist: "Vivek"
director: "VP Viji"
path: "/albums/ezhumin-lyrics"
song: "Ezhu Ezhu"
image: ../../images/albumart/ezhumin.jpg
date: 2018-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OHBx3d62SGA"
type: "mass"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vetri magizhchi mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri magizhchi mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan kodukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan kodukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana thozhvi thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana thozhvi thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungala sethukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungala sethukkum"/>
</div>
(<div class="lyrico-lyrics-wrapper">Dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dialogue)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthaal vithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthaal vithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthaal maram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthaal maram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu ezhu ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu ezhu ezhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuninthaal seyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninthaal seyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Suzhandraal puyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhandraal puyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu ezhu ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu ezhu ezhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Purappadu ithu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purappadu ithu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Puliyin nadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puliyin nadai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pujabalam kaati thadaigal udai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pujabalam kaati thadaigal udai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalaigal ennum kadaiyai adai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigal ennum kadaiyai adai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal jeithidum puthiya padai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal jeithidum puthiya padai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanavan enbavan poo pantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanavan enbavan poo pantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Matravar udhaithidum kaal pantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matravar udhaithidum kaal pantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sothanai thaandi nee vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sothanai thaandi nee vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgamae irukku vaa nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgamae irukku vaa nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu ezhu ezhu ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu ezhu ezhu ezhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthaal vithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthaal vithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthaal maram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthaal maram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu ezhu ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu ezhu ezhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuninthaal seyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninthaal seyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Suzhandraal puyalEzhu ezhu ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhandraal puyalEzhu ezhu ezhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neengalam yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengalam yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu latcham per onna koodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu latcham per onna koodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu poiruntha jallikatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu poiruntha jallikatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Marupadiyum nadathana enam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marupadiyum nadathana enam da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponga da poi ellarayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponga da poi ellarayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti thookkungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutti thookkungada"/>
</div>
(<div class="lyrico-lyrics-wrapper">Dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dialogue)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urangi kidanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangi kidanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarvu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarvu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaipaal vendridu pon medalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaipaal vendridu pon medalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundidum vaanam namakku illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundidum vaanam namakku illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Erangi adi da thozhvi illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangi adi da thozhvi illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theanaai ini kattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theanaai ini kattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vervai thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai thuli"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaai irukkattum uzhaipin vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaai irukkattum uzhaipin vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaai thirakkuma vetri vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaai thirakkuma vetri vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethanae unnai sethukkum uzhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethanae unnai sethukkum uzhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu ezhu ezhu ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu ezhu ezhu ezhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthaal vithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthaal vithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthaal maram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthaal maram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu ezhu ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu ezhu ezhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuninthaal seyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninthaal seyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Suzhandraal puyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhandraal puyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu ezhu ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu ezhu ezhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhumin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhumin"/>
</div>
</pre>
