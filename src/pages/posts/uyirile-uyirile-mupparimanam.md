---
title: "uyirile uyirile song lyrics"
album: "Mupparimanam"
artist: "G V Prakash Kumar"
lyricist: "Yugabharathi"
director: "Adhiroopan"
path: "/albums/mupparimanam-lyrics"
song: "Uyirile Uyirile"
image: ../../images/albumart/mupparimanam.jpg
date: 2017-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Tln5m_6mOFE"
type: "sad"
singers:
  -	Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uyirilae uyirilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirilae uyirilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraindhadhae kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraindhadhae kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravu nee pirindhadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu nee pirindhadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyilae thooralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyilae thooralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovae un vaasam indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovae un vaasam indri"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yedhu adii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yedhu adii"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai serndhu neeyum ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai serndhu neeyum ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaapatrudii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaapatrudii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirilae uyirilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirilae uyirilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraindhadhae kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraindhadhae kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravu nee pirindhadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu nee pirindhadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyilae thooralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyilae thooralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan kaanum katchi ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaanum katchi ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaatudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaatudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae nee yenghae endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae nee yenghae endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ketkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ketkudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theneerilum un poomugham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theneerilum un poomugham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindaadudhae yen nyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaadudhae yen nyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann thedudhae un kaal thadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann thedudhae un kaal thadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mmm mmmm mmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmm mmmm mmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirilae uyirilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirilae uyirilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraindhadhae kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraindhadhae kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravu nee pirindhadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu nee pirindhadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyilae thooralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyilae thooralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyaamal veesum kaatrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaamal veesum kaatrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai pesudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai pesudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mootchodu serndhu kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mootchodu serndhu kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenai yesudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenai yesudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannerilae kann muzhgudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerilae kann muzhgudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadiyae kal modhudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadiyae kal modhudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenaaghumoo en kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenaaghumoo en kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aee aeee aeeee aeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aee aeee aeeee aeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirilae uyirilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirilae uyirilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraindhadhae kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraindhadhae kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravu nee pirindhadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu nee pirindhadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyilae thooralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyilae thooralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovae un vaasam indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovae un vaasam indri"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yedhu adii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yedhu adii"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai serndhu neeyum ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai serndhu neeyum ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaapatrudii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaapatrudii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirilae uyirilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirilae uyirilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uraindhadhae kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraindhadhae kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravu nee pirindhadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu nee pirindhadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyilae thooralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyilae thooralae"/>
</div>
</pre>
