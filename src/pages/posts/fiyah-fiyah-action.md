---
title: "fiyah fiyah song lyrics"
album: "Action"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha - Navz47 - Arivu"
director: "R Vijay Anand - AR Suriyan"
path: "/albums/thavam-lyrics"
song: "Fiyah Fiyah"
image: ../../images/albumart/thavam.jpg
date: 2019-11-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1YFza3yDBAc"
type: "happy"
singers:
  - Navz47
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah Fiyah Fiyah"/>
</div>
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah Fiyah"/>
</div>
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah Fiyah Fiyah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah Fiyah"/>
</div>
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah Fiyah Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah Fiyah Fiyah Fiyah"/>
</div>
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah Fiyah Fiyah Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah Fiyah Fiyah Fiyah Fiyah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottadha Mudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottadha Mudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittadha Thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittadha Thodarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Idam Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Idam Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pera Kettalae Adhirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pera Kettalae Adhirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthalae Padharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthalae Padharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu Kaathu Full-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Kaathu Full-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kazhuvura Meenula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuvura Meenula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nazhuvura Meena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazhuvura Meena"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Thuli Visham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Thuli Visham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalandhidum Pudhu Thaena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalandhidum Pudhu Thaena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuppakki Edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppakki Edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Oru Pen-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Oru Pen-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattunnu Erakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattunnu Erakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Chapter Close Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Chapter Close Now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Yeah Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah Yeah Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Killa Killa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killa Killa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kangal Vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangal Vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paarvai Tequila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paarvai Tequila"/>
</div>
<div class="lyrico-lyrics-wrapper">En Swasam Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Swasam Fiyah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Killa Killa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killa Killa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kangal Vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangal Vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paarvai Tequila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paarvai Tequila"/>
</div>
<div class="lyrico-lyrics-wrapper">En Swasam Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Swasam Fiyah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah Fiyah Fiyah"/>
</div>
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah Fiyah"/>
</div>
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah Fiyah Fiyah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah Fiyah"/>
</div>
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah Fiyah Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah Fiyah Fiyah Fiyah"/>
</div>
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah Fiyah Fiyah Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah Fiyah Fiyah Fiyah Fiyah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Maayaavi Hey Maayaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Maayaavi Hey Maayaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Maayaavi Hey Maayaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Maayaavi Hey Maayaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Poraali Hey Poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Poraali Hey Poraali"/>
</div>
<div class="lyrico-lyrics-wrapper">En Por Thaan Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Por Thaan Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En Por Thaan Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Por Thaan Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Pudhu Vidha Maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pudhu Vidha Maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhiyaadha Kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhiyaadha Kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigalai Udaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalai Udaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veli Varum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli Varum Neram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo Pol Sirippaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Pol Sirippaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangi Vidaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangi Vidaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee Pol Suduvaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Pol Suduvaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutta Vadu Aaraadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutta Vadu Aaraadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fiyah Fiyah Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fiyah Fiyah Fiyah"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t Mess With The Tiger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t Mess With The Tiger"/>
</div>
<div class="lyrico-lyrics-wrapper">Finger On The Trigger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Finger On The Trigger"/>
</div>
<div class="lyrico-lyrics-wrapper">Trigga Trigga Trigga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trigga Trigga Trigga"/>
</div>
<div class="lyrico-lyrics-wrapper">My Gucci Bag Got
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Gucci Bag Got"/>
</div>
<div class="lyrico-lyrics-wrapper">That AK 47
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That AK 47"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethi Pottil Eduthu Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethi Pottil Eduthu Vechu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Yeah Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah Yeah Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Killa Killa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killa Killa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kangal Vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangal Vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paarvai Tequila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paarvai Tequila"/>
</div>
<div class="lyrico-lyrics-wrapper">En Swasam Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Swasam Fiyah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Killa Killa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killa Killa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kangal Vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangal Vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paarvai Tequila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paarvai Tequila"/>
</div>
<div class="lyrico-lyrics-wrapper">En Swasam Fiyah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Swasam Fiyah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arakki Naan Azhikka Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arakki Naan Azhikka Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaikkiren Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaikkiren Vaa Vaa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirukki Naan Sirithu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirukki Naan Sirithu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkuren Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkuren Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Maayaavi Hey Maayaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Maayaavi Hey Maayaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Maayaavi Hey Maayaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Maayaavi Hey Maayaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Poraali Hey Poraali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Poraali Hey Poraali"/>
</div>
<div class="lyrico-lyrics-wrapper">En Por Thaan Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Por Thaan Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En Por Thaan Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Por Thaan Nee"/>
</div>
</pre>
