---
title: "vaama vaama song lyrics"
album: "Kaathadi"
artist: "R. Pavan - Deepan B"
lyricist: "Mohan Rajan"
director: "Kalyaan"
path: "/albums/kaathadi-song-lyrics"
song: "Vaama Vaama"
image: ../../images/albumart/kaathadi.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NpBj7Nsyoaw"
type: "intro song"
singers:
  - Mohanrajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vaama vaama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaama vaama "/>
</div>
<div class="lyrico-lyrics-wrapper">nethili vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethili vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">aalai thooki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalai thooki "/>
</div>
<div class="lyrico-lyrics-wrapper">kupura veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kupura veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">mongan pottu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mongan pottu "/>
</div>
<div class="lyrico-lyrics-wrapper">monjiya kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="monjiya kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">china jet ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="china jet ah "/>
</div>
<div class="lyrico-lyrics-wrapper">sound ah kootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sound ah kootum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annaka pathukunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaka pathukunu"/>
</div>
<div class="lyrico-lyrics-wrapper">andava vachukunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andava vachukunu"/>
</div>
<div class="lyrico-lyrics-wrapper">aataya than potukunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aataya than potukunu"/>
</div>
<div class="lyrico-lyrics-wrapper">antathellam suttukinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antathellam suttukinu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathadi noola pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathadi noola pola"/>
</div>
<div class="lyrico-lyrics-wrapper">kathula than aadikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathula than aadikinu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannala pathathu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala pathathu ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">thanala navatikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanala navatikinu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kallu vitha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallu vitha "/>
</div>
<div class="lyrico-lyrics-wrapper">kaasu mutaikathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu mutaikathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasu illana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu illana "/>
</div>
<div class="lyrico-lyrics-wrapper">ethuvum masakathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuvum masakathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasa pola kasa pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasa pola kasa pola"/>
</div>
<div class="lyrico-lyrics-wrapper">otha pathom athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otha pathom athu"/>
</div>
<div class="lyrico-lyrics-wrapper">kutti potu thottu poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutti potu thottu poda"/>
</div>
<div class="lyrico-lyrics-wrapper">marakathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marakathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaama vaama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaama vaama "/>
</div>
<div class="lyrico-lyrics-wrapper">nethili vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethili vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">aalai thooki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalai thooki "/>
</div>
<div class="lyrico-lyrics-wrapper">kupura veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kupura veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">mongan pottu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mongan pottu "/>
</div>
<div class="lyrico-lyrics-wrapper">monjiya kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="monjiya kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">china jet ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="china jet ah "/>
</div>
<div class="lyrico-lyrics-wrapper">sound ah kootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sound ah kootum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">annaka pathukunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaka pathukunu"/>
</div>
<div class="lyrico-lyrics-wrapper">andava vachukunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andava vachukunu"/>
</div>
<div class="lyrico-lyrics-wrapper">aataya than potukunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aataya than potukunu"/>
</div>
<div class="lyrico-lyrics-wrapper">antathellam suttukinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antathellam suttukinu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathadi noola pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathadi noola pola"/>
</div>
<div class="lyrico-lyrics-wrapper">kathula than aadikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathula than aadikinu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannala pathathu ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala pathathu ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">thanala navatikinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanala navatikinu"/>
</div>
</pre>
