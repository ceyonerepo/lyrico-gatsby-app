---
title: "karuppu perazhaga song lyrics"
album: "Kanchana-Muni2"
artist: "S. Thaman"
lyricist: "Viveka"
director: "Raghava Lawrence"
path: "/albums/kanchana-muni2-lyrics"
song: "Karuppu Perazhaga"
image: ../../images/albumart/kanchana-muni2.jpg
date: 2011-07-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/n32bBj27cHM"
type: "Love"
singers:
  - Suchith Suresan
  - Darshana KT
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Move your body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move your body"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Move your body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move your body"/>
</div>
<div class="lyrico-lyrics-wrapper">Body body body body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Body body body body"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Move your body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Move your body"/>
</div>
<div class="lyrico-lyrics-wrapper">Body body body body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Body body body body"/>
</div>
<div class="lyrico-lyrics-wrapper">Body body body body
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Body body body body"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">If u wanna come with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If u wanna come with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake it up and move with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake it up and move with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Can u just do it oh baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can u just do it oh baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets party come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets party come on"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">If u wanna come with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If u wanna come with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake it up and move with me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake it up and move with me"/>
</div>
<div class="lyrico-lyrics-wrapper">Can u just do it oh baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can u just do it oh baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa…aa…aa….aaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa…aa…aa….aaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu perazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu perazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukkulla nikkiriyae joraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkulla nikkiriyae joraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhinjiputten naaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhinjiputten naaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppu perazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu perazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukkulla nikkiriyae joraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkulla nikkiriyae joraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhinjiputten naaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhinjiputten naaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppu pola oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu pola oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Perazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyengum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyengum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kannanoda pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kannanoda pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi aathaa aathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi aathaa aathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kungumapoo mottaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kungumapoo mottaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinnupputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinnupputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungamma pethaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungamma pethaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi paartha paarthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi paartha paarthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Palapalannu irukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palapalannu irukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum paala oothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum paala oothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulikkavachaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulikkavachaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada karuppu kannaa vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada karuppu kannaa vaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaathirukken soodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaathirukken soodaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orasiputtu podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasiputtu podaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini karuppu vellaipadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini karuppu vellaipadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En sekka chekka chevappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sekka chekka chevappi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee saela kattuna gulfi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee saela kattuna gulfi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambu narambu ezhuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambu narambu ezhuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ottura pudhu padam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ottura pudhu padam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa…aa…aa….aaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa…aa…aa….aaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu perazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu perazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukkulla nikkiriyae joraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukkulla nikkiriyae joraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhinjiputten naaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhinjiputten naaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm naa hmm naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm naa hmm naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelae yelae yelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelae yelae yelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm naa hmm naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm naa hmm naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelae yelae yelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelae yelae yelae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaa…aa..aaa.haa..aaa..aaa..aaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa…aa..aaa.haa..aaa..aaa..aaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa…aa..aaa.haa..aaa..aaa..aaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa…aa..aaa.haa..aaa..aaa..aaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa…aa..aaa.haa..aaa..aaa..aaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa…aa..aaa.haa..aaa..aaa..aaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa…aa..aaa.haa..aaa..aaa..aaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa…aa..aaa.haa..aaa..aaa..aaa.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neruppu kulichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppu kulichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan neram varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan neram varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu nerandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu nerandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kavarndhidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kavarndhidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi nee kulichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi nee kulichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thuli jalamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thuli jalamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalil vizhundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalil vizhundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal veluthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal veluthidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karu megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karu megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattum thaanae boomiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattum thaanae boomiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaithoovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaithoovum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu mazha thoovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu mazha thoovum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppaana raathiriya thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppaana raathiriya thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelaa varum pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelaa varum pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhenamum varum pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhenamum varum pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi aathaa aathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi aathaa aathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennakkatti dhegathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennakkatti dhegathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakkatti izhuthu puttedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakkatti izhuthu puttedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi aathaa aathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi aathaa aathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellakkalara kaattithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellakkalara kaattithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu paiyana kavuthupputtedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu paiyana kavuthupputtedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada karuppu kannaa vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada karuppu kannaa vaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaathirukken soodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaathirukken soodaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orasiputtu podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasiputtu podaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini karuppu vellaipadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini karuppu vellaipadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En sekka chekka chevappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sekka chekka chevappi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee saela kattuna gulfi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee saela kattuna gulfi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambu narambu ezhuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambu narambu ezhuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ottura pudhu padam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ottura pudhu padam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Do you hear my
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do you hear my"/>
</div>
<div class="lyrico-lyrics-wrapper">Dilku dum dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dilku dum dum"/>
</div>
<div class="lyrico-lyrics-wrapper">It is simple
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It is simple"/>
</div>
<div class="lyrico-lyrics-wrapper">Call you come come
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Call you come come"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Do you hear my
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do you hear my"/>
</div>
<div class="lyrico-lyrics-wrapper">Dilku dum dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dilku dum dum"/>
</div>
<div class="lyrico-lyrics-wrapper">It is simple
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It is simple"/>
</div>
<div class="lyrico-lyrics-wrapper">Call you come come
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Call you come come"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My oo my naughty boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My oo my naughty boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada nee vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada nee vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Cant you just do it ippo vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cant you just do it ippo vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My oo my naughty boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My oo my naughty boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada nee vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada nee vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Cant you just do it ippo vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cant you just do it ippo vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Super staaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Super staaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thalaivan karuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thalaivan karuppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaroda rasigan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaroda rasigan"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum karuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum karuppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye super staaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye super staaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu romba veluppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu romba veluppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaroda rasigarukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaroda rasigarukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhae sirappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhae sirappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un odambellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un odambellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Macham vachi padaichiputtaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macham vachi padaichiputtaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayakkannan nee ennappanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayakkannan nee ennappanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaimela ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaimela ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanavatha erakki vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavatha erakki vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Macham varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macham varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukkul saami varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukkul saami varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada karuppaa karuppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada karuppaa karuppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannakkatti mayakkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannakkatti mayakkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu mannu onnum puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu mannu onnum puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi karuppum veluppum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi karuppum veluppum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottikkittu ninnuttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottikkittu ninnuttaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Blacken white-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blacken white-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu paarupulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu paarupulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada karuppu kannaa vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada karuppu kannaa vaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaathirukken soodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaathirukken soodaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orasiputtu podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasiputtu podaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini karuppu vellaipadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini karuppu vellaipadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En sekka chekka chevappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sekka chekka chevappi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee saela kattuna gulfi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee saela kattuna gulfi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambu narambu ezhuppi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambu narambu ezhuppi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ottura pudhu padam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ottura pudhu padam"/>
</div>
</pre>
