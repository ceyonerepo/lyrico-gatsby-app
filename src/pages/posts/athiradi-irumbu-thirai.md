---
title: "athiradi song lyrics"
album: "Irumbu Thirai"
artist: "Yuvan Shankar Raja"
lyricist: "Vivek"
director: "P.S. Mithran"
path: "/albums/irumbu-thirai-song-lyrics"
song: "Athiradi"
image: ../../images/albumart/irumbu-thirai.jpg
date: 2018-05-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gDanRKv3qls"
type: "mass"
singers:
  - Naveen
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Ready Athiradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ready Athiradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi ivan pidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi ivan pidi"/>
</div>
<div class="lyrico-lyrics-wrapper">Salute dhinam adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salute dhinam adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aratavaraan meratavaraan unnathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aratavaraan meratavaraan unnathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudi senji mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudi senji mudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa sethu madi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa sethu madi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poratavaaran ayyayyayayo vanthuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poratavaaran ayyayyayayo vanthuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallu velakka bullet kudupaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallu velakka bullet kudupaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasikidhuna landmine thattil vaipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasikidhuna landmine thattil vaipaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishathila padhara vaipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishathila padhara vaipaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi nodi vedi vedi vedipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi nodi vedi vedi vedipaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukulla alavedupaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukulla alavedupaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhakathula paadhi maathi vaipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhakathula paadhi maathi vaipaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemadharuman pola irupaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemadharuman pola irupaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu manushana unna padaipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu manushana unna padaipaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Ready Athiradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ready Athiradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi ivan pidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi ivan pidi"/>
</div>
<div class="lyrico-lyrics-wrapper">Salute dhinam adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salute dhinam adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aratavaraan meratavaraan unnathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aratavaraan meratavaraan unnathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thuninji ninna armour-a kudupaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninji ninna armour-a kudupaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayandhu ninna torture kudupaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayandhu ninna torture kudupaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathula border-a idipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathula border-a idipaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Surusurupa sozhalavittu paapaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surusurupa sozhalavittu paapaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppa tired ah sariyira nilaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppa tired ah sariyira nilaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Appo timer-ah on panni tholaipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo timer-ah on panni tholaipaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppo marandhu vali theriyadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo marandhu vali theriyadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvaraikum udayavaipaan siripaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvaraikum udayavaipaan siripaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasama tankeril yethuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasama tankeril yethuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">On your mark
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On your mark"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavuku Sudden visit adikavaipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavuku Sudden visit adikavaipaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Ready Athiradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ready Athiradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idiivan pidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idiivan pidi"/>
</div>
<div class="lyrico-lyrics-wrapper">Salute dhinam adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salute dhinam adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aratavaraan meratavaraan unnathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aratavaraan meratavaraan unnathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Udan adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudi senji mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudi senji mudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa sethu madi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa sethu madi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poratavaaran ayyayyayayo vanthuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poratavaaran ayyayyayayo vanthuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanmurai pazhi ennai serattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanmurai pazhi ennai serattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanmurai pazhi ennai serattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanmurai pazhi ennai serattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththam sadhai vazhi odattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththam sadhai vazhi odattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththam sadhai vazhi odattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththam sadhai vazhi odattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennagattum yedhagattum povom povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennagattum yedhagattum povom povom"/>
</div>
<div class="lyrico-lyrics-wrapper">Povom povom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povom povom"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaal varum pinbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaal varum pinbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vaazhvom vaazhvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir vaazhvom vaazhvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvom vaazhvoom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvom vaazhvoom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saavai dhinam dhinam yerkindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavai dhinam dhinam yerkindrom"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavai dhinam dhinam yerkindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavai dhinam dhinam yerkindrom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayangum maranamum varavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayangum maranamum varavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayangum maranamum varavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayangum maranamum varavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasukulla alavedupaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukulla alavedupaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhakathula paadhi maathi vaipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhakathula paadhi maathi vaipaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemadharuman pola irupaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemadharuman pola irupaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu manushana unna padaipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu manushana unna padaipaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Ready Athiradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ready Athiradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi ivan pidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi ivan pidi"/>
</div>
<div class="lyrico-lyrics-wrapper">Salute dhinam adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salute dhinam adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aratavaraan meratavaraan unnathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aratavaraan meratavaraan unnathaan"/>
</div>
</pre>
