---
title: "tum tum song lyrics"
album: "Enemy"
artist: "S. Thaman"
lyricist: "Vivek"
director: "Anand Shankar"
path: "/albums/enemy-lyrics"
song: "Tum Tum"
image: ../../images/albumart/enemy.jpg
date: 2021-11-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tYSrY4iPX6w"
type: "happy"
singers:
  - Sri Vardhini
  - Aditi
  - Satya Yamini
  - Roshini
  - Tejaswini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manaso ippo thandhiyadikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manaso ippo thandhiyadikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaman nadaikku mathala tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman nadaikku mathala tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathala tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathala tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathala tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathala tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirippo illai minnal adikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirippo illai minnal adikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai ponnukku atchadha tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai ponnukku atchadha tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Atchadha tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atchadha tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Atchadha tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atchadha tum tum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusa oru vetkam molaikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusa oru vetkam molaikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudicha oru veppam adikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudicha oru veppam adikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti onnu selaiyathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetti onnu selaiyathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti kittu sikki thavikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti kittu sikki thavikkidhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjara tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjara tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathu adikka mangala tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathu adikka mangala tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ola tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhukku tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhukku tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ongi thattikkum oththiga tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ongi thattikkum oththiga tum tum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjara tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjara tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathu adikka mangala tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathu adikka mangala tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ola tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhukku tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhukku tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ongi thattikkum oththiga tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ongi thattikkum oththiga tum tum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manaso ippo thandhiyadikkidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manaso ippo thandhiyadikkidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaman nadaikku mathala tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman nadaikku mathala tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathala tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathala tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathala tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathala tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ola tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ola tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhukku tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhukku tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ongi thattikkum oththiga tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ongi thattikkum oththiga tum tum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum vandha nalla neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum vandha nalla neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattena aarambam aagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattena aarambam aagumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kai pudicha indha naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kai pudicha indha naalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandiga thaedhiyil serumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandiga thaedhiyil serumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asainja vandhu nithira kedukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asainja vandhu nithira kedukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sithana koyilu sithiramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithana koyilu sithiramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenappa vandhu sakkaram suththura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenappa vandhu sakkaram suththura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththara kottuna rathinamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththara kottuna rathinamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazha sezhikka vaakku palikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha sezhikka vaakku palikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangum sandhanam vaasam kozhaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangum sandhanam vaasam kozhaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettu kizhikka koottam kudhikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettu kizhikka koottam kudhikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam rendum pattunu ottika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam rendum pattunu ottika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjara tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjara tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathu adikka mangala tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathu adikka mangala tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ola tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhukku tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhukku tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ongi thattikkum oththiga tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ongi thattikkum oththiga tum tum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjara tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjara tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathu adikka mangala tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathu adikka mangala tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ola tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ola tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhukku tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhukku tum tum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ongi thattikkum oththiga tum tum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ongi thattikkum oththiga tum tum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaaa"/>
</div>
</pre>
