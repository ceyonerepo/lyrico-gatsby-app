---
title: "chinna kabali song lyrics"
album: "Shivalinga"
artist: "S S Thaman"
lyricist: "Viveka"
director: "P Vasu"
path: "/albums/shivalinga-lyrics"
song: "Chinna Kabali"
image: ../../images/albumart/shivalinga.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WF-pndanUjw"
type: "mass"
singers:
  -	Shankar Mahadevan
  - Naveen
  - Arunraja Kamaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Alluvutta Appeatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alluvutta Appeatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dillurundha Repeatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dillurundha Repeatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dalladikkum Nerathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dalladikkum Nerathila"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan Nippattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Nippattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allivitta Getitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allivitta Getitu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallivitta Ticketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallivitta Ticketu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolluvitta Mattikuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolluvitta Mattikuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan Wicketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Wicketu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gillikkuthaan Salute Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gillikkuthaan Salute Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Solliadi Rotate Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solliadi Rotate Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parapatcham Pakka Mattan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parapatcham Pakka Mattan"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Kabali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Kabali"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Kabali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Kabali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pattali Pattali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pattali Pattali"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhaikkellam Kottali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhaikkellam Kottali"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellamanan Ullavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellamanan Ullavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Kabali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Kabali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samali Samali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samali Samali"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhilirundha Samali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhilirundha Samali"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandu Ninna Andha Yedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandu Ninna Andha Yedam"/>
</div>
<div class="lyrico-lyrics-wrapper">Double Diwali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Double Diwali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhagunaa Uruguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagunaa Uruguven"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasathukku Kalanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasathukku Kalanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagaya Valakka Venam Pangali	
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagaya Valakka Venam Pangali	"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odambuthan Karuppuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambuthan Karuppuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Orasipparu Neruppuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orasipparu Neruppuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjamattan Chinna Kabali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjamattan Chinna Kabali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Gumma Gumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Gumma Gumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummanguthu Kuttidalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummanguthu Kuttidalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Dappa Dappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Dappa Dappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappanguthu Adidalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappanguthu Adidalamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Gumma Gumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Gumma Gumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummanguthu Kuttidalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummanguthu Kuttidalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Dappa Dappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Dappa Dappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappanguthu Adidalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappanguthu Adidalamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Kabali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Kabali"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Kabali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Kabali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alluvutta Appeatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alluvutta Appeatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dillurundha Repeatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dillurundha Repeatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dalladikkum Nerathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dalladikkum Nerathila"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan Nippattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Nippattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pattali Pattali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pattali Pattali"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhaikkellam Kottali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhaikkellam Kottali"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellamanan Ullavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellamanan Ullavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Kabali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Kabali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samali Samali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samali Samali"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhilirundha Samali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhilirundha Samali"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandu Ninna Andha Yedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandu Ninna Andha Yedam"/>
</div>
<div class="lyrico-lyrics-wrapper">Double Diwali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Double Diwali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therasa Pol Ayulellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therasa Pol Ayulellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thyagiyaga Vazhavendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thyagiyaga Vazhavendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirusa Orr Orathula Sevai Seiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirusa Orr Orathula Sevai Seiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paaripol Vaarithara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaripol Vaarithara"/>
</div>
<div class="lyrico-lyrics-wrapper">Budget Anga Pathalenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budget Anga Pathalenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasiaarum Idam Enge Kaatividudda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiaarum Idam Enge Kaatividudda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirurokkam Koduthathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirurokkam Koduthathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Pakka Vilambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Pakka Vilambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nazhithalil Neeyum Tharaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazhithalil Neeyum Tharaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhaikagalin Kannoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaikagalin Kannoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Parkkindra Santhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parkkindra Santhosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola Oru Inbam Varathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Oru Inbam Varathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Gumma Gumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Gumma Gumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummanguthu Kuttidalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummanguthu Kuttidalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Dappa Dappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Dappa Dappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappanguthu Adidalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappanguthu Adidalamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Gumma Gumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Gumma Gumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummanguthu Kuttidalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummanguthu Kuttidalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Dappa Dappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Dappa Dappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappanguthu Adidalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappanguthu Adidalamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pattali Pattali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pattali Pattali"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhaikkellam Kottali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhaikkellam Kottali"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellamanan Ullavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellamanan Ullavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Kabali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Kabali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samali Samali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samali Samali"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhilirundha Samali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhilirundha Samali"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandu Ninna Andha Yedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandu Ninna Andha Yedam"/>
</div>
<div class="lyrico-lyrics-wrapper">Double Diwali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Double Diwali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alluvutta Appeatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alluvutta Appeatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dillurundha Repeatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dillurundha Repeatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dalladikkum Nerathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dalladikkum Nerathila"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan Nippattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Nippattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Allivitta Getitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allivitta Getitu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallivitta Ticketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallivitta Ticketu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolluvitta Mattikuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolluvitta Mattikuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan Wicketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Wicketu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vettu Kettaithaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vettu Kettaithaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru Morai Yosichittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Morai Yosichittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bill Gates Su Agalennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bill Gates Su Agalennu"/>
</div>
<div class="lyrico-lyrics-wrapper">Feeling Aagathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling Aagathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palgova Kadikkave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palgova Kadikkave"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallusettu Venumunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallusettu Venumunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Barfiya Vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barfiya Vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettil Seththu Vekkathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettil Seththu Vekkathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeraikkatha Kenarellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeraikkatha Kenarellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorakkathu Solvaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorakkathu Solvaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhavi Na Odu Munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhavi Na Odu Munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enna Valakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enna Valakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Anba Kodukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Anba Kodukkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuthaanda Chinna Kabali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuthaanda Chinna Kabali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Gumma Gumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Gumma Gumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummanguthu Kuttidalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummanguthu Kuttidalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Dappa Dappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Dappa Dappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappanguthu Adidalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappanguthu Adidalamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Gumma Gumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Gumma Gumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Gummanguthu Kuttidalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gummanguthu Kuttidalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Dappa Dappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Dappa Dappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappanguthu Adidalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappanguthu Adidalamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pattali Pattali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pattali Pattali"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhaikkellam Kottali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhaikkellam Kottali"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellamanan Ullavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellamanan Ullavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Kabali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Kabali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samali Samali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samali Samali"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhilirundha Samali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhilirundha Samali"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandu Ninna Andha Yedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandu Ninna Andha Yedam"/>
</div>
<div class="lyrico-lyrics-wrapper">Double Diwali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Double Diwali"/>
</div>
</pre>
