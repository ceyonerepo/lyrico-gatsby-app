---
title: "sila naal karuvil song lyrics"
album: "Kodiyil Oruvan"
artist: "	Nivas K. Prasanna - Harish arjun"
lyricist: "Mohan Rajan"
director: "Ananda Krishnan"
path: "/albums/kodiyil-oruvan-lyrics"
song: "Sila Naal Karuvil"
image: ../../images/albumart/kodiyil-oruvan.jpg
date: 2021-09-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TWoJ4CkQvG4"
type: "melody"
singers:
  - Sukanya Varadarajan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sila Naal Karuvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Naal Karuvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Naal Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Naal Kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai Unai Naan Sumandhen Magane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Unai Naan Sumandhen Magane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Muzhudhum Edhirai Thirumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Muzhudhum Edhirai Thirumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkai Vazhigal Sumandhen Magane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkai Vazhigal Sumandhen Magane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aram Nee Pazhagu Adhu Thaan Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aram Nee Pazhagu Adhu Thaan Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyaram Kadanthal Uyaram Unadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyaram Kadanthal Uyaram Unadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Nee Jeikka Mudhalil Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Nee Jeikka Mudhalil Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavai Kadakka Adhuve Padagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavai Kadakka Adhuve Padagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila Naal Karuvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Naal Karuvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Naal Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Naal Kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai Unai Naan Sumandhen Magane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Unai Naan Sumandhen Magane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadaigal Unakkai Pudhithai Kilambum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigal Unakkai Pudhithai Kilambum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadikkal Adhu Than Padikkal Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadikkal Adhu Than Padikkal Irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunichal Irundhal Edhuvum Urangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunichal Irundhal Edhuvum Urangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila Sol Unaiye Silayai Sedhukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Sol Unaiye Silayai Sedhukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Naal Ulagam Unakkai Thirumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Ulagam Unakkai Thirumbum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila Naal Karuvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Naal Karuvil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Naal Kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Naal Kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai Unai Naan Sumandhen Magane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Unai Naan Sumandhen Magane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Muzhudhum Edhirai Thirumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Muzhudhum Edhirai Thirumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkai Vazhigal Sumandhen Magane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkai Vazhigal Sumandhen Magane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aram Nee Pazhagu Adhu Thaan Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aram Nee Pazhagu Adhu Thaan Azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyaram Kadandhal Uyaram Unadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyaram Kadandhal Uyaram Unadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Nee Jeikka Mudhalil Pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Nee Jeikka Mudhalil Pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavai Kadakka Adhuve Padagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavai Kadakka Adhuve Padagu"/>
</div>
</pre>
