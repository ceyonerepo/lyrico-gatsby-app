---
title: "omg daddy song lyrics"
album: "Ala Vaikunthapurramuloo"
artist: "S. Thaman"
lyricist: "Krishna Chaitanya"
director: "Trivikram Srinivas"
path: "/albums/ala-vaikunthapurramuloo-lyrics"
song: "OMG Daddy"
image: ../../images/albumart/ala-vaikunthapurramuloo.jpg
date: 2020-01-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/gdt-yIgm2zc"
type: "happy"
singers:
  - Rahul Sipligunj
  - Roll Rida
  - Blaaze
  - Lady Kash
  - Rahul Nambiar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">La la la laa la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la la laa la"/>
</div>
<div class="lyrico-lyrics-wrapper">La la la la laa la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la la la laa la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa story cheppalenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa story cheppalenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa bhaadhakanthu ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa bhaadhakanthu ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee daddy landhurandaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee daddy landhurandaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhukitta peekkuthintunnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhukitta peekkuthintunnaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Maata vinipinchukoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maata vinipinchukoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Assaslu artham chesukoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Assaslu artham chesukoru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalochinsthene NANNA peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalochinsthene NANNA peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaluthundi naa hairuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaluthundi naa hairuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandharoopaylivvamante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandharoopaylivvamante"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamemanna rich ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamemanna rich ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani class peekuthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani class peekuthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Emaina picchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaina picchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukuntu yedchukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukuntu yedchukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu baitikocchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu baitikocchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andharinta same scene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharinta same scene"/>
</div>
<div class="lyrico-lyrics-wrapper">Emantaav chicchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emantaav chicchaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My GOD Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My GOD Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Just stop being my baddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just stop being my baddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My GOD Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My GOD Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Just stop being my baddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just stop being my baddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My GOD Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My GOD Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Just stop being my baddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just stop being my baddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t be so hardy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t be so hardy"/>
</div>
<div class="lyrico-lyrics-wrapper">That will make me saddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That will make me saddy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mera nam bantu kaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera nam bantu kaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Peruki kottaare intu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peruki kottaare intu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaar sau bees daddy thone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaar sau bees daddy thone"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesa ney fight day and night-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesa ney fight day and night-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh Ammaki mogudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh Ammaki mogudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh Naannainaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh Naannainaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varshanni o chitti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varshanni o chitti"/>
</div>
<div class="lyrico-lyrics-wrapper">Bottle lo nimpalevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottle lo nimpalevu"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhoshaanni kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhoshaanni kutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu uniform veyyalevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu uniform veyyalevu"/>
</div>
<div class="lyrico-lyrics-wrapper">Swechakemi shortcut
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swechakemi shortcut"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanipettaledhu ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanipettaledhu ottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhante nannu thittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhante nannu thittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledha na jattu kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledha na jattu kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adivemo backyard lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adivemo backyard lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettalevu maccha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettalevu maccha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavuraanni paper weight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavuraanni paper weight"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyalevu piccha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyalevu piccha"/>
</div>
<div class="lyrico-lyrics-wrapper">Volcanotho chali mante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Volcanotho chali mante"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyyalevu chiccha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyyalevu chiccha"/>
</div>
<div class="lyrico-lyrics-wrapper">Blank cheque raa manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blank cheque raa manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppi mari vacchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppi mari vacchaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">He isn’t always right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He isn’t always right"/>
</div>
<div class="lyrico-lyrics-wrapper">Spy daddy spy daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spy daddy spy daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">He isn’t always right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He isn’t always right"/>
</div>
<div class="lyrico-lyrics-wrapper">Spy daddy spy daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spy daddy spy daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Spy daddy spy daddy spy daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spy daddy spy daddy spy daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Spy daddy spy daddy spy daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spy daddy spy daddy spy daddy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Son of valmiki ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Son of valmiki ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Care of kasthalunnatte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Care of kasthalunnatte"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee intlo navvalante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee intlo navvalante"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanos chitikeyyalanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanos chitikeyyalanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh mommy mogudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh mommy mogudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh dummy gaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh dummy gaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My GOD Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My GOD Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Just stop being my baddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just stop being my baddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My GOD Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My GOD Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Just stop being my baddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just stop being my baddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My GOD Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My GOD Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Just stop being my baddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just stop being my baddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t be so hardy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t be so hardy"/>
</div>
<div class="lyrico-lyrics-wrapper">That will make me saddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That will make me saddy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">La la la laa la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la la laa la"/>
</div>
<div class="lyrico-lyrics-wrapper">La la la la laa la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la la la laa la"/>
</div>
<div class="lyrico-lyrics-wrapper">La la la laa la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la la laa la"/>
</div>
<div class="lyrico-lyrics-wrapper">La la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La la la la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">He isn’t always right
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He isn’t always right"/>
</div>
</pre>
