---
title: "chocklet song lyrics"
album: "Unnai Ninaithu"
artist: "Sirpy"
lyricist: "Na. Muthukumar"
director: "Vikraman"
path: "/albums/unnai-ninaithu-lyrics"
song: "Chocklet"
image: ../../images/albumart/unnai-ninaithu.jpg
date: 2002-05-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QDauRF2Xkus"
type: "mass"
singers:
  - Tippu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chocolate chocolate polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chocolate chocolate polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh taste-u taste-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh taste-u taste-u da"/>
</div>
<div class="lyrico-lyrics-wrapper">Chocolate chocolate polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chocolate chocolate polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh taste-u taste-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh taste-u taste-u da"/>
</div>
<div class="lyrico-lyrics-wrapper">Love birds love birds parakkavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love birds love birds parakkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">L board L board edhukku da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="L board L board edhukku da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pookkalin sirippukkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkalin sirippukkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Make-up thevaiyillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make-up thevaiyillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindukkal poottedhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindukkal poottedhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Teen agekku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teen agekku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chocolate chocolate polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chocolate chocolate polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh taste-u taste-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh taste-u taste-u da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saettu kadaiyil vatti illaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saettu kadaiyil vatti illaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanks solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanks solluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadha figure annannu sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadha figure annannu sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanks solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanks solluvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotta illaamal dhosai sudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta illaamal dhosai sudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummykku thanks solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummykku thanks solluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Bata kadaiyil anju paisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bata kadaiyil anju paisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Balance thandha thanks solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Balance thandha thanks solluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">One way la poga sonna thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One way la poga sonna thank you"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunday la thoonga sonna thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunday la thoonga sonna thank you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chocolate chocolate polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chocolate chocolate polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh taste-u taste-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh taste-u taste-u da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Quarter vangaiyil kosuru thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Quarter vangaiyil kosuru thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanks solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanks solluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Bachelor-ku veedu koduthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bachelor-ku veedu koduthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanks solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanks solluvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinathandhi kanni theevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinathandhi kanni theevu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppa mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppa mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Therinchutta thanks solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therinchutta thanks solluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Tea kadai seat mela kaalai vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tea kadai seat mela kaalai vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittalainna thanks solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittalainna thanks solluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam life la solli paaru thank you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam life la solli paaru thank you"/>
</div>
<div class="lyrico-lyrics-wrapper">Un edhiriyum solluvaan i like you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un edhiriyum solluvaan i like you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chocolate chocolate polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chocolate chocolate polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh taste-u taste-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh taste-u taste-u da"/>
</div>
<div class="lyrico-lyrics-wrapper">Love birds love birds parakkavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love birds love birds parakkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">L board L board edhukku da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="L board L board edhukku da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pookkalin sirippukkellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkalin sirippukkellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Make-up thevaiyillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make-up thevaiyillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindukkal poottedhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindukkal poottedhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Teen agekku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teen agekku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chocolate chocolate polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chocolate chocolate polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh taste-u taste-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh taste-u taste-u da"/>
</div>
</pre>
