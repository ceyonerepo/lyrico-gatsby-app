---
title: "oru mara nizhalil song lyrics"
album: "Mercury"
artist: "Santhosh Narayanan"
lyricist: "Vivek"
director: "Karthik Subbaraj"
path: "/albums/mercury-song-lyrics"
song: "Oru Mara Nizhalil"
image: ../../images/albumart/mercury.jpg
date: 2018-04-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/e_19pybfVxc"
type: "love"
singers:
  - Sathyaprakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru mara nizhalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mara nizhalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum irunthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum irunthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnooru varusham kaluchi thirumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnooru varusham kaluchi thirumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kannu kalandhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kannu kalandhom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un moochi kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un moochi kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechi thaannu kurippudethu kedanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechi thaannu kurippudethu kedanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vagadu varainja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vagadu varainja"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhai yeri iruttu kulla tholainjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai yeri iruttu kulla tholainjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru mara nizhalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mara nizhalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum irunthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum irunthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaahaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaaahaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaahaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaaahaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma thanimai kalaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma thanimai kalaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu illamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu illamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu othungi pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu othungi pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha nelavu ilukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha nelavu ilukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Parantha paravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parantha paravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodu thirumbiyaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu thirumbiyaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma konji kolavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma konji kolavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththatha ottu kekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththatha ottu kekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Elaiya othukkum kele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaiya othukkum kele"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kaadhal alli thinnuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kaadhal alli thinnuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham kootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham kootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Poova kakkum chediyaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poova kakkum chediyaeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaahaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaaahaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaahaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaaahaaaaa"/>
</div>
</pre>
