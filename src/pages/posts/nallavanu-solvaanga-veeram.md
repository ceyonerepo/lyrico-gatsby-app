---
title: "nallavannu solvaanga song lyrics"
album: "Veeram"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Siva"
path: "/albums/veeram-lyrics"
song: "Nallavannu solvaanga"
image: ../../images/albumart/veeram.jpg
date: 2014-01-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Gz9LWy_Bwww"
type: "Mass Entry"
singers:
  - Devi Sri Prasad
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">People Wanna Scream
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="People Wanna Scream"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">All The People Wanna Shout
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All The People Wanna Shout"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">All The People Wanna Cheer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All The People Wanna Cheer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">All The People In The House
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All The People In The House"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Here Comes The Man With The New Style
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Here Comes The Man With The New Style"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Get Up He Is Gonna Draw Everyone Wow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get Up He Is Gonna Draw Everyone Wow"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Get Up Lets Get Up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get Up Lets Get Up"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallavannu Solvaanga Nambidathinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallavannu Solvaanga Nambidathinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engala Kettavanu Sonnalum Thittidathinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engala Kettavanu Sonnalum Thittidathinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thangamunu Sonnaalum Orasidhathinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thangamunu Sonnaalum Orasidhathinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engala Thagaram Nu Sonnalum Pagaichidathinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engala Thagaram Nu Sonnalum Pagaichidathinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattaiyila Rendu Button Avundhu Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattaiyila Rendu Button Avundhu Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooril Sandai Naa Engaloda Soundu Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooril Sandai Naa Engaloda Soundu Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethiri Oda Odambula Thaan Kaayam Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiri Oda Odambula Thaan Kaayam Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada EpoME Enga Pakkam Gniyayam Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada EpoME Enga Pakkam Gniyayam Irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
12345 <div class="lyrico-lyrics-wrapper">Vena Enga Kitta Wrong Side Drive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena Enga Kitta Wrong Side Drive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
12345 <div class="lyrico-lyrics-wrapper">Vena Enga Kitta Wrong Side Dirve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena Enga Kitta Wrong Side Dirve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Manam Manam Manam Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Manam Manam Manam Vella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karai karai Karai Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karai karai Karai Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edakka Madakaa Thodukka Kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edakka Madakaa Thodukka Kodukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudukka Adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudukka Adippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yes"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Pala Pala Vetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Pala Pala Vetti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pada Padakura Ghosti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada Padakura Ghosti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uthava Mattum Vekkam Maanam Pakka Maatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthava Mattum Vekkam Maanam Pakka Maatom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engooru Vanthu Paruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engooru Vanthu Paruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athil Tentukotta Joruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Tentukotta Joruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru Padam Odinaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Padam Odinaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hero Anga Naangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Anga Naangada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
12345 <div class="lyrico-lyrics-wrapper">Vena Enga Kitta Wrong Side Drive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena Enga Kitta Wrong Side Drive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
12345 <div class="lyrico-lyrics-wrapper">Vena Enga Kitta Wrong Side Dirve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena Enga Kitta Wrong Side Dirve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Maatineengada Inakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Maatineengada Inakki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyo Anna Thorathuraaru Odu Odu Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Anna Thorathuraaru Odu Odu Odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venam Da Thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam Da Thambi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonna Kelu Venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna Kelu Venam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bale Bale Bale Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bale Bale Bale Kootam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagal Iravugal Aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal Iravugal Aattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanaga Payala Pananga Kanaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanaga Payala Pananga Kanaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nongu Eduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nongu Eduppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Siru Siru settai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Siru Siru settai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viru Viru Viru Vettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viru Viru Viru Vettai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasiyil Adippom Alaiyil Thudupa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasiyil Adippom Alaiyil Thudupa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uruvam Edupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvam Edupom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aruntha Vaalu Pasanga Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruntha Vaalu Pasanga Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aana Karantha Paalu Manasu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Karantha Paalu Manasu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakka Vella Kokku Karuppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakka Vella Kokku Karuppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Sonna Kelu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Sonna Kelu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
12345 <div class="lyrico-lyrics-wrapper">Vena Enga Kitta Wrong Side Drive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena Enga Kitta Wrong Side Drive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
12345 <div class="lyrico-lyrics-wrapper">Vena Enga Kitta Wrong Side Dirve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena Enga Kitta Wrong Side Dirve"/>
</div>
</pre>
