---
title: "mannurunda song lyrics"
album: "Soorarai Pottru"
artist: "G V Prakash Kumar"
lyricist: "K Ekadesi"
director: "Sudha Kongara"
path: "/albums/soorarai-pottru-song-lyrics"
song: "Mannurunda"
image: ../../images/albumart/soorarai-potru.jpg
date: 2020-11-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tXE1ipZKsGI"
type: "message"
singers:
  - Senthil Ganesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
    <div class="lyrico-lyrics-wrapper">Mannu urunda mela…
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannu urunda mela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannu urunda mela…
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannu urunda mela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusapaya aatam paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Manusapaya aatam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aah aaah aatam paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Aah aaah aatam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh yeh aatam paaru aatam paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeh yeh aatam paaru aatam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatam aatam aatam aatam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aatam aatam aatam aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatam aatam aatam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aatam aatam aatam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mannu urunda mela
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannu urunda mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga manusapaya aatam paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Inga manusapaya aatam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu renda moodi putta
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannu renda moodi putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedhiyila pogum thaeru
<input type="checkbox" class="lyrico-select-lyric-line" value="Veedhiyila pogum thaeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andaavula kondu vandhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Andaavula kondu vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saarayatha oothu
<input type="checkbox" class="lyrico-select-lyric-line" value="Saarayatha oothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyaavoda oorvalathula
<input type="checkbox" class="lyrico-select-lyric-line" value="Ayyaavoda oorvalathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadungada koothu
<input type="checkbox" class="lyrico-select-lyric-line" value="Aadungada koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ezhai panakkaaran inga
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhai panakkaaran inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam onnu pangu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellam onnu pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisiyila manushanukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadaisiyila manushanukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodhuvainga sangu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oodhuvainga sangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ezhai panakkaaran inga
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhai panakkaaran inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam onnu pangu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellam onnu pangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisiyila manushanukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadaisiyila manushanukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodhuvainga sangu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oodhuvainga sangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Danakku dakkaaan dakka dakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Danakku dakkaaan dakka dakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Danakku dakkaan dakkaan taa (4 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Danakku dakkaan dakkaan taa"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dinn dinn dakk dakk
<input type="checkbox" class="lyrico-select-lyric-line" value="Dinn dinn dakk dakk"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinn dinn dakk dakk
<input type="checkbox" class="lyrico-select-lyric-line" value="Dinn dinn dakk dakk"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinn dinn dakk dakk
<input type="checkbox" class="lyrico-select-lyric-line" value="Dinn dinn dakk dakk"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinndakk dinndakk dinndakk dinndakk
<input type="checkbox" class="lyrico-select-lyric-line" value="Dinndakk dinndakk dinndakk dinndakk"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nethi kaasu ottha roova
<input type="checkbox" class="lyrico-select-lyric-line" value="Nethi kaasu ottha roova"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda varum sothu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kooda varum sothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey ottha roova aah aah aah
<input type="checkbox" class="lyrico-select-lyric-line" value="Ey ottha roova aah aah aah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottha roova ey ey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ottha roova ey ey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottha roova ottha roova
<input type="checkbox" class="lyrico-select-lyric-line" value="Ottha roova ottha roova"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottha ottha ottha ottha
<input type="checkbox" class="lyrico-select-lyric-line" value="Ottha ottha ottha ottha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottha ottha ottha
<input type="checkbox" class="lyrico-select-lyric-line" value="Ottha ottha ottha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nethi kaasu ottha roova
<input type="checkbox" class="lyrico-select-lyric-line" value="Nethi kaasu ottha roova"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda varum sothu thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kooda varum sothu thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethavarum serndhu aada
<input type="checkbox" class="lyrico-select-lyric-line" value="Sethavarum serndhu aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi pottu kuthuvomae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaangi pottu kuthuvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saarayam kudichavanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Saarayam kudichavanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesti avulnthu vizhlumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vesti avulnthu vizhlumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudam udaikkum idam varaikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kudam udaikkum idam varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pombalainga azhumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pombalainga azhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aayiram peru irundhaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Aayiram peru irundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda yaarum varala da
<input type="checkbox" class="lyrico-select-lyric-line" value="Kooda yaarum varala da"/>
</div>
<div class="lyrico-lyrics-wrapper">Adukki maadi veedu irundhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Adukki maadi veedu irundhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaradi thaan meiyaada
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaradi thaan meiyaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aayiram peru irundhaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Aayiram peru irundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda yaarum varala da
<input type="checkbox" class="lyrico-select-lyric-line" value="Kooda yaarum varala da"/>
</div>
<div class="lyrico-lyrics-wrapper">Adukki maadi veedu irundhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Adukki maadi veedu irundhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaradi thaan meiyaada
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaradi thaan meiyaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Damukku dappaan dappan dappan
<input type="checkbox" class="lyrico-select-lyric-line" value="Damukku dappaan dappan dappan"/>
</div>
<div class="lyrico-lyrics-wrapper">Damukku dappaan dappan da (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Damukku dappaan dappan da"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Keezh saadhi udambukula…eeh
<input type="checkbox" class="lyrico-select-lyric-line" value="Keezh saadhi udambukula…eeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Keezh saadhi udambukulla…
<input type="checkbox" class="lyrico-select-lyric-line" value="Keezh saadhi udambukulla…"/>
</div>
<div class="lyrico-lyrics-wrapper">Oduradhu sakkadaiya?
<input type="checkbox" class="lyrico-select-lyric-line" value="Oduradhu sakkadaiya?"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya oduradhu sakkadaiya?
<input type="checkbox" class="lyrico-select-lyric-line" value="Ayya oduradhu sakkadaiya?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andha mel saadhi kaaranukku….uu..
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha mel saadhi kaaranukku….uu.."/>
</div>
<div class="lyrico-lyrics-wrapper">Andha mel saadhi kaaranukku…
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha mel saadhi kaaranukku…"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu kombu irundhaa aah aah
<input type="checkbox" class="lyrico-select-lyric-line" value="Rendu kombu irundhaa aah aah"/>
</div>
<div class="lyrico-lyrics-wrapper">Kombu irundhaa ey ey
<input type="checkbox" class="lyrico-select-lyric-line" value="Kombu irundhaa ey ey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kombu irundhaa kombu irundhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kombu irundhaa kombu irundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kombu kombu kombu kombu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kombu kombu kombu kombu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kombu kombu kombae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kombu kombu kombae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Keezh saadhi udambukulla…
<input type="checkbox" class="lyrico-select-lyric-line" value="Keezh saadhi udambukulla…"/>
</div>
<div class="lyrico-lyrics-wrapper">Oduradhu sakkadaiya?
<input type="checkbox" class="lyrico-select-lyric-line" value="Oduradhu sakkadaiya?"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha mel saadhi kaaranukku….
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha mel saadhi kaaranukku…."/>
</div>
<div class="lyrico-lyrics-wrapper">Kombu irundhaa kaattungaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kombu irundhaa kaattungaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaikura koottam ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Uzhaikura koottam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Keezh saadhi manushangalaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Keezh saadhi manushangalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkandhu thinguravenellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Ukkandhu thinguravenellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mel saadhi vamsangalaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Mel saadhi vamsangalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennangada naadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennangada naadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada saadhiya thooki podu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada saadhiya thooki podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ennangada naadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada ennangada naadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada saadhiya podhachi moodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada saadhiya podhachi moodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennangada naadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennangada naadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada saadhiya thooki podu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada saadhiya thooki podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ennangada naadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada ennangada naadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada saadhiya podhachi moodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada saadhiya podhachi moodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Danakku dakkaaan dakka dakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Danakku dakkaaan dakka dakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Danakku dakkaan dakkaan taa (4 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Danakku dakkaan dakkaan taa"></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dinndakk dinndakk dinndakk dinndakk
<input type="checkbox" class="lyrico-select-lyric-line" value="Dinndakk dinndakk dinndakk dinndakk"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinndakk dinndakk darrr nakkkaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Dinndakk dinndakk darrr nakkkaa"/>
</div>
</pre>
