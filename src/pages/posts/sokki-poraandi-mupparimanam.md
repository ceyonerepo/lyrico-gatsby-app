---
title: "sokki poraandi song lyrics"
album: "Mupparimanam"
artist: "G V Prakash Kumar"
lyricist: "Na Muthu Kumar"
director: "Adhiroopan"
path: "/albums/mupparimanam-lyrics"
song: "Sokki Poraandi"
image: ../../images/albumart/mupparimanam.jpg
date: 2017-03-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0S0r-Ph_LPk"
type: "love"
singers:
  - Al Rufian
  - Maalavika Sundar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sokki poraan di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokki poraan di"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekki poraan di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekki poraan di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannukul vizundhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannukul vizundhanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru dharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru dharam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Checkachevaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Checkachevaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetka padum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetka padum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannathai kadan vangha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannathai kadan vangha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kothodu kulaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothodu kulaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poopokum vaasam pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poopokum vaasam pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathodu un swasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathodu un swasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulasaami thiruneeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulasaami thiruneeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechalaum silirkaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechalaum silirkaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thotta anghangae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thotta anghangae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siluthu poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siluthu poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkann jadai asaivikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkann jadai asaivikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal kolusu nelivukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal kolusu nelivukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi podi pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi podi pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koda saanju poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koda saanju poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee paarkadha nerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarkadha nerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unna paarthudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unna paarthudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee parkkum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee parkkum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ala panju poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala panju poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey thee kuchi thala mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thee kuchi thala mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Barathai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Barathai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjodu oru baram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjodu oru baram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En thookatha pala naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thookatha pala naala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaanghi pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaanghi pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa paduthaalum kanavodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa paduthaalum kanavodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokki poraan di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokki poraan di"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekki poraan di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekki poraan di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannukul vizundhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannukul vizundhanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru dharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru dharam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Checkachevaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Checkachevaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetka padum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetka padum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannathai kadan vangha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannathai kadan vangha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey panjarathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey panjarathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedai kozhi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedai kozhi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjoram unvaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjoram unvaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulluthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulluthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey panjangathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey panjangathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal paaka solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal paaka solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannoram kadhai pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannoram kadhai pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">solludhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solludhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kaadhorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pakshi vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pakshi vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum un pera sollidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum un pera sollidhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Koovudhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koovudhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En thodum thoorathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thodum thoorathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee varum bothellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee varum bothellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vidum moochu analaagha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vidum moochu analaagha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolludhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolludhadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panjaala mela thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panjaala mela thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee mooti poriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee mooti poriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiy haiyoo angangae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiy haiyoo angangae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eriyudhadii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eriyudhadii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjaaru muththatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaaru muththatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thandhu ponena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thandhu ponena"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha ahaa yen yekkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha ahaa yen yekkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaiyumadii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaiyumadii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokki poraan di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokki poraan di"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekki poraan di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekki poraan di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannukul vizundhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannukul vizundhanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru dharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru dharam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Checkachevaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Checkachevaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetka padum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetka padum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannathai kadan vangha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannathai kadan vangha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey un kooda thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey un kooda thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam theriyaama thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam theriyaama thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada naan vazha varam ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada naan vazha varam ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Venumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venumadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey un maarbil thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey un maarbil thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru kann moodi thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru kann moodi thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada naan thoongha idam onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada naan thoongha idam onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Venumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venumadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey aaghasathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey aaghasathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru methai pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru methai pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Angha un kooda vilaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angha un kooda vilaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonudhadii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonudhadii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey aanandhathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey aanandhathil"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaalu rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaalu rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi yen petchai ketkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi yen petchai ketkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulludhadii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulludhadii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gadigaaram ilaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadigaaram ilaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi neram odaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi neram odaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingeyae ippodhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingeyae ippodhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindraal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindraal enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madimeedhu naan saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madimeedhu naan saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaiyagha ne maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaiyagha ne maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaalattu paatu ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalattu paatu ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnal enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sokki poraan di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokki poraan di"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekki poraan di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekki poraan di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannukul vizundhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannukul vizundhanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru dharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru dharam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Checkachevaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Checkachevaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetka padum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetka padum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannathai kadan vangha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannathai kadan vangha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi varum"/>
</div>
</pre>
