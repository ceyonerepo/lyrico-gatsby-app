---
title: "gundelona song lyrics"
album: "Naandhi"
artist: "Sricharan Pakala"
lyricist: "Chaitanya Prasad"
director: "Vijay Kanakamedala"
path: "/albums/naandhi-lyrics"
song: "Gundelona Manduthonda Mandhu"
image: ../../images/albumart/naandhi.jpg
date: 2021-02-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/o-2MSH4wAKU"
type: "sad"
singers:
  - Kareemulla
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gundelona Manduthonda Mandhu Leni Gayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelona Manduthonda Mandhu Leni Gayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundharantha Unnadhantha Anthuleni Shunyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundharantha Unnadhantha Anthuleni Shunyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karigera Karigera Bandhalanni Kalagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karigera Karigera Bandhalanni Kalagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaladhi Hrdhayame Banda Baare Shilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaladhi Hrdhayame Banda Baare Shilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadiche… Edhavu Neevu Kadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiche… Edhavu Neevu Kadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gruhame… Shithilamayenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gruhame… Shithilamayenugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murisipoyina Gnapakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murisipoyina Gnapakale"/>
</div>
<div class="lyrico-lyrics-wrapper">Musrukunnavi Nedilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musrukunnavi Nedilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiguru Vesina Rojulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiguru Vesina Rojulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedhalu Bujule Aayegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedhalu Bujule Aayegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirigele Ichatane Amma Nannalugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirigele Ichatane Amma Nannalugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Migilele Ipudila Patamu Bommalugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migilele Ipudila Patamu Bommalugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathulule Chithukulai Mandayi O Chithigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathulule Chithukulai Mandayi O Chithigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluguni Niluvunaa Marchayi Cheekatigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluguni Niluvunaa Marchayi Cheekatigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Manase… Pagilipoyenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Manase… Pagilipoyenugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhigule… Pogili Edchenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhigule… Pogili Edchenugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalale… Ragilipoyenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalale… Ragilipoyenugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathuke... Migile Budidhigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathuke... Migile Budidhigaa"/>
</div>
</pre>
