---
title: "maattu mani saththam song lyrics"
album: "Endraavathu Oru Naal"
artist: "N.R. Raghunanthan"
lyricist: "Vairamuthu"
director: "Vetri Duraisamy"
path: "/albums/endraavathu-oru-naal-lyrics"
song: "Maattu Mani Saththam"
image: ../../images/albumart/endraavathu-oru-naal.jpg
date: 2021-10-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/eNryKOPEeeE"
type: "happy"
singers:
  - Namitha Babu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maattu Mani Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattu Mani Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Koyil Mani Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Koyil Mani Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattu Saani Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattu Saani Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Parambaraya Suththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Parambaraya Suththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattu Mani Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattu Mani Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Koyil Mani Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Koyil Mani Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattu Saani Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattu Saani Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Parambaraya Suththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Parambaraya Suththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veedillaama Iruppathundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedillaama Iruppathundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Saathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Saathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Maadillaathe Iruppathille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Maadillaathe Iruppathille"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhavasaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhavasaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sella Pillayodu Moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sella Pillayodu Moththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Peru Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Peru Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevela Kaala Rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevela Kaala Rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethu Naalu Peruthaange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethu Naalu Peruthaange"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veesam Thantha Maadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesam Thantha Maadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Enga Pillayaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Enga Pillayaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ration Card Ezhuthumbothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ration Card Ezhuthumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru Vittu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru Vittu Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattu Mani Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattu Mani Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Koyil Mani Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Koyil Mani Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattu Saani Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattu Saani Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Parambaraya Suththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Parambaraya Suththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattu Mani Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattu Mani Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Koyil Mani Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Koyil Mani Saththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattu Saani Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattu Saani Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Parambaraya Suththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Parambaraya Suththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veedillaama Iruppathundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedillaama Iruppathundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Saathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Saathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Maadillaathe Iruppathille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Maadillaathe Iruppathille"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhavasaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhavasaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sella Pillayodu Moththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sella Pillayodu Moththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Peru Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Peru Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevela Kaala Rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevela Kaala Rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethu Naalu Peruthaange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethu Naalu Peruthaange"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veesam Thantha Maadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesam Thantha Maadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Enga Pillayaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Enga Pillayaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ration Card Ezhuthumbothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ration Card Ezhuthumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru Vittu Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru Vittu Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">KattiKatti Vacha Kaala Maattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="KattiKatti Vacha Kaala Maattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhutha Mani Osayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhutha Mani Osayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Satti Vechu Thanni Thalippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satti Vechu Thanni Thalippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayurappen Vaasayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayurappen Vaasayile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">KattiKatti Vacha Kaala Maattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="KattiKatti Vacha Kaala Maattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhutha Mani Osayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhutha Mani Osayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Satti Vechu Thanni Thalippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satti Vechu Thanni Thalippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayurappen Vaasayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayurappen Vaasayile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peththa Pulla Pasiyaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peththa Pulla Pasiyaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Peththa Pulla Pasiyaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peththa Pulla Pasiyaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Otha Moyi Ottayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Moyi Ottayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevela Maadu Thingathaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevela Maadu Thingathaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti Paappa Aasayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Paappa Aasayile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi Panam Venaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Panam Venaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnum Porulum Venaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnum Porulum Venaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadimane Venaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadimane Venaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodi Maadu Randu Pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodi Maadu Randu Pothume"/>
</div>
</pre>
