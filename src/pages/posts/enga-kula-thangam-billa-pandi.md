---
title: "enga kula thangam song lyrics"
album: "Billa Pandi"
artist: "Ilayavan"
lyricist: "Kalaikumar"
director: "Raj Sethupathy"
path: "/albums/billa-pandi-lyrics"
song: "Enga Kula Thangam"
image: ../../images/albumart/billa-pandi.jpg
date: 2018-11-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FsQvINUjEus"
type: "mass"
singers:
  - Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">enga kula thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kula thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">enga kula thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kula thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">enga thala singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga thala singam"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaipu engal kulam iyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaipu engal kulam iyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaipor or inam iyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaipor or inam iyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enga kula thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kula thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">enga thala singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga thala singam"/>
</div>
<div class="lyrico-lyrics-wrapper">enga kula thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kula thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">enga thala singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga thala singam"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaikira varkam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaikira varkam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">ore kulam engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ore kulam engum"/>
</div>
<div class="lyrico-lyrics-wrapper">engaluku endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engaluku endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">enga thala sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga thala sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">onna vaalugira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna vaalugira"/>
</div>
<div class="lyrico-lyrics-wrapper">annan thambi bantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annan thambi bantham"/>
</div>
<div class="lyrico-lyrics-wrapper">ucham thotta pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ucham thotta pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">thannadakam paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannadakam paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">machan athan namma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machan athan namma"/>
</div>
<div class="lyrico-lyrics-wrapper">ultimate star
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ultimate star"/>
</div>
<div class="lyrico-lyrics-wrapper">patta padi thalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta padi thalai"/>
</div>
<div class="lyrico-lyrics-wrapper">patta padi intha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta padi intha "/>
</div>
<div class="lyrico-lyrics-wrapper">billa pandi paata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="billa pandi paata"/>
</div>
<div class="lyrico-lyrics-wrapper">kettu vetta vedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kettu vetta vedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enga kula thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga kula thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">enga thala singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga thala singam"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaikira varkam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaikira varkam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">ore kulam engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ore kulam engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suyambaaga valarnthathu yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suyambaaga valarnthathu yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">malaiyaaga usanthathu yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaiyaaga usanthathu yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">namma thala athu namma thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma thala athu namma thala"/>
</div>
<div class="lyrico-lyrics-wrapper">payam illa thamilanum yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payam illa thamilanum yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">varalaara padachathu yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varalaara padachathu yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">namma thala veeram ulla thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma thala veeram ulla thala"/>
</div>
<div class="lyrico-lyrics-wrapper">ethiriya kooda thala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethiriya kooda thala "/>
</div>
<div class="lyrico-lyrics-wrapper">ethu kiruvaaru entha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethu kiruvaaru entha"/>
</div>
<div class="lyrico-lyrics-wrapper">thurogathaiyum aerpathe illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurogathaiyum aerpathe illa"/>
</div>
<div class="lyrico-lyrics-wrapper">yei yaarukume sollama than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei yaarukume sollama than"/>
</div>
<div class="lyrico-lyrics-wrapper">peruthavi pannugira nalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peruthavi pannugira nalla"/>
</div>
<div class="lyrico-lyrics-wrapper">gunam ulla thala da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunam ulla thala da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eeerathula tamil thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeerathula tamil thala"/>
</div>
<div class="lyrico-lyrics-wrapper">veerathula beema thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veerathula beema thala"/>
</div>
<div class="lyrico-lyrics-wrapper">ennalume ullukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennalume ullukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">thannambikai ulla thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannambikai ulla thala"/>
</div>
<div class="lyrico-lyrics-wrapper">enga thala thanga thala da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga thala thanga thala da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mudi ellam pepper and salt
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudi ellam pepper and salt"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivellam murukina polt
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivellam murukina polt"/>
</div>
<div class="lyrico-lyrics-wrapper">vesam illa sonna thosam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vesam illa sonna thosam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">nadipu ellam thiraiyila thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadipu ellam thiraiyila thane"/>
</div>
<div class="lyrico-lyrics-wrapper">neja vaalvula athu kedaiyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neja vaalvula athu kedaiyaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">namma thala romba nalla thala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma thala romba nalla thala"/>
</div>
<div class="lyrico-lyrics-wrapper">kanda mathu illa ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanda mathu illa ada"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam athu vella thala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam athu vella thala "/>
</div>
<div class="lyrico-lyrics-wrapper">vetriku than ellai illaiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetriku than ellai illaiye "/>
</div>
<div class="lyrico-lyrics-wrapper">naam ellarumo munerida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam ellarumo munerida"/>
</div>
<div class="lyrico-lyrics-wrapper">namma thala munodida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma thala munodida"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu vachu munne vayenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu vachu munne vayenda"/>
</div>
</pre>
