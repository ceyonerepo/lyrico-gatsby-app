---
title: "aariro song lyrics"
album: "Deiva Thirumagal"
artist: "G.V. Prakash Kumar"
lyricist: "Na. Muthukumar"
director: "A.L. Vijay"
path: "/albums/deiva-thirumagal-lyrics"
song: "Aariro"
image: ../../images/albumart/deiva-thirumagal.jpg
date: 2011-07-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UbGTGAyfx2c"
type: "happy"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aariro Aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aariro Aarariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Thanthaiyin Thaalaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thanthaiyin Thaalaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiye Puthithaanathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiye Puthithaanathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Mazhalaiyin Mozhi Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Mazhalaiyin Mozhi Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Thaayaaga Thanthai Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thaayaaga Thanthai Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu Kaaviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Kaaviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ivan Varaintha Kirukkalil Ivlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ivan Varaintha Kirukkalil Ivlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Oviyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Uyir Ondru Sernthu Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Uyir Ondru Sernthu Ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oar Uyir Aaguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Uyir Aaguthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvarai illai Endra Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvarai illai Endra Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumanthida Thonuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumanthida Thonuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyoram Eeram Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyoram Eeram Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudai Ketkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudai Ketkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aariro Aaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aariro Aaraariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Thanthaiyin Thaalaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thanthaiyin Thaalaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiye Puthithaanathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiye Puthithaanathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Mazhalaiyin Mozhi Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Mazhalaiyin Mozhi Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnam Oru Sontham Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnam Oru Sontham Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Aanathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Aanathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Nindru Ponaal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Nindru Ponaal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Maram Thooruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram Thooruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayathaal Valarnthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayathaal Valarnthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Pillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Pillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillai Pol Irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillai Pol Irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Annaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Annaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithupol Aanandham Verillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithupol Aanandham Verillaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Manam Ondru Sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Manam Ondru Sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingey Mounathil Pesuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingey Mounathil Pesuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Nodi Pothum Pothum Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Nodi Pothum Pothum Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oar Kural Ketkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oar Kural Ketkuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi Oram Eeram Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Oram Eeram Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudai Ketkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudai Ketkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aariro Aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aariro Aarariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Thanthaiyin Thaalaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thanthaiyin Thaalaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiye Puthithaanathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiye Puthithaanathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Mazhalaiyin Mozhi Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Mazhalaiyin Mozhi Kettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadikku Bimbam Athai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadikku Bimbam Athai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Kaattinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Kaattinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkatha Oar Paadal Athil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkatha Oar Paadal Athil"/>
</div>
<div class="lyrico-lyrics-wrapper">Isai Meettinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isai Meettinaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adada Deivam Inge Varamaanadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Deivam Inge Varamaanadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaai Veettil Vilaiyaduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai Veettil Vilaiyaduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin Vithai Ingey Maramaanathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin Vithai Ingey Maramaanathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavulai Paarthathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulai Paarthathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalathu Kangal Kaattuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalathu Kangal Kaattuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasaththin Munbu indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasaththin Munbu indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhagin Arivugal Thorkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhagin Arivugal Thorkuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyoram Eeram Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyoram Eeram Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudai Ketkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudai Ketkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aariro Aarariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aariro Aarariro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Thanthaiyin Thaalaattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thanthaiyin Thaalaattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiye Puthithaanathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiye Puthithaanathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Mazhalaiyin Mozhi Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Mazhalaiyin Mozhi Kettu"/>
</div>
</pre>
