---
title: "mathu vadalara song lyrics"
album: "Mathu Vadalara"
artist: "Kaala Bhairava"
lyricist: "M M Keeravani"
director: "Ritesh Rana"
path: "/albums/mathu-vadalara-lyrics"
song: "Mathu Vadalara"
image: ../../images/albumart/mathu-vadalara.jpg
date: 2019-12-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/0ymWMy13geM"
type: "title track"
singers:
  - M M Keeravani 
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Apayammu Daatadaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apayammu Daatadaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Upayammu Kaavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Upayammu Kaavali"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhakaaram Alaminapudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhakaaram Alaminapudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluthuriki Vethakali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthuriki Vethakali"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundu Choopu Leni Vaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundu Choopu Leni Vaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endulaku Koragaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endulaku Koragaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Somarivai Kunuku Vaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Somarivai Kunuku Vaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sushmamu Grahinchaledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sushmamu Grahinchaledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathu Vadalara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathu Vadalara "/>
</div>
<div class="lyrico-lyrics-wrapper">Nidura Mathu Vadalara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidura Mathu Vadalara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koosindhoy Dekho Dekho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosindhoy Dekho Dekho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kokkroko Kokkoroko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkroko Kokkoroko "/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudoo Kodi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudoo Kodi "/>
</div>
<div class="lyrico-lyrics-wrapper">Kotthaga Inko Roje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotthaga Inko Roje"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitthu Ga Paikosthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitthu Ga Paikosthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyaraa Dhaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyaraa Dhaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raathri Kallo Kocchina Deyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathri Kallo Kocchina Deyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalu Neetho Undadhuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalu Neetho Undadhuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagati Kalale Kantunnaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagati Kalale Kantunnaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka Vadhili Parugu Theeyraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka Vadhili Parugu Theeyraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathu Vadhalaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathu Vadhalaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalaraa Vadhalaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalaraa Vadhalaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathu Vadhalaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathu Vadhalaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathu Vadhalaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathu Vadhalaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhalaraa Vadhalaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhalaraa Vadhalaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathu Vadhalaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathu Vadhalaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raathri Kallo Kocchina Deyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathri Kallo Kocchina Deyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalu Neetho Undadhuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalu Neetho Undadhuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagati Kalale Kantunnaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagati Kalale Kantunnaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka Vadhili Parugu Theeyraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka Vadhili Parugu Theeyraa"/>
</div>
</pre>
