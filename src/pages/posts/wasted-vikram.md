---
title: "wasted song lyrics"
album: "Vikram"
artist: "Anirudh Ravichander"
lyricist: "Heisenberg"
director: "Lokesh Kanagaraj"
path: "/albums/vikram-lyrics"
song: "Wasted"
image: ../../images/albumart/vikram.jpg
date: 2022-06-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/y3tfpBxoci4"
type: "mass"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">I’m Drinkin’ Ha-Ha-Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Drinkin’ Ha-Ha-Ha"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m So Wasted Hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m So Wasted Hooo"/>
</div>
<div class="lyrico-lyrics-wrapper">I’ve Had 3 Shots Of JD
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’ve Had 3 Shots Of JD"/>
</div>
<div class="lyrico-lyrics-wrapper">And 4 Rounds Of Gin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And 4 Rounds Of Gin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Some Jägermeister In My Belly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Some Jägermeister In My Belly"/>
</div>
<div class="lyrico-lyrics-wrapper">I’ma About To Win
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’ma About To Win"/>
</div>
<div class="lyrico-lyrics-wrapper">Im Freaking Drunk Ha-Ha-Ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Im Freaking Drunk Ha-Ha-Ha"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m So Wasted Hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m So Wasted Hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tequila In The Glass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tequila In The Glass"/>
</div>
<div class="lyrico-lyrics-wrapper">Lime & Salt In The Hand
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lime & Salt In The Hand"/>
</div>
<div class="lyrico-lyrics-wrapper">Just Put In Your System
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just Put In Your System"/>
</div>
<div class="lyrico-lyrics-wrapper">You’re The Strongest Man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You’re The Strongest Man"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Aa Aa Aaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Aa Aaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I’m Sober Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Sober Now"/>
</div>
<div class="lyrico-lyrics-wrapper">No Thats A Lie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Thats A Lie"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m So Wasted Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m So Wasted Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I See Colours In The Sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I See Colours In The Sky"/>
</div>
<div class="lyrico-lyrics-wrapper">My Head Is Spinning Round
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Head Is Spinning Round"/>
</div>
<div class="lyrico-lyrics-wrapper">Somebody Help Me Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Somebody Help Me Now"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Falling To The Ground
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Falling To The Ground"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Freakin’ Drunk Oh No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Freakin’ Drunk Oh No"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I’m So Wasted
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m So Wasted"/>
</div>
<div class="lyrico-lyrics-wrapper">Seek My Order
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seek My Order"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Wanna Last Shot Of Whiskey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Wanna Last Shot Of Whiskey"/>
</div>
<div class="lyrico-lyrics-wrapper">And A Beer To Cease
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And A Beer To Cease"/>
</div>
<div class="lyrico-lyrics-wrapper">Take That Absin Pour It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take That Absin Pour It"/>
</div>
<div class="lyrico-lyrics-wrapper">All Over My Face Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Over My Face Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Aa Aa Aaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Aa Aaaaa"/>
</div>
</pre>
