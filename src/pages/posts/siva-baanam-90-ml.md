---
title: "siva baanam song lyrics"
album: "90 ML"
artist: "Silambarasan"
lyricist: "Mirchi Vijay - STR"
director: "Anita Udeep"
path: "/albums/90-ml-lyrics"
song: "Siva Baanam"
image: ../../images/albumart/90-ml.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ggJZ-c8cac8"
type: "happy"
singers:
  - Silambarasan TR
  - Anita Udeep
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Siva Baanam Ulla Yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Baanam Ulla Yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Yedho Pannudhe Pannudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yedho Pannudhe Pannudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyayo Ennanavo Thoonudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayo Ennanavo Thoonudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaiyellam Ulla Suththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyellam Ulla Suththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikkumukki Aadi Poi Thindadi Nikkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikkumukki Aadi Poi Thindadi Nikkudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalum Un Vegam Seeri Paayudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum Un Vegam Seeri Paayudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam Thaan Chinna Ponnu Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Thaan Chinna Ponnu Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmunne Suththi Ippo Oduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmunne Suththi Ippo Oduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetha Thedi Nee Odurane Kelvi Ketkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetha Thedi Nee Odurane Kelvi Ketkudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidai Thedi Naanum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Thedi Naanum Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Naala Anga Inga Oduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Naala Anga Inga Oduran"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathiley Illatha Kelvi Pola Nikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathiley Illatha Kelvi Pola Nikkuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Siva Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Siva Sambo Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Siva Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Siva Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baanam Thaan Ulle Poche…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanam Thaan Ulle Poche…"/>
</div>
<div class="lyrico-lyrics-wrapper">Baanam Thaan Ulle Poche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanam Thaan Ulle Poche"/>
</div>
<div class="lyrico-lyrics-wrapper">Sogamum Kaana Poche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogamum Kaana Poche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirippum Thanave Ippo Varudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirippum Thanave Ippo Varudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Ulla Enna Ennamo Panna Vandhadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ulla Enna Ennamo Panna Vandhadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalaye Kaatula Thookudhey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaye Kaatula Thookudhey Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanmunne Kadavula Thaan Paakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmunne Kadavula Thaan Paakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaiyudan Kekuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyudan Kekuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Varthuma Nenaikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varthuma Nenaikiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Sambo Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo Siva Sambo Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo Siva Sambo Siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Siva Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Siva Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Siva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Sambo Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Sambo Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Siva Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Siva Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Siva Siva Siva Siva Siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Siva Siva Siva Siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Sambo"/>
</div>
</pre>
