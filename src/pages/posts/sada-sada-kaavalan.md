---
title: "sada sada song lyrics"
album: "Kaavalan"
artist: "Vidyasagar"
lyricist: "Yugabharathi"
director: "Siddique"
path: "/albums/kaavalan-lyrics"
song: "Sada Sada"
image: ../../images/albumart/kaavalan.jpg
date: 2011-01-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/87cyHD2YyiE"
type: "love"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sada Sada Sada Sada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sada Sada Sada Sada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyena Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyena Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thada Thada Thada Thada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thada Thada Thada Thada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rail Ena Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rail Ena Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikadi Adikadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikadi Adikadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudikudhu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikudhu Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sada Sada Sada Sada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sada Sada Sada Sada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyena Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyena Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thada Thada Thada Thada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thada Thada Thada Thada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rail Ena Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rail Ena Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikadi Adikadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikadi Adikadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudikudhu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikudhu Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Neraththil Varuvaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Neraththil Varuvaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakkathan Viduvaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkathan Viduvaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parthaaley Muraipaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parthaaley Muraipaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paal Polae Siripaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal Polae Siripaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kettaley Kodupalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettaley Kodupalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkamal Anaipala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkamal Anaipala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konji Konji Kaadhal Seidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji Konji Kaadhal Seidhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolvalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolvalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sada Sada Sada Sada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sada Sada Sada Sada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyena Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyena Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thada Thada Thada Thada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thada Thada Thada Thada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rail Ena Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rail Ena Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikadi Adikadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikadi Adikadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudikudhu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikudhu Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sada Sada Sada Sada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sada Sada Sada Sada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyena Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyena Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thada Thada Thada Thada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thada Thada Thada Thada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rail Ena Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rail Ena Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikadi Adikadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikadi Adikadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudikudhu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikudhu Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattu Thari Indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu Thari Indri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhu Ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhu Ullam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Enni Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Enni Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Angum Ingum Kummi Kottudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angum Ingum Kummi Kottudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solla Mozhi Indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla Mozhi Indri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamil Sorkel Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamil Sorkel Ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittu Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu Vittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andharathil Ammi Kuththudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharathil Ammi Kuththudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Theruviley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Theruviley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhaasai Azhaiyudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhaasai Azhaiyudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenga Ninaivilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Ninaivilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhal Kooda Vidukudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal Kooda Vidukudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuralaaley Enni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuralaaley Enni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudi Yerikondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudi Yerikondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolaikari Undhan Niyabagangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaikari Undhan Niyabagangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Kuththudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kuththudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sada Sada Sada Sada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sada Sada Sada Sada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyena Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyena Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thada Thada Thada Thada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thada Thada Thada Thada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Railena Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Railena Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikadi Adikadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikadi Adikadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudikudhu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikudhu Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gangai Nadhi Vellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangai Nadhi Vellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Sangukulley Sikki Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Sangukulley Sikki Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkaraiku Sella Ennudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkaraiku Sella Ennudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnan Siru Pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnan Siru Pillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Sopanathai Vaithu Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sopanathai Vaithu Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannurakkam Kettu Nikkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannurakkam Kettu Nikkudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee En Edhirilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee En Edhirilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varavendum Viraivilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varavendum Viraivilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naeril Varumvarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naeril Varumvarai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugam Kattu Kanavilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam Kattu Kanavilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Medhuvaga Sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuvaga Sellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedigara Mullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedigara Mullum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Kaana Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kaana Solli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaiyaiyo Nachcharikudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyaiyo Nachcharikudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sada Sada Sada Sada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sada Sada Sada Sada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyena Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyena Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thada Thada Thada Thada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thada Thada Thada Thada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Railena Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Railena Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikadi Adikadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikadi Adikadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudikudhu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikudhu Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sada Sada Sada Sada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sada Sada Sada Sada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaiyena Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyena Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thada Thada Thada Thada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thada Thada Thada Thada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Railena Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Railena Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikadi Adikadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikadi Adikadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudikudhu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikudhu Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Neraththil Varuvaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Neraththil Varuvaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakkathan Viduvaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkathan Viduvaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthaley Muraipala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthaley Muraipala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paal Poley Siripaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal Poley Siripaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketaley Kodupaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketaley Kodupaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkamal Anaipaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkamal Anaipaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konji Konji Kadhal Seidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji Konji Kadhal Seidhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolvaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolvaalaa"/>
</div>
</pre>
