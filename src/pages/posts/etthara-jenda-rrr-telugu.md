---
title: "etthara jenda song lyrics"
album: "RRR Telugu"
artist: "Maragathamani"
lyricist: "Ramajogayya Sastry"
director: "S.S. Rajamouli "
path: "/albums/rrr-telugu-lyrics"
song: "Etthara Jenda"
image: ../../images/albumart/rrr-telugu.jpg
date: 2022-03-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/dtZ14XTwwos"
type: "mass"
singers:
  - Vishal Mishra
  - Prudhvi Chandra
  - MM Keeravaani
  - Sahithi Chaganti
  - Harika Narayan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nethuru Marigathe Etthara Jenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethuru Marigathe Etthara Jenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathuve Urimithe Kottara Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathuve Urimithe Kottara Konda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethuru Marigathe Ethara Jenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethuru Marigathe Ethara Jenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathuve Urumithe Kottara Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathuve Urumithe Kottara Konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Jenda Konda Kathi Suthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jenda Konda Kathi Suthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitha Kotha Kommu Kode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitha Kotha Kommu Kode"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mansile Nee Kode Goru Kode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mansile Nee Kode Goru Kode"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirigale Kode Sirisilla Kode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirigale Kode Sirisilla Kode"/>
</div>
<div class="lyrico-lyrics-wrapper">Ella Ella Kode Yechaina Kode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ella Ella Kode Yechaina Kode"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathikana Gattu Ki Rayalisima Kode Haaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathikana Gattu Ki Rayalisima Kode Haaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethuru Marigathe Etthara Jenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethuru Marigathe Etthara Jenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathuve Urumithe Kottara Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathuve Urumithe Kottara Konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raiya Raiya Regathamle Lemone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raiya Raiya Regathamle Lemone"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhamu Dhamu Gundalake Dhanune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhamu Dhamu Gundalake Dhanune"/>
</div>
<div class="lyrico-lyrics-wrapper">Okku Narum Piru Piru Pigusane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okku Narum Piru Piru Pigusane"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Simma Sikka Mupatna Mugisine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Simma Sikka Mupatna Mugisine"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippudu Kakunthe Nee Kepudu Vazhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippudu Kakunthe Nee Kepudu Vazhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkula Melalu Maha Kopakemokkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkula Melalu Maha Kopakemokkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mokka Kotha Kotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mokka Kotha Kotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thutta Vetta Thirumu Kode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thutta Vetta Thirumu Kode"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kasikkale Kode Kolkata Kode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasikkale Kode Kolkata Kode"/>
</div>
<div class="lyrico-lyrics-wrapper">Guchugale Kode Gujarati Kode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guchugale Kode Gujarati Kode"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathiladi Kode Chittooru Kode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathiladi Kode Chittooru Kode"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirugelenithi Tirunalveli Kode Haaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugelenithi Tirunalveli Kode Haaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethuru Marigathe Ethara Jenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethuru Marigathe Ethara Jenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathuve Urumithe Kottara Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathuve Urumithe Kottara Konda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chuttare Chuttu Thalapaga Chuttara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttare Chuttu Thalapaga Chuttara"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattara Pattu Pidikili Battara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattara Pattu Pidikili Battara"/>
</div>
<div class="lyrico-lyrics-wrapper">Chappalu Rendu Charichi Jai Kottara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chappalu Rendu Charichi Jai Kottara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Okko Gothu Kotlani Pettura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Okko Gothu Kotlani Pettura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chudare a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudare a"/>
</div>
<div class="lyrico-lyrics-wrapper">Chunthe Mainoki Bharosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chunthe Mainoki Bharosa"/>
</div>
<div class="lyrico-lyrics-wrapper">Khumara Ganesha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khumara Ganesha"/>
</div>
<div class="lyrico-lyrics-wrapper">Guna Kettare Gulasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guna Kettare Gulasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asa Gussa Kutta Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asa Gussa Kutta Kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Gincha Gunche Kanchu Kode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gincha Gunche Kanchu Kode"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathamuna Kode Panjabi Kode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathamuna Kode Panjabi Kode"/>
</div>
<div class="lyrico-lyrics-wrapper">Renganana Kode Rangdoori Kode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Renganana Kode Rangdoori Kode"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhusala Kode Allah Si Kode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhusala Kode Allah Si Kode"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijayathi Harame Veeramare Haan Kode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijayathi Harame Veeramare Haan Kode"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nethuru Marigathe Etthara Jenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethuru Marigathe Etthara Jenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathuve Urumithe Kottara Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathuve Urumithe Kottara Konda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethuru Marigathe Ethara Jenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethuru Marigathe Ethara Jenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathuve Urumithe Kottara Konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathuve Urumithe Kottara Konda"/>
</div>
</pre>
