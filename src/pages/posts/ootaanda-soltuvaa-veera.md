---
title: "ootaanda soltuvaa song lyrics"
album: "Veera"
artist: "Leon James"
lyricist: "Ko Sesha"
director: "Rajaraman"
path: "/albums/veera-lyrics"
song: "Ootaanda Soltuvaa"
image: ../../images/albumart/veera.jpg
date: 2018-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/twTeHFzquZU"
type: "happy"
singers:
  - Leon James
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Cool ah ukanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cool ah ukanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee time ah ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee time ah ottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chanceu kedachaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chanceu kedachaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un weight ah kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un weight ah kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raw va adichalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raw va adichalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavadha tight-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavadha tight-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum kondatam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum kondatam thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru rendana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru rendana"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothadi aavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothadi aavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiri seiyavantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiri seiyavantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Surrunnu raavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surrunnu raavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Light ah miss aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light ah miss aana"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirmu saavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirmu saavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam un luck-u dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam un luck-u dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye sithappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye sithappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kiththappu thangaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kiththappu thangaliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen adhappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen adhappu"/>
</div>
<div class="lyrico-lyrics-wrapper">En geththa paakaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En geththa paakaliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh eh hey eh eh heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh eh hey eh eh heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vutanda vutanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda vutanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda soltu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda soltu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanda ennanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanda ennanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanda  vachukina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanda  vachukina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda vutanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda vutanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda soltu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda soltu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kitta maatikaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kitta maatikaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vutanda vutanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda vutanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda soltu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda soltu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanda ennanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanda ennanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanda vachukina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanda vachukina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda vutanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda vutanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda soltu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda soltu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kitta maatikaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kitta maatikaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un dakalti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un dakalti"/>
</div>
<div class="lyrico-lyrics-wrapper">Dubakoor dacoity
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dubakoor dacoity"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacha balikadhu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha balikadhu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un bagilu bigulu dagulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un bagilu bigulu dagulu"/>
</div>
<div class="lyrico-lyrics-wrapper">En kitta selladhu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kitta selladhu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei vavvalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei vavvalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un savalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un savalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Na once sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na once sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnadhudhaan daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnadhudhaan daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna galija
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna galija"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachu kalaicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachu kalaicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Back adichu odatha daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Back adichu odatha daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vutanda vutanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda vutanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda soltu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda soltu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanda ennanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanda ennanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanda  vachukina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanda  vachukina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda vutanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda vutanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda soltu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda soltu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kitta maatikaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kitta maatikaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vutanda vutanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda vutanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda soltu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda soltu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanda ennanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanda ennanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanda  vachukina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanda  vachukina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda vutanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda vutanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda soltu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda soltu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kitta maatikaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kitta maatikaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cool ah ukanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cool ah ukanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee time ah ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee time ah ottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chanceu kedachaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chanceu kedachaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un weight ah kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un weight ah kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raw va adichalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raw va adichalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavadha tight-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavadha tight-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum kondatam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum kondatam thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru rendana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru rendana"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothadi aavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothadi aavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiri seiyavantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiri seiyavantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Surrunnu raavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surrunnu raavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Light ah miss aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light ah miss aana"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirmu saavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirmu saavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam un luck-u dhaan Saavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam un luck-u dhaan Saavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Un Luck-u Dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Un Luck-u Dhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye sithappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye sithappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kiththappu thangaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kiththappu thangaliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen adhappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen adhappu"/>
</div>
<div class="lyrico-lyrics-wrapper">En geththa paakaliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En geththa paakaliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh eh hey eh eh heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh eh hey eh eh heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vutanda vutanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda vutanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda soltu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda soltu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanda ennanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanda ennanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanda vachukina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanda vachukina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda vutanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda vutanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda soltu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda soltu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kitta maatikaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kitta maatikaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vutanda vutanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda vutanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda soltu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda soltu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanda ennanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanda ennanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanda  vachukina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanda  vachukina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda vutanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda vutanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vutanda soltu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vutanda soltu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kitta maatikaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kitta maatikaadha"/>
</div>
</pre>
