---
title: "red cardu song lyrics"
album: "Vantha Rajava Than Varuven"
artist: "Hiphop Tamizha"
lyricist: "Arivu"
director: "Sundar C"
path: "/albums/vantha-rajava-than-varuven-lyrics"
song: "Red Cardu"
image: ../../images/albumart/vantha-rajava-than-varuven.jpg
date: 2019-02-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NKtnt2J7OCg"
type: "happy"
singers:
  - Silambarasan
  - Hiphop Tamizha
  - Snigdha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">I wanna Break Free
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wanna Break Free"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">And Be a Sipping Like a Queen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And Be a Sipping Like a Queen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">And Let You Take Me Out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And Let You Take Me Out"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Show Me  Some Good Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Show Me  Some Good Time"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shinning Like a Star Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shinning Like a Star Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Paakura Paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paakura Paarvaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paayuthu Minnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayuthu Minnal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Take That Away From Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take That Away From Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaikku Ethukku Kadhavu Jannal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaikku Ethukku Kadhavu Jannal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Still Be Star Homie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Still Be Star Homie"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkuda Naan Senthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkuda Naan Senthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadida Paadida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadida Paadida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Know What i Mean Homie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Know What i Mean Homie"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Naan Jodi Sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Naan Jodi Sera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready Thaan Let’s Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready Thaan Let’s Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkaa Red Cardu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkaa Red Cardu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eduththu Paaru Recordu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduththu Paaru Recordu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichchi Meratti Eduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichchi Meratti Eduppan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theruvulathaan Erangi Aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvulathaan Erangi Aadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulla Vida Mattenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vida Mattenu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollurathukku Nee Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollurathukku Nee Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliya Naan Podum Aattathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliya Naan Podum Aattathukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Kootaththa Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Kootaththa Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattadha Pattunu Sonnaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattadha Pattunu Sonnaakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Kettavanu Soldraanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kettavanu Soldraanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orama Poi Ukkaantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orama Poi Ukkaantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Uththamanu Soldraanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Uththamanu Soldraanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappunu Theruncha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappunu Theruncha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tappunu Kothikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tappunu Kothikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prechchanai Ethukku Paayaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prechchanai Ethukku Paayaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pechula illai Poi Vesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pechula illai Poi Vesam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Orasi Paaththa Nee Naasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Orasi Paaththa Nee Naasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Vekkura Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vekkura Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkara Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkara Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkura Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkura Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanda Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanda Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So Welcome to My
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Welcome to My"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Party Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Machaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Your are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Your are"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Welcome to My
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome to My"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Party Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Machaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Adraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Adraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkaa Red Cardu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkaa Red Cardu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eduththu Paaru Recordu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduththu Paaru Recordu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichchi Meratti Eduppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichchi Meratti Eduppan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theruvulathaan Erangi Aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvulathaan Erangi Aadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulla Vida Mattenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla Vida Mattenu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollurathukku Nee Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollurathukku Nee Yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veliya Naan Podum Aattathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliya Naan Podum Aattathukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Kootaththa Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Kootaththa Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oram Katti Vechchavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oram Katti Vechchavana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedi Vanthu Nikka Veppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Vanthu Nikka Veppan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pora Vazhi Thappu illaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pora Vazhi Thappu illaina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuda Ninnu Thol Kuduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuda Ninnu Thol Kuduppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machchaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machchaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesura Vaai Aayiram Pesattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesura Vaai Aayiram Pesattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kekkura Moodula Naan illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkura Moodula Naan illa da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seththukka Maattenu Sollittu Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththukka Maattenu Sollittu Ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gate la Nikkara Aal illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gate la Nikkara Aal illa da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesura Vaai Aayiram Pesattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesura Vaai Aayiram Pesattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kekkura Moodula Naan illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkura Moodula Naan illa da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seththukka Maattenu Sollittu Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththukka Maattenu Sollittu Ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gate la Nikkara Aal illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gate la Nikkara Aal illa da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thani Aal illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thani Aal illa da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thani Aal illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thani Aal illa da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thani Aal illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thani Aal illa da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thani Aal illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thani Aal illa da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkura Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkura Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkara Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkara Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkura Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkura Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanda Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanda Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkura Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkura Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkara Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkara Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkura Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkura Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanda Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanda Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So Welcome to My
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Welcome to My"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Party Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Machaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Your are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Your are"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Welcome to My
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome to My"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Party Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Machaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Adraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Adraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Damaalu Damaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damaalu Damaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adra Adra Adra Adra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adra Adra Adra Adra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Vekkura Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vekkura Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkara Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkara Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Damaalu Damaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damaalu Damaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkura Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkura Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkara Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkara Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adra Adra Adra Adra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adra Adra Adra Adra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avalukku Enna Puduchirukkuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalukku Enna Puduchirukkuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittathum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittathum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudikkalaina Thottadhum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikkalaina Thottadhum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puduchirukkuna Vittathum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puduchirukkuna Vittathum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudikkalaina Thottadhum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikkalaina Thottadhum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porakkura Pozhutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porakkura Pozhutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikaama Aaduna Kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikaama Aaduna Kaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akkam Pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkam Pakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Paththi Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paththi Kelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">If You Wanna Dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If You Wanna Dance"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready 1 2 Fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready 1 2 Fun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shinning Like a Star Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shinning Like a Star Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Take That Away From Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take That Away From Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Still Be Star Homie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Still Be Star Homie"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Know What i Mean Homie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Know What i Mean Homie"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Vekkura Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vekkura Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkara Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkara Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkura Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkura Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanda Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanda Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkura Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkura Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikkara Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkara Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkura Edathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkura Edathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanda Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanda Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So Welcome to My
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Welcome to My"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Party Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Machaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Welcome to My
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome to My"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Party Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party Machaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Adraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Adraa"/>
</div>
</pre>
