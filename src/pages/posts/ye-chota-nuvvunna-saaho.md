---
title: "ye chota nuvvunna song lyrics"
album: "Saaho"
artist: "Guru Randhawa"
lyricist: "Krishna Kanth"
director: "Sujeeth"
path: "/albums/saaho-lyrics"
song: "Ye Chota Nuvvunna"
image: ../../images/albumart/saaho-telugu.jpg
date: 2019-08-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/kTgFw8iqkJ8"
type: "love"
singers:
  - Haricharan
  - Tulsi Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye Chota Nuvvunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Chota Nuvvunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopirilaa Nenuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopirilaa Nenuta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ventaade Yekaantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ventaade Yekaantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Lenatte Neekinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lenatte Neekinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennante Nuvvunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennante Nuvvunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakemaina Baagunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakemaina Baagunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooraala Dhaarullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooraala Dhaarullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeventa Nenutaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeventa Nenutaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannilaa Neelona Daachesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannilaa Neelona Daachesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnalu Marichela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalu Marichela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Premisthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Premisthale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kannulu Alisela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannulu Alisela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ney Kanipisthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ney Kanipisthaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnalu Marichela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalu Marichela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Premisthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Premisthale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kannulu Alisela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannulu Alisela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ney Kanipisthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ney Kanipisthaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innaalla Naa Mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaalla Naa Mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedaali Neekosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedaali Neekosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisochene Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisochene Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorikindhi Nee Sneham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorikindhi Nee Sneham"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadhanna Aasantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadhanna Aasantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesthanu Nee Sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesthanu Nee Sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Raadhinka Ye Dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raadhinka Ye Dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakunte Nee Saayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakunte Nee Saayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannilaa Neelona Dhaachesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannilaa Neelona Dhaachesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnalu Marichela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalu Marichela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Premisthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Premisthale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kannulu Alisela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannulu Alisela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ney Kanipisthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ney Kanipisthaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Reppalu Moosunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppalu Moosunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ney Ninne Choostharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ney Ninne Choostharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppatike Ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppatike Ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Dhaasthaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Dhaasthaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnalu Marichela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalu Marichela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Premisthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Premisthale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kannulu Alisela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannulu Alisela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ney Kanipisthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ney Kanipisthaale"/>
</div>
</pre>
