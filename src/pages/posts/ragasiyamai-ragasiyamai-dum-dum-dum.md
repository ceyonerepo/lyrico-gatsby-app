---
title: "ragasiyamai song lyrics"
album: "Dum Dum Dum"
artist: "Karthik Raja"
lyricist: "Na. Muthukumar - Vaali - Pa. Vijay"
director: "Azhagam Perumal"
path: "/albums/dum-dum-dum-song-lyrics"
song: "Ragasiyamai Ragasiyamai"
image: ../../images/albumart/dum-dum-dum.jpg
date: 2001-04-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2nbx2rddQZc"
type: "love"
singers:
  - Hariharan
  - Sadhana Sargam
  - Ramanathan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ragasiyamaai Ragasiyamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyamaai Ragasiyamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagaithaal Porul Ennavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagaithaal Porul Ennavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragasiyamaai Ragasiyamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyamaai Ragasiyamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagaithaal Porul Ennavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagaithaal Porul Ennavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solla Thudikum Vaarthai Kirangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla Thudikum Vaarthai Kirangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondai Kuzhiyil Oosi Irangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondai Kuzhiyil Oosi Irangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilai Vadivil Idhayam Irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilai Vadivil Idhayam Irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai Vadivil Adhuvum Ganakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Vadivil Adhuvum Ganakum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirithu Sirithu Sirayile Siki Kolla Adam Pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithu Sirithu Sirayile Siki Kolla Adam Pidikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilam Neer Kaatrile Minsaarangal Piranthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilam Neer Kaatrile Minsaarangal Piranthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Tharum Minsaaramo Prabanjathai Kadanthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Tharum Minsaaramo Prabanjathai Kadanthidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamaai Nee Ennai Theendinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaai Nee Ennai Theendinaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamaai Nee Ennai Theendinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamaai Nee Ennai Theendinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paniyaai Paniyaai Uraigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniyaai Paniyaai Uraigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oliyaai Nee Ennai Theendinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyaai Nee Ennai Theendinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuraiyaai Unnul Karaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuraiyaai Unnul Karaigiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Vanthaale Vanthaale Yeno Ularalgal Thaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vanthaale Vanthaale Yeno Ularalgal Thaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avasaramaai Avasaramaai Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaramaai Avasaramaai Mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholainthaal Porul Ennavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholainthaal Porul Ennavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velli Tharai Polave En Idhayam Irunthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli Tharai Polave En Idhayam Irunthudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella Vanthu Un Viral Kaadhal Endru Ezhuthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella Vanthu Un Viral Kaadhal Endru Ezhuthudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Kaadhal En Vaasalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Kaadhal En Vaasalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Kaadhal En Vaasalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Kaadhal En Vaasalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Varava Varava Ketathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varava Varava Ketathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Naal Kaadhal En Veetukul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Naal Kaadhal En Veetukul"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimai Saasanam Meetuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimai Saasanam Meetuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhuvo Adhu Idhuvo Idhu Yedhuvo Adhuve Naam Ariyome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvo Adhu Idhuvo Idhu Yedhuvo Adhuve Naam Ariyome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragasiyamaai Ragasiyamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyamaai Ragasiyamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagaithaal Porul Ennavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagaithaal Porul Ennavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avasaramaai Avasaramaai Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaramaai Avasaramaai Mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholainthaal Porul Ennavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholainthaal Porul Ennavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solla Thudikum Vaarthai Kirangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla Thudikum Vaarthai Kirangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondai Kuzhiyil Oosi Irangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondai Kuzhiyil Oosi Irangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilai Vadivil Idhayam Irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilai Vadivil Idhayam Irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai Vadivil Adhuvum Ganakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Vadivil Adhuvum Ganakum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirithu Sirithu Sirayile Siki Kolla Adam Pidikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithu Sirithu Sirayile Siki Kolla Adam Pidikum"/>
</div>
</pre>
