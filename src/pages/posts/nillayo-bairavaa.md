---
title: "nillayo song lyrics"
album: "Bairavaa"
artist: "Santhosh Narayanan"
lyricist: "Vairamuthu"
director: "Bharathan"
path: "/albums/bairavaa-lyrics"
song: "Nillayo"
image: ../../images/albumart/bairavaa.jpg
date: 2017-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/naGxpB5FbXw"
type: "love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manjal megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru manjal megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru manjal megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru pennaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru pennaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Munne pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munne pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padharum udalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padharum udalum"/>
</div>
<div class="lyrico-lyrics-wrapper">En kadharum uyirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kadharum uyirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval perkettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval perkettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinne pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinne pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chella Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella Poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unnai kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unnai kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Silluu sillaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silluu sillaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir sidhara kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir sidhara kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nillaayooo Nillayooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillaayooo Nillayooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un per enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un per enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalaee marandhenaee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalaee marandhenaee"/>
</div>
<div class="lyrico-lyrics-wrapper">En per enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En per enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanava kanvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanava kanvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kanbadhu kanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kanbadhu kanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kanmunnae kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanmunnae kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">Thugalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thugalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattrin udalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattrin udalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamban kavidhai madalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamban kavidhai madalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival thennaattin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival thennaattin"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangam kadalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangam kadalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silicon silayoooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silicon silayoooh"/>
</div>
<div class="lyrico-lyrics-wrapper">Siruvaai malarooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruvaai malarooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellai nadhiyooooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai nadhiyooooh"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyur nilavooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyur nilavooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nillaayooo Nillayooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillaayooo Nillayooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un per enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un per enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalaee marandhenaee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalaee marandhenaee"/>
</div>
<div class="lyrico-lyrics-wrapper">En per enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En per enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sempon silayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sempon silayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival ayimpon azhago
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival ayimpon azhago"/>
</div>
<div class="lyrico-lyrics-wrapper">Brahmman magalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmman magalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival penpaal veyilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival penpaal veyilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unnai pondra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unnai pondra"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennai kandadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennai kandadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyiril paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyiril paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum kondradhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum kondradhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnazhagaal mutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnazhagaal mutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Motcham kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motcham kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai pin mudiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai pin mudiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thookkilidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thookkilidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nillaayooo Nillayooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillaayooo Nillayooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un per enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un per enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalaee marandhenaee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalaee marandhenaee"/>
</div>
<div class="lyrico-lyrics-wrapper">En per enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En per enna"/>
</div>
<div class="lyrico-lyrics-wrapper">oooooo oooooo ooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oooooo oooooo ooooooo"/>
</div>
</pre>
