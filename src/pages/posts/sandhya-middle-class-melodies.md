---
title: "sandhya song lyrics"
album: "Middle Class Melodies"
artist: "Sweekar Agasthi"
lyricist: "Sanapati Bharadwaj Patrudu"
director: "Vinod Anantoju"
path: "/albums/middle-class-melodies-lyrics"
song: "Sandhya"
image: ../../images/albumart/middle-class-melodies.jpg
date: 2020-11-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Km2uVbMyWJk"
type: "happy"
singers:
  - Sweekar Agasthi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sandhya padha padha padhamani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhya padha padha padhamani "/>
</div>
<div class="lyrico-lyrics-wrapper">ante sigge aapindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ante sigge aapindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Baava ani pilichendhuku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baava ani pilichendhuku "/>
</div>
<div class="lyrico-lyrics-wrapper">mohamaatamtho ibbandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mohamaatamtho ibbandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu vanakka thonakka berakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu vanakka thonakka berakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarigga unte chaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarigga unte chaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha venakki jarakka churugga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha venakki jarakka churugga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolikki thwaragaa vasthaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolikki thwaragaa vasthaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi vayassu vipatthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi vayassu vipatthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okintha theginchi unte mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okintha theginchi unte mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi tharinchi thalonchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi tharinchi thalonchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Correct mugimpu ipude isthaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Correct mugimpu ipude isthaadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhyalo unnadhi dhaggaro dhooramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhyalo unnadhi dhaggaro dhooramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasthainaa thelisindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasthainaa thelisindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthakee thelani premalo theladam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthakee thelani premalo theladam"/>
</div>
<div class="lyrico-lyrics-wrapper">Emainaa baagundhaa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emainaa baagundhaa aaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatlani kukkesaave manasu nindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatlani kukkesaave manasu nindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaatinika pampedhundhaa pedhavi gundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaatinika pampedhundhaa pedhavi gundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bidiyamtho sahavasam ika chalu balika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bidiyamtho sahavasam ika chalu balika"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi entho apachaaram ani anukove chilakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi entho apachaaram ani anukove chilakaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhya padha padha padhamani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhya padha padha padhamani "/>
</div>
<div class="lyrico-lyrics-wrapper">ante sigge aapindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ante sigge aapindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Baava ani pilichendhuku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baava ani pilichendhuku "/>
</div>
<div class="lyrico-lyrics-wrapper">mohamaatamtho ibbandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mohamaatamtho ibbandhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yem saripoddhe nuvu choope premaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yem saripoddhe nuvu choope premaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo thega choosthe panulevi kaavammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo thega choosthe panulevi kaavammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paikalaa avupisthaade evarikainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paikalaa avupisthaade evarikainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadikee ishtam undhe thamaripainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadikee ishtam undhe thamaripainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Visiraavo gurichoosi valapanna baaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visiraavo gurichoosi valapanna baaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Padipodha valalona pilagaadi praaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padipodha valalona pilagaadi praaname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhya padha padha padhamani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhya padha padha padhamani "/>
</div>
<div class="lyrico-lyrics-wrapper">ante sigge aapindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ante sigge aapindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aune pogarunu prematho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aune pogarunu prematho "/>
</div>
<div class="lyrico-lyrics-wrapper">manishini chesthe mee baave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manishini chesthe mee baave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu vanakka thonakka berakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu vanakka thonakka berakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarigga unte chaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarigga unte chaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha venakki jarakka churugga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha venakki jarakka churugga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolikki thwaragaa vasthaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolikki thwaragaa vasthaadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi vayassu vipatthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi vayassu vipatthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okintha theginchi unte mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okintha theginchi unte mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi tharinchi thalonchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi tharinchi thalonchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Correct mugimpu ipude isthaadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Correct mugimpu ipude isthaadhe"/>
</div>
</pre>
