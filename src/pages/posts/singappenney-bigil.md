---
title: 'singappenney song lyrics'
album: 'Bigil'
artist: 'A R Rahman'
lyricist: 'Vivek'
director: 'Atlee'
path: '/albums/bigil-song-lyrics'
song: 'Singappenney'
image: ../../images/albumart/bigil.jpg
date: 2019-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IiUNRYQ1Cak"
type: 'motivation'
singers: 
- A R Rahman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Maadharae….ae…ae….ae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Maadharae….ae…ae….ae…."/>
</div>
<div class="lyrico-lyrics-wrapper">Maadharae….ae….ae…ae…ae
<input type="checkbox" class="lyrico-select-lyric-line" value="Maadharae….ae….ae…ae…ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaalaagum keeralgal thunivodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaalaagum keeralgal thunivodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paagangal thimiroodu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paagangal thimiroodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seerungal vaarungal vaarungal
<input type="checkbox" class="lyrico-select-lyric-line" value="Seerungal vaarungal vaarungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyin kolangal idhu ungal
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhoomiyin kolangal idhu ungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam inimel ulagam parkka povathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam inimel ulagam parkka povathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithaiyin veerangal
<input type="checkbox" class="lyrico-select-lyric-line" value="Manithaiyin veerangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oooo..ohooho…ohooo..
<input type="checkbox" class="lyrico-select-lyric-line" value="Oooo..ohooho…ohooo.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Singapennae singapennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Singapennae singapennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaninamae unnai vanangumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaninamae unnai vanangumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandri kadan theerpatharkku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nandri kadan theerpatharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvilae unnai yenthumae…hae
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuvilae unnai yenthumae…hae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai thalai kuni
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru murai thalai kuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee rendu singa mugaththai
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee rendu singa mugaththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarpatharkku mattumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarpatharkku mattumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yeru yeru yeru
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeru yeru yeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil valimai kondu yeru
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjil valimai kondu yeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pennendru gaeli seidha
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai pennendru gaeli seidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootam oru naal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootam oru naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vanangidum uyarnthu nillu (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai vanangidum uyarnthu nillu"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Singapennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Singapennae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Amaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Amaam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Singapennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Singapennae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Singapennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Singapennae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Heyy eeyy eyy eyy eyy eyy heyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy eeyy eyy eyy eyy eyy heyy"/>
</div>
  <div class="lyrico-lyrics-wrapper">Aaninamae unnai vanangumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaninamae unnai vanangumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandri kadan theerpatharkku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nandri kadan theerpatharkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvilae unnai yenthumae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuvilae unnai yenthumae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yeru yeru yeru ..yeru
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeru yeru yeru ..yeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil valimai kondu yeru…yeru
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjil valimai kondu yeru…yeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pennendru gaeli seidha
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai pennendru gaeli seidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootam oru naal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootam oru naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vanangidum uyarnthu nillu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai vanangidum uyarnthu nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Annai thangai manaivi endru
<input type="checkbox" class="lyrico-select-lyric-line" value="Annai thangai manaivi endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenadiththa viyarvai undhan
<input type="checkbox" class="lyrico-select-lyric-line" value="Veenadiththa viyarvai undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhaikkul pattrum andha
<input type="checkbox" class="lyrico-select-lyric-line" value="Paadhaikkul pattrum andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyai anaikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Theeyai anaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee bhayamindri thuninthu sellu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee bhayamindri thuninthu sellu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oooo..ohooho…ohooo..
<input type="checkbox" class="lyrico-select-lyric-line" value="Oooo..ohooho…ohooo.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heyy …..(8 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Heyy"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ohooo..ohooho.ohooo..hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ohooo..ohooho.ohooo..hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo..ohooho.ohooo..hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ohooo..ohooho.ohooo..hey"/>
</div>
<div class="lyrico-lyrics-wrapper">O.o.o.o.o.o.o……………………..
<input type="checkbox" class="lyrico-select-lyric-line" value="O.o.o.o.o.o.o"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unnaalae mudiyaadhenru
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnaalae mudiyaadhenru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae sollum nambaathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oorae sollum nambaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Parithaabam kattum yendha
<input type="checkbox" class="lyrico-select-lyric-line" value="Parithaabam kattum yendha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vargathodum iniyaathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vargathodum iniyaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey..unnaalae mudiyaadhenru
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey..unnaalae mudiyaadhenru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae sollum nambaathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oorae sollum nambaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi parithaabam kattum yendha
<input type="checkbox" class="lyrico-select-lyric-line" value="Poi parithaabam kattum yendha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vargathodum iniyaathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vargathodum iniyaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ulagathin valiyelaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulagathin valiyelaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaal enna unmunnae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandhaal enna unmunnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Prasavathin valiyaithaanda
<input type="checkbox" class="lyrico-select-lyric-line" value="Prasavathin valiyaithaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirantha akkini siragae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pirantha akkini siragae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ezhunthu vaaa..
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhunthu vaaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai asaipom…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulagai asaipom…"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarnthu vaaa….
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyarnthu vaaa…."/>
</div>
<div class="lyrico-lyrics-wrapper">Akkini siragae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Akkini siragae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthu vaaa..aa…aaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhunthu vaaa..aa…aaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Un oli vidum kanaavai serpom vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Un oli vidum kanaavai serpom vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu sagathiyil vilaamal paarpom vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhu sagathiyil vilaamal paarpom vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yeru yeru yeru ..yeru
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeru yeru yeru ..yeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil valimai kondu yeru…yeru
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjil valimai kondu yeru…yeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pennendru gaeli seidha
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai pennendru gaeli seidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootam oru naal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootam oru naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vanangidum uyarnthu nillu (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai vanangidum uyarnthu nillu"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nana nana nana..
<input type="checkbox" class="lyrico-select-lyric-line" value="Nana nana nana.."/>
</div>
<div class="lyrico-lyrics-wrapper">Na na nannaaaannananana
<input type="checkbox" class="lyrico-select-lyric-line" value="Na na nannaaaannananana"/>
</div>
<div class="lyrico-lyrics-wrapper">Idho kaayangal maarum kalangaathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idho kaayangal maarum kalangaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thunbam veezhnthaarum
<input type="checkbox" class="lyrico-select-lyric-line" value="Un thunbam veezhnthaarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unakaaga neeyae uthipaai ammaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakaaga neeyae uthipaai ammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhaatral unarnthiduvaaai..
<input type="checkbox" class="lyrico-select-lyric-line" value="Unadhaatral unarnthiduvaaai.."/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyal ondrai koovi yetruvaai…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidiyal ondrai koovi yetruvaai…"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyal ondrai koovi yetruvaai…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidiyal ondrai koovi yetruvaai…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Singapennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Singapennae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey"/>
</div>
  <div class="lyrico-lyrics-wrapper">Singapennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Singapennae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hey hey
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey"/>
</div>
  <div class="lyrico-lyrics-wrapper">Aaninamae unnai vanangumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaninamae unnai vanangumae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Aaninamae unnai vanangumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaninamae unnai vanangumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nandri kadan theerpathaarkku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nandri kadan theerpathaarkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvilae unnai yenthumae…ae
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuvilae unnai yenthumae…ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai thalai kuni
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru murai thalai kuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee rendu singa mugaththai
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee rendu singa mugaththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarpatharkku mattumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarpatharkku mattumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yeru yeru yeru ..
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeru yeru yeru .."/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil valimai kondu yeru…
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjil valimai kondu yeru…"/>
</div>
  <div class="lyrico-lyrics-wrapper">Unnai pennendru gaeli seidha
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai pennendru gaeli seidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootam oru naal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootam oru naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vanangidum uyarnthu nillu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai vanangidum uyarnthu nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paaru paaru paaru paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Paaru paaru paaru paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yeru yeru yeru ..
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeru yeru yeru .."/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil valimai kondu yeru… 
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjil valimai kondu yeru… "/>
</div>
  <div class="lyrico-lyrics-wrapper">Unnai pennendru gaeli seidha
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai pennendru gaeli seidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootam oru naal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootam oru naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vanangidum uyarnthu nillu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai vanangidum uyarnthu nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Singapennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Singapennae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Singapennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Singapennae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Singapennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Singapennae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Singapennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Singapennae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Aaninamae unnai vanangumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaninamae unnai vanangumae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Aaninamae unnai vanangumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaninamae unnai vanangumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nandri kadan theerpathaarkku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nandri kadan theerpathaarkku"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvilae unnai yenthumae…ae
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuvilae unnai yenthumae…ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru murai thalai kuni
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru murai thalai kuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee rendu singa mugaththai
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee rendu singa mugaththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarpatharkku mattumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarpatharkku mattumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oooo..ohooho…ohooo..
<input type="checkbox" class="lyrico-select-lyric-line" value="Oooo..ohooho…ohooo.."/>
</div>
<div class="lyrico-lyrics-wrapper">Oooo..ohooho…ohooo..
<input type="checkbox" class="lyrico-select-lyric-line" value="Oooo..ohooho…ohooo.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yeru yeru yeru ..
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeru yeru yeru .."/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil valimai kondu yeru…
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjil valimai kondu yeru…"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pennendru gaeli seidha
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai pennendru gaeli seidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootam oru naal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootam oru naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vanangidum uyarnthu nillu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai vanangidum uyarnthu nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Annai thangai manaivi endru
<input type="checkbox" class="lyrico-select-lyric-line" value="Annai thangai manaivi endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenadiththa viyarvai undhan
<input type="checkbox" class="lyrico-select-lyric-line" value="Veenadiththa viyarvai undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhaikkul pattrum andha
<input type="checkbox" class="lyrico-select-lyric-line" value="Paadhaikkul pattrum andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyai anaikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Theeyai anaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee bhayamindri nee bhayamindri
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee bhayamindri nee bhayamindri"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee bhayamindri thuninthu sellu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee bhayamindri thuninthu sellu"/>
</div>
</pre>