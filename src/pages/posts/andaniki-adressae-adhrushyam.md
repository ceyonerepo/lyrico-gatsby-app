---
title: "andaniki adressae song lyrics"
album: "Adhrushyam"
artist: "Aldrin"
lyricist: "Vennelakanti"
director: "Ravi Prakash Krishnamsetty"
path: "/albums/adhrushyam-lyrics"
song: "Andaniki Adressae"
image: ../../images/albumart/adhrushyam.jpg
date: 2019-03-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wP9pPFmrpxg"
type: "happy"
singers:
  - Sindhuri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">andaniki addressae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andaniki addressae"/>
</div>
<div class="lyrico-lyrics-wrapper">nee mundhey thirigesthuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mundhey thirigesthuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">half dressu paapalakey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="half dressu paapalakey"/>
</div>
<div class="lyrico-lyrics-wrapper">padipothey nee bad luckey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padipothey nee bad luckey"/>
</div>
<div class="lyrico-lyrics-wrapper">milkyway na tabbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="milkyway na tabbu"/>
</div>
<div class="lyrico-lyrics-wrapper">jabilley na sabbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jabilley na sabbu"/>
</div>
<div class="lyrico-lyrics-wrapper">vennalaney powder ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennalaney powder ga"/>
</div>
<div class="lyrico-lyrics-wrapper">veskunta make uppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veskunta make uppu"/>
</div>
<div class="lyrico-lyrics-wrapper">beauty antey ne firstu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beauty antey ne firstu"/>
</div>
<div class="lyrico-lyrics-wrapper">babes lona ne bestu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="babes lona ne bestu"/>
</div>
<div class="lyrico-lyrics-wrapper">neeku dhorikithey feastu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeku dhorikithey feastu"/>
</div>
<div class="lyrico-lyrics-wrapper">leda lifeu wasteu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="leda lifeu wasteu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">selfie theedhamani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="selfie theedhamani "/>
</div>
<div class="lyrico-lyrics-wrapper">ne click chesthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne click chesthey"/>
</div>
<div class="lyrico-lyrics-wrapper">cameralu kanney kottey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cameralu kanney kottey"/>
</div>
<div class="lyrico-lyrics-wrapper">flash naa skin thalathala choosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="flash naa skin thalathala choosi"/>
</div>
<div class="lyrico-lyrics-wrapper">siggu paduthu half mode ettey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siggu paduthu half mode ettey"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kanna andhanga filters
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kanna andhanga filters"/>
</div>
<div class="lyrico-lyrics-wrapper">choopalemani golettey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choopalemani golettey"/>
</div>
<div class="lyrico-lyrics-wrapper">kolathaley kevvu kekkantunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolathaley kevvu kekkantunna"/>
</div>
<div class="lyrico-lyrics-wrapper">ankalanni aripinchey vanney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ankalanni aripinchey vanney"/>
</div>
<div class="lyrico-lyrics-wrapper">figuru soku naa sommey kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="figuru soku naa sommey kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">miss universe nenamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="miss universe nenamma"/>
</div>
<div class="lyrico-lyrics-wrapper">missu ayyava ni karma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="missu ayyava ni karma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hip hop lo hal chal chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hip hop lo hal chal chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">hippu dansuley veseyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hippu dansuley veseyna"/>
</div>
<div class="lyrico-lyrics-wrapper">pop sonu ke oopekinchey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pop sonu ke oopekinchey"/>
</div>
<div class="lyrico-lyrics-wrapper">thopu songule padena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thopu songule padena"/>
</div>
<div class="lyrico-lyrics-wrapper">ice lona ice kaantham unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ice lona ice kaantham unna"/>
</div>
<div class="lyrico-lyrics-wrapper">softy ice cream neney ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="softy ice cream neney ra"/>
</div>
<div class="lyrico-lyrics-wrapper">okka navvukey flat ayipora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okka navvukey flat ayipora"/>
</div>
<div class="lyrico-lyrics-wrapper">okka choopukey dung ayipora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okka choopukey dung ayipora"/>
</div>
<div class="lyrico-lyrics-wrapper">okka touchkey moksham raadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okka touchkey moksham raadha"/>
</div>
<div class="lyrico-lyrics-wrapper">punyamedho nuvvu cheskuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punyamedho nuvvu cheskuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">devathayindhiley nee jantey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devathayindhiley nee jantey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">andaniki addressae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andaniki addressae"/>
</div>
<div class="lyrico-lyrics-wrapper">nee mundhey thirigesthuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee mundhey thirigesthuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">half dressu paapalakey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="half dressu paapalakey"/>
</div>
<div class="lyrico-lyrics-wrapper">padipothey nee bad luckey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padipothey nee bad luckey"/>
</div>
<div class="lyrico-lyrics-wrapper">milkyway na tabbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="milkyway na tabbu"/>
</div>
<div class="lyrico-lyrics-wrapper">jabilley na sabbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jabilley na sabbu"/>
</div>
<div class="lyrico-lyrics-wrapper">vennalaney powder ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennalaney powder ga"/>
</div>
<div class="lyrico-lyrics-wrapper">veskunta make uppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veskunta make uppu"/>
</div>
</pre>
