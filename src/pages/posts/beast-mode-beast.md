---
title: "beast mode song lyrics"
album: "Beast"
artist: "Anirudh Ravichander"
lyricist: "Sivakarthikeyan"
director: "Vivek"
path: "/albums/beast-lyrics"
song: "Beast Mode"
image: ../../images/albumart/beast.jpg
date: 2022-04-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KOwDgUzijCI"
type: "mass"
singers:
  - Anirudh Ravichandran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Meaner leaner stronger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meaner leaner stronger"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you feel the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you feel the"/>
</div>
<div class="lyrico-lyrics-wrapper">Power terror fire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power terror fire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meaner leaner stronger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meaner leaner stronger"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you feel the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you feel the"/>
</div>
<div class="lyrico-lyrics-wrapper">Power terror fire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power terror fire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirai thee pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirai thee pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedi vedikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedi vedikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruthan vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthan vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Padai nadungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padai nadungum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meaner leaner stronger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meaner leaner stronger"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you feel the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you feel the"/>
</div>
<div class="lyrico-lyrics-wrapper">Power terror fire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power terror fire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bayama irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayama irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhukkapram innum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhukkapram innum "/>
</div>
<div class="lyrico-lyrics-wrapper">bayangarama irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayangarama irukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan mel idikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan mel idikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottamellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholvi mattum palagidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholvi mattum palagidanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meaner leaner stronger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meaner leaner stronger"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you feel the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you feel the"/>
</div>
<div class="lyrico-lyrics-wrapper">Power terror fire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power terror fire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala perin mugamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala perin mugamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu aadura puli dhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu aadura puli dhanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velayaada ninacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velayaada ninacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vidhi mudipaaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vidhi mudipaaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila perin peyar dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila perin peyar dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru aalumai perumaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru aalumai perumaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru veeran thadamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru veeran thadamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kaayam vaangura edamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaayam vaangura edamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meaner leaner stronger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meaner leaner stronger"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you feel the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you feel the"/>
</div>
<div class="lyrico-lyrics-wrapper">Power terror fire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power terror fire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meaner leaner stronger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meaner leaner stronger"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you feel the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you feel the"/>
</div>
<div class="lyrico-lyrics-wrapper">Power terror fire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power terror fire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evan vaazhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan vaazhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Evan poganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan poganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai yosichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai yosichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinjaa nerunganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinjaa nerunganum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thani raanuvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani raanuvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi aayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi aayudham"/>
</div>
<div class="lyrico-lyrics-wrapper">alasaadha ngoppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alasaadha ngoppan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan dhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan dhanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna polavey oru aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna polavey oru aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha kaalamum adaiyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha kaalamum adaiyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha per pugazh mudiyathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha per pugazh mudiyathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum yerumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum yerumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala perin mugamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala perin mugamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu aadura puli dhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu aadura puli dhanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velayaada ninacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velayaada ninacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vidhi mudipaaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vidhi mudipaaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila perin peyar dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila perin peyar dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru aalumai perumaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru aalumai perumaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru veeran thadamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru veeran thadamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kaayam vaangura edamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaayam vaangura edamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirai thee pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirai thee pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedi vedikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedi vedikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruthan vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthan vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Padai nadungum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padai nadungum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan mel idikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan mel idikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottmellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottmellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholvi mattum palagidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholvi mattum palagidanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meaner leaner stronger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meaner leaner stronger"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you feel the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you feel the"/>
</div>
<div class="lyrico-lyrics-wrapper">Power terror fire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power terror fire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meaner leaner stronger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meaner leaner stronger"/>
</div>
<div class="lyrico-lyrics-wrapper">Can you feel the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can you feel the"/>
</div>
<div class="lyrico-lyrics-wrapper">Power terror fire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power terror fire"/>
</div>
</pre>
