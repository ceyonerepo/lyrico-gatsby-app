---
title: "aanakatti aalavantha song lyrics"
album: "Anbirkiniyal"
artist: "Javed Riaz"
lyricist: "Lalithanand"
director: "Gokul"
path: "/albums/anbirkiniyal-lyrics"
song: "Aanakatti Aalavantha"
image: ../../images/albumart/anbirkiniyal.jpg
date: 2021-03-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6brklQNGg8w"
type: "Affection"
singers:
  - Sriram Parthasarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aanakatti aalavandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanakatti aalavandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanakatti aalavandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanakatti aalavandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazhgala varalaru neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazhgala varalaru neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanimuththe ozhi veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanimuththe ozhi veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirkaalam paaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirkaalam paaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maada vizhakke eppothum aruge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maada vizhakke eppothum aruge"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee irunthale nimmathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee irunthale nimmathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maandal kooda unakkaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maandal kooda unakkaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir poga sammathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir poga sammathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netru antha vaanil paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru antha vaanil paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mega koottam neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mega koottam neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai intha bhoomi pookum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai intha bhoomi pookum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovum kooda neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovum kooda neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanave unai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave unai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalile sumappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalile sumappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire unai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire unai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaisi varai kaappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaisi varai kaappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanakatti aalavandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanakatti aalavandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanakatti aalavandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanakatti aalavandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazhgala varalaru neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazhgala varalaru neeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanimuththe ozhi veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanimuththe ozhi veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirkaalam paaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirkaalam paaradi"/>
</div>
</pre>
