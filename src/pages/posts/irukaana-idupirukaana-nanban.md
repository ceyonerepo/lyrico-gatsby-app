---
title: "irukaana idupirukaana song lyrics"
album: "Nanban"
artist: "Harris Jayaraj"
lyricist: "Pa Vijay"
director: "S. Shankar"
path: "/albums/nanban-lyrics"
song: "Irukaana Idupirukaana"
image: ../../images/albumart/nanban.jpg
date: 2012-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/a0cFlE9TmCM"
type: "Love"
singers:
  - Vijay Prakash
  - Javed Ali
  - Sunidhi Chauhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Labakku Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labakku Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Labakku Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labakku Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalappiyaane Kalappiyaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalappiyaane Kalappiyaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkaana Idupirukkaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkaana Idupirukkaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaiyaanaa Illieana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaiyaanaa Illieana"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Idai Thaana Inbaa Kadaithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idai Thaana Inbaa Kadaithaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mellidai Thaandi Udai Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellidai Thaandi Udai Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Ikkani Mukkani Mugadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ikkani Mukkani Mugadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thutha Naga Thagudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thutha Naga Thagudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Udhatukul Ethana Udhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Udhatukul Ethana Udhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Kudu Kudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Kudu Kudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Anjana Manjana Mayilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Anjana Manjana Mayilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kanjan Janga Railu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kanjan Janga Railu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Iduppe Aaram Viralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Iduppe Aaram Viralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gilu Gilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gilu Gilu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olli Belly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olli Belly"/>
</div>
<div class="lyrico-lyrics-wrapper">Jelly Belly Malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jelly Belly Malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam Malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Meni Vengala Velli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Meni Vengala Velli"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Alli Killi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Alli Killi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam Killi Vizhayaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam Killi Vizhayaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhavan Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhavan Gilli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olli Belly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olli Belly"/>
</div>
<div class="lyrico-lyrics-wrapper">Jelly Belly Malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jelly Belly Malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam Malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Malli"/>
</div>
<div class="lyrico-lyrics-wrapper">En Meni Vengala Velli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Meni Vengala Velli"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Alli Killi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Alli Killi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam Killi Vizhayaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam Killi Vizhayaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhavan Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhavan Gilli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oruvaati Idupaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruvaati Idupaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai Eraka Erakathila Thallaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Eraka Erakathila Thallaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edangaati Thadangaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edangaati Thadangaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena Araka Paraka Vanthu Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Araka Paraka Vanthu Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adangaati Madangaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangaati Madangaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai Uraika Uraika Mutham Veiyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai Uraika Uraika Mutham Veiyen"/>
</div>
<div class="lyrico-lyrics-wrapper">Padankaati Bayam Kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padankaati Bayam Kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju Eraika Eraika Thapu Seiyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju Eraika Eraika Thapu Seiyen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Chinna PaiyenPaiyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Chinna PaiyenPaiyen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kanna Veiyen Veiyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kanna Veiyen Veiyen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Sonna Seiyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sonna Seiyen"/>
</div>
<div class="lyrico-lyrics-wrapper">Va Vayil Vazhai Vaayen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Vayil Vazhai Vaayen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Labakku Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labakku Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Labakku Raani Kumbathu Maganee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labakku Raani Kumbathu Maganee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalappiyaane Kalappiyaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalappiyaane Kalappiyaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakshalube Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakshalube Be"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olli Belly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olli Belly"/>
</div>
<div class="lyrico-lyrics-wrapper">Jelly Belly Malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jelly Belly Malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam Malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Meni Vengala Velli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Meni Vengala Velli"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Alli Killi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Alli Killi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam Killi Vizhayaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam Killi Vizhayaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhavan Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhavan Gilli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laichaana Laichaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laichaana Laichaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Idha Lavangam Lavangam Kadichaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha Lavangam Lavangam Kadichaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Inichaana Inichaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inichaana Inichaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai Manalil Kadala Thinichaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai Manalil Kadala Thinichaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kozhukkaana Molukkaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhukkaana Molukkaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Pazhuthu Pazhuthu Thalukkaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Pazhuthu Pazhuthu Thalukkaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaichaana Kozhachaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaichaana Kozhachaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba Sedhukki Sedhuki Ozhachaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Sedhukki Sedhuki Ozhachaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Chengisthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Chengisthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Un Kiss Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Un Kiss Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mangose Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mangose Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaiyil Kajakasthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaiyil Kajakasthaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Labakku Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labakku Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Labakku Raani Kumbathu Maganee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Labakku Raani Kumbathu Maganee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalappiyaane Kalappiyaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalappiyaane Kalappiyaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakshalube
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rakshalube"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkaana Idupirukkaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkaana Idupirukkaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaiyaanaa Illieana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaiyaanaa Illieana"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Idai Thaana Inbaa Kadaithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Idai Thaana Inbaa Kadaithaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mellidai Thaandi Udai Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellidai Thaandi Udai Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Ikkani Mukkani Mugadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ikkani Mukkani Mugadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thutha Naga Thagudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thutha Naga Thagudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Udhatukul Ethana Udhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Udhatukul Ethana Udhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Kudu Kudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Kudu Kudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Anjana Manjana Mayilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Anjana Manjana Mayilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kanjan Janga Railu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kanjan Janga Railu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Iduppe Aaram Viralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Iduppe Aaram Viralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gilu Gilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gilu Gilu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olli Belly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olli Belly"/>
</div>
<div class="lyrico-lyrics-wrapper">Jelly Belly Malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jelly Belly Malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam Malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Meni Vengala Velli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Meni Vengala Velli"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Alli Killi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Alli Killi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam Killi Vizhayaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam Killi Vizhayaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhavan Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhavan Gilli"/>
</div>
</pre>
