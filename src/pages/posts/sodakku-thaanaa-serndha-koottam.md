---
title: "sodakku song lyrics"
album: "Thaanaa Serndha Koottam"
artist: "Anirudh Ravichander"
lyricist: "Vignesh Shivan - Mani Amudhavan"
director: "Vignesh Shivan"
path: "/albums/thaanaa-serndha-koottam-lyrics"
song: "Sodakku"
image: ../../images/albumart/thaanaa-serndha-koottam.jpg
date: 2018-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pn6M7_L1JbQ"
type: "mass"
singers:
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sodakku mela sodakku poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela sodakku poduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En verallu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En verallu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu theruvil ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu theruvil ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodakku mela sodakku poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela sodakku poduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sodakku mela sodakku poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela sodakku poduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En verallu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En verallu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu theruvil ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu theruvil ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodakku mela sodakku poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela sodakku poduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayya vangayya vangayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya vangayya vangayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Engayya irukeenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engayya irukeenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyya seiveenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyya seiveenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppayya seiveenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppayya seiveenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sodakku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey sodakku mela appadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sodakku mela appadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodakku mela sodakku poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela sodakku poduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En verallu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En verallu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu theruvil ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu theruvil ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodakku mela sodakku poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela sodakku poduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayya vangayya vangayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya vangayya vangayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Engayya irukeenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engayya irukeenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyya seiveenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyya seiveenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppayya seiveenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppayya seiveenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sodakku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey sodakku mela appadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sodakku mela appadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodakku mela sodakku poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela sodakku poduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En verallu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En verallu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu theruvil ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu theruvil ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodakku mela sodakku poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela sodakku poduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sodakku mela sodakku poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela sodakku poduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En verallu vanthu hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En verallu vanthu hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu theruvil ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu theruvil ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodakku mela sodakku poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela sodakku poduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadakuravana parakka vidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakuravana parakka vidanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aluguravana sirikka vidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aluguravana sirikka vidanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Modangunavana thodanga vidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modangunavana thodanga vidanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangunavana kalakka vidanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangunavana kalakka vidanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadukka thadukka thaandi varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukka thadukka thaandi varanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Midhika midhika meendu varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhika midhika meendu varanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothikka kothikka kovam varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothikka kothikka kovam varanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Keela pothacha molachu varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keela pothacha molachu varanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sodakku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey sodakku mela appadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sodakku mela appadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodakku mela sodakku poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela sodakku poduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey thadukkuravana kedukkiravana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thadukkuravana kedukkiravana"/>
</div>
<div class="lyrico-lyrics-wrapper">Morachu paakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morachu paakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala ganathula kuthikkiravana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala ganathula kuthikkiravana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarichu paakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarichu paakanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi vayithula adikkiravana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vayithula adikkiravana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuthu kekanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuthu kekanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini oru mura namma thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini oru mura namma thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan nenachu paakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan nenachu paakanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koduththa koduththa adiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduththa koduththa adiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppi thiruppi tharanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppi thiruppi tharanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluththa koluththa eliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluththa koluththa eliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluppa kuraikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluppa kuraikkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aduththa aduththa nodithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduththa aduththa nodithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenacha maari varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenacha maari varanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adachcha adachcha kathava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adachcha adachcha kathava"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthachchu thorakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthachchu thorakkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayya pongayya pongayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya pongayya pongayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanama pongayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanama pongayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu munna vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu munna vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam dhaan veengumya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam dhaan veengumya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Angayyo ingayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angayyo ingayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Engaiyo pongayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engaiyo pongayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga side vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga side vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Injury aagumya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Injury aagumya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sodakku mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey sodakku mela appadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey sodakku mela appadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodakku mela sodakku poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela sodakku poduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En verallu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En verallu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu theruvil ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu theruvil ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodakku mela sodakku poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela sodakku poduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo enna kalatitomnnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo enna kalatitomnnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha aatam elaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha aatam elaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha maathiri paatellam potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha maathiri paatellam potu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadanumna naama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadanumna naama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethavathu pannirukanumla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethavathu pannirukanumla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei athaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei athaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaththa kilichitomnnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaththa kilichitomnnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha aattam paattam keetamlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha aattam paattam keetamlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku velai kidaichiducha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku velai kidaichiducha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennada puthusa etho kettunu irukkaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennada puthusa etho kettunu irukkaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aana ithellam namma thappu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana ithellam namma thappu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga irukkira alukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga irukkira alukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha alukka uruvakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha alukka uruvakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha alukkula oori ponaidho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha alukkula oori ponaidho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanunga maari aalunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanunga maari aalunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanungala ellam paathaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanungala ellam paathaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veratti veratti velukka thonuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veratti veratti velukka thonuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu veratti veratti velukka thonuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu veratti veratti velukka thonuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhigara thimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhigara thimira"/>
</div>
<div class="lyrico-lyrics-wrapper">Panakkaara powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panakkaara powera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookki pottu midhika thonuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookki pottu midhika thonuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey thatti dhaan thookkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thatti dhaan thookkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniya kaatanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniya kaatanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda oda vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda oda vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttiya pekkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttiya pekkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kootaththa sekkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootaththa sekkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththuna kekkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththuna kekkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Illathavan vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illathavan vali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanu kaatanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanu kaatanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veratti veratti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veratti veratti"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey poratti poratti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey poratti poratti"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna veratti veratti velukka thonuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna veratti veratti velukka thonuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athigaara thimira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athigaara thimira"/>
</div>
<div class="lyrico-lyrics-wrapper">Panakkaara powera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panakkaara powera"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookki pottu midhika thonuthu hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookki pottu midhika thonuthu hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sodakku mela sodakku poduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodakku mela sodakku poduthu"/>
</div>
</pre>
