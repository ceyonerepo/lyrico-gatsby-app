---
title: "jagada thom song lyrics"
album: "Deiva Thirumagal"
artist: "G.V. Prakash Kumar"
lyricist: "Na. Muthukumar"
director: "A.L. Vijay"
path: "/albums/deiva-thirumagal-lyrics"
song: "Jagada Thom"
image: ../../images/albumart/deiva-thirumagal.jpg
date: 2011-07-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NbPHpEjaA8Y"
type: "mass"
singers:
  - S.P. Balasubrahmanyam
  - Maya
  - Rajesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jagada Thom Jagada Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Thom Jagada Thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaiye Porkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiye Porkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagada Thom Jagada Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Thom Jagada Thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthuvom Sarithiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthuvom Sarithiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadanthu Nadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthu Nadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal Theyalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Theyalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhithu Vizhithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhithu Vizhithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Moodalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Moodalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruntha Pothum Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruntha Pothum Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Porile Mothalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porile Mothalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irutai Virata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutai Virata"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Sooriyan Adutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sooriyan Adutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirupathilum Thondralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirupathilum Thondralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaitha Kanavu Kai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaitha Kanavu Kai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodalaam Koodalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodalaam Koodalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagada Thom Jagada Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Thom Jagada Thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaiye Porkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiye Porkalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palakodi Kaalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palakodi Kaalangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannukul Vaazhnthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannukul Vaazhnthaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Karithundu Vaazhkai Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karithundu Vaazhkai Oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal Vairamaaga Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal Vairamaaga Maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varalaatril Ennaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaatril Ennaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali Indri Vaazhvillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Indri Vaazhvillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Valithaane Vetriyil Yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valithaane Vetriyil Yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeni Ondru Podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeni Ondru Podum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theemaiyai Theeyida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemaiyai Theeyida"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemaiyai Naadidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemaiyai Naadidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutram Adhil Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram Adhil Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottathai Kaathida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottathai Kaathida"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyil Mutkalai Vaithaal Thavarillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyil Mutkalai Vaithaal Thavarillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannil Kaarkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Kaarkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Indre Maaraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Indre Maaraatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Pookaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Pookaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Naalai Vaaraatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Naalai Vaaraatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadanthu Nadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthu Nadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal Theyalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Theyalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhithu Vizhithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhithu Vizhithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Moodalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Moodalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruntha Pothum Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruntha Pothum Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Porile Mothalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porile Mothalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irutai Virata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutai Virata"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Sooriyan Adutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sooriyan Adutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirupathilum Thondralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirupathilum Thondralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaitha Kanavu Kai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaitha Kanavu Kai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodalaam Koodalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodalaam Koodalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagada Thom Jagada Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Thom Jagada Thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaiye Porkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiye Porkalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudiyaatha Paathaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyaatha Paathaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaiyaathu Man Meethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaiyaathu Man Meethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnerum Nathigal Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnerum Nathigal Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallam Paarthitadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallam Paarthitadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyaatha Naatkalthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyaatha Naatkalthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaiyaathu Vinmeethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaiyaathu Vinmeethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi Sinthum Eeram Patu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Sinthum Eeram Patu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Moozhgidathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Moozhgidathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalayam Enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalayam Enbathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kopura Vaasalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopura Vaasalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Silaiyum Matumthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silaiyum Matumthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhai Vida Melyedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Vida Melyedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandavan Vaazhum Nallor Ullamthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavan Vaazhum Nallor Ullamthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaimai Endraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaimai Endraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Un Pol Aagaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Un Pol Aagaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai Nenjamthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Nenjamthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Unnai Pol Yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Unnai Pol Yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadanthu Nadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthu Nadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal Theyalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Theyalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhithu Vizhithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhithu Vizhithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Moodalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Moodalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruntha Pothum Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruntha Pothum Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Porile Mothalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porile Mothalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irutai Virata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutai Virata"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Sooriyan Adutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sooriyan Adutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirupathilum Thondralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirupathilum Thondralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaitha Kanavu Kai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaitha Kanavu Kai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodalaam Koodalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodalaam Koodalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagada Thom Jagada Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Thom Jagada Thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaiye Porkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiye Porkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagada Thom Jagada Thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Thom Jagada Thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthuvom Sarithiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthuvom Sarithiram"/>
</div>
</pre>
