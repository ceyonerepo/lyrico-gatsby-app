---
title: "jilla asanthu nikkum song lyrics"
album: "MGR Magan"
artist: "Anthony Daasan"
lyricist: "Yugabharathi"
director: "Ponram"
path: "/albums/mgr-magan-lyrics"
song: "Jilla Asanthu Nikkum"
image: ../../images/albumart/mgr-magan.jpg
date: 2021-11-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qeOyD3KnqtQ"
type: "happy"
singers:
  - Meenakshi Elayaraja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jilla asandhu nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla asandhu nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukoda chithi ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukoda chithi ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla udukku adicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla udukku adicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku naan aththai ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku naan aththai ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru sanam koodi ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru sanam koodi ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthamiya pol iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthamiya pol iruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Othaiyila neeyum vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othaiyila neeyum vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Motha kadhai naan padippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motha kadhai naan padippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Motha kadhai naan padippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motha kadhai naan padippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilla asandhu nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla asandhu nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukoda chithi ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukoda chithi ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla udukku adicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla udukku adicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku naan aththai ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku naan aththai ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru sanam koodi ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru sanam koodi ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthamiya pol iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthamiya pol iruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Othaiyila neeyum vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othaiyila neeyum vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Motha kadhai naan padippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motha kadhai naan padippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattasu vangan naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattasu vangan naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga inga ponadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga inga ponadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana naan mathappunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana naan mathappunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivagasiyil pechirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivagasiyil pechirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Neiyaana neiya vanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neiyaana neiya vanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uththukuli ponadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uththukuli ponadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana naan vennayinum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana naan vennayinum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mailapuru kaathirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mailapuru kaathirukken"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marikozhunthu vanga naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marikozhunthu vanga naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamadurai ponadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamadurai ponadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana en vaasathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana en vaasathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Javvathumae koothurukkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Javvathumae koothurukkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathula naan mungayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathula naan mungayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Meenum thalli poguthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenum thalli poguthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambalaga kannu mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambalaga kannu mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandapadi meiyuthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandapadi meiyuthamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilla asandhu nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla asandhu nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukoda chithi ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukoda chithi ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla udukku adicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla udukku adicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku naan aththai ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku naan aththai ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattantharaiyila naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattantharaiyila naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikku kolam pottadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikku kolam pottadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana en kannukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana en kannukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Color-u pola nooru irukkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color-u pola nooru irukkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarupadai ayyanukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarupadai ayyanukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Maavizhakku pottadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maavizhakku pottadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana naan kettathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana naan kettathellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli thara aalu irukkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli thara aalu irukkae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagan sittu naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagan sittu naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Poomalai pottadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poomalai pottadhillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana en thozh sehra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana en thozh sehra"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram poo poothirukkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram poo poothirukkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koda idu koyilyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koda idu koyilyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothedukka en pozhappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothedukka en pozhappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodi neenga nikkayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodi neenga nikkayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Angam ellam pul arippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angam ellam pul arippu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilla asandhu nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilla asandhu nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Silukoda chithi ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silukoda chithi ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla udukku adicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla udukku adicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku naan aththai ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku naan aththai ponnu"/>
</div>
</pre>
