---
title: "theeratha vilayattu pillai song lyrics"
album: "Kavan"
artist: "Hiphop Tamizha"
lyricist: "Mahakavi Subramania Bharati"
director: "K V Anand"
path: "/albums/kavan-lyrics"
song: "Theeratha Vilayattu Pillai"
image: ../../images/albumart/kavan.jpg
date: 2017-03-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/yDKTZemqVZo"
type: "happy"
singers:
  - Hiphop Tamizha
  - Padmalatha
  - Anthony Daasan
  - Georgina Mathew
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paayum oli nee enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayum oli nee enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkum vizhi naan unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkum vizhi naan unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoyum madhu nee enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoyum madhu nee enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thumbi adi naan unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumbi adi naan unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayuraikka varuguthillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayuraikka varuguthillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhi nindran menmai ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhi nindran menmai ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooya sudar vaan oliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooya sudar vaan oliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Soorai amuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorai amuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannammaa en kaadhalii eee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannammaa en kaadhalii eee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannammaa en kaadhali ee ohooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannammaa en kaadhali ee ohooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal adi ne enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal adi ne enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaandham adi naan unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaandham adi naan unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedham adi nee enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedham adi nee enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Viththai adi naan unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viththai adi naan unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhumutra podhinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhumutra podhinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongi varum theenjuvaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongi varum theenjuvaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatha vadivaanavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatha vadivaanavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalluyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalluyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeraadha vilaiyaatu pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraadha vilaiyaatu pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraadha vilaiyaatu pillai aahaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraadha vilaiyaatu pillai aahaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraadha vilaiyaatu pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraadha vilaiyaatu pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraadha vilaiyaatu pillaiiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraadha vilaiyaatu pillaiiii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theruvilae pengalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvilae pengalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyaatha thollaiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaatha thollaiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraadha vilaiyaatu pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraadha vilaiyaatu pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thom tharikita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thom tharikita"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thiranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thiranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thiranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thiranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thiranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thiranaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannammaa en kaadhalii ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannammaa en kaadhalii ee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannammaa en kaadhali ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannammaa en kaadhali ee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallauyir nee enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallauyir nee enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadiyadi naan unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadiyadi naan unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Selvamadi nee enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selvamadi nee enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Semanidhi naan unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semanidhi naan unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaiyatra perazhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaiyatra perazhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum nirai porchudarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum nirai porchudarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullai nigar punnagaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullai nigar punnagaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhum inbamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhum inbamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeraadha vilaiyaatu pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraadha vilaiyaatu pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraadha vilaiyaatu pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraadha vilaiyaatu pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraadha vilaiyaatu pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraadha vilaiyaatu pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannan"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraadha vilaiyaatu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraadha vilaiyaatu "/>
</div>
<div class="lyrico-lyrics-wrapper">pillai hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pillai hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theruvilae pengalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvilae pengalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyaatha thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaatha thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraadha vilaiyaatu pillai ohooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraadha vilaiyaatu pillai ohooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Theruvilae pengalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvilae pengalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyaatha thollai yeeiii ohooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaatha thollai yeeiii ohooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraadha vilaiyaatu pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraadha vilaiyaatu pillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannammaa kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannammaa kaadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannammaa en kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannammaa en kaadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhali en kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhali en kaadhali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallauyir nee enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallauyir nee enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadiyadi naan unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadiyadi naan unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Selvamadi nee enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selvamadi nee enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Semanidhi naan unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semanidhi naan unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaiyatra perazhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaiyatra perazhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum nirai porchudarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum nirai porchudarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullai nigar punnagaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullai nigar punnagaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhum inbamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhum inbamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi hoihoi hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoihoi hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanae thanae thanae thanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanae thanae thanae thanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanae tanae tanae tanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanae tanae tanae tanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana naa nae thaana naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana naa nae thaana naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae naanae naa nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannae naanae naa nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana naa nae thaana naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana naa nae thaana naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae naanae naa nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannae naanae naa nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanaaaaaaahoi hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanaaaaaaahoi hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nae thana nae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana nae thana nae "/>
</div>
<div class="lyrico-lyrics-wrapper">thana nae thana nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thana nae thana nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana nan nae hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana nan nae hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannammaa kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannammaa kaadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannammaa en kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannammaa en kaadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhali en kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhali en kaadhali"/>
</div>
</pre>
