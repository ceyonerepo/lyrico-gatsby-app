---
title: "varaamale vandhaale song lyrics"
album: "Thirumanam"
artist: "Siddharth Vipin"
lyricist: "	Lalithanand"
director: "Cheran"
path: "/albums/thirumanam-lyrics"
song: "Varaamale vandhaale"
image: ../../images/albumart/thirumanam.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/i8LHpK-71EA"
type: "love"
singers:
  - Jagadeesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Varamale Vandhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamale Vandhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaazhkaiyai Poratti Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhkaiyai Poratti Potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula Thaai Pola Theriyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Thaai Pola Theriyura"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamale Vandhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamale Vandhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sogathai Koluthi Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sogathai Koluthi Potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyile Sei Pola Anaikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyile Sei Pola Anaikura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varamale Vandhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamale Vandhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaazhkaiyai Poratti Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhkaiyai Poratti Potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula Thaai Pola Theriyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Thaai Pola Theriyura"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamale Vandhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamale Vandhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Sogathai Koluthi Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Sogathai Koluthi Potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyile Sei Pola Anaikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyile Sei Pola Anaikura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Thedura Maruthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Thedura Maruthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendura Aruthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendura Aruthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Vera Engum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Vera Engum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaarthaiyil Vaazhkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaarthaiyil Vaazhkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Maperum Maatrangal Avalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maperum Maatrangal Avalale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamale Vandhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamale Vandhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaazhkaiyai Poratti Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhkaiyai Poratti Potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasula Thaai Pola Theriyura hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Thaai Pola Theriyura hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maram Chedi Myna Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maram Chedi Myna Kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Manjal Malar Thottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Manjal Malar Thottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Rendum Angeyum Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Rendum Angeyum Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan Mugam Thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Mugam Thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi Theera Unavoottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Theera Unavoottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilam Pola Pala Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilam Pola Pala Kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvootta Vanthaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvootta Vanthaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Muttru Pulli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Muttru Pulli"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru Enni Thalli Nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru Enni Thalli Nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Netri Pottai Ittu Kondai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri Pottai Ittu Kondai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Ondrum Illai Endre Nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Ondrum Illai Endre Nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Undu Endru Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Undu Endru Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Nindrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Nindrai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakka Vanthaye Meetka Vanthaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakka Vanthaye Meetka Vanthaye"/>
</div>
<div class="lyrico-lyrics-wrapper">En paaram Theerka Vanthaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paaram Theerka Vanthaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathu Nee Udan Padum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu Nee Udan Padum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga Paathu Nee Muran Padum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Paathu Nee Muran Padum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothum Yetru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum Yetru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Purithalai Kaatuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Purithalai Kaatuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Naan Naan Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Naan Naan Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Sethu Un Uyirula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Sethu Un Uyirula"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyira Serthu En Kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyira Serthu En Kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yavum Kothu Pin Thodarnthidum Thozhamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yavum Kothu Pin Thodarnthidum Thozhamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Naan Naan Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Naan Naan Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethi Vegu Uyarathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethi Vegu Uyarathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela Yethi Varum Thuyarathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela Yethi Varum Thuyarathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Thethi Nee Sethukura Gopuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Thethi Nee Sethukura Gopuram"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Naan Naan Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Naan Naan Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Pothi En Imaikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Pothi En Imaikulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Pothi Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Pothi Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiraipadam Paarpathai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiraipadam Paarpathai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pugaipadam Paarkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pugaipadam Paarkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Naan Naan Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Naan Naan Naan"/>
</div>
</pre>
