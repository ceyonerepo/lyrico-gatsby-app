---
title: "kingini kingini song lyrics"
album: "MGR Magan"
artist: "Anthony Daasan"
lyricist: "Muragan Manthiram"
director: "Ponram"
path: "/albums/mgr-magan-lyrics"
song: "Kingini Kingini"
image: ../../images/albumart/mgr-magan.jpg
date: 2021-11-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/u_uX2l7TVmQ"
type: "happy"
singers:
  - Ranjith
  - Srinidhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yei kingini kingini maniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei kingini kingini maniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enakku pudicha saniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enakku pudicha saniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thangamae thaayumanavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thangamae thaayumanavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi ennadi ennadi kodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi ennadi ennadi kodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandaikul vedikkum vediyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaikul vedikkum vediyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjula muzhaicha kaadhal chediyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjula muzhaicha kaadhal chediyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna solla varthai yedhum sikkavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna solla varthai yedhum sikkavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illama vaazhuvenu ninaikkalamo neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illama vaazhuvenu ninaikkalamo neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">En raasathi en usuru unkooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En raasathi en usuru unkooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannu asaivil padham maari pona naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannu asaivil padham maari pona naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan enna solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthuyirum kola uyiruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthuyirum kola uyiruma"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkuthu en idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkuthu en idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nitham unnai theduren naan whatsapp-ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nitham unnai theduren naan whatsapp-ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathu tharam yosichadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu tharam yosichadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Othai variyil pesuviyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othai variyil pesuviyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Posukkuna kovikira en gap-ula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Posukkuna kovikira en gap-ula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei kingini kingini maniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei kingini kingini maniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enakku pudicha saniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enakku pudicha saniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thangamae thaayumanavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thangamae thaayumanavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi ennadi ennadi kodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi ennadi ennadi kodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandaikul vedikkum vediyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaikul vedikkum vediyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjula muzhaicha kaadhal chediyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjula muzhaicha kaadhal chediyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna solla varthai yedhum sikkavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna solla varthai yedhum sikkavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illama vaazhuvenu ninaikkalamo neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illama vaazhuvenu ninaikkalamo neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">En raasathi en usuru unkooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En raasathi en usuru unkooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vithai thaan pechula thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithai thaan pechula thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai savadal adikkadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai savadal adikkadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Budhi kettu pochaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budhi kettu pochaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un aalu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un aalu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pola enakkullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pola enakkullum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeraalam aasai irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeraalam aasai irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaaga poranthathala poraduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaaga poranthathala poraduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee sonnadhu sonnadhu seri thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sonnadhu sonnadhu seri thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ennaikkum unakku sani thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ennaikkum unakku sani thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unni pinni thodarum kodi thaan naanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unni pinni thodarum kodi thaan naanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey innikkum ennaikkum thunaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey innikkum ennaikkum thunaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippovum eppovum usuraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippovum eppovum usuraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu iruppen urava naanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu iruppen urava naanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna solla varthai yedhum sikkavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna solla varthai yedhum sikkavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illama vaazhuvenu ninaikkalamo neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illama vaazhuvenu ninaikkalamo neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">En raasathi …en usuru unkooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En raasathi …en usuru unkooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannu asaivil padham maari pona naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannu asaivil padham maari pona naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan enna solla"/>
</div>
</pre>
