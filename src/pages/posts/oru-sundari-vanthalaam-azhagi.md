---
title: "oru sundari vanthalaam song lyrics"
album: "Azhagi"
artist: "Ilaiyaraaja"
lyricist: "Karunanithi"
director: "Thangar Bachchan"
path: "/albums/azhagi-lyrics"
song: "Oru Sundari Vanthalaam"
image: ../../images/albumart/azhagi.jpg
date: 2002-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/G77Wq9mFAnc"
type: "melody"
singers:
  - P. Unni Krishnan
  - Sadhana Sargam
  - Malgudi Subha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">oru sundari vanthalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sundari vanthalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">antha munthiri thopula thopula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha munthiri thopula thopula"/>
</div>
<div class="lyrico-lyrics-wrapper">oru sundari vanthalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sundari vanthalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">antha munthiri thopula thopula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha munthiri thopula thopula"/>
</div>
<div class="lyrico-lyrics-wrapper">oru santham paduchalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru santham paduchalam"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla anthiyil pookura poo pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla anthiyil pookura poo pola"/>
</div>
<div class="lyrico-lyrics-wrapper">sangeetham sathiram theriyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangeetham sathiram theriyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalum paaduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalum paaduva"/>
</div>
<div class="lyrico-lyrics-wrapper">vaathiyar kedaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaathiyar kedaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalum vaarthaigal poduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalum vaarthaigal poduva"/>
</div>
<div class="lyrico-lyrics-wrapper">edutha edutha sruthi maarathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edutha edutha sruthi maarathu"/>
</div>
<div class="lyrico-lyrics-wrapper">anth thaalam velagathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anth thaalam velagathu"/>
</div>
<div class="lyrico-lyrics-wrapper">yarum solli tharama mottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarum solli tharama mottu"/>
</div>
<div class="lyrico-lyrics-wrapper">virikum malligai poo pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virikum malligai poo pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru sundari vanthalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sundari vanthalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">antha munthiri thopula thopula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha munthiri thopula thopula"/>
</div>
<div class="lyrico-lyrics-wrapper">oru santham paduchalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru santham paduchalam"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla anthiyil pookura poo pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla anthiyil pookura poo pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nesam neranja manam pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesam neranja manam pol"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam veesum malar pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam veesum malar pol"/>
</div>
<div class="lyrico-lyrics-wrapper">nathi karaiyila poranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nathi karaiyila poranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">intha ulagam maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha ulagam maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">uthirthu vilunthu nathiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthirthu vilunthu nathiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">porandu porandu alaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porandu porandu alaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">ilukum thisaiyil mithanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilukum thisaiyil mithanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">athu thaduka maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu thaduka maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">oodum nathi ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodum nathi ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kadalil kalakuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kadalil kalakuma"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalum isai kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalum isai kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">sendru kalaka marukuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sendru kalaka marukuma"/>
</div>
<div class="lyrico-lyrics-wrapper">ellarkum ethirparpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellarkum ethirparpu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanai ethaniyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai ethaniyo"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai pol thaniyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai pol thaniyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">valvathu ethanaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valvathu ethanaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">oru sugam illathavar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sugam illathavar"/>
</div>
<div class="lyrico-lyrics-wrapper">sorga kulanthaigale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sorga kulanthaigale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru sundari vanthalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sundari vanthalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla anthiyil poothidum poo pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla anthiyil poothidum poo pola"/>
</div>
<div class="lyrico-lyrics-wrapper">poo pandhu aduchalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo pandhu aduchalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">thulli thulli kuthikira meen pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli thulli kuthikira meen pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nandraga vaalthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nandraga vaalthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">ellorin kangal thirumbum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellorin kangal thirumbum"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan meethile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan meethile"/>
</div>
<div class="lyrico-lyrics-wrapper">illathu ponaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illathu ponaal"/>
</div>
<div class="lyrico-lyrics-wrapper">sollatha kadhaigal sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollatha kadhaigal sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">thangal moliyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangal moliyile"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniye nonthu valthirunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniye nonthu valthirunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">ethai than mudipai enniduvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethai than mudipai enniduvai"/>
</div>
<div class="lyrico-lyrics-wrapper">ellarkum kuraigal undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellarkum kuraigal undu"/>
</div>
<div class="lyrico-lyrics-wrapper">aekam etharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aekam etharku"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniye thaalthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniye thaalthi"/>
</div>
<div class="lyrico-lyrics-wrapper">kondu thavithai etharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondu thavithai etharku"/>
</div>
<div class="lyrico-lyrics-wrapper">man meethu veesinalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man meethu veesinalum"/>
</div>
<div class="lyrico-lyrics-wrapper">pon maatru kuraiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pon maatru kuraiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyinal suttalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyinal suttalum"/>
</div>
<div class="lyrico-lyrics-wrapper">minni minni jolikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minni minni jolikum"/>
</div>
<div class="lyrico-lyrics-wrapper">thangam thangamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam thangamadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru sundari vanthalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sundari vanthalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla anthiyil poothidum poo pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla anthiyil poothidum poo pola"/>
</div>
<div class="lyrico-lyrics-wrapper">poo pandhu aduchalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo pandhu aduchalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">thulli thulli kuthikira meen pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli thulli kuthikira meen pola"/>
</div>
</pre>
