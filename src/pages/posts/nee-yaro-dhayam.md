---
title: "nee yaro song lyrics"
album: "Dhayam"
artist: "Sathish Selvam"
lyricist: "Muthamil"
director: "Kannan Rangaswamy"
path: "/albums/dhayam-lyrics"
song: "Nee Yaro"
image: ../../images/albumart/dhayam.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zxYx34J9q5A"
type: "mass"
singers:
  - Nikhita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Yaro Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Yaro Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aedhum Kanaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aedhum Kanaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum Kaalam Yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Kaalam Yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Solluvaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Solluvaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee Raadho Maayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Raadho Maayam "/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Poraadum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Poraadum "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Koodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neram Veenaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Veenaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum Podhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Podhum "/>
</div>
<div class="lyrico-lyrics-wrapper">Melum Baadhaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melum Baadhaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Dhayamaaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhayamaaga "/>
</div>
<div class="lyrico-lyrics-wrapper">Maarinaayo Maayamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarinaayo Maayamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Varamo Yen Paadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Varamo Yen Paadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Meedhu Yeniyodu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meedhu Yeniyodu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naagam Thedi Pin Varumo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naagam Thedi Pin Varumo "/>
</div>
<div class="lyrico-lyrics-wrapper">Neer Pogum Dhisaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer Pogum Dhisaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">Soodhum Arindhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodhum Arindhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyum Kalandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyum Kalandhu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulaavarum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaavarum "/>
</div>
<div class="lyrico-lyrics-wrapper">Por Thondrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Por Thondrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilaiyil Dhega Manadhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilaiyil Dhega Manadhum "/>
</div>
<div class="lyrico-lyrics-wrapper">Koori Vidumo Tharaa Dhaaram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koori Vidumo Tharaa Dhaaram "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Urutti Eriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Urutti Eriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundha Porulaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundha Porulaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Viratti Pidiththaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viratti Pidiththaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Idama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Idama "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unn Idamum Theriya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Idamum Theriya "/>
</div>
<div class="lyrico-lyrics-wrapper">Thoadarum Kanavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoadarum Kanavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvae Mudivil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvae Mudivil "/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Kadhaiyaa Veen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Kadhaiyaa Veen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pola Thaeriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Thaeriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesha Mugamum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesha Mugamum "/>
</div>
<div class="lyrico-lyrics-wrapper">Paalin Thelivaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalin Thelivaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoon Pola Irundhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoon Pola Irundhum "/>
</div>
<div class="lyrico-lyrics-wrapper">Sorndhu Irundhaaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorndhu Irundhaaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Saindhu Vizhumae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saindhu Vizhumae "/>
</div>
<div class="lyrico-lyrics-wrapper">Thani Maram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Maram"/>
</div>
</pre>
