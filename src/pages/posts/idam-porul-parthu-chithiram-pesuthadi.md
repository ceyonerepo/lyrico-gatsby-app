---
title: "idam porul parthu song lyrics"
album: "Chithiram Pesuthadi"
artist: "Sundar C Babu"
lyricist: "Pa. Vijay"
director: "Mysskin"
path: "/albums/chithiram-pesuthadi-lyrics"
song: "Idam Porul Parthu"
image: ../../images/albumart/chithiram-pesuthadi.jpg
date: 2006-02-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6z9ovUjsNVQ"
type: "love"
singers:
  - Karthik
  - Sujatha Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idam Porul Paarthu Idhayathai Maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Porul Paarthu Idhayathai Maathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Oru Kaadhal Koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Oru Kaadhal Koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalai Paarthu Viralgalai Serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalai Paarthu Viralgalai Serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinil Ennai Korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinil Ennai Korthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Pole Yevarum Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Pole Yevarum Unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalika Mudiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalika Mudiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyum Yendral Kooda Avanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyum Yendral Kooda Avanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalika Mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalika Mudiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idam Porul Paarthu Idhayathai Maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Porul Paarthu Idhayathai Maathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Oru Kaadhal Koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Oru Kaadhal Koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalai Paarthu Viralgalai Serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalai Paarthu Viralgalai Serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinil Ennai Korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinil Ennai Korthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Nagangalai Paarthu En Iruvadhu Mugangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nagangalai Paarthu En Iruvadhu Mugangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannangal Paarthen En Idhazhin Regaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannangal Paarthen En Idhazhin Regaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Yendra Marathin Keezhe Budhthan Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Yendra Marathin Keezhe Budhthan Aagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Kondu Unthan Madiyil Pookal Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Kondu Unthan Madiyil Pookal Aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Paarkum Thisai Yenthan Nadai Paathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paarkum Thisai Yenthan Nadai Paathaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pesum Mozhi Yenthan Agaraathiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pesum Mozhi Yenthan Agaraathiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idam Porul Paarthu Idhayathai Maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Porul Paarthu Idhayathai Maathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Oru Kaadhal Koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Oru Kaadhal Koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhigalai Paarthu Viralgalai Serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalai Paarthu Viralgalai Serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinil Ennai Korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinil Ennai Korthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vizhigalin Melil En Vervai Irukudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vizhigalin Melil En Vervai Irukudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Punnagai Ninaivil En Thookam Tholaindhathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Punnagai Ninaivil En Thookam Tholaindhathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Yendra Thaayin Madiyil Kuzhanthai Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Yendra Thaayin Madiyil Kuzhanthai Aagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhalai Pesum Mozhiyil Indru Manithan Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhalai Pesum Mozhiyil Indru Manithan Aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavodu Unai Kaana Imai Theduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavodu Unai Kaana Imai Theduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaiyaaga Naan Vanthu Unai Mooduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaiyaaga Naan Vanthu Unai Mooduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idam Porul Paarthu Idhayathai Maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Porul Paarthu Idhayathai Maathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Oru Kaadhal Koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Oru Kaadhal Koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalai Paarthu Viralgalai Serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalai Paarthu Viralgalai Serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinil Ennai Korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinil Ennai Korthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Pole Yevarum Unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Pole Yevarum Unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalika Mudiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalika Mudiyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyum Yendral Kooda Avanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyum Yendral Kooda Avanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalika Mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalika Mudiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idam Porul Paarthu Idhayathai Maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Porul Paarthu Idhayathai Maathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Oru Kaadhal Koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Oru Kaadhal Koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalai Paarthu Viralgalai Serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalai Paarthu Viralgalai Serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinil Ennai Korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinil Ennai Korthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idam Porul Paarthu Idhayathai Maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Porul Paarthu Idhayathai Maathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Oru Kaadhal Koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Oru Kaadhal Koothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalai Paarthu Viralgalai Serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalai Paarthu Viralgalai Serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirinil Ennai Korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirinil Ennai Korthu"/>
</div>
</pre>
