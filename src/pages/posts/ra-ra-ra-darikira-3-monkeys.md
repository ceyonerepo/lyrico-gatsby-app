---
title: "ra ra ra darikira song lyrics"
album: "3 Monkeys"
artist: "Anil kumar G"
lyricist: "Anil Kumar G"
director: "Anil Kumar G"
path: "/albums/3-monkeys-lyrics"
song: "Ra Ra Ra Darikira"
image: ../../images/albumart/3-monkeys.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/_sm5eDOv1A4"
type: "love"
singers:
  - Lipiska
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raa Raa Raa Dariki Raara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa Raa Dariki Raara"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliya Paruvame Nedhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Paruvame Nedhiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raa Raa Raa Dariki Raara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa Raa Dariki Raara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Paruvame Nidhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Paruvame Nidhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyi veyaara Cheliya Nederaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyi veyaara Cheliya Nederaa"/>
</div>
<div class="lyrico-lyrics-wrapper">THakeiraa Thanuvu needegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="THakeiraa Thanuvu needegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo touch me touch me touch me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo touch me touch me touch me"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Kiss Me Kiss Me Kiss Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Kiss Me Kiss Me Kiss Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo touch me touch me touch me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo touch me touch me touch me"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Kiss Me Kiss Me Kiss Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Kiss Me Kiss Me Kiss Me"/>
</div>
</pre>
