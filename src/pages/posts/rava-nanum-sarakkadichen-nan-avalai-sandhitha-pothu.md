---
title: "rava nanum sarakkadichen song lyrics"
album: "Nan Avalai Sandhitha Pothu"
artist: "Hithesh Murugavel- Jai"
lyricist: "LG Ravichandar"
director: "L G Ravichnadar"
path: "/albums/nan-avalai-sandhitha-pothu-lyrics"
song: "Rava Nanum Sarakkadichen"
image: ../../images/albumart/nan-avalai-sandhitha-pothu.jpg
date: 2019-12-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/sNk43956c4s"
type: "sad"
singers:
  - Tipu
  - Madhumitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">rava nanum saraku adichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rava nanum saraku adichen"/>
</div>
<div class="lyrico-lyrics-wrapper">life ah nanum tholachu puten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life ah nanum tholachu puten"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniyala nanum inge 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyala nanum inge "/>
</div>
<div class="lyrico-lyrics-wrapper">mati kiten da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mati kiten da"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna nanum kooti vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna nanum kooti vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">enna nanum matti viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nanum matti viten"/>
</div>
<div class="lyrico-lyrics-wrapper">veena nanum vaalkayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veena nanum vaalkayila"/>
</div>
<div class="lyrico-lyrics-wrapper">thothu puten da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thothu puten da"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna neyum pakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna neyum pakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">patha neyum pesatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha neyum pesatha"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna neyum pakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna neyum pakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">patha neyum pesatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha neyum pesatha"/>
</div>
<div class="lyrico-lyrics-wrapper">rotoram figure ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rotoram figure ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">ennanu than ketkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennanu than ketkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">chennai neyum pogathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chennai neyum pogathe"/>
</div>
<div class="lyrico-lyrics-wrapper">pona ponna pakathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pona ponna pakathe"/>
</div>
<div class="lyrico-lyrics-wrapper">chennai neyum pogathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chennai neyum pogathe"/>
</div>
<div class="lyrico-lyrics-wrapper">pona ponna pakathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pona ponna pakathe"/>
</div>
<div class="lyrico-lyrics-wrapper">patha ponna bus la 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha ponna bus la "/>
</div>
<div class="lyrico-lyrics-wrapper">eri neyum kooda pogathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eri neyum kooda pogathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oosi mani paasi mani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oosi mani paasi mani"/>
</div>
<div class="lyrico-lyrics-wrapper">vangalaiyo saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vangalaiyo saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">vangikita nanga konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vangikita nanga konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">oothikuvom saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothikuvom saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">podu pasi mani ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podu pasi mani ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">vangikanga saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vangikanga saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">neenga konjam vangi kita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga konjam vangi kita"/>
</div>
<div class="lyrico-lyrics-wrapper">oothikuvom saamye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothikuvom saamye"/>
</div>
<div class="lyrico-lyrics-wrapper">singa eh singi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singa eh singi "/>
</div>
<div class="lyrico-lyrics-wrapper">eh singa singi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh singa singi"/>
</div>
<div class="lyrico-lyrics-wrapper">singa singi singa singi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singa singi singa singi"/>
</div>
<div class="lyrico-lyrics-wrapper">singa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">siment um sengal um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siment um sengal um"/>
</div>
<div class="lyrico-lyrics-wrapper">senthu vaikum sithalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senthu vaikum sithalu"/>
</div>
<div class="lyrico-lyrics-wrapper">kothanaru illama nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kothanaru illama nan"/>
</div>
<div class="lyrico-lyrics-wrapper">othala aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othala aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">odambu valikuthu nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odambu valikuthu nu"/>
</div>
<div class="lyrico-lyrics-wrapper">oothikita en mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oothikita en mama"/>
</div>
<div class="lyrico-lyrics-wrapper">othadam nan kuduka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othadam nan kuduka"/>
</div>
<div class="lyrico-lyrics-wrapper">maru veedu polam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru veedu polam"/>
</div>
<div class="lyrico-lyrics-wrapper">velaka anaikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velaka anaikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">paaya virikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaya virikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">mambalatha juice potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mambalatha juice potu"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum kudikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum kudikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna moodanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna moodanum"/>
</div>
<div class="lyrico-lyrics-wrapper">enna marakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna marakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">un udambu vervaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un udambu vervaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum kulikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum kulikanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rava nanum saraku adichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rava nanum saraku adichen"/>
</div>
<div class="lyrico-lyrics-wrapper">life ah nanum tholachu puten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life ah nanum tholachu puten"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniyala nanum inge 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyala nanum inge "/>
</div>
<div class="lyrico-lyrics-wrapper">mati kiten da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mati kiten da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sithana vaasalula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithana vaasalula "/>
</div>
<div class="lyrico-lyrics-wrapper">sirpama nan irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirpama nan irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">bit pada theatre pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bit pada theatre pola"/>
</div>
<div class="lyrico-lyrics-wrapper">kette ponene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kette ponene"/>
</div>
<div class="lyrico-lyrics-wrapper">oora paka vanthavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora paka vanthavana"/>
</div>
<div class="lyrico-lyrics-wrapper">katti vacha en mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti vacha en mama"/>
</div>
<div class="lyrico-lyrics-wrapper">odi poi thapika oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odi poi thapika oru"/>
</div>
<div class="lyrico-lyrics-wrapper">vali sollu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali sollu mama"/>
</div>
<div class="lyrico-lyrics-wrapper">kathaiya sollanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathaiya sollanum"/>
</div>
<div class="lyrico-lyrics-wrapper">atha nee kekanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha nee kekanum"/>
</div>
<div class="lyrico-lyrics-wrapper">veen paliya maranthu putu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veen paliya maranthu putu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna anupanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna anupanum"/>
</div>
<div class="lyrico-lyrics-wrapper">sathiyama oru thavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathiyama oru thavarum"/>
</div>
<div class="lyrico-lyrics-wrapper">nan seiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan seiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">thathuvama sollida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thathuvama sollida"/>
</div>
<div class="lyrico-lyrics-wrapper">oru variyum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru variyum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">sathiyama oru thavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathiyama oru thavarum"/>
</div>
<div class="lyrico-lyrics-wrapper">nee seiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee seiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">thathuvama sollida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thathuvama sollida"/>
</div>
<div class="lyrico-lyrics-wrapper">oru variyum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru variyum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rava nanum saraku adichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rava nanum saraku adichen"/>
</div>
<div class="lyrico-lyrics-wrapper">life ah nanum tholachu puten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life ah nanum tholachu puten"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniyala nanum inge 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyala nanum inge "/>
</div>
<div class="lyrico-lyrics-wrapper">mati kiten da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mati kiten da"/>
</div>
<div class="lyrico-lyrics-wrapper">ponna nanum kooti vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna nanum kooti vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">enna nanum matti viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nanum matti viten"/>
</div>
<div class="lyrico-lyrics-wrapper">veena nanum vaalkayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veena nanum vaalkayila"/>
</div>
<div class="lyrico-lyrics-wrapper">thothu puten da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thothu puten da"/>
</div>
</pre>
