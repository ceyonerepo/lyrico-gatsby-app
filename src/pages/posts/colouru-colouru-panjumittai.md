---
title: "colouru colouru song lyrics"
album: "Panjumittai"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "S.P. Mohan"
path: "/albums/panjumittai-lyrics"
song: "Colouru Colouru"
image: ../../images/albumart/panjumittai.jpg
date: 2018-06-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/t04DQ8Qc_6I"
type: "mass"
singers:
  - Jayamoorthy
  - Kalyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">colouru colouru colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="colouru colouru colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">manja manja colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja manja colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">colouru colouru colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="colouru colouru colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">manja manja colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja manja colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">colouru colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="colouru colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">manja colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">colouru colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="colouru colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">manja colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja colouru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">colouru colouru colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="colouru colouru colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">manja manja colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja manja colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">colouru colouru colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="colouru colouru colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">manja manja colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja manja colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">colouru colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="colouru colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">manja colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">colouru colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="colouru colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">manja colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">colouru manja colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="colouru manja colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">colouru manja colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="colouru manja colouru"/>
</div>
<div class="lyrico-lyrics-wrapper">colouru manja colouru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="colouru manja colouru"/>
</div>
</pre>
