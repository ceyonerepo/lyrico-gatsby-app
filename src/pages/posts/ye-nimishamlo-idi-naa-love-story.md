---
title: "ye nimishamlo song lyrics"
album: "Idi Naa Love Story"
artist: "Srinath Vijay"
lyricist: "Ramajogayya Sastry"
director: "Ramesh Gopi"
path: "/albums/idi-naa-love-story-lyrics"
song: "Ye Nimishamlo"
image: ../../images/albumart/idi-naa-love-story.jpg
date: 2018-02-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/xP6YnEgVKLs"
type: "love"
singers:
  -	Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye Nimishamlo Ninu Choosano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Nimishamlo Ninu Choosano"/>
</div>
<div class="lyrico-lyrics-wrapper">Padipoya Premalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padipoya Premalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Nimisham LO Manasichhesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Nimisham LO Manasichhesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathi Poye Maayalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathi Poye Maayalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Nimishamlo Ninu Choosano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Nimishamlo Ninu Choosano"/>
</div>
<div class="lyrico-lyrics-wrapper">Padipoya Premalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padipoya Premalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Nimisham LO Manasichhesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Nimisham LO Manasichhesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathi Poye Maayalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathi Poye Maayalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manase Doche Merupedo Undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manase Doche Merupedo Undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Chirunavvulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Chirunavvulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Piliche Lope Tholipreme Nuvvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Piliche Lope Tholipreme Nuvvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaluvayyavu Naa Chinni Yedalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaluvayyavu Naa Chinni Yedalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Talachi Maimarachipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Talachi Maimarachipoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Prathi Oohalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Prathi Oohalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvevaro Emo Gurthemo Radu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvevaro Emo Gurthemo Radu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthe Leni Nee Theepi Kalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthe Leni Nee Theepi Kalalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Nimishamlo Ninu Choosano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Nimishamlo Ninu Choosano"/>
</div>
<div class="lyrico-lyrics-wrapper">Padipoya Premalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padipoya Premalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Nimisham LO Manasichhesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Nimisham LO Manasichhesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathi Poye Maayalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathi Poye Maayalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Choose Choopulathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Choose Choopulathone"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelalo Vintha Alajade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelalo Vintha Alajade"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pedavula Matalathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pedavula Matalathone"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vaipuke Nannu Lagene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaipuke Nannu Lagene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Thakina Kshname Naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Thakina Kshname Naalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manesemo Eela Vesene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manesemo Eela Vesene"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Needalo Naku Baduluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Needalo Naku Baduluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Choosanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Choosanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaliloni Chinna Matala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaliloni Chinna Matala"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduraite Emi Cheyane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduraite Emi Cheyane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naluvaipula Chilipi Mattuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naluvaipula Chilipi Mattuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Melukuve Rani Kalalo Munchave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melukuve Rani Kalalo Munchave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Nimishamlo Ninu Choosano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Nimishamlo Ninu Choosano"/>
</div>
<div class="lyrico-lyrics-wrapper">Padipoya Premalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padipoya Premalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Nimisham LO Manasichhesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Nimisham LO Manasichhesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathi Poye Maayalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathi Poye Maayalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Nimishamlo Ninu Choosano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Nimishamlo Ninu Choosano"/>
</div>
<div class="lyrico-lyrics-wrapper">Padipoya Premalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padipoya Premalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Nimisham LO Manasichhesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Nimisham LO Manasichhesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathi Poye Maayalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathi Poye Maayalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">E Dikkuna Nuvvuna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Dikkuna Nuvvuna "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pakkane Unna Janta Needaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pakkane Unna Janta Needaga"/>
</div>
<div class="lyrico-lyrics-wrapper">E Vaipuku Veltunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Vaipuku Veltunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Vastunna Nee Adugu Jadaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Vastunna Nee Adugu Jadaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pere Pedavi Teepiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pere Pedavi Teepiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Nimishma Paliki Choodana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Nimishma Paliki Choodana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manase Pooladhariga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manase Pooladhariga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Nadipinchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Nadipinchana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Leka Nenu Nenu Lenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Leka Nenu Nenu Lenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopirilo Ninnu Unchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopirilo Ninnu Unchana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Premaku Hadduledule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Premaku Hadduledule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvila Nannu Cherina Samyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvila Nannu Cherina Samyana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Nimishamlo Ninu Choosano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Nimishamlo Ninu Choosano"/>
</div>
<div class="lyrico-lyrics-wrapper">Padipoya Premalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padipoya Premalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Nimisham LO Manasichhesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Nimisham LO Manasichhesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathi Poye Maayalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathi Poye Maayalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Nimishamlo Ninu Choosano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Nimishamlo Ninu Choosano"/>
</div>
<div class="lyrico-lyrics-wrapper">Padipoya Premalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padipoya Premalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Nimisham LO Manasichhesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Nimisham LO Manasichhesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathi Poye Maayalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathi Poye Maayalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manase Doche Merupedo Undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manase Doche Merupedo Undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Chirunavvulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Chirunavvulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Piliche Lope Tholipreme Nuvvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Piliche Lope Tholipreme Nuvvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaluvayyavu Naa Chinni Yedalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaluvayyavu Naa Chinni Yedalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Talachi Maimarachipoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Talachi Maimarachipoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Prathi Oohalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Prathi Oohalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvevaro Emo Gurthemo Radu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvevaro Emo Gurthemo Radu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthe Leni Nee Theepi Kalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthe Leni Nee Theepi Kalalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Nimishamlo Ninu Choosano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Nimishamlo Ninu Choosano"/>
</div>
<div class="lyrico-lyrics-wrapper">Padipoya Premalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padipoya Premalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Nimisham LO Manasichhesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Nimisham LO Manasichhesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathi Poye Maayalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathi Poye Maayalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Nimishamlo Ninu Choosano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Nimishamlo Ninu Choosano"/>
</div>
<div class="lyrico-lyrics-wrapper">Padipoya Premalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padipoya Premalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Nimisham LO Manasichhesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Nimisham LO Manasichhesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathi Poye Maayalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathi Poye Maayalo"/>
</div>
</pre>
