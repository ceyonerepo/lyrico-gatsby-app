---
title: "alli poo nirathazhagi song lyrics"
album: "Thaen"
artist: "Sanath Bharadwaj"
lyricist: "S.Gnanakaravel"
director: "Ganesh Vinayakan"
path: "/albums/thaen-song-lyrics"
song: "Alli Poo Nirathazhagi"
image: ../../images/albumart/thaen.jpg
date: 2021-03-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LLmRabEI_gU"
type: "Sad"
singers:
  - Sanath Bharadwaj
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">alli poo nirathalagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli poo nirathalagi"/>
</div>
<div class="lyrico-lyrics-wrapper">aavaram poo alagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavaram poo alagi"/>
</div>
<div class="lyrico-lyrics-wrapper">athi mara sendu alagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athi mara sendu alagi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanalaye kaanalaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanalaye kaanalaye "/>
</div>
<div class="lyrico-lyrics-wrapper">vella poondu pal alagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vella poondu pal alagi"/>
</div>
<div class="lyrico-lyrics-wrapper">vellendhi siripalagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellendhi siripalagi"/>
</div>
<div class="lyrico-lyrics-wrapper">karuvandu kan alagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvandu kan alagi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanalaye kaanalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanalaye kaanalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">velliyora pookal ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velliyora pookal ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">vedhanaiyil vaaduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedhanaiyil vaaduthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vennilave nee illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennilave nee illama"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam engu ponathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam engu ponathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">velliyora pookal ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velliyora pookal ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">vedhanaiyil vaaduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedhanaiyil vaaduthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vennilave nee illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennilave nee illama"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam engu ponathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam engu ponathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alli poo nirathalagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli poo nirathalagi"/>
</div>
<div class="lyrico-lyrics-wrapper">aavaram poo alagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavaram poo alagi"/>
</div>
<div class="lyrico-lyrics-wrapper">athi mara sendu alagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athi mara sendu alagi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanalaye kaanalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanalaye kaanalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sillaraya un pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sillaraya un pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">sitharudhadi moolayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sitharudhadi moolayila"/>
</div>
<div class="lyrico-lyrics-wrapper">sell aricha puthagama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sell aricha puthagama"/>
</div>
<div class="lyrico-lyrics-wrapper">aanene naanum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanene naanum pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">karuvelam kaatu vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvelam kaatu vali"/>
</div>
<div class="lyrico-lyrics-wrapper">thadumari ponenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadumari ponenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanam paadum moongil ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanam paadum moongil ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhari aluvuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhari aluvuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kan vilichi nan paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan vilichi nan paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">kanne unna kanaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne unna kanaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">enge nan poveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge nan poveno"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kadhi aaveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kadhi aaveno"/>
</div>
<div class="lyrico-lyrics-wrapper">nitham nitham un nenapaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitham nitham un nenapaal"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi than vanthenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi than vanthenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">usura kudupen nu sonnavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura kudupen nu sonnavale"/>
</div>
<div class="lyrico-lyrics-wrapper">en usura eduthu ponale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en usura eduthu ponale"/>
</div>
<div class="lyrico-lyrics-wrapper">usura kudupen nu sonnavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura kudupen nu sonnavale"/>
</div>
<div class="lyrico-lyrics-wrapper">en usura eduthu ponale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en usura eduthu ponale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alli poo nirathalagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli poo nirathalagi"/>
</div>
<div class="lyrico-lyrics-wrapper">aavaram poo alagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavaram poo alagi"/>
</div>
<div class="lyrico-lyrics-wrapper">athi mara sendu alagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athi mara sendu alagi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanalaye kaanalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanalaye kaanalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en kurunji mala thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kurunji mala thane"/>
</div>
<div class="lyrico-lyrics-wrapper">ne sarinja maayam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne sarinja maayam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">koodi vaala vanthavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi vaala vanthavale"/>
</div>
<div class="lyrico-lyrics-wrapper">kudiya vitu ponathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudiya vitu ponathenna"/>
</div>
<div class="lyrico-lyrics-wrapper">mala vaala onnu ipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mala vaala onnu ipo"/>
</div>
<div class="lyrico-lyrics-wrapper">kola sanju ponathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola sanju ponathenna"/>
</div>
<div class="lyrico-lyrics-wrapper">entha thisa nee pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha thisa nee pora"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum varen un kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum varen un kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">en usure ne illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en usure ne illama"/>
</div>
<div class="lyrico-lyrics-wrapper">thadumari thavikirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadumari thavikirene"/>
</div>
<div class="lyrico-lyrics-wrapper">unna kalavadi pona paya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kalavadi pona paya"/>
</div>
<div class="lyrico-lyrics-wrapper">kaala nu solluranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaala nu solluranga"/>
</div>
<div class="lyrico-lyrics-wrapper">avana yelu logam thedi vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avana yelu logam thedi vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">sollidunga paathakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollidunga paathakka"/>
</div>
<div class="lyrico-lyrics-wrapper">avan usura edukkama vida maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan usura edukkama vida maten"/>
</div>
<div class="lyrico-lyrics-wrapper">athu vara en usuru saagathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu vara en usuru saagathu"/>
</div>
<div class="lyrico-lyrics-wrapper">usura edukkama vida maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="usura edukkama vida maten"/>
</div>
<div class="lyrico-lyrics-wrapper">athu vara en usuru saagathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu vara en usuru saagathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alli poo nirathalagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli poo nirathalagi"/>
</div>
<div class="lyrico-lyrics-wrapper">aavaram poo alagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavaram poo alagi"/>
</div>
<div class="lyrico-lyrics-wrapper">athi mara sendu alagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athi mara sendu alagi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanalaye kaanalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanalaye kaanalaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sarkaar senja thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarkaar senja thappa"/>
</div>
<div class="lyrico-lyrics-wrapper">saamy athu senja thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saamy athu senja thappa"/>
</div>
<div class="lyrico-lyrics-wrapper">manasachi illatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasachi illatha"/>
</div>
<div class="lyrico-lyrics-wrapper">manusapaya senja thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusapaya senja thappa"/>
</div>
<div class="lyrico-lyrics-wrapper">nenju odanchu noguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenju odanchu noguthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">nirgathiya nikirendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirgathiya nikirendi"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna kuli eeramilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna kuli eeramilla"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thetha yarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thetha yarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">unna konna noyum athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna konna noyum athu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna konna pothum adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna konna pothum adi"/>
</div>
<div class="lyrico-lyrics-wrapper">paamarana kaakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paamarana kaakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">paavi paya naadu ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavi paya naadu ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna paavam senjomo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna paavam senjomo"/>
</div>
<div class="lyrico-lyrics-wrapper">inga vanthu poranthomo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga vanthu poranthomo"/>
</div>
<div class="lyrico-lyrics-wrapper">aelaiku than neethi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aelaiku than neethi "/>
</div>
<div class="lyrico-lyrics-wrapper">inga kedaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga kedaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">inga valvathe sabakedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga valvathe sabakedu"/>
</div>
<div class="lyrico-lyrics-wrapper">aelaiku than neethi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aelaiku than neethi "/>
</div>
<div class="lyrico-lyrics-wrapper">inga kedaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga kedaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">inga valvathe sabakedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga valvathe sabakedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alli poo nirathalagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alli poo nirathalagi"/>
</div>
<div class="lyrico-lyrics-wrapper">aavaram poo alagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aavaram poo alagi"/>
</div>
<div class="lyrico-lyrics-wrapper">athi mara sendu alagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athi mara sendu alagi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanalaye kaanalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanalaye kaanalaye"/>
</div>
</pre>
