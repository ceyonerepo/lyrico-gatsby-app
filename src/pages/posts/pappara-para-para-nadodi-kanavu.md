---
title: "pappara para para song lyrics"
album: "Nadodi Kanavu"
artist: "Sabesh Murali"
lyricist: "Sirgazhi Sirpi"
director: "Veera Selva"
path: "/albums/nadodi-kanavu-lyrics"
song: "Pappara Para Para"
image: ../../images/albumart/nadodi-kanavu.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uGgMgtuKKhg"
type: "happy"
singers:
  - Anitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yelo yelamba yelo yelamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelamba yelo yelamba"/>
</div>
<div class="lyrico-lyrics-wrapper">yelo yelamba yelo yelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelamba yelo yelo"/>
</div>
<div class="lyrico-lyrics-wrapper">yelo yelamba yelo yelamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelamba yelo yelamba"/>
</div>
<div class="lyrico-lyrics-wrapper">yelo yelamba yelo yelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelamba yelo yelo"/>
</div>
<div class="lyrico-lyrics-wrapper">yelo yelamba yelo yelamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelamba yelo yelamba"/>
</div>
<div class="lyrico-lyrics-wrapper">yelo yelamba yelo yelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelamba yelo yelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">papara para para 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="papara para para "/>
</div>
<div class="lyrico-lyrics-wrapper">papara para para 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="papara para para "/>
</div>
<div class="lyrico-lyrics-wrapper">pattampoochi kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattampoochi kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">pambaram kozhi killi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pambaram kozhi killi"/>
</div>
<div class="lyrico-lyrics-wrapper">thandu engaloda aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandu engaloda aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">papara para para 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="papara para para "/>
</div>
<div class="lyrico-lyrics-wrapper">papara para para 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="papara para para "/>
</div>
<div class="lyrico-lyrics-wrapper">pattampoochi kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattampoochi kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">pambaram kozhi killi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pambaram kozhi killi"/>
</div>
<div class="lyrico-lyrics-wrapper">thandu engaloda aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandu engaloda aatam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puthi mahi sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthi mahi sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">atha ketukave matom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha ketukave matom"/>
</div>
<div class="lyrico-lyrics-wrapper">potiyinu vanthu nanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potiyinu vanthu nanga"/>
</div>
<div class="lyrico-lyrics-wrapper">payanthu oda matom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payanthu oda matom"/>
</div>
<div class="lyrico-lyrics-wrapper">school uh than leave vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="school uh than leave vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">school uh than leave vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="school uh than leave vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">appa amma pecha ketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appa amma pecha ketu"/>
</div>
<div class="lyrico-lyrics-wrapper">veetula thanga maaton
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetula thanga maaton"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yelo yelamba yelo yelamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelamba yelo yelamba"/>
</div>
<div class="lyrico-lyrics-wrapper">yelo yelamba yelo yelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelamba yelo yelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aagayatha nanga nitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagayatha nanga nitham"/>
</div>
<div class="lyrico-lyrics-wrapper">yeraamale thottom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeraamale thottom"/>
</div>
<div class="lyrico-lyrics-wrapper">vinmeena than pidikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinmeena than pidikka"/>
</div>
<div class="lyrico-lyrics-wrapper">naangal thoondilgal potom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangal thoondilgal potom"/>
</div>
<div class="lyrico-lyrics-wrapper">yelo yelama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelama"/>
</div>
<div class="lyrico-lyrics-wrapper">aagayatha nanga nitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagayatha nanga nitham"/>
</div>
<div class="lyrico-lyrics-wrapper">yeraamale thottom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeraamale thottom"/>
</div>
<div class="lyrico-lyrics-wrapper">vinmeena than pidikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinmeena than pidikka"/>
</div>
<div class="lyrico-lyrics-wrapper">naangal thoondilgal potom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangal thoondilgal potom"/>
</div>
<div class="lyrico-lyrics-wrapper">kuruvinga renkaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuruvinga renkaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">kumbalaa yerikitu ulagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumbalaa yerikitu ulagatha"/>
</div>
<div class="lyrico-lyrics-wrapper">sutri vara aasa patom nanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutri vara aasa patom nanga"/>
</div>
<div class="lyrico-lyrics-wrapper">nilava than kooti vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilava than kooti vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kammayil kulika vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kammayil kulika vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu satta potu vitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu satta potu vitu"/>
</div>
<div class="lyrico-lyrics-wrapper">photo pidipom vanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="photo pidipom vanga"/>
</div>
<div class="lyrico-lyrics-wrapper">sadugudu vanga santhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sadugudu vanga santhira"/>
</div>
<div class="lyrico-lyrics-wrapper">mandalam poi varuvom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandalam poi varuvom "/>
</div>
<div class="lyrico-lyrics-wrapper">vanga vanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanga vanga"/>
</div>
<div class="lyrico-lyrics-wrapper">adikadi vanthu alagiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adikadi vanthu alagiya"/>
</div>
<div class="lyrico-lyrics-wrapper">pattampoochinga kekuthunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattampoochinga kekuthunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yelo yelamba yelo yelamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelamba yelo yelamba"/>
</div>
<div class="lyrico-lyrics-wrapper">yelo yelamba yelo yelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelamba yelo yelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oor suthave nanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor suthave nanga"/>
</div>
<div class="lyrico-lyrics-wrapper">thingam kootaliya aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thingam kootaliya aavom"/>
</div>
<div class="lyrico-lyrics-wrapper">aathorathil ninnu nanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathorathil ninnu nanga"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathadiya viduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathadiya viduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">yelo yelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelo "/>
</div>
<div class="lyrico-lyrics-wrapper">oor suthave nanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor suthave nanga"/>
</div>
<div class="lyrico-lyrics-wrapper">thingam kootaliya aavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thingam kootaliya aavom"/>
</div>
<div class="lyrico-lyrics-wrapper">aathorathil ninnu nanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathorathil ninnu nanga"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathadiya viduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathadiya viduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">kowthari muttaiya than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kowthari muttaiya than"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavadi eduthu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavadi eduthu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">alungaama omlet potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alungaama omlet potu"/>
</div>
<div class="lyrico-lyrics-wrapper">pangu vaipom naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pangu vaipom naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">thaattana kandu puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaattana kandu puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">sittaga paranthu kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sittaga paranthu kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnala ooduvome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnala ooduvome"/>
</div>
<div class="lyrico-lyrics-wrapper">potti pottu naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potti pottu naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">kottura malaiyil katturai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottura malaiyil katturai"/>
</div>
<div class="lyrico-lyrics-wrapper">notil kappal viduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="notil kappal viduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">nanga nanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanga nanga"/>
</div>
<div class="lyrico-lyrics-wrapper">sethula kuthipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethula kuthipom"/>
</div>
<div class="lyrico-lyrics-wrapper">aathula mithapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathula mithapom"/>
</div>
<div class="lyrico-lyrics-wrapper">vaathugal kootam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaathugal kootam "/>
</div>
<div class="lyrico-lyrics-wrapper">nanga nanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanga nanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">papara para para 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="papara para para "/>
</div>
<div class="lyrico-lyrics-wrapper">papara para para 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="papara para para "/>
</div>
<div class="lyrico-lyrics-wrapper">pattampoochi kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattampoochi kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">pambaram kozhi killi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pambaram kozhi killi"/>
</div>
<div class="lyrico-lyrics-wrapper">thandu engaloda aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandu engaloda aatam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puthi mahi sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthi mahi sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">atha ketukave matom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha ketukave matom"/>
</div>
<div class="lyrico-lyrics-wrapper">potiyinu vanthu nanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potiyinu vanthu nanga"/>
</div>
<div class="lyrico-lyrics-wrapper">payanthu oda matom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payanthu oda matom"/>
</div>
<div class="lyrico-lyrics-wrapper">school uh than leave vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="school uh than leave vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">school uh than leave vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="school uh than leave vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">appa amma pecha ketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appa amma pecha ketu"/>
</div>
<div class="lyrico-lyrics-wrapper">veetula thanga maaton
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetula thanga maaton"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yelo yelamba yelo yelamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelamba yelo yelamba"/>
</div>
<div class="lyrico-lyrics-wrapper">yelo yelamba yelo yelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelo yelamba yelo yelo"/>
</div>
</pre>
