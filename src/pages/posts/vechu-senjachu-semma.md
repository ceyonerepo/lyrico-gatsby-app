---
title: "vechu senjachu song lyrics"
album: "Semma"
artist: "G.V. Prakash Kumar"
lyricist: "Arunraja Kamaraj"
director: "Vallikanthan"
path: "/albums/semma-lyrics"
song: "Vechu Senjachu"
image: ../../images/albumart/semma.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Dh98MdZsj_E"
type: "happy"
singers:
  - Adhik Ravichandran
  - Rajaganapathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennada Athupa Suthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennada Athupa Suthura"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Appadithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Appadithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Suththuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suththuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Appadithaan Suththuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadithaan Suththuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennada Collara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennada Collara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thukkurra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thukkurra"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Appadithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Appadithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thukkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thukkuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Appadithaan Thukkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadithaan Thukkuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennada Avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennada Avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhaar Udraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhaar Udraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Rendaayiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Rendaayiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaila Irukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaila Irukkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Vachi Naamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Vachi Naamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Seirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Seirathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eththu Ethikava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththu Ethikava"/>
</div>
<div class="lyrico-lyrics-wrapper">Eththu Machi Eththu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththu Machi Eththu Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sound Ah Pottukava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sound Ah Pottukava"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu Machi Podu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu Machi Podu Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Geththa Kaatikava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geththa Kaatikava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatu Machi Kaatu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu Machi Kaatu Machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatam Pottukava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatam Pottukava"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu Machi Podu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu Machi Podu Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi Ennachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi Ennachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichi Thinnachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichi Thinnachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham Illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham Illama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Senjachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Senjachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Senjachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Senjachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Vechu Vechu Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Vechu Vechu Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Senjachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatha Vandhadhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatha Vandhadhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Polam Thatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polam Thatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Che Che Che
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Che Che Che"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweet Ethi Kalakatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweet Ethi Kalakatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Tea Tea Tea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tea Tea Tea"/>
</div>
<div class="lyrico-lyrics-wrapper">Gym Body Ellam Kedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gym Body Ellam Kedi"/>
</div>
<div class="lyrico-lyrics-wrapper">The Thee Thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Thee Thee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Thaane Kutti Kaththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Thaane Kutti Kaththi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morning Polappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morning Polappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nightu Kalappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nightu Kalappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhum Pannen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhum Pannen"/>
</div>
<div class="lyrico-lyrics-wrapper">Hardworku Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hardworku Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seera Nenachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seera Nenachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirappa Polacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirappa Polacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Life Eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Life Eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma Thaan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Thaan Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi Ennachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi Ennachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichi Thinnachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichi Thinnachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham Illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham Illama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Senjachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Senjachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Senjachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Senjachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Vechu Vechu Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Vechu Vechu Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Senjachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Veyilula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Veyilula"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chicken Biriyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chicken Biriyani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mukuvomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukuvomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty Sevuthula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty Sevuthula"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttalamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutha Vachu Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutha Vachu Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Siku Mamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siku Mamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salna Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salna Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Poratta Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poratta Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhu Iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhu Iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Tear Illa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tear Illa Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusu Pudhusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusu Pudhusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Trend-ah Pudichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trend-ah Pudichi"/>
</div>
<div class="lyrico-lyrics-wrapper">R?use-u Vida Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="R?use-u Vida Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure Illa Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure Illa Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi Ennachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi Ennachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichi Thinnachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichi Thinnachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham Illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham Illama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Senjachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Senjachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Senjachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Senjachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechu Vechu Vechu Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Vechu Vechu Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Senjachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vechu Senjiruvomm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechu Senjiruvomm"/>
</div>
</pre>
