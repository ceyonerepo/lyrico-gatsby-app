---
title: "go corono song lyrics"
album: "Zombie Reddy"
artist: "Mark K. Robin"
lyricist: "Mama Singh"
director: "Prasanth Varma"
path: "/albums/zombie-reddy-lyrics"
song: "Go Corono - Intlone Undamante"
image: ../../images/albumart/zombie-reddy.jpg
date: 2021-02-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/gsiqQJWzh4Y"
type: "motivational"
singers:
  - Mama Sing
  - Sri Krishna
  - Anudeep
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Intlone undamante oorukuntaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intlone undamante oorukuntaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Roadlannee khaaleega unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roadlannee khaaleega unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Raaka untaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaka untaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Evadenni cheptha unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evadenni cheptha unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Memu oorukuntaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Memu oorukuntaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa veepu pagile varaku maanukuntama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa veepu pagile varaku maanukuntama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey vadilyaalu aarabetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vadilyaalu aarabetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadalantha untam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadalantha untam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maa brandu uppukosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa brandu uppukosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorlu thiruguthuntam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorlu thiruguthuntam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa sintha chettu kinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa sintha chettu kinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pekalaadukuntaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pekalaadukuntaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivannee chesi kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivannee chesi kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paata paadukuntam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paata paadukuntam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye rogam ayithe endhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye rogam ayithe endhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakundhi kadha belaching powder
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakundhi kadha belaching powder"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee virus peekedhendhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee virus peekedhendhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa jebu ninda paracetamol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa jebu ninda paracetamol"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa raashtra budget anthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa raashtra budget anthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Wine shoplundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wine shoplundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaabatte mandhu koraku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaabatte mandhu koraku"/>
</div>
<div class="lyrico-lyrics-wrapper">Roaddu ninda mandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roaddu ninda mandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhi emainaa kaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhi emainaa kaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Matthu vadhalamandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthu vadhalamandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lockdown pedithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lockdown pedithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhi uchha aagahandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhi uchha aagahandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaitaka taka thai thaitaka taka thai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaitaka taka thai thaitaka taka thai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaitaka taka thai thaitaka taka thai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaitaka taka thai thaitaka taka thai"/>
</div>
<div class="lyrico-lyrics-wrapper">Online lo paataalantaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Online lo paataalantaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Background lo paatalu vintaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Background lo paatalu vintaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Six pack kalale kantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Six pack kalale kantu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppudu choosinaa thintaam pantaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudu choosinaa thintaam pantaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikottha vantalu choosthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikottha vantalu choosthaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mantabetti pente chesthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mantabetti pente chesthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Instantu noodle chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Instantu noodle chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Instalona buildup isthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Instalona buildup isthaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Matchingu maskulu vesthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matchingu maskulu vesthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">maru nimisham jebulo daasthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru nimisham jebulo daasthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Daggochhinaa thumme vachhinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daggochhinaa thumme vachhinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolatho doctor poojalu chesthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolatho doctor poojalu chesthaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarukulapai surf esesthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarukulapai surf esesthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanitizer snaanam chesthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanitizer snaanam chesthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Secret gaa party petti hatthuku pothaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Secret gaa party petti hatthuku pothaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hatthukupothaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hatthukupothaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaitaka taka thai etthukupothaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaitaka taka thai etthukupothaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaitaka taka thai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaitaka taka thai "/>
</div>
<div class="lyrico-lyrics-wrapper">etthukupothaam etthukupothaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etthukupothaam etthukupothaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaitaka taka thai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaitaka taka thai "/>
</div>
<div class="lyrico-lyrics-wrapper">etthukupothaam etthukupothaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etthukupothaam etthukupothaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey chappatlu kottaamante pm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey chappatlu kottaamante pm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey pallemtho chaavu dabbuleddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey pallemtho chaavu dabbuleddhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey cheekatla pettaamante deepam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey cheekatla pettaamante deepam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey advance deepaalavi cheddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey advance deepaalavi cheddhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ille hell ayipaaye ipaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ille hell ayipaaye ipaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Olle gullaipaaye ipaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olle gullaipaaye ipaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pelle lollai paaye ipaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pelle lollai paaye ipaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pille thallai paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pille thallai paaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
<div class="lyrico-lyrics-wrapper">Go corona go corona go go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go corona go corona go go"/>
</div>
</pre>
