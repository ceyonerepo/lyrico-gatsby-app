---
title: "ooh la laa song lyrics"
album: "Idi Naa Love Story"
artist: "Srinath Vijay"
lyricist: "Rameshgopi"
director: "Ramesh Gopi"
path: "/albums/idi-naa-love-story-lyrics"
song: "Ooh La Laa"
image: ../../images/albumart/idi-naa-love-story.jpg
date: 2018-02-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/gkWaLrT8JgU"
type: "happy"
singers:
  -	Ranjith Govind
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ooh la laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh la laa"/>
</div>
<div class="lyrico-lyrics-wrapper">champagnene pongaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="champagnene pongaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ollantha oogela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ollantha oogela"/>
</div>
<div class="lyrico-lyrics-wrapper">Music eh mogaalila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Music eh mogaalila"/>
</div>
<div class="lyrico-lyrics-wrapper">Pub-u llo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pub-u llo"/>
</div>
<div class="lyrico-lyrics-wrapper">liquor lo swim chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="liquor lo swim chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Babies tho dance chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babies tho dance chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Full night-u enjoy ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full night-u enjoy ila"/>
</div>
<div class="lyrico-lyrics-wrapper">TIME TO MOVE TO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TIME TO MOVE TO"/>
</div>
<div class="lyrico-lyrics-wrapper">BEAT ON THE DANCE FLOOR
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BEAT ON THE DANCE FLOOR"/>
</div>
<div class="lyrico-lyrics-wrapper">TIME TO GROOVE TO RHYTHM
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TIME TO GROOVE TO RHYTHM"/>
</div>
<div class="lyrico-lyrics-wrapper">NA NA NA NA
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="NA NA NA NA"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La Laser lighting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La Laser lighting"/>
</div>
<div class="lyrico-lyrics-wrapper">Shalaalla laughing shouting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shalaalla laughing shouting"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooh la laa beating breathing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh la laa beating breathing"/>
</div>
<div class="lyrico-lyrics-wrapper">Edola moving dancing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edola moving dancing"/>
</div>
<div class="lyrico-lyrics-wrapper">Oogala singing rapping
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oogala singing rapping"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogaala flooring rocking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogaala flooring rocking"/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom its weekend party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom its weekend party"/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang this full night party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang this full night party"/>
</div>
<div class="lyrico-lyrics-wrapper">ooh la laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooh la laa"/>
</div>
<div class="lyrico-lyrics-wrapper">champagnene pongaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="champagnene pongaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ollantha oogela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ollantha oogela"/>
</div>
<div class="lyrico-lyrics-wrapper">Music eh mogaalila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Music eh mogaalila"/>
</div>
<div class="lyrico-lyrics-wrapper">Pub-u llo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pub-u llo"/>
</div>
<div class="lyrico-lyrics-wrapper">liquor lo swim chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="liquor lo swim chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Babies tho dance chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babies tho dance chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Full night-u enjoy ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full night-u enjoy ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Beauty shape-u lu choopulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beauty shape-u lu choopulu"/>
</div>
<div class="lyrico-lyrics-wrapper">hip eh choosthe cocktail eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hip eh choosthe cocktail eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Grape juice-u expire ayithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Grape juice-u expire ayithe"/>
</div>
<div class="lyrico-lyrics-wrapper">healthy drink-u red wine eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="healthy drink-u red wine eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Crazy leni funny dance eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crazy leni funny dance eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Pub-u lo free style eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pub-u lo free style eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Volume-u pencheyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Volume-u pencheyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dance cheyyi natho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance cheyyi natho"/>
</div>
<div class="lyrico-lyrics-wrapper">Girls-u Boys-u theda ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girls-u Boys-u theda ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">pub-u lo halchal maadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pub-u lo halchal maadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Peddha chinna theda ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peddha chinna theda ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">matthulo maaku meme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matthulo maaku meme"/>
</div>
<div class="lyrico-lyrics-wrapper">Miss eh kamu weekend party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miss eh kamu weekend party"/>
</div>
<div class="lyrico-lyrics-wrapper">life-u lo thrilling maadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life-u lo thrilling maadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Tensionlanni trash eh chesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tensionlanni trash eh chesthu"/>
</div>
<div class="lyrico-lyrics-wrapper">enjoy lone ending choosey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enjoy lone ending choosey"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La Laser lighting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La Laser lighting"/>
</div>
<div class="lyrico-lyrics-wrapper">Shalaalla laughing shouting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shalaalla laughing shouting"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooh la laa beating breathing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh la laa beating breathing"/>
</div>
<div class="lyrico-lyrics-wrapper">Edola moving dancing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edola moving dancing"/>
</div>
<div class="lyrico-lyrics-wrapper">Oogala singing rapping
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oogala singing rapping"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogaala flooring rocking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogaala flooring rocking"/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom its weekend party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom its weekend party"/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang this full night party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang this full night party"/>
</div>
<div class="lyrico-lyrics-wrapper">ooh la laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooh la laa"/>
</div>
<div class="lyrico-lyrics-wrapper">champagnene pongaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="champagnene pongaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ollantha oogela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ollantha oogela"/>
</div>
<div class="lyrico-lyrics-wrapper">Music eh mogaalila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Music eh mogaalila"/>
</div>
<div class="lyrico-lyrics-wrapper">Pub-u llo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pub-u llo"/>
</div>
<div class="lyrico-lyrics-wrapper">liquor lo swim chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="liquor lo swim chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Babies tho dance chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babies tho dance chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Full night-u enjoy ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full night-u enjoy ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey ye ee ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ye ee ye"/>
</div>
<div class="lyrico-lyrics-wrapper">hey Ah Ah Ah Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey Ah Ah Ah Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooh ye Ah haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh ye Ah haa"/>
</div>
<div class="lyrico-lyrics-wrapper">ooh ye come on everybody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooh ye come on everybody"/>
</div>
<div class="lyrico-lyrics-wrapper">Vodka thaagina beauty 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vodka thaagina beauty "/>
</div>
<div class="lyrico-lyrics-wrapper">chooputho mind eh poyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chooputho mind eh poyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Whisky vesina baby 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whisky vesina baby "/>
</div>
<div class="lyrico-lyrics-wrapper">shape tho kick eh ekkindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="shape tho kick eh ekkindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Beauty girls dance-u nu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beauty girls dance-u nu "/>
</div>
<div class="lyrico-lyrics-wrapper">choosi B.P perigindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choosi B.P perigindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Volume penchey Dance chey natho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Volume penchey Dance chey natho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammayi chethilo iphone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammayi chethilo iphone "/>
</div>
<div class="lyrico-lyrics-wrapper">laaga nene naligi pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laaga nene naligi pona"/>
</div>
<div class="lyrico-lyrics-wrapper">sweety sitting seat eh 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sweety sitting seat eh "/>
</div>
<div class="lyrico-lyrics-wrapper">nenai mathuga hatthukupona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenai mathuga hatthukupona"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathulo vaalina table 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathulo vaalina table "/>
</div>
<div class="lyrico-lyrics-wrapper">nenai ninnu mostha sona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenai ninnu mostha sona"/>
</div>
<div class="lyrico-lyrics-wrapper">DJ music raise ayindhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="DJ music raise ayindhi "/>
</div>
<div class="lyrico-lyrics-wrapper">enjoy lone ending choosey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enjoy lone ending choosey"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La Laser lighting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La Laser lighting"/>
</div>
<div class="lyrico-lyrics-wrapper">Shalaalla laughing shouting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shalaalla laughing shouting"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooh la laa beating breathing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh la laa beating breathing"/>
</div>
<div class="lyrico-lyrics-wrapper">Edola moving dancing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edola moving dancing"/>
</div>
<div class="lyrico-lyrics-wrapper">Oogala singing rapping
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oogala singing rapping"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogaala flooring rocking
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogaala flooring rocking"/>
</div>
<div class="lyrico-lyrics-wrapper">Boom Boom its weekend party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boom Boom its weekend party"/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang this full night party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang this full night party"/>
</div>
<div class="lyrico-lyrics-wrapper">End Thankyou By NarayanGelli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="End Thankyou By NarayanGelli"/>
</div>
</pre>
