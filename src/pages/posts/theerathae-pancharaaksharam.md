---
title: "theerathae song lyrics"
album: "Pancharaaksharam"
artist: "K.S. Sundaramurthy"
lyricist: "Soundararajan"
director: "Balaji Vairamuthu"
path: "/albums/pancharaaksharam-lyrics"
song: "Theerathae"
image: ../../images/albumart/pancharaaksharam.jpg
date: 2019-12-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bcrO1pLMAYk"
type: "happy"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaa aa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theerathae un isaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerathae un isaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee moottum un alaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee moottum un alaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae illatha naanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae illatha naanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnalae aanen uyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnalae aanen uyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneerin azhaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerin azhaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kankonda kaayaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kankonda kaayaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinmeengal vaasththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinmeengal vaasththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naan parppenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naan parppenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanatha dhesaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanatha dhesaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkatha thooraththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkatha thooraththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae nee ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae nee ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naan servenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naan servenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paarvai urasaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvai urasaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaikul naanillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaikul naanillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paarvai urasaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvai urasaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaalai pozhuthillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaalai pozhuthillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjaththai adiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjaththai adiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eerkindra pennae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerkindra pennae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thaakkum usuraliyae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thaakkum usuraliyae ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuraaliyae aeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraaliyae aeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaatrilaeSumaithaan kooduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaatrilaeSumaithaan kooduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">En tholilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En tholilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimai kaayamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai kaayamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaintha naatkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaintha naatkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthirntha nesam malaruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthirntha nesam malaruthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasaiyum iragaai ilaguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasaiyum iragaai ilaguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisaiyum thiranthu azhaikuthae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisaiyum thiranthu azhaikuthae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theerathae un uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerathae un uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee moottum ul unarvae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee moottum ul unarvae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelam illaatha vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelam illaatha vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalum illai uyiraeae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum illai uyiraeae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paarvai urasaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvai urasaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaikul naanillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaikul naanillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paarvai urasaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvai urasaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaalai pozhuthillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaalai pozhuthillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjaththai adiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjaththai adiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eerkindra pennae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerkindra pennae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai thaakkum usuraliyaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai thaakkum usuraliyaeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usuraliyae haeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraliyae haeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuraaaliyae haeaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraaaliyae haeaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuraliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuraliiyae haeae aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraliiyae haeae aaa"/>
</div>
</pre>
