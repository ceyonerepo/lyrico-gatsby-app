---
title: "are emi ayyindho song lyrics"
album: "Thellavarithe Guruvaram"
artist: "Kaala Bhairava"
lyricist: "Punam Nath"
director: "Manikanth Gelli"
path: "/albums/thellavarithe-guruvaram-lyrics"
song: "Are Emi Ayyindho Emo"
image: ../../images/albumart/thellavarithe-guruvaram.jpg
date: 2021-03-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/sPmD4pPSiAc"
type: "love"
singers:
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Are em ayyindho emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are em ayyindho emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre emayyindho emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre emayyindho emo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre evare evare pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre evare evare pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee cheyi paduthunte illaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee cheyi paduthunte illaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pulse-ye perige ellaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pulse-ye perige ellaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Majnu leni lailaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majnu leni lailaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasantha modhalaindhi gola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasantha modhalaindhi gola"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh mandhe isthe polaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh mandhe isthe polaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ooh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evo kalale kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evo kalale kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho nijamanukunnaa rojoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho nijamanukunnaa rojoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maate thadabaduthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate thadabaduthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Baashe pagabaduthondhe naapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baashe pagabaduthondhe naapai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre emayyindho emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre emayyindho emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee choopu thaakina ee nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee choopu thaakina ee nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre emayyindho emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre emayyindho emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalakindhulayyene naa lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalakindhulayyene naa lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre emayyindho emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre emayyindho emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaallu theliyadhe ee maikam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallu theliyadhe ee maikam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre emayyindho emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre emayyindho emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre chethavaalene aa swargam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre chethavaalene aa swargam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evo kalale kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evo kalale kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho nijamanukunnaa rojoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho nijamanukunnaa rojoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maate thadabaduthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate thadabaduthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Baashe pagabaduthondhe naapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baashe pagabaduthondhe naapai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tha re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha re"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho nuvvunte gadiyaarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho nuvvunte gadiyaarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruge peduthunnadhe, oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruge peduthunnadhe, oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho ne leni nimishaalake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho ne leni nimishaalake"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadake raakunnadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadake raakunnadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre emayyindho emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre emayyindho emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogadaali ante nee andhaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogadaali ante nee andhaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre emayyindho emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre emayyindho emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethakaali kotthagaa polikani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethakaali kotthagaa polikani"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre emayyindho emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre emayyindho emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaaraanni katti aa vegaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaaraanni katti aa vegaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre emayyindho emo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre emayyindho emo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neepaina kuripistha aa varshaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neepaina kuripistha aa varshaanni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evo kalale kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evo kalale kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho nijamanukunnaa rojoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho nijamanukunnaa rojoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maate thadabaduthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate thadabaduthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Baashe pagabaduthondhe naapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baashe pagabaduthondhe naapai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arre evare evare pillaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre evare evare pillaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee cheyi paduthunte illaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee cheyi paduthunte illaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pulse-ye perige ellaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pulse-ye perige ellaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Majnu leni lailaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majnu leni lailaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasantha modhalaindhi gola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasantha modhalaindhi gola"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo mandhe isthe polaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo mandhe isthe polaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh ooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh ooh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evo kalale kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evo kalale kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho nijamanukunnaa rojoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho nijamanukunnaa rojoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Maate thadabaduthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate thadabaduthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Baashe pagabaduthondhe naapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baashe pagabaduthondhe naapai"/>
</div>
</pre>
