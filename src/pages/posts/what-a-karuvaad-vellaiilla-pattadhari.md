---
title: "what a karuvaad song lyrics"
album: "Vellaiilla Pattadhari"
artist: "Anirudh Ravichander"
lyricist: "Dhanush"
director: "Velraj"
path: "/albums/vellaiilla-pattadhari-lyrics"
song: "What a Karuvaad"
image: ../../images/albumart/vellaiilla-pattadhari.jpg
date: 2014-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/P2YRN8iGQ6g"
type: "Enjoy"
singers:
  - Anirudh Ravichander
  - Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">What a Karavaad What a Karavaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karavaad What a Karavaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karavaadu What Karuvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karavaadu What Karuvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karuvaad What a Karuvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaad What a Karuvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karuvaadu What a Karuvaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaadu What a Karuvaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutta Vada Pochuda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutta Vada Pochuda "/>
</div>
<div class="lyrico-lyrics-wrapper">What a Karvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattam Kilunju Pochuda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Kilunju Pochuda "/>
</div>
<div class="lyrico-lyrics-wrapper">What a Karvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kattam Alunju Pochuda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kattam Alunju Pochuda "/>
</div>
<div class="lyrico-lyrics-wrapper">What a Karuvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Mattam Thattiyaachuda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Mattam Thattiyaachuda "/>
</div>
<div class="lyrico-lyrics-wrapper">What a Karvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Karuthu Solla Porenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Karuthu Solla Porenda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Kar What a Kar What a Karu What a Karu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Kar What a Kar What a Karu What a Karu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Kar What a Kar What a Kar What a Kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Kar What a Kar What a Kar What a Kar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Kar What a Kar What a Kar What a Kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Kar What a Kar What a Kar What a Kar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karuvaad What a Karvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaad What a Karvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karuvaadu What a Karvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaadu What a Karvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karuvaad What a Karvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaad What a Karvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karuvaadu What a Karvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaadu What a Karvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhum Pa Off Pannikkalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum Pa Off Pannikkalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idho Ippo Epdi Off Panrenu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho Ippo Epdi Off Panrenu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Brother Calm Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brother Calm Down"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yetho Karuthu Solrenu Sonneengale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetho Karuthu Solrenu Sonneengale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athaiyaavathu Sollunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaiyaavathu Sollunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Karuthu What is iam Saying
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Karuthu What is iam Saying"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakka Karuppu Batta Seruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakka Karuppu Batta Seruppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sharppa Iru da Puriyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharppa Iru da Puriyaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maangaai Pulikkum Maambalam Inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maangaai Pulikkum Maambalam Inikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithuthaan Vaazhkai Maaraathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuthaan Vaazhkai Maaraathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Kedacha Vechukka Vechukka Vechukka Vechukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Kedacha Vechukka Vechukka Vechukka Vechukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Pochaa Vittududaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Pochaa Vittududaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lucku Aducha Allikka Allikka Allikka Allikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lucku Aducha Allikka Allikka Allikka Allikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lucku Pocha Thallikkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lucku Pocha Thallikkada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Perumaale Hey Oh Perumaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Perumaale Hey Oh Perumaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Nambi Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nambi Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalangaama Irukkenpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangaama Irukkenpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Thedi Suththurane Naanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Thedi Suththurane Naanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Paarva En Mela Ada Thirumbave Thirumbala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paarva En Mela Ada Thirumbave Thirumbala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karuvaad What a Karuvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaad What a Karuvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karuvaadu What a Karuvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaadu What a Karuvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karuvaad What a Karuvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaad What a Karuvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karuvaadu What a Karuvaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaadu What a Karuvaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karuvaad What a Karvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaad What a Karvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karuvaadu What a Karuvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaadu What a Karuvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karuvaad What a Karvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaad What a Karvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">What a Karuvaadu What a Karuvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaadu What a Karuvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah Sutta Vada Vada Vada Vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah Sutta Vada Vada Vada Vada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutta Vada Vada Vada Vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutta Vada Vada Vada Vada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutta Vada Pochuda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutta Vada Pochuda "/>
</div>
<div class="lyrico-lyrics-wrapper">What a Karvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pattam Kilunju Pochuda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pattam Kilunju Pochuda "/>
</div>
<div class="lyrico-lyrics-wrapper">What a Karvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kattam Alunju Pochuda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kattam Alunju Pochuda "/>
</div>
<div class="lyrico-lyrics-wrapper">What a Karvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Mattam Thattiyaachuda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Mattam Thattiyaachuda "/>
</div>
<div class="lyrico-lyrics-wrapper">What a Karuvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What a Karuvaad"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Karuthu Solla Porenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Karuthu Solla Porenda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaniye Pudunga Vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaniye Pudunga Vendaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey What a Karuvaad What a Karuvaad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey What a Karuvaad What a Karuvaad"/>
</div>
</pre>
