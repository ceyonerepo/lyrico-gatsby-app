---
title: "karna karni ka song lyrics"
album: "Anaganaga O Athidhi"
artist: "Arrol Corelli"
lyricist: "Kalyan Chakravarthy"
director: "Dayal Padmanabhan"
path: "/albums/anaganaga-o-athidhi-lyrics"
song: "Karna Karni Ka"
image: ../../images/albumart/anaganaga-o-athidhi.jpg
date: 2020-11-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/D_uFZMMf5E0"
type: "melody"
singers:
  - Deepak Blues
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Karna karnika vintava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karna karnika vintava"/>
</div>
<div class="lyrico-lyrics-wrapper">Karna katora mee maata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karna katora mee maata"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalu biddalu leranta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalu biddalu leranta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashaku chikki ye chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashaku chikki ye chota"/>
</div>
<div class="lyrico-lyrics-wrapper">Guppita dache thappedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppita dache thappedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Guppuna aaratheseno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppuna aaratheseno"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkunu muse veeledho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkunu muse veeledho "/>
</div>
<div class="lyrico-lyrics-wrapper">thappuku poye theereno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappuku poye theereno"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee aaratam gelupe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aaratam gelupe "/>
</div>
<div class="lyrico-lyrics-wrapper">leni o paatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="leni o paatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa gaalula gajjala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gaalula gajjala "/>
</div>
<div class="lyrico-lyrics-wrapper">jallakola golakalatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jallakola golakalatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chimme chikati vakitlo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chimme chikati vakitlo "/>
</div>
<div class="lyrico-lyrics-wrapper">chivve bandheeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chivve bandheeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gana ganamane kshana kshanamila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gana ganamane kshana kshanamila"/>
</div>
<div class="lyrico-lyrics-wrapper">Musi musi vadhe musrinadhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musi musi vadhe musrinadhila"/>
</div>
<div class="lyrico-lyrics-wrapper">Poli kalaike poli pilupula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poli kalaike poli pilupula"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedu chevitidhi thanu katikati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedu chevitidhi thanu katikati"/>
</div>
<div class="lyrico-lyrics-wrapper">Thegu vila vile vinabadanidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegu vila vile vinabadanidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithi malupidhi chivariki vidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithi malupidhi chivariki vidhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammakamedho ammakam aaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammakamedho ammakam aaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammani amme kammuku poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammani amme kammuku poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Molaketthe vichhula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molaketthe vichhula "/>
</div>
<div class="lyrico-lyrics-wrapper">katthiki metthani navvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katthiki metthani navvula"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchhula pootha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchhula pootha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala etthi molasina chote 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala etthi molasina chote "/>
</div>
<div class="lyrico-lyrics-wrapper">mugisi poye raatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugisi poye raatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Garchinchina kali kalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garchinchina kali kalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varjinchina visha shakalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varjinchina visha shakalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Polamarina yama pasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamarina yama pasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalpaniki vidadhee dosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalpaniki vidadhee dosam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Garchinchina kali kalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garchinchina kali kalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varjinchina visha shakalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varjinchina visha shakalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Polamarina yama pasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamarina yama pasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalpaniki vidadhee dosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalpaniki vidadhee dosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeragani kathedho jariginadhilaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeragani kathedho jariginadhilaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">marigina gathalu oorigenila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marigina gathalu oorigenila"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukoni athidhi adagani samadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukoni athidhi adagani samadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutilapu manaadhi kathanamula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutilapu manaadhi kathanamula"/>
</div>
<div class="lyrico-lyrics-wrapper">Gana ganamane kshana kshanamila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gana ganamane kshana kshanamila"/>
</div>
<div class="lyrico-lyrics-wrapper">Musi musi vadhe musrinadhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musi musi vadhe musrinadhila"/>
</div>
<div class="lyrico-lyrics-wrapper">Poli kalaike poli pilupula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poli kalaike poli pilupula"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedu chevitidhi thanu katikati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedu chevitidhi thanu katikati"/>
</div>
<div class="lyrico-lyrics-wrapper">Thegu vila vile vinabadanidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegu vila vile vinabadanidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithi malupidhi chivariki vidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithi malupidhi chivariki vidhi"/>
</div>
</pre>
