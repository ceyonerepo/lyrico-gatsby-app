---
title: "uchcha pasanga uchcha pasanga song lyrics"
album: "Maanik"
artist: "Dharan Kumar"
lyricist: "Mirchi Vijay"
director: "Martyn"
path: "/albums/maanik-lyrics"
song: "Uchcha Pasanga Uchcha Pasanga"
image: ../../images/albumart/maanik.jpg
date: 2019-01-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/o_xhqap6q_A"
type: "happy"
singers:
  - Vaikom Vijayalakshmi
  - Chris Stephan Fernando
  - Rita Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">oruthavan pakka maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oruthavan pakka maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">innoruthavan double maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innoruthavan double maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu perum onu sendha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu perum onu sendha"/>
</div>
<div class="lyrico-lyrics-wrapper">seivanga businessu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seivanga businessu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oruthavan super hittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oruthavan super hittu"/>
</div>
<div class="lyrico-lyrics-wrapper">innoruthavan poduvan bittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innoruthavan poduvan bittu"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu perum onnu sendha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu perum onnu sendha"/>
</div>
<div class="lyrico-lyrics-wrapper">gappula dhan keda vettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gappula dhan keda vettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaarukkuda venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarukkuda venum"/>
</div>
<div class="lyrico-lyrics-wrapper">un anudhaba ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un anudhaba ottu"/>
</div>
<div class="lyrico-lyrics-wrapper">yeduthu neeyum thayen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeduthu neeyum thayen"/>
</div>
<div class="lyrico-lyrics-wrapper">oru otha rooba nottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru otha rooba nottu"/>
</div>
<div class="lyrico-lyrics-wrapper">illana ne poyi ada koyil la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illana ne poyi ada koyil la"/>
</div>
<div class="lyrico-lyrics-wrapper">kai neetu apurama nee vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai neetu apurama nee vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">un moonja neeyum kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un moonja neeyum kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paavapatta scene nu venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavapatta scene nu venam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaya moodu neyum pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaya moodu neyum pona"/>
</div>
<div class="lyrico-lyrics-wrapper">paavapatta scene nu venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavapatta scene nu venam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaya moodu neyum pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaya moodu neyum pona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchcha pasanga uchcha pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchcha pasanga uchcha pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">uchanthala rendu pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchanthala rendu pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">uchcha poyi machan aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchcha poyi machan aana"/>
</div>
<div class="lyrico-lyrics-wrapper">saithanki bacha pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saithanki bacha pasanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchcha pasanga uchcha pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchcha pasanga uchcha pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">uchanthala rendu pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchanthala rendu pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">uchcha poyi machan aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchcha poyi machan aana"/>
</div>
<div class="lyrico-lyrics-wrapper">saithanki bacha pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saithanki bacha pasanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchcha pasanga uchcha pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchcha pasanga uchcha pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">uchanthala rendu pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchanthala rendu pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">uchcha poyi machan aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchcha poyi machan aana"/>
</div>
<div class="lyrico-lyrics-wrapper">saithanki bacha pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saithanki bacha pasanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paavapatta scene nu venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavapatta scene nu venam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaya moodu neyum pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaya moodu neyum pona"/>
</div>
<div class="lyrico-lyrics-wrapper">paavapatta scene nu venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavapatta scene nu venam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaya moodu neyum pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaya moodu neyum pona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kingni mingi yaa aaya yengayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kingni mingi yaa aaya yengayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">paaty onu yaa pasanga rendu yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaty onu yaa pasanga rendu yaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kingni mingi yaa aaya yengayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kingni mingi yaa aaya yengayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">paaty onu yaa pasanga rendu yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaty onu yaa pasanga rendu yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oruthavan chinna mullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oruthavan chinna mullu"/>
</div>
<div class="lyrico-lyrics-wrapper">inoruthavan periya mullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inoruthavan periya mullu"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu perum onu sendha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu perum onu sendha"/>
</div>
<div class="lyrico-lyrics-wrapper">all time sema thillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all time sema thillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iyayo ungala ipadiya da valathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyayo ungala ipadiya da valathen"/>
</div>
<div class="lyrico-lyrics-wrapper">dai dai yenna da shot adikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dai dai yenna da shot adikira"/>
</div>
<div class="lyrico-lyrics-wrapper">dhoni madhiri helicapto shot ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhoni madhiri helicapto shot ah"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla kaala thooki adra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla kaala thooki adra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaaka kaati soru ootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaka kaati soru ootum"/>
</div>
<div class="lyrico-lyrics-wrapper">palaya paaty illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palaya paaty illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">kakka mutai patty pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kakka mutai patty pola"/>
</div>
<div class="lyrico-lyrics-wrapper">pizza suttu tharuven da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pizza suttu tharuven da"/>
</div>
<div class="lyrico-lyrics-wrapper">ipl season vandha na romba busy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipl season vandha na romba busy"/>
</div>
<div class="lyrico-lyrics-wrapper">because i am dhoni fan u see
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="because i am dhoni fan u see"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">parottakku mutta kurma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parottakku mutta kurma"/>
</div>
<div class="lyrico-lyrics-wrapper">pasathuku enga grandma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasathuku enga grandma"/>
</div>
<div class="lyrico-lyrics-wrapper">pasanga dha romba naughty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasanga dha romba naughty"/>
</div>
<div class="lyrico-lyrics-wrapper">paaty than romba sweety
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaty than romba sweety"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaal mela kaal potu ukkarum haaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaal mela kaal potu ukkarum haaya"/>
</div>
<div class="lyrico-lyrics-wrapper">indha pullingala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha pullingala "/>
</div>
<div class="lyrico-lyrics-wrapper">pathuka poogathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathuka poogathu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha aaya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha aaya "/>
</div>
<div class="lyrico-lyrics-wrapper">csk match ada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="csk match ada "/>
</div>
<div class="lyrico-lyrics-wrapper">paaty set and wachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaty set and wachi"/>
</div>
<div class="lyrico-lyrics-wrapper">catch miss  agi pona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="catch miss  agi pona "/>
</div>
<div class="lyrico-lyrics-wrapper">ada ninnudum da moochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada ninnudum da moochi"/>
</div>
<div class="lyrico-lyrics-wrapper">last two ball lu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="last two ball lu"/>
</div>
<div class="lyrico-lyrics-wrapper">we need twelve run nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we need twelve run nu"/>
</div>
<div class="lyrico-lyrics-wrapper">dhoni dhanda closingla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhoni dhanda closingla"/>
</div>
<div class="lyrico-lyrics-wrapper">yapavume kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yapavume kannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paavapata scene nu venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavapata scene nu venam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaya moodinu neeyum pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaya moodinu neeyum pona"/>
</div>
<div class="lyrico-lyrics-wrapper">paavapata scene nu venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavapata scene nu venam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaya moodinu neeyum pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaya moodinu neeyum pona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchcha pasanga uchcha pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchcha pasanga uchcha pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">uchanthala rendu pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchanthala rendu pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">uchcha poyi machan aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchcha poyi machan aana"/>
</div>
<div class="lyrico-lyrics-wrapper">saithanki bacha pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saithanki bacha pasanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchcha pasanga uchcha pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchcha pasanga uchcha pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">uchanthala rendu pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchanthala rendu pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">uchcha poyi machan aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchcha poyi machan aana"/>
</div>
<div class="lyrico-lyrics-wrapper">saithanki bacha pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saithanki bacha pasanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchcha pasanga uchcha pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchcha pasanga uchcha pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">uchanthala rendu pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchanthala rendu pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">uchcha poyi machan aana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchcha poyi machan aana"/>
</div>
<div class="lyrico-lyrics-wrapper">saithanki bacha pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saithanki bacha pasanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paavapata scene nu venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavapata scene nu venam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaya moodinu neeyum pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaya moodinu neeyum pona"/>
</div>
<div class="lyrico-lyrics-wrapper">paavapata scene nu venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavapata scene nu venam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaya moodinu neeyum pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaya moodinu neeyum pona"/>
</div>
</pre>
