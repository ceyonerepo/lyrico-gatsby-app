---
title: "darrnakka dhakkunakka song lyrics"
album: "Saamy 2"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari"
path: "/albums/saamy-2-song-lyrics"
song: "Darrnakka Dhakkunakka"
image: ../../images/albumart/saamy-2.jpg
date: 2018-09-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YWFtHEPvTaE"
type: "Mass Intro"
singers:
  - Benny Dayal
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yo perusu…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo perusu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Idli thosai aappam ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idli thosai aappam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arisi jaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arisi jaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poori roti chappati ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poori roti chappati ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Godhumai jaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godhumai jaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kichadi kesari uppuma ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichadi kesari uppuma ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ravva jaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravva jaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaki sattai potta ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaki sattai potta ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Police jaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police jaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey naan porantha oorula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey naan porantha oorula"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanum innum maarala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanum innum maarala"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasu aarala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasu aarala"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluratha kelulae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluratha kelulae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kettavana thallula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettavana thallula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallavana allula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallavana allula"/>
</div>
<div class="lyrico-lyrics-wrapper">Serious-ah paarula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serious-ah paarula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkirama maarula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkirama maarula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey aadi-na month illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey aadi-na month illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Car-u ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car-u ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Item-na porul illa ponnu ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Item-na porul illa ponnu ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene-na padamilla bandha ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene-na padamilla bandha ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Cellu-na jail illa phone-u ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cellu-na jail illa phone-u ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Matter-na news illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matter-na news illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Meter-na length illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meter-na length illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhammu-na strength illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhammu-na strength illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni-na water illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni-na water illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Let’s do it…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s do it…"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s shake it…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s shake it…"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka ((2) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka ((2) times)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhoomi yen suthudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomi yen suthudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalu yen kathudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalu yen kathudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppu yen pathudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppu yen pathudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nondadha uttudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nondadha uttudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ozone-il poththalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozone-il poththalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarellaam vathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarellaam vathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivasayi kathalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivasayi kathalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakellam sikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakellam sikkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mangaana kaai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangaana kaai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madayan ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madayan ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattaina sariyaana bodhai ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattaina sariyaana bodhai ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pangu-na share illa friend-u ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangu-na share illa friend-u ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Bulbu-na light illa verai ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulbu-na light illa verai ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammoda pechellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda pechellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nonsense-ah maariyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nonsense-ah maariyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyaiyo adhumela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaiyo adhumela"/>
</div>
<div class="lyrico-lyrics-wrapper">Bulldozer yeri yaachae…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulldozer yeri yaachae….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Let’s do it…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s do it…"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s shake it…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s shake it…"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka ((2) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka ((2) times)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei dolu dolu dapsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei dolu dolu dapsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei takkaru takkaru dupsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei takkaru takkaru dupsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei dress-eh podu class-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei dress-eh podu class-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada dance-eh podu maas-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dance-eh podu maas-ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei chettan kadai chaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei chettan kadai chaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha aattu kaalu paaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha aattu kaalu paaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee serthu onna thandhu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee serthu onna thandhu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudichukuven haaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudichukuven haaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei machan enthiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei machan enthiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Varra paaru sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varra paaru sundari"/>
</div>
<div class="lyrico-lyrics-wrapper">Kindi vacha kesari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kindi vacha kesari"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma strawberry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma strawberry"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei coco cola pepsi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei coco cola pepsi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan oturadhu gipsy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oturadhu gipsy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada thinnukalam kulfi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada thinnukalam kulfi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa eduthukalam selfie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa eduthukalam selfie"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka darrnaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka darrnaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka darrnaka ((2) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka darrnaka ((2) times)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Police-eh gethu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police-eh gethu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Porumai ellaam rathu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porumai ellaam rathu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Inform-u panni putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inform-u panni putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirm-ah death-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirm-ah death-u da"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattaiyila star-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattaiyila star-u da"/>
</div>
<div class="lyrico-lyrics-wrapper">Siren vacha car-uda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siren vacha car-uda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuppariyum moolai da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuppariyum moolai da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongurathae illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongurathae illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mufti-la irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mufti-la irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Duty paapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duty paapom"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaiyaga irunthaalum aatti paapom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyaga irunthaalum aatti paapom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda area limitukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda area limitukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Evankkum vaalaata urimai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evankkum vaalaata urimai illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandai ellam bore-u la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandai ellam bore-u la"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangamellam thevailla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangamellam thevailla"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathi matham pesaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathi matham pesaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaruchaami oorula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaruchaami oorula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Let’s do it…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s do it…"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Let’s shake it…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let’s shake it…"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrnaka dakkunakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrnaka dakkunakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dakkunakka dakkunakka ((2) times)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dakkunakka dakkunakka ((2) times)"/>
</div>
</pre>
