---
title: "asamanjakari song lyrics"
album: "Kazhugu 2"
artist: "Yuvan Shankar Raja"
lyricist: "Mohan Rajan"
director: "Sathyasiva"
path: "/albums/kazhugu-2-lyrics"
song: "Asamanjakari"
image: ../../images/albumart/kazhugu-2.jpg
date: 2019-08-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EWUHNjxZbGc"
type: "love"
singers:
  - Syed Subahan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Asamanjakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asamanjakari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Vesham Vecha Keeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Vesham Vecha Keeri"/>
</div>
<div class="lyrico-lyrics-wrapper">En Asamajakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Asamajakari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Vesham Vecha Keeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Vesham Vecha Keeri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenga Ponalum Ennai Vidame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Ponalum Ennai Vidame"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengayum Vaara Enna Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengayum Vaara Enna Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonga Ponalum Thooram Ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonga Ponalum Thooram Ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoda Neethane Verai Ille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoda Neethane Verai Ille"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadi En Kazhugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi En Kazhugi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Usure Thaan Uruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usure Thaan Uruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Soorakaatha Pola Aala Saachiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorakaatha Pola Aala Saachiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhouthaiya Pattene Vartaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhouthaiya Pattene Vartaiyilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asamanjakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asamanjakari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Vesham Vecha Keeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Vesham Vecha Keeri"/>
</div>
<div class="lyrico-lyrics-wrapper">En Asamajakari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Asamajakari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Vesham Vecha Keeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Vesham Vecha Keeri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Sonnalum Ennai Konnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Sonnalum Ennai Konnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Vaaren Enna Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Vaaren Enna Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Moochula Naan Pesum Pechula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Moochula Naan Pesum Pechula"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppothum Neethandi Verai Ille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppothum Neethandi Verai Ille"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadi En Kazhugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi En Kazhugi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Usure Thaan Uruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usure Thaan Uruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Soorakaatha Pola Aala Saachiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorakaatha Pola Aala Saachiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhouthaiya Pattene Vartaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhouthaiya Pattene Vartaiyilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettu Paraiya Pachai Koraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettu Paraiya Pachai Koraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vanthu Nee Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vanthu Nee Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaththi Putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaththi Putta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollakaarene Vellaikaaran Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollakaarene Vellaikaaran Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Medukka Nee Nadakka Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medukka Nee Nadakka Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechi Putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechi Putta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manase Idhu Onnu Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Idhu Onnu Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusa Ini Vaazha Thonum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusa Ini Vaazha Thonum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhusa Nee Sehrai Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhusa Nee Sehrai Venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Ooru Metchum Vazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Ooru Metchum Vazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhnthida Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhnthida Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeda Podum Kannala Paakkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeda Podum Kannala Paakkuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Yesa Pola En Nenja Thaakkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yesa Pola En Nenja Thaakkuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadi En Usure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi En Usure"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Konjam Pisure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Konjam Pisure"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaalum Ennai Eppadi Neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaalum Ennai Eppadi Neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalum Senjiyo Puriyaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalum Senjiyo Puriyaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadi En Kazhugi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi En Kazhugi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Usure Thaan Uruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usure Thaan Uruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Soorakaatha Pola Aala Saachiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorakaatha Pola Aala Saachiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhouthaiya Pattene Vartaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhouthaiya Pattene Vartaiyilla"/>
</div>
</pre>
