---
title: "kuruvi kodanja koyapalam song lyrics"
album: "Azhagi"
artist: "Ilaiyaraaja"
lyricist: "Palani Bharathi"
director: "Thangar Bachchan"
path: "/albums/azhagi-lyrics"
song: "Kuruvi Kodanja Koyapalam"
image: ../../images/albumart/azhagi.jpg
date: 2002-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IwqHhyZkB_U"
type: "happy"
singers:
  - Pushpavanam Kuppusamy
  - Swarnalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kuruvi kodanja goyapazham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruvi kodanja goyapazham"/>
</div>
<div class="lyrico-lyrics-wrapper">kondu vanthu tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondu vanthu tharava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruvi kodanja goyapazham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruvi kodanja goyapazham"/>
</div>
<div class="lyrico-lyrics-wrapper">kondu vanthu tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondu vanthu tharava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumari ponnu vethalaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumari ponnu vethalaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">sunambu naa tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sunambu naa tharava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yesunna yesnu sollu no vunna nonu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yesunna yesnu sollu no vunna nonu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">yesunna yesnu sollu no vunna nonu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yesunna yesnu sollu no vunna nonu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">aah aahn aah aahn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aah aahn aah aahn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kothugira kuruvikellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kothugira kuruvikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">goyapazham naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goyapazham naana"/>
</div>
<div class="lyrico-lyrics-wrapper">ennudaiya vethalaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennudaiya vethalaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">sunambu thaan vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sunambu thaan vena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">konjam nee thaliye nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam nee thaliye nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">konjina kothidum mullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjina kothidum mullu"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam nee thaliye nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam nee thaliye nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">konjina kothidum mullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjina kothidum mullu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kothugira kuruvikellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kothugira kuruvikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">goyapazham naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goyapazham naana"/>
</div>
<div class="lyrico-lyrics-wrapper">ennudaiya vethalaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennudaiya vethalaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">sunambu thaan vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sunambu thaan vena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhichi muzhichi parkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhichi muzhichi parkudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaiyil irukura mosakutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaiyil irukura mosakutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhikuthadi kotta kotta muzhikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhikuthadi kotta kotta muzhikuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">valachi pudichu moochu mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valachi pudichu moochu mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">enne vetaigal aadida thoonduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enne vetaigal aadida thoonduthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">thoonduthadi pudika thonduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoonduthadi pudika thonduthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unne naa ulla vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unne naa ulla vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattukettu pogumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattukettu pogumada"/>
</div>
<div class="lyrico-lyrics-wrapper">unudaiya ambu patta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unudaiya ambu patta"/>
</div>
<div class="lyrico-lyrics-wrapper">mosakutti nogumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mosakutti nogumada"/>
</div>
<div class="lyrico-lyrics-wrapper">rombo rombo nogumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rombo rombo nogumada"/>
</div>
<div class="lyrico-lyrics-wrapper">vambu vandhu serumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambu vandhu serumada"/>
</div>
<div class="lyrico-lyrics-wrapper">ambu patta gayatha aathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambu patta gayatha aathura"/>
</div>
<div class="lyrico-lyrics-wrapper">mooligai ilayada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mooligai ilayada"/>
</div>
<div class="lyrico-lyrics-wrapper">adada da da da...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada da da da..."/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruvi kodanja goyapazham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruvi kodanja goyapazham"/>
</div>
<div class="lyrico-lyrics-wrapper">kondu vanthu tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondu vanthu tharava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumari ponnu vethalaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumari ponnu vethalaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">sunambu naa tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sunambu naa tharava"/>
</div>
<div class="lyrico-lyrics-wrapper">yesnuna yesnu sollu no vunna nonu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yesnuna yesnu sollu no vunna nonu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">yesnuna yesnu sollu no vunna nonu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yesnuna yesnu sollu no vunna nonu sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">kuruvi kodanja goyapazham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuruvi kodanja goyapazham"/>
</div>
<div class="lyrico-lyrics-wrapper">kondu vanthu tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondu vanthu tharava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kothugira kuruvikellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kothugira kuruvikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">goyapazham naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goyapazham naana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meleyum keezheyum parkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meleyum keezheyum parkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">en selaiya thirudum pokiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en selaiya thirudum pokiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">parkuriye uthu uthu parkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parkuriye uthu uthu parkuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">kudika thanniye kettuputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudika thanniye kettuputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">en kudatha izhuka parkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kudatha izhuka parkuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">parkuriye nee izhuka parkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parkuriye nee izhuka parkuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oosiyile kuthi kuthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oosiyile kuthi kuthi"/>
</div>
<div class="lyrico-lyrics-wrapper">kizhikudhu munazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kizhikudhu munazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">kizhichada thachu thachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kizhichada thachu thachu"/>
</div>
<div class="lyrico-lyrics-wrapper">izhukudhu pinazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="izhukudhu pinazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">vittu vittu izhukuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittu vittu izhukuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Undiyala kulukkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undiyala kulukkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">munazhagu nenjula nanjula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munazhagu nenjula nanjula"/>
</div>
<div class="lyrico-lyrics-wrapper">raatinam suthuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raatinam suthuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adada da da da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada da da da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kothugira kuruvikellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kothugira kuruvikellam"/>
</div>
<div class="lyrico-lyrics-wrapper">goyapazham naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goyapazham naana"/>
</div>
<div class="lyrico-lyrics-wrapper">ennudaiya vethalaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennudaiya vethalaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">sunambu thaan vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sunambu thaan vena"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam nee thaliye nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam nee thaliye nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">konjina kothidum mullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjina kothidum mullu"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam nee thaliye nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam nee thaliye nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">konjina kothidum mullu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjina kothidum mullu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuruvi kodanja goyapazham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruvi kodanja goyapazham"/>
</div>
<div class="lyrico-lyrics-wrapper">kondu vanthu tharava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondu vanthu tharava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumari ponnu vethalaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumari ponnu vethalaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">sunambu naa tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sunambu naa tharavaa"/>
</div>
</pre>
