---
title: "uchimalai azhagu song lyrics"
album: "Kadamban"
artist: "Yuvan Shankar Raja"
lyricist: "Yugabharathi"
director: "Ragava"
path: "/albums/kadamban-lyrics"
song: "Uchimalai Azhagu"
image: ../../images/albumart/kadamban.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/N7j_De_blM8"
type: "happy"
singers:
  -	Mukesh Mohamed
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey thanthana naa nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thanthana naa nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thanthana naa nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thanthana naa nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thanthana thanthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thanthana thanthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthaanaa naaa naee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthaanaa naaa naee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thanthana thanthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thanthana thanthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthaana thantha naee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthaana thantha naee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey thanthana naa nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thanthana naa nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thanthana naa nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thanthana naa nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thanthana thanthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thanthana thanthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthaanaa naaa naee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthaanaa naaa naee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thanthana thanthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thanthana thanthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthaana thantha naee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthaana thantha naee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey uchi malai azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey uchi malai azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadharama ulla vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadharama ulla vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha ooru ulagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ooru ulagamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Selichu vazha illa kora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selichu vazha illa kora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachai chedi kodiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai chedi kodiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Palagi pesi allithara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palagi pesi allithara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga kaiya yenthithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga kaiya yenthithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Polaika maattom pogum vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polaika maattom pogum vara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kasu panam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasu panam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sothu sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sothu sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiyidum tholanjithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiyidum tholanjithan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma kakkum intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kakkum intha"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta nambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Valvomae thuninjithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valvomae thuninjithan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma kakkum intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kakkum intha"/>
</div>
<div class="lyrico-lyrics-wrapper">Katta nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katta nambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Valvomae thuninjithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valvomae thuninjithan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey uchi malai azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey uchi malai azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadharama ulla vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadharama ulla vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha ooru ulagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ooru ulagamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Selichu vazha illa kora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selichu vazha illa kora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paambu thelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paambu thelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Palli kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palli kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam kaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli oda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli oda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodi valum intha vaazhva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodi valum intha vaazhva"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kora illamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kora illamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallum poluthum panpaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallum poluthum panpaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvula namma somanthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvula namma somanthava"/>
</div>
<div class="lyrico-lyrics-wrapper">Erakki vidura veliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erakki vidura veliyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Usurathu pogum varaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurathu pogum varaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanguthu kaadu madiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanguthu kaadu madiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ehhhhhhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ehhhhhhh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ehhhhhhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ehhhhhhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oda neeril thaavum meena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda neeril thaavum meena"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna sernthu kombuthena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna sernthu kombuthena"/>
</div>
<div class="lyrico-lyrics-wrapper">Pangu podum enga kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangu podum enga kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Joranathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joranathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi maaramalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maaramalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum thesaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum thesaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neranathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neranathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irupatha ellam kodukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irupatha ellam kodukura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadaa iraivan irukkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadaa iraivan irukkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikira kathula asanjuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikira kathula asanjuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa avanum sirikkiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaa avanum sirikkiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey thanthana naa nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thanthana naa nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thanthana naa nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thanthana naa nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thanthana thanthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thanthana thanthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthaanaa naaa naee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthaanaa naaa naee"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thanthana thanthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thanthana thanthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthaana thantha naee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthaana thantha naee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadana kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadana kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayaga aaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayaga aaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetha vetkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetha vetkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellama pannama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellama pannama"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimbomae soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimbomae soru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mala thaiyoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala thaiyoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbalae ellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbalae ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiserum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiserum paaru"/>
</div>
</pre>
