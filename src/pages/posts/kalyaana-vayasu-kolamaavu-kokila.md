---
title: "kalyaana vayasu song lyrics"
album: "Kolamaavu Kokila"
artist: "Anirudh Ravichander"
lyricist: "Sivakarthikeyan"
director: "Nelson Dilipkumar"
path: "/albums/kolamaavu-kokila-lyrics"
song: "Kalyaana Vayasu"
image: ../../images/albumart/kolamaavu-kokila.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SbodDCS5_Ao"
type: "happy"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Veetukku pona yarayavathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetukku pona yarayavathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam panikka solra appa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam panikka solra appa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththai ponna thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai ponna thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam seiyanum solra amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam seiyanum solra amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Pera kuzhandhainghla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pera kuzhandhainghla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakanum solra paati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakanum solra paati"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha torture laam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha torture laam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanga mudiyaama kelambi vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanga mudiyaama kelambi vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiyae yekamaa paakura ponnunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiyae yekamaa paakura ponnunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana avanga ellar kittaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana avanga ellar kittaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae bathil thaan solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae bathil thaan solluven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattuna antha ponna thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattuna antha ponna thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyanam pannippen nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyanam pannippen nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu varaikkum antha ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu varaikkum antha ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarunnu naa yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarunnu naa yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kittayum sonnadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kittayum sonnadhilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal muraiyaa unga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal muraiyaa unga "/>
</div>
<div class="lyrico-lyrics-wrapper">kitta solla poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kitta solla poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava munnalaa nikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava munnalaa nikkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kannalaa sokkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kannalaa sokkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thannalae sikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thannalae sikkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnalaa sutthuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnalaa sutthuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala saavuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala saavuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava munnalaa nikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava munnalaa nikkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kannalaa sokkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kannalaa sokkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thannalae sikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thannalae sikkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnalaa sutthuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnalaa sutthuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala saavuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala saavuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorula avalo ponnunga irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorula avalo ponnunga irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Luck-u than unakku adichirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luck-u than unakku adichirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ade unnaalaa sikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ade unnaalaa sikkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalaa thikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalaa thikkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalaa vikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalaa vikkuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anushka sharma’ku kohli-ye pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anushka sharma’ku kohli-ye pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku than naanum kidaichirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku than naanum kidaichirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Ade unnai naan daavuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ade unnai naan daavuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala saavuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala saavuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku ippo kalyaana vayasu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku ippo kalyaana vayasu than"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuduchi di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuduchi di"/>
</div>
<div class="lyrico-lyrics-wrapper">Date pannavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Date pannavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa chat pannavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa chat pannavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkuda sernthu vaazhai aasai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkuda sernthu vaazhai aasai than"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuduchi di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuduchi di"/>
</div>
<div class="lyrico-lyrics-wrapper">Meet pannavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meet pannavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa wait pannavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa wait pannavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava munnalaa nikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava munnalaa nikkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kannalaa sokkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kannalaa sokkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thannalae sikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thannalae sikkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnalaa sutthuranUnnaala saavuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnalaa sutthuranUnnaala saavuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava munnalaa nikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava munnalaa nikkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha kannalaa sokkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha kannalaa sokkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thannalae sikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thannalae sikkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naa daavuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naa daavuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalaa saavuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalaa saavuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku mottham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku mottham"/>
</div>
<div class="lyrico-lyrics-wrapper">Paththu peru than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththu peru than"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththai ponnunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai ponnunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthum naan tick-u adichathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthum naan tick-u adichathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga perunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga perunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love-teeyil pootilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-teeyil pootilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Csk naanthanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Csk naanthanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Break vittu vanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Break vittu vanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikka viduvennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikka viduvennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyarae single-ahnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyarae single-ahnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai parthu kettavanlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai parthu kettavanlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkuda mingle-ahna gaandaavaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkuda mingle-ahna gaandaavaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nekku kalyaana vayasu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nekku kalyaana vayasu than"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuduthu di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuduthu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Date pannavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Date pannavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa chat pannavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa chat pannavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkuda sernthu vaazhai aasai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkuda sernthu vaazhai aasai than"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuduchi di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuduchi di"/>
</div>
<div class="lyrico-lyrics-wrapper">Meet pannavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meet pannavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam wait pannavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam wait pannavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku ippo marriage vayasu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku ippo marriage vayasu than"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhuduchi di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhuduchi di"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal paarkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal paarkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Block pannavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Block pannavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkuda sernthu living-u together
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkuda sernthu living-u together"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuda enakku okay di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuda enakku okay di"/>
</div>
<div class="lyrico-lyrics-wrapper">House paarkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="House paarkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal kaachavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal kaachavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohooava munnalaa nikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooava munnalaa nikkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava kannalaa sokkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava kannalaa sokkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thannalae sikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thannalae sikkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnalaa sutthuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnalaa sutthuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala saavuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala saavuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava munnalaa nikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava munnalaa nikkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha kannalaa sokkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha kannalaa sokkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thannalae sikkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thannalae sikkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai naa daavuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai naa daavuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaalaa saavuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaalaa saavuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha ponnae neegha thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha ponnae neegha thaan"/>
</div>
</pre>
