---
title: "annathe sethi song lyrics"
album: "Tughlaq Durbar"
artist: "Govind Vasantha"
lyricist: "Karthik Netha"
director: "Balaji Tharaneetharan"
path: "/albums/tughlaq-durbar-song-lyrics"
song: "Annathe Sethi"
image: ../../images/albumart/tughlaq-durbar.jpg
date: 2021-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9vBPcMFN81Q"
type: "mass"
singers:
  - Arivu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ingayo Irunthu Vanthaa Oruthann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingayo Irunthu Vanthaa Oruthann"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambala Yemaathi Nama Idamtha Pudichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambala Yemaathi Nama Idamtha Pudichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nama Thalaai Mela Yeri Ukara Varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nama Thalaai Mela Yeri Ukara Varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Elame Thannalla Sariyawum Nennachhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elame Thannalla Sariyawum Nennachhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Namba Summave Irunthurukom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Summave Irunthurukom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuru Pudichi Poiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Pudichi Poiyu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambaloda Urimaiggaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambaloda Urimaiggaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoruthan Kitta Ketoo Wangura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoruthan Kitta Ketoo Wangura "/>
</div>
<div class="lyrico-lyrics-wrapper">Pichai Kidaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichai Kidaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Nambaloda Iyalbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Nambaloda Iyalbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambaloda Urimaiya Thadukanum Nenacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambaloda Urimaiya Thadukanum Nenacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukuravan Munjiya Vitutu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukuravan Munjiya Vitutu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Muzhaiya Adichi Odaikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Muzhaiya Adichi Odaikkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appo Thaan Aduthu Varavanukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appo Thaan Aduthu Varavanukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Appdi Yosikanumnu Enname Varathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appdi Yosikanumnu Enname Varathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Epovume Main Switch Thaan Must
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epovume Main Switch Thaan Must"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Waa Oru Vajhi Vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waa Oru Vajhi Vanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooriya Vithaikalai Payiriduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriya Vithaikalai Payiriduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadung Kaatula Meetula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadung Kaatula Meetula"/>
</div>
<div class="lyrico-lyrics-wrapper">Velichattha Machaan Viritthiduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichattha Machaan Viritthiduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Vaeliya Meeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Vaeliya Meeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhambaa Ninnutom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhambaa Ninnutom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mala Mala Malavena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala Mala Malavena"/>
</div>
<div class="lyrico-lyrics-wrapper">Adimaigal Kannai Muzhichittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adimaigal Kannai Muzhichittom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kottaiyil Yaerida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kottaiyil Yaerida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettaigal Yaavum Thodangitom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaigal Yaavum Thodangitom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yalachavan Ozhachavan Yezhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yalachavan Ozhachavan Yezhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Othachavan Mudhugula Turanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othachavan Mudhugula Turanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Kanula Thudikkuthu Poori Poori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Kanula Thudikkuthu Poori Poori"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaiyila Yaanaiya Moori Moori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaiyila Yaanaiya Moori Moori"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevan Thanthathu Theemaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevan Thanthathu Theemaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Arasiyal Moolaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Arasiyal Moolaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kizhi Kizhithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhi Kizhithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Mara Mandaiya Arivile Pilanthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mara Mandaiya Arivile Pilanthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Thondaiya Urimaigal Thiranthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Thondaiya Urimaigal Thiranthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Urappaduvom Marappaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urappaduvom Marappaduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaimurai Ellam Kondaada Purappaduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaimurai Ellam Kondaada Purappaduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saeri Maarip Pogattum Intha Seithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saeri Maarip Pogattum Intha Seithi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilai Aagattum Mela Keezha Thirattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilai Aagattum Mela Keezha Thirattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Boomi Pudhithagattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Boomi Pudhithagattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saeri Maarip Pogattum Intha Seithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saeri Maarip Pogattum Intha Seithi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilai Aagattum, Mela Keezha Thirattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilai Aagattum, Mela Keezha Thirattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Boomi Pudhithagattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Boomi Pudhithagattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pangaali Yaar Sonnathu Kadan Vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangaali Yaar Sonnathu Kadan Vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vaazhnthu Vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vaazhnthu Vida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam Thoongi Vida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam Thoongi Vida"/>
</div>
<div class="lyrico-lyrics-wrapper">Achcham Koochcham Vetkam Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achcham Koochcham Vetkam Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vaazhga Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vaazhga Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar Thanthathu Evan Thanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Thanthathu Evan Thanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu Veetile Kudi Vanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Veetile Kudi Vanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siram Thaazhthiyee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siram Thaazhthiyee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kida Kida Kida Ada Ada Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kida Kida Kida Ada Ada Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemaanthavan Maaranum Maaranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaanthavan Maaranum Maaranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaanthavan Yeranum Yeranum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaanthavan Yeranum Yeranum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunnambula Vaanavil Utthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunnambula Vaanavil Utthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Adi Adi Vannangal Allum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Adi Adi Vannangal Allum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi Sayrinthida Vinmeenum Sitthikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Sayrinthida Vinmeenum Sitthikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karam Sayrinthida Kanneerum Thithikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karam Sayrinthida Kanneerum Thithikum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kupanum Suppanum Yekkanum Yekkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kupanum Suppanum Yekkanum Yekkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennannu Kekkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennannu Kekkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamaayie Krishnaayie 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamaayie Krishnaayie "/>
</div>
<div class="lyrico-lyrics-wrapper">Yengatha Yenthaayie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengatha Yenthaayie"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamay Unnai Vanthu Serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamay Unnai Vanthu Serum"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli Vesham Pottalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli Vesham Pottalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naayiendrum Urumaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayiendrum Urumaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethirthaale Ellamay Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethirthaale Ellamay Maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Somari Bemaani Varthaikal Urumaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Somari Bemaani Varthaikal Urumaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaatha Vanthaachu Seithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaatha Vanthaachu Seithi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Komaali Yemaali Veshangal Thoolaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Komaali Yemaali Veshangal Thoolaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajaali Idamaachi Saeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajaali Idamaachi Saeri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Paathi Naan Paathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paathi Naan Paathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuthane Samaneethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuthane Samaneethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Dey Pangaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Dey Pangaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saeri Maarip Pogattum Intha Seithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saeri Maarip Pogattum Intha Seithi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilai Aagattum Mela Keezha Thirattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilai Aagattum Mela Keezha Thirattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Boomi Pudhithagattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Boomi Pudhithagattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saeri Maarip Pogattum Intha Seithi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saeri Maarip Pogattum Intha Seithi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilai Aagattum Mela Keezha Thirattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilai Aagattum Mela Keezha Thirattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Boomi Pudhithagattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Boomi Pudhithagattum"/>
</div>
</pre>
