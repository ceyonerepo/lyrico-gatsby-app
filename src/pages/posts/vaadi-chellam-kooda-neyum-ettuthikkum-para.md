---
title: "vaadi chellam song lyrics"
album: "Ettuthikkum Para"
artist: "M. S. Sreekanth"
lyricist: "Uma Devi"
director: "Keera"
path: "/albums/ettuthikkum-para-song-lyrics"
song: "Vaadi Chellam Kooda Neyum"
image: ../../images/albumart/ettuthikkum-para.jpg
date: 2020-03-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7w44DzXJSxY"
type: "love"
singers:
  - Ananthu
  - Suganthi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vaadi chellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi chellam "/>
</div>
<div class="lyrico-lyrics-wrapper">kooda neeyum thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda neeyum thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">namma thooramellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma thooramellam"/>
</div>
<div class="lyrico-lyrics-wrapper">thoora poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoora poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kannu rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kannu rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">kathi pola thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi pola thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">ena rendu thunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena rendu thunda"/>
</div>
<div class="lyrico-lyrics-wrapper">vetti poduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetti poduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mullula maadi veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mullula maadi veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaagangal valvathu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaagangal valvathu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">kallula kotta katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallula kotta katti"/>
</div>
<div class="lyrico-lyrics-wrapper">thera pol vaalthal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thera pol vaalthal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">paatham mela patham vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paatham mela patham vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">voodi poi seruvondi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="voodi poi seruvondi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaadi chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi chellam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadi chellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi chellam "/>
</div>
<div class="lyrico-lyrics-wrapper">kooda neeyum thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda neeyum thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">namma thooramellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma thooramellam"/>
</div>
<div class="lyrico-lyrics-wrapper">thoora poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoora poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kannu rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kannu rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">kathi pola thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi pola thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">ena rendu thunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena rendu thunda"/>
</div>
<div class="lyrico-lyrics-wrapper">vetti poduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetti poduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nooru urava paarva padave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru urava paarva padave"/>
</div>
<div class="lyrico-lyrics-wrapper">mounangalum thoolaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounangalum thoolaguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">rayapurame aayapurama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rayapurame aayapurama"/>
</div>
<div class="lyrico-lyrics-wrapper">aagi pogatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagi pogatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaalum valiya kadhal varave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalum valiya kadhal varave"/>
</div>
<div class="lyrico-lyrics-wrapper">muthangalum soraguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthangalum soraguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">oosi thulaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oosi thulaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">yaanai vali aagi pogatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaanai vali aagi pogatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada thoora kadalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada thoora kadalile"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum valiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum valiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">peiyum malaigalum nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peiyum malaigalum nee than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ada naalum karaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada naalum karaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">veesum valaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum valaigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">pechu thunaigalum nee than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pechu thunaigalum nee than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en sontha bandhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sontha bandhame"/>
</div>
<div class="lyrico-lyrics-wrapper">en sothu sugame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sothu sugame"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaala suthu paatu padum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaala suthu paatu padum"/>
</div>
<div class="lyrico-lyrics-wrapper">gaanavum naan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaanavum naan than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaadi chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi chellam"/>
</div>
<div class="lyrico-lyrics-wrapper">hey vaadi chellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey vaadi chellam "/>
</div>
<div class="lyrico-lyrics-wrapper">kooda neeyum thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda neeyum thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">namma thooramellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma thooramellam"/>
</div>
<div class="lyrico-lyrics-wrapper">thoora poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoora poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kannu rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kannu rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">kathi pola thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi pola thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">ena rendu thunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena rendu thunda"/>
</div>
<div class="lyrico-lyrics-wrapper">vetti poduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetti poduthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nooru vagaiyai aasa varave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru vagaiyai aasa varave"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan vizhi thayaguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan vizhi thayaguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">yengi alutha thangum karam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengi alutha thangum karam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal aagatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal aagatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaanam mudiyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam mudiyum "/>
</div>
<div class="lyrico-lyrics-wrapper">thooram varaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram varaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan nizhal nanaguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan nizhal nanaguren"/>
</div>
<div class="lyrico-lyrics-wrapper">neengi nadantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neengi nadantha"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda varum nesam neengatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda varum nesam neengatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un soora paarvaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un soora paarvaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">thanam tharugaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanam tharugaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalum jeevanum naan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalum jeevanum naan than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vizhi moodi thirandhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi moodi thirandhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanin vaigarai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanin vaigarai "/>
</div>
<div class="lyrico-lyrics-wrapper">kaanum vidiyalum naan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanum vidiyalum naan than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna kondadidum en urchagame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kondadidum en urchagame"/>
</div>
<div class="lyrico-lyrics-wrapper">un thagathukum mohathukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thagathukum mohathukum"/>
</div>
<div class="lyrico-lyrics-wrapper">sirabunji naan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirabunji naan than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaadi chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi chellam"/>
</div>
<div class="lyrico-lyrics-wrapper">eh vaadi chellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh vaadi chellam "/>
</div>
<div class="lyrico-lyrics-wrapper">kooda neeyum thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda neeyum thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">namma thooramellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma thooramellam"/>
</div>
<div class="lyrico-lyrics-wrapper">thoora poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoora poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kannu rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kannu rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">kathi pola thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi pola thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">ena rendu thunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena rendu thunda"/>
</div>
<div class="lyrico-lyrics-wrapper">vetti poduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetti poduthe"/>
</div>
</pre>
