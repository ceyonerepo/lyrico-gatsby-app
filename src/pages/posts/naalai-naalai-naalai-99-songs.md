---
title: "naalai naalai naalai song lyrics"
album: "99 Songs"
artist: "A.R. Rahman"
lyricist: "Vivek"
director: "Vishwesh Krishnamoorthy"
path: "/albums/99-songs-lyrics"
song: "Naalai Naalai Naalai"
image: ../../images/albumart/99-songs.jpg
date: 2021-04-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/p1HNgkdvwwo"
type: "Farewell"
singers:
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">pesi sirithom ingu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesi sirithom ingu than"/>
</div>
<div class="lyrico-lyrics-wrapper">nanban thol meethu sarinthom ingu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban thol meethu sarinthom ingu than"/>
</div>
<div class="lyrico-lyrics-wrapper">naalai nam meethu pore thodukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalai nam meethu pore thodukum"/>
</div>
<div class="lyrico-lyrics-wrapper">yenda sogamillai ingu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenda sogamillai ingu than"/>
</div>
<div class="lyrico-lyrics-wrapper">yeee natpin kural matum sangeetham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeee natpin kural matum sangeetham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ninaivai ini nam kooda varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivai ini nam kooda varum"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu pol yengum illai veru idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu pol yengum illai veru idam"/>
</div>
<div class="lyrico-lyrics-wrapper">valkai ethuvendu kaatividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valkai ethuvendu kaatividum"/>
</div>
<div class="lyrico-lyrics-wrapper">oru ragasiya siraginai maati vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru ragasiya siraginai maati vidum"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivindri vara yengum kanavithuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivindri vara yengum kanavithuvo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooo naalai naalai naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooo naalai naalai naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">naalai naalai naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalai naalai naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">neethane yen naalai naalai naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethane yen naalai naalai naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">ethir kaalam namakke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethir kaalam namakke"/>
</div>
<div class="lyrico-lyrics-wrapper">ena sollum antha kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena sollum antha kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">atham pathai intha kalluri saalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atham pathai intha kalluri saalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">intha suvaril yengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha suvaril yengal"/>
</div>
<div class="lyrico-lyrics-wrapper">peyar kalanthirukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peyar kalanthirukum"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kirukalin pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kirukalin pinne"/>
</div>
<div class="lyrico-lyrics-wrapper">nooru kadhai irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru kadhai irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">vendame inge ethume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendame inge ethume"/>
</div>
<div class="lyrico-lyrics-wrapper">nanbargal pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanbargal pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">naangal unmaiyil valthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangal unmaiyil valthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha sila kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha sila kaalame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooo naalai naalai naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooo naalai naalai naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">naalai naalai naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalai naalai naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">neethane yen naalai naalai naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethane yen naalai naalai naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">ethir kaalam namakke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethir kaalam namakke"/>
</div>
<div class="lyrico-lyrics-wrapper">ena sollum antha kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena sollum antha kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">atham pathai intha kalluri saalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atham pathai intha kalluri saalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maram thedi oru nilalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maram thedi oru nilalil"/>
</div>
<div class="lyrico-lyrics-wrapper">pakirnthome unavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakirnthome unavai"/>
</div>
<div class="lyrico-lyrics-wrapper">ariyamal uyirum pakirnthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ariyamal uyirum pakirnthom"/>
</div>
<div class="lyrico-lyrics-wrapper">vidumurai vanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidumurai vanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">manam dinam yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam dinam yengum"/>
</div>
<div class="lyrico-lyrics-wrapper">natpodu yendru servomo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpodu yendru servomo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooo naalai naalai naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooo naalai naalai naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">naalai naalai naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalai naalai naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">neethane yen naalai naalai naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethane yen naalai naalai naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">ethir kaalam namakke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethir kaalam namakke"/>
</div>
<div class="lyrico-lyrics-wrapper">ena sollum antha kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena sollum antha kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">atham pathai intha kalluri saalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atham pathai intha kalluri saalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaadhal kobam paasam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal kobam paasam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">malayai polaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malayai polaa"/>
</div>
<div class="lyrico-lyrics-wrapper">yengal uravu entha uravin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yengal uravu entha uravin"/>
</div>
<div class="lyrico-lyrics-wrapper">urimaikum melaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urimaikum melaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">antha muthal kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha muthal kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">marapomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marapomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nam tholigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam tholigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">marapomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marapomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">intha naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha naal"/>
</div>
<div class="lyrico-lyrics-wrapper">marapomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marapomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">iravattangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravattangal"/>
</div>
<div class="lyrico-lyrics-wrapper">marapomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marapomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">libraryyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="libraryyai"/>
</div>
<div class="lyrico-lyrics-wrapper">marapomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marapomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">canteen ai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="canteen ai"/>
</div>
<div class="lyrico-lyrics-wrapper">marapomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marapomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">cut aditha class galai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cut aditha class galai"/>
</div>
<div class="lyrico-lyrics-wrapper">marapomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marapomaa"/>
</div>
<div class="lyrico-lyrics-wrapper">intha ninaivugalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha ninaivugalai"/>
</div>
<div class="lyrico-lyrics-wrapper">marapomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marapomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooo naalai naalai naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooo naalai naalai naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">naalai naalai naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalai naalai naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">neethane yen naalai naalai naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethane yen naalai naalai naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">ethir kaalam namakke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethir kaalam namakke"/>
</div>
<div class="lyrico-lyrics-wrapper">ena sollum antha kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena sollum antha kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">atham pathai intha kalluri saalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atham pathai intha kalluri saalai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">intha suvaril yengal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha suvaril yengal"/>
</div>
<div class="lyrico-lyrics-wrapper">peyar kalanthirukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peyar kalanthirukum"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kirukalin pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kirukalin pinne"/>
</div>
<div class="lyrico-lyrics-wrapper">nooru kadhai irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru kadhai irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">vendame inge ethume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendame inge ethume"/>
</div>
<div class="lyrico-lyrics-wrapper">nanbargal pothume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanbargal pothume"/>
</div>
<div class="lyrico-lyrics-wrapper">naangal unmaiyil valthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangal unmaiyil valthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">intha sila kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha sila kaalame"/>
</div>
</pre>
