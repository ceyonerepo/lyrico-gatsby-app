---
title: "porale song lyrics"
album: "Aayiram Vilakku"
artist: "Srikanth Deva"
lyricist: "Vairamuthu"
director: "S.P. Hosimin"
path: "/albums/aayiram-vilakku-lyrics"
song: "Porale"
image: ../../images/albumart/aayiram-vilakku.jpg
date: 2011-09-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Z4XiNIx5paE"
type: "love"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poraalae nenjakkillikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraalae nenjakkillikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Killikkittu killikkittu poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Killikkittu killikkittu poraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraalae nanja allikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraalae nanja allikkittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Allikkittu allikkittu vaaraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allikkittu allikkittu vaaraalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttippatta poonai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttippatta poonai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna vandhu suththiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna vandhu suththiriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondappotta kozhippolaKitta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondappotta kozhippolaKitta "/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu koththuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu koththuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnappola enna kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnappola enna kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Arali vedha thevaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arali vedha thevaiyilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm mmm mmm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmm mmm mm mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mmm mmm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mmm mmm mm mm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poraalae poraalae poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraalae poraalae poraalae"/>
</div>
</pre>
