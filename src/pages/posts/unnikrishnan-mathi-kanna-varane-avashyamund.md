---
title: "unnikrishnan song lyrics"
album: "Varane Avashyamund"
artist: "Alphons Joseph"
lyricist: "Santhosh Varma"
director: "Anoop Sathyan"
path: "/albums/varane-avashyamund-lyrics"
song: "Unnikrishnan"
image: ../../images/albumart/varane-avashyamund.jpg
date: 2020-02-07
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/7v7PW2foaHw"
type: "happy"
singers:
  - Alphons Joseph
  - Sherdhin
  - Shelton Pinheiro
  - Thirumali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mathi Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathi Kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullathu Chollan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathu Chollan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madi Enthey Unnikrishna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madi Enthey Unnikrishna"/>
</div>
<div class="lyrico-lyrics-wrapper">Soothrathil Venna Kavarnitt
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soothrathil Venna Kavarnitt"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariya Bhavam Kaattendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariya Bhavam Kaattendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iniyum Nin Vesham Venda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyum Nin Vesham Venda"/>
</div>
<div class="lyrico-lyrics-wrapper">Iniyum Nin Maayam Venda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyum Nin Maayam Venda"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruthe Nee Mannum Chaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruthe Nee Mannum Chaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavathanayi Nilkanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavathanayi Nilkanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Ding Ding Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ding Ding Ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Premam Ulakilkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premam Ulakilkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding Ding Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding Ding Ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakalam Apakadam Illathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakalam Apakadam Illathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding Ding Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding Ding Ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh Ottum Pinnot Pokanalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Ottum Pinnot Pokanalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding Ding Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding Ding Ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharikkum Padichitt Gunichittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharikkum Padichitt Gunichittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Harichittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harichittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Njan Olichu Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Njan Olichu Vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ente Hridayam Oruthavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ente Hridayam Oruthavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaku Vendi Onnu Thurakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaku Vendi Onnu Thurakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thurakkumenkil Athin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thurakkumenkil Athin"/>
</div>
<div class="lyrico-lyrics-wrapper">Orukkam Enkilin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orukkam Enkilin"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranaya Kadhakal Uriyadaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranaya Kadhakal Uriyadaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Njan Ettum Pottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Njan Ettum Pottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiriyatha Oral Alle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiriyatha Oral Alle"/>
</div>
<div class="lyrico-lyrics-wrapper">Enne Vattam Ittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enne Vattam Ittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttichathu Neeyalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttichathu Neeyalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Ente Nenjil Padayani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ente Nenjil Padayani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninte Kannin Udampadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninte Kannin Udampadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eni Onnu Maathram Ariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eni Onnu Maathram Ariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ente Alle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ente Alle"/>
</div>
<div class="lyrico-lyrics-wrapper">Enikulla Ethirali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enikulla Ethirali"/>
</div>
<div class="lyrico-lyrics-wrapper">Njan Thanne Muthalaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Njan Thanne Muthalaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Randum Kathakali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Randum Kathakali"/>
</div>
<div class="lyrico-lyrics-wrapper">Otta Nimishathil Veenupoyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otta Nimishathil Veenupoyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninte Kavil Thadam Ente
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninte Kavil Thadam Ente"/>
</div>
<div class="lyrico-lyrics-wrapper">Chundukale Uriyaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chundukale Uriyaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Nammal Parayunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Nammal Parayunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathuakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathuakale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ding Ding Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding Ding Ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Premam Ulakilkellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premam Ulakilkellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding Ding Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding Ding Ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakalam Apakadam Illathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakalam Apakadam Illathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding Ding Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding Ding Ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh Ottum Pinnot Pokanalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Ottum Pinnot Pokanalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ding Ding Ding
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ding Ding Ding"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharikkum Padichitt Gunichittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharikkum Padichitt Gunichittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Harichittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harichittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eng Aananthanno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eng Aananthanno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veezhathavar Indo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhathavar Indo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennin Kannin Ambu Kondaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennin Kannin Ambu Kondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenettundallo Pandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenettundallo Pandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maamuni Maarividey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamuni Maarividey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathi Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathi Kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullathu Chollan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullathu Chollan"/>
</div>
<div class="lyrico-lyrics-wrapper">Madi Enthey Unnikrishna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madi Enthey Unnikrishna"/>
</div>
<div class="lyrico-lyrics-wrapper">Soothrathil Venna Kavarnitt
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soothrathil Venna Kavarnitt"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariya Bhavam Kaattendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariya Bhavam Kaattendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iniyum Nin Vesham Venda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyum Nin Vesham Venda"/>
</div>
<div class="lyrico-lyrics-wrapper">Iniyum Nin Maayam Venda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniyum Nin Maayam Venda"/>
</div>
<div class="lyrico-lyrics-wrapper">Veruthe Nee Mannum Chaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veruthe Nee Mannum Chaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavathanayi Nilkanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavathanayi Nilkanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paramanathakaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paramanathakaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhura Paalamrutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura Paalamrutham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakshe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakshe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottum Venda Marayathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottum Venda Marayathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerundankil Parayille
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerundankil Parayille"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruthane Pidichittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthane Pidichittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthi Thalli Erakkanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthi Thalli Erakkanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chila Neram Kadinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chila Neram Kadinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathikitta Narakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathikitta Narakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Premam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premam"/>
</div>
</pre>
