---
title: "husn hai suhana song lyrics"
album: "Coolie No1"
artist: "Tanishk Bagchi - Anand Milind"
lyricist: "Sameer Anjaan"
director: "David Dhawan"
path: "/albums/coolie-no1-lyrics"
song: "Husn Hai Suhana"
image: ../../images/albumart/coolie-no1.jpg
date: 2020-12-25
lang: hindi
youtubeLink: "https://www.youtube.com/embed/EsXG4YET4zs"
type: "love"
singers:
  - Chandana Dixit
  - Abhijeet Bhattacharya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ho hoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ho hoy"/>
</div>
<div class="lyrico-lyrics-wrapper">oye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oye"/>
</div>
<div class="lyrico-lyrics-wrapper">su ru ru tu tu tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="su ru ru tu tu tu"/>
</div>
<div class="lyrico-lyrics-wrapper">ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ho"/>
</div>
<div class="lyrico-lyrics-wrapper">tu ru ru tu tu tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu ru ru tu tu tu"/>
</div>
<div class="lyrico-lyrics-wrapper">koy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koy"/>
</div>
<div class="lyrico-lyrics-wrapper">su ru ru tu tu tu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="su ru ru tu tu tu "/>
</div>
<div class="lyrico-lyrics-wrapper">tu ru ru tu tu tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu ru ru tu tu tu"/>
</div>
<div class="lyrico-lyrics-wrapper">husn hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="husn hai"/>
</div>
<div class="lyrico-lyrics-wrapper">ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ishq hai"/>
</div>
<div class="lyrico-lyrics-wrapper">husn hai suhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="husn hai suhana"/>
</div>
<div class="lyrico-lyrics-wrapper">ishq hai deewana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ishq hai deewana"/>
</div>
<div class="lyrico-lyrics-wrapper">roop ka khajana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roop ka khajana"/>
</div>
<div class="lyrico-lyrics-wrapper">aaj hai lutana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaj hai lutana"/>
</div>
<div class="lyrico-lyrics-wrapper">aake deewane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aake deewane"/>
</div>
<div class="lyrico-lyrics-wrapper">mujhe seene se laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mujhe seene se laga"/>
</div>
<div class="lyrico-lyrics-wrapper">na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">goriya chura na mera jiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goriya chura na mera jiya"/>
</div>
<div class="lyrico-lyrics-wrapper">goriya chura na mera jiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goriya chura na mera jiya"/>
</div>
<div class="lyrico-lyrics-wrapper">goriya chura na mera jiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goriya chura na mera jiya"/>
</div>
<div class="lyrico-lyrics-wrapper">goriya chura na mera jiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goriya chura na mera jiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karke ishaara bulaye jawani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karke ishaara bulaye jawani"/>
</div>
<div class="lyrico-lyrics-wrapper">aise lubha na mujhe deewani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aise lubha na mujhe deewani"/>
</div>
<div class="lyrico-lyrics-wrapper">aise lubha na mujhe deewani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aise lubha na mujhe deewani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">karke ishaara bulaye jawani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karke ishaara bulaye jawani"/>
</div>
<div class="lyrico-lyrics-wrapper">aise lubha na mujhe deewani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aise lubha na mujhe deewani"/>
</div>
<div class="lyrico-lyrics-wrapper">aise lubha na mujhe deewani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aise lubha na mujhe deewani"/>
</div>
<div class="lyrico-lyrics-wrapper">aaja aaja na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaja aaja na"/>
</div>
<div class="lyrico-lyrics-wrapper">aaja aaja na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaja aaja na"/>
</div>
<div class="lyrico-lyrics-wrapper">dilbar jaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dilbar jaani"/>
</div>
<div class="lyrico-lyrics-wrapper">jaane jaana oh jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaane jaana oh jaana"/>
</div>
<div class="lyrico-lyrics-wrapper">tu hai khwabon ki rani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tu hai khwabon ki rani"/>
</div>
<div class="lyrico-lyrics-wrapper">apna banaungi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="apna banaungi "/>
</div>
<div class="lyrico-lyrics-wrapper">nigahein toh mila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nigahein toh mila"/>
</div>
<div class="lyrico-lyrics-wrapper">na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">goriya ne pagal mujhe kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goriya ne pagal mujhe kiya"/>
</div>
<div class="lyrico-lyrics-wrapper">goriya ne pagal mujhe kiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goriya ne pagal mujhe kiya"/>
</div>
<div class="lyrico-lyrics-wrapper">goriya chura na mera jiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goriya chura na mera jiya"/>
</div>
<div class="lyrico-lyrics-wrapper">goriya chura na mera jiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goriya chura na mera jiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mauka milan ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mauka milan ka"/>
</div>
<div class="lyrico-lyrics-wrapper">kahan roz aaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kahan roz aaye"/>
</div>
<div class="lyrico-lyrics-wrapper">aise mujhe kyon kareeb laye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aise mujhe kyon kareeb laye"/>
</div>
<div class="lyrico-lyrics-wrapper">aise mujhe kyon kareeb laye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aise mujhe kyon kareeb laye"/>
</div>
<div class="lyrico-lyrics-wrapper">dheere dheere se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheere dheere se"/>
</div>
<div class="lyrico-lyrics-wrapper">haule haule se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haule haule se"/>
</div>
<div class="lyrico-lyrics-wrapper">kyon tadpaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kyon tadpaye"/>
</div>
<div class="lyrico-lyrics-wrapper">aisi raaton mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aisi raaton mein"/>
</div>
<div class="lyrico-lyrics-wrapper">aisi baaton mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aisi baaton mein"/>
</div>
<div class="lyrico-lyrics-wrapper">kyon behkaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kyon behkaye"/>
</div>
<div class="lyrico-lyrics-wrapper">kya hai irada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kya hai irada"/>
</div>
<div class="lyrico-lyrics-wrapper">mere yaar bata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mere yaar bata"/>
</div>
<div class="lyrico-lyrics-wrapper">na na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na na na"/>
</div>
<div class="lyrico-lyrics-wrapper">goriya hai aashiq tera piya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goriya hai aashiq tera piya"/>
</div>
<div class="lyrico-lyrics-wrapper">goriya hai aashiq tera piya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goriya hai aashiq tera piya"/>
</div>
<div class="lyrico-lyrics-wrapper">goriya chura na mera jiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goriya chura na mera jiya"/>
</div>
<div class="lyrico-lyrics-wrapper">goriya chura na mera jiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goriya chura na mera jiya"/>
</div>
<div class="lyrico-lyrics-wrapper">hoy hoy...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hoy hoy..."/>
</div>
</pre>
