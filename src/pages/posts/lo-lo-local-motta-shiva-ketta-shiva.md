---
title: "lo lo local song lyrics"
album: "Motta Shiva Ketta Shiva"
artist: "Amresh Ganesh"
lyricist: "Viveka"
director: "Sai Ramani"
path: "/albums/motta-shiva-ketta-shiva-lyrics"
song: "Lo Lo Local"
image: ../../images/albumart/motta-shiva-ketta-shiva.jpg
date: 2017-03-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/H10btfffuio"
type: "love"
singers:
  -	Raghava Lawrence
  - Suchitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Morattu massu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu massu"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta shiva is
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta shiva is"/>
</div>
<div class="lyrico-lyrics-wrapper">Back in action
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Back in action"/>
</div>
<div class="lyrico-lyrics-wrapper">Its a mass local song
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Its a mass local song"/>
</div>
<div class="lyrico-lyrics-wrapper">Hehe ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hehe ha ha ha ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lo lo lo local
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo lo lo local"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka local
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka local"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maasa rough and touch-a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasa rough and touch-a"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyuvom naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyuvom naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure-a kodutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure-a kodutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Supera konjuvom ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Supera konjuvom ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raavaanaalum konjuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavaanaalum konjuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalaanaalum konjuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalaanaalum konjuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichaalum konjuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichaalum konjuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Morachaalum konjuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morachaalum konjuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lo lo lo local
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo lo lo local"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka local
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka local"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkaththoottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaththoottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhirvoottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirvoottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnugha elaam yenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnugha elaam yenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maassa kitta vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maassa kitta vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kissu onnu thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kissu onnu thaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannathula kissungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathula kissungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhuthula kissungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuthula kissungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppula kissungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppula kissungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Just konjam missungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just konjam missungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta shiva da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta shiva da"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba ketta shiva da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba ketta shiva da"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta shiva da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta shiva da"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba ketta shiva da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba ketta shiva da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta ketta shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta ketta shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta shiva motta shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta shiva motta shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta shiva aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta shiva aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyiyyio
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyiyyio"/>
</div>
<div class="lyrico-lyrics-wrapper">What is this
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What is this"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un motta thala kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un motta thala kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Un morattu pudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un morattu pudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha stylum thimirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha stylum thimirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee eppadi vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee eppadi vandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lo lo lo local
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo lo lo local"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka local
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka local"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un mookkum muzhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mookkum muzhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mullu kuthum meesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mullu kuthum meesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee ninna nadandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ninna nadandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nizhalum enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nizhalum enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasa rough and touch-a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasa rough and touch-a"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyuvom naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyuvom naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure-a kodutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure-a kodutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Supera konjuvom ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Supera konjuvom ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raavaanaalum konjuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavaanaalum konjuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalaanaalum konjuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalaanaalum konjuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichaalum konjuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichaalum konjuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Morachaalum konjuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morachaalum konjuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee looku vidum paarvaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee looku vidum paarvaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee aadura stylum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aadura stylum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna adichu pudichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna adichu pudichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna alli anaichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna alli anaichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lo lo lo local
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo lo lo local"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka local
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka local"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kaandha karupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaandha karupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kalla sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kalla sirippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un muththamum konjalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un muththamum konjalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada moththathuula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada moththathuula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasukkae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasukkae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Maasu daaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasu daaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ingha paaru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingha paaru da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maasa rough and touch-a
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasa rough and touch-a"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyuvom naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyuvom naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure-a kodutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure-a kodutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Supera konjuvom ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Supera konjuvom ponga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raavaanaalum konjuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavaanaalum konjuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalaanaalum konjuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalaanaalum konjuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichaalum konjuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichaalum konjuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Morachaalum konjuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morachaalum konjuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lo lo lo local
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo lo lo local"/>
</div>
<div class="lyrico-lyrics-wrapper">Marana maasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marana maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka local
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka local"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkaththoottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaththoottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhirvoottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirvoottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnugha elaam yenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnugha elaam yenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maassa kitta vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maassa kitta vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kissu onnu thaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kissu onnu thaanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannathula kissungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathula kissungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhuthula kissungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuthula kissungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppula kissungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppula kissungo"/>
</div>
<div class="lyrico-lyrics-wrapper">Just konjam missungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just konjam missungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta shiva da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta shiva da"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba ketta shiva da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba ketta shiva da"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta shiva da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta shiva da"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba ketta shiva da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba ketta shiva da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan ketta shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan ketta shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada motta shiva ketta shiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada motta shiva ketta shiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan bad gud boy daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan bad gud boy daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta aa"/>
</div>
</pre>
