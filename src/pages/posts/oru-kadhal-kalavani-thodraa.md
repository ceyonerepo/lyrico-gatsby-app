---
title: "oru kadhal kalavani song lyrics"
album: "Thodraa"
artist: "RN Uthamaraja - Navin Shander"
lyricist: "SBI Mohan"
director: "Madhuraj"
path: "/albums/thodraa-lyrics"
song: "Oru Kadhal Kalavani"
image: ../../images/albumart/thodraa.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JJnut6m2rh8"
type: "love"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana naa nana naa nana naaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana naa nana naa nana naaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana naa nana naa nana naaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana naa nana naa nana naaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kaadhal kalavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaadhal kalavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura thirudi ponaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura thirudi ponaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi pesum kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi pesum kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa maraichu vechanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa maraichu vechanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan moochu kaathaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan moochu kaathaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaravenum aththaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaravenum aththaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaala porenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaala porenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kooda pinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kooda pinnadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollatha kaadhal vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollatha kaadhal vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollamaa ennai kolluthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollamaa ennai kolluthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollatha vekkam vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollatha vekkam vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullu kullae thaakkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullu kullae thaakkuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kaadhal kalavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaadhal kalavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura thirudi ponaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura thirudi ponaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manakkolam konda pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakkolam konda pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai soodi manamedai vantha kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai soodi manamedai vantha kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maangalyam thanthunaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maangalyam thanthunaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaalanum malar malai soodi naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalanum malar malai soodi naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathiruntha kazhugaai thanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathiruntha kazhugaai thanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thedi paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thedi paarkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandathumae paambai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandathumae paambai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathungi kolgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathungi kolgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai enthan kanavil thanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai enthan kanavil thanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thinam kangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thinam kangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji konji pesi naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji konji pesi naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhandhayagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhandhayagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poothu poothu naan ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothu poothu naan ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Solai aaga maarinenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solai aaga maarinenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanavillai endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kaanavillai endral"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalayaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalayaagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai sernthidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai sernthidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu naan piranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu naan piranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini vazhnthidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini vazhnthidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Angu naan varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angu naan varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae vanthu nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae vanthu nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan venumnu sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan venumnu sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kaadhal kalavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaadhal kalavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura thirudi ponaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura thirudi ponaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi pesum kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi pesum kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa maraichu vechanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa maraichu vechanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan moochu kaathaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan moochu kaathaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaravenum aththaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaravenum aththaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaala porenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaala porenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kooda pinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kooda pinnadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollatha kaadhal vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollatha kaadhal vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollamaa ennai kolluthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollamaa ennai kolluthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollatha vekkam vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollatha vekkam vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullu kullae thaakkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullu kullae thaakkuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thana naa naa nana nana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana naa naa nana nana naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana naa naa nana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana naa naa nana naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana naa naa nana nana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana naa naa nana nana naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana naa naa naana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana naa naa naana naa"/>
</div>
</pre>
