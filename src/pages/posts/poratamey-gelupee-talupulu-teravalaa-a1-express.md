---
title: "poratamey song lyrics"
album: "A1 Express"
artist: "Hiphop Tamizha"
lyricist: "Krishna Chaitanya"
director: "Dennis Jeevan Kanukolanu"
path: "/albums/a1-express-lyrics"
song: "Poratamey - Gelupee Talupulu Teravalaa"
image: ../../images/albumart/a1-express.jpg
date: 2021-03-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/LNtASuQBBjw"
type: "happy"
singers:
  - Sanjith Hegde
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gelupee talupulu teravalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupee talupulu teravalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tegabadudam padaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tegabadudam padaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidividi vellaku pidikilla okate memu kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidividi vellaku pidikilla okate memu kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Poratame ne jevitham gelechenduku nee yuddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poratame ne jevitham gelechenduku nee yuddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee aashayam nee oopirai poratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aashayam nee oopirai poratame"/>
</div>
<div class="lyrico-lyrics-wrapper">Lede bhayam bhadhe gayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lede bhayam bhadhe gayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Porade balamunte jagate talavanchunuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porade balamunte jagate talavanchunuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shikarame edaina nee ettuku yedagali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shikarame edaina nee ettuku yedagali"/>
</div>
<div class="lyrico-lyrics-wrapper">Samkalpame ne balam raninchu ne krushi tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samkalpame ne balam raninchu ne krushi tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chemala dandella saudalu kulchunule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chemala dandella saudalu kulchunule"/>
</div>
<div class="lyrico-lyrics-wrapper">Poradi odinavadevadu porapatu undadule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poradi odinavadevadu porapatu undadule"/>
</div>
<div class="lyrico-lyrics-wrapper">Chevaraku nilabadu ne nijam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chevaraku nilabadu ne nijam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiti varaku nethone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiti varaku nethone"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontare kale otamilo odarpu koradule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontare kale otamilo odarpu koradule"/>
</div>
<div class="lyrico-lyrics-wrapper">Srikrishnudu gethanusaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Srikrishnudu gethanusaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupokate Aadaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupokate Aadaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Chedupai sadinche vijayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chedupai sadinche vijayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne aashayam ne oopirai poratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne aashayam ne oopirai poratame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poratame ne jevitham gelichenduke ne yuddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poratame ne jevitham gelichenduke ne yuddham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne aashayam ne opirai poratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne aashayam ne opirai poratame"/>
</div>
<div class="lyrico-lyrics-wrapper">Poratame ne jevitham gelichenduke ne yuddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poratame ne jevitham gelichenduke ne yuddham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne aashayam ne opirai poratame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne aashayam ne opirai poratame"/>
</div>
<div class="lyrico-lyrics-wrapper">Tado pedo telcheyera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tado pedo telcheyera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kastam nastam lepodhura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kastam nastam lepodhura"/>
</div>
<div class="lyrico-lyrics-wrapper">Navacharithane raseyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navacharithane raseyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Repannadi vadileyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repannadi vadileyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Chevariki nuvvu ne nilabadara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chevariki nuvvu ne nilabadara"/>
</div>
</pre>
