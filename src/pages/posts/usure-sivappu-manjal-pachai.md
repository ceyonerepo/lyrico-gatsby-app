---
title: "usure song lyrics"
album: "Sivappu Manjal Pachai"
artist: "Siddhu Kumar"
lyricist: "Mohan Rajan"
director: "Sasi"
path: "/albums/sivappu-manjal-pachai-lyrics"
song: "Usure"
image: ../../images/albumart/sivappu-manjal-pachai.jpg
date: 2019-09-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/27U4UPhHqjE"
type: "sad"
singers:
  - Sudharshan Ashok
  - Jothi Pushpa
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Usure Vittu Poiyitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usure Vittu Poiyitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa Vetti Veesitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Vetti Veesitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Usure Vittu Poiyitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usure Vittu Poiyitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa Vetti Veesitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Vetti Veesitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thandha Kaayamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thandha Kaayamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thandha Kobamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thandha Kobamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Irukkiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Irukkiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thandha Paasamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thandha Paasamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Konda Nesamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Konda Nesamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Irukkiradhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Irukkiradhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usure Vittu Poiyitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usure Vittu Poiyitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa Vetti Veesitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Vetti Veesitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarodum Pesama Oru Theeva Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarodum Pesama Oru Theeva Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal Ellam Vazhdhenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal Ellam Vazhdhenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Verodu Seradhaa Oru Poova Nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verodu Seradhaa Oru Poova Nambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhndhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhndhene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Thannoda Iragellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Thannoda Iragellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanmunne Vizhundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanmunne Vizhundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaigal Thedaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaigal Thedaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Aanalum Iragukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Aanalum Iragukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaiyin Nyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaiyin Nyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum Pogaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum Pogaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Valigalum Maraiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valigalum Maraiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhudhida Thonala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhudhida Thonala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naano"/>
</div>
<div class="lyrico-lyrics-wrapper">Sedhariya Kannaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedhariya Kannaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Po Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani Maram Naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Maram Naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Po Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakini Yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakini Yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Po Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Evalum Venaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evalum Venaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usure Vittu Poiyitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usure Vittu Poiyitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa Vetti Veesitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Vetti Veesitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thandha Kaayamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thandha Kaayamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thandha Kobamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thandha Kobamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Irukkiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Irukkiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Thandha Paasamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thandha Paasamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Konda Nesamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Konda Nesamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Irukkiradhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Irukkiradhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Usure Ae Manasae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Usure Ae Manasae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohhh Usure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh Usure"/>
</div>
</pre>
