---
title: "vascodagama pogatha oora song lyrics"
album: "Pririyadha Varam Vendum"
artist: "S. A. Rajkumar"
lyricist: "Pa. Vijay"
director: "Kamal"
path: "/albums/pririyadha-varam-vendum-lyrics"
song: "Vascodagama Pogatha Oora"
image: ../../images/albumart/piriyadha-varam-vendum.jpg
date: 2001-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lYBtB_V80kc"
type: "happy"
singers:
  - Devan
  - Sujatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">vascoda gama parkatha oora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vascoda gama parkatha oora"/>
</div>
<div class="lyrico-lyrics-wrapper">julious ceaser pannatha pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="julious ceaser pannatha pora"/>
</div>
<div class="lyrico-lyrics-wrapper">leave layum history ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="leave layum history ah"/>
</div>
<div class="lyrico-lyrics-wrapper">vendam da dhosth
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendam da dhosth"/>
</div>
<div class="lyrico-lyrics-wrapper">kee mu vil vaalathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kee mu vil vaalathey"/>
</div>
<div class="lyrico-lyrics-wrapper">teenage eh waste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teenage eh waste"/>
</div>
<div class="lyrico-lyrics-wrapper">time table valkaikul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="time table valkaikul"/>
</div>
<div class="lyrico-lyrics-wrapper">mattathe julie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mattathe julie"/>
</div>
<div class="lyrico-lyrics-wrapper">paadathil kidaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadathil kidaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">friendship in jolly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="friendship in jolly"/>
</div>
<div class="lyrico-lyrics-wrapper">sirupa sirupa sirupa eehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirupa sirupa sirupa eehe"/>
</div>
<div class="lyrico-lyrics-wrapper">sirupa sirupa sirupa eehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirupa sirupa sirupa eehe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vascoda gama parkatha oora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vascoda gama parkatha oora"/>
</div>
<div class="lyrico-lyrics-wrapper">julious ceaser pannatha pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="julious ceaser pannatha pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bilgates in window vil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bilgates in window vil"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagathai etti paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagathai etti paar"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamaiyai muthumaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamaiyai muthumaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagellam thedi paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagellam thedi paar"/>
</div>
<div class="lyrico-lyrics-wrapper">discovery channelgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="discovery channelgal"/>
</div>
<div class="lyrico-lyrics-wrapper">ftv paadalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ftv paadalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">sylabus ill ullatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sylabus ill ullatha"/>
</div>
<div class="lyrico-lyrics-wrapper">nanbane ne sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanbane ne sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne puthagathil ullukul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne puthagathil ullukul"/>
</div>
<div class="lyrico-lyrics-wrapper">mayil iragai aagathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayil iragai aagathe"/>
</div>
<div class="lyrico-lyrics-wrapper">vanna vanna vaalvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanna vanna vaalvai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kannai moodi vaalathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kannai moodi vaalathe"/>
</div>
<div class="lyrico-lyrics-wrapper">oru aanantha vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru aanantha vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithil upset aagathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithil upset aagathey"/>
</div>
<div class="lyrico-lyrics-wrapper">sirupa sirupa sirupa eehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirupa sirupa sirupa eehe"/>
</div>
<div class="lyrico-lyrics-wrapper">sirupa sirupa sirupa eehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirupa sirupa sirupa eehe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vascoda gama parkatha oora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vascoda gama parkatha oora"/>
</div>
<div class="lyrico-lyrics-wrapper">julious ceaser pannatha pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="julious ceaser pannatha pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaazhkai oru kaanikai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkai oru kaanikai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaana vedi vedikkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaana vedi vedikkai"/>
</div>
<div class="lyrico-lyrics-wrapper">konja naal valvathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konja naal valvathil"/>
</div>
<div class="lyrico-lyrics-wrapper">inbame un laabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbame un laabam"/>
</div>
<div class="lyrico-lyrics-wrapper">nutranum protanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nutranum protanum"/>
</div>
<div class="lyrico-lyrics-wrapper">neengatha nanbargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neengatha nanbargal"/>
</div>
<div class="lyrico-lyrics-wrapper">anaithilum natpai paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anaithilum natpai paar"/>
</div>
<div class="lyrico-lyrics-wrapper">thozhamai athu laabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thozhamai athu laabam"/>
</div>
<div class="lyrico-lyrics-wrapper">antha theepu kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha theepu kannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">athu pole natpai thedi paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu pole natpai thedi paar"/>
</div>
<div class="lyrico-lyrics-wrapper">anbil villai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbil villai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">oru kodi thangam vangiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kodi thangam vangiko"/>
</div>
<div class="lyrico-lyrics-wrapper">ada podatha vaanam undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada podatha vaanam undu"/>
</div>
<div class="lyrico-lyrics-wrapper">siragadika kathuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragadika kathuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sirupa sirupa sirupa eehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirupa sirupa sirupa eehe"/>
</div>
<div class="lyrico-lyrics-wrapper">sirupa sirupa sirupa eehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirupa sirupa sirupa eehe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vascoda gama parkatha oora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vascoda gama parkatha oora"/>
</div>
<div class="lyrico-lyrics-wrapper">julious ceaser pannatha pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="julious ceaser pannatha pora"/>
</div>
<div class="lyrico-lyrics-wrapper">leave layum history ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="leave layum history ah"/>
</div>
<div class="lyrico-lyrics-wrapper">vendam da dhosth
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendam da dhosth"/>
</div>
<div class="lyrico-lyrics-wrapper">kee mu vil vaalathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kee mu vil vaalathey"/>
</div>
<div class="lyrico-lyrics-wrapper">teenage eh waste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="teenage eh waste"/>
</div>
<div class="lyrico-lyrics-wrapper">time table valkaikul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="time table valkaikul"/>
</div>
<div class="lyrico-lyrics-wrapper">mattathe julie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mattathe julie"/>
</div>
<div class="lyrico-lyrics-wrapper">paadathil kidaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadathil kidaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">friendship in jolly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="friendship in jolly"/>
</div>
<div class="lyrico-lyrics-wrapper">sirupa sirupa sirupa eehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirupa sirupa sirupa eehe"/>
</div>
<div class="lyrico-lyrics-wrapper">sirupa sirupa sirupa eehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirupa sirupa sirupa eehe"/>
</div>
</pre>
