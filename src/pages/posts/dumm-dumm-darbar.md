---
title: 'Dumm Dumm song lyrics'
album: 'Darbar'
artist: 'Anirudh Ravichander'
lyricist: 'Vivek'
director: 'A R Murugadoss'
path: '/albums/darbar-song-lyrics'
song: 'Dumm Dumm'
image: ../../images/albumart/darbar.jpg
date: 2020-01-09
lang: tamil
singers:
- Nakash Aziz
youtubeLink: "https://www.youtube.com/embed/PJ8CYFUn0wQ"
type: 'happy'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Ae….ae…ae….ae….hooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ae….ae…ae….ae….hooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae…ae…hae…ae..ae…hae…ae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ae…ae…hae…ae..ae…hae…ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dumm dumm dumm dumm
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumm dumm dumm dumm"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketti melam thalaiyaatum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketti melam thalaiyaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla saththam mattum paravattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nalla saththam mattum paravattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dumm dumm dumm dumm
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumm dumm dumm dumm"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti athiradhu vaettum
<input type="checkbox" class="lyrico-select-lyric-line" value="Katti athiradhu vaettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kalyaanam kala kattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma kalyaanam kala kattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Maapilai ponnodu valiyura velaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Maapilai ponnodu valiyura velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhangal sollaamal maranjirukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sondhangal sollaamal maranjirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Keliyum kindalum nadakkura koothula
<input type="checkbox" class="lyrico-select-lyric-line" value="Keliyum kindalum nadakkura koothula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosham kaathodu niranjirukum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sandhosham kaathodu niranjirukum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dumm dumm dumm dumm
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumm dumm dumm dumm"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketti melam thalaiyaatum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketti melam thalaiyaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla saththam mattum paravattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nalla saththam mattum paravattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaalam vara unakaaga vanthutaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam vara unakaaga vanthutaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaththukanum magaraasiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Paaththukanum magaraasiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaasiyellam unakaaga maathipaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaasiyellam unakaaga maathipaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha vaiyi magaraasana
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazha vaiyi magaraasana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Avan kavalaiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan kavalaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaikka therinjava
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalaikka therinjava"/>
</div>
<div class="lyrico-lyrics-wrapper">Avana jeikkira oh
<input type="checkbox" class="lyrico-select-lyric-line" value="Avana jeikkira oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Avalidam
<input type="checkbox" class="lyrico-select-lyric-line" value="Avalidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thorkka therinthavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thorkka therinthavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam jeikiraan oh ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulagam jeikiraan oh ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dumm dumm heyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumm dumm heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumm dumm heyy
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumm dumm heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dumm dumm dumm dumm
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumm dumm dumm dumm"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketti melam thalaiyaatum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketti melam thalaiyaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla saththam mattum paravattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nalla saththam mattum paravattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dumm dumm dumm dumm
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumm dumm dumm dumm"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti athiradhu vaettum
<input type="checkbox" class="lyrico-select-lyric-line" value="Katti athiradhu vaettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kalyaanam kala kattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma kalyaanam kala kattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maapilai ponnodu valiyura velaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Maapilai ponnodu valiyura velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhangal sollaamal maranjirukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sondhangal sollaamal maranjirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Keliyum kindalum nadakkura koothula
<input type="checkbox" class="lyrico-select-lyric-line" value="Keliyum kindalum nadakkura koothula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosham kaathodu niranjirukum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sandhosham kaathodu niranjirukum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ae…ae…hae..a.e….(4 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Ae…ae…hae..a.e…"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haa..oo hoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa..oo hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaiya izhama mayakkathil
<input type="checkbox" class="lyrico-select-lyric-line" value="Aasaiya izhama mayakkathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththatha kodukkaiyil anba kodukkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Muththatha kodukkaiyil anba kodukkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Narppathu varusam kadanthappodhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Narppathu varusam kadanthappodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya pidipathil kaadhal irukkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiya pidipathil kaadhal irukkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Varusha kanakka
<input type="checkbox" class="lyrico-select-lyric-line" value="Varusha kanakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu sanda pottu
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhagu sanda pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam purinjikka thodangum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjam purinjikka thodangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakkil edutha
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanakkil edutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravil edhunaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Uravil edhunaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha uravula adangum
<input type="checkbox" class="lyrico-select-lyric-line" value="Intha uravula adangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unnoda unnoda uyirukku kaavala
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnoda unnoda uyirukku kaavala"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru nenjamum thudi thudikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Innoru nenjamum thudi thudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann mela vaazhnthida
<input type="checkbox" class="lyrico-select-lyric-line" value="Mann mela vaazhnthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku oru kaaranam
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakku oru kaaranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Undakki kaliyanam parisalikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Undakki kaliyanam parisalikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dumm dumm dei
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumm dumm dei"/>
</div>
<div class="lyrico-lyrics-wrapper">Dumm dumm podu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumm dumm podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dumm dumm dumm dumm
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumm dumm dumm dumm"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketti melam thalaiyaatum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketti melam thalaiyaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla saththam mattum paravattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nalla saththam mattum paravattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Dumm dumm dumm dumm
<input type="checkbox" class="lyrico-select-lyric-line" value="Dumm dumm dumm dumm"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti athiradhu vaettum
<input type="checkbox" class="lyrico-select-lyric-line" value="Katti athiradhu vaettum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kalyaanam kala kattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma kalyaanam kala kattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maapilai ponnodu valiyura velaiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Maapilai ponnodu valiyura velaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondhangal sollaamal maranjirukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sondhangal sollaamal maranjirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Keliyum kindalum nadakkura koothula
<input type="checkbox" class="lyrico-select-lyric-line" value="Keliyum kindalum nadakkura koothula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosham kaathodu niranjirukum
<input type="checkbox" class="lyrico-select-lyric-line" value="Sandhosham kaathodu niranjirukum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ae…ae…hae..a.e….(4 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Ae…ae…hae..a.e…."/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pullaingala purushan pondatiya illaama
<input type="checkbox" class="lyrico-select-lyric-line" value="Pullaingala purushan pondatiya illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbargala iruntheengana vaazhkai nalla irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanbargala iruntheengana vaazhkai nalla irukkum"/>
</div>
</pre>