---
title: "aiyayo aiyayo puduchu iruku song lyrics"
album: "Saamy"
artist: "Harris Jayaraj "
lyricist: "Na. Muthukumar"
director: "Hari"
path: "/albums/saamy-song-lyrics"
song: "Aiyayo Aiyayo Puduchu Iruku"
image: ../../images/albumart/saamy.jpg
date: 2003-05-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RyojGypMCsA"
type: "Love"
singers:
  - Hariharan
  - Komal Ramesh
  - Mahathi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aiyayo Aiyayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayo Aiyayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichiruku Unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichiruku Unaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennavo Ennavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavo Ennavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichiruku Enakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichiruku Enakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunichal Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunichal Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thudipum Romba Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thudipum Romba Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegulithanam Thaan Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegulithanam Thaan Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thirudum Paarvai Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thirudum Paarvai Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhithaai Thirudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhithaai Thirudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudi Enaku Muzhudhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi Enaku Muzhudhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudathaan Theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudathaan Theriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyayo Aiyayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayo Aiyayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichiruku Unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichiruku Unaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennavo Ennavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavo Ennavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichiruku Enakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichiruku Enakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valluvarin Kuralaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valluvarin Kuralaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Vari Irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Vari Irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhatai Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhatai Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Madal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Madal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruge Udhadugal Nadathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruge Udhadugal Nadathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadagam Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadagam Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Madisaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Madisaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Madipukul Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madipukul Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Kudithanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Kudithanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadathida Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadathida Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil Varuvadhanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil Varuvadhanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo Thookathai Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Thookathai Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyayo Aiyayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayo Aiyayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaku Ennai Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaku Ennai Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennavo Ennavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavo Ennavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakum Unnai Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakum Unnai Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuzhaindhaal Bodhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuzhaindhaal Bodhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mara Kilaiyil Oonjal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mara Kilaiyil Oonjal"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti Budhan Aaduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Budhan Aaduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhalile Vizhundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalile Vizhundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattabomman Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattabomman Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Porkalathil Pookal Paripaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porkalathil Pookal Paripaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalayum Maalayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalayum Maalayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padikum Unnai Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padikum Unnai Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Paadangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Paadangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Padika Vaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padika Vaithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaval Kaaranaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaval Kaaranaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundha Unnai Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundha Unnai Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalvanaai Maatri Viten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalvanaai Maatri Viten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adada Adada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Adada"/>
</div>
<div class="lyrico-lyrics-wrapper">Adada Adada Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Adada Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaku Ennai Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaku Ennai Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennavo Ennavo Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavo Ennavo Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakum Unnai Thaan Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakum Unnai Thaan Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thunichal Konjam Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunichal Konjam Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thudipum Romba Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thudipum Romba Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegulithanam Thaan Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegulithanam Thaan Pudichiruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thirudum Paarvai Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thirudum Paarvai Pudichiruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhithaai Thirudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhithaai Thirudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudi Enaku Muzhudhaai Pudichiruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi Enaku Muzhudhaai Pudichiruku"/>
</div>
</pre>
