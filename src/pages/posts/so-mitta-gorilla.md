---
title: "so mitta song lyrics"
album: "Gorilla"
artist: "Sam C.S."
lyricist: "Logan"
director: "Don Sandy"
path: "/albums/gorilla-lyrics"
song: "So Mitta"
image: ../../images/albumart/gorilla.jpg
date: 2019-07-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/15HaRFIfUdo"
type: "happy"
singers:
  - Guna
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili mittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili mittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jungle-u jingle-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jungle-u jingle-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkalu koondhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalu koondhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ippadi thaan aanomda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ippadi thaan aanomda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga mingle-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga mingle-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Jungle-u jingle-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jungle-u jingle-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkalu koondhalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkalu koondhalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada ippadi thaan aanomda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ippadi thaan aanomda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga mingle-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga mingle-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vyasarpaadi munsamikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vyasarpaadi munsamikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Side band-u kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Side band-u kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku vartha kariya pearti tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku vartha kariya pearti tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaiya okkandhu thunnuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaiya okkandhu thunnuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarakkuketha side dish-una
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakkuketha side dish-una"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaramana nethili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaramana nethili"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee voottulathan maattikka pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee voottulathan maattikka pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya nalla koppili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya nalla koppili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili mittaa Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili mittaa Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili mittaa Amaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili mittaa Amaa"/>
</div>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili mittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa podu machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa podu machi"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyyy podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyy podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruthana kedukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruthana kedukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kaasu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kaasu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manushan uyira edukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushan uyira edukuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenappu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenappu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhappa kedukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhappa kedukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bank locker-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bank locker-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Odaikka manasu thudikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odaikka manasu thudikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru cutting oru meeting
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru cutting oru meeting"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru water glass-u ooruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru water glass-u ooruga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam vaanga panam venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam vaanga panam venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Itha yaarukitta keppom baa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itha yaarukitta keppom baa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh night-u sema tight-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh night-u sema tight-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada enga life-u jolly-pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada enga life-u jolly-pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha panam illenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha panam illenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada namma vaazhkai gaali-pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada namma vaazhkai gaali-pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili"/>
</div>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili mittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">So mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichu vudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu vudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenukku mudhuvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenukku mudhuvula"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai vaazha thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai vaazha thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevai romba dhillu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevai romba dhillu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morning-u sevalu Koovuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morning-u sevalu Koovuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manushan manasuthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushan manasuthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Daily monkey aavuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daily monkey aavuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaila kaasu vaaila thosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaila kaasu vaaila thosai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu olagathoda needhi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu olagathoda needhi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Money iruntha namma hero thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Money iruntha namma hero thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Cinema-la aavan boss-u da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cinema-la aavan boss-u da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un theramai athu perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un theramai athu perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru neram varum kaattu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru neram varum kaattu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha pozhappae perum paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha pozhappae perum paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini kaththiya venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini kaththiya venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththiya theettu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththiya theettu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili"/>
</div>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili gili gili gili mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili gili gili gili mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">So mittaa so mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So mittaa so mittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gili gili gili gili gili gili mittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili gili gili gili gili mittaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gili gili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gili gili"/>
</div>
</pre>
