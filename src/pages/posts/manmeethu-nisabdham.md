---
title: "manmeethu song lyrics"
album: "Nisabdham"
artist: "Shawn Jazeel"
lyricist: "Na Muthukumar"
director: "Michael Arun"
path: "/albums/nisabdham-lyrics"
song: "Manmeethu"
image: ../../images/albumart/nisabdham.jpg
date: 2017-03-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tys3yq3EKh4"
type: "melody"
singers:
  -	Shawn Jazeel
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">manmeethu pennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manmeethu pennai "/>
</div>
<div class="lyrico-lyrics-wrapper">vanthaai kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthaai kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">vithai pondraval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithai pondraval"/>
</div>
<div class="lyrico-lyrics-wrapper">nee adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee adi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaneerai pole 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaneerai pole "/>
</div>
<div class="lyrico-lyrics-wrapper">veelnthom munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veelnthom munne"/>
</div>
<div class="lyrico-lyrics-wrapper">ithai maatra vai kaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithai maatra vai kaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">yar unai kaaka varuvaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar unai kaaka varuvaro"/>
</div>
<div class="lyrico-lyrics-wrapper">pul veli neeye veraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pul veli neeye veraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalve maaratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalve maaratho"/>
</div>
<div class="lyrico-lyrics-wrapper">poove vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poove vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manmeethu pennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manmeethu pennai "/>
</div>
<div class="lyrico-lyrics-wrapper">vanthaai kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthaai kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">vithai pondraval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithai pondraval"/>
</div>
<div class="lyrico-lyrics-wrapper">nee adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee adi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaneerai pole 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaneerai pole "/>
</div>
<div class="lyrico-lyrics-wrapper">veelnthom munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veelnthom munne"/>
</div>
<div class="lyrico-lyrics-wrapper">ithai maatra vai kaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithai maatra vai kaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pennai kizhi pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennai kizhi pol"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnathu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnathu yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">siragai murithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragai murithu"/>
</div>
<div class="lyrico-lyrics-wrapper">aan magilavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aan magilavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pennai nathiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennai nathiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">sonnathu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnathu yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">thanathu paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanathu paavam"/>
</div>
<div class="lyrico-lyrics-wrapper">aan karaikavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aan karaikavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oyaa thee pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyaa thee pole"/>
</div>
<div class="lyrico-lyrics-wrapper">nee poradiye velvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee poradiye velvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manmeethu pennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manmeethu pennai "/>
</div>
<div class="lyrico-lyrics-wrapper">vanthaai kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthaai kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">vithai pondraval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithai pondraval"/>
</div>
<div class="lyrico-lyrics-wrapper">nee adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee adi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaneerai pole 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaneerai pole "/>
</div>
<div class="lyrico-lyrics-wrapper">veelnthom munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veelnthom munne"/>
</div>
<div class="lyrico-lyrics-wrapper">ithai maatra vai kaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithai maatra vai kaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thendral pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thendral pole"/>
</div>
<div class="lyrico-lyrics-wrapper">seeratum velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeratum velai"/>
</div>
<div class="lyrico-lyrics-wrapper">puyal pol thuninthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyal pol thuninthe"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vendridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vendridu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thangam pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangam pole"/>
</div>
<div class="lyrico-lyrics-wrapper">paaratum naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaratum naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">nizhal pol thodarnthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhal pol thodarnthe"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vendridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vendridu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaaraai vaan mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaraai vaan mele"/>
</div>
<div class="lyrico-lyrics-wrapper">nee poradiye velvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee poradiye velvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manmeethu pennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manmeethu pennai "/>
</div>
<div class="lyrico-lyrics-wrapper">vanthaai kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthaai kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">vithai pondraval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithai pondraval"/>
</div>
<div class="lyrico-lyrics-wrapper">nee adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee adi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaneerai pole 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaneerai pole "/>
</div>
<div class="lyrico-lyrics-wrapper">veelnthom munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veelnthom munne"/>
</div>
<div class="lyrico-lyrics-wrapper">ithai maatra vai kaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithai maatra vai kaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">yar unai kaaka varuvaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar unai kaaka varuvaro"/>
</div>
<div class="lyrico-lyrics-wrapper">pul veli neeye veraaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pul veli neeye veraaro"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalve maaratho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalve maaratho"/>
</div>
<div class="lyrico-lyrics-wrapper">poove vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poove vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manmeethu pennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manmeethu pennai "/>
</div>
<div class="lyrico-lyrics-wrapper">vanthaai kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthaai kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">vithai pondraval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithai pondraval"/>
</div>
<div class="lyrico-lyrics-wrapper">nee adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee adi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaneerai pole 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaneerai pole "/>
</div>
<div class="lyrico-lyrics-wrapper">veelnthom munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veelnthom munne"/>
</div>
<div class="lyrico-lyrics-wrapper">ithai maatra vai kaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithai maatra vai kaladi"/>
</div>
</pre>
