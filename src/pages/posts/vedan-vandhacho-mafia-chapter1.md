---
title: "vedan vandhacho song lyrics"
album: "Mafia - Chapter1"
artist: "Jakes Bejoy"
lyricist: "	Vivek"
director: "Karthick Naren"
path: "/albums/mafia-chapter1-lyrics"
song: "Vedan Vandhacho"
image: ../../images/albumart/mafia-chapter1.jpg
date: 2020-02-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-S0n_zyFcHc"
type: "Mass Intro"
singers:
  - Vijay Prakash
  - Sathya Prakash
  - ST TFC
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
(<div class="lyrico-lyrics-wrapper">arun vijay's dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arun vijay's dialogue)"/>
</div>
<div class="lyrico-lyrics-wrapper">oru kaatula oru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kaatula oru "/>
</div>
<div class="lyrico-lyrics-wrapper">singam irunthucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singam irunthucha"/>
</div>
<div class="lyrico-lyrics-wrapper">matha vilangukulam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matha vilangukulam"/>
</div>
<div class="lyrico-lyrics-wrapper">athan raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athan raja"/>
</div>
<div class="lyrico-lyrics-wrapper">meeri atha seenduna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meeri atha seenduna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puyal karuvil poothavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyal karuvil poothavan"/>
</div>
<div class="lyrico-lyrics-wrapper">puli uruvam etravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puli uruvam etravan"/>
</div>
<div class="lyrico-lyrics-wrapper">kannukulla adangatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukulla adangatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul pondravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul pondravan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pasi urakkam thotravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasi urakkam thotravan"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">nadai athirum patravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadai athirum patravan"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">enge nee olinthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge nee olinthal"/>
</div>
<div class="lyrico-lyrics-wrapper">okay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okay"/>
</div>
<div class="lyrico-lyrics-wrapper">ethiril nirpavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethiril nirpavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">in tha belly of the beast
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="in tha belly of the beast"/>
</div>
<div class="lyrico-lyrics-wrapper">as if feast
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="as if feast"/>
</div>
<div class="lyrico-lyrics-wrapper">you better seize or
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you better seize or"/>
</div>
<div class="lyrico-lyrics-wrapper">be deceased
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="be deceased"/>
</div>
<div class="lyrico-lyrics-wrapper">not a tease yes please
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="not a tease yes please"/>
</div>
<div class="lyrico-lyrics-wrapper">get your demons on the knees
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="get your demons on the knees"/>
</div>
<div class="lyrico-lyrics-wrapper">not another way
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="not another way"/>
</div>
<div class="lyrico-lyrics-wrapper">not another day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="not another day"/>
</div>
<div class="lyrico-lyrics-wrapper">it's judgement day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="it's judgement day"/>
</div>
<div class="lyrico-lyrics-wrapper">you gotta pay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you gotta pay"/>
</div>
<div class="lyrico-lyrics-wrapper">he came to slay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="he came to slay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ivanai theriyamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivanai theriyamal"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu vilaiyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu vilaiyadu"/>
</div>
<div class="lyrico-lyrics-wrapper">therinthu kondal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therinthu kondal"/>
</div>
<div class="lyrico-lyrics-wrapper">thirumbi nee odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumbi nee odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pagaivar kaatil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagaivar kaatil"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniyagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniyagave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vedan vandhacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedan vandhacho"/>
</div>
<div class="lyrico-lyrics-wrapper">vedan vandhacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedan vandhacho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
(<div class="lyrico-lyrics-wrapper">arun vijay's dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arun vijay's dialogue)"/>
</div>
<div class="lyrico-lyrics-wrapper">bodhai palakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bodhai palakam"/>
</div>
<div class="lyrico-lyrics-wrapper">adimaiya irukkuravanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adimaiya irukkuravanga"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam genders and age 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam genders and age "/>
</div>
<div class="lyrico-lyrics-wrapper">groups'liyum irukanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="groups'liyum irukanga"/>
</div>
<div class="lyrico-lyrics-wrapper">ivanga ellarukum oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivanga ellarukum oru"/>
</div>
<div class="lyrico-lyrics-wrapper">reason'um iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="reason'um iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
(<div class="lyrico-lyrics-wrapper">accused dialogue)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="accused dialogue)"/>
</div>
<div class="lyrico-lyrics-wrapper">wife erantha appothale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="wife erantha appothale"/>
</div>
<div class="lyrico-lyrics-wrapper">use panna aaramichen sir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="use panna aaramichen sir"/>
</div>
<div class="lyrico-lyrics-wrapper">i know it but it's a mistake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i know it but it's a mistake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu illame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu illame"/>
</div>
<div class="lyrico-lyrics-wrapper">studies'ah ennale cope up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="studies'ah ennale cope up"/>
</div>
<div class="lyrico-lyrics-wrapper">panna mudila sir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panna mudila sir"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennoda boyfriend 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoda boyfriend "/>
</div>
<div class="lyrico-lyrics-wrapper">palaki vittan sir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palaki vittan sir"/>
</div>
<div class="lyrico-lyrics-wrapper">ippo ennala vida mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippo ennala vida mudiyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sir reason'lam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sir reason'lam "/>
</div>
<div class="lyrico-lyrics-wrapper">onnum illa sir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum illa sir"/>
</div>
<div class="lyrico-lyrics-wrapper">adicha jolly'ah iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adicha jolly'ah iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">otrai vizhi inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otrai vizhi inga"/>
</div>
<div class="lyrico-lyrics-wrapper">kadami atruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadami atruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">matr vizhi unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matr vizhi unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagai kootuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagai kootuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">un koopaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un koopaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">nenjam neeradathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjam neeradathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalai maalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalai maalai"/>
</div>
<div class="lyrico-lyrics-wrapper">iravellam neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravellam neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">melai kooda unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="melai kooda unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">pola naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">naam kadhalil pillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam kadhalil pillai"/>
</div>
<div class="lyrico-lyrics-wrapper">pole aanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pole aanathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pathingidum yaathilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathingidum yaathilum"/>
</div>
<div class="lyrico-lyrics-wrapper">poi valarume en nizhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poi valarume en nizhal"/>
</div>
<div class="lyrico-lyrics-wrapper">irulin koodathil thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulin koodathil thee"/>
</div>
<div class="lyrico-lyrics-wrapper">theerkume en nizhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerkume en nizhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ivanai theriyamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivanai theriyamal"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthu vilaiyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthu vilaiyadu"/>
</div>
<div class="lyrico-lyrics-wrapper">therinthu kondal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therinthu kondal"/>
</div>
<div class="lyrico-lyrics-wrapper">thirumbi nee odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumbi nee odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irangi suththam seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irangi suththam seiven"/>
</div>
<div class="lyrico-lyrics-wrapper">idhuvum en veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhuvum en veedu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vedan vandhacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedan vandhacho"/>
</div>
<div class="lyrico-lyrics-wrapper">ithai veetai theerndhacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithai veetai theerndhacho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">get ready for the thunder
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="get ready for the thunder"/>
</div>
<div class="lyrico-lyrics-wrapper">now get rady for the pain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="now get rady for the pain"/>
</div>
<div class="lyrico-lyrics-wrapper">get ready for the showers
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="get ready for the showers"/>
</div>
<div class="lyrico-lyrics-wrapper">all the bullets not the rain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="all the bullets not the rain"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ibbayan atich katu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ibbayan atich katu"/>
</div>
<div class="lyrico-lyrics-wrapper">ibboyya valik potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ibboyya valik potu"/>
</div>
<div class="lyrico-lyrics-wrapper">veeti pechu neral illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeti pechu neral illai"/>
</div>
<div class="lyrico-lyrics-wrapper">vetti saayika neram aethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetti saayika neram aethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yes i'm a monster
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yes i'm a monster"/>
</div>
<div class="lyrico-lyrics-wrapper">a boogeyman a fowler
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="a boogeyman a fowler"/>
</div>
<div class="lyrico-lyrics-wrapper">run and  hide now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="run and  hide now"/>
</div>
<div class="lyrico-lyrics-wrapper">hit you like a baller
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hit you like a baller"/>
</div>
<div class="lyrico-lyrics-wrapper">got my bow 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="got my bow "/>
</div>
<div class="lyrico-lyrics-wrapper">got my arrows
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="got my arrows"/>
</div>
<div class="lyrico-lyrics-wrapper">i'm a hunter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i'm a hunter"/>
</div>
<div class="lyrico-lyrics-wrapper">i'm an old-school OG
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i'm an old-school OG"/>
</div>
<div class="lyrico-lyrics-wrapper">with a brand new swagger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="with a brand new swagger"/>
</div>
</pre>
