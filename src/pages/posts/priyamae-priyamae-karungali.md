---
title: "priyamae priyamae song lyrics"
album: "Karungali"
artist: "Srikanth Deva"
lyricist: "Ma. Pugazhenthi"
director: "Kalanjiyam"
path: "/albums/karungali-lyrics"
song: "Priyamae Priyamae"
image: ../../images/albumart/karungali.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1FREKe5dePM"
type: "love"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Priyamey priyamey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyamey priyamey "/>
</div>
<div class="lyrico-lyrics-wrapper">unai nenjiley sumandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai nenjiley sumandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyil nee serndhadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyil nee serndhadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatril megamaai midhandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatril megamaai midhandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paadhi nee paadhi naan aagiroam paaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadhi nee paadhi naan aagiroam paaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">inbam vandhaalum thunbam thandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam vandhaalum thunbam thandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">unai neengamaattenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai neengamaattenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkey enakkaai pirandhaai neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkey enakkaai pirandhaai neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan uyirai uyirai tharuven naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan uyirai uyirai tharuven naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkey enakkaai pirandhaai neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkey enakkaai pirandhaai neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan uyirai uyirai tharuven naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan uyirai uyirai tharuven naanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Priyamey priyamey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyamey priyamey "/>
</div>
<div class="lyrico-lyrics-wrapper">unai nenjiley sumandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai nenjiley sumandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyil nee serndhadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyil nee serndhadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatril megamaai midhandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatril megamaai midhandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi irandum unadhazhagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi irandum unadhazhagai"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhungidavey virumbudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhungidavey virumbudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">urangaiyilum un nivaivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urangaiyilum un nivaivey"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir muzhudhum ulavudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir muzhudhum ulavudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">inbam ingey ennenna thedippidippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam ingey ennenna thedippidippen"/>
</div>
<div class="lyrico-lyrics-wrapper">poadhum poadhum endraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poadhum poadhum endraalum"/>
</div>
<div class="lyrico-lyrics-wrapper">aallikkoduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aallikkoduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">un sinna chinna seigai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un sinna chinna seigai "/>
</div>
<div class="lyrico-lyrics-wrapper">pesum kavidhai rasippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesum kavidhai rasippen"/>
</div>
<div class="lyrico-lyrics-wrapper">en kannukkulley unnai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kannukkulley unnai "/>
</div>
<div class="lyrico-lyrics-wrapper">vaiththu sugammaai thavippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaiththu sugammaai thavippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Priyamey priyamey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyamey priyamey "/>
</div>
<div class="lyrico-lyrics-wrapper">unai nenjiley sumandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai nenjiley sumandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyil nee serndhadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyil nee serndhadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatril megamaai midhandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatril megamaai midhandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhuyirin mugavari nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirin mugavari nee"/>
</div>
<div class="lyrico-lyrics-wrapper">alavu illa sugamadi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alavu illa sugamadi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">piraviyellaam thunai varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piraviyellaam thunai varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">virumbugiren thavamadhu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virumbugiren thavamadhu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjil vaazhum unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjil vaazhum unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">naalum varamaai madhippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalum varamaai madhippen"/>
</div>
<div class="lyrico-lyrics-wrapper">en aayul yaavum kadavul poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aayul yaavum kadavul poala"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai thudhippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thudhippen"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaalgal poagum paadhai engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaalgal poagum paadhai engum"/>
</div>
<div class="lyrico-lyrics-wrapper">nizhalaai kidappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nizhalaai kidappen"/>
</div>
<div class="lyrico-lyrics-wrapper">un thoalil saaindhu thoongumboadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thoalil saaindhu thoongumboadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">sugamaai irappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugamaai irappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Priyamey priyamey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyamey priyamey "/>
</div>
<div class="lyrico-lyrics-wrapper">unai nenjiley sumandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai nenjiley sumandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyil nee serndhadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyil nee serndhadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatril megamaai midhandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatril megamaai midhandhen"/>
</div>
</pre>
