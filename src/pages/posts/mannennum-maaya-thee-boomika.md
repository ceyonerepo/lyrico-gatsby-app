---
title: "mannennum maaya thee song lyrics"
album: "Boomika"
artist: "Chandrasekhar"
lyricist: "Kadhirmozhi Sudha"
director: "Rathindran R Prasad"
path: "/albums/boomika-song-lyrics"
song: "Mannennum Maaya Thee"
image: ../../images/albumart/boomika.jpg
date: 2021-08-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/W8U9V2BblVg"
type: "melody"
singers:
  - Andrea Jeremiah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan mannennum maaya thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan mannennum maaya thee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam seiyaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam seiyaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaal theenda en dhegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaal theenda en dhegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam paarkathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam paarkathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theera valigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera valigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithamum thandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithamum thandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Imai naan asaitthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imai naan asaitthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelumo vaazhvea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelumo vaazhvea"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Visaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Disaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thasaiyum Naan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thasaiyum Naan thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oliyil vazhiyum thoogalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyil vazhiyum thoogalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal baagam thaanea nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal baagam thaanea nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagarum nathiyai nagaram seithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarum nathiyai nagaram seithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudarum thanalum kadalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudarum thanalum kadalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uru maari pogum daar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uru maari pogum daar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verum mann thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum mann thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan endru nee enna vaendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan endru nee enna vaendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un peraasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un peraasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai kondu nee veezha venda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai kondu nee veezha venda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anuvai pilanthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuvai pilanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai nee aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai nee aala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodi naan silirthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodi naan silirthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Engu nee vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engu nee vaazha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Visaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Disaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thasaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thasaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oliyil vazhiyum thoogalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oliyil vazhiyum thoogalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaal baagam thaanea nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal baagam thaanea nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagarum nathiyai nagaram seithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagarum nathiyai nagaram seithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudarum thanalum kadalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudarum thanalum kadalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uru maari pogum daar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uru maari pogum daar"/>
</div>
</pre>
