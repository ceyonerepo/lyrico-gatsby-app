---
title: "otha paarvaiyil song lyrics"
album: "Kadamban"
artist: "Yuvan Shankar Raja"
lyricist: "Yugabharathi"
director: "Ragava"
path: "/albums/kadamban-lyrics"
song: "Otha Paarvaiyil"
image: ../../images/albumart/kadamban.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5XUiYNFlpEM"
type: "love"
singers:
  -	Yuvan Shankar Raja
  - Srimathumitha  
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey oththa paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey oththa paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna yendi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna yendi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Padam potu kattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padam potu kattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththu saaviyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththu saaviyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja thookki en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja thookki en"/>
</div>
<div class="lyrico-lyrics-wrapper">Idupporam maattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idupporam maattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moththa aasaiyum oru sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththa aasaiyum oru sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn kekkuthae parimaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn kekkuthae parimaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththi maaruthae pozhuthellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththi maaruthae pozhuthellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaiyera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna neeyum en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna neeyum en"/>
</div>
<div class="lyrico-lyrics-wrapper">Padam potu kattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padam potu kattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththu saaviyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththu saaviyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thookki en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thookki en"/>
</div>
<div class="lyrico-lyrics-wrapper">Idupporam maattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idupporam maattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorae paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Porenn unna thookka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porenn unna thookka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaththum poovum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththum poovum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil paakkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil paakkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam pooraa unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam pooraa unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal seiven kekaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal seiven kekaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho yemaathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho yemaathura"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenja paazhaakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenja paazhaakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagaatha pecha pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaatha pecha pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala soodethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala soodethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Padam potu kattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padam potu kattura"/>
</div>
<div class="lyrico-lyrics-wrapper">Koththu saaviyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththu saaviyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaviyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaviyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idupporam maatturaa ahh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idupporam maatturaa ahh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasal  thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasal  thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaren una vendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaren una vendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen vaazhka pooraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen vaazhka pooraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna serndhaa podhaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna serndhaa podhaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvae neeyaa maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvae neeyaa maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi jenmam theeraathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi jenmam theeraathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaiyyo un pechula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyyo un pechula"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagaayam en kaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam en kaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeththaatha enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeththaatha enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum aasa pallaakkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum aasa pallaakkula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey oththa paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey oththa paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna yendi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna yendi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Padam potu kattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padam potu kattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koththu saaviyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththu saaviyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja thookki en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja thookki en"/>
</div>
<div class="lyrico-lyrics-wrapper">Idupporam maattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idupporam maattura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moththa aasaiyum oru sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththa aasaiyum oru sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn kekkuthae porai yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn kekkuthae porai yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththi maaruthae pozhuthellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththi maaruthae pozhuthellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veriyera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veriyera"/>
</div>
</pre>
