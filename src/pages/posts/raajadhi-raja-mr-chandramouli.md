---
title: "raajadhi raja song lyrics"
album: "Mr Chandramouli"
artist: "Sam CS"
lyricist: "Vivek"
director: "Thiru"
path: "/albums/mr-chandramouli-lyrics"
song: "Raajadhi Raja"
image: ../../images/albumart/mr-chandramouli.jpg
date: 2018-07-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CXvFcTb4rRE"
type: "happy"
singers:
  - Mukesh Mohamed
  - Ranjith Govind
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aa ee ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa ee ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru konjam paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru konjam paaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellorum kaadhalikum sooran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellorum kaadhalikum sooran"/>
</div>
<div class="lyrico-lyrics-wrapper">Daddy-ke competition paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daddy-ke competition paaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthachu pearl-uh petha peran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthachu pearl-uh petha peran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pixel maari pooyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pixel maari pooyachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pistha combo aayachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pistha combo aayachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Style-um grace-um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Style-um grace-um"/>
</div>
<div class="lyrico-lyrics-wrapper">Charm-um form-um yeripochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charm-um form-um yeripochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar akkapoorachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar akkapoorachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raajadhi raaja raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajadhi raaja raaja"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookathae engum koojaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookathae engum koojaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Podunga da high stage ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podunga da high stage ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Return-nu ma new age ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Return-nu ma new age ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Navarasam irukanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navarasam irukanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayaganu kondadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayaganu kondadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Panivizhum malarvanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panivizhum malarvanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kizhakku vaasal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhakku vaasal"/>
</div>
<div class="lyrico-lyrics-wrapper">Therandhu vechu poo podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therandhu vechu poo podum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yelae keechan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelae keechan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paada vechaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paada vechaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppothum avan heart-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppothum avan heart-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kodi innalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kodi innalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaalum pohnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaalum pohnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bright aavae nippen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bright aavae nippen da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan agni natchathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan agni natchathiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raajadhi raaja raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajadhi raaja raaja"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookathae engum koojaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookathae engum koojaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Podunga da high stage ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podunga da high stage ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Return-nu ma new age ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Return-nu ma new age ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Worry ellam othukumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Worry ellam othukumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thadukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thadukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha power um illae maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha power um illae maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Erangi nee kalukkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangi nee kalukkumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kadalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kadalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigal oivathillai maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigal oivathillai maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life-eh inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh inga"/>
</div>
<div class="lyrico-lyrics-wrapper">One time-uh thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One time-uh thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaana friend-oda kondadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaana friend-oda kondadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En father superman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En father superman"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan than da lucky man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan than da lucky man"/>
</div>
<div class="lyrico-lyrics-wrapper">Close-aavae ninnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Close-aavae ninnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kooda avaru fan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kooda avaru fan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raajadhi raaja raaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajadhi raaja raaja"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookathae engum koojaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookathae engum koojaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Podunga da high stage ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podunga da high stage ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Return-nu ma new age ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Return-nu ma new age ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podu ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey heyyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey heyyyy"/>
</div>
</pre>
