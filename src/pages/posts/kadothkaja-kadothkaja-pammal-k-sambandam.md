---
title: "kadothkaja kadothkaja song lyrics"
album: "Pammal K Sambandam"
artist: "Deva"
lyricist: "Vaali"
director: "Moulee"
path: "/albums/pammal-k-sambandam-lyrics"
song: "Kadothkaja Kadothkaja"
image: ../../images/albumart/pammal-k-sambandam.jpg
date: 2002-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IecEwiMOC3A"
type: "happy"
singers:
  - Srinivas
  - Mahalakshmi Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Siritha Mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siritha Mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Siritha Mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Siritha Mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theiyaadha Muzhu Nilave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theiyaadha Muzhu Nilave"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyaadha Alai Kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaadha Alai Kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirithamugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithamugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu Siritha Mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Siritha Mugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadothkaja Kadothkaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadothkaja Kadothkaja"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadothkaja Kalaadharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadothkaja Kalaadharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivaagame Vidaadhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivaagame Vidaadhudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indrodu Hanumaar Bhakthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrodu Hanumaar Bhakthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Brahmachariyam Mudigiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmachariyam Mudigiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbodu Kaithalam Pattra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbodu Kaithalam Pattra"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanni Nilaadhaan Varugiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni Nilaadhaan Varugiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayirathil Oruvan Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayirathil Oruvan Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagan Endru Arignan Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagan Endru Arignan Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Vaitha Ila Maan Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Vaitha Ila Maan Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaithaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaithaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanavarai Vizhiyaal Pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavarai Vizhiyaal Pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam Ennum Mozhiyaal Pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam Ennum Mozhiyaal Pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaga Motham Anbe Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaga Motham Anbe Unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Dhaan Naayagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Dhaan Naayagane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadothkajan Kalaadharan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadothkajan Kalaadharan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanne Undhan Kaiyil Saran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Undhan Kaiyil Saran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tik Tik Endru Satham Unnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tik Tik Endru Satham Unnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennulle Ketkindradhe Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennulle Ketkindradhe Kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Buckbuck Endru Anji Unnaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buckbuck Endru Anji Unnaale"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ullam Vegindradhe Kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ullam Vegindradhe Kannaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ore Naalil Kaadhal Noi Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore Naalil Kaadhal Noi Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal Vandhadhe Thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal Vandhadhe Thozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ore Oosi Pottaal Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore Oosi Pottaal Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vaazhume Thozhane Thozhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vaazhume Thozhane Thozhane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalaiyida Oru Naal Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaiyida Oru Naal Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mananaal Paarthu Thirunaal Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mananaal Paarthu Thirunaal Paarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selai Thodum Maru Naal Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selai Thodum Maru Naal Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruppenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruppenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai Nenjam Ketkaa Dhammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Nenjam Ketkaa Dhammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedigaaram Dhaan Paarkaa Dhammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedigaaram Dhaan Paarkaa Dhammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatru Enna Neram Paarthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatru Enna Neram Paarthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhiyil Kulikiradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyil Kulikiradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadothkaja Kadothkaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadothkaja Kadothkaja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaiyal Pottu Ennai Thaithaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaiyal Pottu Ennai Thaithaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaiyal Ini Unnodudhaan Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaiyal Ini Unnodudhaan Kanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyai Pottu Ennai Kaiyaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyai Pottu Ennai Kaiyaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Seru Ennodudhaan Kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Seru Ennodudhaan Kannaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi Meindhu Oyumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Meindhu Oyumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral Meyume Thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Meyume Thozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi Poovum Koocham Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Poovum Koocham Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madi Saayume Thozhiye Thozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madi Saayume Thozhiye Thozhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poongarathu Valaiyaadaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poongarathu Valaiyaadaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaiyaadathaan Vilaiyaadaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyaadathaan Vilaiyaadaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai Sevappu Unnaal Konjam Velukaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai Sevappu Unnaal Konjam Velukaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netru Varai Adaiththe Vaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Varai Adaiththe Vaitha"/>
</div>
<div class="lyrico-lyrics-wrapper">Madaiyai Needhaan Odaithaai Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madaiyai Needhaan Odaithaai Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavi Manam Paayai Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavi Manam Paayai Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadaai Paduthiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadaai Paduthiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadothkaja Kalaadharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadothkaja Kalaadharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivaagame Vidaadhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivaagame Vidaadhudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indrodu Hanumaar Bhakthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrodu Hanumaar Bhakthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Brahmachariyam Mudigiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmachariyam Mudigiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbodu Kaithalam Pattra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbodu Kaithalam Pattra"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanni Nilaadhaan Varugiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni Nilaadhaan Varugiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayirathil Oruvan Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayirathil Oruvan Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagan Endru Arignan Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagan Endru Arignan Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Vaitha Ila Maan Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Vaitha Ila Maan Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaithaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaithaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanavarai Vizhiyaal Pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavarai Vizhiyaal Pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam Ennum Mozhiyaal Pesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam Ennum Mozhiyaal Pesu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaga Motham Anbe Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaga Motham Anbe Unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Dhaan Naayagane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Dhaan Naayagane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadothkaja Kadothkaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadothkaja Kadothkaja"/>
</div>
</pre>
