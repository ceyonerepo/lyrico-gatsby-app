---
title: "thalaimurai song lyrics"
album: "Irandam Ulagaporin Kadaisi Gundu"
artist: "Tenma"
lyricist: "Arivu"
director: "Athiyan Athirai"
path: "/albums/irandam-ulagaporin-kadaisi-gundu-lyrics"
song: "Thalaimurai"
image: ../../images/albumart/irandam-ulagaporin-kadaisi-gundu.jpg
date: 2019-12-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/M21OHiSDXS0"
type: "sad"
singers:
  - Shakthisree Gopalan
  - Chinnaponnu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thalaimurai Thalai Thookumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaimurai Thalai Thookumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhai Theerkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhai Theerkumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Punidha Bhoomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Punidha Bhoomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Por Indri Naam Ondraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Por Indri Naam Ondraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Serum Naal Vaaradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum Naal Vaaradha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viduthalai Vazhi Kaatumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduthalai Vazhi Kaatumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilakketrumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilakketrumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Manidha Sathiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Manidha Sathiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mel Ennum Keezh Ennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mel Ennum Keezh Ennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolangal Maradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolangal Maradha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thai Nilam Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thai Nilam Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhvugal Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhvugal Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanudam Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanudam Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivinaiyin Peru Vali Thaangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivinaiyin Peru Vali Thaangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaimurai Thalai Thookumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaimurai Thalai Thookumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhai Theerkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhai Theerkumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Punidha Bhoomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Punidha Bhoomiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Greendangal Thorkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Greendangal Thorkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athigaaram Valigalai Kootum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athigaaram Valigalai Kootum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaginil Yaettrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaginil Yaettrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravinai Maattrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravinai Maattrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Agathigal Aakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agathigal Aakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali Vaer Thaangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali Vaer Thaangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaayae Maraindha Pinnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayae Maraindha Pinnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayagam Inithaagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayagam Inithaagumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhukira Kobam Bathil Thaedumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhukira Kobam Bathil Thaedumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Varunkaalamum Puvi Meetkaatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varunkaalamum Puvi Meetkaatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaimurai Thalai Thookumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaimurai Thalai Thookumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhai Theerkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhai Theerkumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Punidha Bhoomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Punidha Bhoomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Por Indri Naam Ondraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Por Indri Naam Ondraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Serum Naal Vaaradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum Naal Vaaradha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanmam Pala Vedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanmam Pala Vedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Anbum Parithaabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Anbum Parithaabam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathariya Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathariya Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathavinai Moodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathavinai Moodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Erigira Veedum Viragaai Enjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erigira Veedum Viragaai Enjum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadum Mazhalai Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadum Mazhalai Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayunthangal Thaangumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayunthangal Thaangumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Anu Anuvaaga Karuvaanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anu Anuvaaga Karuvaanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhinthoduthae Anuvaal Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhinthoduthae Anuvaal Ingae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohellaigal Yedhum Ariyaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohellaigal Yedhum Ariyaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Purandooduthae Manitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purandooduthae Manitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo Ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaintha Theepori Ulagamaanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaintha Theepori Ulagamaanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaintha Kodugal Karaigalaanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaintha Kodugal Karaigalaanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhuntha Saavugal Vithaigalaaguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhuntha Saavugal Vithaigalaaguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaindha Saambalum Amaidhi Venduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaindha Saambalum Amaidhi Venduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaimurai Thalai Thookumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaimurai Thalai Thookumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhai Theerkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhai Theerkumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Punidha Bhoomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Punidha Bhoomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Por Indri Naam Ondraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Por Indri Naam Ondraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Serum Naal Vaaradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum Naal Vaaradha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viduthalai Vazhi Kaatumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduthalai Vazhi Kaatumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilakketrumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilakketrumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Manidha Sathiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Manidha Sathiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Mel Ennum Keezh Ennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mel Ennum Keezh Ennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolangal Maradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolangal Maradha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thai Nilam Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thai Nilam Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhvugal Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhvugal Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanudam Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanudam Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivinaiyin Peru Vali Thaangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivinaiyin Peru Vali Thaangum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaimurai Thalai Thookumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaimurai Thalai Thookumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhai Theerkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhai Theerkumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Punidha Bhoomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Punidha Bhoomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh Ooo Oh Ooo Oh Ooo Oh Ooo Ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Ooo Oh Ooo Oh Ooo Oh Ooo Ooo"/>
</div>
</pre>
