---
title: "muddabanthi song lyrics"
album: "Kousalya Krishnamurthy"
artist: "Dhibu Ninan Thomas"
lyricist: "Krishnakanth"
director: "Bhimaneni Srinivasa Rao"
path: "/albums/kousalya-krishnamurthy-lyrics"
song: "Muddabanthi"
image: ../../images/albumart/kousalya-krishnamurthy.jpg
date: 2019-08-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/CcgXrgfenQo"
type: "love"
singers:
  - Yazin Nizar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Muddabanthi puvvu ila paita vesena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddabanthi puvvu ila paita vesena"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddu muddu choopulatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddu muddu choopulatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde kosenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde kosenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Netiki nedu maarina eedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netiki nedu maarina eedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chese nerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chese nerame"/>
</div>
<div class="lyrico-lyrics-wrapper">Niddura ledu aakali ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niddura ledu aakali ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni dhoorame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni dhoorame"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakkadanaala chukkakivaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkadanaala chukkakivaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Dishti theesi haaratheeyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dishti theesi haaratheeyana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammadive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadive"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa da ni sa sa maa ga ma sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa da ni sa sa maa ga ma sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Da ni sa da ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da ni sa da ni sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Da pa ga sa ga ga ga sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da pa ga sa ga ga ga sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ni da ni sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ni da ni sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalalanu daache naa kannu neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalanu daache naa kannu neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamai pove naavanni neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamai pove naavanni neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagale merise minuguruve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagale merise minuguruve"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagale velise velugu nuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagale velise velugu nuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilapai nadiche merupu nuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilapai nadiche merupu nuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai varamai dhoruku nuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai varamai dhoruku nuve"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needa kooda cheekatlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needa kooda cheekatlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninniodhili pothundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninniodhili pothundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Neneppudu nee vente unta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neneppudu nee vente unta"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddabanthi puvvu ila paita vesena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddabanthi puvvu ila paita vesena"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddu muddu choopulatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddu muddu choopulatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde kosenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde kosenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parugulu theese naa raani neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugulu theese naa raani neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Padithe metthani nelauthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padithe metthani nelauthaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudu niliche bhujamautha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudu niliche bhujamautha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalanu kante nijamauthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalanu kante nijamauthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kashtam vasthe kalabadathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashtam vasthe kalabadathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadadhaakaane nilabadathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadadhaakaane nilabadathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alisosthe jo kodathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alisosthe jo kodathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelichosthe jai kodathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelichosthe jai kodathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisosthe o gudine kadathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisosthe o gudine kadathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muddabanthi puvvu ila paita vesena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddabanthi puvvu ila paita vesena"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddu muddu choopulatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddu muddu choopulatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde kosenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde kosenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounamugaane saigalathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamugaane saigalathone"/>
</div>
<div class="lyrico-lyrics-wrapper">Yentha kaalame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yentha kaalame"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaalini choopi daggarayyeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaalini choopi daggarayyeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaari choopave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaari choopave"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapasopaale naavika aape
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapasopaale naavika aape"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkasaari chentha cherave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkasaari chentha cherave"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammadive Ammadive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadive Ammadive"/>
</div>
</pre>
