---
title: "mugile venmugile song lyrics"
album: "Paramapadham"
artist: "Amrish"
lyricist: "Phonenix Dasan, Balanraj"
director: "K. Thirugnanam"
path: "/albums/Paramapadham-song-lyrics"
song: "Mugile Venmugile"
image: ../../images/albumart/paramapadham.jpg
date: 2021-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/L5CaRH8bZ-M"
type: "Love"
singers:
  - Bamba Bakiya
  - Yuhashini Matheyalagan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">mugile venmugile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugile venmugile"/>
</div>
<div class="lyrico-lyrics-wrapper">un vaanam naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vaanam naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">adi kovakara kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi kovakara kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">en mel kovam yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en mel kovam yenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">rathiye ratchasiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathiye ratchasiye"/>
</div>
<div class="lyrico-lyrics-wrapper">en boomi neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en boomi neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">unna suthi suthi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna suthi suthi "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kalvan naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalvan naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en yuire yuire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en yuire yuire"/>
</div>
<div class="lyrico-lyrics-wrapper">en aayul neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aayul neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en sagiye sagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sagiye sagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalvum neyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalvum neyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vennilave nilave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennilave nilave"/>
</div>
<div class="lyrico-lyrics-wrapper">en thedal neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thedal neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">venmathiye mathiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venmathiye mathiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ini yaavum neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini yaavum neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ennuyire.... ennuyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennuyire.... ennuyire"/>
</div>
<div class="lyrico-lyrics-wrapper">uyire uyire uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyire uyire uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">pesugindra varthaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesugindra varthaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanavilin saayale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanavilin saayale"/>
</div>
<div class="lyrico-lyrics-wrapper">maayam seigirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayam seigirai"/>
</div>
<div class="lyrico-lyrics-wrapper">thodargiren naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodargiren naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">un vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">nyabagangal yaavayum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nyabagangal yaavayum "/>
</div>
<div class="lyrico-lyrics-wrapper">oviyangal aakuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oviyangal aakuven"/>
</div>
<div class="lyrico-lyrics-wrapper">mucche kaatrilum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mucche kaatrilum "/>
</div>
<div class="lyrico-lyrics-wrapper">kalanthidu enthan jivane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalanthidu enthan jivane"/>
</div>
<div class="lyrico-lyrics-wrapper">oh osai illa aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh osai illa aasaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjam alluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjam alluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">nithanamaai kanaakkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nithanamaai kanaakkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kaankiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kaankiren"/>
</div>
<div class="lyrico-lyrics-wrapper">oh suvasame en suvasame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh suvasame en suvasame"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam ketkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam ketkume"/>
</div>
<div class="lyrico-lyrics-wrapper">kan jaadaiyil killathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan jaadaiyil killathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum saaigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum saaigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyire uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyire uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">en aayul neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aayul neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">en sagiye sagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en sagiye sagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">en vaalvum neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalvum neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">manmadhane madhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manmadhane madhane"/>
</div>
<div class="lyrico-lyrics-wrapper">en thedal neeyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thedal neeyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalane urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalane urave"/>
</div>
<div class="lyrico-lyrics-wrapper">ini yaavum neeyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini yaavum neeyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ennuyire uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennuyire uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">en aayul neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aayul neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ennavane alage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavane alage"/>
</div>
<div class="lyrico-lyrics-wrapper">ini yaavum neeyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini yaavum neeyadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vennilave nilave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennilave nilave"/>
</div>
<div class="lyrico-lyrics-wrapper">en thedal neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thedal neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">venmathiye mathiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venmathiye mathiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ini yaavum neeyadi eh..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini yaavum neeyadi eh.."/>
</div>
<div class="lyrico-lyrics-wrapper">en uyire eh eh eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyire eh eh eh"/>
</div>
</pre>
