---
title: "krishnaveni song lyrics"
album: "Orey Bujjiga"
artist: "Anup Rubens"
lyricist: "Shyam Kasarla"
director: "Vijay Kumar Konda"
path: "/albums/orey-bujjiga-lyrics"
song: "Krishnaveni"
image: ../../images/albumart/orey-bujjiga.jpg
date: 2020-10-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/3bp317vp9dU"
type: "happy"
singers:
  - Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Allamelligadda oo avva saatu bidda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allamelligadda oo avva saatu bidda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enuka nenu padda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enuka nenu padda"/>
</div>
<div class="lyrico-lyrics-wrapper">arey plan borla padda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey plan borla padda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenem baagu padda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenem baagu padda"/>
</div>
<div class="lyrico-lyrics-wrapper">Notlo velu peditey korakanonni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Notlo velu peditey korakanonni"/>
</div>
<div class="lyrico-lyrics-wrapper">Na notlo mannu kottakey krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na notlo mannu kottakey krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvantey manasu padi chachetonni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvantey manasu padi chachetonni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu sampi bonda pettakey krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu sampi bonda pettakey krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">krishnaveni oo krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="krishnaveni oo krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethoti kastamey krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethoti kastamey krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani nuvantey istamey krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani nuvantey istamey krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">krishna krishna krishna krishna krishna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="krishna krishna krishna krishna krishna"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey pasenger bandilona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey pasenger bandilona "/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaru ninnu chesinney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaru ninnu chesinney"/>
</div>
<div class="lyrico-lyrics-wrapper">Messenger whatsapplo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Messenger whatsapplo"/>
</div>
<div class="lyrico-lyrics-wrapper">Muchetlenno cheppine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muchetlenno cheppine"/>
</div>
<div class="lyrico-lyrics-wrapper">November manchu lekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="November manchu lekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvutuntey murisinney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvutuntey murisinney"/>
</div>
<div class="lyrico-lyrics-wrapper">December puvvulekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="December puvvulekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil lona dachinney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil lona dachinney"/>
</div>
<div class="lyrico-lyrics-wrapper">Calender simpesi cylinder ayyi pelithivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Calender simpesi cylinder ayyi pelithivey"/>
</div>
<div class="lyrico-lyrics-wrapper">oy krishnaveni oo krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oy krishnaveni oo krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethoti kastamey krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethoti kastamey krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani nuvantey istamey krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani nuvantey istamey krishnaveni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">krishnaveni oo krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="krishnaveni oo krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethoti kastamey krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethoti kastamey krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani nuvantey istamey krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani nuvantey istamey krishnaveni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maa urla andaritla neney pedda thopuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa urla andaritla neney pedda thopuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Bakaranaithini mi amma giste map ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakaranaithini mi amma giste map ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadimitla maa ayyaku ruddinaru soapuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadimitla maa ayyaku ruddinaru soapuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakemi samaj kaka guddhinanu off ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakemi samaj kaka guddhinanu off ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninna monna nuvvu kalisi kalalu kannamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna monna nuvvu kalisi kalalu kannamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyyala nuvu naku peeda kalai poyavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyyala nuvu naku peeda kalai poyavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka nimusham ayina ninnu idisi undalenonni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka nimusham ayina ninnu idisi undalenonni"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkal oo pamu lekka busalu kottuthunnavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkal oo pamu lekka busalu kottuthunnavey"/>
</div>
<div class="lyrico-lyrics-wrapper">krishnaveni oo krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="krishnaveni oo krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethoti kastamey krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethoti kastamey krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani nuvantey istamey krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani nuvantey istamey krishnaveni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Matter chinnadi meter emo peddadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matter chinnadi meter emo peddadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bujjigani batuku chudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bujjigani batuku chudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajarlaney paddadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajarlaney paddadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachina chinnadi naral tista unnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachina chinnadi naral tista unnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ijath anta chinigi chinigi sareytunnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ijath anta chinigi chinigi sareytunnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudumullu esukoni ayittadantey wifeu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudumullu esukoni ayittadantey wifeu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalgu rolla madya nilichi nurutundi knifu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalgu rolla madya nilichi nurutundi knifu"/>
</div>
<div class="lyrico-lyrics-wrapper">Daanintla peenugella undho ledho repu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daanintla peenugella undho ledho repu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa dead bodyni naa tho moistundira lifeu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa dead bodyni naa tho moistundira lifeu"/>
</div>
<div class="lyrico-lyrics-wrapper">krishnaveni oo krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="krishnaveni oo krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethoti kastamey krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethoti kastamey krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani nuvantey istamey krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani nuvantey istamey krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethoti kastamey krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethoti kastamey krishnaveni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaani nuvantey istamey krishnaveni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani nuvantey istamey krishnaveni"/>
</div>
</pre>
