---
title: "theriyatha thendral song lyrics"
album: "Kamali from Nadukkaveri"
artist: "Dheena Dhayalan"
lyricist: "Madhan Karky"
director: "Rajasekar Duraisamy"
path: "/albums/kamali-from-nadukkaveri-lyrics"
song: "Theriyatha Thendral"
image: ../../images/albumart/kamali-from-nadukkaveri.jpg
date: 2021-02-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/z0wcuyIQAiU"
type: "Love"
singers:
  - Akshaya Sivakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">theriyadha thendral 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyadha thendral "/>
</div>
<div class="lyrico-lyrics-wrapper">ennaith thaluvudhu yeno 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaith thaluvudhu yeno "/>
</div>
<div class="lyrico-lyrics-wrapper">puriyaadha pookkal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyaadha pookkal "/>
</div>
<div class="lyrico-lyrics-wrapper">ennul thirakkudhu thaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennul thirakkudhu thaano"/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiyaga nee en kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiyaga nee en kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">nadathidum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadathidum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyavun endhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyavun endhan"/>
</div>
<div class="lyrico-lyrics-wrapper">naanam uthirththiduveno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanam uthirththiduveno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vilundhen pidiththai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilundhen pidiththai "/>
</div>
<div class="lyrico-lyrics-wrapper">aludhen siriththai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aludhen siriththai "/>
</div>
<div class="lyrico-lyrics-wrapper">erindhen anaiththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erindhen anaiththai"/>
</div>
<div class="lyrico-lyrics-wrapper">en thaayin punnakaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thaayin punnakaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">isaaiyaai idhayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isaaiyaai idhayam "/>
</div>
<div class="lyrico-lyrics-wrapper">variyaai ulagam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="variyaai ulagam "/>
</div>
<div class="lyrico-lyrics-wrapper">mudivili nadanamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivili nadanamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tholan endru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholan endru "/>
</div>
<div class="lyrico-lyrics-wrapper">solli paarththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli paarththen"/>
</div>
<div class="lyrico-lyrics-wrapper">kaval endrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaval endrum "/>
</div>
<div class="lyrico-lyrics-wrapper">solli paarththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="solli paarththen"/>
</div>
<div class="lyrico-lyrics-wrapper">innum nooru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum nooru "/>
</div>
<div class="lyrico-lyrics-wrapper">pattam thandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam thandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">podhavillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhavillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">andha otrai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha otrai "/>
</div>
<div class="lyrico-lyrics-wrapper">sollai solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollai solla"/>
</div>
<div class="lyrico-lyrics-wrapper">veeramangai naanum alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeramangai naanum alla"/>
</div>
<div class="lyrico-lyrics-wrapper">neeye adhai sonnaal alagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeye adhai sonnaal alagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">midhavai nilavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="midhavai nilavaai"/>
</div>
<div class="lyrico-lyrics-wrapper">sitharum oliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sitharum oliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">adhile kanavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhile kanavaai"/>
</div>
<div class="lyrico-lyrics-wrapper">neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uravin pudhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravin pudhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">thiravaa mugaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiravaa mugaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">avai dhaanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avai dhaanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">avilnthaal thaan alagu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avilnthaal thaan alagu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">inmugam menkural
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inmugam menkural"/>
</div>
<div class="lyrico-lyrics-wrapper">venmoli ena ilukkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venmoli ena ilukkirai"/>
</div>
<div class="lyrico-lyrics-wrapper">enai un vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai un vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee eerththuk kolkindrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee eerththuk kolkindrai"/>
</div>
<div class="lyrico-lyrics-wrapper">en nilal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nilal"/>
</div>
<div class="lyrico-lyrics-wrapper">un nilal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nilal"/>
</div>
<div class="lyrico-lyrics-wrapper">otti konde kondaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otti konde kondaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivilli nadanamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivilli nadanamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivilli nadanamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivilli nadanamaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paadam ena undhan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadam ena undhan "/>
</div>
<div class="lyrico-lyrics-wrapper">paarvai enil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarvai enil"/>
</div>
<div class="lyrico-lyrics-wrapper">naal muluthum padipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naal muluthum padipen"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam ena undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam ena undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">vaarthai enil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaarthai enil"/>
</div>
<div class="lyrico-lyrics-wrapper">kopaiyai manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kopaiyai manam"/>
</div>
<div class="lyrico-lyrics-wrapper">veraethum ketkavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veraethum ketkavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhai mooda villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhai mooda villai"/>
</div>
<div class="lyrico-lyrics-wrapper">yaarum pesa villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarum pesa villai"/>
</div>
<div class="lyrico-lyrics-wrapper">vasam ennum mullai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasam ennum mullai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrum veesa villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrum veesa villai"/>
</div>
<div class="lyrico-lyrics-wrapper">moochum thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochum thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">udan udan udambe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udan udan udambe"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaale pogum pogavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaale pogum pogavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">onnum thondravillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum thondravillai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai kaanavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai kaanavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">arugil arugil nan kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arugil arugil nan kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">urugi urugi vaalthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urugi urugi vaalthale"/>
</div>
<div class="lyrico-lyrics-wrapper">pothatha enna thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothatha enna thollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aayiram vaanamai maarinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram vaanamai maarinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">nan ithul eerida kooruvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan ithul eerida kooruvaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">aayiram megamai thoorinaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram megamai thoorinaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalaai ennile veeluvaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalaai ennile veeluvaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vilundhen pidiththai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilundhen pidiththai "/>
</div>
<div class="lyrico-lyrics-wrapper">aludhen siriththai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aludhen siriththai "/>
</div>
<div class="lyrico-lyrics-wrapper">erindhen anaiththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erindhen anaiththai"/>
</div>
<div class="lyrico-lyrics-wrapper">en thaayin punnakaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thaayin punnakaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">isaaiyaai idhayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isaaiyaai idhayam "/>
</div>
<div class="lyrico-lyrics-wrapper">variyaai ulagam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="variyaai ulagam "/>
</div>
<div class="lyrico-lyrics-wrapper">mudivili nadanamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivili nadanamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">mudivili nadanamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudivili nadanamaai"/>
</div>
</pre>
