---
title: "kannadi nenjan song lyrics"
album: "Vanjagar Ulagam"
artist: "	Sam CS"
lyricist: "Madhan Karky"
director: "Manoj Beedha"
path: "/albums/vanjagar-ulagam-lyrics"
song: "Kannadi Nenjan"
image: ../../images/albumart/vanjagar-ulagam.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8IaYFrPPcN0"
type: "mass"
singers:
  - Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naano kannadi nenjan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naano kannadi nenjan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallae ennai yeno rasikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallae ennai yeno rasikkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naano katroodhum paiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naano katroodhum paiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullae ennil yeno vasikkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullae ennil yeno vasikkirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulo irulaai karaigindren ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulo irulaai karaigindren ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhiyae ozhiyae kollathae ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhiyae ozhiyae kollathae ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aravam ethuvum vara vendam pinnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aravam ethuvum vara vendam pinnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhiyae ozhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhiyae ozhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothum pothum pothum pothum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum pothum pothum pothum pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaiyin mugamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyin mugamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhin mugamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhin mugamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Orupol irukka koodathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orupol irukka koodathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugamum izhanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugamum izhanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhum izhanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhum izhanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyaal edai nee podathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyaal edai nee podathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee oru dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oru dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru ganam eriyumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru ganam eriyumae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thalaiyilae irunthu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thalaiyilae irunthu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan pizhai ena thavar ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan pizhai ena thavar ena"/>
</div>
<div class="lyrico-lyrics-wrapper">ThirunthaNee naduvana thalaivana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ThirunthaNee naduvana thalaivana"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivana madaiyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivana madaiyana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Virumbum ethuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbum ethuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaikkaa ulagil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaikkaa ulagil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaikkum ethaiyum yetrenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaikkum ethaiyum yetrenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbum vazhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbum vazhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhiyum pozhuthil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhiyum pozhuthil"/>
</div>
<div class="lyrico-lyrics-wrapper">Viriyum paathai yetrenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viriyum paathai yetrenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan mirugamai thirigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan mirugamai thirigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku yen vidaigalai alikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku yen vidaigalai alikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Etharkku nee kelvigal adukkinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etharkku nee kelvigal adukkinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku nee naduvana thalaivana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku nee naduvana thalaivana"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivana madaiyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivana madaiyana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irulo irulaai karaigindren ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulo irulaai karaigindren ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhiyae ozhiyae kollathae ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhiyae ozhiyae kollathae ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aravam ethuvum vara vendam pinnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aravam ethuvum vara vendam pinnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhiyae ozhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhiyae ozhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothum pothum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum pothum pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothum pothum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum pothum pothum"/>
</div>
</pre>
