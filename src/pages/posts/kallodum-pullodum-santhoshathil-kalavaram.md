---
title: "kallodum pullodum song lyrics"
album: "Santhoshathil Kalavaram"
artist: "Sivanag"
lyricist: "Mani Amudhavan"
director: "Kranthi Prasad"
path: "/albums/santhoshathil-kalavaram-lyrics"
song: "Kallodum Pullodum"
image: ../../images/albumart/santhoshathil-kalavaram.jpg
date: 2018-11-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fprAJSQBsTo"
type: "mass"
singers:
  - Nivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kallodum pullodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallodum pullodum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavulathu kandaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavulathu kandaai"/>
</div>
<div class="lyrico-lyrics-wrapper">mankooda maram kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mankooda maram kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">dheivamena kondaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheivamena kondaai"/>
</div>
<div class="lyrico-lyrics-wrapper">kallodum pullodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallodum pullodum"/>
</div>
<div class="lyrico-lyrics-wrapper">irukindra dheivam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukindra dheivam"/>
</div>
<div class="lyrico-lyrics-wrapper">unnodum undendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnodum undendru"/>
</div>
<div class="lyrico-lyrics-wrapper">ariyaamal nindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ariyaamal nindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">kankondu paarkindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kankondu paarkindra"/>
</div>
<div class="lyrico-lyrics-wrapper">yellamae maayai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yellamae maayai"/>
</div>
<div class="lyrico-lyrics-wrapper">kanmoodi paar ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanmoodi paar ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">yerigindra theeyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yerigindra theeyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kallodum pullodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallodum pullodum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavulathu kandaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavulathu kandaai"/>
</div>
<div class="lyrico-lyrics-wrapper">mankooda maram kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mankooda maram kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">dheivamena kondaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheivamena kondaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aabathai kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aabathai kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">anjuvadhu nandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anjuvadhu nandra"/>
</div>
<div class="lyrico-lyrics-wrapper">dheepathai irul vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheepathai irul vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">theendiyathu unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theendiyathu unda"/>
</div>
<div class="lyrico-lyrics-wrapper">megathai kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megathai kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">miraluvadhu nandra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="miraluvadhu nandra"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanathai idi vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanathai idi vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">thaakiyadhu unda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaakiyadhu unda"/>
</div>
<div class="lyrico-lyrics-wrapper">yezhundhidu yezhundhidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yezhundhidu yezhundhidu"/>
</div>
<div class="lyrico-lyrics-wrapper">irulinai yerithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irulinai yerithidu"/>
</div>
<div class="lyrico-lyrics-wrapper">theeye indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeye indri"/>
</div>
<div class="lyrico-lyrics-wrapper">velicham adhu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velicham adhu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">neeye dheivam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeye dheivam"/>
</div>
<div class="lyrico-lyrics-wrapper">veliyil adhu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veliyil adhu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">bayandhu varum aado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayandhu varum aado"/>
</div>
<div class="lyrico-lyrics-wrapper">paanaiyil veezhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paanaiyil veezhum"/>
</div>
<div class="lyrico-lyrics-wrapper">paayndhu varum puli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paayndhu varum puli "/>
</div>
<div class="lyrico-lyrics-wrapper">than kaatinai aalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than kaatinai aalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kallodum pullodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallodum pullodum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavulathu kandaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavulathu kandaai"/>
</div>
<div class="lyrico-lyrics-wrapper">mankooda maram kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mankooda maram kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">dheivamena kondaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheivamena kondaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatrukku ulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrukku ulle"/>
</div>
<div class="lyrico-lyrics-wrapper">kadum puyalum undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadum puyalum undu"/>
</div>
<div class="lyrico-lyrics-wrapper">unakulle yedhu undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakulle yedhu undu"/>
</div>
<div class="lyrico-lyrics-wrapper">theduvadhu nandru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theduvadhu nandru"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalukku ulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalukku ulle"/>
</div>
<div class="lyrico-lyrics-wrapper">yeri malaigal undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeri malaigal undu"/>
</div>
<div class="lyrico-lyrics-wrapper">udalukkul uyirukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udalukkul uyirukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">thedu adhai sendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedu adhai sendru"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhithidu vizhithidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhithidu vizhithidu"/>
</div>
<div class="lyrico-lyrics-wrapper">viraindhu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viraindhu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">vidai yedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidai yedu"/>
</div>
<div class="lyrico-lyrics-wrapper">kambai arindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambai arindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">anjugira maadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anjugira maadu"/>
</div>
<div class="lyrico-lyrics-wrapper">kombai arindhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kombai arindhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">vandi izhukkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandi izhukkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">unmai ariyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unmai ariyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirellaam savame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirellaam savame"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai arindhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai arindhaale"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ingu sivame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ingu sivame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kallodum pullodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallodum pullodum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavulathu kandaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavulathu kandaai"/>
</div>
<div class="lyrico-lyrics-wrapper">mankooda maram kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mankooda maram kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">dheivamena kondaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheivamena kondaai"/>
</div>
<div class="lyrico-lyrics-wrapper">kallodum pullodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallodum pullodum"/>
</div>
<div class="lyrico-lyrics-wrapper">irukindra dheivam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukindra dheivam"/>
</div>
<div class="lyrico-lyrics-wrapper">unnodum undendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnodum undendru"/>
</div>
<div class="lyrico-lyrics-wrapper">ariyaamal nindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ariyaamal nindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">kankondu paarkindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kankondu paarkindra"/>
</div>
<div class="lyrico-lyrics-wrapper">yellamae maayai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yellamae maayai"/>
</div>
<div class="lyrico-lyrics-wrapper">kanmoodi paar ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanmoodi paar ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">yerigindra theeyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yerigindra theeyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kallodum pullodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallodum pullodum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavulathu kandaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavulathu kandaai"/>
</div>
<div class="lyrico-lyrics-wrapper">mankooda maram kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mankooda maram kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">dheivamena kondaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dheivamena kondaai"/>
</div>
</pre>
