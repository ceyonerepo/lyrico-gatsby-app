---
title: "parda parda song lyrics"
album: "Once Upon a Time in Mumbai"
artist: "Pritam Chakraborty"
lyricist: "Irshad Kamil"
director: "Milan Luthria"
path: "/albums/once-upon-a-time-in-mumbai-lyrics"
song: "Parda Parda"
image: ../../images/albumart/once-upon-a-time-in-mumbai.jpg
date: 2010-07-30
lang: hindi
youtubeLink: "https://www.youtube.com/embed/2mXHC43rCDk"
type: "happy"
singers:
  - Sunidhi Chauhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Duniya mein ha ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duniya mein ha ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Logon ko ha ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Logon ko ha ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhokha kabhi ho jaata hai.. hu..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhokha kabhi ho jaata hai.. hu.."/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhon hi ha ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon hi ha ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhon mein ha ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon mein ha ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaron ka dil kho jata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaron ka dil kho jata hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Duniya mein ha ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duniya mein ha ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Logon ko ha ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Logon ko ha ha ha ha ha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhokha kabhi ho jaata hai.. hu..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhokha kabhi ho jaata hai.. hu.."/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhon hi ha ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon hi ha ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhon mein ha ha ha ha ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon mein ha ha ha ha ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaron ka dil kho jata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaron ka dil kho jata hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Do dil donon jawaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do dil donon jawaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhon ka hai yeh bayan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhon ka hai yeh bayan"/>
</div>
<div class="lyrico-lyrics-wrapper">Donon ke darmiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Donon ke darmiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi bhi ho na yahan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi bhi ho na yahan.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parda parda haan.. parda parda haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parda parda haan.. parda parda haan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Apno se kaisa parda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apno se kaisa parda"/>
</div>
<div class="lyrico-lyrics-wrapper">Parda parda haan.. parda parda haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parda parda haan.. parda parda haan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Apno se kaisa parda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apno se kaisa parda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Monica... Oh my darling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monica... Oh my darling"/>
</div>
<div class="lyrico-lyrics-wrapper">O Monica... aa.. O my darling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Monica... aa.. O my darling"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dilbar ki fitrat mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dilbar ki fitrat mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhokha bhi hai aur chahat bhi..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhokha bhi hai aur chahat bhi.."/>
</div>
<div class="lyrico-lyrics-wrapper">Jaan jaaye [hu..] to jaaye [ha..]
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaan jaaye [hu..] to jaaye [ha..]"/>
</div>
<div class="lyrico-lyrics-wrapper">Sochun kyun jab maine ulfat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sochun kyun jab maine ulfat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaye dilbar ki fitrat mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaye dilbar ki fitrat mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoka bhi hai aur chahat bhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoka bhi hai aur chahat bhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaan jaaye to jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaan jaaye to jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sochun kyun jab maine ulfat ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sochun kyun jab maine ulfat ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaaye bhi to kahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaaye bhi to kahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dushman hai sara jahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dushman hai sara jahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jis pe bhi ho guman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jis pe bhi ho guman"/>
</div>
<div class="lyrico-lyrics-wrapper">Kehna tu uska yahan...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kehna tu uska yahan..."/>
</div>
<div class="lyrico-lyrics-wrapper">Parda parda haan.. parda parda haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parda parda haan.. parda parda haan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Apno se kaisa parda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apno se kaisa parda"/>
</div>
<div class="lyrico-lyrics-wrapper">Parda parda haan.. parda parda haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parda parda haan.. parda parda haan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Apno se kaisa parda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apno se kaisa parda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Monica... Oh my darling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monica... Oh my darling"/>
</div>
<div class="lyrico-lyrics-wrapper">O Monica... aa.. O my darling ha!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Monica... aa.. O my darling ha!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Woh aa gaya, dekho dekho woh aa gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh aa gaya, dekho dekho woh aa gaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Halchal si har pal hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halchal si har pal hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Betaab apni jawani hai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Betaab apni jawani hai.."/>
</div>
<div class="lyrico-lyrics-wrapper">Bekal main [hu..], bekal tu [ha..]
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bekal main [hu..], bekal tu [ha..]"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedard dil ki kahani hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedard dil ki kahani hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaye haaye..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaye haaye.."/>
</div>
<div class="lyrico-lyrics-wrapper">Halchal si har pal hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halchal si har pal hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Betaab apni jawani hai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Betaab apni jawani hai.."/>
</div>
<div class="lyrico-lyrics-wrapper">Bekal main, bekal tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bekal main, bekal tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedard dil ki kahani hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedard dil ki kahani hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthta dil se dhuan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthta dil se dhuan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh dil hai tera pata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh dil hai tera pata"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaja reh le yahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja reh le yahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaayega ab tu kahan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaayega ab tu kahan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Parda parda haan.. parda parda haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parda parda haan.. parda parda haan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Apno se kaisa parda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apno se kaisa parda"/>
</div>
<div class="lyrico-lyrics-wrapper">Parda parda haan.. parda parda haan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parda parda haan.. parda parda haan.."/>
</div>
<div class="lyrico-lyrics-wrapper">Apno se kaisa parda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apno se kaisa parda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Monica... Oh my darling
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monica... Oh my darling"/>
</div>
<div class="lyrico-lyrics-wrapper">O Monica... O my darling ha!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Monica... O my darling ha!"/>
</div>
</pre>
