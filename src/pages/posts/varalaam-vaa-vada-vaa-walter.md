---
title: 'varalaam vaa vada vaa song lyrics'
album: 'Walter'
artist: 'Dharma Prakash'
lyricist: 'Arun Bharathi'
director: 'U Anbu'
path: '/albums/walter-song-lyrics'
song: 'Varalaam Vaa Vada Vaa'
image: ../../images/albumart/walter.jpg
date: 2020-03-13
lang: tamil
singers: 
- Mano
youtubeLink: "https://www.youtube.com/embed/yBQuTsHoNmg"
type: 'intro'
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">varalaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varalaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thozha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thozha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">adangatha kootatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangatha kootatha "/>
</div>
<div class="lyrico-lyrics-wrapper">anbala adakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbala adakka"/>
</div>
<div class="lyrico-lyrics-wrapper">varalaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varalaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">yarunu kaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarunu kaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">varalaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varalaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thangatha kotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangatha kotta"/>
</div>
<div class="lyrico-lyrics-wrapper">varalaam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varalaam vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu enga petta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu enga petta"/>
</div>
<div class="lyrico-lyrics-wrapper">kondadi theerpome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondadi theerpome"/>
</div>
<div class="lyrico-lyrics-wrapper">vedi onnu vedika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedi onnu vedika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vambukku varamatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambukku varamatom"/>
</div>
<div class="lyrico-lyrics-wrapper">vanthale vidamatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthale vidamatom"/>
</div>
<div class="lyrico-lyrics-wrapper">veelatha vengaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veelatha vengaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">verunkuchi pathathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verunkuchi pathathu"/>
</div>
<div class="lyrico-lyrics-wrapper">verunkoochal podathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verunkoochal podathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu enga kaalaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu enga kaalaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu vegam"/>
</div>
<div class="lyrico-lyrics-wrapper">undachu undachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undachu undachu"/>
</div>
<div class="lyrico-lyrics-wrapper">engal kaaval neeyagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal kaaval neeyagave"/>
</div>
<div class="lyrico-lyrics-wrapper">pagai kootam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagai kootam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">thoolaga thoolaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoolaga thoolaga"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum engal theeyagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum engal theeyagave"/>
</div>
<div class="lyrico-lyrics-wrapper">narigal kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narigal kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">nadungattum indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadungattum indru"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjai nimirthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjai nimirthi"/>
</div>
<div class="lyrico-lyrics-wrapper">nirpom vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirpom vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">varumai kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varumai kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">oliyatum indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oliyatum indru"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaigal udaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaigal udaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">velvom vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velvom vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">urangi kedakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urangi kedakura"/>
</div>
<div class="lyrico-lyrics-wrapper">kathi onnu ethuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi onnu ethuku"/>
</div>
<div class="lyrico-lyrics-wrapper">oorum oru kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorum oru kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">atha vida sirapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha vida sirapu"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaivan aagida than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaivan aagida than"/>
</div>
<div class="lyrico-lyrics-wrapper">thambattam ethuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thambattam ethuku"/>
</div>
<div class="lyrico-lyrics-wrapper">thondan nee aanal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thondan nee aanal"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi varum unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi varum unaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">urasa than payanthu ne oyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urasa than payanthu ne oyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">ahah appapo anganga sayatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ahah appapo anganga sayatha"/>
</div>
<div class="lyrico-lyrics-wrapper">ellorkum paninje than pogatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellorkum paninje than pogatha"/>
</div>
<div class="lyrico-lyrics-wrapper">summame valthutu sagatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summame valthutu sagatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh vesangal podathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh vesangal podathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kosangal pote than valathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kosangal pote than valathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thedathe nee thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedathe nee thane"/>
</div>
<div class="lyrico-lyrics-wrapper">vetriyai verango thedatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetriyai verango thedatha "/>
</div>
<div class="lyrico-lyrics-wrapper">thedatha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedatha da"/>
</div>
</pre>