---
title: 'kathari poovazhagi song lyrics'
album: 'Asuran'
artist: 'G.V. Prakash Kumar'
lyricist: 'Ekadesi'
director: 'Vetrimaaran'
path: '/albums/asuran-song-lyrics'
song: 'Kathari Poovazhagi'
image: ../../images/albumart/asuran.jpg
date: 2019-10-04
lang: tamil
singers: 
- Velmurugan
- Rajalakshmi
- Napolia
youtubeLink: "https://www.youtube.com/embed/OyrTL5U81og"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Thannae nannaanae thannae nannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae nannaanae thannae nannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae nannaanae thannae nannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae nannaanae thannae nannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae nannaanae thannae nannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae nannaanae thannae nannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae nannaanae thannae nannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae nannaanae thannae nannaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kathiri poovazhagi karayaa pottazhagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kathiri poovazhagi karayaa pottazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Color-u suvaiyaattam unnoda nenappu adiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Color-u suvaiyaattam unnoda nenappu adiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sottaankallu aadayila pudikkuthu kirukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Sottaankallu aadayila pudikkuthu kirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Varappu meesakaara vathaatha aasakaara
<input type="checkbox" class="lyrico-select-lyric-line" value="Varappu meesakaara vathaatha aasakaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna naan kattikkuren ooru munnala
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna naan kattikkuren ooru munnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada vekkapada vena enna paaru kannaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada vekkapada vena enna paaru kannaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thannae thannaanae thannae thannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae thannaanae thannae thannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae thannaanae thannae thannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae thannaanae thannae thannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae thannaanae thannae thannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae thannaanae thannae thannaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Maiyaala kannezhudhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Maiyaala kannezhudhi"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaalibatha mayakkuriyae
<input type="checkbox" class="lyrico-select-lyric-line" value="En vaalibatha mayakkuriyae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Kaathaadi pola naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaathaadi pola naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna nikkaama suthurenae..ae….ae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna nikkaama suthurenae..ae….ae…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kazhutha pola thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kazhutha pola thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga sumakkaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhaga sumakkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaku thaayendi konja venum naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enaku thaayendi konja venum naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Both  Aruvaa pola nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Both  Aruvaa pola nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Morappaa nadakkuriyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Morappaa nadakkuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudaa muradaa iruppen unnodathaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirudaa muradaa iruppen unnodathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey kathiri poovazhagi karayaa pottazhagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey kathiri poovazhagi karayaa pottazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Color-u suvaiyaattam unnoda nenappu adiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Color-u suvaiyaattam unnoda nenappu adiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sottaankallu aadayila pudikkuthu kirukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Sottaankallu aadayila pudikkuthu kirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haan haan aan
<input type="checkbox" class="lyrico-select-lyric-line" value="Haan haan aan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varappu meesakaara vathaatha aasakaara
<input type="checkbox" class="lyrico-select-lyric-line" value="Varappu meesakaara vathaatha aasakaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna naan kattikkuren ooru munnala
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna naan kattikkuren ooru munnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada vekkapada vena enna paaru kannaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada vekkapada vena enna paaru kannaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thannae thannaanae thannae thannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae thannaanae thannae thannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae thannaanae thannae thannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae thannaanae thannae thannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae thannaanae thannae thannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae thannaanae thannae thannaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thannae nannaanae thannae nannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae nannaanae thannae nannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae nannaanae thannae nannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae nannaanae thannae nannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae nannaanae thannae nannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae nannaanae thannae nannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae nannaanae thannae nannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae nannaanae thannae nannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thannaenannae thannaenannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey thannaenannae thannaenannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae thannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae thannaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thannaenannae thannaenannae
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey thannaenannae thannaenannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae thannaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae thannaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Karagaattam aaduthu nenju
<input type="checkbox" class="lyrico-select-lyric-line" value="Karagaattam aaduthu nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna kandaalae theruvula ninnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna kandaalae theruvula ninnu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naan kulikkum thamirabharani
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan kulikkum thamirabharani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann thoongaama vaanguna varam nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Kann thoongaama vaanguna varam nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aalam vizhuthaattam adada thalamayiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Aalam vizhuthaattam adada thalamayiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooli aadidathaan thothu senju thaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thooli aadidathaan thothu senju thaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Both  Ilavam panjula nee yethura vilakku thiri
<input type="checkbox" class="lyrico-select-lyric-line" value="Both  Ilavam panjula nee yethura vilakku thiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathikkum thithikkum anaichaa nikkaathadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Pathikkum thithikkum anaichaa nikkaathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey hey kathiri poovazhagi karayaa pottazhagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey hey kathiri poovazhagi karayaa pottazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Color-u suvaiyaattam unnoda nenappu adiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Color-u suvaiyaattam unnoda nenappu adiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sottaankallu aadayila pudikkuthu kirukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Sottaankallu aadayila pudikkuthu kirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haan haan haan
<input type="checkbox" class="lyrico-select-lyric-line" value="Haan haan haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Varappu meesakaara vathaatha aasakaara
<input type="checkbox" class="lyrico-select-lyric-line" value="Varappu meesakaara vathaatha aasakaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna naan kattikkuren ooru munnala
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna naan kattikkuren ooru munnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada vekkapada vena enna paaru kannaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada vekkapada vena enna paaru kannaala"/>
</div>
</pre>