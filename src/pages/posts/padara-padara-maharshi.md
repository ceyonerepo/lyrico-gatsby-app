---
title: "padara padara song lyrics"
album: "Maharshi"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Vamshi Paidipally"
path: "/albums/maharshi-lyrics"
song: "Padara Padara"
image: ../../images/albumart/maharshi.jpg
date: 2019-05-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/EgZmo26mp8s"
type: "happy"
singers:
  - Shankar Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bhallumantu ningi vollu virigenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhallumantu ningi vollu virigenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaddi parakathona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaddi parakathona"/>
</div>
<div class="lyrico-lyrics-wrapper">Edaari kallu theruchukunna velana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edaari kallu theruchukunna velana"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinuku poola vaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinuku poola vaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Samudhramentha daahamesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samudhramentha daahamesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethikenu oota bhaavine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethikenu oota bhaavine"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirassu vanchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirassu vanchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikaramanchu mudhide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikaramanchu mudhide"/>
</div>
<div class="lyrico-lyrics-wrapper">Matti nelaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matti nelaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padara padara padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padara padara padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee adugiki padunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee adugiki padunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Petti padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petti padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee adavini chadunu cheyyimari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee adavini chadunu cheyyimari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethukuthunna siri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethukuthunna siri"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorukuthundi kadha raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorukuthundi kadha raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padara padara padara ee pudamini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padara padara padara ee pudamini"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigi choodu padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigi choodu padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gelupanu malupu ekkadanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gelupanu malupu ekkadanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Prashnalannitiki samadhaanamidira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prashnalannitiki samadhaanamidira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee katha idhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee katha idhira"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee modhalidhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee modhalidhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee padhamuna modhatadugeyiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee padhamuna modhatadugeyiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee tharamidhira anitaramidiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee tharamidhira anitaramidiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani chaatey raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani chaatey raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padara padara padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padara padara padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee adugiki padunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee adugiki padunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Petti padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Petti padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee adavini chadunu cheyyimari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee adavini chadunu cheyyimari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethukuthunna siri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethukuthunna siri"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorukuthundi kadha raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorukuthundi kadha raa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padara padara padara ee pudamini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padara padara padara ee pudamini"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigi choodu padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigi choodu padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gelupanu malupu ekkadanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gelupanu malupu ekkadanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Prashnalannitiki samadhaanamidira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prashnalannitiki samadhaanamidira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhallumantu ningi vollu virigenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhallumantu ningi vollu virigenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaddi parakathona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaddi parakathona"/>
</div>
<div class="lyrico-lyrics-wrapper">Edaari kallu theruchukunna velana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edaari kallu theruchukunna velana"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinuku poola vaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinuku poola vaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Samudhramentha daahamesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samudhramentha daahamesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethikenu oota bhaavine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethikenu oota bhaavine"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirassu vanchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirassu vanchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikaramanchu mudhide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikaramanchu mudhide"/>
</div>
<div class="lyrico-lyrics-wrapper">Matti nelaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matti nelaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhile ee kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhile ee kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana ragile vedhanaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana ragile vedhanaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhulalley visirina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhulalley visirina"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasala baanam nuvveraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasala baanam nuvveraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagile ila hrudayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagile ila hrudayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana yedhalo rodhanaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana yedhalo rodhanaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Varamalle dhorikina aakhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamalle dhorikina aakhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayam nuvveraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayam nuvveraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanureppalalo thadi endhukani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanureppalalo thadi endhukani"/>
</div>
<div class="lyrico-lyrics-wrapper">Thananadigey vaade leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thananadigey vaade leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilapincheti ee bhoomi odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilapincheti ee bhoomi odi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chigurinchelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chigurinchelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padara padara padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padara padara padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee halamunu bhujamuketthi padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee halamunu bhujamuketthi padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee nelanu edhaku hatthukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nelanu edhaku hatthukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Molakaletthamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molakaletthamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilupunicchi padaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilupunicchi padaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padara padara padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padara padara padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee veluganu palugu dinchi padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee veluganu palugu dinchi padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagullatho panikiraanidanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagullatho panikiraanidanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathuku bhoomilika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathuku bhoomilika"/>
</div>
<div class="lyrico-lyrics-wrapper">Methukulicchu kadaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Methukulicchu kadaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelo ee chalanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo ee chalanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari kada sanchalanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari kada sanchalanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinukalle modalay uppena kaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinukalle modalay uppena kaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee kathanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee kathanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo jadiki chelarege alajadiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo jadiki chelarege alajadiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupalle modalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupalle modalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Charithaga maare nee payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charithaga maare nee payanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee aashayame thama kosamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aashayame thama kosamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelisaakaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelisaakaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu lakshyamani thama rakshavani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu lakshyamani thama rakshavani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninadinchelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninadinchelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padara padara padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padara padara padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gathamuku kottha janmamidira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gathamuku kottha janmamidira"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yetthuku thagina lothu idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yetthuku thagina lothu idhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholi punaadi gadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi punaadi gadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalupu terichi padaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalupu terichi padaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padara padara padara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padara padara padara"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathokkari kathavu nuvvu kadara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathokkari kathavu nuvvu kadara"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oravadi bhavitha kalala vodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oravadi bhavitha kalala vodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathuku sadhyapadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathuku sadhyapadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagubadiki badiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagubadiki badiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanani thanu thelusukunna halamuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanani thanu thelusukunna halamuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Polamutho prayanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamutho prayanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana loni rushi ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana loni rushi ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Velikitheeyu manishiki ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velikitheeyu manishiki ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye pramaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye pramaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ushassu entha oopirichhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushassu entha oopirichhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Penchina kaanthichukkavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penchina kaanthichukkavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharala velithi vethiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharala velithi vethiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Theercha vacchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theercha vacchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Velugu rekavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velugu rekavo"/>
</div>
</pre>
