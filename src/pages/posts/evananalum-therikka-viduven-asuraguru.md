---
title: "evananalum therikka viduven song lyrics"
album: "Asuraguru"
artist: "Ganesh Raghavendra"
lyricist: "Ganesh Raghavendra"
director: "A. Raajdheep"
path: "/albums/asuraguru-song-lyrics"
song: "Evananalum Therikka Viduven"
image: ../../images/albumart/asuraguru.jpg
date: 2020-03-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/R-PnutXG_e4"
type: "Mass"
singers:
  - Sivam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thakitta Thakitta Thaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakitta Thakitta Thaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhiram Kodhi Kodhikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhiram Kodhi Kodhikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakitta Thakitta Thaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakitta Thakitta Thaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Thada Thadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Thada Thadakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evananalum Therikka Viduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evananalum Therikka Viduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Emananaalum Therikka Viduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emananaalum Therikka Viduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanai Ethirthu Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanai Ethirthu Vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Evan Evan Evan Evan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Evan Evan Evan Evan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivanukku Inga Pottiyum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanukku Inga Pottiyum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivana Veezhtha Yaarume Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivana Veezhtha Yaarume Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanukku Nigare Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanukku Nigare Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivandhaan Sivan Sivan Sivan Sivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivandhaan Sivan Sivan Sivan Sivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evananalum Therikka Viduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evananalum Therikka Viduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Emananaalum Therikka Viduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emananaalum Therikka Viduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanai Ethirthu Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanai Ethirthu Vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Evan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Evan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivanukku Inga Pottiyum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanukku Inga Pottiyum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivana Veezhtha Yaarume Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivana Veezhtha Yaarume Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanukku Nigare Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanukku Nigare Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivandhaan Sivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivandhaan Sivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhudhaan Inge Dharmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhudhaan Inge Dharmam"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhudhaan Inge Nyaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhudhaan Inge Nyaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnul Thondruvathethuvoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnul Thondruvathethuvoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuve Unakku Dharmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuve Unakku Dharmam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asura Asura Asura Asuragurudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asura Asura Asura Asuragurudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravum Pagalum Vettai Nadathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravum Pagalum Vettai Nadathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Therima Ivandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therima Ivandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Agorathaandavam Aadidu Vaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agorathaandavam Aadidu Vaandaa"/>
</div>
</pre>
