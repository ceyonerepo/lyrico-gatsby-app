---
title: "she is my girl song lyrics"
album: "Dev"
artist: "Harris Jayaraj"
lyricist: "Rajath Ravishankar"
director: "Rajath Ravishankar"
path: "/albums/dev-lyrics"
song: "She Is My Girl"
image: ../../images/albumart/dev.jpg
date: 2019-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SgcIj0X0B3s"
type: "happy"
singers:
  - Haricharan
  - Christopher Stanley
  - Mahathi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Peppen Pepepen Pepepepepen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peppen Pepepen Pepepepepen"/>
</div>
<div class="lyrico-lyrics-wrapper">Peppen Pepepen Pepepen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peppen Pepepen Pepepen"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluthey Why Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluthey Why Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peppen Pepepen Pepepepepen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peppen Pepepen Pepepepepen"/>
</div>
<div class="lyrico-lyrics-wrapper">Peppen Pepepen Pepepen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peppen Pepepen Pepepen"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluthey Why Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluthey Why Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First Time Nenjamthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First Time Nenjamthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaru Maara Thulluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru Maara Thulluthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rest-eh Illamal Love-u Panna Solluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rest-eh Illamal Love-u Panna Solluthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Girl You Are My Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Girl You Are My Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First Time Unnaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First Time Unnaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Panna Yeangguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Panna Yeangguthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Stop-eh Pannama Suththi Vara Solluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stop-eh Pannama Suththi Vara Solluthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Girl You Are My Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Girl You Are My Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bike-il Pogalama Oora Suththalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bike-il Pogalama Oora Suththalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Love-il Idhu Common Thaanma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Love-il Idhu Common Thaanma"/>
</div>
<div class="lyrico-lyrics-wrapper">Theanil Vannam Neema Kovam Enmel Yenma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theanil Vannam Neema Kovam Enmel Yenma"/>
</div>
<div class="lyrico-lyrics-wrapper">Seri Walking At Least Pogalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seri Walking At Least Pogalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First Time Nenjamthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First Time Nenjamthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaru Maara Thulluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru Maara Thulluthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rest-eh Illamal Love-u Panna Solluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rest-eh Illamal Love-u Panna Solluthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Girl You Are My Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Girl You Are My Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First Time Unnaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First Time Unnaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Panna Yeangguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Panna Yeangguthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Stop-eh Pannama Suththi Vara Solluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stop-eh Pannama Suththi Vara Solluthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Girl You Are My Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Girl You Are My Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Azhagukku Definition Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Azhagukku Definition Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannil Theriyuthu Silmisam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannil Theriyuthu Silmisam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Kooda Kadhal Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kooda Kadhal Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan Vaasathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Vaasathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai Uchiyinil Sernthuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Uchiyinil Sernthuduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Nesithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Nesithal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Ulagaiye Maranthidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Ulagaiye Maranthidalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Pola Dhinamum Malarthidalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Pola Dhinamum Malarthidalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkena Oruthiyai Piranthu Vittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena Oruthiyai Piranthu Vittal"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasaye Azhaginil Urukki Vittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasaye Azhaginil Urukki Vittal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee En Ninaivinil Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee En Ninaivinil Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaikkamal Irukkavum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaikkamal Irukkavum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Yeno Puriyavum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Yeno Puriyavum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaley Thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaley Thollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Kariyuthu Unnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kariyuthu Unnal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidivatham Pidikkuthu Thannal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidivatham Pidikkuthu Thannal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Okay Mattum Naan Sonnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Okay Mattum Naan Sonnal"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvaaye Pinnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvaaye Pinnal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First Time Nenjamthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First Time Nenjamthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaru Maara Thulluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaru Maara Thulluthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rest-eh Illamal Love-u Panna Solluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rest-eh Illamal Love-u Panna Solluthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Girl You Are My Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Girl You Are My Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First Time Unnaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First Time Unnaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Panna Yeangguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Panna Yeangguthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Stop-eh Pannama Suththi Vara Solluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stop-eh Pannama Suththi Vara Solluthey"/>
</div>
<div class="lyrico-lyrics-wrapper">Why Girl You Are My Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Why Girl You Are My Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bike-il Pogalama Oora Suththelama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bike-il Pogalama Oora Suththelama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Love-il Idhu Common Thaanma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Love-il Idhu Common Thaanma"/>
</div>
<div class="lyrico-lyrics-wrapper">Theanil Vannam Ama Aasai Illai Poma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theanil Vannam Ama Aasai Illai Poma"/>
</div>
<div class="lyrico-lyrics-wrapper">Seri Walking Mattum Polam Vaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seri Walking Mattum Polam Vaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peppen Pepepen Pepepepepen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peppen Pepepen Pepepepepen"/>
</div>
<div class="lyrico-lyrics-wrapper">Peppen Pepepen Pepepen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peppen Pepepen Pepepen"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Girl You Are My Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Girl You Are My Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peppen Pepepen Pepepepepen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peppen Pepepen Pepepepepen"/>
</div>
<div class="lyrico-lyrics-wrapper">Peppen Pepepen Pepepen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peppen Pepepen Pepepen"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Girl You Are My Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Girl You Are My Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peppen Pepepen Pepepepepen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peppen Pepepen Pepepepepen"/>
</div>
<div class="lyrico-lyrics-wrapper">Peppen Pepepen Pepepen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peppen Pepepen Pepepen"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Girl You Are My Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Girl You Are My Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peppen Pepepen Pepepepepen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peppen Pepepen Pepepepepen"/>
</div>
<div class="lyrico-lyrics-wrapper">Peppen Pepepen Pepepen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peppen Pepepen Pepepen"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are My Girl You Are My Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are My Girl You Are My Girl"/>
</div>
</pre>
