---
title: "thimiru kaattaadha di song lyrics"
album: "LKG"
artist: "Leon James"
lyricist: "Vignesh Shivan"
director: "K. R. Prabhu"
path: "/albums/lkg-lyrics"
song: "Thimiru Kaattaadha Di"
image: ../../images/albumart/lkg.jpg
date: 2019-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/igidmYrFT-Y"
type: "love"
singers:
  - D. Sathyaprakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru Kaattaadha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thimiru Kaattaadha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thimiru Kaattaadha Thimiru Kaattaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thimiru Kaattaadha Thimiru Kaattaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru Kaattaadha Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prachaarame Nee Pannamale Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prachaarame Nee Pannamale Yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vote’ah Thaan Vangi Vachikitiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vote’ah Thaan Vangi Vachikitiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhukoottam Kooda Pottu Solvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhukoottam Kooda Pottu Solvene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammoda Kootani Set Aagaadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammoda Kootani Set Aagaadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Vaangaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Vaangaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart Inga Kedaayadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart Inga Kedaayadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Midhi Vaangaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhi Vaangaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Inga Kidayaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Inga Kidayaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhu Kedaikkum Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu Kedaikkum Nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind-u-ku Theriyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind-u-ku Theriyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumaara Irukkum Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumaara Irukkum Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Thaan Puriyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Thaan Puriyaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Jithu Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Jithu Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Veththu Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Veththu Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthuttu Pogattumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthuttu Pogattumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Jillu Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Jillu Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Jakku Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Jakku Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravala Kavalai Illa Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravala Kavalai Illa Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru Kaattaadha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thimiru Kaattaadha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey Manasa Kozhapura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Manasa Kozhapura"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart-a Olapura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart-a Olapura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vennamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vennamadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimuru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimuru Kaattaadha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thimiru Kaattaadha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey Heart-a Kazhitti Vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Heart-a Kazhitti Vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichi Thovakkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichi Thovakkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-u Venaamdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-u Venaamdi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Colour Colour-ah Kanavu Kaanura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Colour Colour-ah Kanavu Kaanura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Illadi Kaamaatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Illadi Kaamaatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadar Kariyilla Kaadhala Valakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadar Kariyilla Kaadhala Valakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodu Illadi Meenaatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodu Illadi Meenaatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagal Irava Pesi Pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal Irava Pesi Pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bore-u Aadikkavum Venaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bore-u Aadikkavum Venaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhalum Kaadhal Aasaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhalum Kaadhal Aasaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelappi Vittava Needhaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelappi Vittava Needhaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love School-il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love School-il"/>
</div>
<div class="lyrico-lyrics-wrapper">LKG Sera Aasai Illadi Yenakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="LKG Sera Aasai Illadi Yenakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennai Serthu Pass Aakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennai Serthu Pass Aakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha Manasu Irukka Unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Manasu Irukka Unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Vaangaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Vaangaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart Inga Kedaayadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart Inga Kedaayadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Midhi Vaangaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhi Vaangaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Inga Kidayaadhu oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Inga Kidayaadhu oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhu Kedaikkum Nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu Kedaikkum Nu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind-u Ku Theriyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind-u Ku Theriyaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumaara Irukkum Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumaara Irukkum Pasanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi Thaan Puriyaadhu oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Thaan Puriyaadhu oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Jithu Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Jithu Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vetthu Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vetthu Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthuttu Pogattumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irunthuttu Pogattumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Jillu Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Jillu Thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Jakku Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Jakku Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravala Kavalai Illa Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravala Kavalai Illa Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru Kaattaadha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thimiru Kaattaadha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey Manasa Kozhapura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Manasa Kozhapura"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart-a Olapura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart-a Olapura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vennamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vennamadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru Kaattaadha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Thimiru Kaattaadha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey Girls Geels Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Girls Geels Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhiga Virumbaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiga Virumbaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandhi Naandhaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandhi Naandhaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru Kaattaadha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru Kaattaadha Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thimiru Kaattaadha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thimiru Kaattaadha Di"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thimiru Kaattaadha Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thimiru Kaattaadha Di"/>
</div>
</pre>
