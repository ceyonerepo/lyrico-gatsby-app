---
title: "nee siricha kondattam song lyrics"
album: "Thoonga Nagaram"
artist: "Sundar C Babu"
lyricist: "S Annamalai"
director: "Gaurav Narayanan"
path: "/albums/thoonga-nagaram-lyrics"
song: "Nee Siricha Kondattam"
image: ../../images/albumart/thoonga-nagaram.jpg
date: 2011-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IpNF1swQlZQ"
type: "happy"
singers:
  - Shankar Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee sirichaa kondaattam nee nadandhaa kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sirichaa kondaattam nee nadandhaa kondaattam "/>
</div>
<div class="lyrico-lyrics-wrapper">nee azhagaa kannadicha kondaattam kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee azhagaa kannadicha kondaattam kondaattam "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sirichaa kondaattam nee nadandhaa kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sirichaa kondaattam nee nadandhaa kondaattam "/>
</div>
<div class="lyrico-lyrics-wrapper">nee azhagaa kannadicha kondaattam kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee azhagaa kannadicha kondaattam kondaattam "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aavanimaasam aththapponnu thaavanippoattaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavanimaasam aththapponnu thaavanippoattaa "/>
</div>
<div class="lyrico-lyrics-wrapper">kondaattamthaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondaattamthaan "/>
</div>
<div class="lyrico-lyrics-wrapper">kaarthigai maasam kitta vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaarthigai maasam kitta vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">ava kaadhala sonnaa kondaattandhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava kaadhala sonnaa kondaattandhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">vazhukkil maratha oonivachchaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhukkil maratha oonivachchaa "/>
</div>
<div class="lyrico-lyrics-wrapper">vayashuppasanga kondaattandhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayashuppasanga kondaattandhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">sooda yeththum poosaarikki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sooda yeththum poosaarikki "/>
</div>
<div class="lyrico-lyrics-wrapper">kaasakkandaa kondaattandhaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasakkandaa kondaattandhaan "/>
</div>
<div class="lyrico-lyrics-wrapper">madhurakkaaran vaazhkkaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhurakkaaran vaazhkkaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">niththam niththam kondaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niththam niththam kondaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee sirichaa kondaattam nee nadandhaa kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sirichaa kondaattam nee nadandhaa kondaattam "/>
</div>
<div class="lyrico-lyrics-wrapper">nee azhagaa kannadicha kondaattam kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee azhagaa kannadicha kondaattam kondaattam "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sirichaa kondaattam nee nadandhaa kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sirichaa kondaattam nee nadandhaa kondaattam "/>
</div>
<div class="lyrico-lyrics-wrapper">nee azhagaa kannadicha kondaattam kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee azhagaa kannadicha kondaattam kondaattam "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalathalannu varaappaaru manjulada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalathalannu varaappaaru manjulada "/>
</div>
<div class="lyrico-lyrics-wrapper">ava theruviley nadandhuvarum yenjaladaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava theruviley nadandhuvarum yenjaladaa "/>
</div>
<div class="lyrico-lyrics-wrapper">malarazhagaa iva azhagaa ketpomada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malarazhagaa iva azhagaa ketpomada "/>
</div>
<div class="lyrico-lyrics-wrapper">badhil solvaaraa Salaman Pappaiyaada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="badhil solvaaraa Salaman Pappaiyaada "/>
</div>
<div class="lyrico-lyrics-wrapper">stikkar pottu neththiyil vachu oththaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="stikkar pottu neththiyil vachu oththaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">panjumittaai aasaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panjumittaai aasaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">ava poaraa ippo rettaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava poaraa ippo rettaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">vazhukkai manda maamanukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhukkai manda maamanukku "/>
</div>
<div class="lyrico-lyrics-wrapper">paakkettula seeppedhukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paakkettula seeppedhukku "/>
</div>
<div class="lyrico-lyrics-wrapper">mugathil poatta poudarukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugathil poatta poudarukku "/>
</div>
<div class="lyrico-lyrics-wrapper">mookkil yerum nedi namakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mookkil yerum nedi namakku "/>
</div>
<div class="lyrico-lyrics-wrapper">madhurakkaaran vaazhkkaiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhurakkaaran vaazhkkaiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">niththam niththam kondaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niththam niththam kondaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee sirichaa kondaattam nee nadandhaa kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sirichaa kondaattam nee nadandhaa kondaattam "/>
</div>
<div class="lyrico-lyrics-wrapper">nee azhagaa kannadicha kondaattam kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee azhagaa kannadicha kondaattam kondaattam "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sirichaa kondaattam nee nadandhaa kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sirichaa kondaattam nee nadandhaa kondaattam "/>
</div>
<div class="lyrico-lyrics-wrapper">nee azhagaa kannadicha kondaattam kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee azhagaa kannadicha kondaattam kondaattam "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye karansiyila iva mogaththa adikkalaandaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye karansiyila iva mogaththa adikkalaandaa "/>
</div>
<div class="lyrico-lyrics-wrapper">namma indhiyaavin pana madhippa osaththalaanda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma indhiyaavin pana madhippa osaththalaanda "/>
</div>
<div class="lyrico-lyrics-wrapper">therdhal sinnam ival mogamaai irukkalaanda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therdhal sinnam ival mogamaai irukkalaanda "/>
</div>
<div class="lyrico-lyrics-wrapper">inga endhakkatchi ninnaalum jeyikkalaandaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga endhakkatchi ninnaalum jeyikkalaandaa "/>
</div>
<div class="lyrico-lyrics-wrapper">brammanukkey koththnaaru velappoattu koduththanaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="brammanukkey koththnaaru velappoattu koduththanaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">kuttu kuttu koburaththa katti vachchaa romba joaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuttu kuttu koburaththa katti vachchaa romba joaru "/>
</div>
<div class="lyrico-lyrics-wrapper">gungumalli vaasam veesum thaara thappa melam pesum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gungumalli vaasam veesum thaara thappa melam pesum "/>
</div>
<div class="lyrico-lyrics-wrapper">vanna vaana saayam poosi saamiyellaam oora suththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanna vaana saayam poosi saamiyellaam oora suththum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee sirichaa kondaattam nee nadandhaa kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sirichaa kondaattam nee nadandhaa kondaattam "/>
</div>
<div class="lyrico-lyrics-wrapper">nee azhagaa kannadicha kondaattam kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee azhagaa kannadicha kondaattam kondaattam "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sirichaa kondaattam nee nadandhaa kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sirichaa kondaattam nee nadandhaa kondaattam "/>
</div>
<div class="lyrico-lyrics-wrapper">nee azhagaa kannadicha kondaattam kondaattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee azhagaa kannadicha kondaattam kondaattam "/>
</div>
</pre>
