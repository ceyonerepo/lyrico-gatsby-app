---
title: "kari kaadu dhane song lyrics"
album: "Adutha Saattai"
artist: "Justin Prabhakaran"
lyricist: "Thenmozhi Das"
director: "M. Anbazhagan"
path: "/albums/adutha-saattai-lyrics"
song: "Kari Kaadu Dhane"
image: ../../images/albumart/adutha-saattai.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ng7KN48sa0s"
type: "happy"
singers:
  - Sathyan Ilanko
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kari Kaadu Thanae Perarivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kari Kaadu Thanae Perarivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal Aazham Kannin Veralavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Aazham Kannin Veralavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Aattu Mantha Arivapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Aattu Mantha Arivapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Namba Ettu Kalvi Oralavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Ettu Kalvi Oralavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadai Pathaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai Pathaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadusandhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadusandhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Narpaadangal Ketkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narpaadangal Ketkumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedal Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedal Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaagam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaagam Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Pera Pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Pera Pothumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kari Kaadu Thanae Perarivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kari Kaadu Thanae Perarivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal Aazham Kannin Veralavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Aazham Kannin Veralavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Aattu Mantha Arivapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Aattu Mantha Arivapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Namba Ettu Kalvi Oralavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Ettu Kalvi Oralavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirukkovilil Paakkura Kalasathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirukkovilil Paakkura Kalasathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariviyal Ullathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariviyal Ullathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayam Illaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayam Illaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Paattan Moolai Puthaiyalada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattan Moolai Puthaiyalada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Man Paanaiyil Vetru Paguthiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man Paanaiyil Vetru Paguthiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanniya Thaangumada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanniya Thaangumada"/>
</div>
<div class="lyrico-lyrics-wrapper">Verumai Nallathada Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verumai Nallathada Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathiramaai Iruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathiramaai Iruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilamum Neerum Kaatrum Veyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilamum Neerum Kaatrum Veyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithamum Koorum Sethi Nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithamum Koorum Sethi Nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallikoodam Illaa Kaattukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikoodam Illaa Kaattukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Payanam Povomda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payanam Povomda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhai Ellaam Paadam Kidakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai Ellaam Paadam Kidakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padichittu Varuvomda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padichittu Varuvomda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillaamalae Boomi Suthum Anbula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillaamalae Boomi Suthum Anbula"/>
</div>
<div class="lyrico-lyrics-wrapper">Samaneethi Sollum Neethibathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samaneethi Sollum Neethibathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyarkaiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyarkaiyadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kari Kaadu Thanae Perarivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kari Kaadu Thanae Perarivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal Aazham Kannin Veralavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Aazham Kannin Veralavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Aattu Mantha Arivapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Aattu Mantha Arivapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Namba Ettu Kalvi Oralavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Ettu Kalvi Oralavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayakkaattula Paadura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayakkaattula Paadura "/>
</div>
<div class="lyrico-lyrics-wrapper">Paattula Paasam Ullathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattula Paasam Ullathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadam Ullathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam Ullathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Panpaadum Ullathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panpaadum Ullathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaikkodi Manithanin 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaikkodi Manithanin "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyilum Arasiyal Ullathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyilum Arasiyal Ullathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkarai Ullathada Nilamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkarai Ullathada Nilamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithiya Kadavulada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithiya Kadavulada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril Thodangi Kaatril Mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Thodangi Kaatril Mudiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkai Yaavum Kaatrin Paadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai Yaavum Kaatrin Paadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanavan Enbavan Muthukelumbaavaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanavan Enbavan Muthukelumbaavaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattin Udalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattin Udalukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhan Enbavan Raththam Aavaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhan Enbavan Raththam Aavaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkum Uyirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkum Uyirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollamalae Kaalam Munthum Nammala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamalae Kaalam Munthum Nammala"/>
</div>
<div class="lyrico-lyrics-wrapper">Athappurinji Nee Puththi Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athappurinji Nee Puththi Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhchikkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhchikkadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kari Kaadu Thanae Perarivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kari Kaadu Thanae Perarivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal Aazham Kannin Veralavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Aazham Kannin Veralavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Aattu Mantha Arivapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Aattu Mantha Arivapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Namba Ettu Kalvi Oralavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namba Ettu Kalvi Oralavu"/>
</div>
</pre>
