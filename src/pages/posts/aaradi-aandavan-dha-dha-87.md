---
title: "aaradi aandavan song lyrics"
album: "Dha Dha 87"
artist: "Leander Lee Marty"
lyricist: "Vijay Sri G"
director: "Vijay Sri G"
path: "/albums/dha-dha-87-lyrics"
song: "Aaradi Aandavan"
image: ../../images/albumart/dha-dha-87.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1r_6jDyKmfo"
type: "mass"
singers:
  - Naresh Iyer
  - Vijay Sri G
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaradi Aandavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradi Aandavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan aaruthala Raavanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan aaruthala Raavanan"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhir varum porada..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhir varum porada.."/>
</div>
<div class="lyrico-lyrics-wrapper">Kalathiil Edhirikke Emanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalathiil Edhirikke Emanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu dha Paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu dha Paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja venru dha paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja venru dha paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottaikkul naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottaikkul naanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini naatukkul puliyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini naatukkul puliyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aandacarukke annenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandacarukke annenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ive paandavarukke karnenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ive paandavarukke karnenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendiyavarkku dheivamda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendiyavarkku dheivamda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi vandhavarukku dharmanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi vandhavarukku dharmanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadai dhan thalavaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadai dhan thalavaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamo irumbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamo irumbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai dhan mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai dhan mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udai vaal pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udai vaal pesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un viral nuniyil naan vaazhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un viral nuniyil naan vaazhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ean pirappurimai Un iddakkottila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ean pirappurimai Un iddakkottila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sollil jaadhi thaitthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sollil jaadhi thaitthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En kayyil vilangaiyittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kayyil vilangaiyittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Allal pattai nirutthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allal pattai nirutthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un dhittam sattam udaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un dhittam sattam udaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Erippavan karaippavan vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erippavan karaippavan vera"/>
</div>
<div class="lyrico-lyrics-wrapper">Inak kalavaram ini kuura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inak kalavaram ini kuura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhai dhaan pudhidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhai dhaan pudhidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidham pazhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidham pazhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irumanam inayyia
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irumanam inayyia"/>
</div>
<div class="lyrico-lyrics-wrapper">Madha kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madha kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Inamanam earrkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inamanam earrkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Ean padai soozha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Ean padai soozha vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ean Thamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ean Thamizha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee aariyam dhravidam pesadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aariyam dhravidam pesadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ean inamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ean inamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee indhiyan enbadhai marakkadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee indhiyan enbadhai marakkadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pinnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pinnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaga Thamizhan uyirai unakkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaga Thamizhan uyirai unakkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaradi Aandavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradi Aandavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan aaruthala Raavanan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan aaruthala Raavanan"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhir varum porada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhir varum porada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalathil emanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalathil emanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu dha paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu dha paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja venru dha paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja venru dha paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidai tharum thamizhan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai tharum thamizhan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandavar aaluda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavar aaluda"/>
</div>
</pre>
