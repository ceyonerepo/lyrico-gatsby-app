---
title: "sutrum bhoomi song lyrics"
album: "Dum Dum Dum"
artist: "Karthik Raja"
lyricist: "Na. Muthukumar - Vaali - Pa. Vijay"
director: "Azhagam Perumal"
path: "/albums/dum-dum-dum-song-lyrics"
song: "Sutrum Bhoomi Sutrum"
image: ../../images/albumart/dum-dum-dum.jpg
date: 2001-04-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WLNUcJFfEwQ"
type: "happy"
singers:
  - Harini
  - Sri Madhumitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yelalangiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelalangiliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athiri Bathiri Chaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiri Bathiri Chaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kathiri Kathiri Kozhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kathiri Kathiri Kozhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhiri Endhiri Vaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhiri Endhiri Vaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mundhiri Mundhiri Chaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mundhiri Mundhiri Chaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillaalangadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Thillaalangadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Thillaalangadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Thillaalangadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Thillaalangadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Thillaalangadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Thillaalangadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutrum Bhoomi Sutrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutrum Bhoomi Sutrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan Sakkaram Theindhu Vidaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Sakkaram Theindhu Vidaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattam Parakkum Kayiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Parakkum Kayiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaanavil Aagi Vidaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaanavil Aagi Vidaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattai Paiyil Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattai Paiyil Ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikkuttaiyil Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikkuttaiyil Vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattalai Ittadhum Ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattalai Ittadhum Ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">E Thennai Marame Thennai Marama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Thennai Marame Thennai Marama"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambil Valaiyalgal Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambil Valaiyalgal Yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumanam Unakku Thaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumanam Unakku Thaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athiri Bathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiri Bathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiri Bathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiri Bathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Athiri Bathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Athiri Bathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Athiri Bathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Athiri Bathiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirithiri Bommai Kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirithiri Bommai Kaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thillaalangadi Mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thillaalangadi Mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sedhaarathai Paarthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedhaarathai Paarthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Thanga Theevu Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Thanga Theevu Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillaalangadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillelangadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillelangadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillaalangadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaam Poochi Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaam Poochi Parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnil Illai Kattanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnil Illai Kattanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhiyam Pathaayathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhiyam Pathaayathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhiyam Padaraathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhiyam Padaraathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjarai Petti Arai Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjarai Petti Arai Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukkul Araigal Vechirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkul Araigal Vechirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaigal Adhile Olichirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaigal Adhile Olichirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Alai Alaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Alaiyaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neruppin Kaal Mele Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppin Kaal Mele Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerin Kaal Keezhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerin Kaal Keezhe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaalgal Melaa Keezhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaalgal Melaa Keezhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Evar Solvaar Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evar Solvaar Inge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutrum Bhoomi Sutrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutrum Bhoomi Sutrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan Sakkaram Theindhu Vidaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Sakkaram Theindhu Vidaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattam Parakkum Kayiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Parakkum Kayiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaanavil Aagi Vidaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaanavil Aagi Vidaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athiri Bathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiri Bathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Athiri Bathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Athiri Bathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Athiri Bathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Athiri Bathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Athiri Bathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Athiri Bathiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai Naalu Manikku Paduppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Naalu Manikku Paduppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Udane Anju Manikku Muzhippaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udane Anju Manikku Muzhippaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa Aaru Thadavai Kulippaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa Aaru Thadavai Kulippaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu Ettu Peruvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu Ettu Peruvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akka Purushan Arai Purushan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka Purushan Arai Purushan"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakkalathi Naanum Irukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkalathi Naanum Irukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka Mattum Neeyum Thovaippe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka Mattum Neeyum Thovaippe"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Maaman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Maaman"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal Vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal Vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koduppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koduppaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal Iravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal Iravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkkaama Pakkathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkaama Pakkathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutrum Bhoomi Sutrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutrum Bhoomi Sutrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan Sakkaram Theindhu Vidaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Sakkaram Theindhu Vidaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattam Parakkum Kayiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Parakkum Kayiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaanavil Aagi Vidaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaanavil Aagi Vidaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattai Paiyil Ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattai Paiyil Ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikkuttaiyil Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikkuttaiyil Vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattalai Ittadhum Ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattalai Ittadhum Ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">E Thennai Marame Thennai Marama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Thennai Marame Thennai Marama"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambil Valaiyalgal Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambil Valaiyalgal Yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumanam Unakku Thaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumanam Unakku Thaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athiri Bathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiri Bathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Athiri Bathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athiri Bathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei Athiri Bathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Athiri Bathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Athiri Bathiri…heiiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Athiri Bathiri…heiiii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirithiri Bommai Kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirithiri Bommai Kaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thillaalangadi Mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thillaalangadi Mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sedhaarathai Paarthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedhaarathai Paarthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Thanga Theevu Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Thanga Theevu Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillaalangadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillelangadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillelangadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Thillaalangadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Thillaalangadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thillelangadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillelangadi"/>
</div>
</pre>
