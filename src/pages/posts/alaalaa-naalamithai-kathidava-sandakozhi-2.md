---
title: "alaalaa - naalamithai kathidava song lyrics"
album: "Sandakozhi 2"
artist: "Yuvan Sankar Raja"
lyricist: "Madhan Karky"
director: "N. Linguswamy"
path: "/albums/sandakozhi-2-lyrics"
song: "Alaalaa - Naalamithai Kathidava"
image: ../../images/albumart/sandakozhi-2.jpg
date: 2018-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ENX3MYaSrbk"
type: "Sad"
singers:
  - Kailash Kher
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naalamithai Kaathidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalamithai Kaathidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalam Athai Nee Kudithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalam Athai Nee Kudithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelamena Nenjam Ingey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelamena Nenjam Ingey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaagam Ena En Inaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaagam Ena En Inaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiyagam Ena En Inaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiyagam Ena En Inaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agam Athai Udaithinayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agam Athai Udaithinayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Umai Aval Imaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umai Aval Imaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanaiya Kandai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaiya Kandai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athuvunnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvunnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaiyathai Unakkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaiyathai Unakkul"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sumathi Kondai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumathi Kondai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Aalaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Aalaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalamithai Kaathidava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalamithai Kaathidava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalam Athai Nee Kudithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalam Athai Nee Kudithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelamena Nenjam Ingey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelamena Nenjam Ingey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaagam Ena En Inaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaagam Ena En Inaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiyagam Ena En Inaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiyagam Ena En Inaiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agam Athai Udaithinayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agam Athai Udaithinayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinnum Mannum Punnagayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnum Mannum Punnagayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulage Punnagayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulage Punnagayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmai Attra Punnagayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai Attra Punnagayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otrai Vizhi Paasa Vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai Vizhi Paasa Vizhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maru Vizhi Kaadhal Vizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Vizhi Kaadhal Vizhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhum Thuli Entha Thuli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhum Thuli Entha Thuli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalaalaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaalaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Moottam Panimoottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Moottam Panimoottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Ullaye Tholainthaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Ullaye Tholainthaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha Vedam Kodum Vedam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Vedam Kodum Vedam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inthu Yeno Aninthayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthu Yeno Aninthayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuraiyaa Kuraiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuraiyaa Kuraiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithave Vazhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithave Vazhiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruley Ozhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruley Ozhiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh Aalaalaa Aalaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh Aalaalaa Aalaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sol Ithu Nizhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sol Ithu Nizhaiya"/>
</div>
</pre>
