---
title: "cheliya maate song lyrics"
album: "Jodi"
artist: "Phani Kalyan"
lyricist: "Anantha Sriram"
director: "Vishwanath Arigella"
path: "/albums/jodi-lyrics"
song: "Cheliya Maate"
image: ../../images/albumart/jodi.jpg
date: 2019-09-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/xr-qhFcOgoU"
type: "love"
singers:
  - Haricharan
  - Sameera Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Cheliya Maate 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Maate "/>
</div>
<div class="lyrico-lyrics-wrapper">Chandanam Chandanamleee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandanam Chandanamleee"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliya Maate 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Maate "/>
</div>
<div class="lyrico-lyrics-wrapper">Chandanam Chandanamleee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandanam Chandanamleee"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliya Maate Chandanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Maate Chandanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliya Maate Chandanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Maate Chandanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliya Maate Chandanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Maate Chandanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliya Maate Chandanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Maate Chandanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Maatatho Edho Sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Maatatho Edho Sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Swaasalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Swaasalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhalaindheekshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalaindheekshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Chota Undalenanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Chota Undalenanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaakuthundi Gunde Ninginee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakuthundi Gunde Ninginee"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Malli Cheppamandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Malli Cheppamandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinna Maatanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinna Maatanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaalle Idhi Nakee Janmakee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaalle Idhi Nakee Janmakee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Rojaina Ae Rojukaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Rojaina Ae Rojukaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanathone Nenuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanathone Nenuntaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Maate Vintuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Maate Vintuntaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Rojaina Ae Rojukaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Rojaina Ae Rojukaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Maatatho Edho Sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Maatatho Edho Sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliya Maate 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Maate "/>
</div>
<div class="lyrico-lyrics-wrapper">Chandanam Chandanamleee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandanam Chandanamleee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kshanam Undalenu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kshanam Undalenu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thanani Choodaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanani Choodaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Nimisham Aagalenu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nimisham Aagalenu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kougilivvakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kougilivvakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Kalanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Kalanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam Chesedhaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Chesedhaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Nidraponu Ennadu Ikaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Nidraponu Ennadu Ikaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhamulu Nela Meedha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhamulu Nela Meedha "/>
</div>
<div class="lyrico-lyrics-wrapper">Mopae Mundhare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mopae Mundhare"/>
</div>
<div class="lyrico-lyrics-wrapper">Chethulanu Unchuthaanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethulanu Unchuthaanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Mosae Thondharai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosae Thondharai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Nalusu Nannu Dhaatakundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Nalusu Nannu Dhaatakundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanani Thaakalede Nedika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanani Thaakalede Nedika"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaaramulu Maayamai Ila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaaramulu Maayamai Ila "/>
</div>
<div class="lyrico-lyrics-wrapper">Thelikainaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelikainaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Meghamulapaina Haayiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meghamulapaina Haayiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Theluthunnaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theluthunnaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Swaasalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Swaasalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhalaindheekshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhalaindheekshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Chota Undalenanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Chota Undalenanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaakuthundi Gunde Ninginee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaakuthundi Gunde Ninginee"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Malli Cheppamandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Malli Cheppamandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinna Maatanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinna Maatanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliya Maate 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Maate "/>
</div>
<div class="lyrico-lyrics-wrapper">Chandanam Chandanamleee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandanam Chandanamleee"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheliya Maate 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheliya Maate "/>
</div>
<div class="lyrico-lyrics-wrapper">Chandanam Chandanamleee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandanam Chandanamleee"/>
</div>
</pre>
