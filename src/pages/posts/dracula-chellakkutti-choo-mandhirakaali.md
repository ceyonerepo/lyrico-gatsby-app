---
title: "dracula chellakkutti song lyrics"
album: "Choo Mandhirakaali"
artist: "A Sathish Raghunathan"
lyricist: "Gugai. Ma. Pugalendi"
director: "Eswar Kotravai"
path: "/albums/choo-mandhirakaali-lyrics"
song: "Dracula Chellakkutti"
image: ../../images/albumart/choo-mandhirakaali.jpg
date: 2021-09-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wCwrUOv8XPI"
type: "happy"
singers:
  - Sean Roldan
  - Manasi Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">dracula chellakutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dracula chellakutti"/>
</div>
<div class="lyrico-lyrics-wrapper">tharavaa pullakkutti nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharavaa pullakkutti nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">aaviyaa pinnikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaviyaa pinnikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">muzhusaa thinnupputtu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muzhusaa thinnupputtu paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaateriya seeraadhadee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaateriya seeraadhadee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">vedhaalama yeraadhadaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedhaalama yeraadhadaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">thokkadha aal dhandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thokkadha aal dhandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatatha poochandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatatha poochandi"/>
</div>
<div class="lyrico-lyrics-wrapper">pei kadhal seivom vaayendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pei kadhal seivom vaayendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dracula chellakutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dracula chellakutti"/>
</div>
<div class="lyrico-lyrics-wrapper">tharavaa pullakkutti nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharavaa pullakkutti nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">aaviyaa pinnikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaviyaa pinnikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">muzhusaa thinnupputtu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muzhusaa thinnupputtu paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yevaadha kaadhal boodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yevaadha kaadhal boodham"/>
</div>
<div class="lyrico-lyrics-wrapper">yedhedho vedham odhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedhedho vedham odhum"/>
</div>
<div class="lyrico-lyrics-wrapper">poondhaada unna enna kekum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poondhaada unna enna kekum"/>
</div>
<div class="lyrico-lyrics-wrapper">theeraama oorum kaamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeraama oorum kaamam"/>
</div>
<div class="lyrico-lyrics-wrapper">saathana pola maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathana pola maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">oyaama mutham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyaama mutham "/>
</div>
<div class="lyrico-lyrics-wrapper">thandhu thaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandhu thaakum"/>
</div>
<div class="lyrico-lyrics-wrapper">padharadikkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padharadikkum "/>
</div>
<div class="lyrico-lyrics-wrapper">baangaana oorvashi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baangaana oorvashi"/>
</div>
<div class="lyrico-lyrics-wrapper">kola nadunga godhaavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kola nadunga godhaavil"/>
</div>
<div class="lyrico-lyrics-wrapper">koluthum raatchasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koluthum raatchasi"/>
</div>
<div class="lyrico-lyrics-wrapper">raavodu raavaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raavodu raavaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">oor koodi peyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor koodi peyadu"/>
</div>
<div class="lyrico-lyrics-wrapper">perinba vaasal pongum yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perinba vaasal pongum yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">choo mandhirakaali podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choo mandhirakaali podu"/>
</div>
<div class="lyrico-lyrics-wrapper">showkaali aattam aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="showkaali aattam aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">paakaadha baagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paakaadha baagam"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">ezhezhu jenman thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ezhezhu jenman thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">sendhale zombie jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sendhale zombie jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">nogaama nonga thinbom vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nogaama nonga thinbom vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">alaparaiya yethama vethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaparaiya yethama vethura"/>
</div>
<div class="lyrico-lyrics-wrapper">orambaraiya pols nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orambaraiya pols nee"/>
</div>
<div class="lyrico-lyrics-wrapper">odhungi pasapura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odhungi pasapura"/>
</div>
<div class="lyrico-lyrics-wrapper">peyodu sendhache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peyodu sendhache"/>
</div>
<div class="lyrico-lyrics-wrapper">melogam keezhaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="melogam keezhaache"/>
</div>
<div class="lyrico-lyrics-wrapper">yammaadi andhar bulti aache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yammaadi andhar bulti aache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dracula chellakutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dracula chellakutti"/>
</div>
<div class="lyrico-lyrics-wrapper">tharavaa pullakkutti nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharavaa pullakkutti nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">aaviyaa pinnikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaviyaa pinnikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">muzhusaa thinnupputtu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muzhusaa thinnupputtu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kaateriya seeraadhadee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaateriya seeraadhadee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">vedhaalama yeraadhadaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedhaalama yeraadhadaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">thokkadha aal dhandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thokkadha aal dhandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatatha poochandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatatha poochandi"/>
</div>
<div class="lyrico-lyrics-wrapper">pei kadhal seivom vaayendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pei kadhal seivom vaayendi"/>
</div>
</pre>
