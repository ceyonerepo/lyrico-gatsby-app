---
title: "onnavitta yaarum yenakilla song lyrics"
album: "Seemaraja"
artist: "D Imman"
lyricist: "Yugabharathi"
director: "Ponram"
path: "/albums/seemaraja-lyrics"
song: "Onnavitta Yaarum Yenakilla"
image: ../../images/albumart/seemaraja.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DE7wF8KfWy8"
type: "love"
singers:
  - Sathyaprakash
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Onna vitta yaarum enakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna vitta yaarum enakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kanden naanum unakkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kanden naanum unakkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna vitta yaarum enakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna vitta yaarum enakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kanden naanum unakkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kanden naanum unakkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravaga neeyum sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaga neeyum sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Usurula veesum soorakaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurula veesum soorakaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala nooru kodi aandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala nooru kodi aandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelavula podavenum koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelavula podavenum koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyae kootta thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae kootta thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthu vaa veliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthu vaa veliyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyila aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyila aaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna vitta yaarum enakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna vitta yaarum enakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kanden naanum unakkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kanden naanum unakkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna vitta yaarum enakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna vitta yaarum enakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kanden naanum unakkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kanden naanum unakkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaga neeyum sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaga neeyum sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Usurula veesum soorakaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurula veesum soorakaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala nooru kodi aandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala nooru kodi aandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelavula podavenum koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelavula podavenum koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyae kootta thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae kootta thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthu vaa veliyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthu vaa veliyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyila aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyila aaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam…nee vanthu nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam…nee vanthu nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallapadi vidiyumae vidiyumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallapadi vidiyumae vidiyumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi…un kannukulla sonnapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi…un kannukulla sonnapadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sozhalumae sozhalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sozhalumae sozhalumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anthi pagal yedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthi pagal yedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna maranthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna maranthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaiyum pesaPathalaiyae naalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaiyum pesaPathalaiyae naalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasae thaangama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasae thaangama"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un madiyil thoongaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un madiyil thoongaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyil mani osai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyil mani osai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedham ketpen rendu vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedham ketpen rendu vizhiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna vitta yaarum enakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna vitta yaarum enakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kanden naanum unakkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kanden naanum unakkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna vitta yaarum enakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna vitta yaarum enakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kanden naanum unakkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kanden naanum unakkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nekka nee kann asaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nekka nee kann asaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandapadi methakkuren methakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandapadi methakkuren methakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatha naan ulla vanthu onna saera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha naan ulla vanthu onna saera"/>
</div>
<div class="lyrico-lyrics-wrapper">Edukkuren edukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukkuren edukkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa nodi neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa nodi neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli irunthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli irunthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna iva moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna iva moodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyiduven melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyiduven melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalae kaanjalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalae kaanjalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu malaiyum saanjalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu malaiyum saanjalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaappen onna naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaappen onna naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangathae kannumaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangathae kannumaniyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna vitta yaarum enakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna vitta yaarum enakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kanden naanum unakkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kanden naanum unakkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna vitta yaarum enakilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna vitta yaarum enakilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai kanden naanum unakkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai kanden naanum unakkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaaa aaa"/>
</div>
</pre>
