---
title: "kattam potta satta song lyrics"
album: "Ondikatta"
artist: "Bharani"
lyricist: "Bharani"
director: "Bharani"
path: "/albums/ondikatta-lyrics"
song: "Kattam Potta Satta"
image: ../../images/albumart/ondikatta.jpg
date: 2018-07-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lVM8XhzUxXk"
type: "love"
singers:
  - Sriram Parthasarathy
  - Priya Yamesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kattam potta satta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattam potta satta"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaiya ethuku thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaiya ethuku thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">kovam kanna mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovam kanna mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kattu katha katha katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kattu katha katha katha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">katta semma katta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katta semma katta"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pathu puten kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pathu puten kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa vanthu mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa vanthu mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">un pinnala naan sutha sutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pinnala naan sutha sutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adada ithu kathal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adada ithu kathal "/>
</div>
<div class="lyrico-lyrics-wrapper">athanaal vantha mothal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athanaal vantha mothal"/>
</div>
<div class="lyrico-lyrics-wrapper">un mutham puthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mutham puthu "/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukulla thudipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukulla thudipen"/>
</div>
<div class="lyrico-lyrics-wrapper">oru puthagatha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru puthagatha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">unna padipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna padipen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kattam potta satta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattam potta satta"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaiya ethuku thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaiya ethuku thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">kovam kanna mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovam kanna mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kattu katha katha katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kattu katha katha katha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai thedi adi unnai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedi adi unnai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">naan oorai sutri alainthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan oorai sutri alainthene"/>
</div>
<div class="lyrico-lyrics-wrapper">ammmaadi adi ammammadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammmaadi adi ammammadi"/>
</div>
<div class="lyrico-lyrics-wrapper">un ullangaiyil kidaithene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un ullangaiyil kidaithene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nethiyila vacha pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethiyila vacha pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan viralaa kathukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan viralaa kathukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan pechu kannan kulalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan pechu kannan kulalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poovukulla vandhu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovukulla vandhu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">thenai karakkum anbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenai karakkum anbu"/>
</div>
<div class="lyrico-lyrics-wrapper">endra vaarthaikoru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endra vaarthaikoru "/>
</div>
<div class="lyrico-lyrics-wrapper">artham pirakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="artham pirakum"/>
</div>
<div class="lyrico-lyrics-wrapper">moodiya kathavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moodiya kathavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">muluvathum thirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muluvathum thirakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kattam potta satta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattam potta satta"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaiya ethuku thotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaiya ethuku thotta"/>
</div>
<div class="lyrico-lyrics-wrapper">kovam kanna mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovam kanna mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kattu katha katha katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kattu katha katha katha"/>
</div>
</pre>
