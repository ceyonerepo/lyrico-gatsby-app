---
title: "moonu moonu moonu song lyrics"
album: "3 33"
artist: "Harshavardhan Rameshwar"
lyricist: "Asal Kolaar - Dharmaseelan Udaiyappan"
director: "Nambikkai Chandru"
path: "/albums/3-33-lyrics"
song: "Moonu Moonu Moonu"
image: ../../images/albumart/3-33.jpg
date: 2021-12-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2foT-H2Pbdk"
type: "happy"
singers:
  - G.V. Prakash Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy wanted-ahh vandha virundhali poola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy wanted-ahh vandha virundhali poola"/>
</div>
<div class="lyrico-lyrics-wrapper">Torture-ahh pandhu ennai pudcha peeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Torture-ahh pandhu ennai pudcha peeda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy wanted-ahh vandha virundhali pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy wanted-ahh vandha virundhali pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Torture-ahh pandhu ennai pudcha peeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Torture-ahh pandhu ennai pudcha peeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna paavam senjen theriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna paavam senjen theriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavuren dhinamum timing maaraama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavuren dhinamum timing maaraama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy raavugalam yemagandam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy raavugalam yemagandam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhellam summakachi ullalaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhellam summakachi ullalaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonum moonum serum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonum moonum serum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouser-ehh kaltidum vandha peiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouser-ehh kaltidum vandha peiyu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenda porantheno indha time laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda porantheno indha time laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Polambaa kooda ippa time illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polambaa kooda ippa time illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenda porantheno indha time laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda porantheno indha time laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Polambaa kooda ippa time illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polambaa kooda ippa time illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cartoon pola enn kadhaya paarthu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cartoon pola enn kadhaya paarthu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandavan entertain aaguran daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavan entertain aaguran daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru kannu big boss-u vootula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru kannu big boss-u vootula"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakadha kannellam paarkuthuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakadha kannellam paarkuthuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy raavugalam yemagandam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy raavugalam yemagandam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhellam summakachi ullalaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhellam summakachi ullalaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonum moonum serum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonum moonum serum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Trouser-ehh kaltidum vandha peiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trouser-ehh kaltidum vandha peiyu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenda porantheno indha time laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda porantheno indha time laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Polamba kooda ippa time illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamba kooda ippa time illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenda porantheno indha time laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda porantheno indha time laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Polamba kooda ippa time illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamba kooda ippa time illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu moonu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu moonu"/>
</div>
</pre>
