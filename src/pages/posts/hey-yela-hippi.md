---
title: "hey yela song lyrics"
album: "Hippi"
artist: "Nivas K. Prasanna"
lyricist: "Anantha Sriram"
director: "Krishna"
path: "/albums/hippi-lyrics"
song: "Hey Yela"
image: ../../images/albumart/hippi.jpg
date: 2019-06-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/57j3d5rVIm4"
type: "love"
singers:
  - Sathya Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey yelaa etepu vellinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey yelaa etepu vellinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atepu nee kalaa vastaandi tokalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atepu nee kalaa vastaandi tokalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey yelaa etepu choosinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey yelaa etepu choosinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atepu nee valaa untaandi tovalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atepu nee valaa untaandi tovalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chi chi ani chiraakupadda vennelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chi chi ani chiraakupadda vennelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Po po ani pattesukoke nannilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po po ani pattesukoke nannilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Melipadaku teegalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melipadaku teegalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey yelaa etepu vellinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey yelaa etepu vellinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atepu nee kalaa vastaandi tokalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atepu nee kalaa vastaandi tokalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey yelaa etepu choosinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey yelaa etepu choosinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atepu nee valaa untaandi tovalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atepu nee valaa untaandi tovalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Once Upon A Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Once Upon A Time"/>
</div>
<div class="lyrico-lyrics-wrapper">Too Long Ago
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too Long Ago"/>
</div>
<div class="lyrico-lyrics-wrapper">The First Time 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The First Time "/>
</div>
<div class="lyrico-lyrics-wrapper">I Saw You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Saw You"/>
</div>
<div class="lyrico-lyrics-wrapper">So Beautiful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Beautiful"/>
</div>
<div class="lyrico-lyrics-wrapper">I Do Believe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Do Believe"/>
</div>
<div class="lyrico-lyrics-wrapper">You Were Meant For Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Were Meant For Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Up On The Bike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Up On The Bike"/>
</div>
<div class="lyrico-lyrics-wrapper">And Let’S Run The Streets
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And Let’S Run The Streets"/>
</div>
<div class="lyrico-lyrics-wrapper">I Always Will Wait For You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Always Will Wait For You"/>
</div>
<div class="lyrico-lyrics-wrapper">Even Take Care Of You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Even Take Care Of You"/>
</div>
<div class="lyrico-lyrics-wrapper">Right There
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right There"/>
</div>
<div class="lyrico-lyrics-wrapper">Beside Of You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beside Of You"/>
</div>
<div class="lyrico-lyrics-wrapper">I Will Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Will Be"/>
</div>
<div class="lyrico-lyrics-wrapper">I’M Seeing The Truth
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’M Seeing The Truth"/>
</div>
<div class="lyrico-lyrics-wrapper">Are You Feeling It Too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are You Feeling It Too"/>
</div>
<div class="lyrico-lyrics-wrapper">You’Re Following Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You’Re Following Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Am I Following You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Am I Following You"/>
</div>
<div class="lyrico-lyrics-wrapper">I’M Freaking Like A Hippy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’M Freaking Like A Hippy"/>
</div>
<div class="lyrico-lyrics-wrapper">But Your Eyes Are Making Me Trippy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Your Eyes Are Making Me Trippy"/>
</div>
<div class="lyrico-lyrics-wrapper">I’M Freaking Like A Hippy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’M Freaking Like A Hippy"/>
</div>
<div class="lyrico-lyrics-wrapper">I Love You Do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Love You Do"/>
</div>
<div class="lyrico-lyrics-wrapper">Do You Love Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do You Love Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeku nenu needano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku nenu needano"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaku nuvvu needavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku nuvvu needavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa sagam nuvo nuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa sagam nuvo nuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sagam nijamgaa neno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sagam nijamgaa neno"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye balam ivvaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye balam ivvaalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vasam avaalanantu unnado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vasam avaalanantu unnado"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey deniko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey deniko"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey deniko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey deniko"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho telusuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho telusuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaa ilaa elaagolaa oppuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaa ilaa elaagolaa oppuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey yelaa etepu vellinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey yelaa etepu vellinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atepu nee kalaa vastaandi tokalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atepu nee kalaa vastaandi tokalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey yelaa etepu choosinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey yelaa etepu choosinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atepu nee valaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atepu nee valaa"/>
</div>
<div class="lyrico-lyrics-wrapper">untaandi tovalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="untaandi tovalaa"/>
</div>
</pre>
