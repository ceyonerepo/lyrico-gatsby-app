---
title: "ayile ayile song lyrics"
album: "Kalavani 2"
artist: "Mani Amudhavan"
lyricist: "Mani Amuthavan"
director: "A. Sarkunam"
path: "/albums/kalavani-2-lyrics"
song: "Ayile Ayile"
image: ../../images/albumart/kalavani-2.jpg
date: 2019-07-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9F_Xi3gNOnE"
type: "happy"
singers:
  - Mani Amuthavan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ayile Ayile Ayile Ayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayile Ayile Ayile Ayile"/>
</div>
<div class="lyrico-lyrics-wrapper">En Oyile Oyile Oyile Oyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Oyile Oyile Oyile Oyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayile Ayile Ayile Ayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayile Ayile Ayile Ayile"/>
</div>
<div class="lyrico-lyrics-wrapper">En Oyile Oyile Oyile Oyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Oyile Oyile Oyile Oyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mena Mena Mena Mena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mena Mena Mena Mena"/>
</div>
<div class="lyrico-lyrics-wrapper">Mena Minukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mena Minukki"/>
</div>
<div class="lyrico-lyrics-wrapper">Current Ah Pachuriyae Pachuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Current Ah Pachuriyae Pachuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Muzhikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Muzhikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mena Mena Mena Mena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mena Mena Mena Mena"/>
</div>
<div class="lyrico-lyrics-wrapper">Mena Minukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mena Minukki"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga Vangikiren Vangikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Vangikiren Vangikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Velakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Velakki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Anthaangura Inthaangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Anthaangura Inthaangura"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhaangura Ponthaangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhaangura Ponthaangura"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Anthaangura Inthaangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Anthaangura Inthaangura"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhaangura Ponthaangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhaangura Ponthaangura"/>
</div>
<div class="lyrico-lyrics-wrapper">Varengura Vallengura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varengura Vallengura"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Ninna Illengura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Ninna Illengura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Anthaangura Inthaangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Anthaangura Inthaangura"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaiyum Kondaangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaiyum Kondaangura"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Andaangura Kundaangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Andaangura Kundaangura"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaiyum Kondaangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaiyum Kondaangura"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarengura Thallengura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarengura Thallengura"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalli Konjam Nillengura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli Konjam Nillengura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayile Ayile Ayile Ayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayile Ayile Ayile Ayile"/>
</div>
<div class="lyrico-lyrics-wrapper">En Oyile Oyile Oyile Oyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Oyile Oyile Oyile Oyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayile Ayile Ayile Ayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayile Ayile Ayile Ayile"/>
</div>
<div class="lyrico-lyrics-wrapper">En Oyile Oyile Oyile Oyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Oyile Oyile Oyile Oyile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mena Mena Mena Mena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mena Mena Mena Mena"/>
</div>
<div class="lyrico-lyrics-wrapper">Mena Minukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mena Minukki"/>
</div>
<div class="lyrico-lyrics-wrapper">Current Ah Pachuriyae Pachuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Current Ah Pachuriyae Pachuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Muzhikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Muzhikki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mena Mena Mena Mena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mena Mena Mena Mena"/>
</div>
<div class="lyrico-lyrics-wrapper">Mena Minukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mena Minukki"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga Vangikiren Vangikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Vangikiren Vangikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha Velakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha Velakki"/>
</div>
</pre>
