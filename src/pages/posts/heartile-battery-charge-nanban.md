---
title: "heartile battery charge song lyrics"
album: "Nanban"
artist: "Harris Jayaraj"
lyricist: "Na Muthu Kumar"
director: "S. Shankar"
path: "/albums/nanban-lyrics"
song: "Heartile Battery Charge"
image: ../../images/albumart/nanban.jpg
date: 2012-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ff2COtpuMXI"
type: "Motivational"
singers:
  - Hemachandra
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Heartile Battery
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartile Battery"/>
</div>
<div class="lyrico-lyrics-wrapper">Chargu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chargu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhviya Tensiona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhviya Tensiona"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollidu"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heartiley Battery
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartiley Battery"/>
</div>
<div class="lyrico-lyrics-wrapper">Charge Ju Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Charge Ju Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhviya Tensiona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhviya Tensiona"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollidu"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tightaaga Life Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tightaaga Life Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Loose Aaga Nee Maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loose Aaga Nee Maaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vavvaalai Pole Nee Vaazhndhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vavvaalai Pole Nee Vaazhndhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Enghum Thongum Thottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Enghum Thongum Thottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Canteen Vadayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Canteen Vadayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Noolai Kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolai Kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Noolai Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolai Kondu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattam Viduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Viduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maasa Kadaisiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasa Kadaisiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kingsum Theerndhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kingsum Theerndhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundu Beediyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundu Beediyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpai Korpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpai Korpom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heartile Battery
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartile Battery"/>
</div>
<div class="lyrico-lyrics-wrapper">Chargu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chargu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhviya Tensiona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhviya Tensiona"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollidu"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tightaaga Life Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tightaaga Life Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Loose Aaga Nee Maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loose Aaga Nee Maaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vavvaalai Pole Nee Vaazhndhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vavvaalai Pole Nee Vaazhndhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Enghum Thongum Thottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Enghum Thongum Thottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Canteen Vadayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Canteen Vadayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Noolai Kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolai Kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Noolai Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolai Kondu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattam Viduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Viduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maasa Kadaisiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasa Kadaisiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kingsum Theerndhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kingsum Theerndhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundu Beediyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundu Beediyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpai Korpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpai Korpom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yele Vella Sokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yele Vella Sokka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil Enna Bookah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil Enna Bookah"/>
</div>
<div class="lyrico-lyrics-wrapper">Educated Cook Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Educated Cook Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene Podadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scene Podadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yele Makka Makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yele Makka Makka"/>
</div>
<div class="lyrico-lyrics-wrapper">Maggaa Adikumm Maanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maggaa Adikumm Maanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththa Kaalu Kokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Kaalu Kokka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Marka Kothi Nogaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Marka Kothi Nogaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochu Muttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Muttum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padam Ellam Soda Goliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padam Ellam Soda Goliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Load Mela Load Yetha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Load Mela Load Yetha"/>
</div>
<div class="lyrico-lyrics-wrapper">Moola Lorrya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moola Lorrya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moolaiyathaan Moota Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolaiyathaan Moota Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Your
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Your"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartu Beatu Rootu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartu Beatu Rootu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Topper Enbathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Topper Enbathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Hero Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Topic Maarinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Topic Maarinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanum Zero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanum Zero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Joker Enbathaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joker Enbathaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Zero Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zero Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Seatu Kattile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seatu Kattile"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Hero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heartile Battery
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartile Battery"/>
</div>
<div class="lyrico-lyrics-wrapper">Chargu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chargu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhviya Tensiona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhviya Tensiona"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollidu"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Kooda Bramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Kooda Bramma"/>
</div>
<div class="lyrico-lyrics-wrapper">Table Deskah Dammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Table Deskah Dammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathi Kuthu Gumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathi Kuthu Gumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Tune Podamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tune Podamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Students Enna Yamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Students Enna Yamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Cell Kulla Sim Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cell Kulla Sim Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooti Vaikalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooti Vaikalaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam Escape Aagi Povoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Escape Aagi Povoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bathroom Thaapal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathroom Thaapal"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Endral"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatu Paadenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatu Paadenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru TMS Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru TMS Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Jesudaasah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jesudaasah"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavom Vaayenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavom Vaayenda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moolaiyathaan Mootta Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolaiyathaan Mootta Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Follow Your
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Follow Your"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartu Beatu Rootu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartu Beatu Rootu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bathroomu Kul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathroomu Kul"/>
</div>
<div class="lyrico-lyrics-wrapper">Paambum Vanthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paambum Vanthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Thervil Vaangiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thervil Vaangiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttai Neetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttai Neetu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beeru Adichuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beeru Adichuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoppai Pottaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoppai Pottaal"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Aagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Aagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Police Aettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police Aettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heartile Battery
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartile Battery"/>
</div>
<div class="lyrico-lyrics-wrapper">Chargu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chargu Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhviya Tensiona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhviya Tensiona"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollidu"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tightaaga Life Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tightaaga Life Aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Loose Aaga Nee Maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loose Aaga Nee Maaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vavvaalai Pole Nee Vaazhndhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vavvaalai Pole Nee Vaazhndhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Enghum Thongum Thottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Enghum Thongum Thottam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Canteen Vadayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Canteen Vadayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Noolai Kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolai Kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Noolai Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolai Kondu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattam Viduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Viduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maasa Kadaisiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasa Kadaisiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kingsum Theerndhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kingsum Theerndhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundu Beediyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundu Beediyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpai Korpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpai Korpom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Canteen Vadayile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Canteen Vadayile"/>
</div>
<div class="lyrico-lyrics-wrapper">Noolai Kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolai Kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Noolai Kondu Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolai Kondu Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattam Viduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattam Viduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maasa Kadaisiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maasa Kadaisiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kingsum Theerndhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kingsum Theerndhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">All Is Well
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Is Well"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundu Beediyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundu Beediyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpai Korpom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpai Korpom"/>
</div>
</pre>
