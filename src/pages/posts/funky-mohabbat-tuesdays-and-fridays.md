---
title: "funky mohabbat song lyrics"
album: "Tuesdays And Fridays"
artist: "Tony Kakkar"
lyricist: "Kumaar"
director: "Taranveer Singh"
path: "/albums/tuesdays-and-fridays-lyrics"
song: "Funky Mohabbat"
image: ../../images/albumart/tuesdays-and-fridays.jpg
date: 2021-02-19
lang: hindi
youtubeLink: "https://www.youtube.com/embed/aMIbH27QMqI"
type: "happy"
singers:
  - Shreya Ghoshal
  - Sonu Kakkar
  - Benny Dayal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ho teri meri yeh mohabbat funky badi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho teri meri yeh mohabbat funky badi"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri meri yeh mohabbat funky badi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri meri yeh mohabbat funky badi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho duniya karne na deti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho duniya karne na deti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho duniya karne na deti sanki badi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho duniya karne na deti sanki badi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teri meri yeh mohabbat funky badi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri meri yeh mohabbat funky badi"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri meri yeh mohabbat funky badi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri meri yeh mohabbat funky badi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Funk! Funk! Funk! Funk!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Funk! Funk! Funk! Funk!"/>
</div>
<div class="lyrico-lyrics-wrapper">Funk! Funk! Funky badi funk!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Funk! Funk! Funky badi funk!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Back seat pe meri rover ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Back seat pe meri rover ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Lagega sixer har over pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagega sixer har over pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaag ke sab party karte hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaag ke sab party karte hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Main kar leta hun so karke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main kar leta hun so karke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jalve meri power ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalve meri power ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Hamle jaise babar ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hamle jaise babar ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Film dikha dun tujhe ghooma dun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Film dikha dun tujhe ghooma dun"/>
</div>
<div class="lyrico-lyrics-wrapper">Chadh jaana tv tower pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadh jaana tv tower pe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kab tak daba ke rakhun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kab tak daba ke rakhun"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil wale pain ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil wale pain ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhootne na dungi main to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhootne na dungi main to"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar wali train ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar wali train ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoye kab tak daba ke rakhun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoye kab tak daba ke rakhun"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil wale pain ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil wale pain ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhootne na dungi main to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhootne na dungi main to"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyar wali train ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyar wali train ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ok tata bye bye main to chali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok tata bye bye main to chali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ok tata bye bye main to chali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok tata bye bye main to chali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho duniya karne na deti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho duniya karne na deti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho duniya karne na deti sanki badi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho duniya karne na deti sanki badi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Funk! Funk! Funk! Funk!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Funk! Funk! Funk! Funk!"/>
</div>
<div class="lyrico-lyrics-wrapper">Funk! Funk! Funky oh teri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Funk! Funk! Funky oh teri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Umar 18 teri feeling bhi nahi wrong hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umar 18 teri feeling bhi nahi wrong hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Time pe hi gaana jo yeh love wala song hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time pe hi gaana jo yeh love wala song hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho umar 18 teri feeling bhi nahi wrong hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho umar 18 teri feeling bhi nahi wrong hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Time pe hi gaana jo yeh love wala song hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time pe hi gaana jo yeh love wala song hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoda wait to kar don't do hadbadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda wait to kar don't do hadbadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda wait to kar don't do hadbadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda wait to kar don't do hadbadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Zara soch le samajh le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zara soch le samajh le"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho zara soch le samajh le umar hai padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho zara soch le samajh le umar hai padi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Funk! Funk! Funk! Funk!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Funk! Funk! Funk! Funk!"/>
</div>
<div class="lyrico-lyrics-wrapper">Funk! Funk! Funky oh teri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Funk! Funk! Funky oh teri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Funk! Funk! Funk! Funk!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Funk! Funk! Funk! Funk!"/>
</div>
<div class="lyrico-lyrics-wrapper">Funk! Funk! Funky funk funk oh teri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Funk! Funk! Funky funk funk oh teri"/>
</div>
</pre>
