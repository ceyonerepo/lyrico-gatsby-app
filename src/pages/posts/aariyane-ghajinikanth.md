---
title: "aariyane song lyrics"
album: "Ghajinikanth"
artist: "Balamurali Balu"
lyricist: "Kabilan Vairamuthu"
director: "Santhosh P. Jayakumar"
path: "/albums/ghajinikanth-lyrics"
song: "Aariyane"
image: ../../images/albumart/ghajinikanth.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7Refb0A6ZCE"
type: "love"
singers:
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aariyanae pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aariyanae pala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayangal purinthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayangal purinthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaveriyae en kanavodu virinthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaveriyae en kanavodu virinthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan sirippaal en nenjai koithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan sirippaal en nenjai koithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aariyanae pala maayangal purinthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aariyanae pala maayangal purinthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhnilai thuyilodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhnilai thuyilodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanangal thodarnthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanangal thodarnthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Margazhi veyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Margazhi veyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">En maarbodu padarnthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En maarbodu padarnthaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalin vazhiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalin vazhiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaasalil nindrenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaasalil nindrenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanniyar soozhthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanniyar soozhthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannanai velvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannanai velvenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavul athu neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul athu neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En penmai thirakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En penmai thirakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavuchollum neethanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavuchollum neethanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaa aaa aaaaaa Aaaaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaa aaaaaa Aaaaaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Papani pamaripa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papani pamaripa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamapa maripama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamapa maripama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ririma rimanisaaaaaaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ririma rimanisaaaaaaaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Papani papani pama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papani papani pama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamapa mamapa mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamapa mamapa mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ririma ririma risa sarimamapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ririma ririma risa sarimamapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rimapa rimapa ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rimapa rimapa ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Mapani mapani sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mapani mapani sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Panisa panisa re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panisa panisa re"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisari nisari ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisari nisari ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Mapa panisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mapa panisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ripadhani sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ripadhani sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarimarisa risadhanipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarimarisa risadhanipa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aariyanae pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aariyanae pala"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayangal purinthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayangal purinthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan sirippaal en nenjai koithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan sirippaal en nenjai koithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aariyanae pala maayangal purinthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aariyanae pala maayangal purinthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa"/>
</div>
</pre>
