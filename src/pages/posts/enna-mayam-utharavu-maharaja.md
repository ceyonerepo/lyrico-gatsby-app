---
title: "enna mayam song lyrics"
album: "Utharavu Maharaja"
artist: "Naren Balakumar"
lyricist: "Asif Kuraishi"
director: "Asif Kuraishi"
path: "/albums/utharavu-maharaja-lyrics"
song: "Enna Mayam"
image: ../../images/albumart/utharavu-maharaja.jpg
date: 2018-11-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/p63_qww0R7U"
type: "love"
singers:
  - Nivas
  - Padmaja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">enna maayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna maayam "/>
</div>
<div class="lyrico-lyrics-wrapper">seithu vittai etho etho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seithu vittai etho etho"/>
</div>
<div class="lyrico-lyrics-wrapper">ennil vithaithu vittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennil vithaithu vittai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam kadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam kadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">gnanam thanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gnanam thanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">kulir kaatril mithakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulir kaatril mithakum"/>
</div>
<div class="lyrico-lyrics-wrapper">unarvu aliththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unarvu aliththai"/>
</div>
<div class="lyrico-lyrics-wrapper">putham puthithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="putham puthithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai pirakka vaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai pirakka vaithai"/>
</div>
<div class="lyrico-lyrics-wrapper">kallam illa anbaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallam illa anbaal"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai ventru vittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai ventru vittai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna maayam maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna maayam maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">maayam seithaai penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayam seithaai penne"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan sogam thanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan sogam thanai"/>
</div>
<div class="lyrico-lyrics-wrapper">maatri vittai penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatri vittai penne"/>
</div>
<div class="lyrico-lyrics-wrapper">enna maayam maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna maayam maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">maayam seithaai penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayam seithaai penne"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan sogam thanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan sogam thanai"/>
</div>
<div class="lyrico-lyrics-wrapper">maatri vittai penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatri vittai penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaaradi nee yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaradi nee yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">nam uravukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam uravukku "/>
</div>
<div class="lyrico-lyrics-wrapper">enna peradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna peradi"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaradi nee yaaradi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaradi nee yaaradi "/>
</div>
<div class="lyrico-lyrics-wrapper">nee yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuvarai vaazhaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuvarai vaazhaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">nee yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhkaiyai vaazha seithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhkaiyai vaazha seithai"/>
</div>
<div class="lyrico-lyrics-wrapper">nam uravukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam uravukku"/>
</div>
<div class="lyrico-lyrics-wrapper">ithayam muzhuvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayam muzhuvathum"/>
</div>
<div class="lyrico-lyrics-wrapper">enna peradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna peradi"/>
</div>
<div class="lyrico-lyrics-wrapper">neeye thaan viyaapithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeye thaan viyaapithu"/>
</div>
<div class="lyrico-lyrics-wrapper">irukindrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukindrai"/>
</div>
</pre>
