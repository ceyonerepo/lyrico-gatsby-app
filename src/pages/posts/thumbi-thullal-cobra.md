---
title: 'thumbi thullal song lyrics'
album: 'Cobra'
artist: 'A R Rahman'
lyricist: 'Vivek'
director: 'G Vasanthabalan'
path: '/albums/cobra-song-lyrics'
song: 'Thumbi Thullal'
image: ../../images/albumart/cobra.jpg
date: 2020-06-29
lang: tamil
singers: 
- Nakul Abhyankar
- Shreya Ghoshal
youtubeLink: "https://www.youtube.com/embed/Dj6DPX53Vfk"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">  
<div class="lyrico-lyrics-wrapper">Sadugudu sadugudu di di
<input type="checkbox" class="lyrico-select-lyric-line" value="Sadugudu sadugudu di di"/>
</div>
<div class="lyrico-lyrics-wrapper">Di di dama dam dam
<input type="checkbox" class="lyrico-select-lyric-line" value="Di di dama dam dam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadugudu di di di di dam dam
<input type="checkbox" class="lyrico-select-lyric-line" value="Sadugudu di di di di dam dam"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum sindhoora singaara kinnaara
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjum sindhoora singaara kinnaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponthendralaai vannu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponthendralaai vannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhu manamo madhumanamo madhumanmo
<input type="checkbox" class="lyrico-select-lyric-line" value="Madhu manamo madhumanamo madhumanmo"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhu manamoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Madhu manamoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thumbi thumbi thumbi thullalo
<input type="checkbox" class="lyrico-select-lyric-line" value="Thumbi thumbi thumbi thullalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Indro indro thumbi thullalo
<input type="checkbox" class="lyrico-select-lyric-line" value="Indro indro thumbi thullalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh hooo jala jal jala jal maniyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh hooo jala jal jala jal maniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanidam idai irangidu maniyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Avanidam idai irangidu maniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyae thaniyae thaniyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniyae thaniyae thaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan ulaginil naan mattum thaniyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan ulaginil naan mattum thaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En kalla chiripin neelam neeyae
<input type="checkbox" class="lyrico-select-lyric-line" value="En kalla chiripin neelam neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thumbi thumbi thumbi thullalo
<input type="checkbox" class="lyrico-select-lyric-line" value="Thumbi thumbi thumbi thullalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh indro indro thumbi thullalo
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh indro indro thumbi thullalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sadugudu sadugudu di di
<input type="checkbox" class="lyrico-select-lyric-line" value="Sadugudu sadugudu di di"/>
</div>
<div class="lyrico-lyrics-wrapper">Di di dama dam dam
<input type="checkbox" class="lyrico-select-lyric-line" value="Di di dama dam dam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadugudu di di di di dam dam
<input type="checkbox" class="lyrico-select-lyric-line" value="Sadugudu di di di di dam dam"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum sindhoora singaara kinaara
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjum sindhoora singaara kinaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponthendralaai vannu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponthendralaai vannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kan thoongum neraththil
<input type="checkbox" class="lyrico-select-lyric-line" value="Kan thoongum neraththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee neenga koodathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee neenga koodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathoram un moochin thee vendumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaathoram un moochin thee vendumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodum ellamae theeyena thee maatrumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thodum ellamae theeyena thee maatrumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu pol theenda nee yena naan venduvaen
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhu pol theenda nee yena naan venduvaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thumbi thumbi thumbi thullalo
<input type="checkbox" class="lyrico-select-lyric-line" value="Thumbi thumbi thumbi thullalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh …. indro indro thumbi thullalo
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh …. indro indro thumbi thullalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaaranangal yedhum theriyaamal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaaranangal yedhum theriyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkal poga kandenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naatkal poga kandenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnidam vandhen andha nodiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnidam vandhen andha nodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr artham serthanthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Orr artham serthanthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru ganam kooda vilagaamal uyiraavaen
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru ganam kooda vilagaamal uyiraavaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhi varai kaigal naluvaamal yaendhuven
<input type="checkbox" class="lyrico-select-lyric-line" value="Irudhi varai kaigal naluvaamal yaendhuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Aaan azhagan kaalu namma pakkam
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaan azhagan kaalu namma pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu mattum perazhagi pakkam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannu mattum perazhagi pakkam"/>
</div>
   <div class="lyrico-lyrics-wrapper">Namma satham kaadhil vila villaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma satham kaadhil vila villaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikkum beerangi kondu varanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vedikkum beerangi kondu varanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
   <div class="lyrico-lyrics-wrapper">Neenga thara venum nooru pulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Neenga thara venum nooru pulla"/>
</div>
   <div class="lyrico-lyrics-wrapper">Aiyyo… podhavillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Aiyyo… podhavillai"/>
</div>
   <div class="lyrico-lyrics-wrapper">Unga poruppil namma
<input type="checkbox" class="lyrico-select-lyric-line" value="Unga poruppil namma"/>
</div>
<div class="lyrico-lyrics-wrapper">Janathogai irukkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Janathogai irukkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thumbi thumbi thumbi thullalo
<input type="checkbox" class="lyrico-select-lyric-line" value="Thumbi thumbi thumbi thullalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Indro indro thumbi thullalo
<input type="checkbox" class="lyrico-select-lyric-line" value="Indro indro thumbi thullalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh hooo jal jal jala jal maniyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh hooo jal jal jala jal maniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanidam idai irangidu maniyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Avanidam idai irangidu maniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyae thaniyae thaniyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniyae thaniyae thaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan ulaginil naan mattum thaniyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan ulaginil naan mattum thaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En kalla chiripin neelam neeyae
<input type="checkbox" class="lyrico-select-lyric-line" value="En kalla chiripin neelam neeyae"/>
</div>
</pre>