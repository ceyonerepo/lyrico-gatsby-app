---
title: "kalavathi kalavathi song lyrics"
album: "Palasa 1978"
artist: "Raghu Kunche"
lyricist: "Suddala Ashok Teja"
director: "Karuna Kumar"
path: "/albums/palasa-1978-lyrics"
song: "Kalavathi Kalavathi"
image: ../../images/albumart/palasa-1978.jpg
date: 2020-03-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/hcCuNv3wPp4"
type: "happy"
singers:
  - Ramya Behara
  - Raghu Kunche
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kalavathi Kalavathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavathi Kalavathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavathi Vaddhe Vaddhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavathi Vaddhe Vaddhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavathi Kalavathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavathi Kalavathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavathi Vaddhe Vaddhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavathi Vaddhe Vaddhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoyy Hoyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyy Hoyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Middesukunna Dhaanni Sootthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Middesukunna Dhaanni Sootthavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chunnesukunna Dheenni Sootthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chunnesukunna Dheenni Sootthavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Middesukunna Dhaanni Sootthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Middesukunna Dhaanni Sootthavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chunnesukunna Dheenni Sootthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chunnesukunna Dheenni Sootthavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Middi Ledhu Chunni Ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Middi Ledhu Chunni Ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Potta Nuvvu Soodakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta Nuvvu Soodakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddaanavettukunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddaanavettukunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Sootthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Sootthavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaanni Sootthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaanni Sootthavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheenni Sootthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheenni Sootthavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kannulalla Mannupoyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannulalla Mannupoyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Sootthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Sootthavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaanni Sootthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaanni Sootthavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheenni Sootthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheenni Sootthavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kannulalla Mannupoyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannulalla Mannupoyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne Sootthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne Sootthavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Middesukunna Dhaanni Sootthano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Middesukunna Dhaanni Sootthano"/>
</div>
<div class="lyrico-lyrics-wrapper">Chunnesukunna Dheenni Sootthano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chunnesukunna Dheenni Sootthano"/>
</div>
<div class="lyrico-lyrics-wrapper">Middesukunna Dhaanni Sootthano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Middesukunna Dhaanni Sootthano"/>
</div>
<div class="lyrico-lyrics-wrapper">Chunnesukunna Dheenni Sootthano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chunnesukunna Dheenni Sootthano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaanni Sootthano Dheenni Sootthano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaanni Sootthano Dheenni Sootthano"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddaanavettukunna Ninnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddaanavettukunna Ninnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Koodaa Vadhilepettane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodaa Vadhilepettane"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyy Oyy Oyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyy Oyy Oyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saakiletu Laagunna Dhaanni Thintavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakiletu Laagunna Dhaanni Thintavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice Fruit Laagunna Dheenni Thintavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice Fruit Laagunna Dheenni Thintavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Saakiletu Laagunna Dhaanni Thintavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakiletu Laagunna Dhaanni Thintavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice Fruit Laagunna Dheenni Thintavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice Fruit Laagunna Dheenni Thintavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ice Ledhu Geesu Ledhu Vayasantha Karagabosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice Ledhu Geesu Ledhu Vayasantha Karagabosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennepoosa Laagunna Nannu Thintavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennepoosa Laagunna Nannu Thintavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaanni Thintavo Dheenni Thintavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaanni Thintavo Dheenni Thintavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaaramollu Paadugaanu Nannu Thintavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaaramollu Paadugaanu Nannu Thintavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavathi Kalavathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavathi Kalavathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavathi Vathi Vathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavathi Vathi Vathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saakiletu Laagunna Dhaanni Thintano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakiletu Laagunna Dhaanni Thintano"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice Fruit Laagunna Dheenni Thintano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice Fruit Laagunna Dheenni Thintano"/>
</div>
<div class="lyrico-lyrics-wrapper">Saakiletu Laagunna Dhaanni Thintano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakiletu Laagunna Dhaanni Thintano"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice Fruit Laagunna Dheenni Thintano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice Fruit Laagunna Dheenni Thintano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaanni Thintano Dheenni Thintano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaanni Thintano Dheenni Thintano"/>
</div>
<div class="lyrico-lyrics-wrapper">Enne Poosa Laagunna Ninnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enne Poosa Laagunna Ninnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda Mingesthane Ellehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Mingesthane Ellehe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Rammesukochhinaadi Dhaanikisthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Rammesukochhinaadi Dhaanikisthavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Bubble Gummesukochhinaadi Dheenikisthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubble Gummesukochhinaadi Dheenikisthavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rammu Ledhu Gammu Ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rammu Ledhu Gammu Ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thammalapaaku Namili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thammalapaaku Namili"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenneeti Pedhavulunna Naakisthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenneeti Pedhavulunna Naakisthavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaanikistthavo Dheenikitthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaanikistthavo Dheenikitthavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pedhavulalla Thene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pedhavulalla Thene "/>
</div>
<div class="lyrico-lyrics-wrapper">Puyya Naaku Itthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyya Naaku Itthavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaanikistthavo Dheenikitthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaanikistthavo Dheenikitthavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Pedhavulalla Thene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Pedhavulalla Thene "/>
</div>
<div class="lyrico-lyrics-wrapper">Puyya Naaku Itthavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyya Naaku Itthavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rammesukochhinaadi Dhaanikisthano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rammesukochhinaadi Dhaanikisthano"/>
</div>
<div class="lyrico-lyrics-wrapper">Bubble Gummesukochhinaadi Dheenikisthano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubble Gummesukochhinaadi Dheenikisthano"/>
</div>
<div class="lyrico-lyrics-wrapper">Rammesukochhinaadi Dhaanikisthano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rammesukochhinaadi Dhaanikisthano"/>
</div>
<div class="lyrico-lyrics-wrapper">Bubble Gummesukochhinaadi Dheenikisthano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bubble Gummesukochhinaadi Dheenikisthano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaanikitthano Dheenikitthano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaanikitthano Dheenikitthano"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenneeru Pedhavulanna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenneeru Pedhavulanna "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Kooda Ichhetthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Kooda Ichhetthaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavathi Kalavathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavathi Kalavathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavathi Vathi Vathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavathi Vathi Vathi"/>
</div>
</pre>
