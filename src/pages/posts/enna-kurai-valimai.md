---
title: "enna kurai song lyrics"
album: "Valimai"
artist: "Yuvan Shankar Raja"
lyricist: "Thamarai"
director: "H. Vinoth"
path: "/albums/valimai-lyrics"
song: "Enna Kurai"
image: ../../images/albumart/valimai.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ooVHXSZl0PY"
type: "sad"
singers:
  - Nandini Srikar
  - Sriram Parthasarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enna kurai naan vaithen kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kurai naan vaithen kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna ninaithu valarthen unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna ninaithu valarthen unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda kanavum veenaai pogumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda kanavum veenaai pogumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna pizhai naan seithen kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna pizhai naan seithen kanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai pilinthe eduthen unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai pilinthe eduthen unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanda kanavum veenaai pogumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda kanavum veenaai pogumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga valayal thampai maarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga valayal thampai maarumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottil kuzhanthai thookil aaduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottil kuzhanthai thookil aaduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannai thanthe vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannai thanthe vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pole yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pole yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaavilum kanden illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaavilum kanden illaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadal thoonkum un aazham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal thoonkum un aazham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedum vaanin neelam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedum vaanin neelam "/>
</div>
<div class="lyrico-lyrics-wrapper">ellam sernthum konjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam sernthum konjame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi neerai sintha koodathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi neerai sintha koodathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai veedu entrum thaankathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai veedu entrum thaankathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha theeyai aatruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha theeyai aatruven"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerai ootruven paathai maatruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerai ootruven paathai maatruven"/>
</div>
</pre>
