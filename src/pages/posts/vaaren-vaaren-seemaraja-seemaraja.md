---
title: "vaaren vaaren seemaraja song lyrics"
album: "Seemaraja"
artist: "D Imman"
lyricist: "Yugabharathi"
director: "Ponram"
path: "/albums/seemaraja-lyrics"
song: "Vaaren Vaaren Seemaraja"
image: ../../images/albumart/seemaraja.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Sv3dyJz19O0"
type: "intro"
singers:
  - Diwakar
  - Kavitha Gopi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nerappana nellaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerappana nellaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poruppana mannanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poruppana mannanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorai sirappaga vaithidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorai sirappaga vaithidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyalaatrum mannanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyalaatrum mannanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappilatha manasu konda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappilatha manasu konda"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizh nadu singamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamizh nadu singamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan appan kaasa alli eraikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan appan kaasa alli eraikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Avatharicha thangamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avatharicha thangamada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaren vaaren seemaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaren vaaren seemaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiya vidungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiya vidungada"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vesamilla paasakkaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vesamilla paasakkaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesanthaan nambugadahey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesanthaan nambugadahey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaren vaaren seemaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaren vaaren seemaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiya vidungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiya vidungada"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vesamilla paasakkaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vesamilla paasakkaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesanthaan nambugada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesanthaan nambugada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suyanalamae pothunalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suyanalamae pothunalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethuvenae kodiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethuvenae kodiya"/>
</div>
<div class="lyrico-lyrics-wrapper">En pothunalamum suyanalanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pothunalamum suyanalanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukko en padaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukko en padaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En perai kekka koodum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En perai kekka koodum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Arangu arangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arangu arangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ezhaiketha aeroplane-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ezhaiketha aeroplane-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Othungu othungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othungu othungu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En perai kekka koodum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En perai kekka koodum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Arangu arangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arangu arangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ezhaiketha aeroplane-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ezhaiketha aeroplane-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Othungu othungu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othungu othungu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaren vaaren seemaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaren vaaren seemaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiya vidungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiya vidungada"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vesamilla paasakkaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vesamilla paasakkaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesanthaan nambugada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesanthaan nambugada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Potti pottu anbu senji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potti pottu anbu senji"/>
</div>
<div class="lyrico-lyrics-wrapper">Likes-eh allum en saettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Likes-eh allum en saettai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nokkam pola karuthum solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nokkam pola karuthum solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi poven paaratta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi poven paaratta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vivasayi illaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivasayi illaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduppula sorum pongaathuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduppula sorum pongaathuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Periyorgal sonnalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periyorgal sonnalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaiyae naanum solvenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaiyae naanum solvenada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh oorellaam empadatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh oorellaam empadatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Potta aagum Trend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta aagum Trend-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaalilla pattampoochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaalilla pattampoochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellarikkum Friend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarikkum Friend-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethiriyavae irunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiriyavae irunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avana mathikka pazhaganunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana mathikka pazhaganunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Valathu kaiyi kodukurathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valathu kaiyi kodukurathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idathu kaikkum theriyanunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idathu kaikkum theriyanunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaren vaaren seemaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaren vaaren seemaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiya vidungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiya vidungada"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vesamilla paasakkaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vesamilla paasakkaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesanthaan nambugada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesanthaan nambugada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumthalakka gumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumthalakka gumma"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumthalakalaa gumthalakalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumthalakalaa gumthalakalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaala vaara nooru peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala vaara nooru peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Safe-ah neeyum game aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Safe-ah neeyum game aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooki vidathaan kodi peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooki vidathaan kodi peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuninji poyi poraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninji poyi poraadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panam kaasa serthu vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam kaasa serthu vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugar-u BP yerummada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugar-u BP yerummada"/>
</div>
<div class="lyrico-lyrics-wrapper">One by two tea kudicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One by two tea kudicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu thaan taste-u kelungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu thaan taste-u kelungada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan veerappa kattiputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan veerappa kattiputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthathillai dimikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthathillai dimikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar jalrava thattayilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar jalrava thattayilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasipene amukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasipene amukki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Degree vaangi kuvichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Degree vaangi kuvichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamizhu namathu thani peruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamizhu namathu thani peruma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aracha maava arachalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aracha maava arachalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athukkum venum oru theramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athukkum venum oru theramma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaren vaaren seemaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaren vaaren seemaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiya vidungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiya vidungada"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vesamilla paasakkaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vesamilla paasakkaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesanthaan nambugada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesanthaan nambugada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pam parampa rampampam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pam parampa rampampam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pam parampa rampampam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pam parampa rampampam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pam parampa rampampam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pam parampa rampampam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pam parampa rampampam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pam parampa rampampam"/>
</div>
</pre>
