---
title: "aagasam lyrics"
album: "Soorarai Pottru"
artist: "G V Prakash Kumar"
lyricist: "Arunraja Kamaraj"
director: "Sudha Kongara"
path: "/albums/soorarai-pottru-song-lyrics"
song: "Aagasam"
image: ../../images/albumart/soorarai-potru.jpg
date: 2020-11-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BXvSY8tNS60"
type: "motivational"
singers:
  - Christin Jos
  - Govind Vasantha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Thaanay thana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaanay thana naanay"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana naanay naana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Naana naanay naana naanay"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanay thana naanay naana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaanay thana naanay naana naanay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanay thana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaanay thana naanay"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana naanay naana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Naana naanay naana naanay"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanay thana naanay naana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaanay thana naanay naana naanay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ye aagasam eppodhum podhuthaan da inga
<input type="checkbox" class="lyrico-select-lyric-line" value="Ye aagasam eppodhum podhuthaan da inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada avagaasam vaangi adha pudipoma da
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada avagaasam vaangi adha pudipoma da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vesham podu koottam adha marappom da konjam
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee vesham podu koottam adha marappom da konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irumbaala rekka senji ini parappoma da
<input type="checkbox" class="lyrico-select-lyric-line" value="Irumbaala rekka senji ini parappoma da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti pasanga vela idhunnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti pasanga vela idhunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluravanga veththu payaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Solluravanga veththu payaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettri vandha vatti mudhaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Vettri vandha vatti mudhaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu ilipaainga echa payaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandhu ilipaainga echa payaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti pasanga vela idhunnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti pasanga vela idhunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluravanga veththu payaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Solluravanga veththu payaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettri vandha vatti mudhaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Vettri vandha vatti mudhaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu ilipaainga echa payaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Vandhu ilipaainga echa payaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ingu irukkuda vaanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Ingu irukkuda vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha etti pudikka vaa naalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Adha etti pudikka vaa naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga edhukkuda dhooram
<input type="checkbox" class="lyrico-select-lyric-line" value="Enga edhukkuda dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta poga poga adhu maarum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kitta poga poga adhu maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thannae thana thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae thana thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana thaanae thana thaane
<input type="checkbox" class="lyrico-select-lyric-line" value="Thana thaanae thana thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanava perusaavae vedhapomae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanava perusaavae vedhapomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannae thana thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannae thana thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana thaanae thana thaane
<input type="checkbox" class="lyrico-select-lyric-line" value="Thana thaanae thana thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigal irundhaalum theripomae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thadaigal irundhaalum theripomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nilava dhenam thorathipona
<input type="checkbox" class="lyrico-select-lyric-line" value="Nilava dhenam thorathipona"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu illa pangaali
<input type="checkbox" class="lyrico-select-lyric-line" value="Vayasu illa pangaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanava ini thorathipona
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanava ini thorathipona"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavala illa kootaali
<input type="checkbox" class="lyrico-select-lyric-line" value="Kavala illa kootaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thoththa kooda parava illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thoththa kooda parava illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulgam unna marakaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulgam unna marakaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikka mattum paartha
<input type="checkbox" class="lyrico-select-lyric-line" value="Vedikka mattum paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Veththa kooda madhikaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Veththa kooda madhikaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetri yedhum paakaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetri yedhum paakaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Porattamaa irundhaakaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Porattamaa irundhaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholvi yedhum theriyaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Tholvi yedhum theriyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondattamaa maaraadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Kondattamaa maaraadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unna pola yaaralum
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna pola yaaralum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna vazha mudiyaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna vazha mudiyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanban kooda ninnaaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanban kooda ninnaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna veztha mudiyaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna veztha mudiyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ingu irukkuda vaanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Ingu irukkuda vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha etti pudikka vaa naalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Adha etti pudikka vaa naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga edhukkuda dhooram
<input type="checkbox" class="lyrico-select-lyric-line" value="Enga edhukkuda dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta poga poga adhu maarum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kitta poga poga adhu maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaanay thana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaanay thana naanay"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana naanay naana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Naana naanay naana naanay"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanay thana naanay naana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaanay thana naanay naana naanay"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanay thana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaanay thana naanay"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana naanay naana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Naana naanay naana naanay"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanay thana naanay naana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaanay thana naanay naana naanay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaanay thana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaanay thana naanay"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana naanay naana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Naana naanay naana naanay"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanay thana naanay naana naanay
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaanay thana naanay naana naanay"/>
</div>
<div class="lyrico-lyrics-wrapper">Irumbaala rekka senji konjam parappomae naame
<input type="checkbox" class="lyrico-select-lyric-line" value="Irumbaala rekka senji konjam parappomae naame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey para para para para para para para para
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey para para para para para para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee para para para para para para para para
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee para para para para para para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee para para para para para para para para
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee para para para para para para para para"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee para para para para para para para para
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee para para para para para para para para"/>
</div>
</pre>
