---
title: "dasu bindasu song lyrics"
album: "Falaknuma Das"
artist: "Vivek Sagar"
lyricist: "Bhaskarbhatla Ravikumar"
director: "Vishwak Sen"
path: "/albums/falaknuma-das-lyrics"
song: "Dasu Bindasu"
image: ../../images/albumart/falaknuma-das.jpg
date: 2019-05-31
lang: telugu
youtubeLink: "https://www.youtube.com/embed/4BDbBDwSUE0"
type: "happy"
singers:
  - Kailash Kher
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Whistlelu Kotti Soodu Raajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whistlelu Kotti Soodu Raajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogipotadi Bandu Baajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogipotadi Bandu Baajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Whistlelu Kotti Soodu Raajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whistlelu Kotti Soodu Raajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogipotadi Bandu Baajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogipotadi Bandu Baajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Firstu Gearulona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Firstu Gearulona "/>
</div>
<div class="lyrico-lyrics-wrapper">Bandi Dookinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandi Dookinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Dookutondira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Dookutondira"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu Patangila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu Patangila "/>
</div>
<div class="lyrico-lyrics-wrapper">Ringu Ringuluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ringu Ringuluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindagi Tirugutondira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindagi Tirugutondira"/>
</div>
<div class="lyrico-lyrics-wrapper">Collarette Time Vachesindira Brotheru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Collarette Time Vachesindira Brotheru"/>
</div>
<div class="lyrico-lyrics-wrapper">Davatichi Chillaipomandira Wheatheru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Davatichi Chillaipomandira Wheatheru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Jindagi Chaala Chinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Jindagi Chaala Chinnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Geluputo Jeevinchamande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Geluputo Jeevinchamande"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kalalu Veela Ashalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalalu Veela Ashalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerchey Antunnnade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerchey Antunnnade"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey Dasu Bindasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Dasu Bindasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Settaipoyave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Settaipoyave"/>
</div>
<div class="lyrico-lyrics-wrapper">Dasu A Classu Mastunnave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dasu A Classu Mastunnave"/>
</div>
<div class="lyrico-lyrics-wrapper">Ey Kaka O Kaka Nuv Hittaipoyave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Kaka O Kaka Nuv Hittaipoyave"/>
</div>
<div class="lyrico-lyrics-wrapper">Keka Nuv Keka Pettinchave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keka Nuv Keka Pettinchave"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijayamante Valapu Lantide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijayamante Valapu Lantide"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu Padite Vadalanantade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Padite Vadalanantade"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Kalasi Vache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Kalasi Vache"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranam Egirivache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranam Egirivache"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Kalasi Vache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Kalasi Vache"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pranam Egirivache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pranam Egirivache"/>
</div>
<div class="lyrico-lyrics-wrapper">Muchchata Ika Modale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muchchata Ika Modale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whistlelu Kotti Soodu Raajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whistlelu Kotti Soodu Raajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogipotadi Bandu Baajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogipotadi Bandu Baajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Firstu Gearulona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Firstu Gearulona "/>
</div>
<div class="lyrico-lyrics-wrapper">Bandi Dookinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandi Dookinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Dookutondira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Dookutondira"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu Patangila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu Patangila "/>
</div>
<div class="lyrico-lyrics-wrapper">Ringu Ringuluga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ringu Ringuluga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindagi Tirugutondira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindagi Tirugutondira"/>
</div>
<div class="lyrico-lyrics-wrapper">Collarette Time Vachesindira Brotheru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Collarette Time Vachesindira Brotheru"/>
</div>
<div class="lyrico-lyrics-wrapper">Davatichi Chillaipomandira Wheatheru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Davatichi Chillaipomandira Wheatheru"/>
</div>
<div class="lyrico-lyrics-wrapper">Collarette Time Vachesindira Brotheru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Collarette Time Vachesindira Brotheru"/>
</div>
<div class="lyrico-lyrics-wrapper">Davatichi Chillaipomandira Wheatheru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Davatichi Chillaipomandira Wheatheru"/>
</div>
</pre>
