---
title: "innum sila naatkalil song lyrics"
album: "Vellai Pookal"
artist: "Ramgopal Krishnaraju"
lyricist: "Madhan Karky"
director: "Vivek Elangovan"
path: "/albums/vellai-pookal-lyrics"
song: "Innum Sila Naatkalil"
image: ../../images/albumart/vellai-pookal.jpg
date: 2019-04-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0nz0jcjqen4"
type: "happy"
singers:
  - Sarath Santosh
  - Ariv Adiaman
  - Pratap
  - Kiran Sravan
  - Deepika Parthasarathy
  - Jothi Lingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m In The Evergreen State
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m In The Evergreen State"/>
</div>
<div class="lyrico-lyrics-wrapper">Not Try Na Hate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Not Try Na Hate"/>
</div>
<div class="lyrico-lyrics-wrapper">But I Need A Break Today
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But I Need A Break Today"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamilnadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamilnadu"/>
</div>
<div class="lyrico-lyrics-wrapper">I Got Too Used To It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Got Too Used To It"/>
</div>
<div class="lyrico-lyrics-wrapper">CNN Wanna Watch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="CNN Wanna Watch"/>
</div>
<div class="lyrico-lyrics-wrapper">Sun News A Bit
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sun News A Bit"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamilnadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamilnadu"/>
</div>
<div class="lyrico-lyrics-wrapper">I Got Too Used To It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Got Too Used To It"/>
</div>
<div class="lyrico-lyrics-wrapper">CNN Wanna Watch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="CNN Wanna Watch"/>
</div>
<div class="lyrico-lyrics-wrapper">Sun News A Bit
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sun News A Bit"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Sila Naatkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Sila Naatkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Mugangal Pazhagividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Mugangal Pazhagividum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Sila Naatkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Sila Naatkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Saalaigal Vilangividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Saalaigal Vilangividum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idharkku Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idharkku Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaraa Unarvugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaraa Unarvugal"/>
</div>
<div class="lyrico-lyrics-wrapper">En Naavukku Pidithuvidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Naavukku Pidithuvidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anniyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anniyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endra Mugamoodi Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endra Mugamoodi Naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mugam Vittu Kazhandru Vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mugam Vittu Kazhandru Vidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Sila Naatkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Sila Naatkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Mugangal Pazhagividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Mugangal Pazhagividum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Sila Naatkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Sila Naatkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Saalaigal Vilangividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Saalaigal Vilangividum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">In A New Place
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In A New Place"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Really Feeling Trapped
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Really Feeling Trapped"/>
</div>
<div class="lyrico-lyrics-wrapper">Missing Old Things And
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Missing Old Things And"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m Really Need It Back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m Really Need It Back"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anna Tower To Space Needle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anna Tower To Space Needle"/>
</div>
<div class="lyrico-lyrics-wrapper">That’s A Real Quick Change
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That’s A Real Quick Change"/>
</div>
<div class="lyrico-lyrics-wrapper">And I’ve Seen It Fast
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And I’ve Seen It Fast"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saalai Ora Theneer Kadaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai Ora Theneer Kadaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Nera Nanbar Padaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Nera Nanbar Padaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaagana Nerisal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaagana Nerisal"/>
</div>
<div class="lyrico-lyrics-wrapper">Horn Oligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horn Oligal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkam Kollum Kaadhal Kiligal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkam Kollum Kaadhal Kiligal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suvarotti Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suvarotti Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivar Padam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivar Padam"/>
</div>
<div class="lyrico-lyrics-wrapper">Metro Lorry Pin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Metro Lorry Pin"/>
</div>
<div class="lyrico-lyrics-wrapper">Negizhi Kudam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Negizhi Kudam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suvarotti Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suvarotti Engum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivar Padam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivar Padam"/>
</div>
<div class="lyrico-lyrics-wrapper">Metro Lorry Pin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Metro Lorry Pin"/>
</div>
<div class="lyrico-lyrics-wrapper">Negizhi Kudam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Negizhi Kudam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethuvum Inge Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvum Inge Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaal Adhu Thaan Thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaal Adhu Thaan Thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Pazhagividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Pazhagividum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Sila Naatkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Sila Naatkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Mugangal Pazhagividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Mugangal Pazhagividum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Sila Naatkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Sila Naatkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Saalaigal Vilangividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Saalaigal Vilangividum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Life Is A Movie
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Life Is A Movie"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m The Writer Producer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m The Writer Producer"/>
</div>
<div class="lyrico-lyrics-wrapper">Director And The Main Actor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Director And The Main Actor"/>
</div>
<div class="lyrico-lyrics-wrapper">And That’s Plain Facts Bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And That’s Plain Facts Bro"/>
</div>
<div class="lyrico-lyrics-wrapper">What’s Natural
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What’s Natural"/>
</div>
<div class="lyrico-lyrics-wrapper">Something That Can’t Be Changed
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Something That Can’t Be Changed"/>
</div>
<div class="lyrico-lyrics-wrapper">I See No Point Going Against The Grain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I See No Point Going Against The Grain"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoosi Illaa Kaatrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoosi Illaa Kaatrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naasi Yetru Kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naasi Yetru Kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam Illaa Sorkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Illaa Sorkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Seviyum Yetru Kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seviyum Yetru Kollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoosi Illaa Kaatrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoosi Illaa Kaatrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naasi Yetru Kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naasi Yetru Kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam Illaa Sorkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Illaa Sorkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Seviyum Yetru Kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seviyum Yetru Kollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thinathanthi Padikkaa Kaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinathanthi Padikkaa Kaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kisukisu Illaa Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kisukisu Illaa Velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Pazhagividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Pazhagividum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Sila Naatkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Sila Naatkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Mugangal Pazhagividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Mugangal Pazhagividum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Sila Naatkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Sila Naatkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Saalaigal Vilangividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Saalaigal Vilangividum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavulum Madhamum Veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulum Madhamum Veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendhudhal Engum Ondre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendhudhal Engum Ondre"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasin Madhippu Veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasin Madhippu Veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Engum Ondre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Engum Ondre"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mozhigal Veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhigal Veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhar Ondre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhar Ondre"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirangal Veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirangal Veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Ondre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Ondre"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu Oru Vedam Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Oru Vedam Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Oru Naanaai Naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Oru Naanaai Naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Pazhagividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Pazhagividum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Sila Naatkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Sila Naatkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Mugangal Pazhagividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Mugangal Pazhagividum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Sila Naatkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Sila Naatkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Saalaigal Vilangividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Saalaigal Vilangividum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idharkku Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idharkku Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaraa Unarvugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaraa Unarvugal"/>
</div>
<div class="lyrico-lyrics-wrapper">En Naavukku Pidithuvidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Naavukku Pidithuvidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anniyan Endra Mugamoodi Naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anniyan Endra Mugamoodi Naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mugam Vittu Kazhandru Vidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mugam Vittu Kazhandru Vidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Sila Naatkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Sila Naatkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Mugangal Pazhagividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Mugangal Pazhagividum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Sila Naatkalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Sila Naatkalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Saalaigal Vilangividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Saalaigal Vilangividum"/>
</div>
</pre>
