---
title: "kottapakkum song lyrics"
album: "Sankarankovil"
artist: "Rajini"
lyricist: "Snehan"
director: "Palanivel Raja"
path: "/albums/sankarankovil-lyrics"
song: "Kottapakkum"
image: ../../images/albumart/sankarankovil.jpg
date: 2011-08-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/R73WaTMH-Wk"
type: "happy"
singers:
  - Sunandha
  - Malathi Lakshman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kottappaakkum kozhundhu veththalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottappaakkum kozhundhu veththalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayila poadaama nee sevandhu nikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayila poadaama nee sevandhu nikkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottappaakkum kozhundhu veththalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottappaakkum kozhundhu veththalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayila poadaama nee sevandhu nikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayila poadaama nee sevandhu nikkiriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vepparaiyillaadha ponnappaarththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vepparaiyillaadha ponnappaarththu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee verum vaayaala mellappaarkkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee verum vaayaala mellappaarkkuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththukkittu variyaa oththukkittu variyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththukkittu variyaa oththukkittu variyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oora vittu Odadaam kuththamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora vittu Odadaam kuththamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththukkida maatten oththukkida maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththukkida maatten oththukkida maatten"/>
</div>
<div class="lyrico-lyrics-wrapper">noorakkoopttu solluven poadaa thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noorakkoopttu solluven poadaa thalli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Police-se vanthaalum bayappadamaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police-se vanthaalum bayappadamaatten"/>
</div>
<div class="lyrico-lyrics-wrapper">adangaadha kaalai naan odhungidamaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangaadha kaalai naan odhungidamaatten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei kokkaattam nee kaaththu nirkkaadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei kokkaattam nee kaaththu nirkkaadhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thimingalatha nee koththa mudiyaadhudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thimingalatha nee koththa mudiyaadhudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottappaakkum kozhundhu veththalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottappaakkum kozhundhu veththalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayila poadaama nee sevandhu nikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayila poadaama nee sevandhu nikkiriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellarikkaa poala vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellarikkaa poala vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjimutta nikkiraaye paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjimutta nikkiraaye paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei kendakkaala paarthappinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei kendakkaala paarthappinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">kerangaama nikkiradhu yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kerangaama nikkiradhu yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei oththaikkoththa mallukkatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei oththaikkoththa mallukkatti"/>
</div>
<div class="lyrico-lyrics-wrapper">mudinjaa nee jeichippudappaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudinjaa nee jeichippudappaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mundhiyellaam avuthuvittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhiyellaam avuthuvittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">munnaadi nikkiradhu yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnaadi nikkiradhu yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paalaa irukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalaa irukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayaa karaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayaa karaka"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa kodhikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa kodhikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaari anaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaari anaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">neiyaa urukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neiyaa urukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerungi vaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungi vaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">paiyaa anaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paiyaa anaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagaa azhagaa unna paarkkumboadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagaa azhagaa unna paarkkumboadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">en mandaikkul munnooru paambuthaan koththudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en mandaikkul munnooru paambuthaan koththudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththukkittu variyaa oththukkittu variyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththukkittu variyaa oththukkittu variyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oora vittu Odadaam kuththamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora vittu Odadaam kuththamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththukkida maatten oththukkida maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththukkida maatten oththukkida maatten"/>
</div>
<div class="lyrico-lyrics-wrapper">noorakkoopttu solluven poadaa thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noorakkoopttu solluven poadaa thalli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avuthuvitta kaalai poala kondaattam poadugira aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avuthuvitta kaalai poala kondaattam poadugira aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konja neram ottikkittaa appuramaa paathukkiren maela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja neram ottikkittaa appuramaa paathukkiren maela"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyamukka kaala amukka enakku nee poattukkodu vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyamukka kaala amukka enakku nee poattukkodu vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachikkalaam kattikkalaam enna seiyappoara enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachikkalaam kattikkalaam enna seiyappoara enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Macham irukku vaadichellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macham irukku vaadichellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaru vellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaru vellam"/>
</div>
<div class="lyrico-lyrics-wrapper">seema sarakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seema sarakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">ooththu ooththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooththu ooththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerum kikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerum kikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adadaa adadaa onnakkattikkittaa indha kannaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa adadaa onnakkattikkittaa indha kannaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">pattaa ezhudhithaarendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattaa ezhudhithaarendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththukkittu variyaa oththukkittu variyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththukkittu variyaa oththukkittu variyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oora vittu Odadaam kuththamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora vittu Odadaam kuththamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththukkida maatten oththukkida maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththukkida maatten oththukkida maatten"/>
</div>
<div class="lyrico-lyrics-wrapper">noorakkoopttu solluven poadaa thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noorakkoopttu solluven poadaa thalli"/>
</div>
</pre>
