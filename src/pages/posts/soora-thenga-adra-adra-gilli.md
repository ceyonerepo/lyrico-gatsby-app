---
title: "soora thenga adra adra song lyrics"
album: "Gilli"
artist: "Vidyasagar"
lyricist: "Na. Muthukumar"
director: "Dharani"
path: "/albums/gilli-lyrics"
song: "Soora Thenga Adra Adra"
image: ../../images/albumart/gilli.jpg
date: 2004-04-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dlqSFqu7K8g"
type: "celebration"
singers:
  - Tippu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sura Thengaa Adra Adra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sura Thengaa Adra Adra"/>
</div>
<div class="lyrico-lyrics-wrapper">Suriyana Thodra Thodra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suriyana Thodra Thodra"/>
</div>
<div class="lyrico-lyrics-wrapper">Soorakaatha Pudra Pudra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorakaatha Pudra Pudra"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi Suthi Udra Udra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi Suthi Udra Udra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sura Thengaa Adra Adra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sura Thengaa Adra Adra"/>
</div>
<div class="lyrico-lyrics-wrapper">Suriyana Thodra Thodra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suriyana Thodra Thodra"/>
</div>
<div class="lyrico-lyrics-wrapper">Soorakaatha Pudra Pudra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorakaatha Pudra Pudra"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi Suthi Udra Udra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi Suthi Udra Udra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenja Thooki Nadra Nadra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Thooki Nadra Nadra"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppai Pole Irda Irda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppai Pole Irda Irda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachethellaam Senji Mudida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachethellaam Senji Mudida"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Maame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Maame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seeri Adicha Ah Gilli Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeri Adicha Ah Gilli Parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Seeri Adicha Vinna Polakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Seeri Adicha Vinna Polakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Ah Gilli Ah Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Ah Gilli Ah Gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Ah Gilli Ah Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Ah Gilli Ah Gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Ah Gilli Ah Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Ah Gilli Ah Gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Ah Gilli Ah Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Ah Gilli Ah Gilli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sura Thengaa Adra Adra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sura Thengaa Adra Adra"/>
</div>
<div class="lyrico-lyrics-wrapper">Suriyana Thodra Thodra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suriyana Thodra Thodra"/>
</div>
<div class="lyrico-lyrics-wrapper">Soorakaatha Pudra Pudra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorakaatha Pudra Pudra"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi Suthi Udra Udra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi Suthi Udra Udra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udra Udra Udra Udra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udra Udra Udra Udra"/>
</div>
<div class="lyrico-lyrics-wrapper">Udra Udra Udra Udra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udra Udra Udra Udra"/>
</div>
<div class="lyrico-lyrics-wrapper">Udra Udra Udra Udra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udra Udra Udra Udra"/>
</div>
<div class="lyrico-lyrics-wrapper">Udra Udra Udra Udra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udra Udra Udra Udra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wow Wow What A Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wow Wow What A Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">Wow Wow What A Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wow Wow What A Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Jeansu Raani Jeansu Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Jeansu Raani Jeansu Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Chance Su Udalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chance Su Udalaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingi Marappa Kaaram Neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingi Marappa Kaaram Neethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadichu Thingalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadichu Thingalaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Missu Worldum Venaam Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Missu Worldum Venaam Venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Muniammavum Venaam Venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muniammavum Venaam Venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Machi Machi Mayangaathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi Machi Mayangaathada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchi Ice Ah Urugaathada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchi Ice Ah Urugaathada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Polappa Paarthu Munneruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polappa Paarthu Munneruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhuthu Pona Kidaikaathuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhuthu Pona Kidaikaathuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa Paarthu Ponnunga Varumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Paarthu Ponnunga Varumda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Maame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Maame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seeri Adicha Ah Gilli Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeri Adicha Ah Gilli Parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Seeri Adicha Vinna Polakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Seeri Adicha Vinna Polakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Ah Gilli Ah Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Ah Gilli Ah Gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Ah Gilli Ah Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Ah Gilli Ah Gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Ah Gilli Ah Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Ah Gilli Ah Gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Ah Gilli Ah Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Ah Gilli Ah Gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Gilli Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Gilli Gilli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ganesanna Karnam Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganesanna Karnam Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyyapanna Saranam Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyyapanna Saranam Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganesanna Karnam Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganesanna Karnam Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyyapanna Saranam Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyyapanna Saranam Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandhanukku Kavasam Paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandhanukku Kavasam Paadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammanukku Veradham Edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammanukku Veradham Edu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivanukku Nee Aatam Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivanukku Nee Aatam Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sreerangamaa Paata Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sreerangamaa Paata Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosama Whistle La Podu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosama Whistle La Podu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Maame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Maame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seeri Adicha Ah Gilli Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeri Adicha Ah Gilli Parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Seeri Adicha Vinna Polakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Seeri Adicha Vinna Polakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Ah Gilli Ah Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Ah Gilli Ah Gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Ah Gilli Ah Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Ah Gilli Ah Gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Ah Gilli Ah Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Ah Gilli Ah Gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Ah Gilli Ah Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Ah Gilli Ah Gilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Gilli Gilli Gilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gilli Gilli Gilli"/>
</div>
</pre>
