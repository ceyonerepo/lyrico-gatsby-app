---
title: "udhiram vazhiya song lyrics"
album: "Methagu"
artist: "Praveen Kumar"
lyricist: "Kittu"
director: "T. Kittu"
path: "/albums/methagu-lyrics"
song: "Udhiram Vazhiya"
image: ../../images/albumart/methagu.jpg
date: 2021-06-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KKRsTUFGXoE"
type: "sad"
singers:
  - Uday Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">uthiram valiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthiram valiya "/>
</div>
<div class="lyrico-lyrics-wrapper">sananam kandom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sananam kandom"/>
</div>
<div class="lyrico-lyrics-wrapper">engal mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">maranam varaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranam varaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">uthira kanneer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthira kanneer"/>
</div>
<div class="lyrico-lyrics-wrapper">mattum kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mattum kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">parame varamathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parame varamathai"/>
</div>
<div class="lyrico-lyrics-wrapper">thaarum theeyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaarum theeyil"/>
</div>
<div class="lyrico-lyrics-wrapper">vegum udale meetham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegum udale meetham"/>
</div>
<div class="lyrico-lyrics-wrapper">alugai mozhiyil keetham paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alugai mozhiyil keetham paadum"/>
</div>
<div class="lyrico-lyrics-wrapper">iraval vaalgai vendam pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraval vaalgai vendam pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sathaiyum kiliyum satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathaiyum kiliyum satham"/>
</div>
<div class="lyrico-lyrics-wrapper">nitham arivom naangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitham arivom naangal"/>
</div>
<div class="lyrico-lyrics-wrapper">puthaiyum thegam kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthaiyum thegam kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">moolaiyathu antha vithaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moolaiyathu antha vithaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">vetum valiyal thudithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetum valiyal thudithom"/>
</div>
<div class="lyrico-lyrics-wrapper">sinthum siripai maranthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinthum siripai maranthom"/>
</div>
<div class="lyrico-lyrics-wrapper">malara motum uthira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malara motum uthira"/>
</div>
<div class="lyrico-lyrics-wrapper">kathi saaga piranthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi saaga piranthom"/>
</div>
<div class="lyrico-lyrics-wrapper">kilaigal muriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kilaigal muriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">athu kaatrin vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu kaatrin vegam"/>
</div>
<div class="lyrico-lyrics-wrapper">thalaigal sitharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalaigal sitharum"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu evarin saabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu evarin saabam"/>
</div>
<div class="lyrico-lyrics-wrapper">magarin aali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magarin aali"/>
</div>
<div class="lyrico-lyrics-wrapper">athu puviyin kobam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu puviyin kobam"/>
</div>
<div class="lyrico-lyrics-wrapper">engal uyiro veenai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal uyiro veenai"/>
</div>
<div class="lyrico-lyrics-wrapper">mithantho pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mithantho pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uthiram valiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthiram valiya "/>
</div>
<div class="lyrico-lyrics-wrapper">sananam kandom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sananam kandom"/>
</div>
<div class="lyrico-lyrics-wrapper">engal mannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal mannil"/>
</div>
<div class="lyrico-lyrics-wrapper">maranam varaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranam varaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">uthira kanneer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthira kanneer"/>
</div>
<div class="lyrico-lyrics-wrapper">mattum kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mattum kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">parame varamathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parame varamathai"/>
</div>
<div class="lyrico-lyrics-wrapper">thaarum theeyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaarum theeyil"/>
</div>
<div class="lyrico-lyrics-wrapper">vegum udale meetham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegum udale meetham"/>
</div>
<div class="lyrico-lyrics-wrapper">alugai mozhiyil keetham paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alugai mozhiyil keetham paadum"/>
</div>
<div class="lyrico-lyrics-wrapper">iraval vaalgai vendam pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraval vaalgai vendam pothum"/>
</div>
</pre>
