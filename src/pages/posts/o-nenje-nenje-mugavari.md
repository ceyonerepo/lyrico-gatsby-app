---
title: "o nenje nenje song lyrics"
album: "Mugavari"
artist: "Deva"
lyricist: "Vairamuthu"
director: "V. Z. Durai"
path: "/albums/mugavari-lyrics"
song: "O Nenje Nenje"
image: ../../images/albumart/mugavari.jpg
date: 2000-02-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tvqCcn_CAXk"
type: "Love"
singers:
  - Hariharan
  - Swarnalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh Nenjae Nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nenjae Nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Ra Ra Oh Nenja Nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Ra Ra Oh Nenja Nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Nenjae Nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Nenjae Nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Ra Ra Oh Nenja Nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Ra Ra Oh Nenja Nenjae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vellai Chandira Veedhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vellai Chandira Veedhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaap Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaap Pogiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Natchathirangalil Vaazhavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Natchathirangalil Vaazhavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaa Kaanngiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kaanngiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjae Nee Vinnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjae Nee Vinnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttri Parandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttri Parandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaalai Mannil Oondri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaalai Mannil Oondri"/>
</div>
<div class="lyrico-lyrics-wrapper">Nil Nil Nil Nil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nil Nil Nil Nil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae Anbae Un Dhukkathai Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae Anbae Un Dhukkathai Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnai Thottu Un Perai Nilavil Vettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai Thottu Un Perai Nilavil Vettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrellam Inikkumpadi Kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrellam Inikkumpadi Kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhukkul Paattupadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhukkul Paattupadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kaalam Nadakkattumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaalam Nadakkattumae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Dhevaa Un Maarbil Saaindhapadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Dhevaa Un Maarbil Saaindhapadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvai Siru Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvai Siru Vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Uyirukku Kavasamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Uyirukku Kavasamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Irandhaalum Uyiroottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandhaalum Uyiroottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Viralgalin Sparisamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Viralgalin Sparisamadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Sollum Sollai Kelaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sollum Sollai Kelaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaiiku Neeyae Velvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaiiku Neeyae Velvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangeetha Naadhangalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangeetha Naadhangalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedham Sollvaai Vedham Sollvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedham Sollvaai Vedham Sollvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennae Pennae Un Ottrai Sollukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae Pennae Un Ottrai Sollukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnum Muththum Naan Kotti Thara Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnum Muththum Naan Kotti Thara Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae Anbae Un Anbu Soll Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae Anbae Un Anbu Soll Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Sollu En Ratham Oora Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Sollu En Ratham Oora Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Nenjae Nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Nenjae Nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Ra Ra Oo Nenjae Nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Ra Ra Oo Nenjae Nenjae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">College College
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College College"/>
</div>
<div class="lyrico-lyrics-wrapper">Akka Endha College
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka Endha College"/>
</div>
<div class="lyrico-lyrics-wrapper">October November
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="October November"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha Maasam Marriage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endha Maasam Marriage"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart-oda Beat-u Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart-oda Beat-u Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Love Love Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Love Love Love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandharpam Amaindhu Vittal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandharpam Amaindhu Vittal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennpoovae Sangeedham Maatri Vaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennpoovae Sangeedham Maatri Vaippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalangal Kaniyum Varai Pesaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalangal Kaniyum Varai Pesaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrukku Isaiamaippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrukku Isaiamaippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalangaadhae Mayangaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangaadhae Mayangaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kanavukku Thunai Iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanavukku Thunai Iruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Boomi Udaindhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Boomi Udaindhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Ullankaiyil Yendhi Parappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ullankaiyil Yendhi Parappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjil Saaithukkolven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Saaithukkolven"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathin Osai Kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayathin Osai Kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjil Ottichellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Ottichellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paattukkettu Mettu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattukkettu Mettu Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovae Poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovae Poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Moochchae Sangeetham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Moochchae Sangeetham"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththam Sindhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththam Sindhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Muthamkooda Naadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Muthamkooda Naadham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvin Dheepam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin Dheepam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Needhaan Eppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Needhaan Eppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrum Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sollum Sollae Vedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sollum Sollae Vedham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Nenjae Nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Nenjae Nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Ra Ra Oo Nenjae Nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Ra Ra Oo Nenjae Nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">O Nenjae Nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Nenjae Nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Ra Ra Ra Oo Nenjae Nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Ra Ra Ra Oo Nenjae Nenjae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vellai Chandira Veedhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vellai Chandira Veedhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaap Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaap Pogiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Natchathirangalil Vaazhavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Natchathirangalil Vaazhavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaa Kaanngiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kaanngiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjae Nee Vinnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjae Nee Vinnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttri Parandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttri Parandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaalai Mannil Oondri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaalai Mannil Oondri"/>
</div>
<div class="lyrico-lyrics-wrapper">Nil Nil Nil Nil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nil Nil Nil Nil"/>
</div>
</pre>
