---
title: "andhar bulty song lyrics"
album: "Natpuna Ennanu Theriyuma"
artist: "Dharan"
lyricist: "Jeyachandra Hashmi"
director: "Shiva Arvind"
path: "/albums/natpuna-ennanu-theriyuma-lyrics"
song: "Andhar Bulty"
image: ../../images/albumart/natpuna-ennanu-theriyuma.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FKvb51KeFxM"
type: "love"
singers:
  - Haricharan
  - Alisha Thomas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Andhar Bulty Adikka Matten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhar Bulty Adikka Matten"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichalum Pudichalum Velagamatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichalum Pudichalum Velagamatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Breakupnu Piriya Matten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Breakupnu Piriya Matten"/>
</div>
<div class="lyrico-lyrics-wrapper">Thitti Paada Vaaipu Tharave Matten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thitti Paada Vaaipu Tharave Matten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhar Bulty Adikka Matten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhar Bulty Adikka Matten"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichalum Pudichalum Velagamatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichalum Pudichalum Velagamatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Breakupnu Piriya Matten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Breakupnu Piriya Matten"/>
</div>
<div class="lyrico-lyrics-wrapper">Thitti Paada Vaaipu Tharave Matten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thitti Paada Vaaipu Tharave Matten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkunnu Oor Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkunnu Oor Aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Oru Peraasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Oru Peraasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Mozhi Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Mozhi Venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama Mattum Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Mattum Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavellam Ini Venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavellam Ini Venam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karparanai Yen Veena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karparanai Yen Veena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijame Podhum Vaazhvom Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijame Podhum Vaazhvom Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poet Illa Duet Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poet Illa Duet Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lyrics Kottudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lyrics Kottudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Paakum Nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Paakum Nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechu Kidayil Music Odudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechu Kidayil Music Odudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana Ooramaaga Kaaramaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana Ooramaaga Kaaramaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullum Kuthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullum Kuthudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thaana Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thaana Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Jannal Seatu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannal Seatu Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Godu Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godu Thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuskka Piece Su Nee Adiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuskka Piece Su Nee Adiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thondaikkul Vikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondaikkul Vikkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinnaka Sikkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinnaka Sikkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Panna Enna Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Panna Enna Panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Almostu Azhuthuten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Almostu Azhuthuten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhar Bulty Adikka Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhar Bulty Adikka Vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichalum Pudichalum Vilaga Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichalum Pudichalum Vilaga Vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Breakupnu Piriya Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Breakupnu Piriya Vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Thitti Paadum Paavam Enakku Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thitti Paadum Paavam Enakku Vena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkunnu Oor Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkunnu Oor Aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Oru Peraasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Oru Peraasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Mozhi Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Mozhi Venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama Mattum Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama Mattum Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavellam Ini Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavellam Ini Vena"/>
</div>
<div class="lyrico-lyrics-wrapper">Karparanai Yen Veena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karparanai Yen Veena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijama Naama Vaazhalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijama Naama Vaazhalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenga Maatenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Maatenada"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenga Maatenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Maatenada"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenga Maatenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Maatenada"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Mattum Nenachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Mattum Nenachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethu Sethu Pozhachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethu Sethu Pozhachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenga Maatenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Maatenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenga Maatenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Maatenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenga Maatenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga Maatenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Mattum Nenachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Mattum Nenachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethu Sethu Pozhachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethu Sethu Pozhachen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sethu Sethu Pozhappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethu Sethu Pozhappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Mattum Nenappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Mattum Nenappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethu Sethu Pozhappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethu Sethu Pozhappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Mattum Nenappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Mattum Nenappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sethu Sethu Pozhappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethu Sethu Pozhappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Mattum Nenappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Mattum Nenappen"/>
</div>
</pre>
