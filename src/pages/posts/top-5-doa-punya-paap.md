---
title: "top 5 doa song lyrics"
album: "Punya Paap"
artist: "Stunnah Beatz"
lyricist: "DIVINE"
director: "Gil Green"
path: "/albums/punya-paap-lyrics"
song: "Top 5 DOA"
image: ../../images/albumart/punya-paap.jpg
date: 2020-12-03
lang: hindi
youtubeLink: "https://www.youtube.com/embed/K0F5K9To0p8"
type: "happy"
singers:
  - DIVINE
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aankhein jaisi khulti meri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhein jaisi khulti meri"/>
</div>
<div class="lyrico-lyrics-wrapper">Karte rehta kuch na kuch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karte rehta kuch na kuch"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeche woh girana chahte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeche woh girana chahte"/>
</div>
<div class="lyrico-lyrics-wrapper">Karte rehte kuch na kuch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karte rehte kuch na kuch"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main na baitha rehta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main na baitha rehta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhko karna rehta kuch na kuch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhko karna rehta kuch na kuch"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhse lena chahte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhse lena chahte"/>
</div>
<div class="lyrico-lyrics-wrapper">Unko karma dega kuch na kuch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unko karma dega kuch na kuch"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mehnat karna aaj tumko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehnat karna aaj tumko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal tab dega kuch na kuch
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal tab dega kuch na kuch"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeena chhod deta jab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeena chhod deta jab"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekhta nahi sapna tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekhta nahi sapna tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mushqilein zameen mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mushqilein zameen mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Main mushqilon ko dafna dun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main mushqilon ko dafna dun"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo jalte mujhse unko tohfa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo jalte mujhse unko tohfa"/>
</div>
<div class="lyrico-lyrics-wrapper">Main sirf mere main dun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main sirf mere main dun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dekh Mumbai main kahan aa gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekh Mumbai main kahan aa gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekh Mumma main kahan aa gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekh Mumma main kahan aa gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Top 5 D.O.A maano ya na maano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Top 5 D.O.A maano ya na maano"/>
</div>
<div class="lyrico-lyrics-wrapper">Top 5 D.O.A maano ya na maano aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Top 5 D.O.A maano ya na maano aah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mera naam divine hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera naam divine hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Shukriya sab ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shukriya sab ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Itne saal se sun rahe tum log
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itne saal se sun rahe tum log"/>
</div>
<div class="lyrico-lyrics-wrapper">Itne saal se tum log
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itne saal se tum log"/>
</div>
<div class="lyrico-lyrics-wrapper">Support karte aa rahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Support karte aa rahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapna tha sapna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapna tha sapna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur abhi bhi sapne mein chal rahe apan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur abhi bhi sapne mein chal rahe apan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shout out everybody out there
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shout out everybody out there"/>
</div>
<div class="lyrico-lyrics-wrapper">Who supports the movement
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who supports the movement"/>
</div>
<div class="lyrico-lyrics-wrapper">Gully gang boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gully gang boy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sunega kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunega kya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya bol reli hai public
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya bol reli hai public"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha! ha!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha! ha!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maza aata hai yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maza aata hai yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaana bananne mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaana bananne mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Bahut maza aata hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bahut maza aata hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gully gang boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gully gang boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyi raaste mein aayenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyi raaste mein aayenge"/>
</div>
<div class="lyrico-lyrics-wrapper">To seedha hurr hurr hurr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To seedha hurr hurr hurr"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal vibe karte rehne ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal vibe karte rehne ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Paisa kamate rehna ka!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paisa kamate rehna ka!"/>
</div>
</pre>
