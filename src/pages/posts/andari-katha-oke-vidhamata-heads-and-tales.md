---
title: "andari katha oke vidhamata song lyrics"
album: "Heads And Tales"
artist: "Mani Sharma"
lyricist: "Kittu Vissapragada"
director: "Sai Krishna Enreddy"
path: "/albums/heads-and-tales-lyrics"
song: "Andari Katha Oke Vidhamata"
image: ../../images/albumart/heads-and-tales.jpg
date: 2021-10-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/jCPEnu1btaM"
type: "melody"
singers:
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Arey chindara vandaraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey chindara vandaraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Taka taka mani thondaraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taka taka mani thondaraga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala rathala rasthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala rathala rasthada"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaka chaka mani chitranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaka chaka mani chitranga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gadiyaram leni lokamlo untaadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadiyaram leni lokamlo untaadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Musthabey chesi bhoommeedha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musthabey chesi bhoommeedha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedathaadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedathaadanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhari katha okey vidhamata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhari katha okey vidhamata"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathanamu maarey gathey bathukata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathanamu maarey gathey bathukata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye gamyamu ye vaipu undho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye gamyamu ye vaipu undho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye vaipu undho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye vaipu undho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee payanamu ye dari pono
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee payanamu ye dari pono"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye dari pono
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye dari pono"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thega gandara golanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thega gandara golanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thika maka padu jeevithama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thika maka padu jeevithama"/>
</div>
<div class="lyrico-lyrics-wrapper">Are indara jaalamuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are indara jaalamuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadabidamani saagenuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadabidamani saagenuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gadiyaram thoti veganga saagaalanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadiyaram thoti veganga saagaalanta"/>
</div>
<div class="lyrico-lyrics-wrapper">O nimisham aina opigga agedundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O nimisham aina opigga agedundha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thondara padey hadavidi thanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondara padey hadavidi thanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manushulakadhey yadha vidhi gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushulakadhey yadha vidhi gunam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaguthu alaa kathala roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaguthu alaa kathala roopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathala roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathala roopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari poyana adigi choodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari poyana adigi choodham"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigi choodham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigi choodham"/>
</div>
</pre>
