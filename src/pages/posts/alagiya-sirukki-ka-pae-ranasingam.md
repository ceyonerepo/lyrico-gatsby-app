---
title: "alagiya sirukki lyrics"
album: "Ka Pae Ranasingam"
artist: "Ghibran"
lyricist: "Vairamuthu"
director: "P Virumandi"
path: "/albums/ka-pae-ranasingam-lyrics"
song: "Alagiya Sirukki"
image: ../../images/albumart/ka-pae-ranasingam.jpg
date: 2020-10-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vmrpEzRoY_w"
type: "love"
singers:
  - Gold Devaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Alagiya sirukki aruvaa mooki
<input type="checkbox" class="lyrico-select-lyric-line" value="Alagiya sirukki aruvaa mooki"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna onnu kekattuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna onnu kekattuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye nenjukulla unakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Ye nenjukulla unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Eera nappu irukaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Eera nappu irukaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerottam paakattuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Neerottam paakattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ye alagiya sirukki aruvaa mooki
<input type="checkbox" class="lyrico-select-lyric-line" value="Ye alagiya sirukki aruvaa mooki"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna onnu kekattuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna onnu kekattuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla unakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjukulla unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Eera nappu irukaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Eera nappu irukaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerottam paakattuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Neerottam paakattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En tharatharam kaangamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="En tharatharam kaangamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adavadi pannathadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Adavadi pannathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan saraasari aal illadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan saraasari aal illadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni kaati pogathadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanni kaati pogathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaivittu ponakkooda
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaivittu ponakkooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal theeradhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal theeradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaala choru mattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kathaala choru mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeram maaradhu (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Eeram maaradhu (2 times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannukettum thoorammattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannukettum thoorammattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithunundu pachaiyila
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithunundu pachaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanju pona tharisirukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaanju pona tharisirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aththuvaana kaatukulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Aththuvaana kaatukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Othha oththa pattaampoochi
<input type="checkbox" class="lyrico-select-lyric-line" value="Othha oththa pattaampoochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukulla usuru irukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhukulla usuru irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kammayae kaanjaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kammayae kaanjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvelam poopokkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuvelam poopokkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemma ne ponaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yemma ne ponaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Usur pookkuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Usur pookkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mundhana enakaaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Mundhana enakaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Muluvetti unakaaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Muluvetti unakaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasumponnil thaali senjaachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pasumponnil thaali senjaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaluththa kaatu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaluththa kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Alagiya sirukki aruvaa mooki
<input type="checkbox" class="lyrico-select-lyric-line" value="Alagiya sirukki aruvaa mooki"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna onnu kekattuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna onnu kekattuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye nenjukulla unakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Ye nenjukulla unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Eera nappu irukaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Eera nappu irukaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerottam paakattuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Neerottam paakattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ye alagiya sirukki aruvaa mooki
<input type="checkbox" class="lyrico-select-lyric-line" value="Ye alagiya sirukki aruvaa mooki"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna onnu kekattuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna onnu kekattuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla unakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjukulla unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Eera nappu irukaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Eera nappu irukaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerottam paakattuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Neerottam paakattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En tharatharam kaangamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="En tharatharam kaangamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adavadi pannathadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Adavadi pannathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan saraasari aal illadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan saraasari aal illadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni kaati pogathadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanni kaati pogathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaivittu ponakkooda
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaivittu ponakkooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal theeradhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal theeradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaala choru mattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kathaala choru mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeram maaradhu (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Eeram maaradhu"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennavittu nee kadantha
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennavittu nee kadantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnavittu naan pirinja
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnavittu naan pirinja"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannavittu maranjiruppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannavittu maranjiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Meen seththu midhakkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Meen seththu midhakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulathula nadhiyila
<input type="checkbox" class="lyrico-select-lyric-line" value="Kulathula nadhiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan seththu midhanthiruppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan seththu midhanthiruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennnaa aanaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennnaa aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaavi ponaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enaavi ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenjil ukkandhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un nenjil ukkandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Usir vazhuven
<input type="checkbox" class="lyrico-select-lyric-line" value="Usir vazhuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un veetu kooraiyila
<input type="checkbox" class="lyrico-select-lyric-line" value="Un veetu kooraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Olugum neer naanaven
<input type="checkbox" class="lyrico-select-lyric-line" value="Olugum neer naanaven"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththathuli paarvai paar kannu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oththathuli paarvai paar kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oram ninnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oram ninnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Alagiya sirukki aruvaa mooki
<input type="checkbox" class="lyrico-select-lyric-line" value="Alagiya sirukki aruvaa mooki"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna onnu kekattuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna onnu kekattuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye nenjukulla unakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Ye nenjukulla unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Eera nappu irukaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Eera nappu irukaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerottam paakattuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Neerottam paakattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ye alagiya sirukki aruvaa mooki
<input type="checkbox" class="lyrico-select-lyric-line" value="Ye alagiya sirukki aruvaa mooki"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna onnu kekattuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna onnu kekattuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla unakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjukulla unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Eera nappu irukaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Eera nappu irukaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerottam paakattuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Neerottam paakattuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En tharatharam kaangamalae
<input type="checkbox" class="lyrico-select-lyric-line" value="En tharatharam kaangamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adavadi pannathadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Adavadi pannathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan saraasari aal illadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan saraasari aal illadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni kaati pogathadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanni kaati pogathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaivittu ponakkooda
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaivittu ponakkooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal theeradhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal theeradhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathaala choru mattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kathaala choru mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeram maaradhu (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Eeram maaradhu (2 times)"/></div>
</div>
</pre>
