---
title: "angali thaye song lyrics"
album: "IPC 376"
artist: "Yaadhav Ramalinkgam"
lyricist: "Madhurakavi"
director: "Ramkumar Subbaraman"
path: "/albums/ipc-376-lyrics"
song: "Angali Thaye"
image: ../../images/albumart/ipc-376.jpg
date: 2021-10-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QauMsWw5XHM"
type: "devotional"
singers:
  - Roshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">simhasini sampoorani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="simhasini sampoorani"/>
</div>
<div class="lyrico-lyrics-wrapper">sringara kalai roopini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sringara kalai roopini"/>
</div>
<div class="lyrico-lyrics-wrapper">jaya veera theeri neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaya veera theeri neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">padavettu pathini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padavettu pathini"/>
</div>
<div class="lyrico-lyrics-wrapper">padai veetu marthini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padai veetu marthini"/>
</div>
<div class="lyrico-lyrics-wrapper">sathguna thayabari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathguna thayabari"/>
</div>
<div class="lyrico-lyrics-wrapper">aathi suyambana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathi suyambana"/>
</div>
<div class="lyrico-lyrics-wrapper">veerambige neethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veerambige neethi"/>
</div>
<div class="lyrico-lyrics-wrapper">sudarana moogambige
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudarana moogambige"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalathin vanjathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalathin vanjathai"/>
</div>
<div class="lyrico-lyrics-wrapper">soolathaal nee vellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soolathaal nee vellamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">angali thaye sengali neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angali thaye sengali neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">mannalum maakaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannalum maakaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">kadungobakari kathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadungobakari kathi "/>
</div>
<div class="lyrico-lyrics-wrapper">kakum mari pennuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kakum mari pennuku"/>
</div>
<div class="lyrico-lyrics-wrapper">nee veliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee veliye"/>
</div>
<div class="lyrico-lyrics-wrapper">soolatha solathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soolatha solathura"/>
</div>
<div class="lyrico-lyrics-wrapper">kali neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kali neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">soorana merattura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soorana merattura"/>
</div>
<div class="lyrico-lyrics-wrapper">sooli neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sooli neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">sooriya kathirana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sooriya kathirana"/>
</div>
<div class="lyrico-lyrics-wrapper">mari neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">sudalaikul sathiradum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudalaikul sathiradum"/>
</div>
<div class="lyrico-lyrics-wrapper">devi neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devi neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">deepatha eathi vachom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deepatha eathi vachom"/>
</div>
<div class="lyrico-lyrics-wrapper">pen thisaiyengum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen thisaiyengum "/>
</div>
<div class="lyrico-lyrics-wrapper">velichatha kaatidama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velichatha kaatidama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">angali thaye sengali neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angali thaye sengali neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">mannalum maakaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannalum maakaliye"/>
</div>
<div class="lyrico-lyrics-wrapper">kadungobakari kathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadungobakari kathi "/>
</div>
<div class="lyrico-lyrics-wrapper">kakum mari pennuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kakum mari pennuku"/>
</div>
<div class="lyrico-lyrics-wrapper">nee veliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee veliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thirisoolam thaga thagakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirisoolam thaga thagakka"/>
</div>
<div class="lyrico-lyrics-wrapper">parimelam pada padaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parimelam pada padaka"/>
</div>
<div class="lyrico-lyrics-wrapper">i nagam kudai pidikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i nagam kudai pidikka"/>
</div>
<div class="lyrico-lyrics-wrapper">arulum sooli ye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arulum sooli ye"/>
</div>
<div class="lyrico-lyrics-wrapper">sri chakra kamatchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sri chakra kamatchiye"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kan partha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kan partha"/>
</div>
<div class="lyrico-lyrics-wrapper">theernthidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theernthidum "/>
</div>
<div class="lyrico-lyrics-wrapper">sothanai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sothanai than"/>
</div>
<div class="lyrico-lyrics-wrapper">sri rudhra meenatchiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sri rudhra meenatchiye"/>
</div>
<div class="lyrico-lyrics-wrapper">pen pooppeidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen pooppeidha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalnaale por munaithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalnaale por munaithan"/>
</div>
<div class="lyrico-lyrics-wrapper">poovayarin poovarasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovayarin poovarasi"/>
</div>
<div class="lyrico-lyrics-wrapper">kamugar pidiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kamugar pidiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">kaapathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaapathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">mangayarin maadharasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mangayarin maadharasi"/>
</div>
<div class="lyrico-lyrics-wrapper">kanniyar karpuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanniyar karpuku"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kaappamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kaappamma"/>
</div>
<div class="lyrico-lyrics-wrapper">pen jenman munnerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen jenman munnerum"/>
</div>
<div class="lyrico-lyrics-wrapper">kalam amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalam amma"/>
</div>
<div class="lyrico-lyrics-wrapper">aan vanmam innamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aan vanmam innamum"/>
</div>
<div class="lyrico-lyrics-wrapper">theeralamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeralamma"/>
</div>
<div class="lyrico-lyrics-wrapper">nathikellam pen pera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nathikellam pen pera"/>
</div>
<div class="lyrico-lyrics-wrapper">vacharamma mathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacharamma mathi"/>
</div>
<div class="lyrico-lyrics-wrapper">kette maasakki vitaramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kette maasakki vitaramma"/>
</div>
<div class="lyrico-lyrics-wrapper">idi kotta varuvai amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idi kotta varuvai amma"/>
</div>
<div class="lyrico-lyrics-wrapper">un thirisoola kan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thirisoola kan "/>
</div>
<div class="lyrico-lyrics-wrapper">vetta velvayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetta velvayamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">para paravena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="para paravena"/>
</div>
<div class="lyrico-lyrics-wrapper">sara saravena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sara saravena"/>
</div>
<div class="lyrico-lyrics-wrapper">avesama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avesama"/>
</div>
<div class="lyrico-lyrics-wrapper">aadum kali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadum kali"/>
</div>
<div class="lyrico-lyrics-wrapper">arakkan siram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arakkan siram"/>
</div>
<div class="lyrico-lyrics-wrapper">koyyum sooli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koyyum sooli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muthu mari thillai mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthu mari thillai mari"/>
</div>
<div class="lyrico-lyrics-wrapper">nellai mari thanjai mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nellai mari thanjai mari"/>
</div>
<div class="lyrico-lyrics-wrapper">thandu mari sakthi mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandu mari sakthi mari"/>
</div>
<div class="lyrico-lyrics-wrapper">bathra kali ukra kali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bathra kali ukra kali"/>
</div>
<div class="lyrico-lyrics-wrapper">nermaikku karanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nermaikku karanama"/>
</div>
<div class="lyrico-lyrics-wrapper">oru pen vazhnthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru pen vazhnthal"/>
</div>
<div class="lyrico-lyrics-wrapper">aan inam viduvathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aan inam viduvathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">bothaikku ubayogama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothaikku ubayogama"/>
</div>
<div class="lyrico-lyrics-wrapper">oru porul aakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru porul aakum"/>
</div>
<div class="lyrico-lyrics-wrapper">pen vazhvil vidive illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen vazhvil vidive illa"/>
</div>
<div class="lyrico-lyrics-wrapper">sooran ellam aliyavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sooran ellam aliyavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">kamathin roopathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kamathin roopathil"/>
</div>
<div class="lyrico-lyrics-wrapper">nadamaduran 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadamaduran "/>
</div>
<div class="lyrico-lyrics-wrapper">kaaval kaakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaval kaakum"/>
</div>
<div class="lyrico-lyrics-wrapper">sami kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sami kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaidhiye vilangai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaidhiye vilangai than"/>
</div>
<div class="lyrico-lyrics-wrapper">matti vittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matti vittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">aniyayam arangerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aniyayam arangerum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam amma"/>
</div>
<div class="lyrico-lyrics-wrapper">adhikaram vedikkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhikaram vedikkai"/>
</div>
<div class="lyrico-lyrics-wrapper">parkuthamma desathin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parkuthamma desathin"/>
</div>
<div class="lyrico-lyrics-wrapper">thisai engum pen olam ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thisai engum pen olam ma"/>
</div>
<div class="lyrico-lyrics-wrapper">ketkuthu ketkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketkuthu ketkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nedungalama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nedungalama "/>
</div>
<div class="lyrico-lyrics-wrapper">andaththin kaval neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andaththin kaval neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">oru avatharam enakkulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru avatharam enakkulle"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatidamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatidamma "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">simhasini sampoorani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="simhasini sampoorani"/>
</div>
<div class="lyrico-lyrics-wrapper">samharini jagatharini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samharini jagatharini"/>
</div>
<div class="lyrico-lyrics-wrapper">avatharini bhavatharini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avatharini bhavatharini"/>
</div>
<div class="lyrico-lyrics-wrapper">omkaarini dakshayini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="omkaarini dakshayini"/>
</div>
</pre>
