---
title: "raththa aaraththi song lyrics"
album: "Asuravadham"
artist: "Govind Menon"
lyricist: "Karthik Netha"
director: "Maruthupandian"
path: "/albums/asuravadham-lyrics"
song: "Raththa Aaraththi"
image: ../../images/albumart/asuravadham.jpg
date: 2018-06-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YZR1_CioYWw"
type: "mass"
singers:
  - Sivam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oor naattu sivan da ivan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor naattu sivan da ivan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Koravala thinnum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koravala thinnum "/>
</div>
<div class="lyrico-lyrics-wrapper">naran da varan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naran da varan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Odae dhinam odae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odae dhinam odae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan saavin pangali da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan saavin pangali da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puli kaal poottu varuvan varuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli kaal poottu varuvan varuvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangula vettum aruva aruva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangula vettum aruva aruva"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavu thinam saavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavu thinam saavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada okkaali neeyum thakkaali than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada okkaali neeyum thakkaali than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukku unakirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukku unakirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan ovvoru uruppum neruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan ovvoru uruppum neruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukku unakirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukku unakirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu kavukku mundhina sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu kavukku mundhina sirippu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan sonnaalum ivan sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan sonnaalum ivan sonnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Evan sonnaalum yeman sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evan sonnaalum yeman sonnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada dandakku dandakku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada dandakku dandakku "/>
</div>
<div class="lyrico-lyrics-wrapper">sandaikku nikkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandaikku nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandaiya thingura madhavam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandaiya thingura madhavam da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usir eduththa podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usir eduththa podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Asuravadham aarumVaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asuravadham aarumVaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raththa araththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththa araththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Erimalaiyaa vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erimalaiyaa vaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Erichupputtae podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erichupputtae podaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni kaala thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni kaala thee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usir eduththa podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usir eduththa podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Asuravadham aarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asuravadham aarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raththa araththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththa araththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Erimalaiyaa vaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erimalaiyaa vaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Erichupputtae podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erichupputtae podaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Veera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni kaala thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni kaala thee"/>
</div>
</pre>
