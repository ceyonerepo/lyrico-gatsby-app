---
title: "paasam indri song lyrics"
album: "Margandeyan"
artist: "Sundar C Babu"
lyricist: "Kavivarman"
director: "FEFSI Vijayan"
path: "/albums/markandeyan-lyrics"
song: "Paasam Indri"
image: ../../images/albumart/markandeyan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Kavivarman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paasam Indri Uyirai Edukkum Kayirudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Indri Uyirai Edukkum Kayirudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Paasakkayiru Endru Sonnaa Thavarudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Paasakkayiru Endru Sonnaa Thavarudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam Indri Uyirai Edukkum Kayirudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Indri Uyirai Edukkum Kayirudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Paasakkayiru Endru Sonnaa Thavarudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Paasakkayiru Endru Sonnaa Thavarudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram Varusham Manushana Thinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Varusham Manushana Thinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannin Pasithaan Adangaalaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannin Pasithaan Adangaalaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandavan Raajiyam Manushan Verum Boojiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavan Raajiyam Manushan Verum Boojiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodanum Gnaaniyum Mudivil Sooniyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodanum Gnaaniyum Mudivil Sooniyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udambukku Peruthaan Meiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambukku Peruthaan Meiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Poiyithaan Aachuthey Aiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Poiyithaan Aachuthey Aiyo"/>
</div>
</pre>
