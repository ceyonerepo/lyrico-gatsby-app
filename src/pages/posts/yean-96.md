---
title: "yean song lyrics"
album: "96"
artist: "Govind Vasantha"
lyricist: "Karthik Netha"
director: "C Premkumar"
path: "/albums/96-lyrics"
song: "Yean"
image: ../../images/albumart/96.jpg
date: 2018-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SYv_jRJoWiE"
type: "sad"
singers:
  - Gowri TP
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yean Yedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yean Yedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooraamal ponaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooraamal ponaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Netrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Netrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poottaamal ponaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poottaamal ponaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saambalaai varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saambalaai varam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge en megam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge en megam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidugathaiyaai ganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidugathaiyaai ganam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneeril pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneeril pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooramaai ponadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooramaai ponadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalin keerthanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalin keerthanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhnthidum neerellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhnthidum neerellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theduthalin praarthanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theduthalin praarthanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorai thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorai thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaan endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaan endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Angum ingum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angum ingum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Verai thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verai thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaan endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaan endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai ulle panthaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai ulle panthaadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theernthathe ganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theernthathe ganam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae un vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae un vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadayenave varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadayenave varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerin theeraa baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerin theeraa baaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veguthe naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veguthe naalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge un pathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge un pathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragugalai paarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragugalai paarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyinil saagum naalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyinil saagum naalai"/>
</div>
</pre>
