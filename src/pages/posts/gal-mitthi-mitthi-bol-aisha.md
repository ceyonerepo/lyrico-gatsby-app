---
title: "gal mitthi mitthi song lyrics"
album: "Aisha"
artist: "Amit Trivedi"
lyricist: "Javed Akhtar"
director: "Rajshree Ojha"
path: "/albums/aisha-lyrics"
song: "Gal Mitthi Mitthi Bol"
image: ../../images/albumart/aisha.jpg
date: 2010-08-06
lang: hindi
youtubeLink: "https://www.youtube.com/embed/MVE2pzZ_B8E"
type: "love"
singers:
  - Tochi Raina
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gal mitthi mitthi bol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gal mitthi mitthi bol"/>
</div>
<div class="lyrico-lyrics-wrapper">Das kaano vich gol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Das kaano vich gol"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajne de taashe dhol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajne de taashe dhol"/>
</div>
<div class="lyrico-lyrics-wrapper">Masti mein tu vi dol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masti mein tu vi dol"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann de naina tu khol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann de naina tu khol"/>
</div>
<div class="lyrico-lyrics-wrapper">Chahat ke moti rol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chahat ke moti rol"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil hunda ae anmol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil hunda ae anmol"/>
</div>
<div class="lyrico-lyrics-wrapper">Jedo lashke na tol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jedo lashke na tol"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa sohni tenu chaand ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa sohni tenu chaand ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Main choodi pehrawa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main choodi pehrawa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu kar de ishaara te
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu kar de ishaara te"/>
</div>
<div class="lyrico-lyrics-wrapper">Main dholi lai aanwa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main dholi lai aanwa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gal mitthi mitthi bol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gal mitthi mitthi bol"/>
</div>
<div class="lyrico-lyrics-wrapper">Das kaano vich gol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Das kaano vich gol"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajne de taashe dhol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajne de taashe dhol"/>
</div>
<div class="lyrico-lyrics-wrapper">Masti mein tu vi dol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masti mein tu vi dol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaan le vatheriyan do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaan le vatheriyan do"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise na koi ho fida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise na koi ho fida"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera ang sharara jaise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera ang sharara jaise"/>
</div>
<div class="lyrico-lyrics-wrapper">Maare lishkara sohniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maare lishkara sohniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekhan ta dil dhadke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekhan ta dil dhadke"/>
</div>
<div class="lyrico-lyrics-wrapper">Tann mein agan bhadke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tann mein agan bhadke"/>
</div>
<div class="lyrico-lyrics-wrapper">Surat aisi mannmoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surat aisi mannmoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta dasta tu hai sohni heeriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta dasta tu hai sohni heeriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Gal mitthi mitthi bol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gal mitthi mitthi bol"/>
</div>
<div class="lyrico-lyrics-wrapper">Das kaano vich gol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Das kaano vich gol"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajne de taashe dhol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajne de taashe dhol"/>
</div>
<div class="lyrico-lyrics-wrapper">Masti mein tu vi dol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masti mein tu vi dol"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann de naina tu khol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann de naina tu khol"/>
</div>
<div class="lyrico-lyrics-wrapper">Chahat ke moti rol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chahat ke moti rol"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil hunda ae anmol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil hunda ae anmol"/>
</div>
<div class="lyrico-lyrics-wrapper">Jedo lashke na tol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jedo lashke na tol"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa sohni tenu chaand ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa sohni tenu chaand ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Main choodi pehrawa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main choodi pehrawa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu kar de ishaara te
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu kar de ishaara te"/>
</div>
<div class="lyrico-lyrics-wrapper">Main dholi lai aanwa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main dholi lai aanwa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chahne wala hoon tera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chahne wala hoon tera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekh le dard zara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekh le dard zara"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu jo vekhe ik nazar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jo vekhe ik nazar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kara lakhan da shukar sohniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kara lakhan da shukar sohniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekh to tu keh ke mujhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekh to tu keh ke mujhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaan bhi de dunga tujhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaan bhi de dunga tujhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera aisa hoon deewana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera aisa hoon deewana"/>
</div>
<div class="lyrico-lyrics-wrapper">Tune ab tak yeh na jaana heeriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tune ab tak yeh na jaana heeriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Gal mitthi mitthi bol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gal mitthi mitthi bol"/>
</div>
<div class="lyrico-lyrics-wrapper">Das kaano vich gol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Das kaano vich gol"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajne de taashe dhol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajne de taashe dhol"/>
</div>
<div class="lyrico-lyrics-wrapper">Masti mein tu vi dol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masti mein tu vi dol"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann de naina tu khol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann de naina tu khol"/>
</div>
<div class="lyrico-lyrics-wrapper">Chahat ke moti rol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chahat ke moti rol"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil hunda ae anmol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil hunda ae anmol"/>
</div>
<div class="lyrico-lyrics-wrapper">Jedo lashke na tol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jedo lashke na tol"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa sohni tenu chaand ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa sohni tenu chaand ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Main choodi pehrawa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main choodi pehrawa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu kar de ishaara te
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu kar de ishaara te"/>
</div>
<div class="lyrico-lyrics-wrapper">Main dholi lai aanwa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main dholi lai aanwa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gal mitthi mitthi bol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gal mitthi mitthi bol"/>
</div>
<div class="lyrico-lyrics-wrapper">Gal mitthi mitthi bol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gal mitthi mitthi bol"/>
</div>
</pre>
