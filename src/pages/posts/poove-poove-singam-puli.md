---
title: "poove poove song lyrics"
album: "Singam Puli"
artist: "Mani Sharma"
lyricist: "Viveka"
director: "Sai Ramani"
path: "/albums/singam-puli-lyrics"
song: "Poove Poove"
image: ../../images/albumart/singam-puli.jpg
date: 2011-03-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dgBwjE1jKgQ"
type: "love"
singers:
  - Rahul Nambiar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poove Poove Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poove Poove Poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai Yettrum Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai Yettrum Poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Paaru Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paaru Poove"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paarthadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paarthadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kattudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kattudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Seigiraai Kradhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Seigiraai Kradhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Paaigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Paaigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ushnam Aagiren Naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushnam Aagiren Naanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poove Poove Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poove Poove Poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai Yettrum Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai Yettrum Poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Paaru Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paaru Poove"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paarthadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paarthadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kattudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kattudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Seigiraai Kradhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Seigiraai Kradhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Paaigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Paaigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ushnam Aagiren Naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushnam Aagiren Naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanadi Naanadi Naanadi Naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanadi Naanadi Naanadi Naanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam Muzhudhum Parandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Muzhudhum Parandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravum Pagalum Azhaindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravum Pagalum Azhaindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi Paarthen Unai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi Paarthen Unai Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagi Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagi Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uzhavu Thuraiyaai Nuzhaindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhavu Thuraiyaai Nuzhaindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu Udalil Irundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu Udalil Irundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhu Pudhidhaai Pudhaiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhu Pudhidhaai Pudhaiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Edukka Aasaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukka Aasaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilai Pola Un Udal Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilai Pola Un Udal Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Parimaara Vendum Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parimaara Vendum Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Devi Prasadham Ena Thinben
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devi Prasadham Ena Thinben"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Paadham Patta Mannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paadham Patta Mannai"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaghil Vizhundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaghil Vizhundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundhen Naan Naan Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhen Naan Naan Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paarthadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paarthadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kattudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kattudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Seigiraai Kradhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Seigiraai Kradhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Paaigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Paaigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ushnam Aagiren Naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushnam Aagiren Naanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poove Poove Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poove Poove Poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai Yettrum Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai Yettrum Poove"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam Paaru Poove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam Paaru Poove"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unarvai Thoondum Udhadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvai Thoondum Udhadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urasi Paarkka Udhavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasi Paarkka Udhavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirakkum Ilamai Kadhavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirakkum Ilamai Kadhavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasa Mullaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasa Mullaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idaiyil Azhagaai Valaivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiyil Azhagaai Valaivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mele Paarthen Nilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mele Paarthen Nilavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasithu Mudikkum Varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasithu Mudikkum Varaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Porumai Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porumai Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvai Veesinaal Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvai Veesinaal Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Theeyil Vendhidaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Theeyil Vendhidaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Podum Aadaiyin Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Podum Aadaiyin Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Motcham Thandhidaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Motcham Thandhidaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Udalaa Kadalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalaa Kadalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhaindhen Naan Naan Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhaindhen Naan Naan Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paarthadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paarthadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Kattudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kattudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Seigiraai Kradhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Seigiraai Kradhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Paaigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Paaigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ushnam Aagiren Naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ushnam Aagiren Naanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanadi Naanadi Naanadi Naanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanadi Naanadi Naanadi Naanadi"/>
</div>
</pre>
