---
title: "idhu varai naan song lyrics"
album: "Zhagaram"
artist: "Dharan Kumar"
lyricist: "Kabilan"
director: "Krish"
path: "/albums/zhagaram-lyrics"
song: "Idhu Varai Naan"
image: ../../images/albumart/zhagaram.jpg
date: 2019-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jOx1J_FYDLo"
type: "love"
singers:
  - Haricharan
  - Swetha Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhu Varai Naan Paarkamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Varai Naan Paarkamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethetho Annen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethetho Annen"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthal Muraiyai Naan Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Muraiyai Naan Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaimoodi Ponnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaimoodi Ponnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thai Masa Isaiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thai Masa Isaiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Vesum Azhaiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Vesum Azhaiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Koosum Azhagaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Koosum Azhagaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellame Neeyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellame Neeyaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koorai Mel Koodiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koorai Mel Koodiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovil Mel Maniyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovil Mel Maniyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarai Mel Paniyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarai Mel Paniyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellame Neeyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellame Neeyaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Pesugira Iruvizhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Pesugira Iruvizhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Thoorugita Mazhaithuliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thoorugita Mazhaithuliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Natkurpin Mudhal Variye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Natkurpin Mudhal Variye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakena Vazhvaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakena Vazhvaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Rathiriyun Nilavozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Rathiriyun Nilavozhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Punaigaiyin Siripozhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Punaigaiyin Siripozhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Paadham Thoodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Paadham Thoodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manalveliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalveliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakena Piranthaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakena Piranthaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Varai Naan Paarkamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Varai Naan Paarkamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethetho Annen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethetho Annen"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthal Muraiyai Naan Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Muraiyai Naan Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaimoodi Ponnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaimoodi Ponnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruvizhiyil Eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvizhiyil Eppothum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kadhal Thanjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kadhal Thanjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruvizhiyil Eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvizhiyil Eppothum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kadhal Thanjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kadhal Thanjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idaiveliye Illamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiveliye Illamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pesu Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pesu Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatru Veesinal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatru Veesinal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhauvugal Thirakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhauvugal Thirakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Reigaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Reigaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgalyil Irupom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgalyil Irupom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaartin Kacheriyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaartin Kacheriyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kadhal Sangeethamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kadhal Sangeethamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Pesugira Iruvizhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Pesugira Iruvizhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Thoorugita Mazhaithuliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thoorugita Mazhaithuliye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Natkurpin Mudhal Variye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Natkurpin Mudhal Variye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakena Vazhvaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakena Vazhvaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Varai Naan Paarkamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Varai Naan Paarkamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethetho Annen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethetho Annen"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthal Muraiyai Naan Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Muraiyai Naan Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaimoodi Ponnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaimoodi Ponnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimaiyileye En Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyileye En Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudakaga Marum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudakaga Marum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimaiyileye En Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimaiyileye En Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudakaga Marum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudakaga Marum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Thuliyai Nee Vanthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Thuliyai Nee Vanthal"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thagam Theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thagam Theerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai Ooramai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai Ooramai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakira Pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakira Pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Selai Pookalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selai Pookalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaruthu Manathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaruthu Manathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Narkaliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Narkaliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Kanngal Utkarnthathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kanngal Utkarnthathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Varai Naan Paarkamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Varai Naan Paarkamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethetho Annen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethetho Annen"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthal Muraiyai Naan Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Muraiyai Naan Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaimoodi Ponnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaimoodi Ponnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thai Masa Isaiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thai Masa Isaiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Vesum Azhaiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Vesum Azhaiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Koosum Azhagaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Koosum Azhagaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellame Neeyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellame Neeyaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koorai Mel Koodiyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koorai Mel Koodiyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovil Mel Maniyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovil Mel Maniyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarai Mel Paniyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarai Mel Paniyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellame Neeyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellame Neeyaga"/>
</div>
</pre>
