---
title: "pee loon tere song lyrics"
album: "Once Upon a Time in Mumbai"
artist: "Pritam Chakraborty"
lyricist: "Irshaad Kamil"
director: "Milan Luthria"
path: "/albums/once-upon-a-time-in-mumbai-lyrics"
song: "Pee Loon Tere"
image: ../../images/albumart/once-upon-a-time-in-mumbai.jpg
date: 2010-07-30
lang: hindi
youtubeLink: "https://www.youtube.com/embed/D8XFTglfSMg"
type: "love"
singers:
  - Mohit Chauhan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pee loon tere neele neele nainon se shabnam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pee loon tere neele neele nainon se shabnam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pee loon tere geele geele hothon ki sargam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pee loon tere geele geele hothon ki sargam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pee loon hai peene ka mausam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pee loon hai peene ka mausam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere sang ishq taari hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang ishq taari hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang ik khumari hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang ik khumari hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang chain bhi mujhko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang chain bhi mujhko"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang bekarari hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang bekarari hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere bin jee nahi lagta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere bin jee nahi lagta"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere bin jee nahi sakta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere bin jee nahi sakta"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhpe hai haare maine vaare do jahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhpe hai haare maine vaare do jahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Qurbaan, meharbaan, ke main to qurbaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Qurbaan, meharbaan, ke main to qurbaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sun le zara, tera qurbaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sun le zara, tera qurbaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hosh mein rahoon kyun aaj main
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hosh mein rahoon kyun aaj main"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu meri baahon mein simti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu meri baahon mein simti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhme samaayi hai yoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhme samaayi hai yoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Jis tarah ki koyi ho nadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jis tarah ki koyi ho nadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu mere seene mein chhupti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu mere seene mein chhupti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagar tumhara main hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagar tumhara main hoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pee loon teri dheemi dheemi lehron ki chham chham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pee loon teri dheemi dheemi lehron ki chham chham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pee loon teri saundhi saundhi saanson ko hardum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pee loon teri saundhi saundhi saanson ko hardum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pee loon hai peene ka mausam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pee loon hai peene ka mausam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere sang ishq taari hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang ishq taari hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang ik khumari hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang ik khumari hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang chain bhi mujhko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang chain bhi mujhko"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang bekarari hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang bekarari hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shaam ko miloon jo main tujhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaam ko miloon jo main tujhe"/>
</div>
<div class="lyrico-lyrics-wrapper">To bura subah na jaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To bura subah na jaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun kuch maan jaati hai yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun kuch maan jaati hai yeh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Har lamha, har ghadi har pehar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har lamha, har ghadi har pehar"/>
</div>
<div class="lyrico-lyrics-wrapper">Hi teri yaadon se tadpa ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hi teri yaadon se tadpa ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhko jalaati hai yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhko jalaati hai yeh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pee loon main dheere dheere jalne ka yeh gham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pee loon main dheere dheere jalne ka yeh gham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pee loon in gore gore haathon se humdum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pee loon in gore gore haathon se humdum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pee loon hai peene ka mausam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pee loon hai peene ka mausam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere sang ishq taari hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang ishq taari hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang ik khumari hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang ik khumari hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang chain bhi mujhko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang chain bhi mujhko"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang bekarari hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang bekarari hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere sang ishq taari hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang ishq taari hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang ik khumari hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang ik khumari hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang chain bhi mujhko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang chain bhi mujhko"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere sang bekarari hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere sang bekarari hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tere bin jee nahi lagta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere bin jee nahi lagta"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere bin jee nahi sakta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere bin jee nahi sakta"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhpe hai haare maine vaare do jahaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhpe hai haare maine vaare do jahaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Qurbaan, meharbaan, ke main to qurbaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Qurbaan, meharbaan, ke main to qurbaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sun le zara, tera qurbaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sun le zara, tera qurbaan"/>
</div>
</pre>
