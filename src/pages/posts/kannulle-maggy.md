---
title: "kannulle song lyrics"
album: "Maggy"
artist: "UM Steven Sathish"
lyricist: "UM stevan sathish"
director: "Kartikeyen"
path: "/albums/maggy-lyrics"
song: "Kannulle"
image: ../../images/albumart/maggy.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pGq2Vonx-IM"
type: "mass"
singers:
  - U.M. Stevan Sathish
  - Nancy sylvia  
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kannulle kannulee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannulle kannulee"/>
</div>
<div class="lyrico-lyrics-wrapper">kannirai thanthavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannirai thanthavale"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai meeri tholai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai meeri tholai "/>
</div>
<div class="lyrico-lyrics-wrapper">thooram sellathe sellathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram sellathe sellathe"/>
</div>
<div class="lyrico-lyrics-wrapper">en nizhale en nizhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nizhale en nizhale"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai vittu piriyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai vittu piriyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">enna nee enna nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nee enna nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kollathe kollathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollathe kollathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adiye thadumari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye thadumari "/>
</div>
<div class="lyrico-lyrics-wrapper">nanum sakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum sakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">unna partha kangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna partha kangal "/>
</div>
<div class="lyrico-lyrics-wrapper">kangal yengi poguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal yengi poguthey"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye thadumari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye thadumari"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum sakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum sakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">unna partha kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna partha kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal yengi poguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal yengi poguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannulle kannulee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannulle kannulee"/>
</div>
<div class="lyrico-lyrics-wrapper">kannirai thanthavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannirai thanthavale"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai meeri tholai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai meeri tholai "/>
</div>
<div class="lyrico-lyrics-wrapper">thooram sellathe sellathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram sellathe sellathe"/>
</div>
<div class="lyrico-lyrics-wrapper">en nizhale en nizhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nizhale en nizhale"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai vittu piriyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai vittu piriyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">enna nee enna nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nee enna nee"/>
</div>
<div class="lyrico-lyrics-wrapper">kollathe kollathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollathe kollathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unthan udambil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan udambil"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanai marmangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanai marmangal"/>
</div>
<div class="lyrico-lyrics-wrapper">un kannai kandi pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannai kandi pozhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">penne ne than enthan devathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne ne than enthan devathai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee illamal naan vaadinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee illamal naan vaadinen"/>
</div>
<div class="lyrico-lyrics-wrapper">nee engey endru thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee engey endru thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai thedi nane tholanthenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedi nane tholanthenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai vittu engu sendrai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai vittu engu sendrai "/>
</div>
<div class="lyrico-lyrics-wrapper">patra theeyai vaitha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patra theeyai vaitha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">indru kozhunthu vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru kozhunthu vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">eriyuthu manasu thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eriyuthu manasu thana"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai vittu engu sendrai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai vittu engu sendrai "/>
</div>
<div class="lyrico-lyrics-wrapper">patra theeyai vaitha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patra theeyai vaitha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">indru kozhunthu vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru kozhunthu vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">eriyuthu manasu thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eriyuthu manasu thana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai neengamal neengamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai neengamal neengamal"/>
</div>
<div class="lyrico-lyrics-wrapper">pen nanaga nanaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pen nanaga nanaga"/>
</div>
<div class="lyrico-lyrics-wrapper">manam maaramal veenamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam maaramal veenamal"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhvenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhvenada"/>
</div>
<div class="lyrico-lyrics-wrapper">imai moodamal moodamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imai moodamal moodamal"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai thedamal thedamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedamal thedamal"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir aga unnil nanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir aga unnil nanum "/>
</div>
<div class="lyrico-lyrics-wrapper">irupenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupenada"/>
</div>
<div class="lyrico-lyrics-wrapper">yei pulla yei pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei pulla yei pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">enai vittu yen pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai vittu yen pora"/>
</div>
<div class="lyrico-lyrics-wrapper">kannalla enna nee kollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannalla enna nee kollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kannulle kannulle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannulle kannulle "/>
</div>
<div class="lyrico-lyrics-wrapper">kathala vacha pull
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathala vacha pull"/>
</div>
<div class="lyrico-lyrics-wrapper">enna nee enna nee kollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nee enna nee kollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye nanum sakurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye nanum sakurene"/>
</div>
<div class="lyrico-lyrics-wrapper">unna partha kangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna partha kangal "/>
</div>
<div class="lyrico-lyrics-wrapper">kangal yengi poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal yengi poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye nanum sakurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye nanum sakurene"/>
</div>
<div class="lyrico-lyrics-wrapper">unna partha kangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna partha kangal "/>
</div>
<div class="lyrico-lyrics-wrapper">kangal yengi poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal yengi poguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en manasa kizhu kittu poravale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasa kizhu kittu poravale"/>
</div>
<div class="lyrico-lyrics-wrapper">en manasa pisu pisa udachitu pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en manasa pisu pisa udachitu pora"/>
</div>
<div class="lyrico-lyrics-wrapper">penne nee enthan thevatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne nee enthan thevatha"/>
</div>
<div class="lyrico-lyrics-wrapper">nee enga endru thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee enga endru thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">unna thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">nanum alanchen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum alanchen"/>
</div>
<div class="lyrico-lyrics-wrapper">aanalum unna nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanalum unna nan"/>
</div>
<div class="lyrico-lyrics-wrapper">marakka mudiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marakka mudiyama"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivale kanavale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivale kanavale "/>
</div>
<div class="lyrico-lyrics-wrapper">verukka mudiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verukka mudiyama"/>
</div>
<div class="lyrico-lyrics-wrapper">thavikiren thudikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavikiren thudikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">inga nee illama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inga nee illama "/>
</div>
<div class="lyrico-lyrics-wrapper">kathuren katharuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathuren katharuran"/>
</div>
<div class="lyrico-lyrics-wrapper">enna nane verukuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna nane verukuran"/>
</div>
<div class="lyrico-lyrics-wrapper">udambai kathalika villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udambai kathalika villai"/>
</div>
<div class="lyrico-lyrics-wrapper">manathai kathalikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathai kathalikka"/>
</div>
<div class="lyrico-lyrics-wrapper">antha manathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha manathai"/>
</div>
<div class="lyrico-lyrics-wrapper">kana villai enbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kana villai enbathu"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">aaviya nee antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaviya nee antha"/>
</div>
<div class="lyrico-lyrics-wrapper">kashtathil naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kashtathil naan"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai parikodutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai parikodutha"/>
</div>
<div class="lyrico-lyrics-wrapper">udambodu nachtathil naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udambodu nachtathil naan"/>
</div>
<div class="lyrico-lyrics-wrapper">penne nee peyendru ninaikava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne nee peyendru ninaikava"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai thedi thedi enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thedi thedi enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal urugava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal urugava"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai ninaithu intha ulagil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai ninaithu intha ulagil"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum sagava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum sagava"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai thiruppi kodukumaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thiruppi kodukumaru"/>
</div>
<div class="lyrico-lyrics-wrapper">antha kadavulidam vendavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha kadavulidam vendavaa"/>
</div>
</pre>
