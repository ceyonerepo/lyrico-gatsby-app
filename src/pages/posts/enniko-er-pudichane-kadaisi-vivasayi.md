---
title: "enniko er pudichane song lyrics"
album: "Kadaisi Vivasayi"
artist: "Santhosh Narayanan"
lyricist: "unknown"
director: "M. Manikandan"
path: "/albums/kadaisi-vivasayi-lyrics"
song: "Enniko Er Pudichane"
image: ../../images/albumart/kadaisi-vivasayi.jpg
date: 2022-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/N0D9jm6pO8Q"
type: "mass"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ennaiko er puduchane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaiko er puduchane"/>
</div>
<div class="lyrico-lyrics-wrapper">namma munnoro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma munnoro"/>
</div>
<div class="lyrico-lyrics-wrapper">annaike sooda vachane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annaike sooda vachane"/>
</div>
<div class="lyrico-lyrics-wrapper">kollaike veli nattalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kollaike veli nattalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kuraivu illamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuraivu illamale"/>
</div>
<div class="lyrico-lyrics-wrapper">vandukum paatha vitane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandukum paatha vitane"/>
</div>
<div class="lyrico-lyrics-wrapper">eh mambati ellam seru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh mambati ellam seru"/>
</div>
<div class="lyrico-lyrics-wrapper">athu ulla vantha soru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu ulla vantha soru"/>
</div>
<div class="lyrico-lyrics-wrapper">thonda kuli neeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thonda kuli neeru"/>
</div>
<div class="lyrico-lyrics-wrapper">athu manusavanga saaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu manusavanga saaru"/>
</div>
<div class="lyrico-lyrics-wrapper">annatha vaari thanthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annatha vaari thanthane"/>
</div>
<div class="lyrico-lyrics-wrapper">thane nilatha keeri thanthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane nilatha keeri thanthane"/>
</div>
<div class="lyrico-lyrics-wrapper">vanatha saarnthu irunthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanatha saarnthu irunthane"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaikkum aathi sanam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaikkum aathi sanam than"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthu irunthaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthu irunthaane"/>
</div>
<div class="lyrico-lyrics-wrapper">ye kanji kudutha boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye kanji kudutha boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">panja pootham namma saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panja pootham namma saami"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaakura thaaye vitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaakura thaaye vitha"/>
</div>
<div class="lyrico-lyrics-wrapper">nelluku vivasaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nelluku vivasaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vandukum nandukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandukum nandukum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaakaa kunjukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaakaa kunjukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">thookanaa kuruvikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookanaa kuruvikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">naathu nataa santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naathu nataa santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">kallukum sillukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallukum sillukum"/>
</div>
<div class="lyrico-lyrics-wrapper">pesum nellum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesum nellum "/>
</div>
<div class="lyrico-lyrics-wrapper">moochu iraikum pullukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochu iraikum pullukum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu adicha urasum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu adicha urasum"/>
</div>
<div class="lyrico-lyrics-wrapper">ye kannankaru aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye kannankaru aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">velli poni thoova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velli poni thoova"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha pulla en kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha pulla en kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">othukoda saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othukoda saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">sandakara ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandakara ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">enna vila naan koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vila naan koora"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli vida ponale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli vida ponale"/>
</div>
<div class="lyrico-lyrics-wrapper">en nella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye vethaiyadi vellama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye vethaiyadi vellama"/>
</div>
<div class="lyrico-lyrics-wrapper">than velanchu kumbuduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than velanchu kumbuduvom"/>
</div>
<div class="lyrico-lyrics-wrapper">kummi adi kununju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kummi adi kununju"/>
</div>
<div class="lyrico-lyrics-wrapper">mannukulla verum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannukulla verum "/>
</div>
<div class="lyrico-lyrics-wrapper">serthu vaikum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serthu vaikum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu patta thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu patta thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru ilukkum theru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru ilukkum theru"/>
</div>
<div class="lyrico-lyrics-wrapper">ye kannankaru aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye kannankaru aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">velli poni thoova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velli poni thoova"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha pulla en kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha pulla en kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">othukoda saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othukoda saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">sandakara ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sandakara ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">enna vila naan koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vila naan koora"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli vida ponale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli vida ponale"/>
</div>
<div class="lyrico-lyrics-wrapper">en nella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennaiko er puduchane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaiko er puduchane"/>
</div>
<div class="lyrico-lyrics-wrapper">ennaiko er puduchane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennaiko er puduchane"/>
</div>
</pre>
