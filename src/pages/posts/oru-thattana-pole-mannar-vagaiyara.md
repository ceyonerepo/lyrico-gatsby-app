---
title: "oru thattana pole song lyrics"
album: "Mannar Vagaiyara"
artist: "Jakes Bejoy"
lyricist: "Mani Amudhavan"
director: "G. Boopathy Pandian"
path: "/albums/mannar-vagaiyara-lyrics"
song: "Oru Thattana Pole"
image: ../../images/albumart/mannar-vagaiyara.jpg
date: 2018-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zTQcAYOOMlY"
type: "love"
singers:
  - D. Sathyaprakash
  - Vandana Srinivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru thattana pola thattana pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thattana pola thattana pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paranthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paranthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru chittaaga thanae chittaaga thanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru chittaaga thanae chittaaga thanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan methanthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan methanthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kunjondu parthen kondanu ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunjondu parthen kondanu ketten"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaanu manasa kuduthutiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaanu manasa kuduthutiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda azhaga ukkandhu pazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda azhaga ukkandhu pazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">En peril muzhusa eluthitiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En peril muzhusa eluthitiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan sitthu erumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sitthu erumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyil nadakiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil nadakiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna sakkaraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna sakkaraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyilae somakkurenaaannn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyilae somakkurenaaannn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru thattana pola thattana pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thattana pola thattana pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paranthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paranthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru chittaaga thanae chittaaga thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru chittaaga thanae chittaaga thane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan methanthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan methanthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathu patta puluthiya pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu patta puluthiya pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai pattu kelambunen mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai pattu kelambunen mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooralla thoovunen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooralla thoovunen"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangunen keezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangunen keezha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanjaa mattai kadamuda pecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanjaa mattai kadamuda pecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathu vechi kekura kichaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathu vechi kekura kichaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruviya vanthu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruviya vanthu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkinnu vaacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkinnu vaacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veyiloda mazhaiyumae sernthu adikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyiloda mazhaiyumae sernthu adikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha azhagiya adhisayam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha azhagiya adhisayam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppo eppo kitta kitta nee varuvenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppo eppo kitta kitta nee varuvenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni kittu irunthen naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni kittu irunthen naala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu ellam terinji thanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu ellam terinji thanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un arugil vanthen naanaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un arugil vanthen naanaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thattanaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thattanaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattanaa paranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattanaa paranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru chittaanaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru chittaanaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittaanaaa methanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittaanaaa methanthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aala vittu pakura azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala vittu pakura azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatthi ippo inikithu mozhagha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatthi ippo inikithu mozhagha"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi ippadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi ippadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ennai maathuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ennai maathuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalankatti mazhaiyai eranjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalankatti mazhaiyai eranjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattu kutti kanakka thirinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattu kutti kanakka thirinjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Elaiya nee neettuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaiya nee neettuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiya naan aatuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiya naan aatuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chekka chekka sevanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chekka chekka sevanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannum kannum than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum kannum than"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna panni tholacha ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna panni tholacha ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootti kunjom kulachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootti kunjom kulachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Parthen en nenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parthen en nenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Michammagha neeyum vanthu ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Michammagha neeyum vanthu ninna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mela irukkavan potta kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela irukkavan potta kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenju than unakku veeduuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenju than unakku veeduuuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru thattana pola thattana polaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thattana pola thattana polaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paranthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paranthenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru chittaaga thanae chittaaga thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru chittaaga thanae chittaaga thane"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan methanthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan methanthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kunjomdu partha kondanu ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunjomdu partha kondanu ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaanu manasa kuduthitennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaanu manasa kuduthitennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda azhaga eppodhum pazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda azhaga eppodhum pazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Un peril muzhusa eluthitennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un peril muzhusa eluthitennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmmmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmmmmmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmmmmmmmmmm"/>
</div>
</pre>
