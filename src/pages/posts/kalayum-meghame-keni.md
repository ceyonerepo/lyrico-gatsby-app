---
title: "kalayum meghame song lyrics"
album: "Keni"
artist: "M. Jayachandran"
lyricist: "Palani Bharathi"
director: "M.A. Nishad"
path: "/albums/keni-song-lyrics"
song: "Kalayum Meghame"
image: ../../images/albumart/keni.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/S8vBFFo578o"
type: "sad"
singers:
  - Sithara Krishnakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kalaiyum megamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiyum megamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaiyum megamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiyum megamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyum paadhaigal kalangudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyum paadhaigal kalangudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyil eeramaai manadhil baaramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyil eeramaai manadhil baaramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyin vaasanai urangudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyin vaasanai urangudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal sudar yaethi ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal sudar yaethi ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir mazhaiyae nee engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir mazhaiyae nee engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyaamal puriyaamal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaamal puriyaamal "/>
</div>
<div class="lyrico-lyrics-wrapper">vizhi neeril moozhgudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi neeril moozhgudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaiyum megamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiyum megamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaiyum megamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiyum megamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyum paadhaigal kalangudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyum paadhaigal kalangudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhil vaanavil nirangal eriyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhil vaanavil nirangal eriyudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivaa unnidam karunai illaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivaa unnidam karunai illaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal oridam urakkam oridam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal oridam urakkam oridam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum paadhai maari ponadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum paadhai maari ponadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaiyum megamaeKalaiyum megamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiyum megamaeKalaiyum megamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyum paadhaigal kalangudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyum paadhaigal kalangudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paravai koottilae paravai illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai koottilae paravai illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkum vaanamum thelivaai illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkum vaanamum thelivaai illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragai thaedidum paravai payanathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai thaedidum paravai payanathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam ondrai ondru saerumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam ondrai ondru saerumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaiyum megamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiyum megamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaiyum megamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiyum megamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyum paadhaigal kalangudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyum paadhaigal kalangudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyil eeramaai manadhil baaramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyil eeramaai manadhil baaramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyin vaasanai urangudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyin vaasanai urangudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal sudar yaethi ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal sudar yaethi ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir mazhaiyae nee engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir mazhaiyae nee engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyaamal puriyaamal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyaamal puriyaamal "/>
</div>
<div class="lyrico-lyrics-wrapper">vizhi neeril moozhgudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhi neeril moozhgudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyil eeramaai manadhil baaramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyil eeramaai manadhil baaramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyin vaasanai urangudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyin vaasanai urangudhae"/>
</div>
</pre>
