---
title: "poo virinchachu song lyrics"
album: "Mugavari"
artist: "Deva"
lyricist: "Vairamuthu"
director: "V. Z. Durai"
path: "/albums/mugavari-lyrics"
song: "Poo Virinchachu"
image: ../../images/albumart/mugavari.jpg
date: 2000-02-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GXX4TIfw5bY"
type: "Love"
singers:
  - Unnikrishnan
  - Anuradha Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poo Virinjaachchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Virinjaachchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaen Vizhundhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen Vizhundhaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuga Varuga Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuga Varuga Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvaal Rusi Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvaal Rusi Kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Enakkullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enakkullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unakkullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unakkullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivadhedhu Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivadhedhu Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai Parimaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Parimaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilaiyil Pasumai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaiyil Pasumai Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Inaindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Inaindhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjil Thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Nadappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Nadappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavin Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavin Melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo Virinjaachchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Virinjaachchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaen Vizhundhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen Vizhundhaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuga Varuga Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuga Varuga Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvaal Rusi Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvaal Rusi Kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo Pooththirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Pooththirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullai Kodigaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullai Kodigaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo Pooththu Vaiththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Pooththu Vaiththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththu Irungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththu Irungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumana Maalaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumana Maalaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaedhi Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaedhi Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Pariththu Kolvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pariththu Kolvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thean Sumandhirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean Sumandhirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thennai Marangaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thennai Marangaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thean Serththu Vaiththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean Serththu Vaiththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaththu Irungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaththu Irungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumana Iravukku Thevaippadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumana Iravukku Thevaippadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduththu Kolvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduththu Kolvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megangaal Sindhaamal Nindraadungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megangaal Sindhaamal Nindraadungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeraada Panneeraith Thaan Thoovungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeraada Panneeraith Thaan Thoovungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththam Sindha Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Sindha Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannodu Kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannodu Kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththu Kulithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththu Kulithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjodu…nenjodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu…nenjodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththaththil Unnai Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththaththil Unnai Kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo Virinjaachchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Virinjaachchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaen Vizhundhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen Vizhundhaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuga Varuga Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuga Varuga Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvaal Rusi Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvaal Rusi Kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Mounangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mounangalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavi Padiththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavi Padiththen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Seigaigalil Mozhi Peyarththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Seigaigalil Mozhi Peyarththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaththin Saayaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaththin Saayaththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththamittu Muththamittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththamittu Muththamittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Karaiththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Karaiththaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kanavugalin Uruvangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanavugalin Uruvangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kaatril Vandhu Padam Pidiththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kaatril Vandhu Padam Pidiththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaigalin Oligalil Vaaliba Thookkaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaigalin Oligalil Vaaliba Thookkaththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaiththu Vittaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaiththu Vittaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unn Maarbil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Maarbil"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttaalum Kutraalamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttaalum Kutraalamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Perai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Perai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnaalum Sangeedhamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnaalum Sangeedhamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththam Koduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Koduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaadhae Sollaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaadhae Sollaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli Cholliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli Cholliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollaadhae Kollaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollaadhae Kollaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Kaigal Idam Maarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Kaigal Idam Maarudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo Virinjaachchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo Virinjaachchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaen Vizhundhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen Vizhundhaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuga Varuga Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuga Varuga Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvaal Rusi Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvaal Rusi Kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Enakkullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enakkullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unakkullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unakkullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivadhedhu Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivadhedhu Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirai Parimaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirai Parimaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilaiyil Pasumai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaiyil Pasumai Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Inaindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Inaindhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjil Thannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Thannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Nadappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Nadappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavin Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavin Melae"/>
</div>
</pre>
