---
title: "wildu strawberry song lyrics"
album: "Murungakkai Chips"
artist: "Dharan Kumar"
lyricist: "	Ravindhar Chandrasekaran - Mali"
director: "Srijar"
path: "/albums/murungakkai-chips-song-lyrics"
song: "Wildu Strawberry"
image: ../../images/albumart/murungakkai-chips.jpg
date: 2021-12-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0sxvkmYW2tQ"
type: "love"
singers:
  - Priya Mali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Wildu strawberry romantic scenery
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wildu strawberry romantic scenery"/>
</div>
<div class="lyrico-lyrics-wrapper">Hot meltu pannuriya hotu jaggery
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot meltu pannuriya hotu jaggery"/>
</div>
<div class="lyrico-lyrics-wrapper">Calling venpaani haunting symphony
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Calling venpaani haunting symphony"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweetu yengi thedugira kaatu erumbu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweetu yengi thedugira kaatu erumbu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavu yengura nee stunning cuteu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu yengura nee stunning cuteu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura pirichi meyuriyaa enna mirugam daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura pirichi meyuriyaa enna mirugam daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai pidichathal kadhal grahanam pidichathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai pidichathal kadhal grahanam pidichathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti paarthu kadhal methuva poranthachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti paarthu kadhal methuva poranthachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukke naan thaan da beauty queenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukke naan thaan da beauty queenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enn skinu alcoholic red wineu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn skinu alcoholic red wineu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enn loveuku illaiye da endu lineu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn loveuku illaiye da endu lineu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mattum podhum everythingu will be fineu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mattum podhum everythingu will be fineu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Week endu laa naan one and only ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Week endu laa naan one and only ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta maadi mela ninna romba lonely ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta maadi mela ninna romba lonely ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal pol thakkunu thaan neeyum vanthiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal pol thakkunu thaan neeyum vanthiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannuliyaa pesuniyaa short and sweetu ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannuliyaa pesuniyaa short and sweetu ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Little heartil little villu ah suththa vittiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little heartil little villu ah suththa vittiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna moodi un pinnadi oda vittuye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna moodi un pinnadi oda vittuye"/>
</div>
<div class="lyrico-lyrics-wrapper">Topu scriptu laa ippa hotu gripu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Topu scriptu laa ippa hotu gripu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un lappu thappu marking panna viduviya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un lappu thappu marking panna viduviya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukke naan thaan da beauty queenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukke naan thaan da beauty queenu"/>
</div>
<div class="lyrico-lyrics-wrapper">En skinu alcoholic red wineu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En skinu alcoholic red wineu"/>
</div>
<div class="lyrico-lyrics-wrapper">En loveu kuu illaiye da endu lineu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En loveu kuu illaiye da endu lineu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mattum podhum everythingu will be fineu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mattum podhum everythingu will be fineu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Walking porathu nalla weight ah kuraikka thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Walking porathu nalla weight ah kuraikka thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hot weightu yethuriya enna nyayam maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot weightu yethuriya enna nyayam maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Feelings oda naan theruvil nadanthu ponen naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feelings oda naan theruvil nadanthu ponen naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Street lightu thalaiya katti kadhaiya ketkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Street lightu thalaiya katti kadhaiya ketkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai nenachu kanna moodi thoonga poguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai nenachu kanna moodi thoonga poguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Verai yaarum vanthu ninna kanavu kalaikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verai yaarum vanthu ninna kanavu kalaikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vittu thaan naanum thooram ponenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vittu thaan naanum thooram ponenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulla muzhunguna vazhiya manasu vazhikkuthu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulla muzhunguna vazhiya manasu vazhikkuthu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poda bachcha!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda bachcha!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukke naan thaan daa beauty queenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukke naan thaan daa beauty queenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enn skinu alcoholic red wineu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn skinu alcoholic red wineu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enn loveu ku illaiye da endu lineu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn loveu ku illaiye da endu lineu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mattum podhum everything will be fineu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mattum podhum everything will be fineu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukke naan thaan da beauty queenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukke naan thaan da beauty queenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enn skinu alcoholic red wineu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn skinu alcoholic red wineu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enn loveu ku illaiye da endu lineu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn loveu ku illaiye da endu lineu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee mattum podhum everything will be fineu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mattum podhum everything will be fineu"/>
</div>
</pre>
