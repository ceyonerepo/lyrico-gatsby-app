---
title: "kabaddi kabaddi song lyrics"
album: "Vennila Kabaddi Kuzhu 2"
artist: "V. Selvaganesh"
lyricist: "Kabilan "
director: "Selva Sekaran"
path: "/albums/vennila-kabaddi-kuzhu-2-lyrics"
song: "Kabaddi Kabaddi"
image: ../../images/albumart/vennila-kabaddi-kuzhu-2.jpg
date: 2019-07-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZcKA5Ri5ETY"
type: "mass"
singers:
  - Shankar Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kabaddi Kabaddi Kabaddi Kabaddi Kabaddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabaddi Kabaddi Kabaddi Kabaddi Kabaddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabaddi Kabaddi Kabaddi Kabaddi Kabaddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabaddi Kabaddi Kabaddi Kabaddi Kabaddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabaddi Kabaddi Kabaddi Kabaddi Kabaddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabaddi Kabaddi Kabaddi Kabaddi Kabaddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabaddi Kabaddi Kabaddi Kabaddi Kabaddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabaddi Kabaddi Kabaddi Kabaddi Kabaddi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puligalin Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puligalin Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Valimayin Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valimayin Kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyalena Needhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalena Needhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Parunthu Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parunthu Vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Jayikka Yavaniruppan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Jayikka Yavaniruppan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurudhiyil Kobham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurudhiyil Kobham"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukum Varayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukum Varayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhiraiyin Paaychal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhiraiyin Paaychal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhu Muraiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu Muraiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevanoruvan Thadaividhipaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevanoruvan Thadaividhipaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udal Vegam Kolvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Vegam Kolvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thidai Yeri Selvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thidai Yeri Selvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thisai Yettum Velvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thisai Yettum Velvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaku Thunaiyae Unnakku Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaku Thunaiyae Unnakku Neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetriyin Vazhiyil – Undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriyin Vazhiyil – Undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikalai Veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikalai Veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhanai Seyvai – Yendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhanai Seyvai – Yendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarithiram Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarithiram Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetriyin Vazhiyil – Undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriyin Vazhiyil – Undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikalai Veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikalai Veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhanai Seyvai – Yendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhanai Seyvai – Yendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarithiram Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarithiram Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabaddi Kabaddi Kabaddi Kabaddi Kabaddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabaddi Kabaddi Kabaddi Kabaddi Kabaddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabaddi Kabaddi Kabaddi Kabaddi Kabaddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabaddi Kabaddi Kabaddi Kabaddi Kabaddi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaru Koodi Kabaddi Kabaddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru Koodi Kabaddi Kabaddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhu Peru Kabaddi Kabaddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhu Peru Kabaddi Kabaddi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Paaththu Kabaddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Paaththu Kabaddi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irumbu Undhan Karamae – Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irumbu Undhan Karamae – Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vairam Paiyandha Maramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vairam Paiyandha Maramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodaikala Veyilai Suduvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodaikala Veyilai Suduvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yudham Ondru Varumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yudham Ondru Varumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Mutti Modhi Vidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Mutti Modhi Vidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Vetri Muthamaga Tharumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Vetri Muthamaga Tharumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Netriyil Illai Thalaikkanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netriyil Illai Thalaikkanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetriyae Undhan Illakkanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriyae Undhan Illakkanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suttriyae Odu Dhinam Dhinam – Unai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttriyae Odu Dhinam Dhinam – Unai"/>
</div>
<div class="lyrico-lyrics-wrapper">Patriyae Pesum Tamilinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patriyae Pesum Tamilinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnal Unnal Unnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal Unnal Unnal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnal Unnal Unnal Dhane Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal Unnal Unnal Dhane Nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetriyin Vazhiyil Undhan Kaigalai Veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriyin Vazhiyil Undhan Kaigalai Veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhanai Seyvai Yendrum Sarithiram Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhanai Seyvai Yendrum Sarithiram Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetriyin Vazhiyil Undhan Kaigalai Veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriyin Vazhiyil Undhan Kaigalai Veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhanai Seyvai Yendrum Sarithiram Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhanai Seyvai Yendrum Sarithiram Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabaddi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabaddi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kobham Illa Nenjam – Adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobham Illa Nenjam – Adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyil Illa Deivam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyil Illa Deivam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Rendum Theyin Niramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Rendum Theyin Niramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veli Illai Unakku – Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli Illai Unakku – Oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettai Aadi Pazhagu – Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettai Aadi Pazhagu – Un"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri Koppai Vendru Viduvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri Koppai Vendru Viduvai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyalin Kadhavai Thirandhidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalin Kadhavai Thirandhidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhudhi Kaatrai Parandhidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhudhi Kaatrai Parandhidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhai Indru Pirandhidu – Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhai Indru Pirandhidu – Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugazhin Uttchm Adaindhidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugazhin Uttchm Adaindhidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnal Unnal Unnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal Unnal Unnal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnal Unnal Unnal Dhane Nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal Unnal Unnal Dhane Nanba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetriyin Vazhiyil – Undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriyin Vazhiyil – Undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikalai Veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikalai Veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhanai Seyvai – Yendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhanai Seyvai – Yendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarithiram Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarithiram Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetriyin Vazhiyil – Undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetriyin Vazhiyil – Undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikalai Veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikalai Veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhanai Seyvai – Yendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhanai Seyvai – Yendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarithiram Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarithiram Pesa"/>
</div>
</pre>
