---
title: 'rainbow thiralil song lyrics'
album: 'Takkar'
artist: 'Nivas K. Prasanna'
lyricist: 'Arivarasan'
director: 'Karthik G Krish'
path: '/albums/takkar-song-lyrics'
song: 'Rainbow Thiralil'
image: ../../images/albumart/takkar.jpg
date: 2020-01-01
singers: 
- T.R.Silambarasan
- Andrea Jeremiah
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/aYnNEDUnO5g'
type: 'love'
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Hoo ooo ooo hoo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo ooo hoo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">hoo hoo hooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="hoo hoo hooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo ooo hoo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo ooo hoo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">hoo hoo hooo oooooo…..
<input type="checkbox" class="lyrico-select-lyric-line" value="hoo hoo hooo oooooo….."/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hoo hoo ooo oo ooo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo hoo hoo ooo oo ooo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo hoo hooo oooo ooo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hooo hoo hooo oooo ooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo ooo …((<div class="lyrico-lyrics-wrapper">4) times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo ooo …((<div class="lyrico-lyrics-wrapper">4) times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rainbow thiralil
<input type="checkbox" class="lyrico-select-lyric-line" value="Rainbow thiralil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vannam kooduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru vannam kooduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind blow kanavil
<input type="checkbox" class="lyrico-select-lyric-line" value="Mind blow kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu ennam moodhuthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudhu ennam moodhuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Heart-oh urugi
<input type="checkbox" class="lyrico-select-lyric-line" value="Heart-oh urugi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhurangam aaduthae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sadhurangam aaduthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naetro vilagi
<input type="checkbox" class="lyrico-select-lyric-line" value="Naetro vilagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu indrum aanathae….
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudhu indrum aanathae…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sevaanam streetaai maarudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sevaanam streetaai maarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadivaalam podaa kaalam ingae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadivaalam podaa kaalam ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam podudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aattam podudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirkaalam kaanal neeraodaiyaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethirkaalam kaanal neeraodaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottram kaanudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thottram kaanudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini pogum dhooramae kankaanadhae……ae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini pogum dhooramae kankaanadhae……ae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaththi thaththi thaavu da
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaththi thaththi thaavu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu vettru theevadaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu vettru theevadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ettuthikkum paaradaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee ettuthikkum paaradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli vattam thaanadaa ((<div class="lyrico-lyrics-wrapper">2) times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Oli vattam thaanadaa ((<div class="lyrico-lyrics-wrapper">2) times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hoo hooo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo hooo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hooo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo hooo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hooo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo hooo oo hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hooo oo hoo oo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo hooo oo hoo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Eye brow varigal
<input type="checkbox" class="lyrico-select-lyric-line" value="Eye brow varigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu arththam kaanudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudhu arththam kaanudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Cobra vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Cobra vizhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli vetkam soozhudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thuli vetkam soozhudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Highway malaril
<input type="checkbox" class="lyrico-select-lyric-line" value="Highway malaril"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam hijack aanadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Manam hijack aanadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ainthaam pulanil
<input type="checkbox" class="lyrico-select-lyric-line" value="Ainthaam pulanil"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru train dhaan oodudhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru train dhaan oodudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ven megam vaccum aanadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ven megam vaccum aanadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaamal thaedal theeradhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniyaamal thaedal theeradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan fortune modelae
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan fortune modelae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyaalam engum thedaadhae
<input type="checkbox" class="lyrico-select-lyric-line" value="Adiyaalam engum thedaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaegam statute polavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhaegam statute polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mocktail mangaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru mocktail mangaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini naan dhaanae…ae…..
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini naan dhaanae…ae….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaththi thaththi thaavu da
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaththi thaththi thaavu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu vettru theevadaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu vettru theevadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ettuthikkum paaradaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee ettuthikkum paaradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oli vattam thaanadaa ((<div class="lyrico-lyrics-wrapper">2) times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Oli vattam thaanadaa ((<div class="lyrico-lyrics-wrapper">2) times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hoo ooo ooo hoo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo ooo hoo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hoo hooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo hoo hooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo ooo hoo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo ooo ooo hoo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hoo hooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoo hoo hooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Theerattum baaramae
<input type="checkbox" class="lyrico-select-lyric-line" value="Theerattum baaramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada thukkam edhukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada thukkam edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaedattum google-ae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaedattum google-ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagaththai odhukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulagaththai odhukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaaiyathin verilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaaiyathin verilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli raththam irukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Thuli raththam irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nirkkum pothilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee nirkkum pothilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu mutham enakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhu mutham enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Arugil neeyum vandhaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Arugil neeyum vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhum bayam illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Yedhum bayam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai yaarukkum naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhai yaarukkum naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli kolvathillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Solli kolvathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaamal naanum vandhen
<input type="checkbox" class="lyrico-select-lyric-line" value="Theriyaamal naanum vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhu ellai
<input type="checkbox" class="lyrico-select-lyric-line" value="Yedhu ellai"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeee eeee eeeeeeeeeeee
<input type="checkbox" class="lyrico-select-lyric-line" value="Eeee eeee eeeeeeeeeeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thirakkaadha oru poottum dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirakkaadha oru poottum dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkadhu ini oottam dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Irukkadhu ini oottam dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Salikaatha oru nodikkooda
<input type="checkbox" class="lyrico-select-lyric-line" value="Salikaatha oru nodikkooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Mani kaatum mullil maatithaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Mani kaatum mullil maatithaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thirakkaadha oru poottum dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thirakkaadha oru poottum dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkadhu ini oottam dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Irukkadhu ini oottam dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Salikaatha oru nodikkooda
<input type="checkbox" class="lyrico-select-lyric-line" value="Salikaatha oru nodikkooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiri pattri kondaal vetri kondaattam
<input type="checkbox" class="lyrico-select-lyric-line" value="Thiri pattri kondaal vetri kondaattam"/>
</div>
</pre>