---
title: "kodali kannaala song lyrics"
album: "Choo Mandhirakaali"
artist: "A Sathish Raghunathan"
lyricist: "Gugai. Ma. Pugalendi"
director: "Eswar Kotravai"
path: "/albums/choo-mandhirakaali-lyrics"
song: "Kodali Kannaala"
image: ../../images/albumart/choo-mandhirakaali.jpg
date: 2021-09-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZtvFuSUaULo"
type: "love"
singers:
  - Anand Aravindakshan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kodali kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodali kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">konnu saaikira aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konnu saaikira aala"/>
</div>
<div class="lyrico-lyrics-wrapper">neppedhum illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neppedhum illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja thooki yen pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja thooki yen pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanaa mootta thookiyandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaa mootta thookiyandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">kadakkaalu poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadakkaalu poda"/>
</div>
<div class="lyrico-lyrics-wrapper">adhungaatti aasa yethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhungaatti aasa yethi"/>
</div>
<div class="lyrico-lyrics-wrapper">edhukkaala vaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhukkaala vaara"/>
</div>
<div class="lyrico-lyrics-wrapper">adi maaya loga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi maaya loga "/>
</div>
<div class="lyrico-lyrics-wrapper">rooba sundhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rooba sundhari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kodali kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodali kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">konnu saaikira aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konnu saaikira aala"/>
</div>
<div class="lyrico-lyrics-wrapper">neppedhum illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neppedhum illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja thooki yen pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja thooki yen pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sotta sotta en usura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sotta sotta en usura"/>
</div>
<div class="lyrico-lyrics-wrapper">mondukittu povuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mondukittu povuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaavu theendhaa thevalanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaavu theendhaa thevalanu"/>
</div>
<div class="lyrico-lyrics-wrapper">thavichen naan dhaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavichen naan dhaane"/>
</div>
<div class="lyrico-lyrics-wrapper">kedaayaatam aala maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedaayaatam aala maathi"/>
</div>
<div class="lyrico-lyrics-wrapper">vettaiyaada paakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettaiyaada paakaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">ooraamootu aamabalainu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooraamootu aamabalainu"/>
</div>
<div class="lyrico-lyrics-wrapper">uppu kandam podadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uppu kandam podadha"/>
</div>
<div class="lyrico-lyrics-wrapper">sozha kaatha moocha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sozha kaatha moocha"/>
</div>
<div class="lyrico-lyrics-wrapper">sozhatti pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sozhatti pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kodali kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodali kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">konnu saaikira aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konnu saaikira aala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uchi manda kaanjiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchi manda kaanjiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulla mazha penjiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulla mazha penjiruchu"/>
</div>
<div class="lyrico-lyrics-wrapper">rendum kettu naa thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendum kettu naa thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">ellaam avalaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellaam avalaala"/>
</div>
<div class="lyrico-lyrics-wrapper">ore vechu veesipona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ore vechu veesipona"/>
</div>
<div class="lyrico-lyrics-wrapper">saanji pona maramaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saanji pona maramaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">ore senthu sendhiponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ore senthu sendhiponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thoondhu pona kenaraanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoondhu pona kenaraanen"/>
</div>
<div class="lyrico-lyrics-wrapper">elakkaaram venam elachipoven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elakkaaram venam elachipoven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kodali kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodali kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">konnu saaikira aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konnu saaikira aala"/>
</div>
<div class="lyrico-lyrics-wrapper">neppedhum illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neppedhum illaama"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja thooki yen pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja thooki yen pora"/>
</div>
<div class="lyrico-lyrics-wrapper">kanaa mootta thookiyandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaa mootta thookiyandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">kadakkaalu poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadakkaalu poda"/>
</div>
<div class="lyrico-lyrics-wrapper">adhungaatti aasa yethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhungaatti aasa yethi"/>
</div>
<div class="lyrico-lyrics-wrapper">edhukkaala vaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhukkaala vaara"/>
</div>
<div class="lyrico-lyrics-wrapper">adi maaya loga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi maaya loga "/>
</div>
<div class="lyrico-lyrics-wrapper">rooba sundhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rooba sundhari"/>
</div>
</pre>
