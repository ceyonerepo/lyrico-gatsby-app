---
title: "banjarey banjarey song lyrics"
album: "Swecha"
artist: "Bhole Shawali"
lyricist: "Bhole Shawali"
director: "KPN Chawhan"
path: "/albums/swecha-lyrics"
song: "Banjarey Banjarey"
image: ../../images/albumart/swecha.jpg
date: 2020-02-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/H2OFA-CMLbg"
type: "happy"
singers:
  - Mangli
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Banjarey Banjarey Banjarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banjarey Banjarey Banjarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rammantu Pilichindi Maa Oorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rammantu Pilichindi Maa Oorey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinanaadu Dhaatanu Godarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinanaadu Dhaatanu Godarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvai Cheranu Naa Oorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvai Cheranu Naa Oorey"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeleley Yennelley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeleley Yennelley"/>
</div>
<div class="lyrico-lyrics-wrapper">Janani Naa Janmabhoomi Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janani Naa Janmabhoomi Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaley Mareley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaley Mareley"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Voori Marachiponule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Voori Marachiponule"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Kanneeru Dachina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kanneeru Dachina"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa yeru Galagale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa yeru Galagale"/>
</div>
<div class="lyrico-lyrics-wrapper">Panneeti Padavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panneeti Padavalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Rani Kila kiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Rani Kila kiley"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janani Naa Janmabhoomi Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janani Naa Janmabhoomi Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Voori Marachiponule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Voori Marachiponule"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Banjarey Banjarey Banjarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banjarey Banjarey Banjarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rammantu Pilichindi Maa Oorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rammantu Pilichindi Maa Oorey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinanaadu Dhaatanu Godarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinanaadu Dhaatanu Godarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvai Cheranu Naa Oorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvai Cheranu Naa Oorey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paccha Pacchani Chelanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paccha Pacchani Chelanni"/>
</div>
<div class="lyrico-lyrics-wrapper">GodariChuttu cheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="GodariChuttu cheri"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu cherey Kattinattuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu cherey Kattinattuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravashamai Maa Tanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravashamai Maa Tanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundelu Uppongeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundelu Uppongeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandagalaa Oogetattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandagalaa Oogetattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinanaati Naa guruthulakey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinanaati Naa guruthulakey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Swagathamichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Swagathamichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chigurinchina Aashalatho Aduge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chigurinchina Aashalatho Aduge"/>
</div>
<div class="lyrico-lyrics-wrapper">Peduthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peduthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeleleyy Yennelleyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeleleyy Yennelleyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohalake Ooperochele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohalake Ooperochele"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeleleyy Yennelleyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeleleyy Yennelleyy"/>
</div>
<div class="lyrico-lyrics-wrapper">EE Chilake Gudu Chereley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="EE Chilake Gudu Chereley"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janani Naa Janmabhoomi Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janani Naa Janmabhoomi Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Voori Marachiponule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Voori Marachiponule"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Banjarey Banjarey Banjarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banjarey Banjarey Banjarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rammantu Pilichindi Maa Oorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rammantu Pilichindi Maa Oorey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinanaadu Dhaatanu Godarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinanaadu Dhaatanu Godarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvai Cheranu Naa Oorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvai Cheranu Naa Oorey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa aa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">aaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ballu Balluna Thellare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ballu Balluna Thellare"/>
</div>
<div class="lyrico-lyrics-wrapper">Na pallello Koyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na pallello Koyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragale Naa Pallavulai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragale Naa Pallavulai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolali Jo jo ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolali Jo jo ani"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Amma Tholipate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Amma Tholipate"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Jeevana Charanaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Jeevana Charanaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Budi Budi na Adugulake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budi Budi na Adugulake"/>
</div>
<div class="lyrico-lyrics-wrapper">Badi nerpina Patam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badi nerpina Patam"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Dhidukula Jeevana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Dhidukula Jeevana"/>
</div>
<div class="lyrico-lyrics-wrapper">Godavari Aratam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godavari Aratam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeleleyy Yennelleyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeleleyy Yennelleyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathalli Guruthukochele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathalli Guruthukochele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeleleyy Yennelleyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeleleyy Yennelleyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa kannulu Chemmagilleley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kannulu Chemmagilleley"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janani Naa Janmabhoomi Le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janani Naa Janmabhoomi Le"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna Voori Marachiponule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Voori Marachiponule"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Banjarey Banjarey Banjarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banjarey Banjarey Banjarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rammantu Pilichindi Maa Oorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rammantu Pilichindi Maa Oorey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinanaadu Dhaatanu Godarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinanaadu Dhaatanu Godarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvai Cheranu Naa Oorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvai Cheranu Naa Oorey"/>
</div>
</pre>
