---
title: "kanaa song lyrics"
album: "Adithya Varma"
artist: "Radhan"
lyricist: "Thamarai"
director: "Gireesaaya"
path: "/albums/adithya-varma-lyrics"
song: "Kanaa"
image: ../../images/albumart/adithya-varma.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/e0Q3PFxCyt0"
type: "love"
singers:
  - Krithika Nelson
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanaa Kanaa Adhil Nee Vanthadhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kanaa Adhil Nee Vanthadhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nila Vizhum Varai Thaen Thanthadhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Vizhum Varai Thaen Thanthadhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakullae Yedho Oru Thadu Maattram Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakullae Yedho Oru Thadu Maattram Yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerungaamal Nee Nagarnthida Thavikkindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungaamal Nee Nagarnthida Thavikkindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa Kanaa Adhil Nee Vanthadhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kanaa Adhil Nee Vanthadhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nila Vizhum Varai Thaen Thanthadhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Vizhum Varai Thaen Thanthadhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Mazhai Yena Melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mazhai Yena Melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Nilam Yena Keezhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Nilam Yena Keezhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhuvathu Neer Maalaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhuvathu Neer Maalaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarvathu Pookkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarvathu Pookkal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valarpirai Polae Ini Varum Naatkalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarpirai Polae Ini Varum Naatkalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Naatkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Naatkale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa Kanaa Adhil Nee Vanthadhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kanaa Adhil Nee Vanthadhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nila Vizhum Varai Thaen Thanthadhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Vizhum Varai Thaen Thanthadhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakullae Yedho Oru Thadu Maattram Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakullae Yedho Oru Thadu Maattram Yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerungaamal Nee Nagarnthida Thavikkindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerungaamal Nee Nagarnthida Thavikkindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanaa Kanaa Adhil Nee Vanthadhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaa Kanaa Adhil Nee Vanthadhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nila Vizhum Varai Thaen Thanthadhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nila Vizhum Varai Thaen Thanthadhen"/>
</div>
</pre>
