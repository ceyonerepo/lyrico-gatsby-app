---
title: "ready steady go song lyrics"
album: "Anbarivu"
artist: "Hiphop Tamizha"
lyricist: "Yaazhi Dragon"
director: "Aswin Raam"
path: "/albums/anbarivu-song-lyrics"
song: "Ready Steady Go"
image: ../../images/albumart/anbarivu.jpg
date: 2022-01-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AYQBMb_9lJg"
type: "happy"
singers:
  - Santhosh Narayanan
  - Chinnaponnu
  - Srinisha Jayaseelan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yennella nadakka kaathirukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennella nadakka kaathirukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu kaathila puyaal adha paathukko bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu kaathila puyaal adha paathukko bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Dialogue ah kedaiyaathu verum action bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dialogue ah kedaiyaathu verum action bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini peace lam illa piece piece mattum bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini peace lam illa piece piece mattum bro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Macha straight out ah tamilnadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macha straight out ah tamilnadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuraikku pakkathula maankaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuraikku pakkathula maankaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Light ah terror aana aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light ah terror aana aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna pasanga yella safe ah velaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pasanga yella safe ah velaiyaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettiya madichi kattunaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettiya madichi kattunaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirm ah kalavaram bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirm ah kalavaram bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattunu porul eduthu potturuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu porul eduthu potturuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam paathukka maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam paathukka maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam paathukka maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam paathukka maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga vettiya madichi kattunaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga vettiya madichi kattunaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirm ah kalavaram bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirm ah kalavaram bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattunu porul eduthu potturuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu porul eduthu potturuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam paathukka maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam paathukka maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam paathukka maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam paathukka maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli nillu thalli nillu thalli nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli nillu thalli nillu thalli nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vandha sambavam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vandha sambavam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelarum pambanum daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelarum pambanum daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready steady go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready steady go"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva suthi suthi adikka pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva suthi suthi adikka pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Soora vaangikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soora vaangikko"/>
</div>
<div class="lyrico-lyrics-wrapper">Everybody loose control
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody loose control"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini confirm ah kalavaram bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini confirm ah kalavaram bro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready steady go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready steady go"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva suthi suthi adikka pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva suthi suthi adikka pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Soora vaangikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soora vaangikko"/>
</div>
<div class="lyrico-lyrics-wrapper">Everybody loose control
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everybody loose control"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini confirm ah kalavaram bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini confirm ah kalavaram bro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anba irrukanum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anba irrukanum da"/>
</div>
<div class="lyrico-lyrics-wrapper">Panba nadakanum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panba nadakanum da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thembu irukkum vara ozhaikanum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thembu irukkum vara ozhaikanum da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vamba polakanum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vamba polakanum da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ambaa parakanum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambaa parakanum da"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpuku urirayum kodukanum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpuku urirayum kodukanum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa nee salambatha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa nee salambatha da"/>
</div>
<div class="lyrico-lyrics-wrapper">Veena nee polambatha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veena nee polambatha da"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee verum bali aadu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee verum bali aadu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un veerappu yellam inga sellathu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un veerappu yellam inga sellathu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangathula thottil senji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangathula thottil senji"/>
</div>
<div class="lyrico-lyrics-wrapper">Singatha nee thoonga vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singatha nee thoonga vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sangula than kaiya vekka paakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangula than kaiya vekka paakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Satunu than mulichi kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satunu than mulichi kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Patunu than paanjiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patunu than paanjiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottunu than poona yengala kekaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottunu than poona yengala kekaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalli nillu thalli nillu thalli nillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalli nillu thalli nillu thalli nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vandha sambavam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vandha sambavam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelarum pambanum daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelarum pambanum daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennella nadakka kaathirukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennella nadakka kaathirukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu kaathila puyal adha paathukko bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu kaathila puyal adha paathukko bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Dialogue ah kedaiyaathu verum action bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dialogue ah kedaiyaathu verum action bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini peace laam illa piece piece mattum bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini peace laam illa piece piece mattum bro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Macha straight out ah tamilnadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macha straight out ah tamilnadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuraikku pakkathula maankaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuraikku pakkathula maankaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Light ah terror aana aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Light ah terror aana aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna pasanga yella safe ah velaiyaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna pasanga yella safe ah velaiyaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettiya madichi kattunaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettiya madichi kattunaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirm ah kalavaram bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirm ah kalavaram bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattunu porul eduthu potturuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu porul eduthu potturuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam paathukka maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam paathukka maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam paathukka maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam paathukka maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga vettiya madichi kattunaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga vettiya madichi kattunaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Confirm ah kalavaram bro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confirm ah kalavaram bro"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattunu porul eduthu potturuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu porul eduthu potturuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam paathukka maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam paathukka maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam paathukka maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam paathukka maa"/>
</div>
</pre>
