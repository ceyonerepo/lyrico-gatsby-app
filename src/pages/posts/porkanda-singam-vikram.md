---
title: "porkanda singam song lyrics"
album: "Vikram"
artist: "Anirudh Ravichander"
lyricist: "Vishnu Edavan"
director: "Lokesh Kanagaraj"
path: "/albums/vikram-lyrics"
song: "Porkanda Singam"
image: ../../images/albumart/vikram.jpg
date: 2022-06-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3EBs73y5iik"
type: "melody"
singers:
  - Ravi G
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uyirum Nadunguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirum Nadunguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaiyum Yendhidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiyum Yendhidave"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyuntha Veerane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyuntha Veerane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangi Azhugire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangi Azhugire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suzhalum Ulagame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhalum Ulagame"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaku Uraindhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaku Uraindhathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adutha Nimidamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha Nimidamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagura Marukkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagura Marukkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaril Unnai Sayuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaril Unnai Sayuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uranga Veypatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uranga Veypatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhantha Uyirkaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhantha Uyirkaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolli Veypatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolli Veypatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porkanda Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porkanda Singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Valikonda Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikonda Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyunthalum Unakaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyunthalum Unakaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vazhgire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vazhgire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhugathe Magane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhugathe Magane"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ayul Unathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ayul Unathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaypola Unai Kakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaypola Unai Kakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Theigire Jeevane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Theigire Jeevane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirum Nadunguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirum Nadunguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaiyum Yendhidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiyum Yendhidave"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyuntha Veerane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyuntha Veerane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangi Azhugire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangi Azhugire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porkanda Singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porkanda Singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Valikonda Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikonda Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyunthalum Unakaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyunthalum Unakaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Vazhgire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Vazhgire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhugathe Magane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhugathe Magane"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ayul Unathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ayul Unathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaypola Unai Kakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaypola Unai Kakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Theigire Jeevane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Theigire Jeevane"/>
</div>
</pre>
