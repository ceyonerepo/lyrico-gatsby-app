---
title: "kerala song lyrics"
album: "Natpe Thunai"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "D. Parthiban Desingu"
path: "/albums/natpe-thunai-lyrics"
song: "Kerala"
image: ../../images/albumart/natpe-thunai.jpg
date: 2019-04-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CtyIEJIP_zE"
type: "happy"
singers:
  - Hiphop Tamizha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enga State tu Kerala Aano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga State tu Kerala Aano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga CM Vijayan Aano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga CM Vijayan Aano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Dance Su Kathakkali Aano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Dance Su Kathakkali Aano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Nee Venu Adiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Nee Venu Adiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga State tu Kerala Aano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga State tu Kerala Aano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga CM Vijayan Aano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga CM Vijayan Aano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Dance Su Kathakkali Aano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Dance Su Kathakkali Aano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Nee Venum Kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Nee Venum Kiliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Nee Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Nee Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keralathu Puttu Puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keralathu Puttu Puttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Combo Hittu Hittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Combo Hittu Hittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Maatra Settu Settu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Maatra Settu Settu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Povan Naan Seththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povan Naan Seththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee illaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Povan Naan Seththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povan Naan Seththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakke Malayalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakke Malayalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Ariyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Ariyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku Enna Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Enna Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lale Ettan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lale Ettan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mammooty Chettan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mammooty Chettan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaraiyum Nalla Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaraiyum Nalla Theriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Poli Un Siruppu Siruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Poli Un Siruppu Siruppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alavalaavuthu Manasu Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavalaavuthu Manasu Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkum Unakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkum Unakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkum Poruththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkum Poruththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga State tu Kerala Aano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga State tu Kerala Aano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga CM Vijayan Aano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga CM Vijayan Aano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Dance Su Kathakkali Aano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Dance Su Kathakkali Aano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Nee Venu Adiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Nee Venu Adiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga State tu Kerala Aano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga State tu Kerala Aano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga CM Vijayan Aano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga CM Vijayan Aano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Dance Su Kathakkali Aano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Dance Su Kathakkali Aano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Nee Venum Kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Nee Venum Kiliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Nee Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Nee Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey All Ballu Sixer Sixer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey All Ballu Sixer Sixer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maththa Ponnungellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththa Ponnungellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sister Sister
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sister Sister"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhapuzha Vaazhapoove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhapuzha Vaazhapoove"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhaporen Police Stationula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaporen Police Stationula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malabaaru Police Sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malabaaru Police Sa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattappaave Aanathukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattappaave Aanathukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avanga Appan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanga Appan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Weapon Eduthuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weapon Eduthuttu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaraan Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraan Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theruchudu Tharigidathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruchudu Tharigidathom"/>
</div>
</pre>
