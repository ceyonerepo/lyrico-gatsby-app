---
title: "oru paarvai song lyrics"
album: "Kandaen"
artist: "Vijay Ebenezer"
lyricist: "Krish"
director: "A.C. Mugil"
path: "/albums/kandaen-lyrics"
song: "Oru Paarvai"
image: ../../images/albumart/kandaen.jpg
date: 2011-05-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tPbbKMocFbU"
type: "love"
singers:
  - Krish
  - Gurupriya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paadhangal Thedudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhangal Thedudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Suzhaludhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Suzhaludhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumendru Thudikkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumendru Thudikkudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayam Ennuyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Ennuyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravugal Paduthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravugal Paduthudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Vedikkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Vedikkudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusugal Saththathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusugal Saththathil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayam Thallaadudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Thallaadudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvai Oru Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvai Oru Vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaalum Podhum Kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaalum Podhum Kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvaayaa Tharuvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvaayaa Tharuvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Nee Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Nee Enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanindri Neeyillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanindri Neeyillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Marujenmam Ondrillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marujenmam Ondrillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyindri Naanumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyindri Naanumillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvai Oru Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvai Oru Vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaalum Podhum Kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaalum Podhum Kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvaayaa Tharuvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvaayaa Tharuvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Nee Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Nee Enge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhangal Thedudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhangal Thedudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Suzhaludhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Suzhaludhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumendru Thudikkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumendru Thudikkudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayam Ennuyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Ennuyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravugal Paduthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravugal Paduthudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Vedikkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Vedikkudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusugal Saththathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusugal Saththathil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayam Thallaadudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Thallaadudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhangal Thedudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhangal Thedudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Suzhaludhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Suzhaludhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumendru Thudikkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumendru Thudikkudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayam Ennuyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Ennuyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravugal Paduthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravugal Paduthudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Vedikkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Vedikkudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusugal Saththathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusugal Saththathil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayam Thallaadudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Thallaadudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Paarthadhu En Vizhigalukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paarthadhu En Vizhigalukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Iruppadhu Un Kangal Imayai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Iruppadhu Un Kangal Imayai Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Neettinen Unnodu Vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Neettinen Unnodu Vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Serndhathaal Vaazhkkai Ondraanadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Serndhathaal Vaazhkkai Ondraanadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai Modha Modha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai Modha Modha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalin Manalum Eeramaanadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalin Manalum Eeramaanadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Paarthu Paarthu Rasithe Irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Paarthu Paarthu Rasithe Irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vaazhume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vaazhume"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Konjumaa Uyir Kenjumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Konjumaa Uyir Kenjumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhupadhumaa Innum Thayakkamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhupadhumaa Innum Thayakkamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhazh Serumaa Karam Moodumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhazh Serumaa Karam Moodumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Naatkalil Kanavu Niraiveruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Naatkalil Kanavu Niraiveruma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovil Vandu Vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovil Vandu Vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenai Uriya Neramaagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenai Uriya Neramaagumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ennai Vandhu Anaithukkolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Vandhu Anaithukkolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Innodi Vendumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innodi Vendumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvai Oru Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvai Oru Vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaalum Podhum Kannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaalum Podhum Kannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvaayaa Tharuvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvaayaa Tharuvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbe Nee Enge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Nee Enge"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanindri Neeyillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanindri Neeyillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Marujenmam Ondrillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marujenmam Ondrillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyindri Naanumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyindri Naanumillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvai Oru Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvai Oru Vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumendru Thudikkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumendru Thudikkudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayam Ennuyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Ennuyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuvaayaa Tharuvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvaayaa Tharuvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusugal Saththathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusugal Saththathil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayam Thallaadudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Thallaadudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paarvai Oru Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvai Oru Vaarthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumendru Thudikkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumendru Thudikkudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayam Ennuyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Ennuyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvaayaa Tharuvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvaayaa Tharuvaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusugal Saththathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusugal Saththathil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayam Thallaadudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Thallaadudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhangal Thedudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhangal Thedudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Suzhaludhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Suzhaludhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumendru Thudikkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumendru Thudikkudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayam Ennuyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Ennuyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravugal Paduthudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravugal Paduthudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Vedikkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Vedikkudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolusugal Saththathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolusugal Saththathil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayam Thallaadudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Thallaadudhe"/>
</div>
</pre>
