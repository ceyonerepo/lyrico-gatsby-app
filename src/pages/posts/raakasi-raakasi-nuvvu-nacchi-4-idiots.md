---
title: "raakasi song lyrics"
album: "4 Idiots"
artist: "Jaya surya"
lyricist: "Jaya surya"
director: "S. Satish Kumar"
path: "/albums/4-idiots-lyrics"
song: "Raakasi Raakasi Nuvvu Nacchi"
image: ../../images/albumart/4-idiots.jpg
date: 2019-11-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/6myGuOot6p8"
type: "love"
singers:
  - Vinayak Satish
  - Sujatha Sanjana
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raakasi Raakasi Nuvve Nacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakasi Raakasi Nuvve Nacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundello Puttinde Edi Picchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundello Puttinde Edi Picchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raakasi Raakasi Kallo Kocchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakasi Raakasi Kallo Kocchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Parishanu Cheshave Pcchinunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Parishanu Cheshave Pcchinunchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu Okunte Duldul Baajee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Okunte Duldul Baajee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu THappukunte Egiri Podha Fuejee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu THappukunte Egiri Podha Fuejee"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhu Muddhola Mogiponi DJ
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhu Muddhola Mogiponi DJ"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Mundundi Manku Peeli Fozey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Mundundi Manku Peeli Fozey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raakasi Raakasi Nuvve Nacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakasi Raakasi Nuvve Nacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundello Puttinde Edi Picchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundello Puttinde Edi Picchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raara Idiot Raara Idiot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raara Idiot Raara Idiot"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Lena Nee Juliot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Lena Nee Juliot"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intilo Maska Kotti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intilo Maska Kotti"/>
</div>
<div class="lyrico-lyrics-wrapper">Podama Pollachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podama Pollachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gantako Desham Chutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gantako Desham Chutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddamaa Ososi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddamaa Ososi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jonna Rotte Koodi Kura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jonna Rotte Koodi Kura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ramjugaa Vaddinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramjugaa Vaddinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andamantha Appagistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andamantha Appagistha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayyaram Jodinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayyaram Jodinchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Liplip Mixaipothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Liplip Mixaipothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Siggu yeggu Matashe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siggu yeggu Matashe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondakona Yekkesthavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondakona Yekkesthavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuveraa Prabhase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuveraa Prabhase"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu Okunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Okunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Duldul Baajee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Duldul Baajee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Thappukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Thappukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Egiri Podha Fuejee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egiri Podha Fuejee"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhu Muddhola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhu Muddhola"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogiponi DJ
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogiponi DJ"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Mundundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Mundundi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manku Peeli Fozey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manku Peeli Fozey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raakasi Raakasi Nuvve Nacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakasi Raakasi Nuvve Nacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundello Puttinde Edi Picchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundello Puttinde Edi Picchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yadagirigutta Paina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yadagirigutta Paina"/>
</div>
<div class="lyrico-lyrics-wrapper">Adukundama Laggam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adukundama Laggam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkapoddhu Velakalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkapoddhu Velakalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesukundama Yuddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesukundama Yuddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Facebook Whatsapp
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Facebook Whatsapp"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaldi Pakkana Pettedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaldi Pakkana Pettedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyarulo Siming Chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyarulo Siming Chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Awardule Kottedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Awardule Kottedham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thatikallu Thaginattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatikallu Thaginattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhe Naaku Ivvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhe Naaku Ivvala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinumaru Aadeddhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinumaru Aadeddhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhugolame Voogelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhugolame Voogelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muddhu Muddhola Mogiponi DJ
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhu Muddhola Mogiponi DJ"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Mundundi Manku Peeli Fozey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Mundundi Manku Peeli Fozey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Okunte Duldul Baajee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Okunte Duldul Baajee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Thappukunte Egiri Podha Fuejee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Thappukunte Egiri Podha Fuejee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raakasi Raakasi Nuvve Nacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakasi Raakasi Nuvve Nacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundello Puttinde Edi Picchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundello Puttinde Edi Picchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raakasi Raakasi Kallo Kocchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakasi Raakasi Kallo Kocchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Parishanu Cheshave Pcchinunchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Parishanu Cheshave Pcchinunchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu Okunte Duldul Baajee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Okunte Duldul Baajee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Thappukunte Egiri Podha Fuejee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Thappukunte Egiri Podha Fuejee"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhu Muddhola Mogiponi DJ
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhu Muddhola Mogiponi DJ"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Mundundi Manku Peeli Fozey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Mundundi Manku Peeli Fozey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raakasi Raakasi Nuvve Nacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakasi Raakasi Nuvve Nacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundello Puttinde Edi Picchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundello Puttinde Edi Picchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raara Idiot Raara Idiot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raara Idiot Raara Idiot"/>
</div>
<div class="lyrico-lyrics-wrapper">Nene Lena Nee Juliot
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nene Lena Nee Juliot"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raakasi Raakasi Nuvve Nacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakasi Raakasi Nuvve Nacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gundello Puttinde Edi Picchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundello Puttinde Edi Picchi"/>
</div>
</pre>
