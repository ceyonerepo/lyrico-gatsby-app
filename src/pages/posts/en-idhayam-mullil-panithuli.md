---
title: "en idhayam song lyrics"
album: "Mullil Panithuli"
artist: "Benny Pradeep "
lyricist: "Kavingar sura"
director: "NM Jegan"
path: "/albums/mullil-panithuli-lyrics"
song: "En Idhayam"
image: ../../images/albumart/mullil-panithuli.jpg
date: 2021-10-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/94LUlyBNGbM"
type: "love"
singers:
  - Nithin Abhishek
  - Kiruthika R.S
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">en idhayam un vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en idhayam un vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">anbe nee en vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbe nee en vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">en idhayam un vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en idhayam un vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">anbe nee en vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbe nee en vasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pagalum iravum namakanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalum iravum namakanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">paruva thooral namathanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruva thooral namathanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalum iravum namakanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalum iravum namakanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">paruva thooral namathanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruva thooral namathanathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanaiyalam koodi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanaiyalam koodi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thunai ena kooda vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunai ena kooda vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mai konda iru kannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mai konda iru kannum"/>
</div>
<div class="lyrico-lyrics-wrapper">malar konda peru nenjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malar konda peru nenjum"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan nenjil entha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan nenjil entha "/>
</div>
<div class="lyrico-lyrics-wrapper">naalum koodatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalum koodatum"/>
</div>
<div class="lyrico-lyrics-wrapper">aalaana un meni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalaana un meni"/>
</div>
<div class="lyrico-lyrics-wrapper">noolaana ennoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noolaana ennoda"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthu vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthu vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pattam pola aadatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam pola aadatum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalaiyum maalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalaiyum maalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">rathiri velaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rathiri velaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">un madi veenai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un madi veenai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">meetu nee naalum than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meetu nee naalum than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">isaikiren kooda vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isaikiren kooda vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">isaivena kooda vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isaivena kooda vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kattendra un nenjum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattendra un nenjum"/>
</div>
<div class="lyrico-lyrics-wrapper">kalai mikka thol rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalai mikka thol rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">penmai vanthu endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penmai vanthu endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">aadum oonjal than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadum oonjal than"/>
</div>
<div class="lyrico-lyrics-wrapper">nee indri ponale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee indri ponale"/>
</div>
<div class="lyrico-lyrics-wrapper">ver indri saaigindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ver indri saaigindra"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhai endru enthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhai endru enthan"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalvum aagum than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalvum aagum than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vethanai yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethanai yenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">thanguven nanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanguven nanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhale neyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhale neyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaviyam padadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaviyam padadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennudan kooda vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennudan kooda vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ennile kooda vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennile kooda vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en idhayam un vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en idhayam un vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">anbe nee en vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbe nee en vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">en idhayam un vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en idhayam un vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">anbe nee en vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbe nee en vasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pagalum iravum namakanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalum iravum namakanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">paruva thooral namathanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruva thooral namathanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalum iravum namakanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalum iravum namakanathu"/>
</div>
<div class="lyrico-lyrics-wrapper">paruva thooral namathanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruva thooral namathanathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanaiyalam koodi vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanaiyalam koodi vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thunai ena kooda vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunai ena kooda vaa"/>
</div>
</pre>
