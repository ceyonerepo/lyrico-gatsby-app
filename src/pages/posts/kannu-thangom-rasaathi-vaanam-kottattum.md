---
title: "kannu thangom rasaathi song lyrics"
album: "Vaanam Kottattum"
artist: "Sid Sriram"
lyricist: "Siva Ananth"
director: "Dhana"
path: "/albums/vaanam-kottattum-lyrics"
song: "Kannu Thangom Rasaathi"
image: ../../images/albumart/vaanam-kottattum.jpg
date: 2020-02-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fUiYmXBJ7dU"
type: "Sad"
singers:
  - Sid Sriram
  - Shakthisree Gopalan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannu Thangom Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Thangom Raasathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Kandaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kandaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenju Moochudum Theevaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju Moochudum Theevaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonna Nambu Mavarassi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna Nambu Mavarassi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Per Sollaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Per Sollaatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Oorukku Peyyathudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Oorukku Peyyathudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagi Un Punnaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagi Un Punnaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ara Dozen Pournami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ara Dozen Pournami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaiyaa Pesudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyaa Pesudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasula Maargazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Maargazhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raani Kaali Yesamaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raani Kaali Yesamaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvai Paarthaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Paarthaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaman Ullaara Poomaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman Ullaara Poomaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lesa Morachaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesa Morachaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochu Thadumaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Thadumaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadi Narambellaam Mookkaduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadi Narambellaam Mookkaduthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onakkum Mela Oorula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onakkum Mela Oorula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkunnu Yaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkunnu Yaradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichu Naan Solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichu Naan Solluven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku Naan Kaalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Naan Kaalani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raasathi Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasathi Raasathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raasathi Raasathi Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasathi Raasathi Raasathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mavaraasi Mavaraasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavaraasi Mavaraasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raasathi Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasathi Raasathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raasathi Raasathi Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasathi Raasathi Raasathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mavaraasi Mavaraasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mavaraasi Mavaraasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raasa Singam En Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasa Singam En Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru Sonnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Sonnaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Neethaane Saripaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Neethaane Saripaathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaayya Paavi Kaathirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayya Paavi Kaathirukken"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pona Povattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona Povattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Kai Korthu Karai Seraiyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kai Korthu Karai Seraiyya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniyila Nadakkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyila Nadakkaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Nee Thonaiyiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Nee Thonaiyiru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madiyila Manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyila Manasula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orangida Edangodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orangida Edangodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Thangom Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Thangom Raasathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Thangom Raasathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Thangom Raasathi"/>
</div>
</pre>
