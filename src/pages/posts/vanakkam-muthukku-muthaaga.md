---
title: "vanakkam song lyrics"
album: "Muthukku Muthaaga"
artist: "Kavi Periyathambi"
lyricist: "Na. Muthukumar - Nandalala"
director: "Rasu Madhuravan"
path: "/albums/muthukku-muthaaga-lyrics"
song: "Vanakkam"
image: ../../images/albumart/muthukku-muthaaga.jpg
date: 2011-03-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ib_kYRNJ4GY"
type: "happy"
singers:
  - Sriram
  - Mukesh
  - Janani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vanakkam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanakkam "/>
</div>
<div class="lyrico-lyrics-wrapper">kalyaana veettulayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyaana veettulayum"/>
</div>
<div class="lyrico-lyrics-wrapper">kedaa vetti soarupoattu vazhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kedaa vetti soarupoattu vazhakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vanakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhu kuththu veettulayum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhu kuththu veettulayum "/>
</div>
<div class="lyrico-lyrics-wrapper">kaththikkuththu nadappadhu vazhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaththikkuththu nadappadhu vazhakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vanakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnukkoru jaadi ena ponna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnukkoru jaadi ena ponna "/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu koduppadhu vazhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu koduppadhu vazhakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vanakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">pottappulla peththupputtaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottappulla peththupputtaa "/>
</div>
<div class="lyrico-lyrics-wrapper">kallippaala oothuradhu vazhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallippaala oothuradhu vazhakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naattaama Theerppa maathi sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattaama Theerppa maathi sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattaama Theerppa maathi sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattaama Theerppa maathi sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vara Vaigaasi maasam 23m thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vara Vaigaasi maasam 23m thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">unakkum andha pullaikkum kalyaanandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkum andha pullaikkum kalyaanandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei kannukkazhagaa ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei kannukkazhagaa ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">kandutholaichen unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandutholaichen unna"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakalannu minna thangabaspamaa thinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakalannu minna thangabaspamaa thinna"/>
</div>
<div class="lyrico-lyrics-wrapper">hey kandadhum nenjamum alaiyum peyaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey kandadhum nenjamum alaiyum peyaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">en kappalthaan paadhiyum kalavu poayaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kappalthaan paadhiyum kalavu poayaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">yei yei yei yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei yei yei yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naattaama Theerppa maathi sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattaama Theerppa maathi sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattaama Theerppa maathi sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattaama Theerppa maathi sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">aattakkaalai varummaiyaa bakthiyudan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aattakkaalai varummaiyaa bakthiyudan "/>
</div>
<div class="lyrico-lyrics-wrapper">irukkiradhu vazhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukkiradhu vazhakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vanakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">pattaa peettu kedhu maela pazhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattaa peettu kedhu maela pazhi "/>
</div>
<div class="lyrico-lyrics-wrapper">poattu thoonguradhu vazhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poattu thoonguradhu vazhakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey thinthaak thaa thinakthaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thinthaak thaa thinakthaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thindhaakthaa thinak thinak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thindhaakthaa thinak thinak"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thinthaak thaa thinakthaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thinthaak thaa thinakthaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thindhaakthaa thinak thinak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thindhaakthaa thinak thinak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">karuvelangaattukkulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvelangaattukkulla "/>
</div>
<div class="lyrico-lyrics-wrapper">kalavaattam yedhumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavaattam yedhumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadi bulla vaadi ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadi bulla vaadi ulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey thinthaak thaa thinakthaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thinthaak thaa thinakthaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thindhaakthaa thinak thinak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thindhaakthaa thinak thinak"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thinthaak thaa thinakthaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thinthaak thaa thinakthaa "/>
</div>
<div class="lyrico-lyrics-wrapper">thindhaakthaa thinak thinak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thindhaakthaa thinak thinak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thannannannaa naannnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannannannaa naannnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thannannannaa naannnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannannannaa naannnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thannannannaa naannnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannannannaa naannnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naaney naaney naaney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naaney naaney naaney "/>
</div>
<div class="lyrico-lyrics-wrapper">naanannaa naanannaa hak hak hak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanannaa naanannaa hak hak hak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey eesal eraga poala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey eesal eraga poala "/>
</div>
<div class="lyrico-lyrics-wrapper">hey engappudicha sela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey engappudicha sela"/>
</div>
<div class="lyrico-lyrics-wrapper">katti maraikkum vela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti maraikkum vela "/>
</div>
<div class="lyrico-lyrics-wrapper">kavukkudhadi aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavukkudhadi aala"/>
</div>
<div class="lyrico-lyrics-wrapper">hey eecham pazhamey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey eecham pazhamey "/>
</div>
<div class="lyrico-lyrics-wrapper">unakkey karaicheney vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakkey karaicheney vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hey eera veragaa manasu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey eera veragaa manasu "/>
</div>
<div class="lyrico-lyrics-wrapper">thovainjeney yaa yaa yaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thovainjeney yaa yaa yaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naattaama Theerppa maathi sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattaama Theerppa maathi sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naattaama Theerppa maathi sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naattaama Theerppa maathi sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undiyalla kaasu poattu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undiyalla kaasu poattu "/>
</div>
<div class="lyrico-lyrics-wrapper">oorukkaaga venduradhu vazhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorukkaaga venduradhu vazhakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru saanja vaelaiyila undiyala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru saanja vaelaiyila undiyala "/>
</div>
<div class="lyrico-lyrics-wrapper">odaikkiradhu vazhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odaikkiradhu vazhakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vanakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vikkikkittaa appathaannu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vikkikkittaa appathaannu "/>
</div>
<div class="lyrico-lyrics-wrapper">vettithanni oothuradhu vazhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettithanni oothuradhu vazhakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vanakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">veru thinnai kaaththavalai vaetti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veru thinnai kaaththavalai vaetti "/>
</div>
<div class="lyrico-lyrics-wrapper">poattu thookkuradhu vazhakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poattu thookkuradhu vazhakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vanakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vanakkam vanakkam vanakkam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanakkam vanakkam vanakkam "/>
</div>
<div class="lyrico-lyrics-wrapper">vanakkam vanakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanakkam vanakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">vanakkam vanakkam vanakkam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanakkam vanakkam vanakkam "/>
</div>
<div class="lyrico-lyrics-wrapper">vanakkam enga ooru vanakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanakkam enga ooru vanakkam"/>
</div>
</pre>
