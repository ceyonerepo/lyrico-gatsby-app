---
title: "disco rap song lyrics"
album: "Punya Paap"
artist: "Karan Kanchan - NDS"
lyricist: "DIVINE - D’EVIL - MC Altaf"
director: "Divine"
path: "/albums/punya-paap-lyrics"
song: "Disco Rap"
image: ../../images/albumart/punya-paap.jpg
date: 2020-12-03
lang: hindi
youtubeLink: "https://www.youtube.com/embed/r_RlqDFKc5c"
type: "happy"
singers:
  - DIVINE
  - D’EVIL
  - MC Altaf
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sabko party karna divine bhai ke sath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabko party karna divine bhai ke sath"/>
</div>
<div class="lyrico-lyrics-wrapper">Sabko party karna dhaval bhai ke sath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabko party karna dhaval bhai ke sath"/>
</div>
<div class="lyrico-lyrics-wrapper">Sabko party karna altaf bhai ke sath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabko party karna altaf bhai ke sath"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suno mera disco rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suno mera disco rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Asal bolte isko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asal bolte isko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum log bolte diss ko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum log bolte diss ko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum diss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum diss nahi karte"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar aim liya miss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar aim liya miss nahi karte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suno mera disco rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suno mera disco rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum log bolte diss ko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum log bolte diss ko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Asal bolte isko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asal bolte isko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum diss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum diss nahi karte"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar aim liya miss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar aim liya miss nahi karte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Club mein entry maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Club mein entry maare"/>
</div>
<div class="lyrico-lyrics-wrapper">Bouncer check nahi karta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bouncer check nahi karta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur ek table nahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur ek table nahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo respect nahi karta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo respect nahi karta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirf jhoomta tera bhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirf jhoomta tera bhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Apan step nahi karta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apan step nahi karta"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekhti hai public
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekhti hai public"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyun ki bless hai yeh banda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun ki bless hai yeh banda"/>
</div>
<div class="lyrico-lyrics-wrapper">Gg ke siva main kuchh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gg ke siva main kuchh"/>
</div>
<div class="lyrico-lyrics-wrapper">Rep nahi karta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rep nahi karta"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar aim maine liya to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar aim maine liya to"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir miss nahi karta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir miss nahi karta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ek time pe apun queue mein the
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek time pe apun queue mein the"/>
</div>
<div class="lyrico-lyrics-wrapper">Crew mein the fake id
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Crew mein the fake id"/>
</div>
<div class="lyrico-lyrics-wrapper">Entry marte the peechhe se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entry marte the peechhe se"/>
</div>
5 <div class="lyrico-lyrics-wrapper">mere dost aur 5 log extra hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mere dost aur 5 log extra hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Life meri movie isliye 5 log extra hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life meri movie isliye 5 log extra hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Automatic sab kuchh zindagi tesla hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Automatic sab kuchh zindagi tesla hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Khelna hai cricket
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khelna hai cricket"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur zindagi chess-la hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur zindagi chess-la hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Masla hai machna to hasna mat chhod
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masla hai machna to hasna mat chhod"/>
</div>
<div class="lyrico-lyrics-wrapper">Gg ke gaanon pe bajna mat chhod
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gg ke gaanon pe bajna mat chhod"/>
</div>
<div class="lyrico-lyrics-wrapper">Isko kehte hain halke mein tod
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isko kehte hain halke mein tod"/>
</div>
<div class="lyrico-lyrics-wrapper">Antarashtriya naka tu janta yeh road
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antarashtriya naka tu janta yeh road"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suno mera disco rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suno mera disco rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Asal bolte isko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asal bolte isko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum log bolte diss ko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum log bolte diss ko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum diss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum diss nahi karte"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar aim liya miss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar aim liya miss nahi karte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sunn mera disco rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sunn mera disco rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum log bolte diss ko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum log bolte diss ko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Asal bolte isko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asal bolte isko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum diss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum diss nahi karte"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar aim liya miss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar aim liya miss nahi karte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Leke kaali peeli nikla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leke kaali peeli nikla"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaupati ke side
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaupati ke side"/>
</div>
<div class="lyrico-lyrics-wrapper">Baje raat ke teen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baje raat ke teen"/>
</div>
<div class="lyrico-lyrics-wrapper">Wahan nakabandi tight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wahan nakabandi tight"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri maang zyada badi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri maang zyada badi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">De nukkad wali chai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="De nukkad wali chai"/>
</div>
<div class="lyrico-lyrics-wrapper">Smarty wale shot dete
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smarty wale shot dete"/>
</div>
<div class="lyrico-lyrics-wrapper">Chatur wali dhayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chatur wali dhayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raantí wale look leke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raantí wale look leke"/>
</div>
<div class="lyrico-lyrics-wrapper">Galiyon mein machaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galiyon mein machaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Chamti deke sir pe woh to
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamti deke sir pe woh to"/>
</div>
<div class="lyrico-lyrics-wrapper">Gupchup se ho jaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gupchup se ho jaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aise pagal dost aise pagal log
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aise pagal dost aise pagal log"/>
</div>
<div class="lyrico-lyrics-wrapper">Gang taabad tod aaye father log
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gang taabad tod aaye father log"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal aye raasta chhod chal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal aye raasta chhod chal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aata account mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata account mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhol kiya bhai log ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhol kiya bhai log ne"/>
</div>
<div class="lyrico-lyrics-wrapper">Call dinner mere naam pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Call dinner mere naam pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Chal kya chahiye bol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chal kya chahiye bol"/>
</div>
<div class="lyrico-lyrics-wrapper">Bas swipe karna swag hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bas swipe karna swag hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaana accha baja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaana accha baja"/>
</div>
<div class="lyrico-lyrics-wrapper">Uspe hype karna shauk se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uspe hype karna shauk se"/>
</div>
<div class="lyrico-lyrics-wrapper">Security nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Security nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Apne bhai log he bahut hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apne bhai log he bahut hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirf khaas log ki party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirf khaas log ki party"/>
</div>
<div class="lyrico-lyrics-wrapper">Umar teri chhoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umar teri chhoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh hai baap log ki party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh hai baap log ki party"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana chahti shawty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana chahti shawty"/>
</div>
<div class="lyrico-lyrics-wrapper">Sab apne bol kya chahiye shawty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab apne bol kya chahiye shawty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suno mera disco rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suno mera disco rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Asal bolte isko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asal bolte isko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum log bolte diss ko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum log bolte diss ko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum diss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum diss nahi karte"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar aim liya miss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar aim liya miss nahi karte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suno mera disco rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suno mera disco rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum log bolte diss ko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum log bolte diss ko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Asal bolte isko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asal bolte isko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum diss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum diss nahi karte"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar aim liya miss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar aim liya miss nahi karte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nachne ka paisa lega kya haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachne ka paisa lega kya haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aisa nahi to waisa lega kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aisa nahi to waisa lega kya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sabko nacha do isko usko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabko nacha do isko usko"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachne ka nahi hai to khisko khisko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachne ka nahi hai to khisko khisko"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghoom ghoom ke ho gayele jhango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghoom ghoom ke ho gayele jhango"/>
</div>
<div class="lyrico-lyrics-wrapper">Farak nahi padta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Farak nahi padta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kare garba kare tango
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kare garba kare tango"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Disco mein scotch hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disco mein scotch hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Country nahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Country nahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekin 21 ke neeche chhote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekin 21 ke neeche chhote"/>
</div>
<div class="lyrico-lyrics-wrapper">Entry nahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entry nahi hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poster pe kuchh bhi dikhane de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poster pe kuchh bhi dikhane de"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyon ki party to chalu hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyon ki party to chalu hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Apne hi aane pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apne hi aane pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Hater log rehne layki mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hater log rehne layki mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre bhai log khade le vip mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre bhai log khade le vip mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Gully gang wali party hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gully gang wali party hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre chhoone ka nahi chhote
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre chhoone ka nahi chhote"/>
</div>
<div class="lyrico-lyrics-wrapper">Sab kuchh hard hi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab kuchh hard hi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo apna swag hai woh ich apna id hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo apna swag hai woh ich apna id hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poori night tere bhai ki hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poori night tere bhai ki hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suno mera disco rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suno mera disco rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Asal bolte isko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asal bolte isko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum log bolte diss ko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum log bolte diss ko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum diss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum diss nahi karte"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar aim liya miss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar aim liya miss nahi karte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suno mera disco rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suno mera disco rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum log bolte diss ko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum log bolte diss ko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Asal bolte isko rap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asal bolte isko rap"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum diss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum diss nahi karte"/>
</div>
<div class="lyrico-lyrics-wrapper">Agar aim liya miss nahi karte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agar aim liya miss nahi karte"/>
</div>
</pre>
