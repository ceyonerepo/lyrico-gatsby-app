---
title: "podungada aatam song lyrics"
album: "Mullil Panithuli"
artist: "Benny Pradeep "
lyricist: "Kavingar Mouli"
director: "NM Jegan"
path: "/albums/mullil-panithuli-lyrics"
song: "Podungada Aatam"
image: ../../images/albumart/mullil-panithuli.jpg
date: 2021-10-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2gLtcv1nEpc"
type: "happy"
singers:
  - Vishnu Das
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">podungada aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podungada aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">nanga nadodi kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanga nadodi kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu koothanoda ratham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu koothanoda ratham"/>
</div>
<div class="lyrico-lyrics-wrapper">adicha kalangum sitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adicha kalangum sitham"/>
</div>
<div class="lyrico-lyrics-wrapper">sonthamilada banthamilada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonthamilada banthamilada"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhum vazhkai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhum vazhkai "/>
</div>
<div class="lyrico-lyrics-wrapper">inbam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">uruvum illada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uruvum illada "/>
</div>
<div class="lyrico-lyrics-wrapper">pagaiyum illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagaiyum illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhkai endrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhkai endrum "/>
</div>
<div class="lyrico-lyrics-wrapper">inbam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasa sekura macha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasa sekura macha"/>
</div>
<div class="lyrico-lyrics-wrapper">panatha sekura makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panatha sekura makka"/>
</div>
<div class="lyrico-lyrics-wrapper">manasa sekatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasa sekatha"/>
</div>
<div class="lyrico-lyrics-wrapper">maranthu nikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranthu nikura"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu sethave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu sethave"/>
</div>
<div class="lyrico-lyrics-wrapper">malaiya uyarnthu nikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaiya uyarnthu nikura"/>
</div>
<div class="lyrico-lyrics-wrapper">macha manasa sethave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="macha manasa sethave"/>
</div>
<div class="lyrico-lyrics-wrapper">malaiya usanthu nikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaiya usanthu nikura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanban unduda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban unduda"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku ethiri illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku ethiri illada"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhum vazhkaiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhum vazhkaiya "/>
</div>
<div class="lyrico-lyrics-wrapper">inbam thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inbam thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal illa da machi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal illa da machi "/>
</div>
<div class="lyrico-lyrics-wrapper">kanner illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanner illada"/>
</div>
<div class="lyrico-lyrics-wrapper">moratu single ah nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moratu single ah nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">vazha poren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazha poren da"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhum vazkaiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhum vazkaiya "/>
</div>
<div class="lyrico-lyrics-wrapper">nalla vazha porenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla vazha porenda"/>
</div>
<div class="lyrico-lyrics-wrapper">en aayul muzhuvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aayul muzhuvathum"/>
</div>
<div class="lyrico-lyrics-wrapper">nane aala poren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane aala poren da"/>
</div>
<div class="lyrico-lyrics-wrapper">eraga polatha nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eraga polatha nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathil methakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathil methakura"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nathiya polatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nathiya polatha"/>
</div>
<div class="lyrico-lyrics-wrapper">samama vazha ninaikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samama vazha ninaikuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vazhum vazkaiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhum vazkaiya "/>
</div>
<div class="lyrico-lyrics-wrapper">nalla vazha porenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla vazha porenda"/>
</div>
<div class="lyrico-lyrics-wrapper">en aayul muzhuvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en aayul muzhuvathum"/>
</div>
<div class="lyrico-lyrics-wrapper">nane aala poren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nane aala poren da"/>
</div>
<div class="lyrico-lyrics-wrapper">eraga polatha nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eraga polatha nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathil methakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathil methakura"/>
</div>
<div class="lyrico-lyrics-wrapper">oru nathiya polatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nathiya polatha"/>
</div>
<div class="lyrico-lyrics-wrapper">samama vazha ninaikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samama vazha ninaikuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nathiya polatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nathiya polatha"/>
</div>
<div class="lyrico-lyrics-wrapper">samama vazha ninaikuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samama vazha ninaikuren"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum nanaiya katuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum nanaiya katuna"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum adichu norukuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum adichu norukuven"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum adichu norukuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum adichu norukuven"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum adichu norukuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum adichu norukuven"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum adichu norukuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum adichu norukuven"/>
</div>
</pre>
