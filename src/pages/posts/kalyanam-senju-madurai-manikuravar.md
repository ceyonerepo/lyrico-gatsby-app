---
title: "kalyanam senju song lyrics"
album: "Madurai Manikuravar"
artist: "Isaignani Ilaiyaraaja"
lyricist: "Kavignar Muthulingam"
director: "K Raajarishi"
path: "/albums/madurai-manikuravar-lyrics"
song: "​Kalyanam Senju"
image: ../../images/albumart/madurai-manikuravar.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4j3PafS04u4"
type: "love"
singers:
  - Karthik
  - Vibhavari Joshi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kalyanam senju vacha meenachiku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam senju vacha meenachiku "/>
</div>
<div class="lyrico-lyrics-wrapper">vaira kallale mookuthi than senju podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaira kallale mookuthi than senju podu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyanam senju vacha meenachiku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam senju vacha meenachiku "/>
</div>
<div class="lyrico-lyrics-wrapper">vaira kallale mookuthi than senju podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaira kallale mookuthi than senju podu"/>
</div>
<div class="lyrico-lyrics-wrapper">maadam muluthum mani sudar oli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maadam muluthum mani sudar oli"/>
</div>
<div class="lyrico-lyrics-wrapper">deepam aetru oodal kodiyai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deepam aetru oodal kodiyai "/>
</div>
<div class="lyrico-lyrics-wrapper">alangarithu oru maalai saatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alangarithu oru maalai saatru"/>
</div>
<div class="lyrico-lyrics-wrapper">thiru kulam aadum potramarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiru kulam aadum potramarai"/>
</div>
<div class="lyrico-lyrics-wrapper">namai vaalthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namai vaalthuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalyanam senju vacha meenachiku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam senju vacha meenachiku "/>
</div>
<div class="lyrico-lyrics-wrapper">vaira kallale mookuthi than senju podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaira kallale mookuthi than senju podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kodi malligai poovil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi malligai poovil "/>
</div>
<div class="lyrico-lyrics-wrapper">sanangalai thodupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sanangalai thodupen"/>
</div>
<div class="lyrico-lyrics-wrapper">karu karuvenum koonthal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karu karuvenum koonthal "/>
</div>
<div class="lyrico-lyrics-wrapper">manam pera mudipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam pera mudipen"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam thinam oru aasai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam thinam oru aasai "/>
</div>
<div class="lyrico-lyrics-wrapper">manathinil valarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathinil valarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">pala vithangalai aada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala vithangalai aada "/>
</div>
<div class="lyrico-lyrics-wrapper">karangalai pidithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karangalai pidithen"/>
</div>
<div class="lyrico-lyrics-wrapper">sithirathu koodam kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithirathu koodam kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">sithirathil namai kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithirathil namai kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">putham puthu poovai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="putham puthu poovai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">pon siripil nan nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pon siripil nan nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">innalum ennalum innalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innalum ennalum innalum"/>
</div>
<div class="lyrico-lyrics-wrapper">ennalum ilamai inimai puthumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennalum ilamai inimai puthumai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalyanam senju vacha meenachiku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam senju vacha meenachiku "/>
</div>
<div class="lyrico-lyrics-wrapper">vaira kallale mookuthi than senju podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaira kallale mookuthi than senju podu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyanam senju vacha meenachiku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam senju vacha meenachiku "/>
</div>
<div class="lyrico-lyrics-wrapper">vaira kallale mookuthi than senju podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaira kallale mookuthi than senju podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethir ethir karai odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethir ethir karai odu"/>
</div>
<div class="lyrico-lyrics-wrapper">nathi nadakirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nathi nadakirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ethir pinnal oru kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethir pinnal oru kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">vithi nadathiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithi nadathiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">manam varantathanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam varantathanale"/>
</div>
<div class="lyrico-lyrics-wrapper">neer varandathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neer varandathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">itham naduvinil anbin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itham naduvinil anbin"/>
</div>
<div class="lyrico-lyrics-wrapper">thulir malarnthiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulir malarnthiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">thundu pattu nirkum sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thundu pattu nirkum sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">ondru patta ullangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondru patta ullangam"/>
</div>
<div class="lyrico-lyrics-wrapper">kanthirunthu kanthirunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanthirunthu kanthirunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">serthu vaitha kolangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serthu vaitha kolangal"/>
</div>
<div class="lyrico-lyrics-wrapper">oorkellam thai thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorkellam thai thane"/>
</div>
<div class="lyrico-lyrics-wrapper">oorkellam thai thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorkellam thai thane"/>
</div>
<div class="lyrico-lyrics-wrapper">uravai inaithu vaithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravai inaithu vaithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalyanam senju vacha meenachiku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam senju vacha meenachiku "/>
</div>
<div class="lyrico-lyrics-wrapper">vaira kallale mookuthi than senju podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaira kallale mookuthi than senju podu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyanam senju vacha meenachiku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam senju vacha meenachiku "/>
</div>
<div class="lyrico-lyrics-wrapper">vaira kallale mookuthi than senju podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaira kallale mookuthi than senju podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maadam muluthum mani sudar oli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maadam muluthum mani sudar oli"/>
</div>
<div class="lyrico-lyrics-wrapper">deepam aetru oodal kodiyai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deepam aetru oodal kodiyai "/>
</div>
<div class="lyrico-lyrics-wrapper">alangarithu oru maalai saatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alangarithu oru maalai saatru"/>
</div>
<div class="lyrico-lyrics-wrapper">thiru kulam aadum potramarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiru kulam aadum potramarai"/>
</div>
<div class="lyrico-lyrics-wrapper">namai vaalthuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namai vaalthuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalyanam senju vacha meenachiku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalyanam senju vacha meenachiku "/>
</div>
<div class="lyrico-lyrics-wrapper">vaira kallale mookuthi than senju podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaira kallale mookuthi than senju podu"/>
</div>
</pre>
