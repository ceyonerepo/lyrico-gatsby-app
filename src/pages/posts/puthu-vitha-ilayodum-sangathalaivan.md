---
title: "puthu vitha ilayodum song lyrics"
album: "Sangathalaivan"
artist: "Robert Sargunam"
lyricist: "Uma Devi"
director: "Manimaaran"
path: "/albums/sangathalaivan-song-lyrics"
song: "Puthu Vitha Ilayodum"
image: ../../images/albumart/sangathalaivan.jpg
date: 2021-02-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3dp_-5ZI4As"
type: "Love"
singers:
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">puthuvitha ilayodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthuvitha ilayodum"/>
</div>
<div class="lyrico-lyrics-wrapper">niramaaga varamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niramaaga varamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">ival manam urangaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival manam urangaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">iravaaga nilavaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravaaga nilavaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">iru vizhigal siru tharipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru vizhigal siru tharipol"/>
</div>
<div class="lyrico-lyrics-wrapper">vanna vanna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanna vanna "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal pinnuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal pinnuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">nilam kadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilam kadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">udal mithakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal mithakka"/>
</div>
<div class="lyrico-lyrics-wrapper">chinna chinna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna chinna "/>
</div>
<div class="lyrico-lyrics-wrapper">maayam pannuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayam pannuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu azhagaga naanaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu azhagaga naanaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">thari izhaiyaaga noolaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thari izhaiyaaga noolaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhi thavari manam thiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhi thavari manam thiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkena kadhalai adainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkena kadhalai adainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu azhagaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu azhagaga "/>
</div>
<div class="lyrico-lyrics-wrapper">naanaanen poovaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanaanen poovaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaarengum kaanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaarengum kaanatha"/>
</div>
<div class="lyrico-lyrics-wrapper">oorondru kandeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorondru kandeney"/>
</div>
<div class="lyrico-lyrics-wrapper">naam vazha oru theevaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam vazha oru theevaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">nerangal paarkkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerangal paarkkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhvondru kondeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhvondru kondeney"/>
</div>
<div class="lyrico-lyrics-wrapper">urangaatha aaraanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urangaatha aaraanen"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai neeye udal naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai neeye udal naaney"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai aenthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai aenthum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalaagi vilayaadinen naaney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalaagi vilayaadinen naaney "/>
</div>
<div class="lyrico-lyrics-wrapper">oli veesi irul poosum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oli veesi irul poosum "/>
</div>
<div class="lyrico-lyrics-wrapper">uravaagi pagayaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravaagi pagayaagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan engu poonaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan engu poonaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaan pola unnaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaan pola unnaye"/>
</div>
<div class="lyrico-lyrics-wrapper">naan paarpen ninaivaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan paarpen ninaivaale"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ennai kaanaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ennai kaanaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">naan unnai kaanbene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan unnai kaanbene"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatraagi naan theenda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatraagi naan theenda "/>
</div>
<div class="lyrico-lyrics-wrapper">veyyil neeram kudai poley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyyil neeram kudai poley"/>
</div>
<div class="lyrico-lyrics-wrapper">enai kaakka nizhalaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enai kaakka nizhalaga "/>
</div>
<div class="lyrico-lyrics-wrapper">padarginraaye neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padarginraaye neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">ival thaagam ini neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival thaagam ini neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">oyaamal rasippeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyaamal rasippeney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puthu azhagaga naanaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu azhagaga naanaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">thari izhaiyaaga noolaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thari izhaiyaaga noolaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhi thavari manam thiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhi thavari manam thiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkena kadhalai adainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkena kadhalai adainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu azhagaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu azhagaga "/>
</div>
<div class="lyrico-lyrics-wrapper">naananen poovaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naananen poovaanen"/>
</div>
</pre>
