---
title: "i am bad boy song lyrics"
album: "Mirugaa"
artist: "Aruldev"
lyricist: "ARP Jayaraam"
director: "J.Parthiban"
path: "/albums/mirugaa-lyrics"
song: "I am Bad Boy"
image: ../../images/albumart/mirugaa.jpg
date: 2021-03-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/I1Eq-RcG8F8"
type: "Mass"
singers:
  - Ranjith
  - Swetha Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">I am a bad boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a bad boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa.."/>
</div>
<div class="lyrico-lyrics-wrapper">I am a bad boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a bad boy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavam naan paarpathillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam naan paarpathillada"/>
</div>
<div class="lyrico-lyrics-wrapper">Panamae en parama bothada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panamae en parama bothada"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasa patta adaiya venunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa patta adaiya venunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaiya venunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaiya venunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham unnai alikkum ethiri da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham unnai alikkum ethiri da"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattam athu ketta valiya da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattam athu ketta valiya da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolaiyum ingu kutham illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaiyum ingu kutham illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutham illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutham illa da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathaa mangaatha soodhattam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaa mangaatha soodhattam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaeyum veliyaeyum vaetta thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaeyum veliyaeyum vaetta thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan vachana thatti thookuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan vachana thatti thookuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai vachana vasam aakkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai vachana vasam aakkuven"/>
</div>
5<div class="lyrico-lyrics-wrapper">D’la padam ottuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="D’la padam ottuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Interpolukkum thanni kaattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Interpolukkum thanni kaattuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanni kaattuven thanni kaattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni kaattuven thanni kaattuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaar ivan ivan ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ivan ivan ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugaiyena vanthu pugunthu vittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugaiyena vanthu pugunthu vittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar ivan ivan ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ivan ivan ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthiryena nooru kelvi thanthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthiryena nooru kelvi thanthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai seendum thaeyilai kaattraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai seendum thaeyilai kaattraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu pogiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Marmangal theeraa mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marmangal theeraa mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadigaarathil neram aagiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigaarathil neram aagiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaippaagiren thaeneer aagiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaippaagiren thaeneer aagiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruga thalaivan avanthan yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruga thalaivan avanthan yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu google aanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu google aanathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I am a bad boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a bad boy"/>
</div>
<div class="lyrico-lyrics-wrapper">I am a bad boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a bad boy"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa…"/>
</div>
<div class="lyrico-lyrics-wrapper">I am a bad boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a bad boy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavul ingu polappu aachu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul ingu polappu aachu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal verum poluthu pokkudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal verum poluthu pokkudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam ithu pothu soththudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam ithu pothu soththudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Porambokku daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porambokku daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethirththaa nee kutravaali daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirththaa nee kutravaali daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduthaa nee buthisaali daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthaa nee buthisaali daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam ingu jolly jolly daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam ingu jolly jolly daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa poli daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa poli daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja naa raja naa raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja naa raja naa raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Raja naa raja naa raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja naa raja naa raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Kan vachana thatti thookuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan vachana thatti thookuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai vachana vasam aakkuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai vachana vasam aakkuven"/>
</div>
5<div class="lyrico-lyrics-wrapper">D’la padam ottuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="D’la padam ottuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Interpolukkum thanni kaattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Interpolukkum thanni kaattuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I am a bad boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a bad boy"/>
</div>
</pre>
