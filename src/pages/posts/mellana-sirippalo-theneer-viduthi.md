---
title: "mellana sirippalo song lyrics"
album: "Theneer Viduthi"
artist: "S.S. Kumaran"
lyricist: "Murugan Manthiram"
director: "S.S. Kumaran"
path: "/albums/theneer-viduthi-lyrics"
song: "Mellana Sirippalo"
image: ../../images/albumart/theneer-viduthi.jpg
date: 2011-07-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rw9eH22EPVc"
type: "love"
singers:
  - Kaushik
  - Mirudula
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mellanna Siripalo Jillena muraipalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellanna Siripalo Jillena muraipalo"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannai imai thindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannai imai thindrathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukul vizhundhalo kallukul mulaithalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukul vizhundhalo kallukul mulaithalo"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjam ennai vendradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjam ennai vendradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal ennum siragugal ennakena tharuvaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal ennum siragugal ennakena tharuvaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam thaandi parandhida 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam thaandi parandhida "/>
</div>
<div class="lyrico-lyrics-wrapper">thunai enna varuvaalo ohh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunai enna varuvaalo ohh"/>
</div>
<div class="lyrico-lyrics-wrapper">Mellanna Siripalo Jillena muraipalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellanna Siripalo Jillena muraipalo"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannai imai thindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannai imai thindrathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pandhakaran muli onnum sariyillakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhakaran muli onnum sariyillakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu mudich-onnu poda sollukaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu mudich-onnu poda sollukaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eno adi eno en idhayam mella udaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eno adi eno en idhayam mella udaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyaai oru theeyaai uyril vandhu kudhithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyaai oru theeyaai uyril vandhu kudhithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakena unnai padaithavan vandhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakena unnai padaithavan vandhal"/>
</div>
<div class="lyrico-lyrics-wrapper">aayiram nanri solven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram nanri solven"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayidham indri por onru seithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayidham indri por onru seithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">arai nodi ennai vendrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arai nodi ennai vendrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alzhaga iva-la-le adiyodu thozhaindhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alzhaga iva-la-le adiyodu thozhaindhene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mellanna Siripalo Jillena muraipalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellanna Siripalo Jillena muraipalo"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannai imai thindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannai imai thindrathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukul vizhundhalo kallukul mulaithalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukul vizhundhalo kallukul mulaithalo"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjam ennai vendradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjam ennai vendradhe"/>
</div>
</pre>
