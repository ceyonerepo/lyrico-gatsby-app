---
title: "talku lessu worku moreu song lyrics"
album: "Murungakkai Chips"
artist: "Dharan Kumar"
lyricist: "Ku Karthik"
director: "Srijar"
path: "/albums/murungakkai-chips-song-lyrics"
song: "Talku Lessu Worku Moreu"
image: ../../images/albumart/murungakkai-chips.jpg
date: 2021-12-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6Z-rd1Sx17E"
type: "happy"
singers:
  - Sivaangi Krish
  - Sam Vishal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Yela Yela Yela Yela Vaadi Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Yela Yela Yela Yela Vaadi Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartukulla Satham Illa Yendi Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartukulla Satham Illa Yendi Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yela Yela Parkaatha Yekkaththula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela Yela Parkaatha Yekkaththula"/>
</div>
<div class="lyrico-lyrics-wrapper">Night Pagala Unnaala Thookkam Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night Pagala Unnaala Thookkam Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Google Panni Paarthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Google Panni Paarthene"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Sera Vazhi Kettene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Sera Vazhi Kettene"/>
</div>
<div class="lyrico-lyrics-wrapper">Adada Indha Chance Su Ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Indha Chance Su Ku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Alanju Alanju Thiruncheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Alanju Alanju Thiruncheney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma Prachanaya Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Prachanaya Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaralaiyum Kaapaaththa Mudiyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaralaiyum Kaapaaththa Mudiyaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Talku Lessu Ini Worku Moreu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talku Lessu Ini Worku Moreu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Topula Pottachu Gearu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Topula Pottachu Gearu"/>
</div>
<div class="lyrico-lyrics-wrapper">Beatu Beatu Idhu Super Beatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beatu Beatu Idhu Super Beatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Kai Kaalu Nikkaathu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Kai Kaalu Nikkaathu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Talku Lessu Ini Worku Moreu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talku Lessu Ini Worku Moreu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Topula Pottachu Gearu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Topula Pottachu Gearu"/>
</div>
<div class="lyrico-lyrics-wrapper">Beatu Beatu Idhu Super Beatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beatu Beatu Idhu Super Beatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Kai Kaalu Nikkaathu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Kai Kaalu Nikkaathu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podu Thagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu Thagida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yela Yela Yela Yela Vaala Vaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yela Yela Yela Yela Vaala Vaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Heartu Kulla Saththam Illa Yennu Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartu Kulla Saththam Illa Yennu Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vacha Kannaththa Vangurathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Kannaththa Vangurathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Paakkama Naan Thoongurathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Paakkama Naan Thoongurathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru Sonnaalum Naa Kekkurathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru Sonnaalum Naa Kekkurathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Vanthu Sonnaa Thatti Pesurathulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Vanthu Sonnaa Thatti Pesurathulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichi Vittalum Thorathi Vittalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichi Vittalum Thorathi Vittalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Suthitha En Heartu Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Suthitha En Heartu Parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutham Vechaalum Illa Kaduchi Vechchaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutham Vechaalum Illa Kaduchi Vechchaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezhu Janma Thandi Kooda Thazhumbu Irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezhu Janma Thandi Kooda Thazhumbu Irukum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Google Panni Paarthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Google Panni Paarthene"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnu Sera Vazhi Kettene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Sera Vazhi Kettene"/>
</div>
<div class="lyrico-lyrics-wrapper">Adada Indha Chance Su Ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Indha Chance Su Ku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Alanju Alanju Thirinchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Alanju Alanju Thirinchene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Talku Lessu Ini Worku More Ru Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talku Lessu Ini Worku More Ru Ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Podu Thagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Podu Thagida"/>
</div>
<div class="lyrico-lyrics-wrapper">Beatu Beatu Idhu Super Beatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beatu Beatu Idhu Super Beatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Podu Thagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Podu Thagida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Talku Lessu Ini Worku More Ru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talku Lessu Ini Worku More Ru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Topula Pottachu Gearu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Topula Pottachu Gearu"/>
</div>
<div class="lyrico-lyrics-wrapper">Beatu Beatu Idhu Super Beatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beatu Beatu Idhu Super Beatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Kai Kaalu Nikkaathu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Kai Kaalu Nikkaathu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Talku Lessu Ini Worku Moreu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talku Lessu Ini Worku Moreu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Topula Pottachi Gear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Topula Pottachi Gear"/>
</div>
<div class="lyrico-lyrics-wrapper">Beat U Beat U Idhu Super Beat U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beat U Beat U Idhu Super Beat U"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Kai Kaalu Nikkathu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Kai Kaalu Nikkathu Paaru"/>
</div>
</pre>
