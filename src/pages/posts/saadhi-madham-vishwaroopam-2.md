---
title: "saadhi madham song lyrics"
album: "Vishwaroopam 2"
artist: "Ghibran Muhammad"
lyricist: "Kamal Haasan"
director: "Kamal Haasan"
path: "/albums/vishwaroopam-2-lyrics"
song: "Saadhi Madham"
image: ../../images/albumart/vishwaroopam-2.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lPpTSuXQCo4"
type: "mass"
singers:
  - Sathyaprakash
  - Andrea Jeremiah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Saadhi Madhamenum Vyathiye Pokkide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi Madhamenum Vyathiye Pokkide"/>
</div>
<div class="lyrico-lyrics-wrapper">Soothiram Sollidum Saaramithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soothiram Sollidum Saaramithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Enappadum Ulaga Marai Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Enappadum Ulaga Marai Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saadhi Madhamenum Vyathiye Pokkide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi Madhamenum Vyathiye Pokkide"/>
</div>
<div class="lyrico-lyrics-wrapper">Soothiram Sollidum Saaramithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soothiram Sollidum Saaramithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Enappadum Ulaga Marai Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Enappadum Ulaga Marai Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andavar Thottu Andivarai Aandum Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andavar Thottu Andivarai Aandum Mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathalinale Kadhal Sei Kadamai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathalinale Kadhal Sei Kadamai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Maiyal Seiyamal Uyvathellam Uythalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiyal Seiyamal Uyvathellam Uythalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathilagida Uy Uy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathilagida Uy Uy"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaran Kanaaikalai Ey Ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaran Kanaaikalai Ey Ey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Kalanthu Mei Mei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Kalanthu Mei Mei"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Seiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathilagida Uy Uy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathilagida Uy Uy"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaran Kanaaikalai Ey Ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaran Kanaaikalai Ey Ey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Kalanthu Mei Mei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Kalanthu Mei Mei"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Seiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayam Seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayam Seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">I Feel It Love It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel It Love It"/>
</div>
<div class="lyrico-lyrics-wrapper">Take Me And Fly Away
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take Me And Fly Away"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mogham Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogham Sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bring Me Hold Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bring Me Hold Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Help Me Fly Away
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Help Me Fly Away"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthidatha Pola kandru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthidatha Pola kandru"/>
</div>
<div class="lyrico-lyrics-wrapper">Orakannil Paarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orakannil Paarkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullarikkum Parvai Munbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullarikkum Parvai Munbu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puvi Erppum Thorkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvi Erppum Thorkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Virakathinal Thavithavarkal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virakathinal Thavithavarkal "/>
</div>
<div class="lyrico-lyrics-wrapper">Vagutha Nalvazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vagutha Nalvazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Varthaigal Ila Mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varthaigal Ila Mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathilagida Uy Uy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathilagida Uy Uy"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaran Kanaaikalai Ey Ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaran Kanaaikalai Ey Ey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Kalanthu Mei Mei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Kalanthu Mei Mei"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Seiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathilagida Uy Uy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathilagida Uy Uy"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaran Kanaaikalai Ey Ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaran Kanaaikalai Ey Ey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Kalanthu Mei Mei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Kalanthu Mei Mei"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Seiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvai Kenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Kenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Say It Know It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Say It Know It"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Is In Your Eyes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Is In Your Eyes"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thevai Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevai Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Believe It See It
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Believe It See It"/>
</div>
<div class="lyrico-lyrics-wrapper">Need To My Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Need To My Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaala Kaalamaga Eriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala Kaalamaga Eriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Jothi Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Jothi Munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum Kolum Maarinaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum Kolum Maarinaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadappathondru Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadappathondru Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvukalin Kelvikellam Unmai Bathilithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvukalin Kelvikellam Unmai Bathilithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaga Arinthathu Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaga Arinthathu Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saadhi Madhamenum Vyathiye Pokkide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi Madhamenum Vyathiye Pokkide"/>
</div>
<div class="lyrico-lyrics-wrapper">Soothiram Sollidum Saaramithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soothiram Sollidum Saaramithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Enappadum Ulaga Marai Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Enappadum Ulaga Marai Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saadhi Madhamenum Vyathiye Pokkide
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi Madhamenum Vyathiye Pokkide"/>
</div>
<div class="lyrico-lyrics-wrapper">Soothiram Sollidum Saaramithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soothiram Sollidum Saaramithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Enappadum Ulaga Marai Ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Enappadum Ulaga Marai Ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andavar Thottu Andivarai Aandum Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andavar Thottu Andivarai Aandum Mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathalinale Kadhal Sei Kadamai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathalinale Kadhal Sei Kadamai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Maiyal Seiyamal Uyvathellam Uythalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maiyal Seiyamal Uyvathellam Uythalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathilagida Uy Uy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathilagida Uy Uy"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaran Kanaaikalai Ey Ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaran Kanaaikalai Ey Ey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Kalanthu Mei Mei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Kalanthu Mei Mei"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Seiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manathilagida Uy Uy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathilagida Uy Uy"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaran Kanaaikalai Ey Ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaran Kanaaikalai Ey Ey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Kalanthu Mei Mei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Kalanthu Mei Mei"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Seiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Seiyada"/>
</div>
</pre>
