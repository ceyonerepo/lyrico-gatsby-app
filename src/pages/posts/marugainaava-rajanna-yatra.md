---
title: "marugainaava rajanna song lyrics"
album: "Yatra"
artist: "K"
lyricist: "Penchal Das"
director: "Mahi V Raghav"
path: "/albums/yatra-lyrics"
song: "Marugainaava Rajanna"
image: ../../images/albumart/yatra.jpg
date: 2019-02-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Jre78iOHcqI"
type: "sad"
singers:
  - Penchal Das
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Marugainaava Rajanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marugainaava Rajanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanumarugainaava Rajanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanumarugainaava Rajanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Inti Devudavee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Inti Devudavee"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Kanti Veluguvee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Kanti Veluguvee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vorigeenaava Raajanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vorigeenaava Raajanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vorigeenaava Raajanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vorigeenaava Raajanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Addumaanam Adivilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addumaanam Adivilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaalagaanee Yaalakaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaalagaanee Yaalakaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Painaboye Pacchulaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Painaboye Pacchulaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaadamma Mana Raajanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadamma Mana Raajanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvocche Daavallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvocche Daavallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagaa Poolujalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagaa Poolujalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosam Vechunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosam Vechunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Chejaree Potheevaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chejaree Potheevaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Challaanee Nee Navv00
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challaanee Nee Navv00"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakkaani Nee Nadakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkaani Nee Nadakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Racchabanda Cherakane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Racchabanda Cherakane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelaraalee Potheevaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelaraalee Potheevaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maata Tappani Raajanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maata Tappani Raajanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Madama Tippani Manishivayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madama Tippani Manishivayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruvajaalamu Nee Roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruvajaalamu Nee Roopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Saati Evarayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Saati Evarayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maa Gundello Gudisello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Gundello Gudisello"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluvuntavu Rajanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluvuntavu Rajanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Saayaam Sandhyaa Deepamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayaam Sandhyaa Deepamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Taluchukuntaamuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Taluchukuntaamuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Taluchukuntaamuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Taluchukuntaamuu"/>
</div>
</pre>
