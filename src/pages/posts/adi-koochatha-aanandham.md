---
title: "adi koochatha song lyrics"
album: "Aanandham"
artist: "S. A. Rajkumar"
lyricist: "Kalaikumar"
director: "N. Lingusamy"
path: "/albums/aanandham-lyrics"
song: "Adi Koochatha"
image: ../../images/albumart/aanandham.jpg
date: 2001-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/juBQa4donwI"
type: "love"
singers:
  - Sukhwinder Singh
  - Swarnalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">adi koochatha korachukadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi koochatha korachukadi"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kuchipudi aadaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kuchipudi aadaporen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kavadi thookikitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kavadi thookikitu"/>
</div>
<div class="lyrico-lyrics-wrapper">nan suthi suthi poda poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan suthi suthi poda poren"/>
</div>
<div class="lyrico-lyrics-wrapper">intha otagatha kachithama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha otagatha kachithama"/>
</div>
<div class="lyrico-lyrics-wrapper">otiga nanum varen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otiga nanum varen"/>
</div>
<div class="lyrico-lyrics-wrapper">nan ketikaran nichayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan ketikaran nichayama"/>
</div>
<div class="lyrico-lyrics-wrapper">pataya kelapa poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pataya kelapa poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en koochatha korachukiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en koochatha korachukiten"/>
</div>
<div class="lyrico-lyrics-wrapper">un kuchipudi paka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kuchipudi paka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kavadi thookika than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kavadi thookika than"/>
</div>
<div class="lyrico-lyrics-wrapper">un pakam vanthu nika poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pakam vanthu nika poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi mathalame unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi mathalame unna"/>
</div>
<div class="lyrico-lyrics-wrapper">nan thati pakata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan thati pakata"/>
</div>
<div class="lyrico-lyrics-wrapper">appalama ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appalama ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">una anachu norukata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="una anachu norukata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ne saruku parai aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne saruku parai aada"/>
</div>
<div class="lyrico-lyrics-wrapper">en idupa kodukata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en idupa kodukata"/>
</div>
<div class="lyrico-lyrics-wrapper">nan iruki kati puduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan iruki kati puduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">un elumba odikata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un elumba odikata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puyal pancha muyal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyal pancha muyal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">nan unna mathata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan unna mathata"/>
</div>
<div class="lyrico-lyrics-wrapper">nodi kooda asarama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nodi kooda asarama"/>
</div>
<div class="lyrico-lyrics-wrapper">nan othulaikata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan othulaikata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi kachikulla ponna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi kachikulla ponna "/>
</div>
<div class="lyrico-lyrics-wrapper">vaikum vithai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaikum vithai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">onna katata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna katata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en koochatha korachukiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en koochatha korachukiten"/>
</div>
<div class="lyrico-lyrics-wrapper">un kuchipudi paka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kuchipudi paka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kavadi thookika than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kavadi thookika than"/>
</div>
<div class="lyrico-lyrics-wrapper">un pakam vanthu nika poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pakam vanthu nika poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi kenja kenja unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi kenja kenja unna"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pancha pirikata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pancha pirikata"/>
</div>
<div class="lyrico-lyrics-wrapper">una suthi suthi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="una suthi suthi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">nan surai aadata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan surai aadata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan mutham onnu thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan mutham onnu thanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">un pecha niruthata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pecha niruthata"/>
</div>
<div class="lyrico-lyrics-wrapper">nan motham ena thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan motham ena thanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">un moocha niruthata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un moocha niruthata"/>
</div>
<div class="lyrico-lyrics-wrapper">idiyatha namathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idiyatha namathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nan unna kakata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan unna kakata"/>
</div>
<div class="lyrico-lyrics-wrapper">irutaki nan vangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irutaki nan vangi"/>
</div>
<div class="lyrico-lyrics-wrapper">nan una thangata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan una thangata"/>
</div>
<div class="lyrico-lyrics-wrapper">nan santhosa kinathula kuthichu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan santhosa kinathula kuthichu "/>
</div>
<div class="lyrico-lyrics-wrapper">tharkola than panikata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharkola than panikata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi koochatha korachukadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi koochatha korachukadi"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kuchipudi aadaporen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kuchipudi aadaporen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kavadi thookikitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kavadi thookikitu"/>
</div>
<div class="lyrico-lyrics-wrapper">nan suthi suthi poda poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan suthi suthi poda poren"/>
</div>
<div class="lyrico-lyrics-wrapper">intha otagatha kachithama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha otagatha kachithama"/>
</div>
<div class="lyrico-lyrics-wrapper">otiga nanum varen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otiga nanum varen"/>
</div>
<div class="lyrico-lyrics-wrapper">nan ketikaran nichayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan ketikaran nichayama"/>
</div>
<div class="lyrico-lyrics-wrapper">pataya kelapa poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pataya kelapa poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en koochatha korachukiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en koochatha korachukiten"/>
</div>
<div class="lyrico-lyrics-wrapper">un kuchipudi paka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kuchipudi paka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kavadi thookika than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kavadi thookika than"/>
</div>
<div class="lyrico-lyrics-wrapper">un pakam vanthu nika poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pakam vanthu nika poren"/>
</div>
</pre>
