---
title: "mannar mannar song lyrics"
album: "Mannar Vagaiyara"
artist: "Jakes Bejoy"
lyricist: "Sarathi"
director: "G. Boopathy Pandian"
path: "/albums/mannar-vagaiyara-lyrics"
song: "Mannar Mannar"
image: ../../images/albumart/mannar-vagaiyara.jpg
date: 2018-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kAGSgVbW9p0"
type: "happy"
singers:
  - Ananthu
  - Velmurugan
  - Kavitha Gopi	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paatuku venum thogaiyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatuku venum thogaiyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu kotaiyilae neenga thanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu kotaiyilae neenga thanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi parakkum mannaru vagaiyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi parakkum mannaru vagaiyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Araaarooo paadi thirinja naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Araaarooo paadi thirinja naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo yaar yaarukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo yaar yaarukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Opening song-u paada vanthurukkom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Opening song-u paada vanthurukkom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadum paadi thulaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadum paadi thulaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaran vaaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaran vaaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Bail edukkira vakkilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bail edukkira vakkilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu thanda pannikalam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu thanda pannikalam da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Jailu namba isschoolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jailu namba isschoolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Machan kitta sollikallam da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan kitta sollikallam da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">License eh illaama ottungaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="License eh illaama ottungaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Silencer saththatha kootungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silencer saththatha kootungada"/>
</div>
<div class="lyrico-lyrics-wrapper">Police-eh vanthaalum nillunga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police-eh vanthaalum nillunga da"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma pangali pera than sollunga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma pangali pera than sollunga da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fight-u-nah fight-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fight-u-nah fight-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Song-u nah song-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Song-u nah song-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundas-eh kattunga da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundas-eh kattunga da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannaru mannaru vagaiyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannaru mannaru vagaiyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga mannaru mannaru vagaiyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga mannaru mannaru vagaiyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannaru mannaru vagaiyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannaru mannaru vagaiyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga mannaru mannaru vagaiyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga mannaru mannaru vagaiyara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum illena sattam enna agum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum illena sattam enna agum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami illatha koyil agum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami illatha koyil agum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattai pottalum naan motta pottalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattai pottalum naan motta pottalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kekaama seiya matten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kekaama seiya matten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thappae senjalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappae senjalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu than right-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu than right-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga sonnalae katha weight-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga sonnalae katha weight-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelvi ethum kettalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelvi ethum kettalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Munja odaippum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munja odaippum"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetukkullae nallavenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetukkullae nallavenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga nadippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga nadippom"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukulla aiyyanaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukulla aiyyanaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaval iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaval iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru enna kettalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru enna kettalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli kuduppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli kuduppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannaru mannaru vagaiyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannaru mannaru vagaiyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga mannaru mannaru vagaiyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga mannaru mannaru vagaiyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannaru mannaru vagaiyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannaru mannaru vagaiyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga mannaru mannaru vagaiyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga mannaru mannaru vagaiyara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanan nan naanae nanan nanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanan nan naanae nanan nanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanan nan naanae naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanan nan naanae naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanan nan naanae naa nanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanan nan naanae naa nanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandha nan naanae naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandha nan naanae naa naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tent-u kottaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tent-u kottaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theanu mittaaiyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theanu mittaaiyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthu thinbomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthu thinbomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna sokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna sokki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga thangam thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga thangam thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aambala singam thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aambala singam thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai kaattaama podi pakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai kaattaama podi pakki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaali ground-u than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali ground-u than"/>
</div>
<div class="lyrico-lyrics-wrapper">Erangi vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangi vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti pudichikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti pudichikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi aada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaga motham munna vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaga motham munna vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aata arukkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aata arukkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaariyatha keduthu puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariyatha keduthu puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda paarkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda paarkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yogiyam than neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yogiyam than neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum othukiren da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum othukiren da"/>
</div>
<div class="lyrico-lyrics-wrapper">Erukenavae rendu somba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erukenavae rendu somba"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga kaanom da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga kaanom da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan otthukiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan otthukiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thaai pathinigira tha otthukiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thaai pathinigira tha otthukiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannaru mannaru vagaiyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannaru mannaru vagaiyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga mannaru mannaru vagaiyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga mannaru mannaru vagaiyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannaru mannaru vagaiyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannaru mannaru vagaiyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga mannaru mannaru vagaiyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga mannaru mannaru vagaiyara"/>
</div>
</pre>
