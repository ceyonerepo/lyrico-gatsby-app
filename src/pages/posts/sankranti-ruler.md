---
title: "sankranti song lyrics"
album: "Ruler"
artist: "Chirantan Bhatt"
lyricist: "Ramajogayya Sastry"
director: "K S Ravikumar"
path: "/albums/ruler-lyrics"
song: "Sankranti"
image: ../../images/albumart/ruler.jpg
date: 2019-12-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/IOb7JX_vMT4"
type: "happy"
singers:
  - Swaraag Keerthan
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aayo Aayo Aayorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayo Aayo Aayorey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangal Mangal Jaayorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangal Mangal Jaayorey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayo Sankranthi Aayorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayo Sankranthi Aayorey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayo Aayo Aayorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayo Aayo Aayorey"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangal Mangal Jaayorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangal Mangal Jaayorey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayo Sankranthi Aayorey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayo Sankranthi Aayorey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thulasammakka Narasammakka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulasammakka Narasammakka "/>
</div>
<div class="lyrico-lyrics-wrapper">Girijakka Geethakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girijakka Geethakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandindhey Mana Pantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandindhey Mana Pantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Kodavali Patti Kosina Panta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Kodavali Patti Kosina Panta"/>
</div>
<div class="lyrico-lyrics-wrapper">Intikkocchinaaka Pandaga Jaragaalantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intikkocchinaaka Pandaga Jaragaalantaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla Regalla Bangaraley Nindu Nattillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Regalla Bangaraley Nindu Nattillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gobbilla Singaraley Palle Mungillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gobbilla Singaraley Palle Mungillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaviranga Yedadhi Kastamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaviranga Yedadhi Kastamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhrustamai Oorantha Kolaatam Aadindhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhrustamai Oorantha Kolaatam Aadindhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolamma Kolo Sankranthi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolamma Kolo Sankranthi "/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Vacchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Vacchindhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koti Saradhaalu Sandhallu Tecchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koti Saradhaalu Sandhallu Tecchindhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makara Sankranthi Sankranthi Vacchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makara Sankranthi Sankranthi Vacchindhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Chirunavvu Rangulni Maarchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Chirunavvu Rangulni Maarchindhiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sankranthi Sankranthi Vacchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Sankranthi Vacchindhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koti Saradhaalu Sandhallu Tecchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koti Saradhaalu Sandhallu Tecchindhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makara Sankranthi Sankranthi Vacchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makara Sankranthi Sankranthi Vacchindhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Chirunavvu Rangulni Maarchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Chirunavvu Rangulni Maarchindhiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Deshamaina Yekkadaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Deshamaina Yekkadaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugodi Cheiye Paicheiyiley Paicheiyiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugodi Cheiye Paicheiyiley Paicheiyiley"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugodi Sattha Telesela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugodi Sattha Telesela"/>
</div>
<div class="lyrico-lyrics-wrapper">Vollonchi Pani Cheiyaliley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vollonchi Pani Cheiyaliley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhainaa Sampradhaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhainaa Sampradhaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannu Dhunney Vyavasaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannu Dhunney Vyavasaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenthetthulonaa Yedhigi Vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthetthulonaa Yedhigi Vunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Moolaalu Marchipovuley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolaalu Marchipovuley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolamma Kolo Sankranthi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolamma Kolo Sankranthi "/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Vacchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Vacchindhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koti Saradhaalu Sandhallu Tecchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koti Saradhaalu Sandhallu Tecchindhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makara Sankranthi Sankranthi Vacchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makara Sankranthi Sankranthi Vacchindhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Chirunavvu Rangulni Maarchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Chirunavvu Rangulni Maarchindhiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gobbiyallo Gobbiyallo Gobbigouramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gobbiyallo Gobbiyallo Gobbigouramma"/>
</div>
<div class="lyrico-lyrics-wrapper">Punyamisthaadhi Poojalu Cheddham Raaye Chilakamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punyamisthaadhi Poojalu Cheddham Raaye Chilakamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Galaganna Korikalanni Therenoyamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Galaganna Korikalanni Therenoyamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Manasaina Vaade Ninnu Manuvaadenammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Manasaina Vaade Ninnu Manuvaadenammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aakallu Theerchey Annadhaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakallu Theerchey Annadhaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Raithanney Kadha Yenaatiki Yenaatiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raithanney Kadha Yenaatiki Yenaatiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Raithanna Challangunte Chaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raithanna Challangunte Chaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lotantu Ledhu Lokanikii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lotantu Ledhu Lokanikii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Matti Kosamm Puttinollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matti Kosamm Puttinollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi Panche Palletoollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi Panche Palletoollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Roju Laage Ye Rajainanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Roju Laage Ye Rajainanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi Rojulu Ravaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi Rojulu Ravaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolamma Kolo Sankranthi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolamma Kolo Sankranthi "/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Vacchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Vacchindhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koti Saradhaalu Sandhallu Tecchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koti Saradhaalu Sandhallu Tecchindhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makara Sankranthi Sankranthi Vacchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makara Sankranthi Sankranthi Vacchindhiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Chirunavvu Rangulni Maarchindhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Chirunavvu Rangulni Maarchindhiraa"/>
</div>
</pre>
