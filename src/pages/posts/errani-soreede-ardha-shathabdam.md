---
title: "errani soreede song lyrics"
album: "Ardha Shathabdam"
artist: "Nawfal Raja AIS"
lyricist: "Lakshmi priyanka"
director: "Rawindra Pulle"
path: "/albums/ardha-shathabdam-lyrics"
song: "Errani Soreede - Ajore O Mama"
image: ../../images/albumart/ardha-shathabdam.jpg
date: 2021-03-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/eQYHN1M4NyQ"
type: "melody"
singers:
  - Mohana bhogaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ajore O Mama Kdigida Bolema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ajore O Mama Kdigida Bolema"/>
</div>
<div class="lyrico-lyrics-wrapper">Kusiti Nacharo Thaande Naajo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kusiti Nacharo Thaande Naajo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aslidallevaala Jandhedhe Valajanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aslidallevaala Jandhedhe Valajanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaandema Jeevaare Andhekajore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaandema Jeevaare Andhekajore"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arey..! Errani Sooreede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey..! Errani Sooreede"/>
</div>
<div class="lyrico-lyrics-wrapper">Poddhanthaa Sindesi Nidharoyaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddhanthaa Sindesi Nidharoyaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey..! Maa Sakkaani Chandrude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey..! Maa Sakkaani Chandrude"/>
</div>
<div class="lyrico-lyrics-wrapper">Reyanthaa Aadangaa Lesthunnaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reyanthaa Aadangaa Lesthunnaade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ontimeeda Jaaruthunna Semata Sukkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontimeeda Jaaruthunna Semata Sukkale"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthaaramgaa Merisipoye Ningi Sukkalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthaaramgaa Merisipoye Ningi Sukkalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dokkaloki Jaarukunte Ganji Methukule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dokkaloki Jaarukunte Ganji Methukule"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkalochhinattu Pongipovaa Bathukule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalochhinattu Pongipovaa Bathukule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Errani Sooreede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Errani Sooreede"/>
</div>
<div class="lyrico-lyrics-wrapper">Poddhanthaa Sindesi Nidharoyaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddhanthaa Sindesi Nidharoyaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Sakkaani Chandrude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Sakkaani Chandrude"/>
</div>
<div class="lyrico-lyrics-wrapper">Reyanthaa Aadangaa Lesthunnaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reyanthaa Aadangaa Lesthunnaade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ontimeeda Jaaruthunna Semata Sukkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontimeeda Jaaruthunna Semata Sukkale"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthaaramgaa Merisipoye Ningi Sukkalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthaaramgaa Merisipoye Ningi Sukkalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dokkaloki Jaarukunte Ganji Methukule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dokkaloki Jaarukunte Ganji Methukule"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkalochhinattu Pongipovaa Bathukule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalochhinattu Pongipovaa Bathukule"/>
</div>
</pre>
