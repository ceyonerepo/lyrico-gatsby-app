---
title: "kanna katti song lyrics"
album: "Kaalakkoothu"
artist: "Justin Prabhakaran"
lyricist: "Kattalai Jaya"
director: "M. Nagarajan"
path: "/albums/kaalakkoothu-lyrics"
song: "Kanna Katti"
image: ../../images/albumart/kaalakkoothu.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9L9bkXI6fmo"
type: "love"
singers:
  - Haricharan
  - Latha Krishna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanna katti kaatil vittu poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna katti kaatil vittu poraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannazhagi kannazhagi thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannazhagi kannazhagi thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya katti aathil vittu poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya katti aathil vittu poraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai nazhuvi kai nazhuvi thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai nazhuvi kai nazhuvi thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna katti kaatil vittu poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna katti kaatil vittu poraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannazhagi kannazhagi thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannazhagi kannazhagi thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya katti aathil vittu poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya katti aathil vittu poraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai nazhuvi kai nazhuvi thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai nazhuvi kai nazhuvi thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varamaa thavama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamaa thavama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kaiyil kedacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaiyil kedacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oravaa usura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oravaa usura"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa unna anachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa unna anachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravaa pagalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravaa pagalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna enni thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna enni thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu kanavaa nesamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu kanavaa nesamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa killi sirichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa killi sirichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanna thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthum boomi pola vandhenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthum boomi pola vandhenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthum poonaiyagi ponenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthum poonaiyagi ponenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna serathaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna serathaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poosaiyellam senjenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poosaiyellam senjenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna sentha pinnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna sentha pinnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaiyellam solvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyellam solvenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konja neram pesava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja neram pesava"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji thaanae pesava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji thaanae pesava"/>
</div>
<div class="lyrico-lyrics-wrapper">Em manasa thookikittu po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em manasa thookikittu po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna katti kaatil vittu poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna katti kaatil vittu poraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannazhagi kannazhagi thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannazhagi kannazhagi thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya katti aathil vittu poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya katti aathil vittu poraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai nazhuvi kai nazhuvi thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai nazhuvi kai nazhuvi thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnala thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnala thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennaanen naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennaanen naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevanthenae naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevanthenae naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn munnalaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn munnalaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullara paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullara paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn paarvayaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn paarvayaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumaari ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaari ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannalaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannalaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veralukkul veralayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veralukkul veralayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum korkkavaUsurukku usurena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum korkkavaUsurukku usurena"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaghava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaghava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En manasukkul kudithanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasukkul kudithanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum vaazhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum vaazhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenam thenam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenam thenam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharisanam kaanava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharisanam kaanava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnala ninnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnala ninnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan urughuranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan urughuranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna katti kaatil vittu poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna katti kaatil vittu poraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannazhagi kannazhagi thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannazhagi kannazhagi thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya katti aathil vittu poraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya katti aathil vittu poraalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai nazhuvi kai nazhuvi thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai nazhuvi kai nazhuvi thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varamaa thavama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varamaa thavama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kaiyil kedacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kaiyil kedacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oravaa usura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oravaa usura"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa unna anachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa unna anachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravaa pagalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravaa pagalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna enni thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna enni thavichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu kanavaa nesamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu kanavaa nesamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa killi sirichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa killi sirichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan jaada kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan jaada kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nee vaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nee vaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoda theeya moottathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoda theeya moottathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyala pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyala pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aasa kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aasa kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkaththa neeyum kootaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkaththa neeyum kootaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi mudhal mudi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi mudhal mudi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum theendava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum theendava"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal yethu mudivethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal yethu mudivethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En vizhiyila vidu katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vizhiyila vidu katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum podava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum podava"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiyila vidaiyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiyila vidaiyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollala kollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollala kollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan mayangurenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan mayangurenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thantha naanae thantha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thantha naanae thantha "/>
</div>
<div class="lyrico-lyrics-wrapper">naanae thanae naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanae thanae naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thantha naanae thantha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thantha naanae thantha "/>
</div>
<div class="lyrico-lyrics-wrapper">naanae naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanae naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thantha naanae thantha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thantha naanae thantha "/>
</div>
<div class="lyrico-lyrics-wrapper">naanae thanae naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanae thanae naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thantha naanae thantha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thantha naanae thantha "/>
</div>
<div class="lyrico-lyrics-wrapper">naanae naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanae naanae"/>
</div>
</pre>
