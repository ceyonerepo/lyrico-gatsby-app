---
title: "alangalan kuruvi song lyrics"
album: "Pulikuthi Pandi"
artist: "N R Raghunanthan"
lyricist: "Maniamudhan"
director: "M. Muthaiah"
path: "/albums/pulikuthi-pandi-song-lyrics"
song: "Alangalan Kuruvi"
image: ../../images/albumart/pulikuthi-pandi.jpg
date: 2021-01-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/VECzA3SxiOs"
type: "Love"
singers:
  - Vandana Srinivasan
  - Lijeshkumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Alangalankuruvi adi aagasamthu aruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alangalankuruvi adi aagasamthu aruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaala kaala thazhuvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaala kaala thazhuvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam vangi vanthen piravi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam vangi vanthen piravi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alangalankuruvi adi aagasamthu aruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alangalankuruvi adi aagasamthu aruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaala kaalathazhuvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaala kaalathazhuvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam vangi vanthen piravi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam vangi vanthen piravi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kotta kotta kotturiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta kotta kotturiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaiya thaan aatturiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaiya thaan aatturiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta kitta vanthu neeyum ennai kolluriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta kitta vanthu neeyum ennai kolluriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nikka vechu paakuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikka vechu paakuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum ennai kekkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum ennai kekkuriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannala thaan jaada kaatti ennai kolluriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala thaan jaada kaatti ennai kolluriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatha vida yaakkai idai kuranji pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha vida yaakkai idai kuranji pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethu vida vazhkai ippa inippa achi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu vida vazhkai ippa inippa achi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alangalankuruvi adi aagasamthu aruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alangalankuruvi adi aagasamthu aruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaala kaala thazhuvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaala kaala thazhuvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam vangi vanthen piravi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam vangi vanthen piravi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kooda pesuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kooda pesuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai paththi pesuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai paththi pesuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Verayethum theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verayethum theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo onnum puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo onnum puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kooda nadakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kooda nadakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai suththi nadakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai suththi nadakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Veraiyethum thonale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veraiyethum thonale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo naanum naan illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo naanum naan illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanai ethanai natchathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai ethanai natchathiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Enni thaane paakkanume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enni thaane paakkanume"/>
</div>
<div class="lyrico-lyrics-wrapper">Karpanani karpanai senjathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karpanani karpanai senjathellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhnthu kaattunume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhnthu kaattunume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhaga padachi kudutha uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga padachi kudutha uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu thaan varamum kudukkum urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu thaan varamum kudukkum urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alangalankuruvi adi aagasamthu aruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alangalankuruvi adi aagasamthu aruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaala kaala thazhuvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaala kaala thazhuvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam vangi vanthen piravi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam vangi vanthen piravi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Entha pakkam thottalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha pakkam thottalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karkandu inikkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karkandu inikkume"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha pola un nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha pola un nenappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjikulla irukkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikulla irukkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna nee sonnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nee sonnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkanumnu thonume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkanumnu thonume"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna solla intha bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna solla intha bandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayusukkum vehnume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayusukkum vehnume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjulla nenjulla ullathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjulla nenjulla ullathellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannulla kannulla naan padippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulla kannulla naan padippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidam vanthu nee ketkum munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidam vanthu nee ketkum munne"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha kaiyula naan kuduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha kaiyula naan kuduppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nesama vazhkai azhaga irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesama vazhkai azhaga irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhala iruppen ini naan unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhala iruppen ini naan unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alangalankuruvi adi aagasamthu aruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alangalankuruvi adi aagasamthu aruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaala kaala thazhuvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaala kaala thazhuvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam vangi vanthen piravi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam vangi vanthen piravi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kotta kotta kotturiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta kotta kotturiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaiya thaan aatturiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaiya thaan aatturiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannala jaada kaatti ennai kolluriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala jaada kaatti ennai kolluriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatha vida yaakkai idai kuranji pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha vida yaakkai idai kuranji pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethu vida vazhkai ippa inippa aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethu vida vazhkai ippa inippa aachi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alangalankuruvi adi aagasamthu aruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alangalankuruvi adi aagasamthu aruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaala kaala thazhuvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaala kaala thazhuvi"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam vangi vanthen piravi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam vangi vanthen piravi"/>
</div>
</pre>
