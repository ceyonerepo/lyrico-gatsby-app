---
title: "kaatrilae song lyrics"
album: "Mudhal Nee Mudivum Nee"
artist: "Darbuka Siva"
lyricist: "Kaber Vasuki"
director: "Darbuka Siva"
path: "/albums/mudhal-nee-mudivum-nee-lyrics"
song: "Kaatrilae"
image: ../../images/albumart/mudhal-nee-mudivum-nee.jpg
date: 2022-01-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bNXfRfDQE4A"
type: "happy"
singers:
  - Nakul Abhyankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaatrilae neram mella parakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrilae neram mella parakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi pokkilae aasai koodi kidakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi pokkilae aasai koodi kidakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam viriyum namakkagavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam viriyum namakkagavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaana kanavellam uruvakkavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana kanavellam uruvakkavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valargiradhae vaazhvin pirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valargiradhae vaazhvin pirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhinilae natpai nirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhinilae natpai nirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukkathilae ennai azhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukkathilae ennai azhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuven pala nadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuven pala nadai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilaiyadi vizhundhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadi vizhundhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala paadam kooda thazhumbaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala paadam kooda thazhumbaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraindhodi thazhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraindhodi thazhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir natpaalae kaayam idhamagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir natpaalae kaayam idhamagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi manadhil inikkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi manadhil inikkirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum neeyum inaindhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum neeyum inaindhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyum nam vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyum nam vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eninum anbendrum nenjil kuraiyadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eninum anbendrum nenjil kuraiyadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugalin kaalathilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugalin kaalathilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karam kodutha nanbargalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karam kodutha nanbargalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yugam kadakkum vinmeen ozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yugam kadakkum vinmeen ozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Namai pol minmini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namai pol minmini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netru polae ippodhu ilai ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru polae ippodhu ilai ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum yeno dhisai maarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum yeno dhisai maarudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam mukki thikkadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam mukki thikkadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigalai udai vazhi thani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalai udai vazhi thani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo hoo hoo hooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo hoo hoo hooo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrilae neram mella parakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrilae neram mella parakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi pokkile aasai koodi kidakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi pokkile aasai koodi kidakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Arumbu pagai mudindha kadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arumbu pagai mudindha kadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhai inge niraivaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai inge niraivaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirindhum nam vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirindhum nam vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarum anbendrum nenjil alaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarum anbendrum nenjil alaiyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valargiradhae vaazhvin pirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valargiradhae vaazhvin pirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhinilae natpai nirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhinilae natpai nirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukkathilae ennai azhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukkathilae ennai azhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuven pala nadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuven pala nadai"/>
</div>
</pre>
