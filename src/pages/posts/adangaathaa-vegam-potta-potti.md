---
title: "adangaathaa vegam song lyrics"
album: "Potta Potti"
artist: "Aruldev"
lyricist: "Jayamurasu - Kevin Shadrach"
director: "Yuvaraj Dhayalan"
path: "/albums/potta-potti-lyrics"
song: "Adangaathaa Vegam"
image: ../../images/albumart/potta-potti.jpg
date: 2011-08-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/M0WujEoyWUA"
type: "happy"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adangaadha vegamkondu vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangaadha vegamkondu vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kalangaadhe thadaigal thaandi vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangaadhe thadaigal thaandi vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">erindhaalum dheebamaai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erindhaalum dheebamaai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">udhirndhaalum vidhaigalaai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udhirndhaalum vidhaigalaai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">saaindhaalum boomi suththudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaindhaalum boomi suththudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">udhirndhaalum pookkal pookkumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udhirndhaalum pookkal pookkumey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravugalai keeripaarthaal vidindhidum naal thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravugalai keeripaarthaal vidindhidum naal thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyumvarai moadhippaarththaal jeyippadhum naamthaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyumvarai moadhippaarththaal jeyippadhum naamthaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Oo Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Oo Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oi minnalena paayum vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oi minnalena paayum vegam"/>
</div>
<div class="lyrico-lyrics-wrapper">namakkadhu vendumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namakkadhu vendumadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayamadhil vaanam thaangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayamadhil vaanam thaangum"/>
</div>
<div class="lyrico-lyrics-wrapper">valimaiyum serungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valimaiyum serungadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aathirangal kolla avasiyamey illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aathirangal kolla avasiyamey illa"/>
</div>
<div class="lyrico-lyrics-wrapper">arivey yoasi OhO hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arivey yoasi OhO hO"/>
</div>
<div class="lyrico-lyrics-wrapper">OhO hO OhO hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OhO hO OhO hO"/>
</div>
<div class="lyrico-lyrics-wrapper">vedhanaigal ellaam karaindhoadum Odum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedhanaigal ellaam karaindhoadum Odum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaattaaru kaalam paarkkaadhey hO hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaattaaru kaalam paarkkaadhey hO hey"/>
</div>
<div class="lyrico-lyrics-wrapper">hO hey hO hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hO hey hO hey"/>
</div>
<div class="lyrico-lyrics-wrapper">udalil vizhum kaayam yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udalil vizhum kaayam yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">parisugal poal aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parisugal poal aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">uyiril valikkooda kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyiril valikkooda kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">payirchigal niraivaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payirchigal niraivaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">O hO hO hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O hO hO hO"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathangarai ellaam arindhuvai mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathangarai ellaam arindhuvai mella"/>
</div>
<div class="lyrico-lyrics-wrapper">salikkaamal aadu hO hO hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="salikkaamal aadu hO hO hO"/>
</div>
<div class="lyrico-lyrics-wrapper">hO hO hO hO hO hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hO hO hO hO hO hO"/>
</div>
<div class="lyrico-lyrics-wrapper">poar gunamey vellum kurippaarththu Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poar gunamey vellum kurippaarththu Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyendrum keezhey paarkkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyendrum keezhey paarkkaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">hO hO hO hO hO hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hO hO hO hO hO hO"/>
</div>
<div class="lyrico-lyrics-wrapper">hO hO hO
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hO hO hO"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adangaadha vegamkondu vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangaadha vegamkondu vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kalangaadhe thadaigal thaandi vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalangaadhe thadaigal thaandi vaa vaa vaa"/>
</div>
</pre>
