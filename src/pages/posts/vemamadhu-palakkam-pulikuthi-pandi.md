---
title: "venamadhu palakkam song lyrics"
album: "Pulikuthi Pandi"
artist: "N R Raghunanthan"
lyricist: "Araikudi Bharathi Ganeshan "
director: "M. Muthaiah"
path: "/albums/pulikuthi-pandi-song-lyrics"
song: "Venamadhu palakkam"
image: ../../images/albumart/pulikuthi-pandi.jpg
date: 2021-01-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UecQ0FkYLd0"
type: "Motivational"
singers:
  - Mahalingam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">venamadhu palakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venamadhu palakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">venamunga kudi palakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venamunga kudi palakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">venamadhu palakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venamadhu palakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">venamunga kudi palakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venamunga kudi palakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">veena odambu kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veena odambu kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">kodalu nogume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodalu nogume"/>
</div>
<div class="lyrico-lyrics-wrapper">veena odambu kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veena odambu kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">kodalu nogume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodalu nogume"/>
</div>
<div class="lyrico-lyrics-wrapper">unga aasa manaivi pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga aasa manaivi pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">ambo vagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambo vagume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pathinaru vayasu pulla...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathinaru vayasu pulla..."/>
</div>
<div class="lyrico-lyrics-wrapper">pathinaru vayasu pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathinaru vayasu pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">pallikoodam pora pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pallikoodam pora pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">pathinaru vayasu pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathinaru vayasu pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">pallikoodam pora pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pallikoodam pora pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">party nu angum ingum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="party nu angum ingum"/>
</div>
<div class="lyrico-lyrics-wrapper">kudikka poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudikka poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">party nu angum ingum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="party nu angum ingum"/>
</div>
<div class="lyrico-lyrics-wrapper">kudikka poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudikka poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">namma panpaada than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma panpaada than"/>
</div>
<div class="lyrico-lyrics-wrapper">maranthu puttu paala poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranthu puttu paala poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">namma panpaada than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma panpaada than"/>
</div>
<div class="lyrico-lyrics-wrapper">maranthu puttu paala poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranthu puttu paala poguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manitha inathuku ellam...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manitha inathuku ellam..."/>
</div>
<div class="lyrico-lyrics-wrapper">manitha inathuku ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manitha inathuku ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">mathipu tharum naaye kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathipu tharum naaye kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">manitha inathuku ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manitha inathuku ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">mathipu tharum naaye kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathipu tharum naaye kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">kudichavan munne vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudichavan munne vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">othungi ooduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othungi ooduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kudichavan munne vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudichavan munne vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">othungi ooduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othungi ooduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne paduthu iruntha vaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne paduthu iruntha vaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi eee than aaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi eee than aaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne paduthu iruntha vaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne paduthu iruntha vaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">suthi eee than aaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthi eee than aaduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">venamadhu palakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venamadhu palakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">venamunga kudi palakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venamunga kudi palakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">venamadhu palakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venamadhu palakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">venamunga kudi palakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venamunga kudi palakkam"/>
</div>
</pre>
