---
title: "hey idi nenena song lyrics"
album: "Solo Brathuke So Better"
artist: "S. Thaman"
lyricist: "Raghuram"
director: "Subbu"
path: "/albums/solo-brathuke-so-better-lyrics"
song: "Hey Idi Nenena"
image: ../../images/albumart/solo-brathuke-so-better.jpg
date: 2020-12-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/AmGn686QCI0"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey idi nenena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey idi nenena"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey idi nizamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey idi nizamena"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa addhamlona kothaga kanabaduthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa addhamlona kothaga kanabaduthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee solo bathuke nuvvochesake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee solo bathuke nuvvochesake"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne thosthondhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne thosthondhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kada dhaaka nee yenake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada dhaaka nee yenake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheem thom thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem thom thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem thom thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem thom thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem dheem thanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem dheem thanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem thom thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem thom thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello modhalayyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello modhalayyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem dheem thanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem dheem thanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem thom thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem thom thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem thom thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem thom thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem dheem thanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem dheem thanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem thom thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem thom thom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannitta cherindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannitta cherindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheem dheem thanana thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheem dheem thanana thom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thelisindhe pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelisindhe pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannulake velugocchela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannulake velugocchela"/>
</div>
<div class="lyrico-lyrics-wrapper">Palikindhe pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palikindhe pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikotha sangeethamla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikotha sangeethamla"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvindhe pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvindhe pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Navarathnaale kurisela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navarathnaale kurisela"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey merisindhe pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey merisindhe pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnami vennela sandhramla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnami vennela sandhramla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelakasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelakasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakosam harivillai maarindhanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakosam harivillai maarindhanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee avakasham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee avakasham"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherajindhante malli raadhanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherajindhante malli raadhanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Anumathinisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anumathinisthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee penivitinai untane nee janta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee penivitinai untane nee janta"/>
</div>
<div class="lyrico-lyrics-wrapper">Alochisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alochisthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhepudo jarigina katha manadhenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhepudo jarigina katha manadhenanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey idi nenena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey idi nenena"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey idi nizamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey idi nizamena"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa addhamlona kothaga kanabaduthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa addhamlona kothaga kanabaduthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee solo bathuke nuvvochesake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee solo bathuke nuvvochesake"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne thosthondhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne thosthondhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kada dhaaka nee yenake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada dhaaka nee yenake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey idi nenena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey idi nenena"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey idi nizamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey idi nizamena"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa addhamlona kothaga kanabaduthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa addhamlona kothaga kanabaduthunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">May nello manche padinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="May nello manche padinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarigindhe edho kanikattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarigindhe edho kanikattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammettu gaane lenattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammettu gaane lenattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Winter vinthalu ennenno jarigettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Winter vinthalu ennenno jarigettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chesesave neemeedhottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesesave neemeedhottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kacchithanga naalone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kacchithanga naalone"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogindhedho sannaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogindhedho sannaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee vidhanga mundheppudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee vidhanga mundheppudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lene ledhe ammayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lene ledhe ammayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey idi nenena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey idi nenena"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey idi nizamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey idi nizamena"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa addhamlona kothaga kanabaduthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa addhamlona kothaga kanabaduthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee solo bathuke nuvvochesake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee solo bathuke nuvvochesake"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanne thosthondhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanne thosthondhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kada dhaaka nee yenake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada dhaaka nee yenake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey idi nenena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey idi nenena"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey idi nizamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey idi nizamena"/>
</div>
</pre>
