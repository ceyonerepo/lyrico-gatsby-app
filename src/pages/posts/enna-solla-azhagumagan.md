---
title: "enna solla song lyrics"
album: "Azhagumagan"
artist: "James Vasanthan"
lyricist: "Thamarai"
director: "Azhagan Selva"
path: "/albums/azhagumagan-lyrics"
song: "Enna Solla"
image: ../../images/albumart/azhagumagan.jpg
date: 2022-05-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7WSZqcl_25U"
type: "sad"
singers:
  - Jay
  - Sai Sutha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">enna solla yethu solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna solla yethu solla"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalka verum vaartha illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalka verum vaartha illa"/>
</div>
<div class="lyrico-lyrics-wrapper">patta pinne puthi varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta pinne puthi varum"/>
</div>
<div class="lyrico-lyrics-wrapper">yaro inge ngaani illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaro inge ngaani illa"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu than vaalkai yinu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu than vaalkai yinu"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaikira poluthunile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaikira poluthunile"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai keela mariduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai keela mariduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kanimaikkum nerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanimaikkum nerathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kangalil thondrum kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangalil thondrum kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyagi pogirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyagi pogirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyodu vaala solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyodu vaala solli"/>
</div>
<div class="lyrico-lyrics-wrapper">kai aati selgirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai aati selgirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">idaivelai vaalkaiyil illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaivelai vaalkaiyil illai"/>
</div>
<div class="lyrico-lyrics-wrapper">mudiyum varai ooive illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudiyum varai ooive illai"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagathil paasam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagathil paasam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ethuvume uyaram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuvume uyaram illai"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalum kadavulum ondre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalum kadavulum ondre"/>
</div>
<div class="lyrico-lyrics-wrapper">irandume therivathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irandume therivathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">irupathai nambi kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupathai nambi kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalgirome mudive illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalgirome mudive illai"/>
</div>
<div class="lyrico-lyrics-wrapper">irukkum bothu ellam inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukkum bothu ellam inge"/>
</div>
<div class="lyrico-lyrics-wrapper">perithaaga theriyaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perithaaga theriyaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ilantha pinne ethuvum inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilantha pinne ethuvum inge"/>
</div>
<div class="lyrico-lyrics-wrapper">aluthaalum thirumbathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aluthaalum thirumbathu"/>
</div>
<div class="lyrico-lyrics-wrapper">yasithal vaalkai varavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yasithal vaalkai varavu"/>
</div>
<div class="lyrico-lyrics-wrapper">swasithal vaalkai selavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swasithal vaalkai selavu"/>
</div>
<div class="lyrico-lyrics-wrapper">nesithal vaalkai alagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nesithal vaalkai alagu"/>
</div>
<div class="lyrico-lyrics-wrapper">yosithal vaalkai sarivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yosithal vaalkai sarivu"/>
</div>
</pre>
