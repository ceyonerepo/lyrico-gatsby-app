---
title: "thaye thaye song lyrics"
album: "Kadhalar Kudiyiruppu"
artist: "James Vasanthan"
lyricist: "Na. Muthukumar"
director: "AMR Ramesh"
path: "/albums/kadhalar-kudiyiruppu-lyrics"
song: "Thaye Thaye"
image: ../../images/albumart/kadhalar-kudiyiruppu.jpg
date: 2011-02-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6os73TbY23E"
type: "sad"
singers:
  - Srinivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaaye nee en vaazhvil mudhalallavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaye nee en vaazhvil mudhalallavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaaney en vaazhvin mudivallavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaaney en vaazhvin mudivallavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaappaatri valarthaaye ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaappaatri valarthaaye ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaamal tholaiththeney unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaamal tholaiththeney unnai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakkaaga thaalaattu theeppaaduvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkaaga thaalaattu theeppaaduvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkaaga theemeedhum nee Oduvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkaaga theemeedhum nee Oduvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En kannil nee vandhaal nee vaaduvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kannil nee vandhaal nee vaaduvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppoadhum enaithaaney neeththeduvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppoadhum enaithaaney neeththeduvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaaga naan enna seithenammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga naan enna seithenammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un anbukkadan endrum theeraadhammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un anbukkadan endrum theeraadhammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaappaatri valarthaaye ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaappaatri valarthaaye ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaamal tholaiththeney unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaamal tholaiththeney unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaye thaaye theeyoadu engey sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaye thaaye theeyoadu engey sendraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeye theeye thaayoadu engey sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeye theeye thaayoadu engey sendraai"/>
</div>
</pre>
