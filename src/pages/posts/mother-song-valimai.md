---
title: "mother song song lyrics"
album: "Valimai"
artist: "Yuvan Shankar Raja"
lyricist: "Vignesh Shivan"
director: "H. Vinoth"
path: "/albums/valimai-lyrics"
song: "Mother Song"
image: ../../images/albumart/valimai.jpg
date: 2022-01-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/auBq_Z6zWSE"
type: "happy"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan Paartha Mudhal Mugam Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paartha Mudhal Mugam Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ketta Mudhal Kural Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ketta Mudhal Kural Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Paartha Mudhal Mugam Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Paartha Mudhal Mugam Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Ketta Mudhal Kural Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Ketta Mudhal Kural Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mugarndha Mudhal Malarum Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mugarndha Mudhal Malarum Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Vazhndha Mudhal Arai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vazhndha Mudhal Arai Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Varaindha Mudhal Padam Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Varaindha Mudhal Padam Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Virumbiya Mudhal Pennum Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Virumbiya Mudhal Pennum Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sinungiyabodhu Sirikka Veithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinungiyabodhu Sirikka Veithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragugal Valarthu Parakka Vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragugal Valarthu Parakka Vaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sigarangal Yera Sollikoduthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sigarangal Yera Sollikoduthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavalodu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavalodu Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarndhavan Pola Therindhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarndhavan Pola Therindhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kannil Naanum Oru Kuzhandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kannil Naanum Oru Kuzhandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigalukkulle Adaikaathaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigalukkulle Adaikaathaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaiyodu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyodu Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma En Mugavari Nee Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma En Mugavari Nee Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mudhal Vari Nee Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mudhal Vari Nee Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyir Endrum Nee Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Endrum Nee Amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeye Enakena Pirandhaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Enakena Pirandhaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithaiyum Thandhaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithaiyum Thandhaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ulagam Nee En Thaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulagam Nee En Thaaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaasam Enakku Valimai Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaasam Enakku Valimai Tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaarthai Enakku Veeram Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaarthai Enakku Veeram Tharum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vazhkaiyin Mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vazhkaiyin Mel"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vazhkaiyinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vazhkaiyinai"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaindhu Vaithaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaindhu Vaithaaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Tholvi Ennai Thodumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Tholvi Ennai Thodumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Tholai Vandhu Thoduvaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Tholai Vandhu Thoduvaaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thottadhume Thulangidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thottadhume Thulangidume"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam Maarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam Maarume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidumuraiye Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidumuraiye Illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thai Velai Seigiraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thai Velai Seigiraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Idharkaana Kaanikayaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idharkaana Kaanikayaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Enna Dhan Tharuvadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enna Dhan Tharuvadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma Oh Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Oh Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amma En Mugavari Nee Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma En Mugavari Nee Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mudhal Vari Nee Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mudhal Vari Nee Amma"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyir Endrum Nee Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Endrum Nee Amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeye Enakena Pirandhaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye Enakena Pirandhaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithaiyum Thandhaaye Thandhaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithaiyum Thandhaaye Thandhaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ulagam Nee En Thaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ulagam Nee En Thaaiye"/>
</div>
</pre>
