---
title: "i'm a fighter song lyrics"
album: "Oh My Dog"
artist: "Nivas K Prasanna"
lyricist: "M K Balaji"
director: "Sarov Shanmugam"
path: "/albums/oh-my-dog-lyrics"
song: "I'm A Fighter"
image: ../../images/albumart/oh-my-dog.jpg
date: 2022-04-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/q_oJZkBEq2w"
type: "happy"
singers:
  - Nivas K Prasanna
  - MK Balaji
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Make the way for the fighter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make the way for the fighter"/>
</div>
<div class="lyrico-lyrics-wrapper">Rising Like a soldier
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rising Like a soldier"/>
</div>
<div class="lyrico-lyrics-wrapper">Everyday grow stronger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everyday grow stronger"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Make way for the winner
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make way for the winner"/>
</div>
<div class="lyrico-lyrics-wrapper">Walking with a saaoulder
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Walking with a saaoulder"/>
</div>
<div class="lyrico-lyrics-wrapper">Thru the night thats colder
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thru the night thats colder"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikadi nee irakkaneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikadi nee irakkaneru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eendu eendu pirapirandhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eendu eendu pirapirandhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Silaninaivai aarakkavendua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silaninaivai aarakkavendua"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalaonokki nada nadanthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalaonokki nada nadanthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai athai thaandavenduaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai athai thaandavenduaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru ellai varaivarindhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru ellai varaivarindhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhigirigaluaa nadungavenduaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhigirigaluaa nadungavenduaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyuaakannil theri therindhida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyuaakannil theri therindhida"/>
</div>
<div class="lyrico-lyrics-wrapper">Va modhipaaru i am a fighter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va modhipaaru i am a fighter"/>
</div>
<div class="lyrico-lyrics-wrapper">Moraikkura aala paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moraikkura aala paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">I am a fighter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a fighter"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettayada vettayada vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettayada vettayada vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighter vizhundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighter vizhundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Melayerum antha fighter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melayerum antha fighter"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedikira theepori antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikira theepori antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighter winning in the game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighter winning in the game"/>
</div>
<div class="lyrico-lyrics-wrapper">I am a fighter I’m a fighter credits
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a fighter I’m a fighter credits"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey do you wanna play with fire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey do you wanna play with fire"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey that’s gonna take you higher
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey that’s gonna take you higher"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey ready to re rewire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey ready to re rewire"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey become what you desire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey become what you desire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Do you wanna play with fire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do you wanna play with fire"/>
</div>
<div class="lyrico-lyrics-wrapper">Thats gonna take you higher
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thats gonna take you higher"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready to re rewire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready to re rewire"/>
</div>
<div class="lyrico-lyrics-wrapper">Become what you desire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Become what you desire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You be the fighter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You be the fighter"/>
</div>
<div class="lyrico-lyrics-wrapper">You be the best
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You be the best"/>
</div>
<div class="lyrico-lyrics-wrapper">You be the legend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You be the legend"/>
</div>
<div class="lyrico-lyrics-wrapper">Thumping on your chest
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thumping on your chest"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dream believe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dream believe"/>
</div>
<div class="lyrico-lyrics-wrapper">Try and hope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Try and hope"/>
</div>
<div class="lyrico-lyrics-wrapper">The four help you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The four help you"/>
</div>
<div class="lyrico-lyrics-wrapper">More more than the dope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="More more than the dope"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Learn a little fast
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Learn a little fast"/>
</div>
<div class="lyrico-lyrics-wrapper">Skin a litle deep
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Skin a litle deep"/>
</div>
<div class="lyrico-lyrics-wrapper">Think a little smart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Think a little smart"/>
</div>
<div class="lyrico-lyrics-wrapper">You are ready to fight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are ready to fight"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Night and day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night and day"/>
</div>
<div class="lyrico-lyrics-wrapper">You are ready to sweat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You are ready to sweat"/>
</div>
<div class="lyrico-lyrics-wrapper">Its the ride of your life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Its the ride of your life"/>
</div>
<div class="lyrico-lyrics-wrapper">So hold it tight now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So hold it tight now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazha vidu vaanai thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha vidu vaanai thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeram athu veezha maru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram athu veezha maru"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyil padu theeti edu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyil padu theeti edu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram kodu nerum vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram kodu nerum vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unekkena vidhi kondu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unekkena vidhi kondu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaginai athil kondu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaginai athil kondu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai norukku balam kondu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai norukku balam kondu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirakadikka vaanam kondu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirakadikka vaanam kondu vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam dhinam palakallai serthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam dhinam palakallai serthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum kattu vervai anai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum kattu vervai anai"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochenbathu muyarchi yaanal unakku illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochenbathu muyarchi yaanal unakku illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eedu inai va modhipaaru i am a fighter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedu inai va modhipaaru i am a fighter"/>
</div>
<div class="lyrico-lyrics-wrapper">Moraikkura aala paaru i am a fighter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moraikkura aala paaru i am a fighter"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettayada vettaya vantha fighter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettayada vettaya vantha fighter"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhundhalum melayerum antha fighter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhalum melayerum antha fighter"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikkira theepori antha fighter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkira theepori antha fighter"/>
</div>
<div class="lyrico-lyrics-wrapper">Winning in the game
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Winning in the game"/>
</div>
<div class="lyrico-lyrics-wrapper">I am a fighter i am a fighter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a fighter i am a fighter"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey you are the sky with stars
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey you are the sky with stars"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey with all the mighty scars
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey with all the mighty scars"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey shine like the mighty sun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey shine like the mighty sun"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey you are the only one
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey you are the only one"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Make the way for the fighter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make the way for the fighter"/>
</div>
<div class="lyrico-lyrics-wrapper">Rising like a soldier
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rising like a soldier"/>
</div>
<div class="lyrico-lyrics-wrapper">Everyday grew stronger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everyday grew stronger"/>
</div>
<div class="lyrico-lyrics-wrapper">Vali enbathu valimai endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali enbathu valimai endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indru netru naalai ninai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru netru naalai ninai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thazhumbugal dhan veeran aakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhumbugal dhan veeran aakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Angum ingum engum unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angum ingum engum unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey i am a fighter iam a fighter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey i am a fighter iam a fighter"/>
</div>
</pre>
