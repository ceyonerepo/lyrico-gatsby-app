---
title: "azhage song lyrics"
album: "Action"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "R Vijay Anand - AR Suriyan"
path: "/albums/thavam-lyrics"
song: "Azhage"
image: ../../images/albumart/thavam.jpg
date: 2019-11-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5qrjBDbyjOc"
type: "love"
singers:
  - Nakul Abhyankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haiyo Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyo Azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyo Haiyo Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyo Haiyo Azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyo Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyo Azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyo Haiyo Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyo Haiyo Azhagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Bogan Villaavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bogan Villaavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Poga Sollaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Poga Sollaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bogan Villaavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bogan Villaavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Sollaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Sollaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Poga Sonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Poga Sonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Enna Aaguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enna Aaguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkendru Ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkendru Ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idam Ondru Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Ondru Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhilae Adhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhilae Adhilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Vaazha Nee Mattum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Vaazha Nee Mattum Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valluvanukku Vaasuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valluvanukku Vaasuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku Neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandhiranukku Suriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandhiranukku Suriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Paarvai Theeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paarvai Theeyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamanukku Seethaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamanukku Seethaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannanukku Raadhaiyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannanukku Raadhaiyina"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkendrum Nee Mattum Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkendrum Nee Mattum Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Devathaiyae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devathaiyae Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniya Naanum Ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniya Naanum Ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiya Neeyum Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiya Neeyum Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini En Vaazhkai Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini En Vaazhkai Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Arugil Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Arugil Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Bogan Villaavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bogan Villaavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Poga Sollaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Poga Sollaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bogan Villaavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bogan Villaavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Sollaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Sollaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Poga Sonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Poga Sonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Enna Aaguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enna Aaguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Bogan Villaavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bogan Villaavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Poga Sollaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Poga Sollaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bogan Villaavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bogan Villaavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Sollaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Sollaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Poga Sonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Poga Sonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Enna Aaguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enna Aaguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkendru Ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkendru Ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idam Ondru Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Ondru Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhilae Adhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhilae Adhilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Vaazha Nee Mattum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Vaazha Nee Mattum Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjikulla Ottu Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikulla Ottu Vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mugatha Haiyo Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugatha Haiyo Azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjikulla Nattu Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjikulla Nattu Vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mugatha Haiyo Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugatha Haiyo Azhagae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu Mudichi Poda Naan Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu Mudichi Poda Naan Ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vaazhkka Muzhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vaazhkka Muzhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathupendi Kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathupendi Kaadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Moonu Mudichi Poda Naan Ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Moonu Mudichi Poda Naan Ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Vaazhkka Muzhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Vaazhkka Muzhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathupendi Kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathupendi Kaadhali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Irundhaalum Illanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Irundhaalum Illanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">I Love You My Kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Love You My Kaadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Marandhu Enna Therila Naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Marandhu Enna Therila Naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">I Will Love You My Kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Will Love You My Kaadhali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Bogan Villaavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bogan Villaavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Poga Sollaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Poga Sollaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bogan Villaavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bogan Villaavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Sollaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Sollaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Poga Sonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Poga Sonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Enna Aaguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enna Aaguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Bogan Villaavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bogan Villaavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Poga Sollaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Poga Sollaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Bogan Villaavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Bogan Villaavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Sollaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Sollaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Poga Sonnaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Poga Sonnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Enna Aaguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enna Aaguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagae Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae Azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkendru Ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkendru Ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idam Ondru Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam Ondru Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhilae Adhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhilae Adhilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu Vaazha Nee Mattum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Vaazha Nee Mattum Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haiyo Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyo Azhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyo Haiyo Azhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyo Haiyo Azhagae"/>
</div>
</pre>
