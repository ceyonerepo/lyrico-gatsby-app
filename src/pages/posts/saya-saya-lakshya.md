---
title: "saya saya song lyrics"
album: "Lakshya"
artist: "Kaala Bhairava"
lyricist: "Krishna Kanth"
director: "Dheerendra Santhossh"
path: "/albums/lakshya-lyrics"
song: "Saya Saya"
image: ../../images/albumart/lakshya.jpg
date: 2021-12-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/zzD7-jp8v88"
type: "love"
singers:
  - Junaid Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aakashame tale etthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakashame tale etthene"/>
</div>
<div class="lyrico-lyrics-wrapper">Na prema kolichendhuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na prema kolichendhuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Boogalame penche vegame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boogalame penche vegame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu nanne kalipendhuke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu nanne kalipendhuke"/>
</div>
<div class="lyrico-lyrics-wrapper">Saya saya saya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saya saya saya"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeshidhine aya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeshidhine aya"/>
</div>
<div class="lyrico-lyrics-wrapper">Diya nuvve diya aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diya nuvve diya aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chere naa kougile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chere naa kougile"/>
</div>
<div class="lyrico-lyrics-wrapper">Saya saya saya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saya saya saya"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeshidhine aya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeshidhine aya"/>
</div>
<div class="lyrico-lyrics-wrapper">Diya nuvve diya aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diya nuvve diya aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chere naa kougile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chere naa kougile"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam vinipinche 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam vinipinche "/>
</div>
<div class="lyrico-lyrics-wrapper">gaale kanipinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaale kanipinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaye nee vallane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaye nee vallane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam aapayina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam aapayina "/>
</div>
<div class="lyrico-lyrics-wrapper">kalise aapaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalise aapaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Veede ne vellane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veede ne vellane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prema neepaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema neepaina "/>
</div>
<div class="lyrico-lyrics-wrapper">enthentha undhante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthentha undhante"/>
</div>
<div class="lyrico-lyrics-wrapper">Gudine kadathanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gudine kadathanule"/>
</div>
<div class="lyrico-lyrics-wrapper">Janme enthaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janme enthaina "/>
</div>
<div class="lyrico-lyrics-wrapper">chaladu levamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chaladu levamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli pudathanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli pudathanule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maare naa ratha o 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maare naa ratha o "/>
</div>
<div class="lyrico-lyrics-wrapper">chooputhone needhele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chooputhone needhele"/>
</div>
<div class="lyrico-lyrics-wrapper">Kore vachhanu ninninka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kore vachhanu ninninka "/>
</div>
<div class="lyrico-lyrics-wrapper">nene polene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nene polene"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye aataina baataina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye aataina baataina "/>
</div>
<div class="lyrico-lyrics-wrapper">vadhile raana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vadhile raana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne padatha nee venakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne padatha nee venakale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu pommanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu pommanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye nee vente vasthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye nee vente vasthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Alusayyana em paravaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alusayyana em paravaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruventi mari nee kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruventi mari nee kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Saya saya saya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saya saya saya"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeshidhine aya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeshidhine aya"/>
</div>
<div class="lyrico-lyrics-wrapper">Diya nuvve diya aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Diya nuvve diya aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chere naa kougile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chere naa kougile"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam vinipinche 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounam vinipinche "/>
</div>
<div class="lyrico-lyrics-wrapper">gaale kanipinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaale kanipinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaye nee vallane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaye nee vallane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam aapayina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam aapayina "/>
</div>
<div class="lyrico-lyrics-wrapper">kalise aapaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalise aapaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Veede ne vellane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veede ne vellane"/>
</div>
</pre>
