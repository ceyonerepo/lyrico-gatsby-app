---
title: "bandar baant song lyrics"
album: "Sherni"
artist: "Bandish Projekt"
lyricist: "Hussain Haidry"
director: "Amit V. Masurkar"
path: "/albums/sherni-lyrics"
song: "Bandar Baant"
image: ../../images/albumart/sherni.jpg
date: 2021-06-18
lang: hindi
youtubeLink: "https://www.youtube.com/embed/K1ybqVxat1s"
type: "happy"
singers:
  - Aishwarya Joshi
  - Roshan Khan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">rakhi jo roti bich bazar ladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rakhi jo roti bich bazar ladi"/>
</div>
<div class="lyrico-lyrics-wrapper">do billiyaan panje maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="do billiyaan panje maar"/>
</div>
<div class="lyrico-lyrics-wrapper">to aaya bandar ek hoshiyaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="to aaya bandar ek hoshiyaar"/>
</div>
<div class="lyrico-lyrics-wrapper">woh bola kyun ladte ho yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="woh bola kyun ladte ho yaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yeah jagda mein mitaunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeah jagda mein mitaunga"/>
</div>
<div class="lyrico-lyrics-wrapper">has jaunga rotiyaa bantkar ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="has jaunga rotiyaa bantkar ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">billiyaan dekhti hi reh gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="billiyaan dekhti hi reh gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">woh din dahade chal gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="woh din dahade chal gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">baat yeah chutkala hi ban gayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baat yeah chutkala hi ban gayi"/>
</div>
<div class="lyrico-lyrics-wrapper">kha gaya roti unki bandar..haye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kha gaya roti unki bandar..haye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hit kisi ki mat kisi ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hit kisi ki mat kisi ki"/>
</div>
<div class="lyrico-lyrics-wrapper">or kii ka roda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="or kii ka roda"/>
</div>
<div class="lyrico-lyrics-wrapper">bhanumati ne naam se apne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhanumati ne naam se apne"/>
</div>
<div class="lyrico-lyrics-wrapper">aakar kunva joda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aakar kunva joda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bandar baat ka khela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bandar baat ka khela"/>
</div>
<div class="lyrico-lyrics-wrapper">khele mein hai jo mele bade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khele mein hai jo mele bade"/>
</div>
<div class="lyrico-lyrics-wrapper">kuth nikal jaaye saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuth nikal jaaye saari"/>
</div>
<div class="lyrico-lyrics-wrapper">lene ke phie dene pade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lene ke phie dene pade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bandar baat ka khela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bandar baat ka khela"/>
</div>
<div class="lyrico-lyrics-wrapper">khele mein hai jo mele bade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khele mein hai jo mele bade"/>
</div>
<div class="lyrico-lyrics-wrapper">nind me hove hajamat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nind me hove hajamat"/>
</div>
<div class="lyrico-lyrics-wrapper">jaage to phir tote ude
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaage to phir tote ude"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chali gayi billiya bich bazar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chali gayi billiya bich bazar"/>
</div>
<div class="lyrico-lyrics-wrapper">boli hua hai atyachaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boli hua hai atyachaar"/>
</div>
<div class="lyrico-lyrics-wrapper">lagayi jangal mein jo guhar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lagayi jangal mein jo guhar"/>
</div>
<div class="lyrico-lyrics-wrapper">to sab ke alag alag the vichar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="to sab ke alag alag the vichar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bhed boli ye to hoi bhole bhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhed boli ye to hoi bhole bhal"/>
</div>
<div class="lyrico-lyrics-wrapper">ko khayi jaate hai sab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ko khayi jaate hai sab"/>
</div>
<div class="lyrico-lyrics-wrapper">bhalu bola koi baat nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhalu bola koi baat nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">chad ke ped pe sehad kha lo ab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chad ke ped pe sehad kha lo ab"/>
</div>
</pre>
