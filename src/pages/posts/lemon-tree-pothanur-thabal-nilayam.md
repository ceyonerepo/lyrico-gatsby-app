---
title: "lemon tree song lyrics"
album: "Pothanur Thabal Nilayam"
artist: "Tenma"
lyricist: "Shamanth Nag"
director: "Praveen"
path: "/albums/pothanur-thabal-nilayam-lyrics"
song: "Lemon Tree"
image: ../../images/albumart/pothanur-thabal-nilayam.jpg
date: 2022-05-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/E0EkgG-fMlk"
type: "love"
singers:
  - Shamanth Nag
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">life is like a 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life is like a "/>
</div>
<div class="lyrico-lyrics-wrapper">lemon tree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lemon tree"/>
</div>
<div class="lyrico-lyrics-wrapper">our life is 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="our life is "/>
</div>
<div class="lyrico-lyrics-wrapper">like a lemon tree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="like a lemon tree"/>
</div>
<div class="lyrico-lyrics-wrapper">im sure it 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="im sure it "/>
</div>
<div class="lyrico-lyrics-wrapper">has some prikcy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="has some prikcy"/>
</div>
<div class="lyrico-lyrics-wrapper">thorns its 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thorns its "/>
</div>
<div class="lyrico-lyrics-wrapper">gonna stick along
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gonna stick along"/>
</div>
<div class="lyrico-lyrics-wrapper">its you try 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="its you try "/>
</div>
<div class="lyrico-lyrics-wrapper">to climb and see
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="to climb and see"/>
</div>
<div class="lyrico-lyrics-wrapper">just go and buy some 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="just go and buy some "/>
</div>
<div class="lyrico-lyrics-wrapper">lemons of the street
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lemons of the street"/>
</div>
<div class="lyrico-lyrics-wrapper">just go and buy some 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="just go and buy some "/>
</div>
<div class="lyrico-lyrics-wrapper">lemons of the street
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lemons of the street"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">we all gonna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we all gonna "/>
</div>
<div class="lyrico-lyrics-wrapper">die some day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="die some day"/>
</div>
<div class="lyrico-lyrics-wrapper">we all gonna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we all gonna "/>
</div>
<div class="lyrico-lyrics-wrapper">die some day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="die some day"/>
</div>
<div class="lyrico-lyrics-wrapper">it can be afraid 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="it can be afraid "/>
</div>
<div class="lyrico-lyrics-wrapper">to go out to night
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="to go out to night"/>
</div>
<div class="lyrico-lyrics-wrapper">catch fresh air and 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="catch fresh air and "/>
</div>
<div class="lyrico-lyrics-wrapper">some love 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="some love "/>
</div>
<div class="lyrico-lyrics-wrapper">just go and take a 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="just go and take a "/>
</div>
<div class="lyrico-lyrics-wrapper">stroll on the street
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="stroll on the street"/>
</div>
<div class="lyrico-lyrics-wrapper">just go and take a 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="just go and take a "/>
</div>
<div class="lyrico-lyrics-wrapper">stroll on the street
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="stroll on the street"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pain the merries 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pain the merries "/>
</div>
<div class="lyrico-lyrics-wrapper">of the life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="of the life"/>
</div>
<div class="lyrico-lyrics-wrapper">lets melt the 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lets melt the "/>
</div>
<div class="lyrico-lyrics-wrapper">merries of the life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="merries of the life"/>
</div>
<div class="lyrico-lyrics-wrapper">if we miss to 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="if we miss to "/>
</div>
<div class="lyrico-lyrics-wrapper">love and forget to dance
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love and forget to dance"/>
</div>
<div class="lyrico-lyrics-wrapper">and what a 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="and what a "/>
</div>
<div class="lyrico-lyrics-wrapper">waste of our life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="waste of our life"/>
</div>
<div class="lyrico-lyrics-wrapper">just gonna drink and
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="just gonna drink and"/>
</div>
<div class="lyrico-lyrics-wrapper">dance on the street
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dance on the street"/>
</div>
<div class="lyrico-lyrics-wrapper">just gonna drink and
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="just gonna drink and"/>
</div>
<div class="lyrico-lyrics-wrapper">dance on the street
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dance on the street"/>
</div>
<div class="lyrico-lyrics-wrapper">just gonna drink and
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="just gonna drink and"/>
</div>
<div class="lyrico-lyrics-wrapper">dance on the street
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dance on the street"/>
</div>
<div class="lyrico-lyrics-wrapper">just gonna drink and
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="just gonna drink and"/>
</div>
<div class="lyrico-lyrics-wrapper">dance on the street
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dance on the street"/>
</div>
<div class="lyrico-lyrics-wrapper">just gonna drink and
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="just gonna drink and"/>
</div>
<div class="lyrico-lyrics-wrapper">dance on the street
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dance on the street"/>
</div>
</pre>
