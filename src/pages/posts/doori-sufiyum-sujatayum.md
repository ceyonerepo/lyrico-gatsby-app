---
title: "doori song lyrics"
album: "Sufiyum Sujatayum"
artist: "M. Jayachandran"
lyricist: "Manoj Yadav"
director: "Naranipuzha Shanavas"
path: "/albums/sufiyum-sujatayum-lyrics"
song: "Doori"
image: ../../images/albumart/sufiyum-sujatayum.jpg
date: 2020-07-03
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/4oNecViFPfk"
type: "melody"
singers:
  - Madhuvanthi Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Doori Teri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doori Teri "/>
</div>
<div class="lyrico-lyrics-wrapper">Dooru bohath hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooru bohath hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mann mera bajj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann mera bajj"/>
</div>
<div class="lyrico-lyrics-wrapper">Dooru bohath hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dooru bohath hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intzar bhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intzar bhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Har gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har gaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabhi kami Teri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabhi kami Teri "/>
</div>
<div class="lyrico-lyrics-wrapper">Kami bohath hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kami bohath hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaane hi mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaane hi mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaya meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaya meri jaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mein kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mein kya karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Mein kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mein kya karoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Who mejudhayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Who mejudhayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil hai sah raha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil hai sah raha"/>
</div>
<div class="lyrico-lyrics-wrapper">ishq ishq mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ishq ishq mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq beh raha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq beh raha"/>
</div>
<div class="lyrico-lyrics-wrapper">bann gayi hu mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bann gayi hu mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Gam ka galiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gam ka galiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janne hi mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janne hi mera"/>
</div>
<div class="lyrico-lyrics-wrapper">le gayi meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="le gayi meri jaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mein kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mein kya karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karoon"/>
</div>
<div class="lyrico-lyrics-wrapper">Mein kya karoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mein kya karoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meri jaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri jaan"/>
</div>
</pre>
