---
title: "alavaate ledhemo song lyrics"
album: "Bhanumathi & Ramakrishna"
artist: "Shravan Bharadwaj"
lyricist: "Krishna Kanth"
director: "Srikanth Nagothi"
path: "/albums/bhanumathi-and-ramakrishna-lyrics"
song: "Alavaate Ledhemo"
image: ../../images/albumart/bhanumathi-and-ramakrishna.jpg
date: 2020-07-03
lang: telugu
youtubeLink: "https://www.youtube.com/embed/vhFhV0h4x20"
type: "love"
singers:
  - Lalitha Kavya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Alavaate Ledhemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavaate Ledhemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadabaate Ee Dhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadabaate Ee Dhaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasemo Thoseene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasemo Thoseene"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhamantu Oo Saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhamantu Oo Saari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adugulu Paduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulu Paduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharikia Neduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharikia Neduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyani Thikamaka Paduthundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyani Thikamaka Paduthundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Alisina Vayase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alisina Vayase"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari Ani Chebithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Ani Chebithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyaka Kalavara Paduthundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyaka Kalavara Paduthundhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prema Prema Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Prema Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Padamu Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padamu Kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vere Pere Edhi Thaganidhaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vere Pere Edhi Thaganidhaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Konchem Chodhyam Aina Nilavukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konchem Chodhyam Aina Nilavukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Vela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Vela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema Prema Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema Prema Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Padamu Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padamu Kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vere Pere Edhi Thaganidhaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vere Pere Edhi Thaganidhaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Konchem Chodhyam Aina Nilavukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konchem Chodhyam Aina Nilavukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Vela…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Vela…"/>
</div>
<div class="lyrico-lyrics-wrapper">Choose Kalle Thelapanidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choose Kalle Thelapanidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Maate Neetho Kalapanidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate Neetho Kalapanidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadavadhu Le Samyamidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadavadhu Le Samyamidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Godavidhe Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godavidhe Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Lo Lo Unna Theliyanidhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lo Lo Unna Theliyanidhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppalanna Mudi Padadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppalanna Mudi Padadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaguvutho Nee Kathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaguvutho Nee Kathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhuputhondhi Padhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhuputhondhi Padhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alavaate Ledhemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavaate Ledhemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadabaate Ee Dhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadabaate Ee Dhaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasemo Thoseene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasemo Thoseene"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhamantu Oo Saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhamantu Oo Saari"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulu Paduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulu Paduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharikia Neduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharikia Neduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyani Thikamaka Paduthundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyani Thikamaka Paduthundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Alisina Vayase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alisina Vayase"/>
</div>
<div class="lyrico-lyrics-wrapper">Sari Ani Chebithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari Ani Chebithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyaka Kalavara Paduthundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyaka Kalavara Paduthundhe"/>
</div>
</pre>
