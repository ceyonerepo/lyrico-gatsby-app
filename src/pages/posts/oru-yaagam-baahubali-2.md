---
title: "oru yaagam song lyrics"
album: "Baahubali 2"
artist: "M.M. Keeravani"
lyricist: "Madhan Karky"
director: "S.S. Rajamouli"
path: "/albums/baahubali-2-song-lyrics"
song: "Oru Yaagam"
image: ../../images/albumart/baahubali-2.jpg
date: 2017-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JypXCruC8-E"
type: "Mass"
singers:
  - Kaala Bhairava
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
 <div class="lyrico-lyrics-wrapper">oru yaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru yaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">oru thiyaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thiyaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhai ondroa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhai ondroa"/>
</div>
<div class="lyrico-lyrics-wrapper">aarambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aarambam"/>
</div>
<div class="lyrico-lyrics-wrapper">irumbendrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irumbendrae"/>
</div>
<div class="lyrico-lyrics-wrapper">mana thinmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana thinmam"/>
</div>
<div class="lyrico-lyrics-wrapper">nerupendrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerupendrae"/>
</div>
<div class="lyrico-lyrics-wrapper">adhil vanmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhil vanmam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maranam ondril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranam ondril"/>
</div>
<div class="lyrico-lyrics-wrapper">pirakkum aruvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirakkum aruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">maranamdhaan kudikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranamdhaan kudikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">avvaanamoa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avvaanamoa"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhthi idikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhthi idikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa mannavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa mannavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaa vaa mannavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa vaa mannavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">manellaam paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manellaam paadum"/>
</div>
<div class="lyrico-lyrics-wrapper">un paadhathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paadhathai"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri thaedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri thaedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pazhitaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhitaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">uli vaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uli vaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">padaipaanoa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padaipaanoa"/>
</div>
<div class="lyrico-lyrics-wrapper">edhirkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhirkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">udhirathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udhirathil"/>
</div>
<div class="lyrico-lyrics-wrapper">sinamoadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinamoadum"/>
</div>
<div class="lyrico-lyrics-wrapper">thuli yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuli yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">sivam... sivam...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sivam... sivam..."/>
</div>
</pre>
