---
title: "dabbunte duniya song lyrics"
album: "Dandupalyam 4"
artist: "Anand Rajavikram"
lyricist: "Bhuvanachandra"
director: "KT Nayak"
path: "/albums/dandupalyam-4-lyrics"
song: "Dabbunte Duniya"
image: ../../images/albumart/dandupalyam-4.jpg
date: 2019-11-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/8InMOWzaN3g"
type: "happy"
singers:
  - Vyasraj
  - Indu Nagaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">dabbu dabbu dabbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbu dabbu dabbu"/>
</div>
<div class="lyrico-lyrics-wrapper">untene dhuniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="untene dhuniya"/>
</div>
<div class="lyrico-lyrics-wrapper">cash cash cash
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cash cash cash"/>
</div>
<div class="lyrico-lyrics-wrapper">unnode bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnode bossu"/>
</div>
<div class="lyrico-lyrics-wrapper">masthuga dabbunnode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="masthuga dabbunnode"/>
</div>
<div class="lyrico-lyrics-wrapper">lokamlo king
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokamlo king"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dabbu dabbu dabbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbu dabbu dabbu"/>
</div>
<div class="lyrico-lyrics-wrapper">untene dhuniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="untene dhuniya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dabbu dabbu dabbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbu dabbu dabbu"/>
</div>
<div class="lyrico-lyrics-wrapper">untene dhuniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="untene dhuniya"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu kasu kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu kasu kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">unnode bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnode bossu"/>
</div>
<div class="lyrico-lyrics-wrapper">dabbu kasu note
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbu kasu note"/>
</div>
<div class="lyrico-lyrics-wrapper">unnolle queen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnolle queen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dabbu dabbu dabbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbu dabbu dabbu"/>
</div>
<div class="lyrico-lyrics-wrapper">untene dhuniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="untene dhuniya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edhaina cheyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhaina cheyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye gaadaina meyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye gaadaina meyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">jebuninda jaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jebuninda jaani"/>
</div>
<div class="lyrico-lyrics-wrapper">nottu nimpi veyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nottu nimpi veyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">thala lanna thiyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala lanna thiyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">thala needhe moyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala needhe moyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru palla kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru palla kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">dabbu dhaachi pettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbu dhaachi pettu"/>
</div>
<div class="lyrico-lyrics-wrapper">edhaina cheyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhaina cheyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">jebuninda jaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jebuninda jaani"/>
</div>
<div class="lyrico-lyrics-wrapper">thala lanna thiyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala lanna thiyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru palla kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru palla kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">jagathey dabbulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jagathey dabbulo"/>
</div>
<div class="lyrico-lyrics-wrapper">undenoy 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undenoy "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dabbu dabbu dabbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbu dabbu dabbu"/>
</div>
<div class="lyrico-lyrics-wrapper">untene dhuniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="untene dhuniya"/>
</div>
<div class="lyrico-lyrics-wrapper">cash cash cash
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cash cash cash"/>
</div>
<div class="lyrico-lyrics-wrapper">unnode bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnode bossu"/>
</div>
<div class="lyrico-lyrics-wrapper">masthuga dabbunnode
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="masthuga dabbunnode"/>
</div>
<div class="lyrico-lyrics-wrapper">lokamlo king
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokamlo king"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dabbu dabbu dabbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbu dabbu dabbu"/>
</div>
<div class="lyrico-lyrics-wrapper">untene dhuniya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="untene dhuniya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naana circus
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naana circus"/>
</div>
<div class="lyrico-lyrics-wrapper">ennenno games
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennenno games"/>
</div>
<div class="lyrico-lyrics-wrapper">chusthunnna crimes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusthunnna crimes"/>
</div>
<div class="lyrico-lyrics-wrapper">paisal ke paisal ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paisal ke paisal ke"/>
</div>
<div class="lyrico-lyrics-wrapper">naana circus
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naana circus"/>
</div>
<div class="lyrico-lyrics-wrapper">ennenno games
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennenno games"/>
</div>
<div class="lyrico-lyrics-wrapper">chusthunnna crimes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusthunnna crimes"/>
</div>
<div class="lyrico-lyrics-wrapper">paisal ke paisal ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paisal ke paisal ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mosam cheseti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mosam cheseti"/>
</div>
<div class="lyrico-lyrics-wrapper">manasula nammedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula nammedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">pasuvai poyedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasuvai poyedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">dabulake dabbulake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabulake dabbulake"/>
</div>
<div class="lyrico-lyrics-wrapper">mosam cheseti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mosam cheseti"/>
</div>
<div class="lyrico-lyrics-wrapper">manasula nammedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasula nammedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">pasuvai poyedhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasuvai poyedhi"/>
</div>
<div class="lyrico-lyrics-wrapper">dabulake dabbulake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabulake dabbulake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paapa punyalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paapa punyalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">pedhalakele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedhalakele"/>
</div>
<div class="lyrico-lyrics-wrapper">nyaya neethi sales
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nyaya neethi sales"/>
</div>
<div class="lyrico-lyrics-wrapper">dhanavanthulake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhanavanthulake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jebu ninda unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jebu ninda unte"/>
</div>
<div class="lyrico-lyrics-wrapper">swargam needhele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swargam needhele"/>
</div>
<div class="lyrico-lyrics-wrapper">cheyyi kaali aithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheyyi kaali aithe"/>
</div>
<div class="lyrico-lyrics-wrapper">kuka brathukele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuka brathukele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edhaina cheyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhaina cheyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye gaadaina meyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye gaadaina meyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">jebuninda jaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jebuninda jaani"/>
</div>
<div class="lyrico-lyrics-wrapper">nottu nimpi veyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nottu nimpi veyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">thala lanna thiyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala lanna thiyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">thala needhe moyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala needhe moyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru palla kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru palla kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">dabbu dhaachi pettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbu dhaachi pettu"/>
</div>
<div class="lyrico-lyrics-wrapper">jagathey dabbulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jagathey dabbulo"/>
</div>
<div class="lyrico-lyrics-wrapper">undenoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undenoy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">raajalu avutharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raajalu avutharu"/>
</div>
<div class="lyrico-lyrics-wrapper">peddollu avutharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peddollu avutharu"/>
</div>
<div class="lyrico-lyrics-wrapper">devullu avutharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devullu avutharu"/>
</div>
<div class="lyrico-lyrics-wrapper">paisal ke paisal ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paisal ke paisal ke"/>
</div>
<div class="lyrico-lyrics-wrapper">raajalu avutharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raajalu avutharu"/>
</div>
<div class="lyrico-lyrics-wrapper">peddollu avutharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peddollu avutharu"/>
</div>
<div class="lyrico-lyrics-wrapper">devullu avutharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devullu avutharu"/>
</div>
<div class="lyrico-lyrics-wrapper">paisal ke paisal ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paisal ke paisal ke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ille bangaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ille bangaram"/>
</div>
<div class="lyrico-lyrics-wrapper">olle singaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olle singaram"/>
</div>
<div class="lyrico-lyrics-wrapper">kadupe kashmiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadupe kashmiram"/>
</div>
<div class="lyrico-lyrics-wrapper">dabbunte dabbunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbunte dabbunte"/>
</div>
<div class="lyrico-lyrics-wrapper">ille bangaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ille bangaram"/>
</div>
<div class="lyrico-lyrics-wrapper">olle singaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olle singaram"/>
</div>
<div class="lyrico-lyrics-wrapper">kadupe kashmiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadupe kashmiram"/>
</div>
<div class="lyrico-lyrics-wrapper">dabbunte dabbunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbunte dabbunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bandhu mitrulantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bandhu mitrulantha"/>
</div>
<div class="lyrico-lyrics-wrapper">dabbu kosame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbu kosame"/>
</div>
<div class="lyrico-lyrics-wrapper">bharya biddalaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bharya biddalaina"/>
</div>
<div class="lyrico-lyrics-wrapper">dabbu kosame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbu kosame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">prema geemalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prema geemalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">nee dabbu medhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dabbu medhane"/>
</div>
<div class="lyrico-lyrics-wrapper">noti koti unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noti koti unte"/>
</div>
<div class="lyrico-lyrics-wrapper">kothaniana okey ne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kothaniana okey ne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">edhaina cheyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhaina cheyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">edhaina cheyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhaina cheyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye gaadaina meyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye gaadaina meyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">jebuninda jaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jebuninda jaani"/>
</div>
<div class="lyrico-lyrics-wrapper">nottu nimpi veyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nottu nimpi veyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">thala lanna thiyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala lanna thiyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">thala needhe moyyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala needhe moyyi"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru palla kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru palla kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">dabbu dhaachi pettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbu dhaachi pettu"/>
</div>
<div class="lyrico-lyrics-wrapper">jagathey dabbulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jagathey dabbulo"/>
</div>
<div class="lyrico-lyrics-wrapper">undenoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undenoy"/>
</div>
</pre>
