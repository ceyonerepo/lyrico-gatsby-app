---
title: "avuna nijamena song lyrics"
album: "Evvurikee Cheppoddu"
artist: "Sankar Sharma"
lyricist: "Vasu Valaboju"
director: "Basava Shanker"
path: "/albums/evvurikee-cheppoddu-lyrics"
song: "Avuna Nijamena"
image: ../../images/albumart/evvurikee-cheppoddu.jpg
date: 2019-10-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/gufI6p3eO54"
type: "love"
singers:
  - KS Harisankar
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Avuna Nijamenaa Nijamenaa Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avuna Nijamenaa Nijamenaa Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avuna Nijamenaa Idi Nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avuna Nijamenaa Idi Nijamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo Jarigindaa Mari Entee Varasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Jarigindaa Mari Entee Varasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koncham Mari Koncham Nanu Marichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koncham Mari Koncham Nanu Marichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Koncham Kalagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Koncham Kalagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Koncham Nijamugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Koncham Nijamugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anipinche Nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anipinche Nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Maate Vinavugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Maate Vinavugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiru Navvai Merisave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiru Navvai Merisave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Manasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avuna Nijamenaa Nijamenaa Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avuna Nijamenaa Nijamenaa Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avuna Nijamenaa Idi Nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avuna Nijamenaa Idi Nijamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo Jarigindaa Mari Entee Varasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Jarigindaa Mari Entee Varasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koncham Mari Koncham Nanu Marichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koncham Mari Koncham Nanu Marichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na maata Koncham Vine Paney Leda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na maata Koncham Vine Paney Leda"/>
</div>
<div class="lyrico-lyrics-wrapper">Giridaati Velataave Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Giridaati Velataave Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maree Alaa Daanni Anichesi Unche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maree Alaa Daanni Anichesi Unche"/>
</div>
<div class="lyrico-lyrics-wrapper">Talapaina Pora Paate Telusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talapaina Pora Paate Telusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Choopu Daati Mari Manaselli Poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Choopu Daati Mari Manaselli Poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeroju Thoti Tana Kadha Maari Poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeroju Thoti Tana Kadha Maari Poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Nammaleni Vintha Gundi Gaanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Nammaleni Vintha Gundi Gaanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapaleni Intha Santhoshaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapaleni Intha Santhoshaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkasari Pondamanna Gani Suluva Suluva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkasari Pondamanna Gani Suluva Suluva"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Choopuloni Chinna Samsayaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Choopuloni Chinna Samsayaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Navvu Nettivesthe Gaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Navvu Nettivesthe Gaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Taakalevu Nuvvu Ambaranni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taakalevu Nuvvu Ambaranni"/>
</div>
<div class="lyrico-lyrics-wrapper">Telusaa Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telusaa Manasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oohalapaina Adugesinene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohalapaina Adugesinene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadichale Tolisarainaa Bagunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadichale Tolisarainaa Bagunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasalu Daati Parugandukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasalu Daati Parugandukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Kanna Kala Neeku Saripodhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Kanna Kala Neeku Saripodhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Innallu Lokam Mari Nanu Choodaledaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innallu Lokam Mari Nanu Choodaledaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Cheyi Chaaste Mari Tanu Andaledaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Cheyi Chaaste Mari Tanu Andaledaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Kothha Marpu Kothagundi Gani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Kothha Marpu Kothagundi Gani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta daari Chooputondi Gani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta daari Chooputondi Gani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotta Bandhamintha Deggaraite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotta Bandhamintha Deggaraite"/>
</div>
<div class="lyrico-lyrics-wrapper">Godave Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Godave Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Tappaledu Tappu Kaadu Gani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Tappaledu Tappu Kaadu Gani"/>
</div>
<div class="lyrico-lyrics-wrapper">Muchhatega Muppuledu Gani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muchhatega Muppuledu Gani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Neeku Chooputaanu Gani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Neeku Chooputaanu Gani"/>
</div>
<div class="lyrico-lyrics-wrapper">Padave Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padave Manasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avuna Nijamenaa Nijamenaa Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avuna Nijamenaa Nijamenaa Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avuna Nijamenaa Idi Nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avuna Nijamenaa Idi Nijamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo Jarigindaa Mari Entee Varasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Jarigindaa Mari Entee Varasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koncham Mari Koncham Nanu Marichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koncham Mari Koncham Nanu Marichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi Koncham Kalagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi Koncham Kalagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Koncham Nijamugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Koncham Nijamugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anipinche Nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anipinche Nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Maate Vinavugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Maate Vinavugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiru Navvai Merisave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiru Navvai Merisave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari Manasaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avuna Nijamenaa Nijamenaa Manasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avuna Nijamenaa Nijamenaa Manasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avuna Nijamenaa Idi Nijamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avuna Nijamenaa Idi Nijamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo Jarigindaa Mari Entee Varasaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo Jarigindaa Mari Entee Varasaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koncham Mari Koncham Nanu Marichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koncham Mari Koncham Nanu Marichaa"/>
</div>
</pre>
