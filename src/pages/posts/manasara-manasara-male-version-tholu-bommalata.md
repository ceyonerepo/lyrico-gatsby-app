---
title: "manasara manasara male version song lyrics"
album: "Tholu Bommalata"
artist: "Suresh Bobbili"
lyricist: "Chaitanya Prasad"
director: "Viswanath Maganti"
path: "/albums/tholu-bommalata-lyrics"
song: "Manasara Manasara Male Version"
image: ../../images/albumart/tholu-bommalata.jpg
date: 2019-11-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/KuJ6pEusqzk"
type: "sad"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manasara Manasara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasara Manasara"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulu Verayye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulu Verayye"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadabato Porapaato
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadabato Porapaato"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedabatayyele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedabatayyele"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi Raayani Kadhalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi Raayani Kadhalona"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraham Migilele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraham Migilele"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavve Velipothu Palike Veedkole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavve Velipothu Palike Veedkole"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Praaname Nannodilese Velipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Praaname Nannodilese Velipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavedane Thodayyindhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavedane Thodayyindhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamannadhe O Mathileni Maatayye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamannadhe O Mathileni Maatayye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Nenuga Vidipoyindhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Nenuga Vidipoyindhile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Soonyame Ika Naa Nesthamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Soonyame Ika Naa Nesthamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhalo Koluvai Untundhi Jantai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalo Koluvai Untundhi Jantai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharahaasame Oka Parihaasamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharahaasame Oka Parihaasamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiriga Nene Nadiche Vinthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiriga Nene Nadiche Vinthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Guri Choosi Kodithe Thagilindhi Baanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guri Choosi Kodithe Thagilindhi Baanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhalona Gaayam Maneyna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhalona Gaayam Maneyna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Baadhatho Ney Bathakaali Kalakaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Baadhatho Ney Bathakaali Kalakaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaadhe Sukham Anukovalile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaadhe Sukham Anukovalile"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Praaname Nannodhilesei Vellipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Praaname Nannodhilesei Vellipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavedhane Thodayyindhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavedhane Thodayyindhile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Oopire Penu Sudigaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Oopire Penu Sudigaalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasiga Dhooram Thosindhi Ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasiga Dhooram Thosindhi Ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Oohale Nanu Oopeyagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Oohale Nanu Oopeyagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Picche Patti Thiriga Nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Picche Patti Thiriga Nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvu Raaku Annaa Kanneeru Vinadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Raaku Annaa Kanneeru Vinadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalanni Karigi Kurisele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalanni Karigi Kurisele"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sokhame Naa Lokanga Marindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sokhame Naa Lokanga Marindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Aashane Masi Chesindhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Aashane Masi Chesindhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Praaname Nannodilese Velipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Praaname Nannodilese Velipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavedane Thodayyindhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavedane Thodayyindhile"/>
</div>
</pre>
