---
title: "thavilu thangam song lyrics"
album: "Melnaattu Marumagan"
artist: "V. Kishorkumar"
lyricist: "Akkatti Arumugam"
director: "MSS"
path: "/albums/melnaattu-marumagan-lyrics"
song: "Thavilu Thangam"
image: ../../images/albumart/melnaattu-marumagan.jpg
date: 2018-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EHA1Hdr8YG4"
type: "happy"
singers:
  - Akkatti Arumugam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nayana chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayana chellam"/>
</div>
<div class="lyrico-lyrics-wrapper">pamba raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pamba raasa"/>
</div>
<div class="lyrico-lyrics-wrapper">urumi machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urumi machan"/>
</div>
<div class="lyrico-lyrics-wrapper">hey machan machan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey machan machan "/>
</div>
<div class="lyrico-lyrics-wrapper">machan machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machan machan"/>
</div>
<div class="lyrico-lyrics-wrapper">tappa kunthu kumma thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tappa kunthu kumma thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">takkarana ponnugala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takkarana ponnugala"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli vaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli vaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">tappa kunthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tappa kunthu "/>
</div>
<div class="lyrico-lyrics-wrapper">kumma thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumma thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">takkarana ponnugala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takkarana ponnugala"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli vaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli vaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puthukottai thandi vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthukottai thandi vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">trichy thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="trichy thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">puthukottai thandi vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthukottai thandi vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">trichy thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="trichy thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">puthusa vanthu nikuthu par
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthusa vanthu nikuthu par"/>
</div>
<div class="lyrico-lyrics-wrapper">foreign figure da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="foreign figure da"/>
</div>
<div class="lyrico-lyrics-wrapper">puthusa vanthu nikuthu par
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthusa vanthu nikuthu par"/>
</div>
<div class="lyrico-lyrics-wrapper">foreign figure da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="foreign figure da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey tappa kuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey tappa kuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">tappa kunthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tappa kunthu "/>
</div>
<div class="lyrico-lyrics-wrapper">kumma thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumma thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">takkarana ponnugala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takkarana ponnugala"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli vaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli vaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey tappa kuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey tappa kuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">tappa kunthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tappa kunthu "/>
</div>
<div class="lyrico-lyrics-wrapper">kumma thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumma thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">takkarana ponnugala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takkarana ponnugala"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli vaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli vaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">komboothi kottu aduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komboothi kottu aduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">kummi adikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kummi adikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">komboothi kottu aduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komboothi kottu aduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">kummi adikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kummi adikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">kumarigala thedi manja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumarigala thedi manja"/>
</div>
<div class="lyrico-lyrics-wrapper">thanni oothanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanni oothanum"/>
</div>
<div class="lyrico-lyrics-wrapper">komboothi kottu aduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komboothi kottu aduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">kummi adikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kummi adikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">kumarigala thedi manja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumarigala thedi manja"/>
</div>
<div class="lyrico-lyrics-wrapper">thanni oothanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanni oothanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sittada katti silambu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sittada katti silambu "/>
</div>
<div class="lyrico-lyrics-wrapper">aattam aadanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aattam aadanum"/>
</div>
<div class="lyrico-lyrics-wrapper">sittada katti silambu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sittada katti silambu "/>
</div>
<div class="lyrico-lyrics-wrapper">aattam aadanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aattam aadanum"/>
</div>
<div class="lyrico-lyrics-wrapper">sithira samba nellu kuthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithira samba nellu kuthi"/>
</div>
<div class="lyrico-lyrics-wrapper">pongal vaikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pongal vaikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">sithira samba sithira samba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithira samba sithira samba"/>
</div>
<div class="lyrico-lyrics-wrapper">sithira samba nellu kuthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sithira samba nellu kuthi"/>
</div>
<div class="lyrico-lyrics-wrapper">pongal vaikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pongal vaikanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tappa kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tappa kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey tappa kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey tappa kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ah aha tappa kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ah aha tappa kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ama tappa kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ama tappa kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kumma thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumma thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">takarana ponnugala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takarana ponnugala"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli vaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli vaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">die aducha perusugala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="die aducha perusugala"/>
</div>
<div class="lyrico-lyrics-wrapper">othuki thalla da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othuki thalla da"/>
</div>
<div class="lyrico-lyrics-wrapper">die aducha perusugala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="die aducha perusugala"/>
</div>
<div class="lyrico-lyrics-wrapper">othuki thalla da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othuki thalla da"/>
</div>
<div class="lyrico-lyrics-wrapper">avan dianovin thangachiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan dianovin thangachiya"/>
</div>
<div class="lyrico-lyrics-wrapper">kettu sollu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kettu sollu da"/>
</div>
<div class="lyrico-lyrics-wrapper">die aducha perusugala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="die aducha perusugala"/>
</div>
<div class="lyrico-lyrics-wrapper">othuki thalla da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othuki thalla da"/>
</div>
<div class="lyrico-lyrics-wrapper">avan dianovin thangachiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avan dianovin thangachiya"/>
</div>
<div class="lyrico-lyrics-wrapper">kettu sollu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kettu sollu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pathinettu pattiyaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathinettu pattiyaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakural ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakural ada"/>
</div>
<div class="lyrico-lyrics-wrapper">pathinettu pattiyaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathinettu pattiyaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakural ada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakural ada"/>
</div>
<div class="lyrico-lyrics-wrapper">oora pathiram pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora pathiram pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">iva peril eluthi vaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva peril eluthi vaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">oora pathiram pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora pathiram pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">iva peril eluthi vaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva peril eluthi vaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tappa kuthu tappa kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tappa kuthu tappa kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">hey tappa kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey tappa kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kumma thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumma thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">takkarana ponnugala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takkarana ponnugala"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli vaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli vaiyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puthukottai thandi vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthukottai thandi vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">trichy thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="trichy thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">puthukottai thandi vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthukottai thandi vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">trichy thana da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="trichy thana da"/>
</div>
<div class="lyrico-lyrics-wrapper">puthusa vanthu nikuthu par
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthusa vanthu nikuthu par"/>
</div>
<div class="lyrico-lyrics-wrapper">foreign figure da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="foreign figure da"/>
</div>
<div class="lyrico-lyrics-wrapper">puthusa vanthu nikuthu par
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthusa vanthu nikuthu par"/>
</div>
<div class="lyrico-lyrics-wrapper">foreign figure da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="foreign figure da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tappa kuthu tappa kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tappa kuthu tappa kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ah aha tappa kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ah aha tappa kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kumma thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumma thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu paru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu paru da"/>
</div>
<div class="lyrico-lyrics-wrapper">takkarana ponnugala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takkarana ponnugala"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli vaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli vaiyada"/>
</div>
</pre>
