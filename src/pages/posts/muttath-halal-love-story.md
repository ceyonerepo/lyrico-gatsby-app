---
title: "muttath song lyrics"
album: "Halal Love Story"
artist: "Bijibal - Shahabaz Aman"
lyricist: "Anwar Ali"
director: "Zakariya Mohammed"
path: "/albums/halal-love-story-lyrics"
song: "Muttath"
image: ../../images/albumart/halal-love-story.jpg
date: 2020-10-15
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/oEqtkLcnr4g"
type: "happy"
singers:
  - Soumya Ramakrishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mmm...Mmmm... Mmm... Mmm..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm...Mmmm... Mmm... Mmm.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm...Mmmm... Mmm... Mmm..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm...Mmmm... Mmm... Mmm.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttath Annaadhyamaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttath Annaadhyamaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulla Poothoru Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulla Poothoru Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanival Pennithalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanival Pennithalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthunnu Oraadhyaksharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthunnu Oraadhyaksharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sukandhamaay Shalabhamaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukandhamaay Shalabhamaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum Paarumpol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Paarumpol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmm...Mmmm... Mmm... Mmm..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm...Mmmm... Mmm... Mmm.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm...Mmmm... Mmm... Mmm..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm...Mmmm... Mmm... Mmm.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nokki Nilke Kannil Poothulanju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nokki Nilke Kannil Poothulanju"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhyamaay Manam Vanampol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhyamaay Manam Vanampol"/>
</div>
<div class="lyrico-lyrics-wrapper">Orma Neeri Ullalinju Paadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orma Neeri Ullalinju Paadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhyamaay Karal Kuyil Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhyamaay Karal Kuyil Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudu Veyilude Thodalukal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudu Veyilude Thodalukal"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheru Cheru Chila Nanavukal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheru Cheru Chila Nanavukal"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarum Kaana Kaattin Thekkangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarum Kaana Kaattin Thekkangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmm...Mmmm... Mmm... Mmm..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm...Mmmm... Mmm... Mmm.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raavaakum Raav Thorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavaakum Raav Thorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulla Poothoru Naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulla Poothoru Naal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarakal Than Thaarithalaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarakal Than Thaarithalaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ida Thoornnoru Udhyaanamaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ida Thoornnoru Udhyaanamaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Prapanchathin Prabhathamaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prapanchathin Prabhathamaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Premam Maarumpol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premam Maarumpol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmm...Mmmm... Mmm... Mmm..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm...Mmmm... Mmm... Mmm.."/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm...Mmmm... Mmm... Mmm..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm...Mmmm... Mmm... Mmm.."/>
</div>
</pre>
