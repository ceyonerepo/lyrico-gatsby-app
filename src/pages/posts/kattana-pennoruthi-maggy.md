---
title: "kattana pennoruthi song lyrics"
album: "Maggy"
artist: "UM Steven Sathish"
lyricist: "Kalaikumar"
director: "Kartikeyen"
path: "/albums/maggy-lyrics"
song: "Kattana Pennoruthi"
image: ../../images/albumart/maggy.jpg
date: 2019-11-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TW0YHXt2VMQ"
type: "happy"
singers:
  - U.M. Stevan Sathish
  - Nancy sylvia  
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kattana penoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattana penoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">kanchipuram pattu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanchipuram pattu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">kanoram vetka pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanoram vetka pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">vara penne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vara penne "/>
</div>
<div class="lyrico-lyrics-wrapper">muthana manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthana manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">mudinjivacha asaigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudinjivacha asaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">munthanai mudichala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthanai mudichala "/>
</div>
<div class="lyrico-lyrics-wrapper">thara penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thara penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">orakannala pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orakannala pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">sirikira onna patha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirikira onna patha"/>
</div>
<div class="lyrico-lyrics-wrapper">mapula mayanguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mapula mayanguran"/>
</div>
<div class="lyrico-lyrics-wrapper">osaillama manasukulla kuthikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osaillama manasukulla kuthikira"/>
</div>
<div class="lyrico-lyrics-wrapper">unna sernthey mapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna sernthey mapula"/>
</div>
<div class="lyrico-lyrics-wrapper">parakuran parakuranee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakuran parakuranee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kattana penoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattana penoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">kanchipuram pattu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanchipuram pattu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">kanoram vetka pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanoram vetka pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">vara penne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vara penne "/>
</div>
<div class="lyrico-lyrics-wrapper">muthana manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthana manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">mudinjivacha asaigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudinjivacha asaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">munthanai mudichala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthanai mudichala "/>
</div>
<div class="lyrico-lyrics-wrapper">thara penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thara penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maalai mathikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maalai mathikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">manamakkale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manamakkale "/>
</div>
<div class="lyrico-lyrics-wrapper">iru manasum mathikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru manasum mathikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">nalamagume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalamagume "/>
</div>
<div class="lyrico-lyrics-wrapper">kaalai maalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalai maalai"/>
</div>
<div class="lyrico-lyrics-wrapper">ena illamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena illamale"/>
</div>
<div class="lyrico-lyrics-wrapper">orukanavilum konjikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orukanavilum konjikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">sugamagumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugamagumey"/>
</div>
<div class="lyrico-lyrics-wrapper">verodu verum seratume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verodu verum seratume"/>
</div>
<div class="lyrico-lyrics-wrapper">verodu verum punaiyatume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="verodu verum punaiyatume"/>
</div>
<div class="lyrico-lyrics-wrapper">ondrai vaazhum anjil paravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ondrai vaazhum anjil paravai"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu sernthu vazha vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu sernthu vazha vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">nandri solla varthai indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nandri solla varthai indri"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhum vazhkaiyilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhum vazhkaiyilla "/>
</div>
<div class="lyrico-lyrics-wrapper">nandri thinam solla vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nandri thinam solla vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">orakannala pathu sirikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orakannala pathu sirikira"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu sernthey mapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu sernthey mapula"/>
</div>
<div class="lyrico-lyrics-wrapper">parakuran parakurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakuran parakurane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">rare rarare rare rararara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rare rarare rare rararara"/>
</div>
<div class="lyrico-lyrics-wrapper">rare rarararareyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rare rarararareyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">rare rarare rare rararara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rare rarare rare rararara"/>
</div>
<div class="lyrico-lyrics-wrapper">rare rarararareyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rare rarararareyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">i love you for mee too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you for mee too"/>
</div>
<div class="lyrico-lyrics-wrapper">i love you too true love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="i love you too true love you"/>
</div>
<div class="lyrico-lyrics-wrapper">and you are my true love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="and you are my true love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">penne uchi muthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penne uchi muthal"/>
</div>
<div class="lyrico-lyrics-wrapper">patham varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patham varai"/>
</div>
<div class="lyrico-lyrics-wrapper">adi indre un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi indre un"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagu koodiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagu koodiyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kannil vachirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannil vachirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kannalanai athai kande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannalanai athai kande"/>
</div>
<div class="lyrico-lyrics-wrapper">un manasu adiyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un manasu adiyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">oyatha vambai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyatha vambai "/>
</div>
<div class="lyrico-lyrics-wrapper">konjam kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">thiratha anbai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiratha anbai"/>
</div>
<div class="lyrico-lyrics-wrapper">naraya ootu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naraya ootu"/>
</div>
<div class="lyrico-lyrics-wrapper">kattilmeley vettiselai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattilmeley vettiselai"/>
</div>
<div class="lyrico-lyrics-wrapper">mtti mothi motcham kolum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mtti mothi motcham kolum"/>
</div>
<div class="lyrico-lyrics-wrapper">motti pottu vittu thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="motti pottu vittu thanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhum vazhkai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhum vazhkai "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu epothume vellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu epothume vellum"/>
</div>
<div class="lyrico-lyrics-wrapper">ora kannala pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ora kannala pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">sirikira onna parthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirikira onna parthey"/>
</div>
<div class="lyrico-lyrics-wrapper">mapula mayanguran 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mapula mayanguran "/>
</div>
<div class="lyrico-lyrics-wrapper">mayangurane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayangurane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kattana penoruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattana penoruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">kanchipuram pattu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanchipuram pattu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">kanoram vetka pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanoram vetka pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">vara penne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vara penne "/>
</div>
<div class="lyrico-lyrics-wrapper">muthana manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthana manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">mudinjivacha asaigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudinjivacha asaigala"/>
</div>
<div class="lyrico-lyrics-wrapper">munthanai mudichala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthanai mudichala "/>
</div>
<div class="lyrico-lyrics-wrapper">thara penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thara penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">orakannala pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orakannala pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">sirikira onna patha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirikira onna patha"/>
</div>
<div class="lyrico-lyrics-wrapper">mapula mayanguran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mapula mayanguran"/>
</div>
<div class="lyrico-lyrics-wrapper">osaillama manasukulla kuthikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osaillama manasukulla kuthikira"/>
</div>
<div class="lyrico-lyrics-wrapper">unna sernthey mapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna sernthey mapula"/>
</div>
<div class="lyrico-lyrics-wrapper">parakuran parakuranee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parakuran parakuranee"/>
</div>
</pre>
