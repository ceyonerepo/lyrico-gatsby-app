---
title: "vaanam thoorammalae song lyrics"
album: "Sketch"
artist: "S. Thaman"
lyricist: "Kabilan"
director: "Vijay Chandar"
path: "/albums/sketch-lyrics"
song: "Vaanam Thoorammalae"
image: ../../images/albumart/sketch.jpg
date: 2018-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Yl1JDQPxstI"
type: "love"
singers:
  - Deepak Subramaniam
  - Roshini
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhaya Kuzhandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaya Kuzhandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalin Ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalin Ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumbai Kadikiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumbai Kadikiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil Enadhu Pozhudhu Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Enadhu Pozhudhu Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karupaai Vidigiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karupaai Vidigiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyin Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyin Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarum Pozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarum Pozhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhiyil Mudigiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhiyil Mudigiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalai Avalo Thirudiya Piragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalai Avalo Thirudiya Piragum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal Irukiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Irukiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Thooraamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Thooraamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Poo Pookuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Poo Pookuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Un Thotta Poovuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Un Thotta Poovuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Dhaan Veyli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Dhaan Veyli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Thooraamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Thooraamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Poo Pookuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Poo Pookuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Un Thotta Poovuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Un Thotta Poovuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Dhaan Veyli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Dhaan Veyli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Kann Paartha Velaiku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kann Paartha Velaiku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Kooli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Kooli"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Vizhi Yaavume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Vizhi Yaavume"/>
</div>
<div class="lyrico-lyrics-wrapper">Mouna Mozhi Aagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mouna Mozhi Aagume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodai Veyilaale Kadal Neerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodai Veyilaale Kadal Neerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadiyaadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadiyaadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Minal Idithaalum En Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minal Idithaalum En Vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyaadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyaadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegathadai Yedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegathadai Yedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paadhai Ariyaadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadhai Ariyaadhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inum Naan Solla Enakedhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inum Naan Solla Enakedhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhaya Kuzhandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaya Kuzhandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalin Ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalin Ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumbaai Kadikiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumbaai Kadikiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil Enadhu Pozhudhu Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Enadhu Pozhudhu Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karupaai Vidigiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karupaai Vidigiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyin Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyin Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarum Pozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarum Pozhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhiyil Mudigiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhiyil Mudigiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalai Avalo Thirudiya Piragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalai Avalo Thirudiya Piragum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal Irukiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Irukiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Mounangal Un Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Mounangal Un Kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum Varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyo En Vaarthaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyo En Vaarthaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naano Un Vaakiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naano Un Vaakiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endhan Kannadi Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Kannadi Nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gadigaarame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gadigaarame"/>
</div>
<div class="lyrico-lyrics-wrapper">Koondhal Pennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondhal Pennodu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Meesai Kudi Yerume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Meesai Kudi Yerume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaradi Yaaradi Yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradi Yaaradi Yaaradi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaradi Yaaradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradi Yaaradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoondil Kannaale Thookaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondil Kannaale Thookaththai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kolgirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kolgirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thaalaati Needhaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thaalaati Needhaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Selgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Selgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhaya Kuzhandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaya Kuzhandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalin Ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalin Ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumbaai Kadikiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumbaai Kadikiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil Enadhu Pozhudhu Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Enadhu Pozhudhu Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karupaai Vidigiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karupaai Vidigiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyin Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyin Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodarum Pozhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarum Pozhudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhiyil Mudigiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhiyil Mudigiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalai Avalo Thirudiya Piragum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalai Avalo Thirudiya Piragum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal Irukiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Irukiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Thooraamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Thooraamale"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Poo Pookuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Poo Pookuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Un Thotta Poovuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Un Thotta Poovuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Dhaan Veyli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Dhaan Veyli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhaya Kuzhandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaya Kuzhandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalin Ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalin Ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumbaai Kadikiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumbaai Kadikiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagil Enadhu Pozhudhu Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil Enadhu Pozhudhu Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karupaai Vidigiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karupaai Vidigiradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pootiya Veetil Moongilaai Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootiya Veetil Moongilaai Irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullaanguzhal Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullaanguzhal Aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaagidham Polave Idhuvarai Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagidham Polave Idhuvarai Irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavidhai Nool Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhai Nool Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Thanimayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Thanimayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhaval Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhaval Indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruvizha Kolamaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruvizha Kolamaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vin Meen Pola Pulliyai Irundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vin Meen Pola Pulliyai Irundhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennila Pol Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennila Pol Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Ketta Kelvikelaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Ketta Kelvikelaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai Badhil Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai Badhil Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Pinne Unmai Nizhalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Pinne Unmai Nizhalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadandhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandhene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan Nila Tholin Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Nila Tholin Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Pataampoochi Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pataampoochi Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarai Mele Thaneer Thuliyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarai Mele Thaneer Thuliyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaindhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaindhene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaana Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aayull Kootadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aayull Kootadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaambile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaambile"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Pookirenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pookirenn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pookiren Pookiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookiren Pookiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovai Pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegame Inikudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegame Inikudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenai Pol"/>
</div>
</pre>
