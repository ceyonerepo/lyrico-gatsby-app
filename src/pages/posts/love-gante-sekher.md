---
title: "love gante song lyrics"
album: "Sekher"
artist: "Anup Rubens"
lyricist: "Chandrabose"
director: "Jeevitha Rajasekhar"
path: "/albums/sekher-lyrics"
song: "Love Gante"
image: ../../images/albumart/sekher.jpg
date: 2022-05-20
lang: telugu
youtubeLink: "https://www.youtube.com/embed/iXllrhYGgMc"
type: "happy"
singers:
  - Vijay Prakash
  - Anup Rubens
  - L.V. Revanth
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bottu petti katuketti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottu petti katuketti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachindamma sinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachindamma sinnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bugga meda sukke petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bugga meda sukke petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Sige paduthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sige paduthunnadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nettti meeda butte petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nettti meeda butte petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachindamma sinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachindamma sinnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Butta lona nanne pette
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Butta lona nanne pette"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanne sinnel unnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanne sinnel unnadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kola kllatho nanu chusthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kola kllatho nanu chusthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Konte navvutho namile sthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konte navvutho namile sthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa navve chusi naa pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa navve chusi naa pranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jivvu jivvu mannadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jivvu jivvu mannadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danda danda danda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda danda danda"/>
</div>
<div class="lyrico-lyrics-wrapper">Love gante mogindanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love gante mogindanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda danda danda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda danda danda"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholi preme puttindanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi preme puttindanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda danda danda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda danda danda"/>
</div>
<div class="lyrico-lyrics-wrapper">Love gante mogindanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love gante mogindanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chudidharu chuttukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudidharu chuttukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachindamma sinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachindamma sinnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chunni laga chandamanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chunni laga chandamanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kappukuni unnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappukuni unnadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chudidharu chuttukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudidharu chuttukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachindamma sinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachindamma sinnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chunni laga chandamanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chunni laga chandamanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kappukuni unnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappukuni unnadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mella mellaga adugesthunnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella mellaga adugesthunnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nele vennala madugaow thunnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nele vennala madugaow thunnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana nadake chusi naa gunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana nadake chusi naa gunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadaku dhadaku mannadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhadaku dhadaku mannadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danda danda danda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda danda danda"/>
</div>
<div class="lyrico-lyrics-wrapper">Love gante mogindanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love gante mogindanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda danda danda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda danda danda"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholi preme puttindanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi preme puttindanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda danda danda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda danda danda"/>
</div>
<div class="lyrico-lyrics-wrapper">Love gante mogindanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love gante mogindanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinthaa poola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinthaa poola"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinthaa poola cheere katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinthaa poola cheere katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachindamma sinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachindamma sinnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhoshala nagale petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhoshala nagale petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganthulesthu unnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganthulesthu unnadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rani laga raike chutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rani laga raike chutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachindamma sinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachindamma sinnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranu ranu antune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranu ranu antune"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayyina vachesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayyina vachesthunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theega malle ki chellelu annadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theega malle ki chellelu annadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thene sukkaki akkanu annadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thene sukkaki akkanu annadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana mate vintu naa manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana mate vintu naa manase"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana mate vintu naa manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana mate vintu naa manase"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danda danda danda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda danda danda"/>
</div>
<div class="lyrico-lyrics-wrapper">Love gante mogindanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love gante mogindanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Danda danda danda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danda danda danda"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholi preme puttindanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholi preme puttindanta"/>
</div>
</pre>
