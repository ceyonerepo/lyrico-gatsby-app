---
title: "kaattu payale song lyrics"
album: "Soorarai Pottru"
artist: "G V Prakash Kumar"
lyricist: "Snehan"
director: "Sudha Kongara"
path: "/albums/soorarai-pottru-song-lyrics"
song: "Kaattu Payale"
image: ../../images/albumart/soorarai-potru.jpg
date: 2020-11-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JwvM4Fiha7E"
type: "love"
singers:
  - Dhee
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Lallahi laire laire…
<input type="checkbox" class="lyrico-select-lyric-line" value="Lallahi laire laire…"/>
</div>
<div class="lyrico-lyrics-wrapper">Lallahi laire laire…
<input type="checkbox" class="lyrico-select-lyric-line" value="Lallahi laire laire…"/>
</div>
<div class="lyrico-lyrics-wrapper">Lallahi laire laire lai…
<input type="checkbox" class="lyrico-select-lyric-line" value="Lallahi laire laire lai…"/>
</div>
<div class="lyrico-lyrics-wrapper">Lallahi laire laire…
<input type="checkbox" class="lyrico-select-lyric-line" value="Lallahi laire laire…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaattu payalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaattu payalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji poda ennai orukka nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Konji poda ennai orukka nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Morattu muyala
<input type="checkbox" class="lyrico-select-lyric-line" value="Morattu muyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookki poga vandha paiyada nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Thookki poga vandha paiyada nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kerattu kaada kedantha enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Kerattu kaada kedantha enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruttu muzhikaara
<input type="checkbox" class="lyrico-select-lyric-line" value="Thiruttu muzhikaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoratti pottu izhukkurada nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Thoratti pottu izhukkurada nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruttu poonai pola enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Thiruttu poonai pola enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Urutti urutti paarthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Urutti urutti paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Surattu paambaa aakki putta nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Surattu paambaa aakki putta nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En mundhiyila soragi vecha
<input type="checkbox" class="lyrico-select-lyric-line" value="En mundhiyila soragi vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sillaraiya pola nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Sillaraiya pola nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Iduppu madippil ennanammo
<input type="checkbox" class="lyrico-select-lyric-line" value="Iduppu madippil ennanammo"/>
</div>
<div class="lyrico-lyrics-wrapper">Senjiputtu pora nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Senjiputtu pora nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarangkalla irundha enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarangkalla irundha enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Panji pola aakki putta
<input type="checkbox" class="lyrico-select-lyric-line" value="Panji pola aakki putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna viththa vechirukka nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna viththa vechirukka nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yaana pasi
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaana pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unakku yaana pasi
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan unakku yaana pasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chola pori
<input type="checkbox" class="lyrico-select-lyric-line" value="Chola pori"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enakku chola pori
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee enakku chola pori"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Lallahi laire laire lai…
<input type="checkbox" class="lyrico-select-lyric-line" value="Lallahi laire laire lai…"/>
</div>
<div class="lyrico-lyrics-wrapper">Lallahi laire laire…
<input type="checkbox" class="lyrico-select-lyric-line" value="Lallahi laire laire…"/>
</div>
<div class="lyrico-lyrics-wrapper">Lallahi laire laire lai…
<input type="checkbox" class="lyrico-select-lyric-line" value="Lallahi laire laire lai…"/>
</div>
<div class="lyrico-lyrics-wrapper">Lallahi laire laire…
<input type="checkbox" class="lyrico-select-lyric-line" value="Lallahi laire laire…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paasathaala enna neeyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Paasathaala enna neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhara vaikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Padhara vaikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathikkittu eriyum enna
<input type="checkbox" class="lyrico-select-lyric-line" value="Pathikkittu eriyum enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu nikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Paathu nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Jigaruthandaa paarvaiyaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Jigaruthandaa paarvaiyaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulira vaikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Kulira vaikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram ninnae en manasa
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhooram ninnae en manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maeya vaikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Maeya vaikkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naan velanji nikkum pombala
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan velanji nikkum pombala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkam kettu nikkuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Vekkam kettu nikkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchchi kotta vaikkuriyae vaada
<input type="checkbox" class="lyrico-select-lyric-line" value="Uchchi kotta vaikkuriyae vaada"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hoii
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoii"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nee echchi oora vaikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee echchi oora vaikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">En udamba thaikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="En udamba thaikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhukku thalli nikkura vaada
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhukku thalli nikkura vaada"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hoii
<input type="checkbox" class="lyrico-select-lyric-line" value="Hoii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naan saamaththula muzhikkuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan saamaththula muzhikkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Saarapaambaa neliyuran
<input type="checkbox" class="lyrico-select-lyric-line" value="Saarapaambaa neliyuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna senja enna nee konjam solludaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna senja enna nee konjam solludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un murattu aasa enakkuthaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Un murattu aasa enakkuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvam theriyum unakkuthaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Athuvam theriyum unakkuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna seiya unna
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna seiya unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinnu theekka poren
<input type="checkbox" class="lyrico-select-lyric-line" value="Thinnu theekka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam konjamaaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam konjamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji kolla poren
<input type="checkbox" class="lyrico-select-lyric-line" value="Konji kolla poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Veecharuvaa illamaalae vetti saaikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Veecharuvaa illamaalae vetti saaikkura"/>
</div>
  <div class="lyrico-lyrics-wrapper">Haeii
<input type="checkbox" class="lyrico-select-lyric-line" value="Haeii"/>
</div>
  <div class="lyrico-lyrics-wrapper">Velukambu vaarthaiyaala kuththi kilikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Velukambu vaarthaiyaala kuththi kilikkura"/>
</div>
  <div class="lyrico-lyrics-wrapper">Haeii
<input type="checkbox" class="lyrico-select-lyric-line" value="Haeii"/>
</div>
  <div class="lyrico-lyrics-wrapper">Soodhanamaa anga inga killi vaikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Soodhanamaa anga inga killi vaikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Soosagamaa aasaiyellam solli vaikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Soosagamaa aasaiyellam solli vaikkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee thottu paesu seekkiram
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thottu paesu seekkiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu pogum en joram
<input type="checkbox" class="lyrico-select-lyric-line" value="Vittu pogum en joram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti kadha paesa vaena vaada
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti kadha paesa vaena vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan olappaaya virikkuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan olappaaya virikkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku virundhu vaikkuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakku virundhu vaikkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhusaa enna thinuputtu podaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Muzhusaa enna thinuputtu podaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee edhuku thayangi nikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee edhuku thayangi nikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna odhukki vaikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna odhukki vaikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Summaa murandu pudikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Summaa murandu pudikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Katti alludaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Katti alludaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un murattu thimiru enakkuthaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Un murattu thimiru enakkuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvum theriyum unakkuthaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Athuvum theriyum unakkuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Poththi vacha aasa aa aa
<input type="checkbox" class="lyrico-select-lyric-line" value="Poththi vacha aasa aa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponguthadaa ulaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ponguthadaa ulaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poththukittu ooththa aa aa
<input type="checkbox" class="lyrico-select-lyric-line" value="Poththukittu ooththa aa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poguthadaa mazhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Poguthadaa mazhaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Lallahi laire laire lai…
<input type="checkbox" class="lyrico-select-lyric-line" value="Lallahi laire laire lai…"/>
</div>
<div class="lyrico-lyrics-wrapper">Lallahi laire laire…
<input type="checkbox" class="lyrico-select-lyric-line" value="Lallahi laire laire…"/>
</div>
<div class="lyrico-lyrics-wrapper">Lallahi laire laire lai…
<input type="checkbox" class="lyrico-select-lyric-line" value="Lallahi laire laire lai…"/>
</div>
<div class="lyrico-lyrics-wrapper">Lallahi laire laire…
<input type="checkbox" class="lyrico-select-lyric-line" value="Lallahi laire laire…"/>
</div>
</pre>
