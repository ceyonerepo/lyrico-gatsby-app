---
title: "sandamama aadathi song lyrics"
album: "Battala Ramaswamy Biopikku"
artist: "Ram Narayan"
lyricist: "Vasudeva Murthy"
director: "Rama Narayanan"
path: "/albums/battala-ramaswamy-biopikku-lyrics"
song: "Moodumulla Kanna Mundhu Sandamama Aadadhi"
image: ../../images/albumart/battala-ramaswamy-biopikku.jpg
date: 2021-05-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/rGZOb9eL9OY"
type: "happy"
singers:
  - Ramki
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Moodumulla Kanna Mundhu Sandamama Aadadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodumulla Kanna Mundhu Sandamama Aadadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Esinaaka Sooreedaina Boodidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Esinaaka Sooreedaina Boodidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pellikaaka Mundhu Banthi Poola Sendu Aadadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pellikaaka Mundhu Banthi Poola Sendu Aadadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sesukunte Ballemaipothadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sesukunte Ballemaipothadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama Nee Netthimeedha Shani Devathekkithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Nee Netthimeedha Shani Devathekkithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pema Ane Dhuradha Antukuntadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pema Ane Dhuradha Antukuntadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bava Nee Karma Meedha Kukka Kaalu Ettithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bava Nee Karma Meedha Kukka Kaalu Ettithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kummese Pelli Munchukotthadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kummese Pelli Munchukotthadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Sandhramla Dhookaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Sandhramla Dhookaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Eethotthe Bathukuthaavuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Eethotthe Bathukuthaavuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Samsaaramlo Dhookithe Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsaaramlo Dhookithe Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sepainaa Eedhalevuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sepainaa Eedhalevuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Sandhramla Dhookaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Sandhramla Dhookaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Eethotthe Bathukuthaavuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Eethotthe Bathukuthaavuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Samsaaramlo Dhookithe Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsaaramlo Dhookithe Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sepainaa Eedhalevuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sepainaa Eedhalevuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meka Esamesukunna Etagaadu Aadadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meka Esamesukunna Etagaadu Aadadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Etalona Magajaathe Boodidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etalona Magajaathe Boodidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Soku Seera Kattukunna Thaasu Paamu Aadadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soku Seera Kattukunna Thaasu Paamu Aadadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatu Shivudi Paanamaina Theetthadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu Shivudi Paanamaina Theetthadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammalle Aadapilla Andhamaina Devathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammalle Aadapilla Andhamaina Devathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanee Pellaamaithe Dheyyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanee Pellaamaithe Dheyyame"/>
</div>
<div class="lyrico-lyrics-wrapper">Ongi Moodu Mullesi Aggi Suttu Thirigithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ongi Moodu Mullesi Aggi Suttu Thirigithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavochhedhaakaa Bathuku Narakame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavochhedhaakaa Bathuku Narakame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Sandhramla Dhookaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Sandhramla Dhookaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Eethotthe Bathukuthaavuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Eethotthe Bathukuthaavuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Samsaaramlo Dhookithe Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsaaramlo Dhookithe Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sepainaa Eedhalevuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sepainaa Eedhalevuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami Sandhramla Dhookaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami Sandhramla Dhookaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Eethotthe Bathukuthaavuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Eethotthe Bathukuthaavuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Samsaaramlo Dhookithe Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samsaaramlo Dhookithe Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sepainaa Eedhalevuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sepainaa Eedhalevuraa"/>
</div>
</pre>
