---
title: "chella penney song lyrics"
album: "Tamizh Padam 2"
artist: "N. Kannan"
lyricist: "CS Amudhan - Chandru"
director: "CS Amudhan"
path: "/albums/tamizh-padam-2-lyrics"
song: "Chella Penney"
image: ../../images/albumart/tamizh-padam-2.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-hDL45hjfZE"
type: "happy"
singers:
  - Jithin
  - Sowmya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chella pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ver oor pen irunthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ver oor pen irunthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un uruvam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un uruvam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Orr kaadhal oviyamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr kaadhal oviyamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhu jenmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu jenmam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana pozhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana pozhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam bantham neendu thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam bantham neendu thodarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha vaanam vazhum varayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha vaanam vazhum varayil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa vaa ennodu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa ennodu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvae sugam thaan ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvae sugam thaan ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam nam kaadhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam nam kaadhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Panpadumae kondadumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panpadumae kondadumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aan azhaga unnai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aan azhaga unnai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veru yaarumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veru yaarumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan nenjil un ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan nenjil un ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Theendum thendralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theendum thendralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum mozhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum mozhigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu tamizh pola inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu tamizh pola inikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha kaadhal vaanil parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha kaadhal vaanil parakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan nee ini vanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nee ini vanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvom vinmeengalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvom vinmeengalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam naamaagalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam naamaagalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai thooralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai thooralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhiyaagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyaagalam"/>
</div>
</pre>
