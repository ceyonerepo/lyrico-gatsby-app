---
title: "orey oru song lyrics"
album: "Kolamaavu Kokila"
artist: "Anirudh Ravichander"
lyricist: "Vignesh Shivan"
director: "Nelson Dilipkumar"
path: "/albums/kolamaavu-kokila-lyrics"
song: "Orey Oru"
image: ../../images/albumart/kolamaavu-kokila.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TmbpMQ1wii4"
type: "happy"
singers:
  - Anirudh Ravichander
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Orae oru ooril oru veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae oru ooril oru veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru adi kooda thaangaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru adi kooda thaangaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor iravil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor iravil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru idi veetil irangiyadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru idi veetil irangiyadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae nodiyil veedum karugiyadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae nodiyil veedum karugiyadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ann naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ann naalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orae oru odai melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae oru odai melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru odam midhandhirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru odam midhandhirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae oru vellam vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae oru vellam vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adithu ponadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adithu ponadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orae oru kootinullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae oru kootinullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru koottam olindhirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru koottam olindhirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kilai murindha dhaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kilai murindha dhaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai mudhindhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhai mudhindhadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moodiyadhellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodiyadhellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini meendum endru thirakkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini meendum endru thirakkumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendiyathellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendiyathellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini eppodhu nadakkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini eppodhu nadakkumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En aasai ondrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aasai ondrae"/>
</div>
<div class="lyrico-lyrics-wrapper">En kanavum ondrae ondruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanavum ondrae ondruthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru naal azhagaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal azhagaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhndhaal podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhndhaal podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathamaai naan azhudhidathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathamaai naan azhudhidathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu athanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu athanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuddhamaaga maarudho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuddhamaaga maarudho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothamaai naan mudindhidathaan indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothamaai naan mudindhidathaan indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai meeriEnnanavo aagudhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai meeriEnnanavo aagudhoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavamaai naan pudhaindhidathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavamaai naan pudhaindhidathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru suzhal ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru suzhal ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi suthi seendutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi suthi seendutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kovamaai naan kidanthidathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovamaai naan kidanthidathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala pudhiragalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala pudhiragalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai theduthoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai theduthoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru pani moottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pani moottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru pugai ena thondiriyadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru pugai ena thondiriyadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru muyal koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru muyal koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal naduvil thinarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal naduvil thinarudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadiyadhellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadiyadhellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini meendum endru malarumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini meendum endru malarumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thediyadhellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thediyadhellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini eppodhu kidaikkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini eppodhu kidaikkumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaasai ondrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaasai ondrae"/>
</div>
<div class="lyrico-lyrics-wrapper">En kanavum ondrae ondru dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanavum ondrae ondru dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru naal azhagaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal azhagaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhndhaal podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhndhaal podhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moodiyadhellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodiyadhellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini meendum endru thirakkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini meendum endru thirakkumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendiyathellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendiyathellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini eppodhu nadakkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini eppodhu nadakkumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En aasai ondrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aasai ondrae"/>
</div>
<div class="lyrico-lyrics-wrapper">En kanavum ondrae ondruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanavum ondrae ondruthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru naal azhagaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal azhagaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhndhaal podhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhndhaal podhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum"/>
</div>
</pre>
