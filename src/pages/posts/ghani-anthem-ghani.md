---
title: "ghani anthem song lyrics"
album: "Ghani"
artist: "S. Thaman"
lyricist: "Ramajogayya Sastry"
director: "Kiran Korrapati"
path: "/albums/ghani-lyrics"
song: "Ghani Anthem"
image: ../../images/albumart/ghani.jpg
date: 2021-12-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/WVS8nAkiWj4"
type: "theme song"
singers:
  - Aditya Iyengar
  - Sri Krishna
  - Sai Charan
  - Prudhvi Chandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee jaga jagadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee jaga jagadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadalaku raa kada varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadalaku raa kada varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee kadana gunaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee kadana gunaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Avasaraame prati kalaaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaraame prati kalaaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy ninnenti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy ninnenti "/>
</div>
<div class="lyrico-lyrics-wrapper">monnenti neekenduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="monnenti neekenduku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivvale neeku maidhanaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvale neeku maidhanaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee chupu ye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee chupu ye "/>
</div>
<div class="lyrico-lyrics-wrapper">vaipo mallinchaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaipo mallinchaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekagrathera sopanaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekagrathera sopanaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paddavo lechavo nuvvagaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paddavo lechavo nuvvagaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Konasaagali kreeda prasthanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konasaagali kreeda prasthanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy thaggedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy thaggedi "/>
</div>
<div class="lyrico-lyrics-wrapper">neggedi lekkinchaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neggedi lekkinchaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee aate neeku sanmaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee aate neeku sanmaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">They call him ghani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They call him ghani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanivini yerugaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanivini yerugaani"/>
</div>
<div class="lyrico-lyrics-wrapper">They call him ghani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They call him ghani"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokaam thanakani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokaam thanakani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">They call him ghani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They call him ghani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanivini yerugaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanivini yerugaani"/>
</div>
<div class="lyrico-lyrics-wrapper">They call him ghani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They call him ghani"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokaam thanakani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokaam thanakani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy repu manadiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy repu manadiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupu manadira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupu manadira"/>
</div>
<div class="lyrico-lyrics-wrapper">Reyi chivaraalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reyi chivaraalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluthurun dhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluthurun dhiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy repu manadiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy repu manadiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupu manadira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupu manadira"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi chamata bottuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi chamata bottuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Palithamundhiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palithamundhiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">They call him ghani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They call him ghani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanivini yerugaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanivini yerugaani"/>
</div>
<div class="lyrico-lyrics-wrapper">They call him ghani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They call him ghani"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokaam thanakaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokaam thanakaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">They call him ghani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They call him ghani"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanivini yerugaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanivini yerugaani"/>
</div>
<div class="lyrico-lyrics-wrapper">They call him ghani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="They call him ghani"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokaam thanakaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokaam thanakaani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Name is gha gha ghani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Name is gha gha ghani"/>
</div>
<div class="lyrico-lyrics-wrapper">Name is gha gha ghani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Name is gha gha ghani"/>
</div>
</pre>
