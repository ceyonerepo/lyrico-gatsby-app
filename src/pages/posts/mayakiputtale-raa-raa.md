---
title: "mayakiputtale song lyrics"
album: "Raa Raa"
artist: "Srikanth Deva"
lyricist: "Yugabharathi"
director: "Sandilya"
path: "/albums/raa-raa-lyrics"
song: "Mayakiputtale"
image: ../../images/albumart/raa-raa.jpg
date: 2011-10-07
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Naresh Iyer
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mayakkipputtaaley enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkipputtaaley enna"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakkipputtaaley enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakkipputtaaley enna"/>
</div>
<div class="lyrico-lyrics-wrapper">saachipputtaaley en anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saachipputtaaley en anbey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madakkipputtaaney enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madakkipputtaaney enna"/>
</div>
<div class="lyrico-lyrics-wrapper">madakkipputtaaney enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madakkipputtaaney enna"/>
</div>
<div class="lyrico-lyrics-wrapper">serndhizhuthaaney en anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serndhizhuthaaney en anbey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru sinna paarvaiyil ennaichikka vaikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru sinna paarvaiyil ennaichikka vaikkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaithu chellam konjiye nenjai sikkedukkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaithu chellam konjiye nenjai sikkedukkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">sollum vaarthaiyil ennai vikkavaikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollum vaarthaiyil ennai vikkavaikkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjil pinbu ennaiye kanno sokkivaikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjil pinbu ennaiye kanno sokkivaikkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravedhu pagaledhu unnai kaanaamal nalamedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravedhu pagaledhu unnai kaanaamal nalamedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhaiyedhu veyiledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhaiyedhu veyiledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">unai seraamal sugamedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai seraamal sugamedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru naalalla iru naalalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naalalla iru naalalla"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinandhoarum nee vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinandhoarum nee vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">thuliyaayilla radhiyaayilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuliyaayilla radhiyaayilla"/>
</div>
<div class="lyrico-lyrics-wrapper">kadal poaley nee vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal poaley nee vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">edhedho aasaigaley ennulley neeludhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhedho aasaigaley ennulley neeludhey"/>
</div>
<div class="lyrico-lyrics-wrapper">koodaadha koalamum kaal poadudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodaadha koalamum kaal poadudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">edhedho aasaigal ennulley neeludhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhedho aasaigal ennulley neeludhey"/>
</div>
<div class="lyrico-lyrics-wrapper">poadaadha koalamum kaal poadudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poadaadha koalamum kaal poadudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulikkaalam edharkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulikkaalam edharkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai mael saadham kural kaettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai mael saadham kural kaettu"/>
</div>
<div class="lyrico-lyrics-wrapper">veiyil kaalam edharkkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veiyil kaalam edharkkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">aalaagaadho unai paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalaagaadho unai paarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila nootraandu puli kaanaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila nootraandu puli kaanaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">puyal megam nee anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyal megam nee anbey"/>
</div>
<div class="lyrico-lyrics-wrapper">unai aaraayum oru medhaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai aaraayum oru medhaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">inimeley naan anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inimeley naan anbey"/>
</div>
<div class="lyrico-lyrics-wrapper">theeraadha kaadhaley koodaamal koodudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeraadha kaadhaley koodaamal koodudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">vethaalam poalavey nenjaanadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethaalam poalavey nenjaanadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">theeraadha kaadhaley koodaamal koodudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeraadha kaadhaley koodaamal koodudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">vethaalam poalavey nenjaanadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethaalam poalavey nenjaanadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkipputtaaley enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkipputtaaley enna"/>
</div>
<div class="lyrico-lyrics-wrapper">mayakkipputtaaley enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayakkipputtaaley enna"/>
</div>
<div class="lyrico-lyrics-wrapper">saachipputtaaley en anbey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saachipputtaaley en anbey"/>
</div>
</pre>
