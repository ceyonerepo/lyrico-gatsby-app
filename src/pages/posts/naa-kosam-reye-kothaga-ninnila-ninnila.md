---
title: "naa kosam song lyrics"
album: "Ninnila Ninnila"
artist: "Rajesh Murugesan"
lyricist: "Sri Mani"
director: "Ani. I. V. Sasi"
path: "/albums/ninnila-ninnila-lyrics"
song: "Naa Kosam Reye Kothaga"
image: ../../images/albumart/ninnila-ninnila.jpg
date: 2021-02-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/G_G3R6e10RQ"
type: "melody"
singers:
  - Vijay Yesudas
  - Rajesh Murugesan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Namaskaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namaskaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Andhamaina london nagaramlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Andhamaina london nagaramlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnamai vennela reyi lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnamai vennela reyi lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mimmalni laalinchi jolinchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mimmalni laalinchi jolinchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidhra pucchadaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidhra pucchadaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Kammani paata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Kammani paata"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh YEAH, BASS
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh YEAH, BASS"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa kosam reye kotthaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kosam reye kotthaga"/>
</div>
<div class="lyrico-lyrics-wrapper">poosene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poosene"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa desham pere melukuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa desham pere melukuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Merisene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merisene"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaaranatha yekaantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaaranatha yekaantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaraltho saavasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaraltho saavasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidhurane marichina bhoomila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidhurane marichina bhoomila"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah ilaaka naadhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah ilaaka naadhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaaga nenunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaaga nenunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaaga nidhurapothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaaga nidhurapothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Antoo kantu melukodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antoo kantu melukodha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaanti vaadantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaanti vaadantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoommeedha ledantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoommeedha ledantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kanti papa nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kanti papa nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">kunuku maani kaachukodhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunuku maani kaachukodhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velugutho voosadaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velugutho voosadaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Raathiraithe vennele vannele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiraithe vennele vannele"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaku choopene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku choopene"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojila saagene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojila saagene"/>
</div>
<div class="lyrico-lyrics-wrapper">Reyi pagalila naatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reyi pagalila naatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Sneha chainee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sneha chainee"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhamaina navvundhi pedavipai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhamaina navvundhi pedavipai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammanaina dhaarundhi padhamukai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammanaina dhaarundhi padhamukai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kantipaina kunukunte lekunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kantipaina kunukunte lekunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothe ponee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothe ponee"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalullo sangeetham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalullo sangeetham"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello santhosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello santhosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Haayigaa brathukilaa saagani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haayigaa brathukilaa saagani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innunte naa chuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innunte naa chuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhanta ye lotu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhanta ye lotu"/>
</div>
<div class="lyrico-lyrics-wrapper">O reppa paatu kunuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O reppa paatu kunuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekapothe lekaponee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekapothe lekaponee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamlo nenuntoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamlo nenuntoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalallo ledhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalallo ledhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kanti papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kanti papa"/>
</div>
<div class="lyrico-lyrics-wrapper">reppaveyanantu Melukonee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="reppaveyanantu Melukonee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ilaaka naadhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilaaka naadhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaaga nenunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaaga nenunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelaaga nidhurapothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelaaga nidhurapothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Antoo kantu melukodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antoo kantu melukodha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaanti vaadantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaanti vaadantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoommeedha ledantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoommeedha ledantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kanti papa nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kanti papa nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">kunuku maani kaachukodhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunuku maani kaachukodhaa"/>
</div>
</pre>
