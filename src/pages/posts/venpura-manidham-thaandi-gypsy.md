---
title: "venpura - manidham thaandi song lyrics"
album: "Gypsy"
artist: "Santhosh Narayanan"
lyricist: "Yugabharathi"
director: "Raju Murugan"
path: "/albums/gypsy-song-lyrics"
song: "Venpura - Manidham Thaandi"
image: ../../images/albumart/gypsy.jpg
date: 2020-03-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kIpoFj_GURg"
type: "motivational"
singers:
  - T.M. Krishna
  - Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manidham thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidham thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Punitham illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punitham illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan illai vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manidham thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidham thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Punitham illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punitham illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan illai vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mandraadum nenjai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandraadum nenjai"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyil yenthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyil yenthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannippil thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannippil thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanudam pookkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanudam pookkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraana kaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraana kaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Malaiyaai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyaai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaana vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaana vegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Verudan saaikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verudan saaikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam inagae orr uyir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam inagae orr uyir"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingoo elaam orr kural
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingoo elaam orr kural"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae ellam orr uyirrr…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae ellam orr uyirrr….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eyyyyyyy oooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyyyyyyy oooh"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy hoo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy hoo oo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manidham thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidham thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Punitham illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punitham illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan illai vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manidham thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidham thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Punitham illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punitham illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan illai vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhanai theera vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhanai theera vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanudan vaazha vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanudan vaazha vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaazhisai koora vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaazhisai koora vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manidham thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidham thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Punitham illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punitham illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I have a dream
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I have a dream"/>
</div>
<div class="lyrico-lyrics-wrapper">That one day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That one day"/>
</div>
<div class="lyrico-lyrics-wrapper">This nation will rise up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This nation will rise up"/>
</div>
<div class="lyrico-lyrics-wrapper">Live out the true meaning of its creed
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Live out the true meaning of its creed"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alli veesidu un kaiyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli veesidu un kaiyaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin dhaaniyam mann melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin dhaaniyam mann melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottrai maarbil pasiyaarum dhesamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottrai maarbil pasiyaarum dhesamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyil pesidum kannalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyil pesidum kannalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbil saathiyam ellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbil saathiyam ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottrai kootil urangaaiyo dhesamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottrai kootil urangaaiyo dhesamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karangal kodi inaiyum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karangal kodi inaiyum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suvargal yaavum idiyaadhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suvargal yaavum idiyaadhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manangal maara thuniyum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manangal maara thuniyum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhangal odi oliyaadhoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhangal odi oliyaadhoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhanai theerkka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhanai theerkka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettrumai neekka vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettrumai neekka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo vaa venpuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo oo vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo oo vaa venpuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugangal kodi endrabothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugangal kodi endrabothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer sirippu ondru thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer sirippu ondru thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirangal kodi endrabothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirangal kodi endrabothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirangal kodi endrabothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirangal kodi endrabothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum en thozhanaeondraavom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum en thozhanaeondraavom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manidham thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidham thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Punitham illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punitham illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan illai vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manidham thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidham thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Punitham illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punitham illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven puraa….aaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven puraa….aaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo ooo hooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo ooo hooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaetrumaiyil ottrumai pondra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaetrumaiyil ottrumai pondra"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathiya vazhi muraogalai peni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathiya vazhi muraogalai peni"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamilargal dhoorum ilam kuzhandhaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamilargal dhoorum ilam kuzhandhaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhil padhithu amaithiyaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhil padhithu amaithiyaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagathai padaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagathai padaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaga tamilargal ottrumai pada vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaga tamilargal ottrumai pada vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un mugathil en mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugathil en mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">En mugathil un mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mugathil un mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan muluthum namm mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan muluthum namm mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam muluthum niraiyum valiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam muluthum niraiyum valiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulum oilyum azhiyum mozhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulum oilyum azhiyum mozhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhiyum vanamum thiraiyum nilamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadhiyum vanamum thiraiyum nilamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Perugum perugum orr kudam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perugum perugum orr kudam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaahh venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahh venpuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oh hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh hoo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo hoo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh hoo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy heyy heyy heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy heyy heyy heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh hoo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo hoo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh hoo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ven puraaa…..aaa….aa….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven puraaa…..aaa….aa…."/>
</div>
<div class="lyrico-lyrics-wrapper">Ven puraa..ven puraa ven puraa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven puraa..ven puraa ven puraa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Venn….puraa….aaa….aaa…..aa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venn….puraa….aaa….aaa…..aa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manidham thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidham thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Punitham illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punitham illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidham thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidham thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Punitham illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punitham illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oh hoo venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh hoo venpuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh hoo venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh hoo venpuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh hoo venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh hoo venpuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh hoo venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh hoo venpuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manidham thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidham thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Punitham illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punitham illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidham thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidham thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Punitham illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punitham illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan illai vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivan illai vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa venpuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa venpuraa"/>
</div>
</pre>
