---
title: "krishna mukundha song lyrics"
album: "Kalakalappu 2"
artist: "Hiphop Tamizha"
lyricist: "Mohan Rajan"
director: "Sundar C"
path: "/albums/kalakalappu-2-lyrics"
song: "Krishna Mukundha"
image: ../../images/albumart/kalakalappu-2.jpg
date: 2018-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/q4GPvV9sziU"
type: "happy"
singers:
  - Kaushik Krish
  - Padmalatha
  - Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Krishnaaa aaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishnaaa aaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mukundhaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukundhaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muraaaraeee ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muraaaraeee ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muraaaaraeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muraaaaraeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Krishna mukundha murarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna mukundha murarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya krishna mukundha murarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya krishna mukundha murarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuna sagara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuna sagara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamala nayaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala nayaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakambara dhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakambara dhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Go paa laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go paa laa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhalaai mudhan mudhalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalaai mudhan mudhalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pennai parthu thorkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pennai parthu thorkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thugalaai siru thugalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thugalaai siru thugalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjai naanum parkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjai naanum parkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathadi melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathadi melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kai rendum keezhlae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kai rendum keezhlae"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum kaatrodum maraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum kaatrodum maraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru noola en kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru noola en kaadhalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan en kaadhal kaata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan en kaadhal kaata"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu teriyamal nindraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu teriyamal nindraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan enna seiya kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan enna seiya kaadhalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Krishna mukundha murarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna mukundha murarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya krishna mukundha murarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya krishna mukundha murarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya krishna mukundha murarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya krishna mukundha murarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya krishna mukundha murarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya krishna mukundha murarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae singari sillari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae singari sillari"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjamellam kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjamellam kannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyiren unnala di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyiren unnala di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae inthadi andhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae inthadi andhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un anba neethaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un anba neethaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkuren thannala di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkuren thannala di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kura mela kalla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kura mela kalla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai sutthi vandhenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai sutthi vandhenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasa nee thokki potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa nee thokki potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Koththikittu povenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koththikittu povenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi pudicha paarai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi pudicha paarai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kozhanjen theriyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kozhanjen theriyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadam pudikum manasa neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadam pudikum manasa neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aravanaikka mudiyadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aravanaikka mudiyadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Krishna mukundha murarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna mukundha murarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Krishna mukundha murarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna mukundha murarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naraenaaarae nanananaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naraenaaarae nanananaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandha na thana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandha na thana naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana thandha na thaana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana thandha na thaana naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana thandha thathaanna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana thandha thathaanna "/>
</div>
<div class="lyrico-lyrics-wrapper">thaathana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaathana naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana thandha thathaanna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana thandha thathaanna "/>
</div>
<div class="lyrico-lyrics-wrapper">thaathana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaathana naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Krishna mukundha murarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishna mukundha murarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaya krishna mukundha murarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaya krishna mukundha murarae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuna sagara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuna sagara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamala nayaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamala nayaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakambara dhaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakambara dhaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Gopaalaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gopaalaaaaa"/>
</div>
</pre>
