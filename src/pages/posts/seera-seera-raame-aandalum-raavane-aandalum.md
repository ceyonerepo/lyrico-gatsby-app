---
title: "seera seera song lyrics"
album: "Raame Aandalum Raavane Aandalum"
artist: "Krish"
lyricist: "Yugabharathi"
director: "Arisil Moorthy"
path: "/albums/raame-aandalum-raavane-aandalum-lyrics"
song: "Seera Seera"
image: ../../images/albumart/raame-aandalum-raavane-aandalum.jpg
date: 2021-09-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/aUxMNGgqGzs"
type: "happy"
singers:
  - Mahalingam
  - Rajeshwari
  - Krish
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kombaana kombu vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombaana kombu vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadu namma selvammadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadu namma selvammadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thembaavae neeyum adha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thembaavae neeyum adha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatha kurai illayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha kurai illayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senjaanthu pottu vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjaanthu pottu vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maata nedham kumbidadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maata nedham kumbidadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheivaamsam ellaam unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheivaamsam ellaam unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi vandhae thangumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi vandhae thangumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seera seera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seera seera"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaman seera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman seera"/>
</div>
<div class="lyrico-lyrics-wrapper">Serum maadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum maadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna nambi vaazha vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna nambi vaazha vandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaeraa thaeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeraa thaeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonjal thaeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal thaeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Potta pulla aala vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potta pulla aala vandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soothu sogam neeyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soothu sogam neeyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondha bandham nooraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondha bandham nooraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Sitham adhu un moonja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitham adhu un moonja"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakka paakka yelelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakka paakka yelelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattaan thara kaathoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaan thara kaathoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kummalam dhaan nee poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kummalam dhaan nee poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Satham podum un madiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satham podum un madiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekka kekka thaalelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekka kekka thaalelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kombaana kombu vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombaana kombu vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadu namma selvammadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadu namma selvammadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thembaavae neeyum adha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thembaavae neeyum adha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatha kurai illayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha kurai illayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senjaanthu pottu vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjaanthu pottu vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maata nedham kumbidadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maata nedham kumbidadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheivaamsam ellaam unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheivaamsam ellaam unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi vandhae thangumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi vandhae thangumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayatha kaadae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayatha kaadae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanjaalum kalla kaadan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanjaalum kalla kaadan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaya maatanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaya maatanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadaanda rasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadaanda rasan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saanjaalum netta vaalan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanjaalum netta vaalan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhva kaapanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhva kaapanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velli nera kaangeyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli nera kaangeyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetukkulla vaaraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetukkulla vaaraaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarathiyum eduthidadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarathiyum eduthidadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnana thaaipasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnana thaaipasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaimari pogaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaimari pogaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayaga un maadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayaga un maadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sellamum konjumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sellamum konjumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thullivarum seiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thullivarum seiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil nee yentha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil nee yentha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachathellaam palikkumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachathellaam palikkumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei vellaiyaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei vellaiyaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaami kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaami kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhu kaaval kaami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu kaaval kaami"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala maada karai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala maada karai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethum saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethum saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odi odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi odi"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaichomae odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaichomae odi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadillaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadillaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Suzhalaathae bhoomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhalaathae bhoomi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nedham vanangum paramasivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nedham vanangum paramasivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyila varuvadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyila varuvadhilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolam velanga varamidhupol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolam velanga varamidhupol"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagila kadavul illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagila kadavul illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadi eduthu adha adicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadi eduthu adha adicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Valikudhu manasukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikudhu manasukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Varam palavae adhu kodukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varam palavae adhu kodukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanangidu kavalayilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanangidu kavalayilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kombaana kombu vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kombaana kombu vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadu namma selvammadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadu namma selvammadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thembaavae neeyum adha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thembaavae neeyum adha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatha kurai illayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatha kurai illayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senjaanthu pottu vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjaanthu pottu vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maata nedham kumbidadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maata nedham kumbidadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheivaamsam ellaam unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheivaamsam ellaam unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedi vandhae thangumadi hmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedi vandhae thangumadi hmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanaanae thanae thaananaae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanaanae thanae thaananaae"/>
</div>
</pre>
