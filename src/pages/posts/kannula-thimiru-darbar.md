---
title: 'kannula thimiru song lyrics'
album: 'Darbar'
artist: 'Anirudh Ravichander'
lyricist: 'Vivek'
director: 'A R Murugadoss'
path: '/albums/darbar-song-lyrics'
song: 'Kannula thimiru'
image: ../../images/albumart/darbar.jpg
date: 2020-01-09
lang: tamil
singers:
- Chandramukhi Muvvala
- Priya Murthy Chadalawala
- Rachana Mudraboyina
youtubeLink: "https://www.youtube.com/embed/pri6MKIGrMc"
type: 'mass'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Raja aaya
<input type="checkbox" class="lyrico-select-lyric-line" value="Raja aaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Darbaar aaj khula re
<input type="checkbox" class="lyrico-select-lyric-line" value="Darbaar aaj khula re"/>
</div>
<div class="lyrico-lyrics-wrapper">Bijli jo kadke ye
<input type="checkbox" class="lyrico-select-lyric-line" value="Bijli jo kadke ye"/>
</div>
<div class="lyrico-lyrics-wrapper">Sher Dahaade
<input type="checkbox" class="lyrico-select-lyric-line" value="Sher Dahaade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey ennanga di
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey ennanga di"/>
</div>
<div class="lyrico-lyrics-wrapper">Hindila paadikittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Hindila paadikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivan enga aalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaivan enga aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee othu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee othu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannula thimiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannula thimiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna rod edukka vandhaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna rod edukka vandhaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivan vera ragam
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaivan vera ragam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu usaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Paathu usaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mogara veengikumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Mogara veengikumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Side-u vaangikumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Side-u vaangikumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Parava moonu vandhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Parava moonu vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaya suthi aadikkumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaya suthi aadikkumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kai kaal maarikkumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai kaal maarikkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuru vaarikkumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Usuru vaarikkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemana paathu vara
<input type="checkbox" class="lyrico-select-lyric-line" value="Yemana paathu vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeni padi yerikkumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeni padi yerikkumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannula thimiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannula thimiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna rod edukka vandhaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna rod edukka vandhaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivan vera ragam
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaivan vera ragam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu vilaadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paathu vilaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thalaivaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaivaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Moracha kaara chilli
<input type="checkbox" class="lyrico-select-lyric-line" value="Moracha kaara chilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Verkum nooru milli
<input type="checkbox" class="lyrico-select-lyric-line" value="Verkum nooru milli"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangada periya pulli
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaangada periya pulli"/>
</div>
<div class="lyrico-lyrics-wrapper">Paski edu pera solli
<input type="checkbox" class="lyrico-select-lyric-line" value="Paski edu pera solli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sooda order panni
<input type="checkbox" class="lyrico-select-lyric-line" value="Sooda order panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppa erakkita nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Neruppa erakkita nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhaikka aasapattaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Pozhaikka aasapattaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomi senju poyiru nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Bhoomi senju poyiru nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannula thimiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannula thimiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna rod edukka vandhaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna rod edukka vandhaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivan vera ragam
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaivan vera ragam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu usaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Paathu usaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thalaivaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaivaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thalaivaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaivaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thalaivaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaivaa…"/>
</div>
</pre>