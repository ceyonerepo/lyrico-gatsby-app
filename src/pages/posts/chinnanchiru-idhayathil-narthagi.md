---
title: "chinnanchiru idhayathil song lyrics"
album: "Narthagi"
artist: "G.V. Prakash Kumar"
lyricist: "Na. Muthukumar"
director: "G. Vijayapadma"
path: "/albums/narthagi-lyrics"
song: "Chinnanchiru Idhayathil"
image: ../../images/albumart/narthagi.jpg
date: 2011-05-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CVQph4koCWQ"
type: "melody"
singers:
  - Vijay Prakash
  - Prashanthini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chinnanjiru idhayathilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanjiru idhayathilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai thuli gudhikkiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai thuli gudhikkiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna idhu sondhamendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna idhu sondhamendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti etti paarkkiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti etti paarkkiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaithuli vizhuvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaithuli vizhuvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannukku theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannukku theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaithuli serum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaithuli serum idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukku theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukku theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu nadhiyaaga odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu nadhiyaaga odi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaloadu koodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaloadu koodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthaagumo illai uppaagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthaagumo illai uppaagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeraeri raeroo yaeraeriroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeraeri raeroo yaeraeriroo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnanjiru idhayathilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanjiru idhayathilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai thuli gudhikkiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai thuli gudhikkiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna idhu sondhamendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna idhu sondhamendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti etti paarkkiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti etti paarkkiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaithuli vizhuvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaithuli vizhuvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannukku theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannukku theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaithuli serum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaithuli serum idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukku theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukku theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu nadhiyaaga odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu nadhiyaaga odi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaloadu koodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaloadu koodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthaagumo illai uppaagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthaagumo illai uppaagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeraeri raeroo yaeraeriroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeraeri raeroo yaeraeriroo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kilaigalil poo poothaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilaigalil poo poothaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukku therindhuvidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukku therindhuvidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Verilum poo pookkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verilum poo pookkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuthaan penn idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuthaan penn idhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penn manandhin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penn manandhin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai regai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai regai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valara valara kooda valarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valara valara kooda valarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum konjam melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum konjam melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu kannaadi bimbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu kannaadi bimbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai kai theendum vayadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai kai theendum vayadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam anbaagi anbaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam anbaagi anbaagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaippaayum manadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaippaayum manadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnanjiru idhayathilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanjiru idhayathilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai thuli gudhikkiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai thuli gudhikkiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna idhu sondhamendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna idhu sondhamendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti etti paarkkiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti etti paarkkiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvoru vayadhinilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru vayadhinilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru kanavirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru kanavirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvae kanavumayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvae kanavumayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enbadhaiyae marandhirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enbadhaiyae marandhirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thozhamaiyin kaippidiththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhamaiyin kaippidiththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthu sellum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadanthu sellum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalennum oorukkadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalennum oorukkadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottichellum paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottichellum paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silar theriyaamal povaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silar theriyaamal povaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Silar therindhaethaan povaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silar therindhaethaan povaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini edhirkaalam ennaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini edhirkaalam ennaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar sollakkoodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar sollakkoodum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyeyy heyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyeyy heyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinnanjiru idhayathilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanjiru idhayathilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai thuli gudhikkiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai thuli gudhikkiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna idhu sondhamendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna idhu sondhamendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Etti etti paarkkiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti etti paarkkiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhaithuli vizhuvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaithuli vizhuvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannukku theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannukku theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaithuli serum idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaithuli serum idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukku theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukku theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu nadhiyaaga odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu nadhiyaaga odi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaloadu koodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaloadu koodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthaagumo illai uppaagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthaagumo illai uppaagumo"/>
</div>
</pre>
