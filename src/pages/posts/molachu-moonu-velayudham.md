---
title: "molachu moonu song lyrics"
album: "Velayudham"
artist: "Vijay Antony"
lyricist: "Viveka"
director: "M. Raja"
path: "/albums/velayudham-lyrics"
song: "Molachu Moonu"
image: ../../images/albumart/velayudham.jpg
date: 2011-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/h_yH7FE7S4c"
type: "love"
singers:
  - V.V. Prasanna
  - Supriya Joshi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ah Naana Naana Nanananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah Naana Naana Nanananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanana Naananaana Nanananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanana Naananaana Nanananaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Molachu Moonu Elayae Vidala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molachu Moonu Elayae Vidala"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuven Ulaga Azhagi Medala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuven Ulaga Azhagi Medala"/>
</div>
<div class="lyrico-lyrics-wrapper">Veralu Vendakaa Un Kaadhu Avarakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veralu Vendakaa Un Kaadhu Avarakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooku Molagaa Mookuthi Kadugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooku Molagaa Mookuthi Kadugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanindha Kaai Thotam Nee Thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanindha Kaai Thotam Nee Thaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Aa Aa Aa Aa Aa Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Aa Aa Aa Aa Aa Aa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayaso Pathinanju Adi Vaadi Maam Pinju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayaso Pathinanju Adi Vaadi Maam Pinju"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavam En Nenju Ennai Paarthu Nee Konju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavam En Nenju Ennai Paarthu Nee Konju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvai Thirupaachi Un Theendal Nerupaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Thirupaachi Un Theendal Nerupaachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarthaalae En Pagalum Iravaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarthaalae En Pagalum Iravaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kanna Pinnannu Nee Azhaga Irukuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kanna Pinnannu Nee Azhaga Irukuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Ennum Vaanaliyil Enna Porikiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Ennum Vaanaliyil Enna Porikiriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaigal Moodaamal Konjal Paarvai Paakuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigal Moodaamal Konjal Paarvai Paakuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anju Nodiyil Nenju Kuzhiyil Enna Podhaikiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anju Nodiyil Nenju Kuzhiyil Enna Podhaikiriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odambellam Machakaari Usupethum Kachikaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambellam Machakaari Usupethum Kachikaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaana Mottukaari Mosakaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaana Mottukaari Mosakaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odambellam Machakaara Usupethum Kachikaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambellam Machakaara Usupethum Kachikaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaana Mottukaaraa Meesaikaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaana Mottukaaraa Meesaikaaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Molachu Moonu Elayae Vidala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molachu Moonu Elayae Vidala"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuven Ulaga Azhagi Medala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuven Ulaga Azhagi Medala"/>
</div>
<div class="lyrico-lyrics-wrapper">Veralu Vendakaa Un Kaadhu Avarakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veralu Vendakaa Un Kaadhu Avarakaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siripu Kalkandu Un Sinungal Anukundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siripu Kalkandu Un Sinungal Anukundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal Karuvandu Adi Vizhunthen Adhai Kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Karuvandu Adi Vizhunthen Adhai Kandu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhu Nagam Keeri En Udambil Thazhumberi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu Nagam Keeri En Udambil Thazhumberi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alarum Naal Thedi En Aaval Peridhaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alarum Naal Thedi En Aaval Peridhaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Dhinusu Dhinusaaga Dhinam Kanavil Thonuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Dhinusu Dhinusaaga Dhinam Kanavil Thonuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiya Thirupi Usura Varuthi Paduthi Edukuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiya Thirupi Usura Varuthi Paduthi Edukuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhusu Muzhusaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhusu Muzhusaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Muzhunga Ninaikiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Muzhunga Ninaikiriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Odamba Muruki Valayal Noruki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odamba Muruki Valayal Noruki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaiya Mudikiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaiya Mudikiriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Medaana Pallathakae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medaana Pallathakae"/>
</div>
<div class="lyrico-lyrics-wrapper">Midhamaana Soorai Kaatrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhamaana Soorai Kaatrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaadhaa Ennai Kona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaadhaa Ennai Kona"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Soodae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Soodae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhoram Kaadhal Pechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhoram Kaadhal Pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaana Arivaal Veechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana Arivaal Veechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu Uyirai Thachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu Uyirai Thachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Molachu Moonu Elayae Vidala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molachu Moonu Elayae Vidala"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuven Ulaga Azhagi Medala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuven Ulaga Azhagi Medala"/>
</div>
<div class="lyrico-lyrics-wrapper">Veralu Vendakaa Un Kaadhu Avarakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veralu Vendakaa Un Kaadhu Avarakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooku Molagaa Mookuthi Kadugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooku Molagaa Mookuthi Kadugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanindha Kaai Thotam Nee Thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanindha Kaai Thotam Nee Thaanaa"/>
</div>
</pre>
