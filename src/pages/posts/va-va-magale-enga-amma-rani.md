---
title: "va va magale song lyrics"
album: "Enga Amma Rani"
artist: "Ilaiyaraaja"
lyricist: "Unknown"
director: "S Bani"
path: "/albums/enga-amma-rani-lyrics"
song: "Va Va Magale"
image: ../../images/albumart/enga-amma-rani.jpg
date: 2017-05-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YmKYJGd9mdI"
type: "sad"
singers:
  -	Rajashree Pathak
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa Vaa Magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Magalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Vazhigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Vazhigal "/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai Thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai Thodarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanai Vazhigalilae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Vazhigalilae "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvarai Nadandhu Vandhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai Nadandhu Vandhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenna Thuyarangalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenna Thuyarangalai "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvarai Kadandhuvandhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai Kadandhuvandhom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serum Idam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum Idam "/>
</div>
<div class="lyrico-lyrics-wrapper">Serndhuvittal Vazhi Yaedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhuvittal Vazhi Yaedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va Va Magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Va Magalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Vazhigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Vazhigal "/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhithaai Thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhithaai Thodarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Magalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam Kaigalil Serthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Kaigalil Serthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhai Vazhithirai Maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai Vazhithirai Maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae Yaarukku Edhuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae Yaarukku Edhuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaridam Adhuvandhu Serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaridam Adhuvandhu Serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthirgalpodum Edhirgaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthirgalpodum Edhirgaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae Nummai Serkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae Nummai Serkumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhigal Ketkum Siru Paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigal Ketkum Siru Paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullin Vali Thaangumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullin Vali Thaangumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valikonjam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikonjam "/>
</div>
<div class="lyrico-lyrics-wrapper">Sugam Konjam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam Konjam "/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhu Minjum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhu Minjum "/>
</div>
<div class="lyrico-lyrics-wrapper">Num Kaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Num Kaiyil "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhiyae Nee Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhiyae Nee Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va Va Magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Va Magalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Vazhigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Vazhigal "/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhithaai Thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhithaai Thodarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Magalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koyil Manigalin Osai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyil Manigalin Osai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrinil Midhandhidum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrinil Midhandhidum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjin Ninaivinil Deepam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjin Ninaivinil Deepam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkayin Sudar Veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkayin Sudar Veesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvin Dhooram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin Dhooram "/>
</div>
<div class="lyrico-lyrics-wrapper">Tholai Dhooram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholai Dhooram "/>
</div>
<div class="lyrico-lyrics-wrapper">Endrae Ninaikindrom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrae Ninaikindrom "/>
</div>
<div class="lyrico-lyrics-wrapper">Engo Yedho Kural Kaetka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo Yedho Kural Kaetka "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Angae Nadakindrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae Nadakindrom"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhudhaalum Sirithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhudhaalum Sirithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunthaalum Ezhunthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthaalum Ezhunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhigal Dhaan Mudivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhigal Dhaan Mudivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Va Va Magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Va Va Magalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Vazhigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Vazhigal "/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhithaai Thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhithaai Thodarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanai Vazhigalilae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Vazhigalilae "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvarai Nadandhu Vandhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai Nadandhu Vandhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenna Thuyarangalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenna Thuyarangalai "/>
</div>
<div class="lyrico-lyrics-wrapper">Ithuvarai Kadanthuvanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuvarai Kadanthuvanthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serum Idam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serum Idam "/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthuvittal Vazhi Yaedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthuvittal Vazhi Yaedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Magalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innoru Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogum Vazhigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Vazhigal "/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhithaai Thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhithaai Thodarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Magalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Magalae"/>
</div>
</pre>
