---
title: "parugu parugu song lyrics"
album: "Chitralahari"
artist: "Devi Sri Prasad"
lyricist: "Devi Sri Prasad"
director: "Kishore Tirumala"
path: "/albums/chitralahari-lyrics"
song: "Parugu Parugu"
image: ../../images/albumart/chitralahari.jpg
date: 2019-04-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/eJsCokuv_YA"
type: "love"
singers:
  - David Simon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Parugu Parugu Velthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugu Parugu Velthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Etu Vaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etu Vaipu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarugu Jarugu Antundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarugu Jarugu Antundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-Uuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-Uuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Penchukuntunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Penchukuntunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Vegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Datipothundhe Lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Datipothundhe Lokam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chakraalleni Cycle Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakraalleni Cycle Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkalleni Flight-Uu Laagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalleni Flight-Uu Laagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bullet Leni Rifle Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullet Leni Rifle Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharam Leni Kite-Uu Laagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharam Leni Kite-Uu Laagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Kuda Migilipoyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Kuda Migilipoyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parugu Parugu Velthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugu Parugu Velthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Etu Vaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etu Vaipu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarugu Jarugu Antundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarugu Jarugu Antundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-Uuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-Uuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Penchukuntunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Penchukuntunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Vegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Datipothundhe Lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Datipothundhe Lokam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Repu anedhi Kalallo Nenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repu anedhi Kalallo Nenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamgaa Adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamgaa Adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raadhaa Raadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raadhaa Raadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnalone Nenu Undipovalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnalone Nenu Undipovalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaati Velle Dhari Ledha Ledha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaati Velle Dhari Ledha Ledha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mabbula Loni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mabbula Loni"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Moon Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Moon Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Orchestra Leni Tune Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orchestra Leni Tune Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Kuda Migilipoyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Kuda Migilipoyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parugu Parugu Velthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugu Parugu Velthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Etu Vaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etu Vaipu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarugu Jarugu Antundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarugu Jarugu Antundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-Uuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-Uuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Penchukuntunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Penchukuntunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Vegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Datipothundhe Lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Datipothundhe Lokam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">You Got To Run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Got To Run"/>
</div>
<div class="lyrico-lyrics-wrapper">You Got To Run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Got To Run"/>
</div>
<div class="lyrico-lyrics-wrapper">You Got To Run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Got To Run"/>
</div>
<div class="lyrico-lyrics-wrapper">You Got To Do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Got To Do"/>
</div>
<div class="lyrico-lyrics-wrapper">What You Have To Do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="What You Have To Do"/>
</div>
<div class="lyrico-lyrics-wrapper">To Get To
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To Get To"/>
</div>
<div class="lyrico-lyrics-wrapper">Where You Wana Be
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Where You Wana Be"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Is Not A
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Is Not A"/>
</div>
<div class="lyrico-lyrics-wrapper">Bed Of Roses Man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bed Of Roses Man"/>
</div>
<div class="lyrico-lyrics-wrapper">You Got To Get
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Got To Get"/>
</div>
<div class="lyrico-lyrics-wrapper">That In Your Head
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That In Your Head"/>
</div>
<div class="lyrico-lyrics-wrapper">Lets Go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lets Go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okka Adugu Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Adugu Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhukeyyanivvadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhukeyyanivvadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Venakki Thosey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venakki Thosey"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuru Gaali Gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuru Gaali Gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Mettu Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Mettu Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiki Ekkaivvadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiki Ekkaivvadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Thokke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Thokke"/>
</div>
<div class="lyrico-lyrics-wrapper">Force Nee Emanali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Force Nee Emanali"/>
</div>
<div class="lyrico-lyrics-wrapper">Antham Leni Nirikshana Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antham Leni Nirikshana Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Phalitham Leni Pariksha Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phalitham Leni Pariksha Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Kuda Migilipoyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Kuda Migilipoyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parugu Parugu Velthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugu Parugu Velthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Etu Vaipu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etu Vaipu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarugu Jarugu Antundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarugu Jarugu Antundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-Uuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-Uuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha Penchukuntunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha Penchukuntunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Vegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Datipothundhe Lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Datipothundhe Lokam"/>
</div>
</pre>
