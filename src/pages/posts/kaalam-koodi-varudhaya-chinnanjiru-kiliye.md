---
title: "kaalam koodi varudhaya song lyrics"
album: "Chinnanjiru Kiliye"
artist: "Mastan Khader"
lyricist: "Padmanaban.G"
director: "Sabarinathan Muthupandian "
path: "/albums/chinnanjiru-kiliye-lyrics"
song: "Kaalam Koodi Varudhaya"
image: ../../images/albumart/chinnanjiru-kiliye.jpg
date: 2021-09-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MhRUJ6Y46Cc"
type: "happy"
singers:
  - Sithan Jeyamoorthy 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">osanthu nikkum koburam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osanthu nikkum koburam "/>
</div>
<div class="lyrico-lyrics-wrapper">ooruke nilal aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooruke nilal aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">alukaana oora ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alukaana oora ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">pala palanu veluthachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala palanu veluthachu"/>
</div>
<div class="lyrico-lyrics-wrapper">sirusu perusu ellarukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirusu perusu ellarukum"/>
</div>
<div class="lyrico-lyrics-wrapper">vaayellam pallachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaayellam pallachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pooviruchu ponga vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pooviruchu ponga vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kondadi theeka than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondadi theeka than"/>
</div>
<div class="lyrico-lyrics-wrapper">motha sanamum onnachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="motha sanamum onnachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalam koodi varuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam koodi varuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosam kodi tharuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosam kodi tharuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">saamy theril varuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saamy theril varuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">un urava thedi varuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un urava thedi varuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">kottu adika kolava vadika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottu adika kolava vadika"/>
</div>
<div class="lyrico-lyrics-wrapper">vettu vedika kalakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettu vedika kalakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">thiruvila inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruvila inge"/>
</div>
<div class="lyrico-lyrics-wrapper">megam idika boomi selika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megam idika boomi selika"/>
</div>
<div class="lyrico-lyrics-wrapper">makka sirika varavai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makka sirika varavai "/>
</div>
<div class="lyrico-lyrics-wrapper">arulvai saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arulvai saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru seri onnaga venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru seri onnaga venum"/>
</div>
<div class="lyrico-lyrics-wrapper">saathi betham alunju poga venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathi betham alunju poga venum"/>
</div>
<div class="lyrico-lyrics-wrapper">olaikum sanam osanthaga venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olaikum sanam osanthaga venum"/>
</div>
<div class="lyrico-lyrics-wrapper">osantha than ooru olagam marum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="osantha than ooru olagam marum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalam koodi varuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam koodi varuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosam kodi tharuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosam kodi tharuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">saamy theril varuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saamy theril varuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">un urava thedi varuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un urava thedi varuthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eliyavarin nilai uyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eliyavarin nilai uyara"/>
</div>
<div class="lyrico-lyrics-wrapper">paruvathile malai poliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruvathile malai poliya"/>
</div>
<div class="lyrico-lyrics-wrapper">vilai nilathin uyir kuliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilai nilathin uyir kuliya"/>
</div>
<div class="lyrico-lyrics-wrapper">vithaithavain manam magila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithaithavain manam magila"/>
</div>
<div class="lyrico-lyrics-wrapper">paavinile pasi oliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavinile pasi oliya"/>
</div>
<div class="lyrico-lyrics-wrapper">padi valakum kalai valara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padi valakum kalai valara"/>
</div>
<div class="lyrico-lyrics-wrapper">manitha inam thinam magila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manitha inam thinam magila"/>
</div>
<div class="lyrico-lyrics-wrapper">tharani yengum tamil vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharani yengum tamil vara"/>
</div>
<div class="lyrico-lyrics-wrapper">ammachi ammane arulvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammachi ammane arulvai"/>
</div>
<div class="lyrico-lyrics-wrapper">eppothum nal valvu tharuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppothum nal valvu tharuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">ammachi ammane arulvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammachi ammane arulvai"/>
</div>
<div class="lyrico-lyrics-wrapper">eppothum nal valvu tharuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eppothum nal valvu tharuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">tharuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">polaika vali thedi enga ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="polaika vali thedi enga ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">sami posanu onna senthachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sami posanu onna senthachu"/>
</div>
<div class="lyrico-lyrics-wrapper">pakthan mogam pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakthan mogam pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">magilchi undachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magilchi undachu"/>
</div>
<div class="lyrico-lyrics-wrapper">aadu paadu kondatam podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadu paadu kondatam podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalam koodi varuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam koodi varuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosam kodi tharuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosam kodi tharuthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">saamy theril varuthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saamy theril varuthaiya"/>
</div>
</pre>
