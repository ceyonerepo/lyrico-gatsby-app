---
title: "yaro yaro avan song lyrics"
album: "Bogan"
artist: "D Imman"
lyricist: "Thamarai - Inno Genga"
director: "Aravi"
path: "/albums/bogan-lyrics"
song: "Yaro Yaro Avan"
image: ../../images/albumart/bogan.jpg
date: 2017-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YotqERqT6DU"
type: "mass"
singers:
  - Vijay Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaaro yaaro avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro yaaro avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yakkai paayum naran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yakkai paayum naran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Por aayudham yerkkum nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Por aayudham yerkkum nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindran"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera yudham theerkkum mudivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera yudham theerkkum mudivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theemaikko theervundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemaikko theervundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poimaikko azhivundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poimaikko azhivundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei jeikka vazhikandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei jeikka vazhikandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koor kondu thoor kondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koor kondu thoor kondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendriduuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendriduuuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro yaaroooo avannn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro yaaroooo avannn"/>
</div>
<div class="lyrico-lyrics-wrapper">Yakkai paayum narannn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yakkai paayum narannn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mei enna ariyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei enna ariyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru vizhi thookkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru vizhi thookkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli yerkadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli yerkadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuivandrae thularamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuivandrae thularamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Uru pazhi seetram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uru pazhi seetram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi vaarkkadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi vaarkkadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala yudham nigazhndhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala yudham nigazhndhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siththam thezhivaai yosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siththam thezhivaai yosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mara niththam nerndhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mara niththam nerndhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jiththam jananam aasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jiththam jananam aasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thagappar sor theetchai kaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagappar sor theetchai kaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayangamal poridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayangamal poridu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanthai paar kalagam neekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanthai paar kalagam neekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaiyangal thedidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaiyangal thedidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedidu thedidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedidu thedidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro yaaroooo avannn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro yaaroooo avannn"/>
</div>
<div class="lyrico-lyrics-wrapper">Yakkai paayum narannn…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yakkai paayum narannn…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Por aayudham yerkkum nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Por aayudham yerkkum nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindran"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera yudham theerkkum mudivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera yudham theerkkum mudivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theemaikko theervundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemaikko theervundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poimaikko azhivundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poimaikko azhivundu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei jeikka vazhikandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei jeikka vazhikandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koor kondu thoor kondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koor kondu thoor kondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendriduuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendriduuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaa aaaaaaaaa aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaa aaaaaaaaa aaaaaa"/>
</div>
</pre>
