---
title: "thindaduren naane song lyrics"
album: "Mudhal Idam"
artist: "D. Imman"
lyricist: "Arivumathi"
director: "R. Kumaran"
path: "/albums/mudhal-idam-lyrics"
song: "Thindaduren Naane"
image: ../../images/albumart/mudhal-idam.jpg
date: 2011-08-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/i5Bm_CHGmFo"
type: "love"
singers:
  - Vijay Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enna Nadanthathu Enna Nadanthathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nadanthathu Enna Nadanthathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattendru Oru Nilaa Sattai Pidithathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattendru Oru Nilaa Sattai Pidithathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam Rendu En Paadham Aanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam Rendu En Paadham Aanadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thindaaduren Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaaduren Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindaaduren Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaaduren Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Ithuvo Enna Ithuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ithuvo Enna Ithuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vanthaadutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vanthaadutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaaduthae Oorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaaduthae Oorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaaduthae Oorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaaduthae Oorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Ithuvo Enna Ithuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ithuvo Enna Ithuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Panthaadutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Panthaadutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Yaedho Yaedho Polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Yaedho Yaedho Polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Aagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaalae Naanae Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaalae Naanae Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Paarvai Paarvai Un Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Paarvai Paarvai Un Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Suzhal Kaattraanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhal Kaattraanathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Kaattru Kaattru Suzhal Kaattru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Kaattru Kaattru Suzhal Kaattru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyir Aanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Aanathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Odi Odi Uyir Odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Odi Odi Uyir Odi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegu Tholaivaanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu Tholaivaanathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Kandeegalo Kandeegalo Kandeegalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Kandeegalo Kandeegalo Kandeegalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalidam Thanthegalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalidam Thanthegalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thindaaduren Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaaduren Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindaaduren Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaaduren Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Ithuvo Enna Ithuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ithuvo Enna Ithuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vanthaadutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vanthaadutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Oh Oh Oh Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oh Oh Oh Oh Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Thaalae Thaalae Thaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thaalae Thaalae Thaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Thaalae Thaalae Thaalaelae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thaalae Thaalae Thaalaelae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Ho Nee Paarkka Veyyil Aanaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Nee Paarkka Veyyil Aanaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Ho Naanae Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Ho Naanae Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Vaerkka Pani Aanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Vaerkka Pani Aanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Ho Neeyae Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Ho Neeyae Neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suttaaiyadi Suttaaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttaaiyadi Suttaaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Minsaara Theeyaal Suttaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Minsaara Theeyaal Suttaayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottaayadi Thottaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottaayadi Thottaayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thonnoru Veedham Thottaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thonnoru Veedham Thottaayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pongum Kadal Neethaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongum Kadal Neethaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bommai Nurai Naanthaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bommai Nurai Naanthaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippadi Naan Aanaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippadi Naan Aanaenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaeno Yaeno Sollaediyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaeno Yaeno Sollaediyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaediyo Sollaediyo Sollaediyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaediyo Sollaediyo Sollaediyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thindaaduren Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaaduren Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindaaduren Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaaduren Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Ithuvo Enna Ithuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ithuvo Enna Ithuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vanthaadutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vanthaadutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">La La La La La La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La La La"/>
</div>
<div class="lyrico-lyrics-wrapper">La La La La La La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="La La La La La La"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkunghu Thakkunghu Thak Kunghu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkunghu Thakkunghu Thak Kunghu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkunghu Thakkunghu Thak Kunghu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkunghu Thakkunghu Thak Kunghu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar Kaambil Poovaanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar Kaambil Poovaanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Neeyae Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Neeyae Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Pookka Kaambaanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pookka Kaambaanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Ho Ho Neeyae Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Ho Ho Neeyae Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nindraaiyadi Nindraaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindraaiyadi Nindraaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Moochchoodum Saaithu Nindraaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Moochchoodum Saaithu Nindraaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondenadi Kondenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondenadi Kondenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kan Meedhu Saainthu Kondenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kan Meedhu Saainthu Kondenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae Enai Kaanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingae Enai Kaanenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Angae Nizhal Aanenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae Nizhal Aanenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Kolla Ponenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Kolla Ponenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaeno Yaeno Sollaediyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaeno Yaeno Sollaediyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaediyo Sollaediyo Sollaediyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaediyo Sollaediyo Sollaediyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thindaaduren Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaaduren Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindaaduren Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaaduren Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Ithuvo Enna Ithuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ithuvo Enna Ithuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Vanthaadutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vanthaadutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaaduthae Oorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaaduthae Oorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaaduthae Oorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaaduthae Oorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Ithuvo Enna Ithuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Ithuvo Enna Ithuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Panthaadutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Panthaadutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Yaedho Yaedho Polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Yaedho Yaedho Polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Aagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanaalae Naanae Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanaalae Naanae Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Paarvai Paarvai Un Paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Paarvai Paarvai Un Paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Suzhal Kaattraanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suzhal Kaattraanathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Kaattru Kaattru Suzhal Kaattru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Kaattru Kaattru Suzhal Kaattru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyir Aanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyir Aanathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Odi Odi Uyir Odi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Odi Odi Uyir Odi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegu Tholaivaanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu Tholaivaanathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Kandeegalo Kandeegalo Kandeegalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Kandeegalo Kandeegalo Kandeegalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalidam Thanthegalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalidam Thanthegalo"/>
</div>
</pre>
