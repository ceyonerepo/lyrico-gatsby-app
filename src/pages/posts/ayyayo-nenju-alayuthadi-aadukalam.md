---
title: "ayyayo nenju alayuthadi song lyrics"
album: "Aadukalam"
artist: "G. V. Prakash Kumar"
lyricist: "Snehan"
director: "V. I. S. Jayapalan"
path: "/albums/aadukalam-lyrics"
song: "Ayyayo Nenju Alayuthadi"
image: ../../images/albumart/aadukalam.jpg
date: 2011-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fqUHGtzSv1E"
type: "love"
singers:
  - S.P. Balasubramanyam
  - S.P.B. Charan
  - Prashanthini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ayyayo Nenju Alaiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayo Nenju Alaiyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam Ippo Valayudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Ippo Valayudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Veetil Minnal Oliyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veetil Minnal Oliyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mela Nila Pozhiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mela Nila Pozhiyudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paartha Andha Nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paartha Andha Nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Uranju Pochey Nagarave illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uranju Pochey Nagarave illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinna Sorum Serikkavey Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinna Sorum Serikkavey Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pomaburen Naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pomaburen Naaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaasam Adikkura Kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaasam Adikkura Kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kooda Nadakkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kooda Nadakkiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">En Seval Koovura Saththam Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Seval Koovura Saththam Un"/>
</div>
<div class="lyrico-lyrics-wrapper">Peraa Kekkuradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peraa Kekkuradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayo Nenju Alaiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayo Nenju Alaiyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam Ippo Valaiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Ippo Valaiyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Veetil Minnal Oliyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veetil Minnal Oliyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mela Nila Pozhiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mela Nila Pozhiyudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Thodum Anal Kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Thodum Anal Kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadakkaiyila Poongaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadakkaiyila Poongaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolambi Thavikkudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolambi Thavikkudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiruvizha Kadaigala Poley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruvizha Kadaigala Poley"/>
</div>
<div class="lyrico-lyrics-wrapper">Thenaruren Naandhaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thenaruren Naandhaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhiril Nee Varumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhiril Nee Varumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meraluren Yendhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meraluren Yendhaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kansimittum Theeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kansimittum Theeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Erichuputta Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Erichuputta Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayo Nenju Alaiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayo Nenju Alaiyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam Ippo Valaiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Ippo Valaiyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Veetil Minnal Oliyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veetil Minnal Oliyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mela Nila Pozhiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mela Nila Pozhiyudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Saaral Vizhum Velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Saaral Vizhum Velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mann Vaasam Manam Veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mann Vaasam Manam Veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mochu Thodavey Naan Medhandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mochu Thodavey Naan Medhandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodaiyila Adikkira Mazhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodaiyila Adikkira Mazhaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enna Nenachaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enna Nenachaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeraththula Adikkira Sugatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeraththula Adikkira Sugatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvaiyila Koduthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyila Koduthaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhagaththi Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhagaththi Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Paarvaiyaala Konna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paarvaiyaala Konna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooroda Vaazhura Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooroda Vaazhura Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarodum Serala Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarodum Serala Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyayo Nenju Alaiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayo Nenju Alaiyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam Ippo Valayudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Ippo Valayudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Veetil Minnal Oliyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veetil Minnal Oliyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mela Nila Pozhiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mela Nila Pozhiyudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paartha Andha Nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paartha Andha Nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Uranju Pochey Nagarave illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uranju Pochey Nagarave illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinna Sorum Serikkavey Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinna Sorum Serikkavey Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pomaburen Naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pomaburen Naaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaasam Adikkura Kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaasam Adikkura Kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kooda Nadakkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kooda Nadakkiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">En Seval Koovura Saththam Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Seval Koovura Saththam Un"/>
</div>
<div class="lyrico-lyrics-wrapper">Peraa Kekkuradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peraa Kekkuradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyyayo Nenju Alaiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyayo Nenju Alaiyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam Ippo Valayudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam Ippo Valayudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Veetil Minnal Oliyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veetil Minnal Oliyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mela Nila Pozhiyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mela Nila Pozhiyudhadi"/>
</div>
</pre>
