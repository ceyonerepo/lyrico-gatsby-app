---
title: "usuremo song lyrics"
album: "Guna 369"
artist: "Chaitan Bharadwaj"
lyricist: "kalyan tripuraneni"
director: "Arjun Jandyala"
path: "/albums/guna-369-lyrics"
song: "Usuremo"
image: ../../images/albumart/guna-369.jpg
date: 2019-08-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/FfRgBbQzOqU"
type: "sad"
singers:
  - Mohana Bhogaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Usuremo Irisesindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuremo Irisesindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karusaina Kaalamila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karusaina Kaalamila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidhuresina Jeevithamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidhuresina Jeevithamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Chidhimesenu Cheekatila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chidhimesenu Cheekatila"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhadha Neeru Kuda Bhasha Leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhadha Neeru Kuda Bhasha Leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonthu Moogabhoyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonthu Moogabhoyanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogilinantha Lona Pontha Ninda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogilinantha Lona Pontha Ninda"/>
</div>
<div class="lyrico-lyrics-wrapper">Leni Nindu Vedhanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leni Nindu Vedhanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheradheeyabhothe Saayamemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheradheeyabhothe Saayamemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetthi Needa Cherchenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetthi Needa Cherchenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neramavvaridho Dhaari Thappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neramavvaridho Dhaari Thappi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Chutti Vesenaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Chutti Vesenaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Polikaleni Porala Lonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polikaleni Porala Lonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulu Unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulu Unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Porapadina Pagaku Neney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porapadina Pagaku Neney"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavunu Kaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavunu Kaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishamedhi Cheyakunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishamedhi Cheyakunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoshi Laaga Unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoshi Laaga Unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasamunna Premalannii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasamunna Premalannii"/>
</div>
<div class="lyrico-lyrics-wrapper">Nela Raaluthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nela Raaluthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhu Choopu Leka Chese Saayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhu Choopu Leka Chese Saayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindha Mosi Nindukundha Nyayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindha Mosi Nindukundha Nyayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inni Dhukkulunna Dhikkalakilaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inni Dhukkulunna Dhikkalakilaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhadha Neeru Kuda Bhasha Leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhadha Neeru Kuda Bhasha Leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Gonthu Moogabhoyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gonthu Moogabhoyanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Odhilinantha Lona Pontha Ninda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhilinantha Lona Pontha Ninda"/>
</div>
<div class="lyrico-lyrics-wrapper">Leni Nindu Vedhanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leni Nindu Vedhanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheradhiyyabhothe Saayamemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheradhiyyabhothe Saayamemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetthi Needa Cherchenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetthi Needa Cherchenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neramavvaridho Dhaari Thappi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neramavvaridho Dhaari Thappi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Chutti Vesenaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Chutti Vesenaaa"/>
</div>
</pre>
