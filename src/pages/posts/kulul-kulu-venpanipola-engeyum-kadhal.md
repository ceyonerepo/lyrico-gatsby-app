---
title: "kulul kulu venpanipola song lyrics"
album: "Engeyum Kadhal"
artist: "Harris Jayaraj"
lyricist: "Harris Jeyaraj"
director: "Prabhudeva"
path: "/albums/engeyum-kadhal-lyrics"
song: "Kulul Kulu Venpanipola"
image: ../../images/albumart/engeyum-kadhal.jpg
date: 2011-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SqDvZFz2yog"
type: "love"
singers:
  - Arjun Menon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kulu Kulu Venpani Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulu Kulu Venpani Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Sala Sala Salladaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sala Sala Salladaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Pala Karpanai Modha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Pala Karpanai Modha"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkava Un Ninaippaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkava Un Ninaippaaga"/>
</div>
</pre>
