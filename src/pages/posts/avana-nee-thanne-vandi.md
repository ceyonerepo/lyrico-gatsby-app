---
title: "avana nee song lyrics"
album: "Thanne Vandi"
artist: "Moses"
lyricist: "Kavingar Saarathi"
director: "Manika Vidya"
path: "/albums/thanne-vandi-lyrics"
song: "Avana Nee"
image: ../../images/albumart/thanne-vandi.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/estK7TsNuVY"
type: "happy"
singers:
  - VM Mahalingam
  - Jaya Moorthy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aanga oonga lanthu vitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanga oonga lanthu vitu"/>
</div>
<div class="lyrico-lyrics-wrapper">konga  manga vela kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konga  manga vela kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">avanaa nee avanaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanaa nee avanaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">anjaa oonja kethu vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anjaa oonja kethu vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">maanja maanja noola podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanja maanja noola podum"/>
</div>
<div class="lyrico-lyrics-wrapper">avanaa nee avanaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanaa nee avanaa nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aanga oonga lanthu vitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanga oonga lanthu vitu"/>
</div>
<div class="lyrico-lyrics-wrapper">konga  manga vela kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konga  manga vela kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">avanaa nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanaa nee "/>
</div>
<div class="lyrico-lyrics-wrapper">anjaa oonja kethu vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anjaa oonja kethu vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">maanja maanja noola podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanja maanja noola podum"/>
</div>
<div class="lyrico-lyrics-wrapper">avanaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanaa nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mukkodal mathuraiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mukkodal mathuraiye"/>
</div>
<div class="lyrico-lyrics-wrapper">alagaroda kuthiraiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagaroda kuthiraiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ivanoda twistu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivanoda twistu theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">mannathi mannana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannathi mannana"/>
</div>
<div class="lyrico-lyrics-wrapper">villathi villana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="villathi villana"/>
</div>
<div class="lyrico-lyrics-wrapper">ivanoda designu puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivanoda designu puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">ambulance polavum uthavuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambulance polavum uthavuran"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu underware kiliyavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu underware kiliyavum"/>
</div>
<div class="lyrico-lyrics-wrapper">othaikiran 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othaikiran "/>
</div>
<div class="lyrico-lyrics-wrapper">otikittu vaalavum mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otikittu vaalavum mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">ivana veti kittu odavum pidikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivana veti kittu odavum pidikala"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey hey avanaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey hey avanaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey hey avanaa da nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey hey avanaa da nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">jigaruthanda figure ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jigaruthanda figure ah "/>
</div>
<div class="lyrico-lyrics-wrapper">naanum keka villa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum keka villa da"/>
</div>
<div class="lyrico-lyrics-wrapper">sugar vantha figureum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugar vantha figureum "/>
</div>
<div class="lyrico-lyrics-wrapper">onna paaka villa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onna paaka villa da"/>
</div>
<div class="lyrico-lyrics-wrapper">ac coachil traval pannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ac coachil traval pannum"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">cycle meethu poga kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cycle meethu poga kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">yogam illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yogam illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">sangam hotel piriyaniku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangam hotel piriyaniku"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">rottu kada idly kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rottu kada idly kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">kidaika villa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidaika villa da"/>
</div>
<div class="lyrico-lyrics-wrapper">thiruparankutrathula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruparankutrathula "/>
</div>
<div class="lyrico-lyrics-wrapper">porantha enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porantha enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">thirumbuna pakkam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirumbuna pakkam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">palani iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palani iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">arogara mithichana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arogara mithichana"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey hey avanaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey hey avanaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey hey avanaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey hey avanaa nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uricha kozhi usuru kozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uricha kozhi usuru kozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">rate uh vera da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rate uh vera da"/>
</div>
<div class="lyrico-lyrics-wrapper">samacha pinne taste il
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="samacha pinne taste il"/>
</div>
<div class="lyrico-lyrics-wrapper">rendum onnu paruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendum onnu paruda"/>
</div>
<div class="lyrico-lyrics-wrapper">pombalainga heart kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pombalainga heart kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">duel sim da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="duel sim da"/>
</div>
<div class="lyrico-lyrics-wrapper">jodi maari jodi sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jodi maari jodi sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">naari pochu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naari pochu da"/>
</div>
<div class="lyrico-lyrics-wrapper">seethaiyoda sister ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seethaiyoda sister ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">ippo illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ippo illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">pothaiku aetha kattinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothaiku aetha kattinga"/>
</div>
<div class="lyrico-lyrics-wrapper">than ulla thallu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than ulla thallu da"/>
</div>
<div class="lyrico-lyrics-wrapper">parataiyum vetaiyanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parataiyum vetaiyanum"/>
</div>
<div class="lyrico-lyrics-wrapper">pudikum enaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudikum enaku"/>
</div>
<div class="lyrico-lyrics-wrapper">aduthavan life ah pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduthavan life ah pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">kavala ethuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavala ethuku"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey hey avanaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey hey avanaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey hey avanaa da nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey hey avanaa da nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aanga oonga lanthu vitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanga oonga lanthu vitu"/>
</div>
<div class="lyrico-lyrics-wrapper">konga  manga vela kaatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konga  manga vela kaatum"/>
</div>
<div class="lyrico-lyrics-wrapper">avanaa nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanaa nee "/>
</div>
<div class="lyrico-lyrics-wrapper">anjaa oonja kethu vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anjaa oonja kethu vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">maanja maanja noola podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanja maanja noola podum"/>
</div>
<div class="lyrico-lyrics-wrapper">avanaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avanaa nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mukkodal mathuraiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mukkodal mathuraiye"/>
</div>
<div class="lyrico-lyrics-wrapper">alagaroda kuthiraiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagaroda kuthiraiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ivanoda twistu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivanoda twistu theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">mannathi mannana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannathi mannana"/>
</div>
<div class="lyrico-lyrics-wrapper">villathi villana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="villathi villana"/>
</div>
<div class="lyrico-lyrics-wrapper">ivanoda designu puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivanoda designu puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">ambulance polavum uthavuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ambulance polavum uthavuran"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu underware kiliyavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu underware kiliyavum"/>
</div>
<div class="lyrico-lyrics-wrapper">othaikiran 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othaikiran "/>
</div>
<div class="lyrico-lyrics-wrapper">otikittu vaalavum mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otikittu vaalavum mudiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">ivana veti kittu odavum pidikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivana veti kittu odavum pidikala"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey hey avanaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey hey avanaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey hey avanaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey hey avanaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey hey avanaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey hey avanaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey hey avanaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey hey avanaa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">hey hey hey avanaa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey hey hey avanaa nee"/>
</div>
</pre>
