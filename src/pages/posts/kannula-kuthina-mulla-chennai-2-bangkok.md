---
title: "kannula kuthina mulla song lyrics"
album: "Chennai 2 Bangkok"
artist: "UK Murali"
lyricist: "Gana Praba"
director: "Sathish Santhosh"
path: "/albums/chennai-2-bangkok-lyrics"
song: "Kannula Kuthina Mulla"
image: ../../images/albumart/chennai-2-bangkok.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ib8DWbuZHb8"
type: "gaana"
singers:
  - Gana Praba
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kannula kuthuna mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula kuthuna mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">en kathula viluntha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kathula viluntha solla"/>
</div>
<div class="lyrico-lyrics-wrapper">kannula kuthuna mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula kuthuna mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">en kathula viluntha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kathula viluntha solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennadi sollura oorula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennadi sollura oorula"/>
</div>
<div class="lyrico-lyrics-wrapper">engadi pora caru la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engadi pora caru la"/>
</div>
<div class="lyrico-lyrics-wrapper">ennadi sollura oorula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennadi sollura oorula"/>
</div>
<div class="lyrico-lyrics-wrapper">engadi pora caru la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engadi pora caru la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unga ammava nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga ammava nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">adika porenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adika porenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ava valartha valarpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava valartha valarpu"/>
</div>
<div class="lyrico-lyrics-wrapper">sari illai adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari illai adi"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye unga ammava nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye unga ammava nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">adika porenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adika porenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ava valartha valarpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava valartha valarpu"/>
</div>
<div class="lyrico-lyrics-wrapper">sari illai adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari illai adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannula kuthuna mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula kuthuna mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">en kathula viluntha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kathula viluntha solla"/>
</div>
<div class="lyrico-lyrics-wrapper">kannula kuthuna mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula kuthuna mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">en kathula viluntha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kathula viluntha solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">emma emma yemma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emma emma yemma"/>
</div>
<div class="lyrico-lyrics-wrapper">emma emma yemma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emma emma yemma"/>
</div>
<div class="lyrico-lyrics-wrapper">emma emma emma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emma emma emma"/>
</div>
<div class="lyrico-lyrics-wrapper">emma emma yemma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emma emma yemma"/>
</div>
<div class="lyrico-lyrics-wrapper">emma emma yemma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emma emma yemma"/>
</div>
<div class="lyrico-lyrics-wrapper">emma emma yemma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="emma emma yemma"/>
</div>
<div class="lyrico-lyrics-wrapper">yemma yemma yemmma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yemma yemma yemmma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sambalam kimbalam kaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sambalam kimbalam kaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">vangi vachen paiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vangi vachen paiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">sambalam kimbalam kaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sambalam kimbalam kaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">vangi vachen paiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vangi vachen paiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">apo onnum theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="apo onnum theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">pombala manasu puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pombala manasu puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">apo onnum theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="apo onnum theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">pombala manasu puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pombala manasu puriyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">velicham potu nikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velicham potu nikura"/>
</div>
<div class="lyrico-lyrics-wrapper">medai yeri kuthikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="medai yeri kuthikira"/>
</div>
<div class="lyrico-lyrics-wrapper">elala thinna korangatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elala thinna korangatam"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavaram panna thudikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavaram panna thudikira"/>
</div>
<div class="lyrico-lyrics-wrapper">velicham potu nikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velicham potu nikura"/>
</div>
<div class="lyrico-lyrics-wrapper">medai yeri kuthikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="medai yeri kuthikira"/>
</div>
<div class="lyrico-lyrics-wrapper">elala thinna korangatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elala thinna korangatam"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavaram panna thudikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavaram panna thudikira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en vaalkai irundu pochadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vaalkai irundu pochadi"/>
</div>
<div class="lyrico-lyrics-wrapper">un kanna theranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kanna theranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">velicham kata di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velicham kata di"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye en vaalkai irundu pochadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye en vaalkai irundu pochadi"/>
</div>
<div class="lyrico-lyrics-wrapper">un kanna theranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kanna theranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">velicham kata di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velicham kata di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannula kuthuna mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula kuthuna mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">en kathula viluntha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kathula viluntha solla"/>
</div>
<div class="lyrico-lyrics-wrapper">kannula kuthuna mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula kuthuna mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">en kathula viluntha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kathula viluntha solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathula thupatta parakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathula thupatta parakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathala yenadi marakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathala yenadi marakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathula thupatta parakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathula thupatta parakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathala yenadi marakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathala yenadi marakuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">udhatil saayam poosiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udhatil saayam poosiye"/>
</div>
<div class="lyrico-lyrics-wrapper">bandha vaalkai theduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bandha vaalkai theduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">udhatil saayam poosiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udhatil saayam poosiye"/>
</div>
<div class="lyrico-lyrics-wrapper">bandha vaalkai theduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bandha vaalkai theduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aatam potta kaalkaluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam potta kaalkaluku"/>
</div>
<div class="lyrico-lyrics-wrapper">adanga thane theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adanga thane theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">nottam patha kannuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nottam patha kannuku"/>
</div>
<div class="lyrico-lyrics-wrapper">paaka paaka thudikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaka paaka thudikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">aatam potta kaalkaluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatam potta kaalkaluku"/>
</div>
<div class="lyrico-lyrics-wrapper">adanga thane theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adanga thane theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">nottam patha kannuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nottam patha kannuku"/>
</div>
<div class="lyrico-lyrics-wrapper">paaka paaka thudikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaka paaka thudikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eye brow pottu enna asathuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eye brow pottu enna asathuna"/>
</div>
<div class="lyrico-lyrics-wrapper">ava jeans ah pottu enna kavuthuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava jeans ah pottu enna kavuthuna"/>
</div>
<div class="lyrico-lyrics-wrapper">pechula than enna mayakuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pechula than enna mayakuna"/>
</div>
<div class="lyrico-lyrics-wrapper">ava beach kaatha romba veesuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava beach kaatha romba veesuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannula kuthuna mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula kuthuna mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">en kathula viluntha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kathula viluntha solla"/>
</div>
<div class="lyrico-lyrics-wrapper">kannula kuthuna mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannula kuthuna mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">en kathula viluntha solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kathula viluntha solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennadi sollura oorula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennadi sollura oorula"/>
</div>
<div class="lyrico-lyrics-wrapper">engadi pora caru la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engadi pora caru la"/>
</div>
<div class="lyrico-lyrics-wrapper">ennadi sollura oorula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennadi sollura oorula"/>
</div>
<div class="lyrico-lyrics-wrapper">engadi pora caru la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engadi pora caru la"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unga ammava nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga ammava nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">adika porenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adika porenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ava valartha valarpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava valartha valarpu"/>
</div>
<div class="lyrico-lyrics-wrapper">sari illai adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari illai adi"/>
</div>
<div class="lyrico-lyrics-wrapper">adiye unga ammava nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiye unga ammava nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">adika porenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adika porenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ava valartha valarpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava valartha valarpu"/>
</div>
<div class="lyrico-lyrics-wrapper">sari illai adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari illai adi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unga ammava nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga ammava nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">adika porenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adika porenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">ava valartha valarpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava valartha valarpu"/>
</div>
<div class="lyrico-lyrics-wrapper">sari illai adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari illai adi"/>
</div>
</pre>
