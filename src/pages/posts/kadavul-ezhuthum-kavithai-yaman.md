---
title: "kadavul ezhuthum kavithai song lyrics"
album: "Yaman"
artist: "Vijay Antony"
lyricist: "Ko Sesha"
director: "Jeeva Shankar"
path: "/albums/yaman-lyrics"
song: "Kadavul Ezhuthum Kavithai"
image: ../../images/albumart/yaman.jpg
date: 2017-02-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_5qsDvMOyUg"
type: "love"
singers:
  - Yazin Nizar
  - Janaki Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadavul Ezhudhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Ezhudhum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kavidhaiyum Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhaiyum Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhaiyam Muzhudhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhaiyam Muzhudhum "/>
</div>
<div class="lyrico-lyrics-wrapper">Niraindhuvittaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraindhuvittaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaley Thookangal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaley Thookangal "/>
</div>
<div class="lyrico-lyrics-wrapper">Thool Thoolai Ponadhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thool Thoolai Ponadhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Verukkul Neerai Pol 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verukkul Neerai Pol "/>
</div>
<div class="lyrico-lyrics-wrapper">Num Kadhal Yerudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Num Kadhal Yerudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavul Ezhudhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Ezhudhum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kavidhaiyum Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhaiyum Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhaiyam Muzhudhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhaiyam Muzhudhum "/>
</div>
<div class="lyrico-lyrics-wrapper">Niraindhuvittaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraindhuvittaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai Yellame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Yellame "/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Arugil Yezhaadho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Arugil Yezhaadho "/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai Yellame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Yellame "/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Madiyil Vizhaadho 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Madiyil Vizhaadho "/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkum Enakkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum Enakkum "/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkum Nerukkam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkum Nerukkam "/>
</div>
<div class="lyrico-lyrics-wrapper">Irudhi Varaikkum Irukkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irudhi Varaikkum Irukkum "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravum Pagalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravum Pagalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Urangum Pozhudhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangum Pozhudhum "/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigal Unnaye Azhaikkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigal Unnaye Azhaikkum "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadavul Ezhudhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavul Ezhudhum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kavidhaiyum Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavidhaiyum Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhaiyam Muzhudhum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhaiyam Muzhudhum "/>
</div>
<div class="lyrico-lyrics-wrapper">Niraindhuvittaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraindhuvittaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaayam Yellame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Yellame "/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaarum Unnaale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaarum Unnaale "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Illamal Uyir 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Illamal Uyir "/>
</div>
<div class="lyrico-lyrics-wrapper">Neengum Thannaale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neengum Thannaale "/>
</div>
<div class="lyrico-lyrics-wrapper">Medhuvaai Manadhai Thirandhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuvaai Manadhai Thirandhai "/>
</div>
<div class="lyrico-lyrics-wrapper">Enakul Karaindhaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakul Karaindhaai "/>
</div>
<div class="lyrico-lyrics-wrapper">Uyiril Kalandhaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyiril Kalandhaai "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uravaai Enaku Kidaithaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaai Enaku Kidaithaai "/>
</div>
<div class="lyrico-lyrics-wrapper">Piriya Maruthaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriya Maruthaai "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Endhan Maru Thaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Endhan Maru Thaai"/>
</div>
</pre>
