---
title: "andaga annaga song lyrics"
album: "Aswathama"
artist: "Sricharan Pakala"
lyricist: "V N V Ramesh Kumar"
director: "Ramana Teja"
path: "/albums/aswathama-lyrics"
song: "Andaga Annaga - Yevarani Teliyani Manasulo"
image: ../../images/albumart/aswathama.jpg
date: 2020-01-31
lang: telugu
youtubeLink: "https://www.youtube.com/embed/8Uw4PI3hpk4"
type: "happy"
singers:
  - Vedala Hemachandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yevarani Teliyani Manasulo Mounam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevarani Teliyani Manasulo Mounam"/>
</div>
<div class="lyrico-lyrics-wrapper">charagani Sirulake Chesene Gaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="charagani Sirulake Chesene Gaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisuve ninnu kamminaa velugai nae maranaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisuve ninnu kamminaa velugai nae maranaa"/>
</div>
<div class="lyrico-lyrics-wrapper">everentalaa ninnu tariminaa nee venta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="everentalaa ninnu tariminaa nee venta"/>
</div>
<div class="lyrico-lyrics-wrapper">nenunna aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenunna aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needagaaaaa Thodugaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needagaaaaa Thodugaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andagaaaaa Annagaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andagaaaaa Annagaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neekanulla thadi chusinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekanulla thadi chusinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudichendukai ne unta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudichendukai ne unta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gundellaaa baruventunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gundellaaa baruventunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mosendukai ne unta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mosendukai ne unta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninne chere prasnalake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne chere prasnalake"/>
</div>
<div class="lyrico-lyrics-wrapper">ika nene badhulugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ika nene badhulugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Needagaaaaa Thodugaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needagaaaaa Thodugaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andagaaaaa Annagaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andagaaaaa Annagaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needagaaaaa Thodugaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needagaaaaa Thodugaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andagaaaaa Annagaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andagaaaaa Annagaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Needagaaaaa Thodugaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needagaaaaa Thodugaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andagaaaaa Annagaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andagaaaaa Annagaaaa"/>
</div>
</pre>
