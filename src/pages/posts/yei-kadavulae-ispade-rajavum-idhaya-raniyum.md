---
title: "yei kadavulae song lyrics"
album: "Ispade Rajavum Idhaya Raniyum"
artist: "Sam C. S."
lyricist: "Sam C. S."
director: "Ranjit Jeyakodi"
path: "/albums/idpade-rajavum-idhaya-raniyum-lyrics"
song: "Yei Kadavulae"
image: ../../images/albumart/ispade-rajavum-idhaya-raniyum.jpg
date: 2019-03-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/04E18c0Ez6E"
type: "sad"
singers:
  - Vijay Sethupathi
  - Harish Kalyan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yen Manasa Undaakkuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Manasa Undaakkuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Kadavule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vena Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena Vena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kirukkuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kirukkuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennaala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennaala Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kirukkanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kirukkanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanen Veena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen Veena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhuguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolamburen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolamburen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vethumburen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethumburen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verukkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Manasa Undaakkuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Manasa Undaakkuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Kadavule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vena Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena Vena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kirukkuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kirukkuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennaala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennaala Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kirukkanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kirukkanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanen Veena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen Veena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thariketta Kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thariketta Kaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnaana Vandaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnaana Vandaagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthu Vattam Potten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Vattam Potten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovaana Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaana Neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthu Vaasam Thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu Vaasam Thanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Ulagaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Ulagaththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Maaththi Vechcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maaththi Vechcha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Nenjam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Nenjam Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullukkul Rail Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkul Rail Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannellaam Aaraaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannellaam Aaraaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasellaam Bhoogambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasellaam Bhoogambam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Kaana Yenguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Kaana Yenguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhuguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolamburen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolamburen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vethumburen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethumburen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verukkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Manasa Undaakkuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Manasa Undaakkuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Kadavule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vena Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena Vena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kirukkuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kirukkuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennaala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennaala Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kirukkanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kirukkanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanen Veena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen Veena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennoda Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Vaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirupulli Aagiththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirupulli Aagiththaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sitharundu Pochchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitharundu Pochchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbaala Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaala Neeyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pervaalvu Thanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pervaalvu Thanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verodu Moththatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verodu Moththatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parichchuthaan Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parichchuthaan Pona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Nenaippinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Nenaippinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullukkul Vishamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkul Vishamaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paayuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Ellaam Ranamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Ellaam Ranamaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaame Neeyaagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaame Neeyaagi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Kaana Yenguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Kaana Yenguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhuguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhuguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolamburen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolamburen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vethumburen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethumburen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verukkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thavikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Manasa Undaakkuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Manasa Undaakkuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dei Kadavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dei Kadavule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vena Vena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena Vena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kirukkuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kirukkuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennaala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennaala Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kirukkanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kirukkanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanen Veena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanen Veena"/>
</div>
</pre>
