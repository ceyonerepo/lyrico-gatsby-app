---
title: "mind block song lyrics"
album: "Sarileru Neekevvaru"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani - Devi Sri Prasad"
director: "Anil Ravipudi"
path: "/albums/sarileru-neekevvaru-lyrics"
song: "Mind Block"
image: ../../images/albumart/sarileru-neekevvaru.jpg
date: 2020-01-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/ZBDSNy4Yn9Q"
type: "happy"
singers:
  - Blaaze
  - Ranina Reddy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Eppudu Pant Esevaduu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudu Pant Esevaduu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipudu Lungi Kattadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipudu Lungi Kattadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppudu Shirt Esevaduu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppudu Shirt Esevaduu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipudu Jubba Todigadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipudu Jubba Todigadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chetikemo Mallepoolu Kantikemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chetikemo Mallepoolu Kantikemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallajodu Chuttesi Pettesi Vachesaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallajodu Chuttesi Pettesi Vachesaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">For The First Time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="For The First Time"/>
</div>
<div class="lyrico-lyrics-wrapper">He Is Into Mass Crime
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He Is Into Mass Crime"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Babu Nuvvu Seppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu Nuvvu Seppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enti Aadni Kottamani Dappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enti Aadni Kottamani Dappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvv Kotara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvv Kotara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonwalk Moonwalk
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonwalk Moonwalk"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla Nee Nadaka Chuste Moonwalk
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla Nee Nadaka Chuste Moonwalk"/>
</div>
<div class="lyrico-lyrics-wrapper">Earthquake Earthquake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Earthquake Earthquake"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilla Nuvvu Takutunte Earthquake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilla Nuvvu Takutunte Earthquake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Lip Lona Undi Cupcake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Lip Lona Undi Cupcake"/>
</div>
<div class="lyrico-lyrics-wrapper">Cake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cake"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatlona Undi Milk Shake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatlona Undi Milk Shake"/>
</div>
<div class="lyrico-lyrics-wrapper">Shake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shake"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokulona Undi Kotta Stock
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokulona Undi Kotta Stock"/>
</div>
<div class="lyrico-lyrics-wrapper">Stock
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stock"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Amma Abba Abba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Abba Abba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu Hot Hot Ga Unna Puthareku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Hot Hot Ga Unna Puthareku"/>
</div>
<div class="lyrico-lyrics-wrapper">Reku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reku"/>
</div>
<div class="lyrico-lyrics-wrapper">Muttukunte Jaare Tamaraku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttukunte Jaare Tamaraku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuni Erra Chese Tamalapaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuni Erra Chese Tamalapaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Paku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paku"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Amma Abba Abba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Abba Abba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mind Block Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Block Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind Block Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Block Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Babu Ni Mass Look Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu Ni Mass Look Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind Block Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Block Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind Block Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Block Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Oka Step Este Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Oka Step Este Mind Block"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Babu Nuvvu Seppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu Nuvvu Seppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enti Aadni Kottamani Dappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enti Aadni Kottamani Dappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvv Kotara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvv Kotara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nondra!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nondra!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu Cheera Kattukunte Jaaruthunde Gunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Cheera Kattukunte Jaaruthunde Gunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora Kanti Choope Baggumantu Mande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Kanti Choope Baggumantu Mande"/>
</div>
<div class="lyrico-lyrics-wrapper">Attaa Nuvv Antunte Naakuettago Aytaande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attaa Nuvv Antunte Naakuettago Aytaande"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Katuk Ettukunte Chikat Avtunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Katuk Ettukunte Chikat Avtunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Bottu Pettu Kunte Tellavaaru Tunde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottu Pettu Kunte Tellavaaru Tunde"/>
</div>
<div class="lyrico-lyrics-wrapper">Atta Nuvv Chustunte Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atta Nuvv Chustunte Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vollantha Giliginta Podatande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vollantha Giliginta Podatande"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kallalona Undi Kallu Munta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kallalona Undi Kallu Munta"/>
</div>
<div class="lyrico-lyrics-wrapper">Munta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vompulona Undi Paala Punta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vompulona Undi Paala Punta"/>
</div>
<div class="lyrico-lyrics-wrapper">Punta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sompulona Undi Lokam Antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sompulona Undi Lokam Antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Amma Abba Abba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Abba Abba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mind Block Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Block Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind Block Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Block Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Babu Ni Mass Look Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu Ni Mass Look Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind Block Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Block Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind Block Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Block Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Oka Step Este Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Oka Step Este Mind Block"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Babu Nuvve Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu Nuvve Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Thanjamani Dolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Thanjamani Dolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Nuvv Kottara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Nuvv Kottara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Babu Nuvvu Seppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu Nuvvu Seppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enti Aadni Kottamani Dappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enti Aadni Kottamani Dappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvv Kotara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvv Kotara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Muddhu Muttakuda Mudha Ekadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Muddhu Muttakuda Mudha Ekadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Huggu Antha Kinda Niddar Yettadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Huggu Antha Kinda Niddar Yettadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Itta Nuvv Vuriste Nuvv Korindi Tirusta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itta Nuvv Vuriste Nuvv Korindi Tirusta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Touch Lo Currente Nannu Guchenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Touch Lo Currente Nannu Guchenanta"/>
</div>
 <div class="lyrico-lyrics-wrapper">Pools Scente Muddhu Repenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pools Scente Muddhu Repenanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayte Nuvvu Touch Chests
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayte Nuvvu Touch Chests"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Touch Chesta Ninnu Ededho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Touch Chesta Ninnu Ededho"/>
</div>
<div class="lyrico-lyrics-wrapper">Maikam Lo Munchesta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maikam Lo Munchesta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Buggalona Undi Paala Kova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Buggalona Undi Paala Kova"/>
</div>
<div class="lyrico-lyrics-wrapper">Kova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kova"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Siggulona Undu Aggi Lava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Siggulona Undu Aggi Lava"/>
</div>
<div class="lyrico-lyrics-wrapper">Lava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nadumu Lona Undi Poola Nava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nadumu Lona Undi Poola Nava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nava"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Amma Abba Abba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Amma Abba Abba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mind Block Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Block Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind Block Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Block Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Babu Ni Mass Look Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Babu Ni Mass Look Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind Block Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Block Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind Block Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Block Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Oka Step Este Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Oka Step Este Mind Block"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mind Block Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Block Mind Block"/>
</div>
<div class="lyrico-lyrics-wrapper">Mind Block Mind Block
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind Block Mind Block"/>
</div>
</pre>
