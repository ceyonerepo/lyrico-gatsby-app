---
title: "nee paadhi song lyrics"
album: "Kanavu Variyam"
artist: "Shyam Benjamin"
lyricist: "Arun Chidambaram"
director: "Arun Chidambaram"
path: "/albums/kanavu-variyam-lyrics"
song: "Nee Paadhi"
image: ../../images/albumart/kanavu-variyam.jpg
date: 2017-02-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RepIXmgepyQ"
type: "love"
singers:
  - Guna
  - Mahalakshmi Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee paadhi naan paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paadhi naan paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enil yaar sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enil yaar sonnadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamum nee endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamum nee endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir dhaan sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir dhaan sonnadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee paadhi naan paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paadhi naan paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enil yaar sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enil yaar sonnadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamum nee endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamum nee endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir dhaan sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir dhaan sonnadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanum mannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanum mannum"/>
</div>
<div class="lyrico-lyrics-wrapper">Polae naamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polae naamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazha vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen dhaan endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen dhaan endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevvaanam kanneerai sindhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvaanam kanneerai sindhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee paadhi naan paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paadhi naan paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enil yaar sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enil yaar sonnadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamum nee endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamum nee endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir dhaan sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir dhaan sonnadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaikkum nodiyil unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaikkum nodiyil unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasikka katraval naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikka katraval naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaigal indri unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaigal indri unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbum aandaalum naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbum aandaalum naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechchukkal podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechchukkal podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththangal vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththangal vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindaadi pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaadi pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verukkul paayum neeraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verukkul paayum neeraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukkul nuzhaigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkul nuzhaigiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochai pol ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochai pol ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnul izhuththu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnul izhuththu "/>
</div>
<div class="lyrico-lyrics-wrapper">nee kadathinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kadathinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee paadhi naan paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paadhi naan paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enil yaar sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enil yaar sonnadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamum nee endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamum nee endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir dhaan sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir dhaan sonnadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee paadhi naan paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paadhi naan paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enil yaar sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enil yaar sonnadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamum nee endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamum nee endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir dhaan sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir dhaan sonnadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppodhum nijangal thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum nijangal thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalai vaazha seiyyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalai vaazha seiyyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhal nee illai endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal nee illai endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">En nijam poiththu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nijam poiththu pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilvaazhkai kaanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilvaazhkai kaanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalyaanam vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalyaanam vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjae"/>
</div>
<div class="lyrico-lyrics-wrapper">Menmelum konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menmelum konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kenja vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kenja vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kaalai thaenkuzhalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaalai thaenkuzhalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttri naan varugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suttri naan varugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Meiyaana undhan anbil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meiyaana undhan anbil"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbi naan veezhgiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbi naan veezhgiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee paadhi naan paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paadhi naan paadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enil yaar sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enil yaar sonnadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaamum nee endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaamum nee endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir dhaan sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir dhaan sonnadhu"/>
</div>
</pre>
