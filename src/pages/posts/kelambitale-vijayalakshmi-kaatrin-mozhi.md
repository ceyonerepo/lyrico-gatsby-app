---
title: "kelambitale vijayalakshmi song lyrics"
album: "Kaatrin Mozhi"
artist: "AH Kaashif"
lyricist: "Madhan Karky"
director: "Radha Mohan"
path: "/albums/kaatrin-mozhi-lyrics"
song: "Kelambitale Vijayalakshmi"
image: ../../images/albumart/kaatrin-mozhi.jpg
date: 2018-11-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-swoBo2ejbI"
type: "happy"
singers:
  - Nakul Abhyankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kelambittaalae hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae ho hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae ho hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae vijayalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae vijayalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambittaalae hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae ho hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae ho hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae vijayalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae vijayalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagagum paadhaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagagum paadhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva nadanthida thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva nadanthida thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Niram maarum vaanamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niram maarum vaanamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva sirichida thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva sirichida thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavellam poopathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavellam poopathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva parichida thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva parichida thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru bhoomi pothuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru bhoomi pothuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva cheyichida thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva cheyichida thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothuvavae iva thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothuvavae iva thangam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru potti vandhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru potti vandhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Singam singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singam singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum cheyikaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum cheyikaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey thoonga maattaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey thoonga maattaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai potta anai potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai potta anai potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva sikkikolla siththoda illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva sikkikolla siththoda illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti thokki mutti pova
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti thokki mutti pova"/>
</div>
<div class="lyrico-lyrics-wrapper">Payum kaattaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payum kaattaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambittaalae hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae ho hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae ho hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae vijayalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae vijayalakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae vijayalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae vijayalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambittaalae hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae ho hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae ho hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae vijayalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae vijayalakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae vijayalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae vijayalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh…ilamai uraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh…ilamai uraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagu nirantharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu nirantharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalatha niruththida pazhagikitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalatha niruththida pazhagikitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seelaikku pozhivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seelaikku pozhivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagaikku vilambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaikku vilambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Seruppum serappurum iva aninja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seruppum serappurum iva aninja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaalae kodi kodiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalae kodi kodiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Baashai pesi mirattiduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baashai pesi mirattiduva"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil kodi kodiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil kodi kodiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhoshaththa nirachiduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhoshaththa nirachiduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi kodiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi kodiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukulla kanavirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla kanavirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha thediyae thediyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha thediyae thediyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhillaaga kelambitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhillaaga kelambitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhillaaga kelambitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhillaaga kelambitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambittaalae hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae ho hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae ho hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae vijayalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae vijayalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambittaalae hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae vijayalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae vijayalakshmi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelambittaalae hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelambittaalae vijayalakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelambittaalae vijayalakshmi"/>
</div>
</pre>
