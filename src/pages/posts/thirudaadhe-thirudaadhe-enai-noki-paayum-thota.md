---
title: "thirudaadhe thirudaadhe song lyrics"
album: "Enai Noki Paayum Thota"
artist: "Darbuka Siva"
lyricist: "Thamarai - Aaryan Dinesh Kanagaratnam"
director: "Gautham Vasudev Menon"
path: "/albums/enai-noki-paayum-thota-lyrics"
song: "Thirudaadhe Thirudaadhe"
image: ../../images/albumart/enai-noki-paayum-thota.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SZzMSxMF_xA"
type: "love"
singers:
  - Darbuka Siva
  - Karthik
  - Aaryan Dinesh Kanagaratnam
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Freak Out Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Freak Out Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Freak Out Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Freak Out Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Freak Out Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Freak Out Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Freak Out Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Freak Out Oh Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirudaadhae Thirudaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudaadhae Thirudaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Nenjam Vendum Yenaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Nenjam Vendum Yenaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudaadhae Varudaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudaadhae Varudaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vizhigal Kulirum Neruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vizhigal Kulirum Neruppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhu Ullam Enadhu Ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhu Ullam Enadhu Ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Kallam Ippo Yedhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Kallam Ippo Yedhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhaalum Irundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhaalum Irundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Arugil Vandhaal Kirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Arugil Vandhaal Kirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viral Nagangalai Kadithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Nagangalai Kadithaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Veembu Vandhaalum Maraithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veembu Vandhaalum Maraithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyilum Konjam Nadithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyilum Konjam Nadithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyil Naan Thudithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyil Naan Thudithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu Suzhndhathae Theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Suzhndhathae Theriyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavu Vandhadhum Theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Vandhadhum Theriyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyae Ennathaan Nadanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyae Ennathaan Nadanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvum Puriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvum Puriyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu Suzhndhathae Theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Suzhndhathae Theriyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavu Vandhadhum Theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Vandhadhum Theriyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyae Ennathaan Nadanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyae Ennathaan Nadanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvum Puriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvum Puriyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Freak Out Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Freak Out Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Freak Out Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Freak Out Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Freak Out Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Freak Out Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Freak Out Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Freak Out Oh Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirudaadhae Thirudaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudaadhae Thirudaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Nenjam Vendum Yenaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Nenjam Vendum Yenaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Varudaadhae Varudaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varudaadhae Varudaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vizhigal Kulirum Neruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vizhigal Kulirum Neruppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhu Ullam Enadhu Ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhu Ullam Enadhu Ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Kallam Ippo Yedhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Kallam Ippo Yedhuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundhaalum Irundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundhaalum Irundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Arugil Vandhaal Kirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Arugil Vandhaal Kirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viral Nagangalai Kadithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Nagangalai Kadithaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Veembu Vandhaalum Maraithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veembu Vandhaalum Maraithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyilum Konjam Nadithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyilum Konjam Nadithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyil Naan Thudithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyil Naan Thudithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu Suzhndhathae Theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Suzhndhathae Theriyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavu Vandhadhum Theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Vandhadhum Theriyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyae Ennathaan Nadanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyae Ennathaan Nadanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvum Puriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvum Puriyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu Suzhndhathae Theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Suzhndhathae Theriyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavu Vandhadhum Theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Vandhadhum Theriyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyae Ennathaan Nadanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyae Ennathaan Nadanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvum Puriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvum Puriyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Engum Unai Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Engum Unai Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi Rendil Kolaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Rendil Kolaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavu Engum Sirikindrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavu Engum Sirikindrai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamalaalaiyam Kandhaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamalaalaiyam Kandhaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Badhil Solla Theriyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhil Solla Theriyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadumaari Sirikindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumaari Sirikindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethil Sendru Mudindhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethil Sendru Mudindhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Seri Endru Ninaikindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seri Endru Ninaikindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragasiya Vizhiyil Theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiya Vizhiyil Theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasanaigal Methuvaai Puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasanaigal Methuvaai Puriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaiyudhu Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaiyudhu Idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Therindhae Thaan Nirkindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Therindhae Thaan Nirkindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aruginil Varava Varava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginil Varava Varava"/>
</div>
<div class="lyrico-lyrics-wrapper">Anumathi Perava Perava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anumathi Perava Perava"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaithida Ninaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaithida Ninaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Thavara Thavara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Thavara Thavara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu Suzhndhathae Theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Suzhndhathae Theriyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavu Vandhadhum Theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Vandhadhum Theriyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veliyae Ennathaan Nadanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veliyae Ennathaan Nadanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvum Puriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvum Puriyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathavu Thirandhathum Theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathavu Thirandhathum Theriyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatru Vandathum Theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatru Vandathum Theriyaadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavu Seithu Yaar Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavu Seithu Yaar Ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvum Theriyaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvum Theriyaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gotta Beach Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gotta Beach Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Machi Aadi Thakaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi Aadi Thakaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Lady Killadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lady Killadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Kuthaadi Semma Kacheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Kuthaadi Semma Kacheri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkathula Juice Glass Iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkathula Juice Glass Iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotukatha Urugavum Iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotukatha Urugavum Iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">Imma Gonna Take You Down
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imma Gonna Take You Down"/>
</div>
<div class="lyrico-lyrics-wrapper">Urukulla Disco Bounce
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urukulla Disco Bounce"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beach Party Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beach Party Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanapina Party Pogalama Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanapina Party Pogalama Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">Beach Party Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beach Party Girl"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanapina Party Pogalama Girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanapina Party Pogalama Girl"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama Go With The Player
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Go With The Player"/>
</div>
<div class="lyrico-lyrics-wrapper">Machi Vaadi Vaadi Aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi Vaadi Vaadi Aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnadi Pogum Pengal Manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnadi Pogum Pengal Manathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathil Thuvi Thalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manathil Thuvi Thalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennadi Pinnadi Thalladi Pinnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadi Pinnadi Thalladi Pinnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi Aadi Paadi Paadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi Aadi Paadi Paadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pengal Manathil Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pengal Manathil Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannala Kaatti Paesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala Kaatti Paesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thali Kollathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thali Kollathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Jadai Kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Jadai Kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Vittu Pogathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Vittu Pogathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nana Nani Nani Nani Nani Nani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana Nani Nani Nani Nani Nani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana Nani Nani Nani Nani Nani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana Nani Nani Nani Nani Nani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama Campagene’a Thookki Tha Melatha Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Campagene’a Thookki Tha Melatha Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannalae Tha Viththai Tha Kaatti Tha Odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannalae Tha Viththai Tha Kaatti Tha Odu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelivu Sulivu Ethuvum Edupadathu Thambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelivu Sulivu Ethuvum Edupadathu Thambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamoochi Aattam Vilaiyadalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamoochi Aattam Vilaiyadalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Unna Nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Unna Nambi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ya Ya Ya Ya Ya Ya Ya Ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ya Ya Ya Ya Ya Ya Ya Ya"/>
</div>
<div class="lyrico-lyrics-wrapper">yeah Yeah Yeah Yeah Yeah Yeah Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeah Yeah Yeah Yeah Yeah Yeah Yeah Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">yeah Yeah Yeah Yeah Yeah Yeah Yeah Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeah Yeah Yeah Yeah Yeah Yeah Yeah Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
<div class="lyrico-lyrics-wrapper">Bounce Gonna Party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bounce Gonna Party"/>
</div>
</pre>
