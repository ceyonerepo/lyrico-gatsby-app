---
title: "zindagi meri shehnai song lyrics"
album: "Punya Paap"
artist: "Stunnah Beatz"
lyricist: "DIVINE"
director: "Gil Green"
path: "/albums/punya-paap-lyrics"
song: "Zindagi Meri Shehnai"
image: ../../images/albumart/punya-paap.jpg
date: 2020-12-03
lang: hindi
youtubeLink: "https://www.youtube.com/embed/oYMqW5L52pM"
type: "happy"
singers:
  - DIVINE
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haan! Haan!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan! Haan!"/>
</div>
<div class="lyrico-lyrics-wrapper">Gully gang!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gully gang!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zindagi meri shehnai hum baje in raston mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi meri shehnai hum baje in raston mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise hum shehnai iss dard ko to sehna hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise hum shehnai iss dard ko to sehna hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poori zindagi bas dard dard dard humne jhela hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poori zindagi bas dard dard dard humne jhela hi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zindagi meri shehnai hum baje in raston mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi meri shehnai hum baje in raston mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise hum shehnai iss dard ko to sehna hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise hum shehnai iss dard ko to sehna hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poori zindagi bas dard dard dard humne jhela hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poori zindagi bas dard dard dard humne jhela hi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hum baje in raston mein shehnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum baje in raston mein shehnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jahan pyaar wahan nafrat to rehna hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jahan pyaar wahan nafrat to rehna hi"/>
</div>
<div class="lyrico-lyrics-wrapper">In sanpon ke beech mujhko rehna nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In sanpon ke beech mujhko rehna nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo fame chahte mujhe dena nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo fame chahte mujhe dena nahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaali raatein aaj kal zyada sota nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali raatein aaj kal zyada sota nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh diss rappers mumma ke sofa pe hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh diss rappers mumma ke sofa pe hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Insta ke fans inke show par nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Insta ke fans inke show par nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hum bajte hum baj rahe haan sober nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hum bajte hum baj rahe haan sober nahi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ro kar hum aate tab hasne lagte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ro kar hum aate tab hasne lagte"/>
</div>
<div class="lyrico-lyrics-wrapper">Haskar hum jaate tab rone lagte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haskar hum jaate tab rone lagte"/>
</div>
<div class="lyrico-lyrics-wrapper">Punya paap humpe dono yeh jachte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punya paap humpe dono yeh jachte"/>
</div>
<div class="lyrico-lyrics-wrapper">Insan hai ghaltiyan hone lagte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Insan hai ghaltiyan hone lagte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zindagi meri shehnai hum baje in raston mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi meri shehnai hum baje in raston mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise hum shehnai iss dard ko to sehna hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise hum shehnai iss dard ko to sehna hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poori zindagi bas dard dard dard humne jhela hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poori zindagi bas dard dard dard humne jhela hi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zindagi meri shehnai hum baje in raston mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi meri shehnai hum baje in raston mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise hum shehnai iss dard ko to sehna hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise hum shehnai iss dard ko to sehna hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poori zindagi bas dard dard dard humne jhela hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poori zindagi bas dard dard dard humne jhela hi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shukriya!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shukriya!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pehle bahut maine dukh jiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehle bahut maine dukh jiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bhi kiya mann se baaki maine sukh diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bhi kiya mann se baaki maine sukh diya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere dost mere headphones pe sun'ne wale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere dost mere headphones pe sun'ne wale"/>
</div>
<div class="lyrico-lyrics-wrapper">Gg ke soldiers ko shukriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gg ke soldiers ko shukriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akela par lagta hai army ke sath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akela par lagta hai army ke sath"/>
</div>
<div class="lyrico-lyrics-wrapper">Savera ab hota hai studio se bahar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savera ab hota hai studio se bahar"/>
</div>
<div class="lyrico-lyrics-wrapper">Hustle se victory lap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hustle se victory lap"/>
</div>
<div class="lyrico-lyrics-wrapper">Khada kiya maine kutub minar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khada kiya maine kutub minar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aitihasik kitaab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aitihasik kitaab"/>
</div>
<div class="lyrico-lyrics-wrapper">Panne pe panne pe panna bhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panne pe panne pe panna bhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Gir gir ke phir hi to chadhne laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gir gir ke phir hi to chadhne laga"/>
</div>
<div class="lyrico-lyrics-wrapper">Rap ke siway mujhko mann nahi laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rap ke siway mujhko mann nahi laga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zindagi meri shehnai hum baje in raston mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi meri shehnai hum baje in raston mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise hum shehnai iss dard ko to sehna hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise hum shehnai iss dard ko to sehna hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poori zindagi bas dard dard dard humne jhela hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poori zindagi bas dard dard dard humne jhela hi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zindagi meri shehnai hum baje in raston mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagi meri shehnai hum baje in raston mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaise hum shehnai iss dard ko to sehna hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaise hum shehnai iss dard ko to sehna hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Poori zindagi bas dard dard dard humne jhela hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poori zindagi bas dard dard dard humne jhela hi"/>
</div>
</pre>
