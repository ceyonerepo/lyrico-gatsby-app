---
title: "heartukulla song lyrics"
album: "Gulaebaghavali"
artist: "Vivek – Mervin"
lyricist: "Pa. Vijay"
director: "Kalyaan"
path: "/albums/gulaebaghavali-lyrics"
song: "Heartukulla"
image: ../../images/albumart/gulaebaghavali.jpg
date: 2018-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OOP4FDO1XeU"
type: "love"
singers:
  - Nakash Aziz
  - Sanjana Diwakar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hoo hei hoo hei hoo hei hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hoo hei hoo hei hoo hei hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei enakku unmela than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei enakku unmela than"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku en mela than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku en mela than"/>
</div>
<div class="lyrico-lyrics-wrapper">Etho onnae onnae irukku irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etho onnae onnae irukku irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Lite ah puththi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lite ah puththi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Loosa suthi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loosa suthi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ok sollariya enakku enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok sollariya enakku enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heartkulla pachakuthiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartkulla pachakuthiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaatha en pachakiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaatha en pachakiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesama nee patha vachiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesama nee patha vachiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu konjam pacha pacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu konjam pacha pacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachakodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachakodiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heartkulla pachakuthiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartkulla pachakuthiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaatha en pachakiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaatha en pachakiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesama nee patha vachiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesama nee patha vachiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu konjam pacha pacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu konjam pacha pacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachakodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachakodiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pachakiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pachakiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En pacha pacha pachakodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pacha pacha pachakodiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Will u be my honey bunny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Will u be my honey bunny"/>
</div>
<div class="lyrico-lyrics-wrapper">People here are very funny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="People here are very funny"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuppa enna ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuppa enna ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa ma honey bunny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa ma honey bunny"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Will u be my honey bunny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Will u be my honey bunny"/>
</div>
<div class="lyrico-lyrics-wrapper">People here are very funny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="People here are very funny"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuppa enna ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuppa enna ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa ma honey bunny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa ma honey bunny"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En firstu looku layae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En firstu looku layae"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura vitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Best friend ah ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Best friend ah ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhatti vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhatti vitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyo paavamunnu ok pandren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo paavamunnu ok pandren"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa illa illa think panni soldren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa illa illa think panni soldren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adadaa kannamma enna ponnama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadaa kannamma enna ponnama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hotu watera than heartkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hotu watera than heartkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Uththittiyae maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uththittiyae maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa vaa sellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa sellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Feelings ennama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feelings ennama"/>
</div>
<div class="lyrico-lyrics-wrapper">Love soldrithuku logic 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love soldrithuku logic "/>
</div>
<div class="lyrico-lyrics-wrapper">ellam Paakuriyae maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam Paakuriyae maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku unmela than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku unmela than"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku en mela than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku en mela than"/>
</div>
<div class="lyrico-lyrics-wrapper">Etho onnae onnae irukku irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etho onnae onnae irukku irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Lite ah puththi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lite ah puththi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Loosa suthi vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loosa suthi vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ok solluvenae unakku unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ok solluvenae unakku unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heartkulla pachakuthiyaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartkulla pachakuthiyaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaatha en pachakiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaatha en pachakiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey heartkulla pachakuthiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey heartkulla pachakuthiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaatha en pachakiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaatha en pachakiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesama nee patha vachiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesama nee patha vachiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu konjam pacha pacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu konjam pacha pacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachakodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachakodiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heartkulla pachakuthiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartkulla pachakuthiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaatha en pachakiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaatha en pachakiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesama nee patha vachiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesama nee patha vachiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu konjam pacha pacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu konjam pacha pacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pachakodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachakodiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pachakiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pachakiliyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pacha pacha pachakodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pacha pacha pachakodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Will u be my honey bunny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Will u be my honey bunny"/>
</div>
<div class="lyrico-lyrics-wrapper">People here are very funny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="People here are very funny"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuppa enna ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuppa enna ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa ma honey bunny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa ma honey bunny"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pacha pacha pachakodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pacha pacha pachakodiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Will u be my honey bunny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Will u be my honey bunny"/>
</div>
<div class="lyrico-lyrics-wrapper">People here are very funny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="People here are very funny"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuppa enna ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuppa enna ini"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa ma honey bunny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa ma honey bunny"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pacha pacha pachakodiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pacha pacha pachakodiyae"/>
</div>
</pre>
