---
title: "vijayam song lyrics"
album: "George Reddy"
artist: "Suresh Bobbili"
lyricist: "Chaithanya Prasad"
director: "Jeevan Reddy"
path: "/albums/george-reddy-lyrics"
song: "Vijayam"
image: ../../images/albumart/george-reddy.jpg
date: 2019-11-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/9XXJkB0jhCk"
type: "happy"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye Samaram Manadi Ayite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Samaram Manadi Ayite"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijayam Manade Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijayam Manade Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalale Kadali Odilo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalale Kadali Odilo "/>
</div>
<div class="lyrico-lyrics-wrapper">Alalai Egase Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alalai Egase Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Vidividi Adugulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Vidividi Adugulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Okatai Parugulu Pedite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatai Parugulu Pedite"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagamanta Mana Vente
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagamanta Mana Vente"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayamantu Sagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayamantu Sagada"/>
</div>
<div class="lyrico-lyrics-wrapper">Prati Madi Tana Gadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prati Madi Tana Gadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidichi Bayatiki Raga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidichi Bayatiki Raga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Hrudayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Hrudayam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Visalame Kada Rara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Visalame Kada Rara "/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Modati Adugu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Modati Adugu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ipude Padindi Sodara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipude Padindi Sodara"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Jagati Kosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Jagati Kosam "/>
</div>
<div class="lyrico-lyrics-wrapper">Sayyantu Sagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayyantu Sagada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu Ganna Kalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu Ganna Kalam "/>
</div>
<div class="lyrico-lyrics-wrapper">Mundundi Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundundi Chudara"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupu Pilupu Vintu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupu Pilupu Vintu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindesey Lera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindesey Lera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Kusinta Nela Lenivadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Kusinta Nela Lenivadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Kulodayyera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulodayyera"/>
</div>
<div class="lyrico-lyrics-wrapper">Sannakaru Raitu Kuda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannakaru Raitu Kuda "/>
</div>
<div class="lyrico-lyrics-wrapper">Kanniru Ayera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanniru Ayera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasta Jivi Kadupukinta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasta Jivi Kadupukinta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudaina Ledura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudaina Ledura"/>
</div>
<div class="lyrico-lyrics-wrapper">Chemata Chukka Viluva Leni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chemata Chukka Viluva Leni "/>
</div>
<div class="lyrico-lyrics-wrapper">Sarukainadira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarukainadira"/>
</div>
<div class="lyrico-lyrics-wrapper">Adollapai Arallu Endira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adollapai Arallu Endira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni Vidyake Udyogamedira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni Vidyake Udyogamedira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Modati Adugu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Modati Adugu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ipude Padindi Sodara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipude Padindi Sodara"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Jagati Kosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Jagati Kosam "/>
</div>
<div class="lyrico-lyrics-wrapper">Sayyantu Sagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayyantu Sagada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu Ganna Kalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu Ganna Kalam "/>
</div>
<div class="lyrico-lyrics-wrapper">Mundundi Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundundi Chudara"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupu Pilupu Vintu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupu Pilupu Vintu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindesey Lera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindesey Lera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Adavi Talli Biddalanta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Adavi Talli Biddalanta "/>
</div>
<div class="lyrico-lyrics-wrapper">Alladi Poyera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alladi Poyera"/>
</div>
<div class="lyrico-lyrics-wrapper">Palletalli Talladilli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palletalli Talladilli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghollumannayira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghollumannayira"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagara Jivi Naddi Virigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagara Jivi Naddi Virigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagubate Ayyera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagubate Ayyera"/>
</div>
<div class="lyrico-lyrics-wrapper">Annapurna Bharata Mata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annapurna Bharata Mata "/>
</div>
<div class="lyrico-lyrics-wrapper">Akrosinchera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akrosinchera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Desame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Desame"/>
</div>
<div class="lyrico-lyrics-wrapper">Adesamichera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adesamichera"/>
</div>
<div class="lyrico-lyrics-wrapper">O Marpukai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Marpukai "/>
</div>
<div class="lyrico-lyrics-wrapper">Sankalpamichera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankalpamichera"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugu Adugu Kalipi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugu Adugu Kalipi "/>
</div>
<div class="lyrico-lyrics-wrapper">Chindesi Aadaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindesi Aadaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakala Jagati Kosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakala Jagati Kosam "/>
</div>
<div class="lyrico-lyrics-wrapper">Sayyantu Sagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayyantu Sagada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu Ganna Kalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu Ganna Kalam "/>
</div>
<div class="lyrico-lyrics-wrapper">Mundundi Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundundi Chudara"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupu Pilupu Vintu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupu Pilupu Vintu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chindesey Lera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chindesey Lera"/>
</div>
</pre>
