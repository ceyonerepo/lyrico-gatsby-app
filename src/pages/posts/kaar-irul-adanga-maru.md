---
title: "kaar irul song lyrics"
album: "Adanga Maru"
artist: "Sam CS"
lyricist: "Sam CS"
director: "Karthik Thangavel"
path: "/albums/adanga-maru-lyrics"
song: "Kaar Irul"
image: ../../images/albumart/adanga-maru.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_o5p0MtLZUQ"
type: "mass"
singers:
  - Sivam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Daata daata daadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daata daata daadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaar irul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaar irul"/>
</div>
<div class="lyrico-lyrics-wrapper">Paar ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paar ithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orr vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thediyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thediyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaligai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaligai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atharma valaigal pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharma valaigal pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanai thedi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanai thedi "/>
</div>
<div class="lyrico-lyrics-wrapper">alaigindren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaigindren naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinathai thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinathai thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kazhugin kan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kazhugin kan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thurathinaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thurathinaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vida maaten naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vida maaten naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolaikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaikazhithidum orr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaikazhithidum orr"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaikalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaikalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulangidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulangidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraivinil thezhinthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraivinil thezhinthidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadayam maaya villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadayam maaya villai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigal yethumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigal yethumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyum maayavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyum maayavalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaigal thooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaigal thooram illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adanga maruthu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanga maruthu vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Atharmam udaithu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharmam udaithu vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinava yethumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinava yethumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththu meeri ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththu meeri ezhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadayam maaya villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadayam maaya villai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigal yethumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigal yethumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaiyum maayavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaiyum maayavalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaigal thooram illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaigal thooram illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adanga maruthu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanga maruthu vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Atharmam udaithu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atharmam udaithu vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinava yethumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinava yethumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththu meeri ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththu meeri ezhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daata daata daadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daata daata daadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Daata daata daadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daata daata daadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Daata daata daadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daata daata daadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Daata daata daadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daata daata daadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Daata daata daadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daata daata daadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Daata daata daadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daata daata daadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugamudiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugamudiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayavar inam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayavar inam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaitherivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaitherivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Udanae ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udanae ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugamudiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugamudiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kayavar inam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayavar inam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaitherivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaitherivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Udanae ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udanae ivan"/>
</div>
</pre>
