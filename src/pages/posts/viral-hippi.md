---
title: "viral song lyrics"
album: "Hippi"
artist: "Nivas K. Prasanna"
lyricist: "Anantha Sriram"
director: "Krishna"
path: "/albums/hippi-lyrics"
song: "Viral"
image: ../../images/albumart/hippi.jpg
date: 2019-06-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/-WjgGdEofTM"
type: "happy"
singers:
  - Raghu Dixit
  - Vishnupriya
  - Christopher Stanley
  - MC Vickey
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">mana selfie viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana selfie viral"/>
</div>
<div class="lyrico-lyrics-wrapper">mana smiley viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana smiley viral"/>
</div>
<div class="lyrico-lyrics-wrapper">manamantha vese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manamantha vese"/>
</div>
<div class="lyrico-lyrics-wrapper">vesallane viral viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vesallane viral viral"/>
</div>
<div class="lyrico-lyrics-wrapper">mana gola viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana gola viral"/>
</div>
<div class="lyrico-lyrics-wrapper">mana godavaa viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana godavaa viral"/>
</div>
<div class="lyrico-lyrics-wrapper">manamantha chese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manamantha chese"/>
</div>
<div class="lyrico-lyrics-wrapper">motham rachha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="motham rachha "/>
</div>
<div class="lyrico-lyrics-wrapper">viral raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral raa"/>
</div>
<div class="lyrico-lyrics-wrapper">viral avuthadhi choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral avuthadhi choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thalapai color edhaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thalapai color edhaina"/>
</div>
<div class="lyrico-lyrics-wrapper">viral kaanidhi leethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral kaanidhi leethu"/>
</div>
<div class="lyrico-lyrics-wrapper">lokaana arey viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lokaana arey viral"/>
</div>
<div class="lyrico-lyrics-wrapper">avuthayi choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avuthayi choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee medapai tattoolaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee medapai tattoolaina"/>
</div>
<div class="lyrico-lyrics-wrapper">gaana bhajaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaana bhajaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mana khaana peena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana khaana peena"/>
</div>
<div class="lyrico-lyrics-wrapper">viral nanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral nanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">viral oo heroinnue
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral oo heroinnue"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu geetithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu geetithe"/>
</div>
<div class="lyrico-lyrics-wrapper">ho yaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ho yaye"/>
</div>
<div class="lyrico-lyrics-wrapper">viral oo item beauty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral oo item beauty"/>
</div>
<div class="lyrico-lyrics-wrapper">noru jaarithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="noru jaarithe"/>
</div>
<div class="lyrico-lyrics-wrapper">ho yaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ho yaye"/>
</div>
<div class="lyrico-lyrics-wrapper">vaaram vaaaram naakai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaaram vaaaram naakai "/>
</div>
<div class="lyrico-lyrics-wrapper">oo viral video
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo viral video"/>
</div>
<div class="lyrico-lyrics-wrapper">vasthoo vasthoo unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasthoo vasthoo unte"/>
</div>
<div class="lyrico-lyrics-wrapper">aa kickke verayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa kickke verayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">ee vaaram mana party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee vaaram mana party"/>
</div>
<div class="lyrico-lyrics-wrapper">yedholaa yedholaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedholaa yedholaa"/>
</div>
<div class="lyrico-lyrics-wrapper">avvaloy viral viral viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avvaloy viral viral viral"/>
</div>
<div class="lyrico-lyrics-wrapper">hippi hippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hippi hippi"/>
</div>
<div class="lyrico-lyrics-wrapper">let's be peppy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="let's be peppy"/>
</div>
<div class="lyrico-lyrics-wrapper">neeke nuvvu vvip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeke nuvvu vvip"/>
</div>
<div class="lyrico-lyrics-wrapper">hippi hippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hippi hippi"/>
</div>
<div class="lyrico-lyrics-wrapper">let's be peppy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="let's be peppy"/>
</div>
<div class="lyrico-lyrics-wrapper">neethe neethe ts ap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethe neethe ts ap"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">baby lemme dance tonight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baby lemme dance tonight"/>
</div>
<div class="lyrico-lyrics-wrapper">we've been partin' harder
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we've been partin' harder"/>
</div>
<div class="lyrico-lyrics-wrapper">go flexin dance floor blazin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="go flexin dance floor blazin"/>
</div>
<div class="lyrico-lyrics-wrapper">oh just gimme some more
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh just gimme some more"/>
</div>
<div class="lyrico-lyrics-wrapper">ey ey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ey ey "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mana dp viral 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana dp viral "/>
</div>
<div class="lyrico-lyrics-wrapper">ooh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooh"/>
</div>
<div class="lyrico-lyrics-wrapper">mana smoochi viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana smoochi viral"/>
</div>
<div class="lyrico-lyrics-wrapper">say what
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="say what"/>
</div>
<div class="lyrico-lyrics-wrapper">mari manapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mari manapai"/>
</div>
<div class="lyrico-lyrics-wrapper">putti gosseipannee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="putti gosseipannee"/>
</div>
<div class="lyrico-lyrics-wrapper">viral viral come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral viral come on"/>
</div>
<div class="lyrico-lyrics-wrapper">mana pere viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana pere viral"/>
</div>
<div class="lyrico-lyrics-wrapper">you know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="you know"/>
</div>
<div class="lyrico-lyrics-wrapper">mana theere viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana theere viral"/>
</div>
<div class="lyrico-lyrics-wrapper">let's go
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="let's go"/>
</div>
<div class="lyrico-lyrics-wrapper">arey manakai chache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arey manakai chache"/>
</div>
<div class="lyrico-lyrics-wrapper">abbai listu viral viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="abbai listu viral viral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">viral avuthadhi choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral avuthadhi choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvese dressedhaniana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvese dressedhaniana"/>
</div>
<div class="lyrico-lyrics-wrapper">viral kaanidhi ledhu eepaina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral kaanidhi ledhu eepaina "/>
</div>
<div class="lyrico-lyrics-wrapper">viral avuthayi choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral avuthayi choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvuvaade perfumesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvuvaade perfumesina"/>
</div>
<div class="lyrico-lyrics-wrapper">oh ya nee close up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh ya nee close up"/>
</div>
<div class="lyrico-lyrics-wrapper">nee make up break up
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee make up break up"/>
</div>
<div class="lyrico-lyrics-wrapper">virals era 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virals era "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">viral nuvu tv nekki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viral nuvu tv nekki"/>
</div>
<div class="lyrico-lyrics-wrapper">boothulu vaagithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boothulu vaagithe"/>
</div>
<div class="lyrico-lyrics-wrapper">o ye eh viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o ye eh viral"/>
</div>
<div class="lyrico-lyrics-wrapper">ninu meetoo moment loki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninu meetoo moment loki"/>
</div>
<div class="lyrico-lyrics-wrapper">laagithe ohh ya ey ey ey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="laagithe ohh ya ey ey ey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maikamlaa kammindhee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maikamlaa kammindhee"/>
</div>
<div class="lyrico-lyrics-wrapper">ee viral mania alchoholu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee viral mania alchoholu"/>
</div>
<div class="lyrico-lyrics-wrapper">lona ledee euphoria
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lona ledee euphoria"/>
</div>
<div class="lyrico-lyrics-wrapper">ee maikam ee lokam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee maikam ee lokam"/>
</div>
<div class="lyrico-lyrics-wrapper">neethonee undhante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethonee undhante"/>
</div>
<div class="lyrico-lyrics-wrapper">nee life eh viral viral viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee life eh viral viral viral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hippi hippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hippi hippi"/>
</div>
<div class="lyrico-lyrics-wrapper">let's be peppy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="let's be peppy"/>
</div>
<div class="lyrico-lyrics-wrapper">neeke nuvvu vvip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeke nuvvu vvip"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hippi hippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hippi hippi"/>
</div>
<div class="lyrico-lyrics-wrapper">let's be peppy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="let's be peppy"/>
</div>
<div class="lyrico-lyrics-wrapper">neethe neethe ts ap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethe neethe ts ap"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hey paapa hey paapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey paapa hey paapa"/>
</div>
<div class="lyrico-lyrics-wrapper">hey paapa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey paapa "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hippi hippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hippi hippi"/>
</div>
<div class="lyrico-lyrics-wrapper">let's be peppy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="let's be peppy"/>
</div>
<div class="lyrico-lyrics-wrapper">neeke nuvvu vvip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeke nuvvu vvip"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hippi hippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hippi hippi"/>
</div>
<div class="lyrico-lyrics-wrapper">let's be peppy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="let's be peppy"/>
</div>
<div class="lyrico-lyrics-wrapper">neethe neethe ts ap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethe neethe ts ap"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hippi hippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hippi hippi"/>
</div>
<div class="lyrico-lyrics-wrapper">let's be peppy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="let's be peppy"/>
</div>
<div class="lyrico-lyrics-wrapper">neeke nuvvu vvip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeke nuvvu vvip"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">hippi hippi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hippi hippi"/>
</div>
<div class="lyrico-lyrics-wrapper">let's be peppy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="let's be peppy"/>
</div>
<div class="lyrico-lyrics-wrapper">neethe neethe ts ap
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethe neethe ts ap"/>
</div>
</pre>
