---
title: "uthira uthira song lyrics"
album: "Pon Manickavel"
artist: "D. Imman"
lyricist: "Madhan Karky"
director: "A.C. Mugil Chellappan"
path: "/albums/pon-manickavel-lyrics"
song: "Uthira Uthira"
image: ../../images/albumart/pon-manickavel.jpg
date: 2021-11-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/x7sH67gXlkY"
type: "love"
singers:
  - Shreya Ghoshal
  - Sreekanth Hariharan
  - Maria Roe Vincent
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Uthiraa Uthiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiraa Uthiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinaval Kodi Ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaval Kodi Ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthiraa Uthiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiraa Uthiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaigal Yaavum Unnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaigal Yaavum Unnil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uthiraa Uthiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiraa Uthiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinaval Kodi Ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaval Kodi Ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthiraa Uthiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiraa Uthiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaigal Yaavum Unnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaigal Yaavum Unnil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai Urasidum Meesai Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Urasidum Meesai Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumbiyal Padiththaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumbiyal Padiththaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Mayakkidum Sorkkal Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Mayakkidum Sorkkal Kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhuviyal Padiththaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhuviyal Padiththaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Rendum Pinni Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Rendum Pinni Kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhiyal Padithiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhiyal Padithiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uthiraa Uthiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiraa Uthiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinaval Yenoo Kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaval Yenoo Kannil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uthiraa Uthiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiraa Uthiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaigal Yaavum Unnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaigal Yaavum Unnil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannagaru Karu Koondhal Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannagaru Karu Koondhal Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanaviyal Vilakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanaviyal Vilakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnachiru Siru Kangal Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnachiru Siru Kangal Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaniyal Vilakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaniyal Vilakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nenjin Aalam Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nenjin Aalam Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaliyal Nee Padikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaliyal Nee Padikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Paritchaikkena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paritchaikkena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival Thavam Kidakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Thavam Kidakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thottu Thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thottu Thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu Thottu Neeyum Kalaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Thottu Neeyum Kalaikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Angae Paanipattu Por
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angae Paanipattu Por"/>
</div>
<div class="lyrico-lyrics-wrapper">Inge Meanipattu Por
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inge Meanipattu Por"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam Ittu Ittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Ittu Ittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ittu Enai Neeyum Kavilkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ittu Enai Neeyum Kavilkka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thevaigal Aayiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaigal Aayiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Thervukenna Theeviram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thervukenna Theeviram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadandhu Mudintha Porgal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandhu Mudintha Porgal Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakku Yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakku Yenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiya Vidiya Porgal Seithae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiya Vidiya Porgal Seithae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saritham Eluthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saritham Eluthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nithinilai Naan Ketkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithinilai Naan Ketkka"/>
</div>
<div class="lyrico-lyrics-wrapper">En Aadai Nee Neekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aadai Nee Neekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pana Mathippin Yettram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pana Mathippin Yettram"/>
</div>
<div class="lyrico-lyrics-wrapper">Irakkangal Nee Kaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakkangal Nee Kaatta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arasiyalai Arinthidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arasiyalai Arinthidavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Urasiyal Arinthidu Maanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasiyal Arinthidu Maanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Iravil Mathippilanthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Iravil Mathippilanthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Namm Moththa Muthangal Rathaanathaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namm Moththa Muthangal Rathaanathaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uthiraa Uthiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiraa Uthiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinaval Kodi Ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaval Kodi Ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthiraa Uthiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiraa Uthiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaigal Yaavum Unnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaigal Yaavum Unnil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Unai Unai Thooram Thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Unai Unai Thooram Thalli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulaviyal Padippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaviyal Padippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Idai Idai-veli Neekkathaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Idai Idai-veli Neekkathaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaviyal Padithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaviyal Padithaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalathtin Kaalai Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalathtin Kaalai Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaviyal Mudithiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaviyal Mudithiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uthiraa Uthiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthiraa Uthiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmh Mhmm Aah Ahh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmh Mhmm Aah Ahh"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa Uthiraa Uthiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa Uthiraa Uthiraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmhmm Mhmm Aah Ahh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmhmm Mhmm Aah Ahh"/>
</div>
</pre>
