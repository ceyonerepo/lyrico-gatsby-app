---
title: "srivalli song lyrics"
album: "Pushpa The Rise"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Sukumar"
path: "/albums/pushpa-the-rise-song-lyrics"
song: "Srivalli"
image: ../../images/albumart/pushpa-the-rise.jpg
date: 2021-12-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RcQiR7Dkfao"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naa Paakkura Paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Paakkura Paakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Paakkama Nee Enga pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paakkama Nee Enga pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Aakkura Paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Aakkura Paakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam Paakkura Enna Thavira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Paakkura Enna Thavira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaannadha Thevatthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaannadha Thevatthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Moodama paakkuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Moodama paakkuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Munne Naanirundhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Munne Naanirundhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadandhu Poogiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadandhu Poogiriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarva Karpoora Deepama Sri Valli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva Karpoora Deepama Sri Valli"/>
</div>
<div class="lyrico-lyrics-wrapper">peche Kalyani Raagama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peche Kalyani Raagama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarva Karoora Deepama Sri Villi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva Karoora Deepama Sri Villi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam Kasthoori Vasamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Kasthoori Vasamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koottathula Poolnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottathula Poolnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Nadappen Munne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Nadappen Munne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nadandhaa Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nadandhaa Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuveen Un Pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuveen Un Pinne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evanaiyume Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanaiyume Paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaai Kunjadhu Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaai Kunjadhu Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kolusa Paakkathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kolusa Paakkathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Kuninjendi Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Kuninjendi Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhagatthi Unna Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhagatthi Unna Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaka Suthi Vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaka Suthi Vandhaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paatthidaama Poreeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatthidaama Poreeye"/>
</div>
<div class="lyrico-lyrics-wrapper">paavam Paakkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavam Paakkaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarva Karpoora Deepama Sri Villi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva Karpoora Deepama Sri Villi"/>
</div>
<div class="lyrico-lyrics-wrapper">peche Kalyani Raagama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peche Kalyani Raagama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarva Karoora Deepama Sri Villi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva Karoora Deepama Sri Villi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam Kasthoori Vasamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Kasthoori Vasamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Onnum Periya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Onnum Periya"/>
</div>
<div class="lyrico-lyrics-wrapper">Perzhagi Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perzhagi Illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theraadha KoottThil Azhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theraadha KoottThil Azhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyiradi Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyiradi Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">padhinettu Vayasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padhinettu Vayasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottale Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottale Pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Illa Ella Onnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illa Ella Onnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinusaatthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinusaatthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutthukkallukku Selai Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutthukkallukku Selai Katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vitta kuuda Sitta Teriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vitta kuuda Sitta Teriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kotthu Poova koondhalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotthu Poova koondhalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Endha Ponnum Bodhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Endha Ponnum Bodhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yettum Aanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yettum Aanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarva Karpoora Deepama Sri Valli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva Karpoora Deepama Sri Valli"/>
</div>
<div class="lyrico-lyrics-wrapper">Peche Kalyani Raagama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peche Kalyani Raagama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarva Karpoora Deepama Sri Valli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva Karpoora Deepama Sri Valli"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam Kasthoori Vasamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam Kasthoori Vasamaa"/>
</div>
</pre>
