---
title: "let's be friends song lyrics"
album: "Pyaar Prema Kaadhal"
artist: "Yuvan Shankar Raja"
lyricist: "Elan"
director: "Elan"
path: "/albums/pyaar-prema-kaadhal-lyrics"
song: "Let's Be Friends"
image: ../../images/albumart/pyaar-prema-kaadhal.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1_T5OVJjOTM"
type: "happy"
singers:
  - Cliffy Carlton
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thikki thikki nenjil sikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikki thikki nenjil sikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu nindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu nindrathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikki vikki kanna sokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikki vikki kanna sokki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonga poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonga poguthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetta vetta parakkuthae kaathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetta vetta parakkuthae kaathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu pattu kizhiyuren naan thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu pattu kizhiyuren naan thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sotta sotta nanaiyuren koothadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sotta sotta nanaiyuren koothadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sillu sillaai norunguren nee theendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sillu sillaai norunguren nee theendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh vennilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh vennilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kuru kuru kannila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kuru kuru kannila"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey un nilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey un nilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vizhunthen vinnilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vizhunthen vinnilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna chinna ulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna chinna ulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothum maanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum maanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna vanna nila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna vanna nila"/>
</div>
<div class="lyrico-lyrics-wrapper">Povom thaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povom thaenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum naanum seravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum seravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai theera"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodal angae koodal ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodal angae koodal ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal engae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikki thikki nenjil sikki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikki thikki nenjil sikki"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu nindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu nindrathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaai yaai yaai yaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaai yaai yaai yaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikki vikki kanna sokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikki vikki kanna sokki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoonga poguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoonga poguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaai yaai yaa yaa yae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaai yaai yaa yaa yae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vetta vetta parakkuthae kaathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetta vetta parakkuthae kaathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu pattu kizhiyuren naan thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu pattu kizhiyuren naan thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sotta sotta nanaiyuren koothadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sotta sotta nanaiyuren koothadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sillu sillaai norunguren nee theendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sillu sillaai norunguren nee theendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh vennilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh vennilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohh vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kuru kuru kannila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kuru kuru kannila"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kuru kuru kannila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kuru kuru kannila"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey un nilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey un nilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vizhunthen vinnilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vizhunthen vinnilaa"/>
</div>
</pre>
