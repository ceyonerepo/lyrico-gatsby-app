---
title: "yellamma yellamma song lyrics"
album: "Thupaki Ramudu"
artist: "T Prabhakar "
lyricist: "Abhinaya Srinivas"
director: "T Prabhakar"
path: "/albums/thupaki-ramudu-lyrics"
song: "Yellamma Yellamma"
image: ../../images/albumart/thupaki-ramudu.jpg
date: 2019-10-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/J9T7YRCuGWo"
type: "devotional"
singers:
  - Rasamayi Balakishan
  - Varam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yellamma yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yellamma yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">yeyigandla thallamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeyigandla thallamma"/>
</div>
<div class="lyrico-lyrics-wrapper">yededu lokaale yelinavamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yededu lokaale yelinavamma"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhilochi kaalivi nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhilochi kaalivi nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">karuninche devatha nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuninche devatha nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">kaapade shakthivi nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaapade shakthivi nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">maayamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maayamma "/>
</div>
<div class="lyrico-lyrics-wrapper">maa oori yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa oori yellamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ammalaganna amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammalaganna amma"/>
</div>
<div class="lyrico-lyrics-wrapper">renuka yallamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renuka yallamma"/>
</div>
<div class="lyrico-lyrics-wrapper">maa nimmalamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa nimmalamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">needhe renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="needhe renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">pallelu sallanga soose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pallelu sallanga soose"/>
</div>
<div class="lyrico-lyrics-wrapper">renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">maa pandaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa pandaga "/>
</div>
<div class="lyrico-lyrics-wrapper">pabbalu neeke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pabbalu neeke"/>
</div>
<div class="lyrico-lyrics-wrapper">renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">ininti ilavelpai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ininti ilavelpai "/>
</div>
<div class="lyrico-lyrics-wrapper">renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">mamu ivuramga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mamu ivuramga"/>
</div>
<div class="lyrico-lyrics-wrapper">savurinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savurinche"/>
</div>
<div class="lyrico-lyrics-wrapper">renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu leni thaavu leni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu leni thaavu leni"/>
</div>
<div class="lyrico-lyrics-wrapper">needhi kani jeevi ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="needhi kani jeevi ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">sivudaina nee aagnyanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sivudaina nee aagnyanu"/>
</div>
<div class="lyrico-lyrics-wrapper">dhatadamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhatadamma "/>
</div>
<div class="lyrico-lyrics-wrapper">nee peru thalachukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee peru thalachukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">chalu renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chalu renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">iga pattarani poonakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iga pattarani poonakale"/>
</div>
<div class="lyrico-lyrics-wrapper">renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">nee bottu pettukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee bottu pettukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">chaalu renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chaalu renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">ye kashtamantu kaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye kashtamantu kaana "/>
</div>
<div class="lyrico-lyrics-wrapper">raadhu renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raadhu renuka yellamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ammalaganna amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammalaganna amma"/>
</div>
<div class="lyrico-lyrics-wrapper">renuka yallamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renuka yallamma"/>
</div>
<div class="lyrico-lyrics-wrapper">maa nimmalamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa nimmalamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">needhe renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="needhe renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">pallelu sallanga soose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pallelu sallanga soose"/>
</div>
<div class="lyrico-lyrics-wrapper">renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">maa pandaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa pandaga "/>
</div>
<div class="lyrico-lyrics-wrapper">pabbalu neeke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pabbalu neeke"/>
</div>
<div class="lyrico-lyrics-wrapper">renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renuka yellamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maavuraala thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maavuraala thalli"/>
</div>
<div class="lyrico-lyrics-wrapper">maa gaavurala bidda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa gaavurala bidda"/>
</div>
<div class="lyrico-lyrics-wrapper">ye theeru ninnu kolichemu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye theeru ninnu kolichemu"/>
</div>
<div class="lyrico-lyrics-wrapper">renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">ooroora kattamaise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooroora kattamaise"/>
</div>
<div class="lyrico-lyrics-wrapper">oorege bakthi posi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorege bakthi posi"/>
</div>
<div class="lyrico-lyrics-wrapper">ye pera ninnu pilichemu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye pera ninnu pilichemu"/>
</div>
<div class="lyrico-lyrics-wrapper">renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">maa mudupulanni mechi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa mudupulanni mechi"/>
</div>
<div class="lyrico-lyrics-wrapper">maa gadapa dhaati vacchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa gadapa dhaati vacchi"/>
</div>
<div class="lyrico-lyrics-wrapper">maa kadupulona chocchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa kadupulona chocchi"/>
</div>
<div class="lyrico-lyrics-wrapper">praanale nilipinaavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="praanale nilipinaavu"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu leni poddhu ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu leni poddhu ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">neeku raani vidhya ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeku raani vidhya ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">jeevantha paramatma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeevantha paramatma"/>
</div>
<div class="lyrico-lyrics-wrapper">vuvvenamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vuvvenamma "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee peru thalachukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee peru thalachukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">chalu renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chalu renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">iga pattarani poonakale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iga pattarani poonakale"/>
</div>
<div class="lyrico-lyrics-wrapper">renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">nee bottu pettukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee bottu pettukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">chaalu renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chaalu renuka yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">ye kashtamantu kaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye kashtamantu kaana "/>
</div>
<div class="lyrico-lyrics-wrapper">raadhu renuka yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raadhu renuka yellamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yay jerripothu jadalu vesi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yay jerripothu jadalu vesi "/>
</div>
<div class="lyrico-lyrics-wrapper">maagu paamu nadumu chutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maagu paamu nadumu chutti"/>
</div>
<div class="lyrico-lyrics-wrapper">kanaka dappu pattukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaka dappu pattukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli yellamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli yellamma "/>
</div>
<div class="lyrico-lyrics-wrapper">eera gola sarupulatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eera gola sarupulatho"/>
</div>
<div class="lyrico-lyrics-wrapper">jogineela arupulatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jogineela arupulatho"/>
</div>
<div class="lyrico-lyrics-wrapper">rangamlo dhumkevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rangamlo dhumkevu"/>
</div>
<div class="lyrico-lyrics-wrapper">maa yamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maa yamma "/>
</div>
<div class="lyrico-lyrics-wrapper">aagula gundaalu dhaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagula gundaalu dhaati"/>
</div>
<div class="lyrico-lyrics-wrapper">vasthamamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasthamamma "/>
</div>
<div class="lyrico-lyrics-wrapper">neeku muggula patnalau 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeku muggula patnalau "/>
</div>
<div class="lyrico-lyrics-wrapper">enno geesthamamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enno geesthamamma"/>
</div>
<div class="lyrico-lyrics-wrapper">korina saakallu neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="korina saakallu neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">poosthamamma nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poosthamamma nee"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaarina baarakulenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaarina baarakulenno"/>
</div>
<div class="lyrico-lyrics-wrapper">thesthamamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thesthamamma"/>
</div>
<div class="lyrico-lyrics-wrapper">yay yepa matala thoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yay yepa matala thoram"/>
</div>
<div class="lyrico-lyrics-wrapper">yeta potla haaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeta potla haaram"/>
</div>
<div class="lyrico-lyrics-wrapper">pachi kundala bonam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pachi kundala bonam"/>
</div>
<div class="lyrico-lyrics-wrapper">palle suthula gaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palle suthula gaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">gana gana gana gantalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gana gana gana gantalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">jama jama jama jamidikitho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jama jama jama jamidikitho"/>
</div>
<div class="lyrico-lyrics-wrapper">siva siva siva sathulatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siva siva siva sathulatho"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaddarilli ooru vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaddarilli ooru vaada"/>
</div>
<div class="lyrico-lyrics-wrapper">oogindhamma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oogindhamma "/>
</div>
<div class="lyrico-lyrics-wrapper">urimeti kallathoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urimeti kallathoti"/>
</div>
<div class="lyrico-lyrics-wrapper">yellamma meriseti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yellamma meriseti"/>
</div>
<div class="lyrico-lyrics-wrapper">soopu needhi yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soopu needhi yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">paapulu thegilinchi yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paapulu thegilinchi yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">kopaana ragilevu maa yamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kopaana ragilevu maa yamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">etthu thokkani matti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etthu thokkani matti"/>
</div>
<div class="lyrico-lyrics-wrapper">gaddenu vestham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaddenu vestham "/>
</div>
<div class="lyrico-lyrics-wrapper">olisina odlu vadi biyyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olisina odlu vadi biyyam"/>
</div>
<div class="lyrico-lyrics-wrapper">postham gappadhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="postham gappadhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">nee neellu aara postham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee neellu aara postham"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaki muttani pallu palaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaki muttani pallu palaram"/>
</div>
<div class="lyrico-lyrics-wrapper">isthamam aada biddalanth
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isthamam aada biddalanth"/>
</div>
<div class="lyrico-lyrics-wrapper">nee bonametthinare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee bonametthinare"/>
</div>
<div class="lyrico-lyrics-wrapper">ammalakka lantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammalakka lantha"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thovva pattinaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thovva pattinaare"/>
</div>
<div class="lyrico-lyrics-wrapper">chinna peddalantha seri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinna peddalantha seri"/>
</div>
<div class="lyrico-lyrics-wrapper">anna dhammulantha koodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anna dhammulantha koodi"/>
</div>
<div class="lyrico-lyrics-wrapper">siralaku sitikelesi dhummukulatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siralaku sitikelesi dhummukulatho"/>
</div>
<div class="lyrico-lyrics-wrapper">dhummu lepi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhummu lepi"/>
</div>
<div class="lyrico-lyrics-wrapper">pata pata pata pallukoriki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pata pata pata pallukoriki"/>
</div>
<div class="lyrico-lyrics-wrapper">potharaju laadanga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potharaju laadanga "/>
</div>
<div class="lyrico-lyrics-wrapper">gaapu katti ghatamu etthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaapu katti ghatamu etthi"/>
</div>
<div class="lyrico-lyrics-wrapper">ganga dolu mogamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganga dolu mogamgaa"/>
</div>
<div class="lyrico-lyrics-wrapper">baileli vastundhi yellamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baileli vastundhi yellamma"/>
</div>
<div class="lyrico-lyrics-wrapper">karikatthula kolatam choodamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karikatthula kolatam choodamma"/>
</div>
<div class="lyrico-lyrics-wrapper">puli meedha kurusundhi maa yamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puli meedha kurusundhi maa yamma"/>
</div>
<div class="lyrico-lyrics-wrapper">tholi bonam ytthindhi peddamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholi bonam ytthindhi peddamma"/>
</div>
</pre>
