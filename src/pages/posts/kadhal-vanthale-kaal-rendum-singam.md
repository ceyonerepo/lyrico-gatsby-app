---
title: "kadhal vanthale kaal rendum song lyrics"
album: "Singam"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari Gopalakrishnan"
path: "/albums/singam-lyrics"
song: "Kadhal Vanthale Kaal Rendum"
image: ../../images/albumart/singam.jpg
date: 2010-05-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-Ce4pB7xcxo"
type: "Love"
singers:
  - Baba Sehgal
  - Priyadarshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaadhal Vandhaley Kaalu Rendum Thannaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Vandhaley Kaalu Rendum Thannaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathaa Suththudhey Undhan Pinnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathaa Suththudhey Undhan Pinnale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasa Vandhaaley I Love You Sonnaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Vandhaaley I Love You Sonnaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Rendum Sutthudhey Undhan Munnaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Rendum Sutthudhey Undhan Munnaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thittam Potu Paathu Thimiraa Pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thittam Potu Paathu Thimiraa Pesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Nee Valaicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nee Valaicha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettaruvaa Meesa Muriki Murichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaruvaa Meesa Muriki Murichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenja Kalaccha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenja Kalaccha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ey Vaadi Ey vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Vaadi Ey vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vaadamalli Poothaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaadamalli Poothaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iduppu Konda Oosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppu Konda Oosi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirippu Window AC Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirippu Window AC Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudhugu Thekku Maram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhugu Thekku Maram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhusaa Paatha Joramdhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhusaa Paatha Joramdhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neththi Naduvey Pottu Vekkavey Kashtamaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neththi Naduvey Pottu Vekkavey Kashtamaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Nenachey Oru Oram Vechukkaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nenachey Oru Oram Vechukkaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnapola Naan Illa Mothama Maariyaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnapola Naan Illa Mothama Maariyaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthavaaliya Kooda Konji Koopidaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthavaaliya Kooda Konji Koopidaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nesama Unna Paththi Pesa Neram Patthala da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesama Unna Paththi Pesa Neram Patthala da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Medhuva Munna Pola Boomi Sutthala da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuva Munna Pola Boomi Sutthala da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chattiniya Vittuputtu Idiliya Thinnuputten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chattiniya Vittuputtu Idiliya Thinnuputten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattunu Amma Keka Edho Reelu Vitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattunu Amma Keka Edho Reelu Vitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Veera Hey Soora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Veera Hey Soora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ottikitta Nenjupooraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ottikitta Nenjupooraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarva Pattakatthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarva Pattakatthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paayum Enna Suththi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paayum Enna Suththi da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enaku Epavumey Nenappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaku Epavumey Nenappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paththi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paththi Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meesa Thirutha Chinna Kaththirikola Vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesa Thirutha Chinna Kaththirikola Vechen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththam Nenappil Naanum Mookai Vettikiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam Nenappil Naanum Mookai Vettikiten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattupudavai Katti Pattunu Kaalilvilzha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattupudavai Katti Pattunu Kaalilvilzha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paththuthadava Dhinam Oththigai Paathukitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththuthadava Dhinam Oththigai Paathukitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sathulla Saapadellam Thinnu Ninnu En Udamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathulla Saapadellam Thinnu Ninnu En Udamba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sattunu Sola Thatta Pola Maathuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattunu Sola Thatta Pola Maathuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattunu Vetti Pesi Mutti Nikkum En Gunattha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattunu Vetti Pesi Mutti Nikkum En Gunattha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katti Thaan Potu Romba Saantham Aakuriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Thaan Potu Romba Saantham Aakuriye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Paaru Nee Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Paaru Nee Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Paatha Manam Joru Joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paatha Manam Joru Joru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaku Munnala Thaan Nilavey Dullaa Iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaku Munnala Thaan Nilavey Dullaa Iruku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Kittathatta Idhayam Fulla Iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Kittathatta Idhayam Fulla Iruku"/>
</div>
</pre>
