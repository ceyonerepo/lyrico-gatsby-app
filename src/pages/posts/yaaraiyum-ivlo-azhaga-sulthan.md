---
title: "yaaraiyum ivlo azhaga song lyrics"
album: "Sulthan"
artist: "vivek - mervin"
lyricist: "viveka"
director: "Bakkiyaraj Kannan"
path: "/albums/sulthan-song-lyrics"
song: "Epadi Iruntha Naanga"
image: ../../images/albumart/sulthan.jpg
date: 2020-04-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZCxumqSYaw4"
type: "Love"
singers:
  - Mervin Solomon
  - Silambarsan TR
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Yaraiyum Ivlo Azhaga Parkalae,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Yaraiyum Ivlo Azhaga Parkalae,"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnapol Evarum Usure Thakkalae,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnapol Evarum Usure Thakkalae,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhula Vera Yedhuvum Keakkalae,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhula Vera Yedhuvum Keakkalae,"/>
</div>
<div class="lyrico-lyrics-wrapper">Galithan Anen Pora Pokkulae,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galithan Anen Pora Pokkulae,"/>
</div>
<div class="lyrico-lyrics-wrapper">Konala Pakura Kovama Pesura,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konala Pakura Kovama Pesura,"/>
</div>
<div class="lyrico-lyrics-wrapper">Channela Matthura En Manasa,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Channela Matthura En Manasa,"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthal Murai Paththen Thazhakizh Anen,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Murai Paththen Thazhakizh Anen,"/>
</div>
<div class="lyrico-lyrics-wrapper">Marumurai Parthen Aiyaiyo,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai Parthen Aiyaiyo,"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Murai Parththa Paithiyam Avene,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Murai Parththa Paithiyam Avene,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthal Murai Paththen Thazhakizh Anen,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Murai Paththen Thazhakizh Anen,"/>
</div>
<div class="lyrico-lyrics-wrapper">Marumurai Parthen Aiyaiyo,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai Parthen Aiyaiyo,"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Murai Parththa Paithiyam Avene Pavam Paru Penne,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Murai Parththa Paithiyam Avene Pavam Paru Penne,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaraiyum Ivlo Azhaga Parkalae,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaraiyum Ivlo Azhaga Parkalae,"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnapol Evarum Usure Thakkalae,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnapol Evarum Usure Thakkalae,"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhula Vera Edhuvum Keakkalae,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhula Vera Edhuvum Keakkalae,"/>
</div>
<div class="lyrico-lyrics-wrapper">Galidhan Anen Pora Pokkulae,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galidhan Anen Pora Pokkulae,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konala Pakura Kovama Pesura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konala Pakura Kovama Pesura"/>
</div>
<div class="lyrico-lyrics-wrapper">Channela Matthura En Manasa,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Channela Matthura En Manasa,"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthal Murai Paththen Thazhakizh Anen,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Murai Paththen Thazhakizh Anen,"/>
</div>
<div class="lyrico-lyrics-wrapper">Marumurai Parthen Aiyaiyo,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai Parthen Aiyaiyo,"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Murai Parthth Paithiyam Avene Pavam Paru Penne,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Murai Parthth Paithiyam Avene Pavam Paru Penne,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thannikulla Kaiya Vecha Thanikku Janni Yerum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thannikulla Kaiya Vecha Thanikku Janni Yerum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Katterumbu Unna Thotta Pattampoochiya Marum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katterumbu Unna Thotta Pattampoochiya Marum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Manja Poosa Kaiya Vecha Anjaru Colour-a Agum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Manja Poosa Kaiya Vecha Anjaru Colour-a Agum,"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ettu Vacha Kattantharai Mittaya Pola Inikum,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ettu Vacha Kattantharai Mittaya Pola Inikum,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathu Thirukaniyil Kadhal Thalaikeruthe,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathu Thirukaniyil Kadhal Thalaikeruthe,"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Poosum Marudhaniyil En Boomi Sivapaguthe,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Poosum Marudhaniyil En Boomi Sivapaguthe,"/>
</div>
<div class="lyrico-lyrics-wrapper">Seval Iragala Selai Nan Senji,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seval Iragala Selai Nan Senji,"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharen Vadi En Tamizh Isaiye Tamizh Isaiye,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharen Vadi En Tamizh Isaiye Tamizh Isaiye,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthal Murai Paththen Thazhakizh Anen,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Murai Paththen Thazhakizh Anen,"/>
</div>
<div class="lyrico-lyrics-wrapper">Marumurai Parthen Aiyaiyo,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai Parthen Aiyaiyo,"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Murai Parththa Paithiyam Avene,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Murai Parththa Paithiyam Avene,"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthal Murai Paththen Thazhakizh Anen,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Murai Paththen Thazhakizh Anen,"/>
</div>
<div class="lyrico-lyrics-wrapper">Marumurai Parthen Aiyaiyo,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marumurai Parthen Aiyaiyo,"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Murai Parththa Paithiyam Avene Pavam Paru Penne,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Murai Parththa Paithiyam Avene Pavam Paru Penne,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaraiyum Ivlo Azhaga Parkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaraiyum Ivlo Azhaga Parkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnapol Evarum Usure Thakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnapol Evarum Usure Thakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhula Vera Edhuvum Keakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhula Vera Edhuvum Keakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Galidhan Anen Pora Pokkula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galidhan Anen Pora Pokkula"/>
</div>
</pre>
