---
title: 'karka karka song lyrics'
album: 'Vettaiyadu Vilayadu'
artist: 'Harris Jayaraj'
lyricist: 'Thamarai'
director: 'Gowtham Vasudev Menon'
path: '/albums/vettaiyadu-vilayadu-song-lyrics'
song: 'Karka Karka'
image: ../../images/albumart/vettaiyadu-vilayadu.jpg
date: 2006-08-25
lang: tamil
singers: 
- Andrea Jeremiah
- Tippu
- Devan
- Nakul
youtubeLink: "https://www.youtube.com/embed/Uu9ZHN-TGoA"
type: 'mass'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Raghavan stay in the process
<input type="checkbox" class="lyrico-select-lyric-line" value="Raghavan stay in the process"/>
</div>
<div class="lyrico-lyrics-wrapper">Top dollar going
<input type="checkbox" class="lyrico-select-lyric-line" value="Top dollar going"/>
</div>
<div class="lyrico-lyrics-wrapper">Losaed getting time
<input type="checkbox" class="lyrico-select-lyric-line" value="Losaed getting time"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready come on
<input type="checkbox" class="lyrico-select-lyric-line" value="Ready come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Hai let’s go
<input type="checkbox" class="lyrico-select-lyric-line" value="Hai let’s go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Karka karka kallam karka
<input type="checkbox" class="lyrico-select-lyric-line" value="Karka karka kallam karka"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru sonna avan
<input type="checkbox" class="lyrico-select-lyric-line" value="Endru sonna avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallam katra kalvar ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kallam katra kalvar ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattikkollum aran
<input type="checkbox" class="lyrico-select-lyric-line" value="Mattikkollum aran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nirka nirka nermayil nirka
<input type="checkbox" class="lyrico-select-lyric-line" value="Nirka nirka nermayil nirka"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrukkonda naran
<input type="checkbox" class="lyrico-select-lyric-line" value="Katrukkonda naran"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutrum sutrum kaatrai polae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sutrum sutrum kaatrai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum selvaan ivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Engum selvaan ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thuppaaki matrum thootaavai thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thuppaaki matrum thootaavai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalithaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhalithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endraalum kaakhi sattayaithaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Endraalum kaakhi sattayaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai pidithaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai pidithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Than saavai sattai payil vaithu
<input type="checkbox" class="lyrico-select-lyric-line" value="Than saavai sattai payil vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engeyum selgindraann………
<input type="checkbox" class="lyrico-select-lyric-line" value="Engeyum selgindraann………"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Who’s the man on the land
<input type="checkbox" class="lyrico-select-lyric-line" value="Who’s the man on the land"/>
</div>
<div class="lyrico-lyrics-wrapper">That can stand now
<input type="checkbox" class="lyrico-select-lyric-line" value="That can stand now"/>
</div>
<div class="lyrico-lyrics-wrapper">Who’s the man on the land
<input type="checkbox" class="lyrico-select-lyric-line" value="Who’s the man on the land"/>
</div>
<div class="lyrico-lyrics-wrapper">That can stand down
<input type="checkbox" class="lyrico-select-lyric-line" value="That can stand down"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Karka karka kallam karka
<input type="checkbox" class="lyrico-select-lyric-line" value="Karka karka kallam karka"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru sonna avan
<input type="checkbox" class="lyrico-select-lyric-line" value="Endru sonna avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallam katra kalvar ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kallam katra kalvar ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattikkollum aran
<input type="checkbox" class="lyrico-select-lyric-line" value="Mattikkollum aran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nirka nirka nermayil nirka
<input type="checkbox" class="lyrico-select-lyric-line" value="Nirka nirka nermayil nirka"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrukkonda naran
<input type="checkbox" class="lyrico-select-lyric-line" value="Katrukkonda naran"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutrum sutrum kaatrai polae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sutrum sutrum kaatrai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum selvaan ivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Engum selvaan ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Maaveeramum oru nermaiyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Maaveeramum oru nermaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai korthu kolla
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai korthu kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Agaraadhiyo adhai raaghavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Agaraadhiyo adhai raaghavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena artham solla
<input type="checkbox" class="lyrico-select-lyric-line" value="Ena artham solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adhigaaramo aarpaatamo
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhigaaramo aarpaatamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan pechil illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivan pechil illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Mun aaivadhil pin aaivadhil
<input type="checkbox" class="lyrico-select-lyric-line" value="Mun aaivadhil pin aaivadhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan pulyin pillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivan pulyin pillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ooo..kaakhi sattaikkum undu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooo..kaakhi sattaikkum undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nal karpugal karpugal endru
<input type="checkbox" class="lyrico-select-lyric-line" value="Nal karpugal karpugal endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattiya thanthavan naanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kattiya thanthavan naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru kaigalai kulikkidum maanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Iru kaigalai kulikkidum maanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru thiriyum neruppum
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru thiriyum neruppum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kondaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal kondaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondrum thotram ivandhaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thondrum thotram ivandhaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Karka karka kallam karka
<input type="checkbox" class="lyrico-select-lyric-line" value="Karka karka kallam karka"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru sonna avan
<input type="checkbox" class="lyrico-select-lyric-line" value="Endru sonna avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallam katra kalvar ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kallam katra kalvar ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattikkollum aran
<input type="checkbox" class="lyrico-select-lyric-line" value="Mattikkollum aran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nirka nirka nermayil nirka
<input type="checkbox" class="lyrico-select-lyric-line" value="Nirka nirka nermayil nirka"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrukkonda naran
<input type="checkbox" class="lyrico-select-lyric-line" value="Katrukkonda naran"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutrum sutrum kaatrai polae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sutrum sutrum kaatrai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum selvaan ivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Engum selvaan ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thuppaaki matrum thootaavai thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thuppaaki matrum thootaavai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalithaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhalithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endraalum kaakhi sattayaithaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Endraalum kaakhi sattayaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai pidithaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai pidithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Than saavai sattai payil vaithu
<input type="checkbox" class="lyrico-select-lyric-line" value="Than saavai sattai payil vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engeyum selgindraann………
<input type="checkbox" class="lyrico-select-lyric-line" value="Engeyum selgindraann………"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">You speed and you get pulled over
<input type="checkbox" class="lyrico-select-lyric-line" value="You speed and you get pulled over"/>
</div>
<div class="lyrico-lyrics-wrapper">And the breath analyzer test provides proof
<input type="checkbox" class="lyrico-select-lyric-line" value="And the breath analyzer test provides proof"/>
</div>
<div class="lyrico-lyrics-wrapper">That you aint sober
<input type="checkbox" class="lyrico-select-lyric-line" value="That you aint sober"/>
</div>
<div class="lyrico-lyrics-wrapper">Good Cop!
<input type="checkbox" class="lyrico-select-lyric-line" value="Good Cop!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Stop the beat, it could be my daughter
<input type="checkbox" class="lyrico-select-lyric-line" value="Stop the beat, it could be my daughter"/>
</div>
<div class="lyrico-lyrics-wrapper">Crossing the street
<input type="checkbox" class="lyrico-select-lyric-line" value="Crossing the street"/>
</div>
<div class="lyrico-lyrics-wrapper">If your brand new Buick Skylark
<input type="checkbox" class="lyrico-select-lyric-line" value="If your brand new Buick Skylark"/>
</div>
<div class="lyrico-lyrics-wrapper">A work of art
<input type="checkbox" class="lyrico-select-lyric-line" value="A work of art"/>
</div>
<div class="lyrico-lyrics-wrapper">And its not sitting in the last place you parked
<input type="checkbox" class="lyrico-select-lyric-line" value="And its not sitting in the last place you parked"/>
</div>
<div class="lyrico-lyrics-wrapper">Good Cop!
<input type="checkbox" class="lyrico-select-lyric-line" value="Good Cop!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Run the place
<input type="checkbox" class="lyrico-select-lyric-line" value="Run the place"/>
</div>
<div class="lyrico-lyrics-wrapper">I’ma see the little thief right after the court gate
<input type="checkbox" class="lyrico-select-lyric-line" value="I’ma see the little thief right after the court gate"/>
</div>
<div class="lyrico-lyrics-wrapper">Look.. a lotta of us see police as foes
<input type="checkbox" class="lyrico-select-lyric-line" value="Look.. a lotta of us see police as foes"/>
</div>
<div class="lyrico-lyrics-wrapper">I ain’t tryin to knock ur hussy..
<input type="checkbox" class="lyrico-select-lyric-line" value="I ain’t tryin to knock ur hussy.."/>
</div>
<div class="lyrico-lyrics-wrapper">Due to each his own
<input type="checkbox" class="lyrico-select-lyric-line" value="Due to each his own"/>
</div>
<div class="lyrico-lyrics-wrapper">But when u get voilated
<input type="checkbox" class="lyrico-select-lyric-line" value="But when u get voilated"/>
</div>
<div class="lyrico-lyrics-wrapper">And the beef is on
<input type="checkbox" class="lyrico-select-lyric-line" value="And the beef is on"/>
</div>
<div class="lyrico-lyrics-wrapper">And u living with ur mom’s
<input type="checkbox" class="lyrico-select-lyric-line" value="And u living with ur mom’s"/>
</div>
<div class="lyrico-lyrics-wrapper">And u see they aint grown
<input type="checkbox" class="lyrico-select-lyric-line" value="And u see they aint grown"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">You gonna see my song n read my poem
<input type="checkbox" class="lyrico-select-lyric-line" value="You gonna see my song n read my poem"/>
</div>
<div class="lyrico-lyrics-wrapper">And know that top dollar
<input type="checkbox" class="lyrico-select-lyric-line" value="And know that top dollar"/>
</div>
<div class="lyrico-lyrics-wrapper">The scurry cannot be that wrong
<input type="checkbox" class="lyrico-select-lyric-line" value="The scurry cannot be that wrong"/>
</div>
<div class="lyrico-lyrics-wrapper">Screw you manhood tops -heart
<input type="checkbox" class="lyrico-select-lyric-line" value="Screw you manhood tops -heart"/>
</div>
<div class="lyrico-lyrics-wrapper">Few good cops!
<input type="checkbox" class="lyrico-select-lyric-line" value="Few good cops!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kan aayiram kai aayiram
<input type="checkbox" class="lyrico-select-lyric-line" value="Kan aayiram kai aayiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena dhegam kolla
<input type="checkbox" class="lyrico-select-lyric-line" value="Ena dhegam kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ibboomiyil nadamaadidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ibboomiyil nadamaadidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan deivum alla
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivan deivum alla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaan suriyan oru naalilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaan suriyan oru naalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaamal poonaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaanaamal poonaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avvaanaiyae muzhu virpanai
<input type="checkbox" class="lyrico-select-lyric-line" value="Avvaanaiyae muzhu virpanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seithenum nirpaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Seithenum nirpaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nara vettayigal vettayaigal aada
<input type="checkbox" class="lyrico-select-lyric-line" value="Nara vettayigal vettayaigal aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru kaigalin viralgal neela
<input type="checkbox" class="lyrico-select-lyric-line" value="Iru kaigalin viralgal neela"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirigal ethirigal saaya
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethirigal ethirigal saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Senkuridhiyil dhegangal thooya
<input type="checkbox" class="lyrico-select-lyric-line" value="Senkuridhiyil dhegangal thooya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oru achcham achcham ennum sollai
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru achcham achcham ennum sollai"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyil ittu theerthanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Theeyil ittu theerthanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Karka karka kallam karka
<input type="checkbox" class="lyrico-select-lyric-line" value="Karka karka kallam karka"/>
</div>
<div class="lyrico-lyrics-wrapper">Gotta love it y’all
<input type="checkbox" class="lyrico-select-lyric-line" value="Gotta love it y’all"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru sonna avan
<input type="checkbox" class="lyrico-select-lyric-line" value="Endru sonna avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallam katra kalvar ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kallam katra kalvar ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattikkollum aran
<input type="checkbox" class="lyrico-select-lyric-line" value="Mattikkollum aran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nirka nirka nermayil nirka
<input type="checkbox" class="lyrico-select-lyric-line" value="Nirka nirka nermayil nirka"/>
</div>
<div class="lyrico-lyrics-wrapper">Katrukkonda naran
<input type="checkbox" class="lyrico-select-lyric-line" value="Katrukkonda naran"/>
</div>
<div class="lyrico-lyrics-wrapper">Stand up now
<input type="checkbox" class="lyrico-select-lyric-line" value="Stand up now"/>
</div>
<div class="lyrico-lyrics-wrapper">Sutrum sutrum kaatrai polae
<input type="checkbox" class="lyrico-select-lyric-line" value="Sutrum sutrum kaatrai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engum selvaan ivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Engum selvaan ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thuppaaki matrum thootaavai thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Thuppaaki matrum thootaavai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalithaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhalithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endraalum kaakhi sattayaithaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Endraalum kaakhi sattayaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai pidithaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai pidithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Than saavai sattai payil vaithu
<input type="checkbox" class="lyrico-select-lyric-line" value="Than saavai sattai payil vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Engeyum selgindraann………
<input type="checkbox" class="lyrico-select-lyric-line" value="Engeyum selgindraann………"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Who’s the man on the land
<input type="checkbox" class="lyrico-select-lyric-line" value="Who’s the man on the land"/>
</div>
<div class="lyrico-lyrics-wrapper">That can stand now
<input type="checkbox" class="lyrico-select-lyric-line" value="That can stand now"/>
</div>
<div class="lyrico-lyrics-wrapper">Who’s the man on the land
<input type="checkbox" class="lyrico-select-lyric-line" value="Who’s the man on the land"/>
</div>
<div class="lyrico-lyrics-wrapper">That can stand  (3 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="That can stand"/></div>
</div>
</pre>