---
title: "kanave nee naan song lyrics"
album: "Kannum Kannum Kollaiyadithaal"
artist: "Masala Coffee - Harshavardhan Rameshwar"
lyricist: "Hafeez Rumi - Desingh Periyasamy"
director: "Desingh Periyasamy"
path: "/albums/kannum-kannum-kollaiyadithaal-lyrics"
song: "Kanave Nee Naan"
image: ../../images/albumart/kannum-kannum-kollaiyadithaal.jpg
date: 2020-02-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZUcvKWegSGw"
type: "Sad"
singers:
  - AnirudSooraj Santhosh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanave Nee Naan Vizhikka Villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave Nee Naan Vizhikka Villai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaivaai Endrey Ninaikka Villai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaivaai Endrey Ninaikka Villai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthaai Pirindhaai Niyaayam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthaai Pirindhaai Niyaayam Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaindhen Thaniye Yaarumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaindhen Thaniye Yaarumillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nijam Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nijam Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Nizhal Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Nizhal Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhil Ketkkiren Kidaikaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhil Ketkkiren Kidaikaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Niyaabagangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Niyaabagangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Ninaithiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Ninaithiruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thediye Thirumbaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thediye Thirumbaathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Nee Kaayam Nee Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Nee Kaayam Nee Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanal Neeye Maraindhaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanal Neeye Maraindhaaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesham Nee Poigal Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham Nee Poigal Neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatram Nee Naan Udainthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatram Nee Naan Udainthene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Nee Kaayam Nee Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Nee Kaayam Nee Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanal Neeyae Maraindhaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanal Neeyae Maraindhaaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesham Nee Poigal Neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham Nee Poigal Neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatram Nee Naan Udaintheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatram Nee Naan Udaintheney"/>
</div>
</pre>
