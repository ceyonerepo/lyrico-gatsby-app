---
title: 'moochile theeyumaai song lyrics'
album: 'Baahubali'
artist: "M.M. Keeravani"
lyricist: 'Madhan Karky'
director: 'S.S. Rajamouli'
path: '/albums/baahubali-song-lyrics'
song: 'Moochile Theeyumaai'
image: ../../images/albumart/baahubali.jpg
date: 2015-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4AgB1QJa37w"
type: 'Mass'
singers: 
- Kailash Kher
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Moochilae theeyumaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochilae theeyumaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjjilae kaayamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjjilae kaayamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Varandu pona vizhigal vaazhudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varandu pona vizhigal vaazhudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatchi ondrinai kaattathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatchi ondrinai kaattathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saatchi sollumae poottundhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saatchi sollumae poottundhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Desamae.. uyirthu ezhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desamae.. uyirthu ezhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Im magizhmadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Im magizhmadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Andathin adhibadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andathin adhibadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilampaai…vilampaai!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilampaai…vilampaai!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nyaayathin nyaalam ikhdhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyaayathin nyaalam ikhdhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyampuvaai..nenjjiyampuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyampuvaai..nenjjiyampuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuraiyeraa maatchiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuraiyeraa maatchiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyuraadha magizhmadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyuraadha magizhmadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiraiveezhaa aatchiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiraiveezhaa aatchiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaiyilaa im magizhmadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaiyilaa im magizhmadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thannir ruyitra thulirgalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannir ruyitra thulirgalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Aranae ena potruvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aranae ena potruvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhirkkum padhargalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhirkkum padhargalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhirthu maaithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhirthu maaithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Asurane ena saatruvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asurane ena saatruvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Purisai mathagam meedhir veetridum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purisai mathagam meedhir veetridum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhaagaiyae nee vaazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhaagaiyae nee vaazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru puraviyum aadhavanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru puraviyum aadhavanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon minnum ariyaasanamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon minnum ariyaasanamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhiyae…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhiyae…"/>
</div>
</pre>