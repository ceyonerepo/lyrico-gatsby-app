---
title: "vaseegara en nenjinika song lyrics"
album: "Minnale"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Gautham Menon"
path: "/albums/minnale-lyrics"
song: "Vaseegara En Nenjinika"
image: ../../images/albumart/minnale.jpg
date: 2001-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/x12WuPDlklw"
type: "love"
singers:
  - Bombay Jayashree
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaseegara En Nenjinika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaseegara En Nenjinika"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pon Madiyil Thoonginaal Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pon Madiyil Thoonginaal Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Atheyganam En Kannuranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atheyganam En Kannuranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mun Jenmangalin Yekkangal Theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mun Jenmangalin Yekkangal Theerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaseegara En Nenjinika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaseegara En Nenjinika"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pon Madiyil Thoonginaal Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pon Madiyil Thoonginaal Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Atheyganam En Kannuranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atheyganam En Kannuranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mun Jenmangalin Yekkangal Theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mun Jenmangalin Yekkangal Theerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Nesippathum Swasippathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Nesippathum Swasippathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thayavaal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thayavaal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengugiren Yengugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengugiren Yengugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ninaivaal Naane Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivaal Naane Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adai Mazhai Varum Athil Nenaivomey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adai Mazhai Varum Athil Nenaivomey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulir Kaaichalodu Sneham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulir Kaaichalodu Sneham"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Porvaikkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Porvaikkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Thookam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Thookam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulu Kulu Poigal Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulu Kulu Poigal Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Velvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Velvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Therinthum Kooda Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Therinthum Kooda Anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Athaiyethaan Yethir Paarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Athaiyethaan Yethir Paarkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yengeyum Pogamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengeyum Pogamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Veetulaiye Nee Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Veetulaiye Nee Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sila Samayam Velaiyataai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Samayam Velaiyataai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aadaikulle Naan Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aadaikulle Naan Vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaseegara En Nenjinika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaseegara En Nenjinika"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pon Madiyil Thoonginaal Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pon Madiyil Thoonginaal Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Atheyganam En Kannuranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atheyganam En Kannuranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mun Jenmangalin Yekkangal Theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mun Jenmangalin Yekkangal Theerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Nee Kulithathum Ennai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Nee Kulithathum Ennai Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Selai Nuniyaal Unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Selai Nuniyaal Unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Thudaipaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Thudaipaaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Kavithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirudan Pol Pathungiye Thidir Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudan Pol Pathungiye Thidir Endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaal irunthu Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaal irunthu Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Anaippaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Anaippaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Kavithai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yarenum Mani Ketaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yarenum Mani Ketaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai Solla Kuda Theriyathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai Solla Kuda Theriyathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalenum Mudiveliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalenum Mudiveliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadigaaram Neram Kidaiyathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigaaram Neram Kidaiyathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaseegara En Nenjinika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaseegara En Nenjinika"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pon Madiyil Thoonginaal Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pon Madiyil Thoonginaal Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Atheyganam En Kannuranga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atheyganam En Kannuranga"/>
</div>
<div class="lyrico-lyrics-wrapper">Mun Jenmangalin Yekkangal Theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mun Jenmangalin Yekkangal Theerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Nesippathum Swasippathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Nesippathum Swasippathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thayavaal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thayavaal Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengugiren Yengugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengugiren Yengugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ninaivaal Naane Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivaal Naane Naan"/>
</div>
</pre>
