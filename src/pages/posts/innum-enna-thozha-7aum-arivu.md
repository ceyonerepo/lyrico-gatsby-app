---
title: "innum enna thozha song lyrics"
album: "7aum Arivu"
artist: "Harris Jayaraj"
lyricist: "Pa. Vijay"
director: "A.R. Murugadoss"
path: "/albums/7aum-arivu-lyrics"
song: "Innum Enna Thozha"
image: ../../images/albumart/7aum-arivu.jpg
date: 2011-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9w6ZysXw1b0"
type: "mass"
singers:
  - Balram
  - Naresh Iyer
  - Suchith Suresan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Innum Enna Thozha Ethanayo Naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Enna Thozha Ethanayo Naala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Ingu Namey Thozaithomey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Ingu Namey Thozaithomey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammaal mudiyadha nammaal mudiyaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammaal mudiyadha nammaal mudiyaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai vellum naalai seivome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai vellum naalai seivome"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum illai thadai poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illai thadai poda"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai mella idai poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai mella idai poda"/>
</div>
<div class="lyrico-lyrics-wrapper">nambikayil nadai poda sammadhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nambikayil nadai poda sammadhame"/>
</div>
<div class="lyrico-lyrics-wrapper">enna illai unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna illai unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">yekam enna kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yekam enna kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri endrum vazhiyodu pirandhidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri endrum vazhiyodu pirandhidume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhaal alaiyaai varuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaal alaiyaai varuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">vilndhaal vidhayaai vilvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilndhaal vidhayaai vilvom"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum meendum azhuvom azhuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum meendum azhuvom azhuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum innum iruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum innum iruga"/>
</div>
<div class="lyrico-lyrics-wrapper">ulle uyirum uruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulle uyirum uruga"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamai padaye varuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamai padaye varuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aezhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aezhuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Enna Thozha Ethanayo Naala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Enna Thozha Ethanayo Naala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammai Ingu Namey Thozaithomey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Ingu Namey Thozaithomey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammaal mudiyadha nammaal mudiyaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammaal mudiyadha nammaal mudiyaadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai vellum naalai seivome
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai vellum naalai seivome"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam ninaithaal adhai dhinam ninaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam ninaithaal adhai dhinam ninaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja ninaithadhai mudikalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja ninaithadhai mudikalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodu vaanam dhinam thodum dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodu vaanam dhinam thodum dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">pala kaikalai serkkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala kaikalai serkkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidhai vidhaithaal nellai vidhai vidhaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhai vidhaithaal nellai vidhai vidhaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">adhil kallendrum mulaikumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhil kallendrum mulaikumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam thalaikuraigal nooru kadandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam thalaikuraigal nooru kadandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">thanda veerangal marakkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanda veerangal marakkumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ore manam ore gunam ore thadam edhirgaalathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore manam ore gunam ore thadam edhirgaalathil"/>
</div>
<div class="lyrico-lyrics-wrapper">adhe balam adhe dhidam agam puram nam dhegathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhe balam adhe dhidam agam puram nam dhegathil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalluthoodum oru aayudhathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalluthoodum oru aayudhathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Kalangalil sumakirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Kalangalil sumakirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aezhuthodum oru aayudhathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aezhuthodum oru aayudhathai"/>
</div>
<div class="lyrico-lyrics-wrapper">engal mozhiyinil suvvaikirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal mozhiyinil suvvaikirom"/>
</div>
<div class="lyrico-lyrics-wrapper">pani moottam vandhu panidhenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pani moottam vandhu panidhenna"/>
</div>
<div class="lyrico-lyrics-wrapper">sudum pagalavan maraiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudum pagalavan maraiyuma"/>
</div>
<div class="lyrico-lyrics-wrapper">andha pagai moottam vandhu paniyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha pagai moottam vandhu paniyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">engal iru vizhi uranguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal iru vizhi uranguma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idho idho innaindhadho inam inam nam kaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho idho innaindhadho inam inam nam kaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adho adho therindhadho idam idam nam kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adho adho therindhadho idam idam nam kannodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarum illai thadai poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum illai thadai poda"/>
</div>
<div class="lyrico-lyrics-wrapper">unnai mella idai poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai mella idai poda"/>
</div>
<div class="lyrico-lyrics-wrapper">nambikayil nadai poda sammadhame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nambikayil nadai poda sammadhame"/>
</div>
<div class="lyrico-lyrics-wrapper">enna illai unnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna illai unnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">yekam enna kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yekam enna kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetri endrum vazhiyodu pirandhidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetri endrum vazhiyodu pirandhidume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhaal alaiyaai varuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaal alaiyaai varuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">vilndhaal vidhayaai vilvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilndhaal vidhayaai vilvom"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum meendum azhuvom azhuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum meendum azhuvom azhuvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum innum iruga ulle uyirum uruga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum innum iruga ulle uyirum uruga"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamai padaye varuga Aezhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamai padaye varuga Aezhuga"/>
</div>
</pre>
