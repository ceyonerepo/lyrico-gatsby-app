---
title: "oru naal oru kanavu - duet version song lyrics"
album: "Kannukkul Nilavu"
artist: "Ilaiyaraaja"
lyricist: "Palani Bharathi"
director: "Fazil"
path: "/albums/kannukkul-nilavu-lyrics"
song: "Oru Naal Oru Kanavu - duet Version"
image: ../../images/albumart/kannukkul-nilavu.jpg
date: 2000-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TgYEAJhHgLQ"
type: "melody"
singers:
  - K.J. Yesudas
  - Anuradha Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru Naal Oru Kanavu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Oru Kanavu "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Naan Marakavum Mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Naan Marakavum Mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamai Inikiradhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamai Inikiradhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhupol Kanavondru Kidaiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhupol Kanavondru Kidaiyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Oru Kanavu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Oru Kanavu "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Naan Marakavum Mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Naan Marakavum Mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamai Inikiradhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamai Inikiradhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhupol Kanavondru Kidaiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhupol Kanavondru Kidaiyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanavillil Nadandhu Sendru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillil Nadandhu Sendru "/>
</div>
<div class="lyrico-lyrics-wrapper">Sirithirukum Natchathira Pooparithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithirukum Natchathira Pooparithom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellipirai Padakeduthu Aakaaya Gangai Alaikalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellipirai Padakeduthu Aakaaya Gangai Alaikalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli Kuthithom Neechaladithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli Kuthithom Neechaladithida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Oru Kanavu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Oru Kanavu "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Naan Marakavum Mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Naan Marakavum Mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamai Inikiradhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamai Inikiradhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhupol Kanavondru Kidaiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhupol Kanavondru Kidaiyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nathiyoram Nathiyoram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nathiyoram Nathiyoram "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Sutri Parandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Sutri Parandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilikoottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilikoottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kilikoottam Kilikoottam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilikoottam Kilikoottam "/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthathenil Neeyoru Pazhathottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthathenil Neeyoru Pazhathottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakum Kilikalile Oru Kili 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakum Kilikalile Oru Kili "/>
</div>
<div class="lyrico-lyrics-wrapper">Unaipol Uruveduka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaipol Uruveduka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiliye Unakaga Nanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiliye Unakaga Nanum "/>
</div>
<div class="lyrico-lyrics-wrapper">Kilipol Avadharika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kilipol Avadharika"/>
</div>
<div class="lyrico-lyrics-wrapper">Irakaikal Kondu Vaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakaikal Kondu Vaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnile Parapom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnile Parapom "/>
</div>
<div class="lyrico-lyrics-wrapper">Ullangal Kalapom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullangal Kalapom "/>
</div>
<div class="lyrico-lyrics-wrapper">Vannam Soodum Vannakili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Soodum Vannakili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Oru Kanavu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Oru Kanavu "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Naan Marakavum Mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Naan Marakavum Mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamai Inikiradhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamai Inikiradhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhupol Kanavondru Kidaiyaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhupol Kanavondru Kidaiyaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanaale Vennilave Aval Pol 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanaale Vennilave Aval Pol "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Ilaithaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Ilaithaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Manadhai Un Manadhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Manadhai Un Manadhai "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaipol Yevarukum Koduthaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaipol Yevarukum Koduthaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Olividum Mukathinile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olividum Mukathinile "/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyen Muthaada Yaaalangalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyen Muthaada Yaaalangalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravil Vizhithirundhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravil Vizhithirundhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Needhan Katrathenna Paadangalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needhan Katrathenna Paadangalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnidum Kannile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnidum Kannile "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennavo Ulladhe Sollamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavo Ulladhe Sollamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollamma Nenjilaadum Minnal Kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamma Nenjilaadum Minnal Kodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Oru Kanavu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Oru Kanavu "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Naan Marakavum Mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Naan Marakavum Mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamai Inikiradhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamai Inikiradhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhupol Kanavondru Kidaiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhupol Kanavondru Kidaiyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanavillil Nadandhu Sendru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavillil Nadandhu Sendru "/>
</div>
<div class="lyrico-lyrics-wrapper">Sirithirukum Natchathira Pooparithom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithirukum Natchathira Pooparithom"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellipirai Padakeduthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellipirai Padakeduthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Aakaaya Gangai Alaikalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakaaya Gangai Alaikalil "/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli Kuthithom Neechaladithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli Kuthithom Neechaladithida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Naal Oru Kanavu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Naal Oru Kanavu "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Naan Marakavum Mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Naan Marakavum Mudiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamai Inikiradhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamai Inikiradhu "/>
</div>
<div class="lyrico-lyrics-wrapper">Idhupol Kanavondru Kidaiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhupol Kanavondru Kidaiyadhu"/>
</div>
</pre>
