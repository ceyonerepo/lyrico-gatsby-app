---
title: "two two two song lyrics"
album: "Kaathuvaakula Rendu Kaadhal"
artist: "Anirudh Ravichander"
lyricist: "Vignesh Shivan"
director: "Vignesh Shivan"
path: "/albums/kaathuvaakula-rendu-kaadhal-lyrics"
song: "Two Two Two"
image: ../../images/albumart/kaathuvaakula-rendu-kaadhal.jpg
date: 2022-04-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Lyr6c84d5AI"
type: "love"
singers:
  - Anirudh Ravichander
  - Sunidhi Chauhan
  - Sanjana Kalmanje
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Two Two 2 Puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Two 2 Puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart Vittu Kudukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart Vittu Kudukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenda Nee Yaaru Sollenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenda Nee Yaaru Sollenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Three Oru Jodi Aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Three Oru Jodi Aaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna Irukanenaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Irukanenaika"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Dhaan Ennalordu Murugana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhaan Ennalordu Murugana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Summa Sumaar Moonji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Summa Sumaar Moonji"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumar Ungalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumar Ungalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhukonjam Overahdhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhukonjam Overahdhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruke Paa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruke Paa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rombo Rombo Nalla Paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rombo Rombo Nalla Paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Nambi Vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nambi Vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yena Mattun Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yena Mattun Paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Love U Sollen Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love U Sollen Pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Two Two Two Two Two
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Two Two Two Two"/>
</div>
<div class="lyrico-lyrics-wrapper">Two Two I Love You Too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Two I Love You Too"/>
</div>
<div class="lyrico-lyrics-wrapper">Nejamma I Love You Too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nejamma I Love You Too"/>
</div>
<div class="lyrico-lyrics-wrapper">Two Two Two Two Two Tdo Tdo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Two Two Two Two Tdo Tdo"/>
</div>
<div class="lyrico-lyrics-wrapper">Two Two I Love You Too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Two I Love You Too"/>
</div>
<div class="lyrico-lyrics-wrapper">Really I Love You Too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Really I Love You Too"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangamey Nu Sonniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamey Nu Sonniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Vairamey Nu Vazhinjiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vairamey Nu Vazhinjiye"/>
</div>
<div class="lyrico-lyrics-wrapper">How Can You Do This To Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How Can You Do This To Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Baby Nu Aasaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Nu Aasaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Konji Konji Kavuthiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konji Konji Kavuthiye"/>
</div>
<div class="lyrico-lyrics-wrapper">How Dare You Do This To Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How Dare You Do This To Me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avala Vittu Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avala Vittu Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vandhu Katti Kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vandhu Katti Kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekallam Ettuku Ettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekallam Ettuku Ettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Cut Outtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Cut Outtu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnakku Favourite’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnakku Favourite’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Naane Dhaana Solli Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naane Dhaana Solli Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhunga Kattu Pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhunga Kattu Pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaali Eh Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaali Eh Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No No 2 Two Two
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No 2 Two Two"/>
</div>
<div class="lyrico-lyrics-wrapper">No No 2 Two Two
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No 2 Two Two"/>
</div>
<div class="lyrico-lyrics-wrapper">Two Two Two Two Two Two Two
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Two Two Two Two Two Two"/>
</div>
<div class="lyrico-lyrics-wrapper">I Love You Too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Love You Too"/>
</div>
<div class="lyrico-lyrics-wrapper">Nejamma I Love You Too (Who?)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nejamma I Love You Too (Who?)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Two Two Two Two Two Tdo Tdo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Two Two Two Two Tdo Tdo"/>
</div>
<div class="lyrico-lyrics-wrapper">Two Two I Love You Too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Two I Love You Too"/>
</div>
<div class="lyrico-lyrics-wrapper">Really I Love You Too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Really I Love You Too"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Baby Thangamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Thangamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Endrum Venu’me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Endrum Venu’me"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraanga Anbaaga Irukalaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraanga Anbaaga Irukalaame"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaiyum Onaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaiyum Onaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Rendu Kozhandha Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Rendu Kozhandha Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum Eppodhum Naan Paathupene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum Eppodhum Naan Paathupene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Summa Sumaar Moonji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Summa Sumaar Moonji"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumar Ungalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumar Ungalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhukonjam Overahdhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhukonjam Overahdhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruke Paa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruke Paa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rombo Rombo Nalla Paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rombo Rombo Nalla Paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Nambi Vandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Nambi Vandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yena Mattun Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yena Mattun Paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Love U Sollen Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love U Sollen Pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Two Two Two Two Two
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Two Two Two Two"/>
</div>
<div class="lyrico-lyrics-wrapper">Two Two I Love You Too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Two I Love You Too"/>
</div>
<div class="lyrico-lyrics-wrapper">Nejamma I Love You Too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nejamma I Love You Too"/>
</div>
<div class="lyrico-lyrics-wrapper">Two Two Two Two Two Tdo Tdo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Two Two Two Two Tdo Tdo"/>
</div>
<div class="lyrico-lyrics-wrapper">Two Two I Love You Too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Two Two I Love You Too"/>
</div>
<div class="lyrico-lyrics-wrapper">I Love You Too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Love You Too"/>
</div>
</pre>
