---
title: "parandhu pogindren song lyrics"
album: "Kuthiraivaal"
artist: "Pradeep Kumar"
lyricist: "Prasath Ramar"
director: "Manoj Leonel Jahson - Shyam Sunder"
path: "/albums/kuthiraivaal-lyrics"
song: "Parandhu Pogindren"
image: ../../images/albumart/kuthiraivaal.jpg
date: 2022-03-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/R3_bOJqsCEs"
type: "happy"
singers:
  - Pradeep Kumar
  - Kalyani Nair
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Parandhu Pogindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu Pogindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraguillamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraguillamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithai Aakindran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithai Aakindran"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhiillamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhiillamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannalin Thagamay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannalin Thagamay!"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paadalin Ragamy!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadalin Ragamy!"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vanthu Serramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vanthu Serramal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Engu Pooveno!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Engu Pooveno!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannagal Illatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannagal Illatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Or Vannavil Naanay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or Vannavil Naanay!"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ennangal Neeroucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ennangal Neeroucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengu Putheyno!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengu Putheyno!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madisaaya Oodivaa!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madisaaya Oodivaa!"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayavaa!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayavaa!"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyatha Vanpol Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyatha Vanpol Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayavaa!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayavaa!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nillavanathal Pulanaalakiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillavanathal Pulanaalakiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vanthu Kaaya Thinam Thondiyay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vanthu Kaaya Thinam Thondiyay!"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithaam Theykirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithaam Theykirai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mayni Vaada!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mayni Vaada!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karrtodu Thee Aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karrtodu Thee Aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Velvi Seithaynay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Velvi Seithaynay!"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Bimbam Nan Sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Bimbam Nan Sera"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvindri Nindranay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvindri Nindranay!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parandhu Pogindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu Pogindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraguillamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraguillamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithai Aakindran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithai Aakindran"/>
</div>
<div class="lyrico-lyrics-wrapper">Mozhiillamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhiillamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannalin Thagamay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannalin Thagamay!"/>
</div>
<div class="lyrico-lyrics-wrapper">En Paadalin Ragamy!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadalin Ragamy!"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vanthu Serramal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vanthu Serramal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Engu Pooveno!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Engu Pooveno!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannagal Illatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannagal Illatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Or Vannavil Naanay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or Vannavil Naanay!"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ennangal Neeroucha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ennangal Neeroucha"/>
</div>
<div class="lyrico-lyrics-wrapper">Engengu Putheyno!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engengu Putheyno!"/>
</div>
</pre>
