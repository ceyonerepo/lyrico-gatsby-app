---
title: "jumbo jegadam song lyrics"
album: "Nadodigal"
artist: "Sundar C Babu"
lyricist: "Yugabharathi"
director: "Samuthrakani"
path: "/albums/nadodigal-lyrics"
song: "Jumbo Jegadam"
image: ../../images/albumart/nadodigal.jpg
date: 2009-06-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ywXL7AG4xlo"
type: "Mass"
singers:
  - Shankar Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagadam Jagadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagadam Jagadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagadaga Ram Tham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagadaga Ram Tham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagadam Jagadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagadam Jagadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagadam Ram Tham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagadam Ram Tham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagadam Jagada Ram Tham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagadam Jagada Ram Tham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagadam Jagadam Jagadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagadam Jagadam Jagadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ram Tham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ram Tham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagadam Jagadam Tham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagadam Jagadam Tham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagada Jagada Jaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Jagada Jaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaga Jaga Ramtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaga Jaga Ramtham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagada Jagada Jaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Jagada Jaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaga Jaga Ramtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaga Jaga Ramtham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagada Jagada Jaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Jagada Jaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaga Jaga Ramtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaga Jaga Ramtham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagadam Jagadam Tham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagadam Jagadam Tham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagada Jagada Jagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Jagada Jagada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagada Ramtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Ramtham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagada Jagada Jagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Jagada Jagada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagada Ramtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Ramtham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagada Jagada Jagada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Jagada Jagada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagada Ramtham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagada Ramtham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagadam Jagafam Tham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagadam Jagafam Tham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sambo Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambo Siva Sambo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siva Siva Sambo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siva Siva Sambo"/>
</div>
</pre>
