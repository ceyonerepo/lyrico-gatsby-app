---
title: 'rakita rakita song lyrics'
album: 'Jagame Thandhiram'
artist: 'Santhosh Narayanan'
lyricist: 'Vivek'
director: 'Karthik Subbaraj'
path: '/albums/jagame-thandhiram-song-lyrics'
song: 'Rakita rakita'
image: ../../images/albumart/jagame-thanthiram.jpg
date: 2020-07-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5UmaMK_OfPw"
type: 'mass'
singers: 
- Dhanush
- Dhee
- Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Rakita rakita rakita… oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita… oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakita rakita rakita …oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita …oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakita rakita rakita… oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita… oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakita rakita rakita… oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita… oooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey enna vena nadakattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey enna vena nadakattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sandhoshama irupaen
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan sandhoshama irupaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuru irukku verenna venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Usuru irukku verenna venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaasama irupaen
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullaasama irupaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enna vena nadakattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna vena nadakattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sandhoshama irupaen
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan sandhoshama irupaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuru irukku verenna venum
<input type="checkbox" class="lyrico-select-lyric-line" value="Usuru irukku verenna venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaasama irupaen
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullaasama irupaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edhaa punch-ah pottu udu maapla!..
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhaa punch-ah pottu udu maapla!.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enakku rajavaa naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku rajavaa naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku rajavaa naan
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku rajavaa naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku rajavaaaaa naan vaazhuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku rajavaaaaa naan vaazhuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum illanaalum aazhuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhuvum illanaalum aazhuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku rajavaaaaa naan vaazhuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku rajavaaaaa naan vaazhuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum illanaalum aazhuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhuvum illanaalum aazhuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey..rakita rakita rakita oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey..rakita rakita rakita oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakita rakita rakita oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakita rakita rakita oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakita rakita rakita oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita oooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey..rakita rakita rakita oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey..rakita rakita rakita oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakita rakita rakita oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakita rakita rakita oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakita rakita rakita
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naalu peru mathikkumpadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Naalu peru mathikkumpadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum irukkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum naanum irukkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam moodikittu avanga sonna
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam moodikittu avanga sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyiladhaan nadakkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhiyiladhaan nadakkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yae… avanukkaaga appadi vaazhnthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yae… avanukkaaga appadi vaazhnthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanukkaaga ippadi pesi
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivanukkaaga ippadi pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanukaaga appadi nadanthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Avanukaaga appadi nadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yae ivanukkaaga ippadi nadichu…
<input type="checkbox" class="lyrico-select-lyric-line" value="Yae ivanukkaaga ippadi nadichu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Hahahahha
<input type="checkbox" class="lyrico-select-lyric-line" value="Hahahahha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Shabaa….enna maapla landha?..
<input type="checkbox" class="lyrico-select-lyric-line" value="Shabaa….enna maapla landha?.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andha naalu peraiyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha naalu peraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvaraikkum paarthathilla naanu
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhuvaraikkum paarthathilla naanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku theva patta neram
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku theva patta neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha paradhesiya kaanum…. ohooo ohooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha paradhesiya kaanum…. ohooo ohooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enakku rajavaaaaa naan vaazhuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku rajavaaaaa naan vaazhuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum illanaalum aazhuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhuvum illanaalum aazhuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku rajavaaaaa naan vaazhuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku rajavaaaaa naan vaazhuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum illanaalum aazhuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhuvum illanaalum aazhuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey..rakita rakita rakita oooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey..rakita rakita rakita oooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakita rakita rakita awwwww
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita awwwww"/>
</div>
<div class="lyrico-lyrics-wrapper">Yay antha adiya thiruppi vidu pangu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yay antha adiya thiruppi vidu pangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edhoo onnu kodukka dhanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhoo onnu kodukka dhanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adutha naalum varuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adutha naalum varuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalladha naan eduthukitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Nalladha naan eduthukitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalladhu dhaan tharuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nalladhu dhaan tharuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambi oru kaala vappen
<input type="checkbox" class="lyrico-select-lyric-line" value="Nambi oru kaala vappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Inbamadhu nooru varum
<input type="checkbox" class="lyrico-select-lyric-line" value="Inbamadhu nooru varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhu vandhaalum purinjikitta
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhu vandhaalum purinjikitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazha oru thembu tharum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazha oru thembu tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edhu en thaguthi…
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhu en thaguthi…"/>
</div>
  <div class="lyrico-lyrics-wrapper">La la la laala lalaa
<input type="checkbox" class="lyrico-select-lyric-line" value="La la la laala lalaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nejama yaar naan…
<input type="checkbox" class="lyrico-select-lyric-line" value="Nejama yaar naan…"/>
</div>
  <div class="lyrico-lyrics-wrapper">La la lalaaa
<input type="checkbox" class="lyrico-select-lyric-line" value="La la lalaaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Who is me….
<input type="checkbox" class="lyrico-select-lyric-line" value="Who is me…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edhu en thaguthi…
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhu en thaguthi…"/>
</div>
  <div class="lyrico-lyrics-wrapper">Yaaru vandhu sollanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaru vandhu sollanum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nejama yaar naan…
<input type="checkbox" class="lyrico-select-lyric-line" value="Nejama yaar naan…"/>
</div>
  <div class="lyrico-lyrics-wrapper">En kitta dhaan kekkanum
<input type="checkbox" class="lyrico-select-lyric-line" value="En kitta dhaan kekkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enna thokkadikka oruthan mattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna thokkadikka oruthan mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvanae….ae….ae…aeh….aeh
<input type="checkbox" class="lyrico-select-lyric-line" value="Varuvanae….ae….ae…aeh….aeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna thokkadikka oruthan mattum varuvanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna thokkadikka oruthan mattum varuvanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannikanum maamsu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannikanum maamsu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada avanum inga naandhanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada avanum inga naandhanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada avanum inga naandhanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada avanum inga naandhanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rakita rakita rakita
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakita rakita rakita
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enakku rajavaaaaa naan vaazhuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku rajavaaaaa naan vaazhuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum illanaalum aazhuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhuvum illanaalum aazhuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakku rajavaaaaa naan vaazhuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku rajavaaaaa naan vaazhuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum illanaalum aazhuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhuvum illanaalum aazhuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey rakita rakita rakita ae ae
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey rakita rakita rakita ae ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakita rakita rakita ae ae
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita ae ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakita rakita rakita ae ae
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita ae ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Rakita rakita rakita ae ae
<input type="checkbox" class="lyrico-select-lyric-line" value="Rakita rakita rakita ae ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Duurrrrrrrrrrrrrrrrrr
<input type="checkbox" class="lyrico-select-lyric-line" value="Duurrrrrrrrrrrrrrrrrr"/>
</div>
<div class="lyrico-lyrics-wrapper">Yey yey yey yey…
<input type="checkbox" class="lyrico-select-lyric-line" value="Yey yey yey yey…"/>
</div>
<div class="lyrico-lyrics-wrapper">Awwww……………
<input type="checkbox" class="lyrico-select-lyric-line" value="Awwww……………"/>
</div>
</pre>