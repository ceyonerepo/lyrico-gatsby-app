---
title: "raani naan song lyrics"
album: "Veppam"
artist: "Joshua Sridhar"
lyricist: "Na. Muthukumar"
director: "Anjana"
path: "/albums/veppam-lyrics"
song: "Raani Naan"
image: ../../images/albumart/veppam.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Wj1zplpN3hY"
type: "happy"
singers:
  - Apoorva
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Rani Naa Maharani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Rani Naa Maharani"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan En Adima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan En Adima"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaya Ena Pallakkula Thooki Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaya Ena Pallakkula Thooki Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Rani Naa Maharani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rani Naa Maharani"/>
</div>
<div class="lyrico-lyrics-wrapper">En Theva Pudhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Theva Pudhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Ya Un Vegatha Nee Kaati Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Ya Un Vegatha Nee Kaati Po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Irukum Varayila Kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Irukum Varayila Kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamam Irukum Ulagila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam Irukum Ulagila"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegam Irukum Varaiyila Theeratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegam Irukum Varaiyila Theeratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhagam Irukum Manadhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhagam Irukum Manadhila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Rani Naa Maharani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Rani Naa Maharani"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan En Adima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan En Adima"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaya Ena Pallakkula Thooki Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaya Ena Pallakkula Thooki Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Rani Naa Maharani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rani Naa Maharani"/>
</div>
<div class="lyrico-lyrics-wrapper">En Theva Pudhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Theva Pudhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Ya Un Vegatha Nee Kaati Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Ya Un Vegatha Nee Kaati Po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Irukum Varayila Kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Irukum Varayila Kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamam Irukum Ulagila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam Irukum Ulagila"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegam Irukum Varaiyila Theeratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegam Irukum Varaiyila Theeratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhagam Irukum Manadhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhagam Irukum Manadhila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Vazhkayil Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Vazhkayil Ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Aayiram Kashtam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Aayiram Kashtam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vasalai Thattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vasalai Thattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Nogumae Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Nogumae Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaedhanai Theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaedhanai Theera"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaalibam Aara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaalibam Aara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Poduraen Maalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Poduraen Maalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena Sethuko Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ena Sethuko Dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adada Un Dhegam Kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Un Dhegam Kannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Nan Parthen Munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Nan Parthen Munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbam Odattum Pinaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbam Odattum Pinaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalargal Ellam Kiladi Kiladi Kiladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalargal Ellam Kiladi Kiladi Kiladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andru Mudhal Indru Varaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andru Mudhal Indru Varaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Irukum Varayila Kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Irukum Varayila Kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamam Irukum Ulagila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam Irukum Ulagila"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegam Irukum Varaiyila Theeratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegam Irukum Varaiyila Theeratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhagam Irukum Manadhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhagam Irukum Manadhila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadi En Valli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi En Valli"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Enna Kavarntha Kalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enna Kavarntha Kalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Unna Naanum Alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Unna Naanum Alli"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Odi Povaen Thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Odi Povaen Thalli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alli Nagar Malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli Nagar Malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Adika Veikara Milli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Adika Veikara Milli"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Needhaan Endhan Villi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Needhaan Endhan Villi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Oyae Oyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Oyae Oyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayyo Vaelayil Tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Vaelayil Tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Veetula Tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Veetula Tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu Roatula Tholaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Roatula Tholaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Vaegumae Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Vaegumae Manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oothiko Ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothiko Ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Thaethiko Mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Thaethiko Mella"/>
</div>
<div class="lyrico-lyrics-wrapper">Sruthi Yethiko Ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sruthi Yethiko Ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Kuraiyumae Ganam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Kuraiyumae Ganam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vegam Ilaamal Thoorathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam Ilaamal Thoorathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogam Ilaama Vaazhvaethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogam Ilaama Vaazhvaethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Eppodhum Theerathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Eppodhum Theerathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham Ilaama Munneru Munneru Munneru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham Ilaama Munneru Munneru Munneru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andru Mudhal Indru Varaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andru Mudhal Indru Varaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Irukum Varayila Kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Irukum Varayila Kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamam Irukum Ulagila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam Irukum Ulagila"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegam Irukum Varaiyila Theeratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegam Irukum Varaiyila Theeratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhagam Irukum Manadhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhagam Irukum Manadhila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Rani Naa Maharani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Rani Naa Maharani"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan En Adima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan En Adima"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaya Ena Pallakkula Thooki Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaya Ena Pallakkula Thooki Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Rani Naa Maharani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rani Naa Maharani"/>
</div>
<div class="lyrico-lyrics-wrapper">En Theva Pudhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Theva Pudhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Ya Un Vegatha Nee Kaati Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Ya Un Vegatha Nee Kaati Po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Irukum Varayila Kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Irukum Varayila Kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamam Irukum Ulagila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam Irukum Ulagila"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegam Irukum Varaiyila Theeratha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegam Irukum Varaiyila Theeratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhagam Irukum Manadhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhagam Irukum Manadhila"/>
</div>
</pre>
