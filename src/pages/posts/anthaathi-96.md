---
title: "anthaathi song lyrics"
album: "96"
artist: "Govind Vasantha"
lyricist: "Karthik Netha - C Premkumar"
director: "C Premkumar"
path: "/albums/96-lyrics"
song: "Anthaathi"
image: ../../images/albumart/96.jpg
date: 2018-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/29WzIwFvVdg"
type: "happy"
singers:
  - Chinmayi
  - Govind Vasantha
  - Bhadra Rajin
  - M Nassar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Peranbae kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peranbae kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulnookki adungindra aadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulnookki adungindra aadal"/>
</div>
<div class="lyrico-lyrics-wrapper">Saatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaratha aaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaratha aaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethetho saayal yetru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethetho saayal yetru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiriyum kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiriyum kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prathyega thedal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathyega thedal"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyil theeratha kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyil theeratha kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">Pul poondil puzhuvil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pul poondil puzhuvil "/>
</div>
<div class="lyrico-lyrics-wrapper">ulathil ilathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulathil ilathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae ellamum aagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae ellamum aagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam kaanum aruvamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam kaanum aruvamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithyaathi kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithyaathi kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Illatha pothum thedum thedal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illatha pothum thedum thedal"/>
</div>
<div class="lyrico-lyrics-wrapper">Satha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarathu kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarathu kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Manraadum pothum maatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manraadum pothum maatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuthil mothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuthil mothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalatha oodal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalatha oodal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaaaaa aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaaaaa aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa aaa aaaaaaaa aaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaaaaaaa aaa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam intha theeyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam intha theeyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu kattum theekuchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu kattum theekuchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam intha kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam intha kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonjal kattum thoosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal kattum thoosi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam intha neeril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam intha neeril"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai oottum neer poochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai oottum neer poochi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam intha kaambil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam intha kaambil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamathin rusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamathin rusi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal kanneril silanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kanneril silanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vinmeenin meganthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vinmeenin meganthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal meiyana vathanthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal meiyana vathanthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam thorum thodarum diary
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam thorum thodarum diary"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal deiveega ethiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal deiveega ethiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal saathaanin visiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal saathaanin visiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal aanmavin pulari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal aanmavin pulari"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhnthu petra degree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhnthu petra degree"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orr"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaikullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaikullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinaavellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinaavellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathunguthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathunguthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Karainthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karainthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marainthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marainthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinthathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinthathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum pooranamae vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum pooranamae vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ee ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ee ee"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjum ezhilisaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjum ezhilisaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Panja varna bootham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panja varna bootham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam niraiyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam niraiyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanbathellaamaa kaadhaladi ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanbathellaamaa kaadhaladi ee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalae kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani perun thunaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani perun thunaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda vaa kooda vaaPothum pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda vaa kooda vaaPothum pothum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalae kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvin neelam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvin neelam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogalam poga vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogalam poga vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee eeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee eeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thigampari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thigampari"/>
</div>
<div class="lyrico-lyrics-wrapper">Valampuri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valampuri"/>
</div>
<div class="lyrico-lyrics-wrapper">Suyambu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suyambu nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prahaaram nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prahaaram nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Prabaavam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prabaavam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pravaaham nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pravaaham nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Srungaaram nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Srungaaram nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aangaaram nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aangaaram nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oangaaram nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oangaaram nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthaathi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthaathi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthaathi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthaathi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthaathi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthaathi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Theda vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theda vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mun arivi pindri varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mun arivi pindri varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athan varugaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athan varugaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam urakka sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam urakka sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal oru naal ungalaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal oru naal ungalaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu adaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu adaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai alli anaithu kollungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai alli anaithu kollungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbaaga paarthu kollungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbaaga paarthu kollungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal thangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal thangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal thayangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal thayangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal sirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal sirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal inikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal kavithaigal varaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kavithaigal varaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kalangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kalangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kuzhambum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kuzhambum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal oralavukku puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal oralavukku puriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vilagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vilagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal piriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal piriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhavugalai moodaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhavugalai moodaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi anupungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi anupungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru velai kaadhal thirumbinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru velai kaadhal thirumbinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorathil thayangi nindraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorathil thayangi nindraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil selungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil selungal"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbudan pesungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbudan pesungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhaal ungal vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhaal ungal vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam kaadhal vasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam kaadhal vasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Matrangal vinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matrangal vinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Matrangalae vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matrangalae vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal"/>
</div>
</pre>
