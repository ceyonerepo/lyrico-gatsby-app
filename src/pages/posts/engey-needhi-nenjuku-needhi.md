---
title: "engey needhi song lyrics"
album: "Nenjuku Needhi"
artist: "Dhibu Ninan Thomas"
lyricist: "Yugabharathi"
director: "Arunraja Kamaraj"
path: "/albums/nenjuku-needhi-lyrics"
song: "Engey Needhi"
image: ../../images/albumart/nenjuku-needhi.jpg
date: 2022-05-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rfyiesTlzUQ"
type: "sad"
singers:
  - Ananya Bhat
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">enge neethi kaanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge neethi kaanama"/>
</div>
<div class="lyrico-lyrics-wrapper">thembuthe boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thembuthe boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">onnath thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnath thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kai yenthum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai yenthum "/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai yen saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai yen saami"/>
</div>
<div class="lyrico-lyrics-wrapper">ethetho thadangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethetho thadangale"/>
</div>
<div class="lyrico-lyrics-wrapper">engeyum edanjale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyum edanjale"/>
</div>
<div class="lyrico-lyrics-wrapper">kaneeril moolginaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaneeril moolginaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathidaamal paarpatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathidaamal paarpatho"/>
</div>
<div class="lyrico-lyrics-wrapper">ellarum umpulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellarum umpulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kanakkup puriyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanakkup puriyaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">yaar mela yaar keezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaar mela yaar keezha"/>
</div>
<div class="lyrico-lyrics-wrapper">onakkum theriyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onakkum theriyaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nithamum ona vendiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nithamum ona vendiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vali theerave illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali theerave illa"/>
</div>
<div class="lyrico-lyrics-wrapper">ethana vaga saangiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethana vaga saangiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">senja pothume nee valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senja pothume nee valla"/>
</div>
<div class="lyrico-lyrics-wrapper">theendaama paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theendaama paakura"/>
</div>
<div class="lyrico-lyrics-wrapper">aala nee kaaval kaakatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala nee kaaval kaakatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathoda naathamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathoda naathamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">engala neyum mathatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engala neyum mathatha"/>
</div>
<div class="lyrico-lyrics-wrapper">thee kooda theetaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee kooda theetaache"/>
</div>
<div class="lyrico-lyrics-wrapper">kan moodi nikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan moodi nikkatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enge neethi kaanama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enge neethi kaanama"/>
</div>
<div class="lyrico-lyrics-wrapper">thembuthe boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thembuthe boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">onnath thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnath thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kai yenthum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai yenthum "/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai yen saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai yen saami"/>
</div>
<div class="lyrico-lyrics-wrapper">ethetho thadangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethetho thadangale"/>
</div>
<div class="lyrico-lyrics-wrapper">engeyum edanjale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyum edanjale"/>
</div>
<div class="lyrico-lyrics-wrapper">kaneeril moolginaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaneeril moolginaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathidaamal paarpatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathidaamal paarpatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanneere illatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanneere illatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam porakaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam porakaathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kai meerum aagasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai meerum aagasam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanak kidaikaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanak kidaikaathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanneere illatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanneere illatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam porakaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam porakaathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kai meerum aagasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai meerum aagasam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanak kidaikaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanak kidaikaathaa"/>
</div>
</pre>
