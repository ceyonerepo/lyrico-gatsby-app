---
title: "ennai theadi song lyrics"
album: "Monster"
artist: "Justin Prabhakaran"
lyricist: "Sankardaas"
director: "Nelson Venkatesan"
path: "/albums/monster-lyrics"
song: "Ennai Theadi"
image: ../../images/albumart/monster.jpg
date: 2019-05-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wAIKGWUOoK4"
type: "love"
singers:
  - Sean Roldan
  - Shalini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennai Thedi Megam Vandhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thedi Megam Vandhathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Jannnal Thedi Vaanam Nindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Jannnal Thedi Vaanam Nindrathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril Aadum Poovai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Aadum Poovai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Saindhu Saindhu Kaadhal Pesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Saindhu Saindhu Kaadhal Pesuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravaa Pagalaa Velagaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravaa Pagalaa Velagaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire Thuliyum Piriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Thuliyum Piriyaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkai Athu Vaasal Thirakkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Athu Vaasal Thirakkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasanthamum Azhaikkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasanthamum Azhaikkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kanda Kanavum Pazhikkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kanda Kanavum Pazhikkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kann Munne Nadakkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kann Munne Nadakkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singarama Veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singarama Veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru Paravai Vaazhum Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Paravai Vaazhum Koodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oiyarama Parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oiyarama Parappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parappom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalara Vaa Nadappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalara Vaa Nadappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Kathaigal Pesi Kidappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Kathaigal Pesi Kidappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatharama Iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatharama Iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andril Endrum Vilagadhathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andril Endrum Vilagadhathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrai Ondru Piriyadhu Andha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrai Ondru Piriyadhu Andha"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbum Endrum Kuraiyathiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbum Endrum Kuraiyathiruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannum Kannum Kalantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannum Kannum Kalantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Rendum Onna Iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Rendum Onna Iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Inbam Thunbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Inbam Thunbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Purindhu Layippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purindhu Layippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senthazham Poo Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senthazham Poo Vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Nadanthu Vandha Veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Nadanthu Vandha Veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondattama Pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondattama Pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manjal Thanga Nilava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal Thanga Nilava"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Konjum Pullanguzhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Konjum Pullanguzhala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu Ninna Azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Ninna Azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Thedi Megam Vandhathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thedi Megam Vandhathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Jannnal Thedi Vaanam Nindrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Jannnal Thedi Vaanam Nindrathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril Aadum Poovai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Aadum Poovai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai Saindhu Saindhu Kaadhal Pesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai Saindhu Saindhu Kaadhal Pesuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravaa Pagalaa Velagaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravaa Pagalaa Velagaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire Thuliyum Piriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire Thuliyum Piriyaama"/>
</div>
</pre>
