---
title: 'andha kanna paathaka song lyrics'
album: 'Master'
artist: 'Anirudh Ravichander'
lyricist: 'Vignesh Sivan'
director: 'Lokesh Kanagaraj'
path: '/albums/master-song-lyrics'
song: 'Andha Kanna Paathaka'
image: ../../images/albumart/master.jpg
date: 2021-01-13
youtubeLink: 'https://www.youtube.com/embed/3hVc3M1IEe0'
type: 'love'
lang: tamil
singers: 
- Yuvan Shankar Raja
---



<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Andha kanna paarthaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha kanna paarthaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-u thaana thonaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Love-u thaana thonaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kitta ponaaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan kitta ponaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam maana maaradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Manam maana maaradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Agamellaam avan dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Agamellaam avan dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan dhaan irundhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan dhaan irundhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthaal avan kanavellaamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadanthaal avan kanavellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan mugam thaanae…ae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan mugam thaanae…ae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Alagan thaan avan dhaan avan dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Alagan thaan avan dhaan avan dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaga alava avan siripaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Alaga alava avan siripaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada alagan thaanae…ae…ae…ae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada alagan thaanae…ae…ae…ae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Po poola manasu… yeratha vayasu
<input type="checkbox" class="lyrico-select-lyric-line" value="Po poola manasu… yeratha vayasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavamda namma girlsu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paavamda namma girlsu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathaapu sirippu maaratha nadappu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mathaapu sirippu maaratha nadappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Class-aana master maasu
<input type="checkbox" class="lyrico-select-lyric-line" value="Class-aana master maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattasu paarvai pattaalae podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pattasu paarvai pattaalae podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Fail aana heart-u pass-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Fail aana heart-u pass-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Single dhaan ma news
<input type="checkbox" class="lyrico-select-lyric-line" value="Single dhaan ma news"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaan ma chance-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu thaan ma chance-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andha kanna paarthaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha kanna paarthaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-u thaana thonaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Love-u thaana thonaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kitta ponaaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan kitta ponaaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andha kanna paarthaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha kanna paarthaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-u thaana thonaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Love-u thaana thonaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kitta ponaaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan kitta ponaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam maana maaradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Manam maana maaradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Agamellaam avan dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Agamellaam avan dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan dhaan irundhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan dhaan irundhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthaal avan kanavellaamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadanthaal avan kanavellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan mugam thaanae…ae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan mugam thaanae…ae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Alagan thaan avan dhaan avan dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Alagan thaan avan dhaan avan dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaga alava avan siripaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Alaga alava avan siripaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada alagan thaanae…ae…ae…ae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada alagan thaanae…ae…ae…ae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Po poola manasu… yeratha vayasu
<input type="checkbox" class="lyrico-select-lyric-line" value="Po poola manasu… yeratha vayasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paavamda namma girlsu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paavamda namma girlsu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathaapu sirippu maaratha nadappu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mathaapu sirippu maaratha nadappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Class-aana master maasu
<input type="checkbox" class="lyrico-select-lyric-line" value="Class-aana master maasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattasu paarvai pattaalae podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pattasu paarvai pattaalae podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Fail aana heart-u pass-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Fail aana heart-u pass-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Single dhaan ma news
<input type="checkbox" class="lyrico-select-lyric-line" value="Single dhaan ma news"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu thaan ma chance-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu thaan ma chance-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andha kanna paarthaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha kanna paarthaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-u thaana thonaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Love-u thaana thonaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kitta ponaaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan kitta ponaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam maana maaradha (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Manam maana maaradha"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haa…aaa…aa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa…aaa…aa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Love-u thaana thonaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Love-u thaana thonaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Agamellaam avan dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Agamellaam avan dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan dhaan irundhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan dhaan irundhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadanthaal avan kanavellaamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadanthaal avan kanavellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan mugam thaanae…ae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan mugam thaanae…ae…"/>
</div>
<div class="lyrico-lyrics-wrapper">Alagan thaan avan dhaan avan dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Alagan thaan avan dhaan avan dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaga alava avan siripaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Alaga alava avan siripaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada alagan thaanae…ae…ae…ae…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada alagan thaanae…ae…ae…ae…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Natpaana paarvai nidhaana pechu
<input type="checkbox" class="lyrico-select-lyric-line" value="Natpaana paarvai nidhaana pechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaarkum pudichu pochu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaarkum pudichu pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Magnet-u eerpu rombha dhaan sharpu
<input type="checkbox" class="lyrico-select-lyric-line" value="Magnet-u eerpu rombha dhaan sharpu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum master topp-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Eppodhum master topp-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhoo un power-u yedhoo un thimiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Yedhoo un power-u yedhoo un thimiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhum irukkum paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Eppodhum irukkum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Solo-va ninna yengaatha ponnaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Solo-va ninna yengaatha ponnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andha kanna paarthaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha kanna paarthaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-u thaana thonaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Love-u thaana thonaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kitta ponaaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan kitta ponaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam maana maaradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Manam maana maaradha"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha kanna paarthaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha kanna paarthaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-u thaana thonaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Love-u thaana thonaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kitta ponaaka
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan kitta ponaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam maana maaradha
<input type="checkbox" class="lyrico-select-lyric-line" value="Manam maana maaradha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Haa…aaa…aa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Haa…aaa…aa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Love-u thaana thonaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Love-u thaana thonaadha"/>
</div>
</pre>