---
title: "pirivondrai santhithen song lyrics"
album: "Pririyadha Varam Vendum"
artist: "S. A. Rajkumar"
lyricist: "Arivumathi"
director: "Kamal"
path: "/albums/pririyadha-varam-vendum-lyrics"
song: "Pirivondrai Santhithen"
image: ../../images/albumart/piriyadha-varam-vendum.jpg
date: 2001-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qg5sYuOCCNk"
type: "sad"
singers:
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pirivondrai Santhithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivondrai Santhithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthal Muthal Netru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Muthal Netru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuraiyeeral Theendamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuraiyeeral Theendamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumbuthu Kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbuthu Kaatru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Endra Thooram Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Endra Thooram Varai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelaatho Enthan Kudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelaatho Enthan Kudai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nan Endra Neram Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan Endra Neram Varai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooraatho Unthan Mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooraatho Unthan Mazhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ododi Vaaraaiyo Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ododi Vaaraaiyo Anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae Anbae Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae Anbae Anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae Anbae Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae Anbae Anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirivondrai Santhithaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivondrai Santhithaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthal Muthal Netru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Muthal Netru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuraiyeeral Theendamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuraiyeeral Theendamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumbuthu Kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbuthu Kaatru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Vari Nee Oru Vari Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vari Nee Oru Vari Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirukkuralaai Unnai Sonnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirukkuralaai Unnai Sonnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thani Thaniye Pirithuvaitthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani Thaniye Pirithuvaitthaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porul Tharumo Kavithai Ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porul Tharumo Kavithai Ingae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kaigal Endrae Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaigal Endrae Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudaikkindra Kaikuttai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudaikkindra Kaikuttai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thotta Adiyaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thotta Adiyaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alikaathu En Sattai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alikaathu En Sattai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Nane Thedi Ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nane Thedi Ponen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirivil Naane Neeyai Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivil Naane Neeyai Aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirivondrai Santhithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivondrai Santhithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthal Muthal Netru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Muthal Netru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuraiyeeral Theendamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuraiyeeral Theendamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumbuthu Kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbuthu Kaatru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keezh Imai Naan Mel Imai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keezh Imai Naan Mel Imai Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirinthathanaal Purindhathillai Kanne Kanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirinthathanaal Purindhathillai Kanne Kanne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mel Imai Nee Pirinthathanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mel Imai Nee Pirinthathanaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Purinthu Konden Kadhal Endrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purinthu Konden Kadhal Endrae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam Piranthanaalil Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Piranthanaalil Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammai Naan Unarnthenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammai Naan Unarnthenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam Piranthanaalildhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Piranthanaalildhan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Kadhal Thiranthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Kadhal Thiranthene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam Engum Neeye Neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam Engum Neeye Neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyirin Thaagam Kadhal Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirin Thaagam Kadhal Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirivondrai Santhithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivondrai Santhithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthal Muthal Netru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthal Muthal Netru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuraiyeeral Theendamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuraiyeeral Theendamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirumbuthu Kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbuthu Kaatru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Nindra Thooram Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nindra Thooram Varai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelatho Endhan Kudai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelatho Endhan Kudai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nan Endra Neram Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nan Endra Neram Varai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooraatho Unthan Mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooraatho Unthan Mazhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ododi Varaayo Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ododi Varaayo Anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Anbe Anbe Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Anbe Anbe Anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Anbe Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Anbe Anbe"/>
</div>
</pre>
