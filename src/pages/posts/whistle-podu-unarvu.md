---
title: "whistle podu song lyrics"
album: "Unarvu"
artist: "Nakul Abhyankar"
lyricist: "Mohammed Raja"
director: "Subu Venkat"
path: "/albums/unarvu-lyrics"
song: "Whistle Podu"
image: ../../images/albumart/unarvu.jpg
date: 2019-07-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ko6wjI43eYE"
type: "happy"
singers:
  - Bamba Bakya
  - Nakul Abhyankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Verameella Thoranamnnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verameella Thoranamnnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Mettukkatti Paadikinnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mettukkatti Paadikinnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Moththa Janam Kalandhukinnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththa Janam Kalandhukinnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Maalaiyathan Podu Onnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalaiyathan Podu Onnu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pambarama Suththikkittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pambarama Suththikkittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thoranatha Thonga Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranatha Thonga Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Meesaiya Than Murukkikinnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesaiya Than Murukkikinnu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Visulu Poduda Ra He I 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Poduda Ra He I "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Podye Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Podye Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Summa Pesikkinnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Summa Pesikkinnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Hair Colour Pannikkinnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hair Colour Pannikkinnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Gaja Beedi Onnu Onnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaja Beedi Onnu Onnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayila Voothu Jinnu Jinnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayila Voothu Jinnu Jinnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Sannakkonni Meenu Kannu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sannakkonni Meenu Kannu "/>
</div>
<div class="lyrico-lyrics-wrapper">Asaltu Pannikkinnu Geththu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asaltu Pannikkinnu Geththu "/>
</div>
<div class="lyrico-lyrics-wrapper">Katti Vanthu Ninnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti Vanthu Ninnu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Visulu Podyeeeeee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Podyeeeeee "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Podye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Podye "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aey Enakkunna Unakkunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aey Enakkunna Unakkunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha Bantham Irukkunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha Bantham Irukkunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Sollamalay Poiduvaan Kaasu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamalay Poiduvaan Kaasu "/>
</div>
<div class="lyrico-lyrics-wrapper">Panama Pochunna Oravaathan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panama Pochunna Oravaathan "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaikirom Unkoodathan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaikirom Unkoodathan "/>
</div>
<div class="lyrico-lyrics-wrapper">Irukurom Onnu Manna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukurom Onnu Manna "/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhakikinna Moththa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhakikinna Moththa "/>
</div>
<div class="lyrico-lyrics-wrapper">Usura Kodukkurom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Kodukkurom "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Visulu Podyeeeeee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Podyeeeeee "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Podye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Podye "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Podye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Podye "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Podye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Podye "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumthalakadi Gumalaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumthalakadi Gumalaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Aithalakadi Vavalye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aithalakadi Vavalye "/>
</div>
<div class="lyrico-lyrics-wrapper">Gumthalakadi Gumalaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumthalakadi Gumalaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Aithalakadi Vavalye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aithalakadi Vavalye "/>
</div>
<div class="lyrico-lyrics-wrapper">Bigilu Bigilu Bigilu Bigilu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bigilu Bigilu Bigilu Bigilu "/>
</div>
<div class="lyrico-lyrics-wrapper">Gumthalakadi Gumalaye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumthalakadi Gumalaye "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Aithalakadi Vavalye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aithalakadi Vavalye "/>
</div>
<div class="lyrico-lyrics-wrapper">Anbukkuthan Panjam Illa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbukkuthan Panjam Illa "/>
</div>
<div class="lyrico-lyrics-wrapper">Alpathanam Pannathilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alpathanam Pannathilla "/>
</div>
<div class="lyrico-lyrics-wrapper">Erangungada Kothavula 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangungada Kothavula "/>
</div>
<div class="lyrico-lyrics-wrapper">Ettuthikkum Enga Mazhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettuthikkum Enga Mazhai "/>
</div>
<div class="lyrico-lyrics-wrapper">Saathi Matham Parthathilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathi Matham Parthathilla "/>
</div>
<div class="lyrico-lyrics-wrapper">Sandaikuthan Ponathilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaikuthan Ponathilla "/>
</div>
<div class="lyrico-lyrics-wrapper">Joly Panni Kondaadalam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joly Panni Kondaadalam "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada Munnala Aye Pallikudam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada Munnala Aye Pallikudam "/>
</div>
<div class="lyrico-lyrics-wrapper">Ponathilla MA,BL,FMS, ah Parthavanthalla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponathilla MA,BL,FMS, ah Parthavanthalla "/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Thala Thanga Sela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Thala Thanga Sela "/>
</div>
<div class="lyrico-lyrics-wrapper">Sonna Solla Meiyaluma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna Solla Meiyaluma "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senjuduvarula Aanna Unna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjuduvarula Aanna Unna "/>
</div>
<div class="lyrico-lyrics-wrapper">Dagulukaati Kalakiduvome 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dagulukaati Kalakiduvome "/>
</div>
<div class="lyrico-lyrics-wrapper">Mama Machan Pola Vanthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Machan Pola Vanthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhakikinnale Motta Gosu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhakikinnale Motta Gosu "/>
</div>
<div class="lyrico-lyrics-wrapper">Podi Maasu Unakku Munnalay 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi Maasu Unakku Munnalay "/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Kaiyila Vanthu Nalla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Kaiyila Vanthu Nalla "/>
</div>
<div class="lyrico-lyrics-wrapper">Maattikinnalay 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikinnalay "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Podyeeeeee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Podyeeeeee "/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Visulu Podyeeeeee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Visulu Podyeeeeee "/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Visulu Hey Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Visulu Hey Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Visulu Podyeeeeee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Visulu Podyeeeeee "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Danga Punga Babuku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Danga Punga Babuku "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaru Dhillana Dubukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaru Dhillana Dubukku "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaru Kummalama Adiparu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaru Kummalama Adiparu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayikiliya Pesiparu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayikiliya Pesiparu "/>
</div>
<div class="lyrico-lyrics-wrapper">Jilakulla Singam Paaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilakulla Singam Paaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Gantil Man Ah Nadanthupaaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gantil Man Ah Nadanthupaaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Belli Vacha Allaparu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Belli Vacha Allaparu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Visulu Poduda He I 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Poduda He I "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Podye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Podye "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Podye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Podye "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Podye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Podye "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Podye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Podye "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Podye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Podye "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Podye Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Podye Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Visulu Visulu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Visulu Visulu "/>
</div>
<div class="lyrico-lyrics-wrapper">Visulu Visulu Podye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visulu Visulu Podye "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Summa Pesikkinnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Summa Pesikkinnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Hair Colour Pannikkinnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hair Colour Pannikkinnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Gaja Beedi Onnu Onnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaja Beedi Onnu Onnu "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayila Voothu Jinnu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayila Voothu Jinnu "/>
</div>
</pre>
