---
title: "kabaddi kabaddi female version song lyrics"
album: "Kennedy Club"
artist: "D. Imman"
lyricist: "Viveka"
director: "Suseenthiran"
path: "/albums/kennedy-club-lyrics"
song: "Kabaddi Kabaddi Female Version"
image: ../../images/albumart/kennedy-club.jpg
date: 2019-08-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/PEsxexsP0Sc"
type: "mass"
singers:
  - Reshmi Sateesh
  - Srinidhi
  - Suriya Badrinath	
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaettai Thamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaettai Thamizhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Verithanam Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verithanam Idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veera Maraththi Porkalam Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera Maraththi Porkalam Idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koondhal Mudichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondhal Mudichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthikuthu Puyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthikuthu Puyal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemtharigida Bharathi Magal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemtharigida Bharathi Magal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaan Adakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaan Adakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen Adanganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Adanganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthivechavan Yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthivechavan Yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankalavida Pen Pala Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankalavida Pen Pala Padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeram Kondava Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram Kondava Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oor Patharattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Patharattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aal Sitharattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aal Sitharattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai Katharattum Aththumeeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai Katharattum Aththumeeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaal Nazhuvattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaal Nazhuvattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Thaluvattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Thaluvattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Veri-Aattam Aadu Aadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Veri-Aattam Aadu Aadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veera Thamizhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera Thamizhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethachu Vittadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethachu Vittadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhi Thamizhan Anuppi Vachathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhi Thamizhan Anuppi Vachathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettaigalin Adaiyaalam Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettaigalin Adaiyaalam Idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veril Irukkum Sarithiram Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veril Irukkum Sarithiram Idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethiri Idathil Egiri Adikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiri Idathil Egiri Adikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadam Edukkum Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadam Edukkum Paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethana Peru Valachchapothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethana Peru Valachchapothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiri Vanthu Nee Seru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiri Vanthu Nee Seru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pulikutti-Pol Thodaithatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pulikutti-Pol Thodaithatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Pagai Netti Thallivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Pagai Netti Thallivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ilavattam Thee Paravattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ilavattam Thee Paravattum"/>
</div>
<div class="lyrico-lyrics-wrapper">On Olivattam Enna Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On Olivattam Enna Kattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kuththula En Kuththula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kuththula En Kuththula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai Veththala Podadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai Veththala Podadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Nettuna Naan Muttuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Nettuna Naan Muttuna"/>
</div>
<div class="lyrico-lyrics-wrapper">On Kattidam Saiyaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="On Kattidam Saiyaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee Pathina Mannenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Pathina Mannenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola Dhaan Veriyaa Irukkom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola Dhaan Veriyaa Irukkom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Geththodaa Suthaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Geththodaa Suthaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Odamba Veragaa Murippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odamba Veragaa Murippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottapulla Sadugudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottapulla Sadugudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhaikka Matta Vazhi Vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhaikka Matta Vazhi Vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamanthiyaa Ninachathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamanthiyaa Ninachathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sammattiyaa Norukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sammattiyaa Norukkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabadi Kabadi Kabadi Kabadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabadi Kabadi Kabadi Kabadi"/>
</div>
</pre>
