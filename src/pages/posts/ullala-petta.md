---
title: 'ullaallaa song lyrics'
album: 'Petta'
artist: 'Anirudh Ravichander'
lyricist: 'Vivek'
director: 'Karthik Subbaraj'
path: '/albums/petta-song-lyrics'
song: 'Ullaallaa'
image: ../../images/albumart/petta.jpg
date: 2019-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oLgzs8nut3A"
type: 'festival'
singers: 
- Nakash Aziz
- Inno Genga
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Hey eththanai santhosam
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey eththanai santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum kottuthu unmela
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinamum kottuthu unmela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee manasu vachuputta
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee manasu vachuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasikka mudiyum unnaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Rasikka mudiyum unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee sindhura kanneerum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee sindhura kanneerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga nerantharam illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Inga nerantharam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu purinjikittaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu purinjikittaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae neethanda aala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ingae neethanda aala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey eththanai santhosam
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey eththanai santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum kottuthu unmela
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinamum kottuthu unmela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee manasu vachuputta
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee manasu vachuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasikka mudiyum unnaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Rasikka mudiyum unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee sindhura kanneerum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee sindhura kanneerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga nerantharam illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Inga nerantharam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu purinjikittaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu purinjikittaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae neethanda aala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ingae neethanda aala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kanna kattikittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanna kattikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam iruttunu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ellaam iruttunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee koovatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee koovatha"/>
</div>
  <div class="lyrico-lyrics-wrapper">Koovaathappa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Koovaathappa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vattam pottukittu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vattam pottukittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna ulagathil
<input type="checkbox" class="lyrico-select-lyric-line" value="Chinna ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaazhaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee vaazhaatha"/>
</div>
  <div class="lyrico-lyrics-wrapper">Vaazhaathappa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhaathappa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennai paar
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaiya thatta
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan kaiya thatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaachu ulagam
<input type="checkbox" class="lyrico-select-lyric-line" value="Undaachu ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey naan sonna pakkam
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey naan sonna pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkaama sozhalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nikkaama sozhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei en kooda sernthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dei en kooda sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothaadum nezhalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Koothaadum nezhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaara eppodhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullaara eppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaallaa ullaallaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullaallaa ullaallaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paar naan kaiya thatta
<input type="checkbox" class="lyrico-select-lyric-line" value="Paar naan kaiya thatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaachu ulagam
<input type="checkbox" class="lyrico-select-lyric-line" value="Undaachu ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey naan sonna pakkam
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey naan sonna pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkaama sozhalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nikkaama sozhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei en kooda sernthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dei en kooda sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothaadum nezhalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Koothaadum nezhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaara eppodhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullaara eppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaallaa ullaallaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullaallaa ullaallaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ribararibarey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribararibarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ribaarey ribararibarey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribaarey ribararibarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ribararibarey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribararibarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ribaarey ribararibarey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribaarey ribararibarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ribararibarey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribararibarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ribaarey ribararibarey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribaarey ribararibarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ribararibarey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribararibarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ribaarey ribapaa rabapapa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribaarey ribapaa rabapapa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unakkaaga nillu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakkaaga nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvenum sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethuvenum sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosam kudukaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Santhosam kudukaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhunaalum thallu
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhunaalum thallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Asaraadha dhillu
<input type="checkbox" class="lyrico-select-lyric-line" value="Asaraadha dhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthaa nee sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="Irunthaa nee sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">En aalu raaja nee
<input type="checkbox" class="lyrico-select-lyric-line" value="En aalu raaja nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En kooda nillu
<input type="checkbox" class="lyrico-select-lyric-line" value="En kooda nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unakkaaga nillu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakkaaga nillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvenum sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethuvenum sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosam kudukaatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Santhosam kudukaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhunaalum thallu
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhunaalum thallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Asaraadha dhillu
<input type="checkbox" class="lyrico-select-lyric-line" value="Asaraadha dhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthaa nee sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="Irunthaa nee sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">En aalu raaja nee
<input type="checkbox" class="lyrico-select-lyric-line" value="En aalu raaja nee"/>
</div>
<div class="lyrico-lyrics-wrapper">En kooda nillu
<input type="checkbox" class="lyrico-select-lyric-line" value="En kooda nillu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaiyil kedachathu tholanjaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaiyil kedachathu tholanjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum romba pudichathu kedaikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Innum romba pudichathu kedaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana…aasai adakkida therinjaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aana…aasai adakkida therinjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga ellaam kaaladiyil kedakkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Inga ellaam kaaladiyil kedakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennai paar
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaiya thatta
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan kaiya thatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaachu ulagam
<input type="checkbox" class="lyrico-select-lyric-line" value="Undaachu ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey naan sonna pakkam
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey naan sonna pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkaama sozhalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nikkaama sozhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei en kooda sernthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dei en kooda sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothaadum nezhalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Koothaadum nezhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaara eppodhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullaara eppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaallaa ullaallaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullaallaa ullaallaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paar naan kaiya thatta
<input type="checkbox" class="lyrico-select-lyric-line" value="Paar naan kaiya thatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaachu ulagam
<input type="checkbox" class="lyrico-select-lyric-line" value="Undaachu ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey naan sonna pakkam
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey naan sonna pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkaama sozhalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nikkaama sozhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dei en kooda sernthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dei en kooda sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothaadum nezhalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Koothaadum nezhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaara eppodhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullaara eppodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaallaa ullaallaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Ullaallaa ullaallaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey eththanai santhosam
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey eththanai santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinamum kottuthu unmela
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinamum kottuthu unmela"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee manasu vachuputta
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee manasu vachuputta"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasikka mudiyum unnaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Rasikka mudiyum unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee sindhura kanneerum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee sindhura kanneerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga nerantharam illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Inga nerantharam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu purinjikittaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu purinjikittaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingae neethanda aala
<input type="checkbox" class="lyrico-select-lyric-line" value="Ingae neethanda aala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ribararibarey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribararibarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ribaarey ribararibarey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribaarey ribararibarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ribararibarey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribararibarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ribaarey ribararibarey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribaarey ribararibarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ribararibarey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribararibarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ribaarey ribararibarey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribaarey ribararibarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ribararibarey
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribararibarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ribaarey ribapaa rabapapa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ribaarey ribapaa rabapapa"/>
</div>
</pre>