---
title: 'blood bath song lyrics'
album: 'Asuran'
artist: 'G.V. Prakash Kumar'
lyricist: 'Arunraja Kamaraj'
director: 'Vetrimaaran'
path: '/albums/asuran-song-lyrics'
song: 'Blood Bath'
image: ../../images/albumart/asuran.jpg
date: 2019-10-04
singers: 
- Arunraja Kamaraj
lang: tamil
youtubeLink: "https://www.youtube.com/embed/x7qwz_1TjLk"
type: 'mass'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Vaa ethiril vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa ethiril vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirpadum nodiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethirpadum nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaigal sidhara
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaigal sidhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa viratti vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa viratti vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virattidum virattil
<input type="checkbox" class="lyrico-select-lyric-line" value="Virattidum virattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagaigal kadhara
<input type="checkbox" class="lyrico-select-lyric-line" value="Pagaigal kadhara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa azhikka vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa azhikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Olikka vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Olikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyavar kulaigal nadunga
<input type="checkbox" class="lyrico-select-lyric-line" value="Theeyavar kulaigal nadunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa nerikka vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa nerikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Murikka vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Murikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiya kuruthi therikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Kodiya kuruthi therikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Achcham thuranthidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Achcham thuranthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuchcham arinthidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thuchcham arinthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchcham kilanthidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Uchcham kilanthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Michcham endru ethuvum indri
<input type="checkbox" class="lyrico-select-lyric-line" value="Michcham endru ethuvum indri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Undhan aayudham
<input type="checkbox" class="lyrico-select-lyric-line" value="Undhan aayudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna venbathai
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna venbathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan kaigalil
<input type="checkbox" class="lyrico-select-lyric-line" value="Unthan kaigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendhi nirkkiraai
<input type="checkbox" class="lyrico-select-lyric-line" value="Yendhi nirkkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Andha aayudham
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha aayudham"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna seidhidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna seidhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Achcha koochal adangum munnae
<input type="checkbox" class="lyrico-select-lyric-line" value="Achcha koochal adangum munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththam killi veesidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Moththam killi veesidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa ethiril vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa ethiril vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirpadum nodiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethirpadum nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaigal sidhara
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaigal sidhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa viratti vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa viratti vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virattidum virattil
<input type="checkbox" class="lyrico-select-lyric-line" value="Virattidum virattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagaigal kadhara
<input type="checkbox" class="lyrico-select-lyric-line" value="Pagaigal kadhara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa azhikka vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa azhikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Olikka vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Olikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyavar kulaigal nadunga
<input type="checkbox" class="lyrico-select-lyric-line" value="Theeyavar kulaigal nadunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa nerikka vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa nerikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Murikka vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Murikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiya kuruthi therikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Kodiya kuruthi therikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yelam pottu pirikkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yelam pottu pirikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooram katti adaikkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooram katti adaikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Baetha paarvai vervai pola
<input type="checkbox" class="lyrico-select-lyric-line" value="Baetha paarvai vervai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Oori pona ulagidhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Oori pona ulagidhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Irugi pona manamidhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Irugi pona manamidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilagi poga marukkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ilagi poga marukkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagi pazhagi kedukka ninaikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pazhagi pazhagi kedukka ninaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaigal kalaiyum varam idhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalaigal kalaiyum varam idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oolam paravidum
<input type="checkbox" class="lyrico-select-lyric-line" value="Oolam paravidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha raagam kodiyadhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha raagam kodiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam kaalamdai ingu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaalam kaalamdai ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunba megam pozhiyudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thunba megam pozhiyudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yerka maruthidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yerka maruthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhroga theeyai arinthidhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhroga theeyai arinthidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkum anaithaiyum veezhtha
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarkum anaithaiyum veezhtha"/>
</div>
<div class="lyrico-lyrics-wrapper">Meedhi vaalai veesidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Meedhi vaalai veesidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa ethiril vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa ethiril vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirpadum nodiyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethirpadum nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaigal sidhara
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalaigal sidhara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa viratti vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa viratti vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virattidum virattil
<input type="checkbox" class="lyrico-select-lyric-line" value="Virattidum virattil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagaigal kadhara
<input type="checkbox" class="lyrico-select-lyric-line" value="Pagaigal kadhara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa azhikka vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa azhikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Olikka vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Olikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asura vaettai thuvanga
<input type="checkbox" class="lyrico-select-lyric-line" value="Asura vaettai thuvanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa nerikka vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa nerikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Murikka vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Murikka vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiya kuruthi therikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Kodiya kuruthi therikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kuruthi thaagam adikkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuruthi thaagam adikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paliyai theerkka thudikkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Paliyai theerkka thudikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Palaya pagaiyin mazhaiyil nanaiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Palaya pagaiyin mazhaiyil nanaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Asura vettai nadakkudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Asura vettai nadakkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Edhukku mannil porakkirom
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhukku mannil porakkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppa ootta thavikkirom
<input type="checkbox" class="lyrico-select-lyric-line" value="Neruppa ootta thavikkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusa payalin arakka manasil
<input type="checkbox" class="lyrico-select-lyric-line" value="Manusa payalin arakka manasil"/>
</div>
<div class="lyrico-lyrics-wrapper">Urakkam kettu kodhikkirom
<input type="checkbox" class="lyrico-select-lyric-line" value="Urakkam kettu kodhikkirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Needhi yethuvena
<input type="checkbox" class="lyrico-select-lyric-line" value="Needhi yethuvena"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru neeyum kooridu
<input type="checkbox" class="lyrico-select-lyric-line" value="Indru neeyum kooridu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhi yethuvena
<input type="checkbox" class="lyrico-select-lyric-line" value="Aadhi yethuvena"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam neeyum thaedi odidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam neeyum thaedi odidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vidhi veliyilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidhi veliyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan kobam kaattidu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unthan kobam kaattidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meedhi kuruthiyai
<input type="checkbox" class="lyrico-select-lyric-line" value="Meedhi kuruthiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli theerthamaaga maatridu
<input type="checkbox" class="lyrico-select-lyric-line" value="Alli theerthamaaga maatridu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa asura vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa asura vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asura vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Asura vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asura thalaigal sidhara
<input type="checkbox" class="lyrico-select-lyric-line" value="Asura thalaigal sidhara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa asura vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa asura vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asura vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Asura vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asura pagaigal kadhara
<input type="checkbox" class="lyrico-select-lyric-line" value="Asura pagaigal kadhara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa asura vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa asura vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asura vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Asura vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asura baligal kodukka
<input type="checkbox" class="lyrico-select-lyric-line" value="Asura baligal kodukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa asura vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa asura vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asura vaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Asura vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Asura raththam therikka
<input type="checkbox" class="lyrico-select-lyric-line" value="Asura raththam therikka"/>
</div>
</pre>