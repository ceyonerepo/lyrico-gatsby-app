---
title: "neeya oli song lyrics"
album: "Sarpatta Parambarai"
artist: "Santhosh Narayanan"
lyricist: "Arivu - Shan Vincent de Paul"
director: "Pa. Ranjith"
path: "/albums/sarpatta-parambarai-lyrics"
song: "Neeya Oli"
image: ../../images/albumart/sarpatta-parambarai.jpg
date: 2021-07-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IPCDlJsXiE4"
type: "mass"
singers:
  - Santhosh Narayanan
  - Navz-47
  - Shan Vincent de Paul
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neeye oli, nee thaan vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye oli, nee thaan vali"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyathini odambe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyathini odambe"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye thadai neeye vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye thadai neeye vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaak kiddu narambe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodaak kiddu narambe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sottum sottum veyrva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sottum sottum veyrva"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinam sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinam sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkoru theerva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkoru theerva"/>
</div>
<div class="lyrico-lyrics-wrapper">Acham acham porva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Acham acham porva"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaka nee serva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaka nee serva"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnadi vai kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnadi vai kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Parampara nimirum unnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parampara nimirum unnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Muneri vai kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muneri vai kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalamura eluthum unpera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalamura eluthum unpera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadala ponguthu rosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadala ponguthu rosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadu unokkosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadu unokkosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai sollaa thaippasum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai sollaa thaippasum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee jaikkanum athukkosum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee jaikkanum athukkosum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadala ponguthu rosum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadala ponguthu rosum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadu, unokkosum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadu, unokkosum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai sollaa thaippasum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai sollaa thaippasum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee jaikkanum athukkosum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee jaikkanum athukkosum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeye oli, nee thaan vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye oli, nee thaan vali"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyathini, odambe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyathini, odambe"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye thadai, neeye vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye thadai, neeye vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaakkiddu, narambe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodaakkiddu, narambe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeru yeru yeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeru yeru yeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeru yeru yeru nee mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeru yeru yeru nee mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga yaaru yaaru yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga yaaru yaaru yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarumilla un pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumilla un pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urukkulaintha udamba usupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urukkulaintha udamba usupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulladaintha ellumba ellupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irulladaintha ellumba ellupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inikuninja idathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inikuninja idathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimirnthu sanatha thirattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimirnthu sanatha thirattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onankku neethaan ella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onankku neethaan ella"/>
</div>
<div class="lyrico-lyrics-wrapper">Thonaikku yaarum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thonaikku yaarum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adida adima unara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adida adima unara"/>
</div>
<div class="lyrico-lyrics-wrapper">Idida imayum thinara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idida imayum thinara"/>
</div>
<div class="lyrico-lyrics-wrapper">Adida adima unara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adida adima unara"/>
</div>
<div class="lyrico-lyrics-wrapper">Idida imayum thinara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idida imayum thinara"/>
</div>
<div class="lyrico-lyrics-wrapper">Adida adima unara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adida adima unara"/>
</div>
<div class="lyrico-lyrics-wrapper">Idida imayum thinara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idida imayum thinara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeye oli, nee thaan vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye oli, nee thaan vali"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyathini, odambe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyathini, odambe"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeye thadai, neeye vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeye thadai, neeye vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaakkiddu, narambe mamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodaakkiddu, narambe mamae"/>
</div>
</pre>
