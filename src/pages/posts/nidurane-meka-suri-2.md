---
title: "nidurane song lyrics"
album: "Meka Suri 2"
artist: "Prajwal Krish"
lyricist: "Gaddam Veeraih"
director: "Trinadh Velisala"
path: "/albums/meka-suri-2-lyrics"
song: "Nidurane"
image: ../../images/albumart/meka-suri-2.jpg
date: 2020-11-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/XS8sShtwYI4"
type: "mass"
singers:
  - Sai Charan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nidhuranu vadhalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nidhuranu vadhalani"/>
</div>
<div class="lyrico-lyrics-wrapper">kalalai kasurutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalalai kasurutha"/>
</div>
<div class="lyrico-lyrics-wrapper">nirathamu needala ventaadutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirathamu needala ventaadutha"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalani reyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalani reyani"/>
</div>
<div class="lyrico-lyrics-wrapper">nimishamu viduvaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimishamu viduvaka"/>
</div>
<div class="lyrico-lyrics-wrapper">gundelo dhada dhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundelo dhada dhada"/>
</div>
<div class="lyrico-lyrics-wrapper">maarmrogutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarmrogutha"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalilo dhaaginavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalilo dhaaginavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vala pedatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vala pedatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kalugulo mooginavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalugulo mooginavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poga pedatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poga pedatha"/>
</div>
<div class="lyrico-lyrics-wrapper">guripedatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guripedatha"/>
</div>
<div class="lyrico-lyrics-wrapper">bhayapedutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhayapedutha"/>
</div>
<div class="lyrico-lyrics-wrapper">tharumutha doosukochhe gaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharumutha doosukochhe gaalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanipedatha panipadatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanipedatha panipadatha"/>
</div>
<div class="lyrico-lyrics-wrapper">yegabadi jaali leni kaalila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yegabadi jaali leni kaalila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nidhuranu vadhalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nidhuranu vadhalani"/>
</div>
<div class="lyrico-lyrics-wrapper">kalalai kasurutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalalai kasurutha"/>
</div>
<div class="lyrico-lyrics-wrapper">nirathamu needala ventaadutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirathamu needala ventaadutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neramunna chota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neramunna chota"/>
</div>
<div class="lyrico-lyrics-wrapper">nenu unta beta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenu unta beta"/>
</div>
<div class="lyrico-lyrics-wrapper">chaalu ayyindhanta naa veta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chaalu ayyindhanta naa veta"/>
</div>
<div class="lyrico-lyrics-wrapper">yerka leni sota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yerka leni sota"/>
</div>
<div class="lyrico-lyrics-wrapper">dhorkabadtha baata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhorkabadtha baata"/>
</div>
<div class="lyrico-lyrics-wrapper">urkakinka saagadhu nee aata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urkakinka saagadhu nee aata"/>
</div>
<div class="lyrico-lyrics-wrapper">musugulu thodigina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="musugulu thodigina"/>
</div>
<div class="lyrico-lyrics-wrapper">mrugamula mukhamunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mrugamula mukhamunu"/>
</div>
<div class="lyrico-lyrics-wrapper">bayataku laage anweshana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayataku laage anweshana"/>
</div>
<div class="lyrico-lyrics-wrapper">nigurula venakana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nigurula venakana"/>
</div>
<div class="lyrico-lyrics-wrapper">bhaga bhaga nippula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhaga bhaga nippula"/>
</div>
<div class="lyrico-lyrics-wrapper">bayataku eedche udhghaatana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayataku eedche udhghaatana"/>
</div>
<div class="lyrico-lyrics-wrapper">choosthunna anni minnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choosthunna anni minnai"/>
</div>
<div class="lyrico-lyrics-wrapper">dega kannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dega kannai"/>
</div>
<div class="lyrico-lyrics-wrapper">vegu gannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegu gannai"/>
</div>
<div class="lyrico-lyrics-wrapper">vasthuna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasthuna "/>
</div>
<div class="lyrico-lyrics-wrapper">nidhuranu vadhalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nidhuranu vadhalani"/>
</div>
<div class="lyrico-lyrics-wrapper">kalalai kasurutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalalai kasurutha"/>
</div>
<div class="lyrico-lyrics-wrapper">nirathamu needala ventaadutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirathamu needala ventaadutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pagalani reyani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalani reyani"/>
</div>
<div class="lyrico-lyrics-wrapper">nimishamu viduvaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimishamu viduvaka"/>
</div>
<div class="lyrico-lyrics-wrapper">gundelo dhada dhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundelo dhada dhada"/>
</div>
<div class="lyrico-lyrics-wrapper">maarmrogutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarmrogutha"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalilo dhaaginavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalilo dhaaginavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vala pedatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vala pedatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kalugulo mooginavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalugulo mooginavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poga pedatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poga pedatha"/>
</div>
<div class="lyrico-lyrics-wrapper">guripedatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="guripedatha"/>
</div>
<div class="lyrico-lyrics-wrapper">bhayapedutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhayapedutha"/>
</div>
<div class="lyrico-lyrics-wrapper">tharumutha doosukochhe gaalilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharumutha doosukochhe gaalilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanipedatha panipadatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanipedatha panipadatha"/>
</div>
<div class="lyrico-lyrics-wrapper">yegabadi jaali leni kaalila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yegabadi jaali leni kaalila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nidhuranu vadhalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nidhuranu vadhalani"/>
</div>
<div class="lyrico-lyrics-wrapper">kalalai kasurutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalalai kasurutha"/>
</div>
<div class="lyrico-lyrics-wrapper">nirathamu needala ventaadutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirathamu needala ventaadutha"/>
</div>
</pre>
