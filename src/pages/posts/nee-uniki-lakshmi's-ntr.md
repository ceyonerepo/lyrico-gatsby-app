---
title: "nee uniki song lyrics"
album: "lakshmis ntr"
artist: "Kalyani Malik"
lyricist: "Sira Sri"
director: "Ram Gopal Varma - Agasthya Manju"
path: "/albums/lakshmi's-ntr-lyrics"
song: "Nee Uniki"
image: ../../images/albumart/lakshmis-ntr.jpg
date: 2019-03-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/mdFHHAidSrY"
type: "melody"
singers:
  - S.P. Balasubrahmanyam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Uniki Naa Jeevithaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Uniki Naa Jeevithaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ardham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ardham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Rakaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Rakaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Naku Swargathulyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naku Swargathulyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kavithale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kavithale"/>
</div>
<div class="lyrico-lyrics-wrapper">Poola Gaalula Gandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poola Gaalula Gandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Matale Naku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Matale Naku"/>
</div>
<div class="lyrico-lyrics-wrapper">Sneha Grandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sneha Grandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Virise Ee Bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virise Ee Bandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurise Makarandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurise Makarandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Velige Deepam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velige Deepam"/>
</div>
<div class="lyrico-lyrics-wrapper">Migile Nestham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migile Nestham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Na Sarvaswam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Na Sarvaswam"/>
</div>
<div class="lyrico-lyrics-wrapper">Virise Ee Bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virise Ee Bandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurise Makarandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurise Makarandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Velige Deepam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velige Deepam"/>
</div>
<div class="lyrico-lyrics-wrapper">Migile Nestham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Migile Nestham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Na Sarvaswam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Na Sarvaswam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiraniniki Udhayam La Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiraniniki Udhayam La Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavananiki Bhavam La Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavananiki Bhavam La Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudhayaniki Chalanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudhayaniki Chalanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naku  Neevele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naku  Neevele"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhananiki Vijayam La Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhananiki Vijayam La Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhananiki  Gamyam La Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhananiki  Gamyam La Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuki Neeve Kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuki Neeve Kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Virise Jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virise Jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugise Sangamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugise Sangamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudhe Leni Bhu Sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudhe Leni Bhu Sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Na Sarvaswam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Na Sarvaswam"/>
</div>
</pre>
