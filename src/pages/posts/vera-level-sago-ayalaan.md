---
title: "vera level sago song lyrics"
album: "Ayalaan"
artist: "AR Rahman"
lyricist: "Vivek"
director: "R.Ravikumar"
path: "/albums/ayalaan-lyrics"
song: "Vera Level Sago"
image: ../../images/albumart/ayalaan.jpg
date: 2021-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pD0BFf7XvTk"
type: "happy"
singers:
  - A R Rahman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee usaram thottaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee usaram thottaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara theriyumae thannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara theriyumae thannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha osaththa ninnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha osaththa ninnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera level sagoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera level sagoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna paathu paathu peraachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paathu paathu peraachum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettri adaiyanum ninachaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettri adaiyanum ninachaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thooki nenjil vechaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thooki nenjil vechaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera level sagoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera level sagoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellarukkum ellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarukkum ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai serndhathu kidayaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai serndhathu kidayaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Illathathu paakama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illathathu paakama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichaa nee vera level
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichaa nee vera level"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo endha singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo endha singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragai kekkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai kekkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo endha paravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo endha paravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neendha thudikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendha thudikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo hoo oo kidacha parisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo hoo oo kidacha parisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasikka pazhagidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikka pazhagidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee usaram thottaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee usaram thottaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara theriyumae thannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara theriyumae thannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha osaththa ninnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha osaththa ninnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera level sagoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera level sagoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna paathu paathu peraachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paathu paathu peraachum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettri adaiyanum ninachaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettri adaiyanum ninachaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thooki nenjil vechaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thooki nenjil vechaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera level sagoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera level sagoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellarukkum ellamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarukkum ellamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai serndhathu kidayaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai serndhathu kidayaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama"/>
</div>
<div class="lyrico-lyrics-wrapper">Illathathu paakama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illathathu paakama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirichaa nee vera level
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichaa nee vera level"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo endha singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo endha singam"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragai kekkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai kekkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo endha paravai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo endha paravai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neendha thudikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neendha thudikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo hoo oo kidacha parisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo hoo oo kidacha parisa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasikka pazhagidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikka pazhagidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera level inga kaathirukku unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera level inga kaathirukku unakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nana nana naana naana naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana nana naana naana naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana nana naana naana naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana nana naana naana naana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saadhi vitta neeyum vera level
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi vitta neeyum vera level"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti ketta neeyum vera level
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti ketta neeyum vera level"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponna padikka vai vera level
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna padikka vai vera level"/>
</div>
<div class="lyrico-lyrics-wrapper">Manna sezhikka vai vera level
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manna sezhikka vai vera level"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu seiya inga vaaipirundhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu seiya inga vaaipirundhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganniyama ninna vera level
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganniyama ninna vera level"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbai parisali vera level
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbai parisali vera level"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo hoo hoo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo hoo hoo oo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee usaram thottaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee usaram thottaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara theriyumae thannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara theriyumae thannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha osaththa ninnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha osaththa ninnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera level sagoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera level sagoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna paathu paathu peraachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paathu paathu peraachum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettri adaiyanum ninachaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettri adaiyanum ninachaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna thooki nenjil vechaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna thooki nenjil vechaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera level sagoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera level sagoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sola vedi kaalai adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sola vedi kaalai adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odamadi kaanamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odamadi kaanamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Motor-u vandi sathatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motor-u vandi sathatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanju parakkum aalankili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanju parakkum aalankili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sola vedi kaalai adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sola vedi kaalai adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Odamadi kaanamadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odamadi kaanamadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Motor-u vandi sathatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motor-u vandi sathatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanju parakkum aalankili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanju parakkum aalankili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee mattum vaazha yen mul veli potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mattum vaazha yen mul veli potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa boologam ellaam un veeda aakikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa boologam ellaam un veeda aakikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey methane- u kotta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey methane- u kotta"/>
</div>
<div class="lyrico-lyrics-wrapper">Un ozone-il ottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un ozone-il ottai"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimelaachum vaanam un oodakippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimelaachum vaanam un oodakippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un thaai manna keeri varum udhirathai kudichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un thaai manna keeri varum udhirathai kudichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee uyir vaazha mudiyaathu vazhi maathikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee uyir vaazha mudiyaathu vazhi maathikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee adiyooda suranda boomi un peril illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee adiyooda suranda boomi un peril illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pillainga thavikkaama nee paathukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pillainga thavikkaama nee paathukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nana nana naana naana naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana nana naana naana naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana nana naana naana naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana nana naana naana naana"/>
</div>
</pre>
