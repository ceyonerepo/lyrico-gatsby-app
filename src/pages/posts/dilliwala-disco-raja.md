---
title: "dilliwala song lyrics"
album: "Disco Raja"
artist: "S. Thaman"
lyricist: "Ramajogayya Sastry"
director: "Vi Anand"
path: "/albums/disco-raja-lyrics"
song: "Dilliwala"
image: ../../images/albumart/disco-raja.jpg
date: 2020-01-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/H4R5KVDQ8T8"
type: "happy"
singers:
  - Aditya Iyengar
  - Geetha Madhuri
  - Rahul Nambiar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye akashvani ka delhi kendar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye akashvani ka delhi kendar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Subha ke 6 baje ab aaja karyakram shuru karenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Subha ke 6 baje ab aaja karyakram shuru karenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharagani gani ithadu sugunamulennitiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharagani gani ithadu sugunamulennitiko"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamathala siri ithadu priyathamulendhariko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamathala siri ithadu priyathamulendhariko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruku gadhilo gaganamithadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruku gadhilo gaganamithadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilanu vadhalani chalanamithadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilanu vadhalani chalanamithadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manaku laga bathukulage sagatu manishithadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manaku laga bathukulage sagatu manishithadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dilli wala dilli wala happy go lucky wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dilli wala dilli wala happy go lucky wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Dilli wala dilli wala happy go lucky wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dilli wala dilli wala happy go lucky wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Wala wala wala dilli wala dilli wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wala wala wala dilli wala dilli wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy go lucky wala dilli wala dilli wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy go lucky wala dilli wala dilli wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy go lucky wala dilli wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy go lucky wala dilli wala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Delhi ki galiyon se nikla apna hero shana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Delhi ki galiyon se nikla apna hero shana"/>
</div>
<div class="lyrico-lyrics-wrapper">Har pal masti mein jhoome ye awara deewana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har pal masti mein jhoome ye awara deewana"/>
</div>
<div class="lyrico-lyrics-wrapper">Zindagilona peddha peddha lekkalleni vadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zindagilona peddha peddha lekkalleni vadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheeku chintha thokka thotakoora lekke cheyanivadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheeku chintha thokka thotakoora lekke cheyanivadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharagani gani ithadu sugunamulennitiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharagani gani ithadu sugunamulennitiko"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamathala siri ithadu priyathamulendhariko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamathala siri ithadu priyathamulendhariko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gadiyaram mullu laga prathi nimisham pani chesthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadiyaram mullu laga prathi nimisham pani chesthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana kalam buddhudila manahshanthini pogesthadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana kalam buddhudila manahshanthini pogesthadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Viluva thelisina samayamithadu baruvunerigina payanamithadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viluva thelisina samayamithadu baruvunerigina payanamithadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupusolupani adugu nilapani kalala parugithadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupusolupani adugu nilapani kalala parugithadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dilli wala dilli wala happy go lucky wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dilli wala dilli wala happy go lucky wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Dilli wala dilli wala happy go lucky wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dilli wala dilli wala happy go lucky wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Wala wala wala dilli wala dilli wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wala wala wala dilli wala dilli wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy go lucky wala dilli wala dilli wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy go lucky wala dilli wala dilli wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy go lucky wala dilli wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy go lucky wala dilli wala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seedha sadha andharlanti simple man ye veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seedha sadha andharlanti simple man ye veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkarleni hangamalaku assalu pone podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkarleni hangamalaku assalu pone podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pocket full of santhoshamtho pandaga chesthuntadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pocket full of santhoshamtho pandaga chesthuntadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rocket speed-ai rock on antoo doosuku pothuntadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rocket speed-ai rock on antoo doosuku pothuntadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhorakuna ye kanthakaina inthati tholivalapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhorakuna ye kanthakaina inthati tholivalapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninuvina ae kanthinaina choodadhu na choopu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninuvina ae kanthinaina choodadhu na choopu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalusukunnanura thelusukunnanura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalusukunnanura thelusukunnanura"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde lolothulo unna nuvvemito
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde lolothulo unna nuvvemito"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelipoyindhira thelisipoyindhira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelipoyindhira thelisipoyindhira"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu na jeevitham ika sagedeto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu na jeevitham ika sagedeto"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madhura vadhana mrudhula vachana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhura vadhana mrudhula vachana"/>
</div>
<div class="lyrico-lyrics-wrapper">Kshanamu ninnika vadhalagalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kshanamu ninnika vadhalagalana"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhaku sarasana vidhula varusana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhaku sarasana vidhula varusana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidani mudi padana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidani mudi padana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dilli wala dilli wala ab ban gaya tera dil wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dilli wala dilli wala ab ban gaya tera dil wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Dilli wala dilli wala ab ban gaya tera dil wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dilli wala dilli wala ab ban gaya tera dil wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Wala wala wala dilli wala dilli wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wala wala wala dilli wala dilli wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab ban gaya tera dil wala dilli wala dilli wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab ban gaya tera dil wala dilli wala dilli wala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab ban gaya tera dil wala delhi wala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab ban gaya tera dil wala delhi wala"/>
</div>
</pre>
