---
title: "oru naal mattum song lyrics"
album: "Seedan"
artist: "Dhina"
lyricist: "Pa. Vijay"
director: "Subramaniam Siva"
path: "/albums/seedan-lyrics"
song: "Oru Naal Mattum"
image: ../../images/albumart/seedan.jpg
date: 2011-02-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/voQyDfBck0U"
type: "sad"
singers:
  - K.S. Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaahaaa aaa aaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaa aaa aaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru naal mattum sirikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal mattum sirikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru naal mattum sirikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal mattum sirikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru naal mattum sirikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal mattum sirikka"/>
</div>
<div class="lyrico-lyrics-wrapper">En padaithaan andha iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En padaithaan andha iraivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru kettadhu pookkalin idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru kettadhu pookkalin idhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marunaal andha chediyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marunaal andha chediyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha malar vaadiyapozhudhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha malar vaadiyapozhudhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattukkidandhadhae iraivanin manamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukkidandhadhae iraivanin manamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru naal mattum sirikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal mattum sirikka"/>
</div>
<div class="lyrico-lyrics-wrapper">En padaithaan andha iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En padaithaan andha iraivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru kettadhu pookkalin idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru kettadhu pookkalin idhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanpaarvai parithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanpaarvai parithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai kaana solgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai kaana solgiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ven neerai ootri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ven neerai ootri"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen pookka solgiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen pookka solgiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oomaiyaai maatriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomaiyaai maatriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadavum ketkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadavum ketkiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sariya illai thavara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sariya illai thavara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kanavu eludhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kanavu eludhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaindhu pona kadhaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaindhu pona kadhaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru naal mattum sirikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal mattum sirikka"/>
</div>
<div class="lyrico-lyrics-wrapper">En padaithaan andha iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En padaithaan andha iraivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru kettadhu pookkalin idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru kettadhu pookkalin idhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hooo un meedhu sindhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo un meedhu sindhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Neer theertham aanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer theertham aanadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai serum saambal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai serum saambal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruneerum aagudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruneerum aagudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urugiyae ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugiyae ketkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduthu en piraviyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduthu en piraviyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam irangi arul vazhangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam irangi arul vazhangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaalil padigalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kaalil padigalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirakkum varam thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirakkum varam thaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murugaa en salanam salanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murugaa en salanam salanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerkka vendum murugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerkka vendum murugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha janajam jananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha janajam jananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum podhum murugaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum podhum murugaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un saranam saranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un saranam saranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saranam saranam saranam saranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saranam saranam saranam saranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Saranam saranam saranam saranam murugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saranam saranam saranam saranam murugaa"/>
</div>
</pre>
