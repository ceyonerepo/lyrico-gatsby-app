---
title: "sira chukkalatho song lyrics"
album: "Natana"
artist: "Prabhu Praveen Lanka"
lyricist: "Bharati Babu Penupatruni"
director: "Bharati Babu Penupatruni"
path: "/albums/natana-lyrics"
song: "Sira Chukkalatho"
image: ../../images/albumart/natana.jpg
date: 2019-01-04
lang: telugu
youtubeLink: "https://www.youtube.com/embed/MQ8psD4My-k"
type: "happy"
singers:
  - Dhanunjay
  - Bhargavi Pillai
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sira Chukkalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sira Chukkalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopu Kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopu Kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tara Taralugaa Dunnukune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tara Taralugaa Dunnukune"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Polaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Polaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sira Chukkalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sira Chukkalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopu Kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopu Kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tara Taralugaa Dunnukune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tara Taralugaa Dunnukune"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Polaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Polaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyyy Kuduke Ladainchu Seenuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyy Kuduke Ladainchu Seenuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunuke Udayinchu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunuke Udayinchu Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ala Adukuni Padukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala Adukuni Padukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazaa Cheseddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazaa Cheseddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ala Bhaggumani Siggu Honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala Bhaggumani Siggu Honey"/>
</div>
<div class="lyrico-lyrics-wrapper">Peggey Lepeddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peggey Lepeddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaley Undhi Ani Andukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaley Undhi Ani Andukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Makaam Yetheddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makaam Yetheddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Galass Attukuni Fattumani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galass Attukuni Fattumani"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Maroti Eddaam..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Maroti Eddaam.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sira Chukkalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sira Chukkalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopu Kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopu Kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tara Taralugaa Dunnukune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tara Taralugaa Dunnukune"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Polaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Polaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gorinka Dorikindika Donka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gorinka Dorikindika Donka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetu Vaipu Okka Chudu Asalu Donka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetu Vaipu Okka Chudu Asalu Donka"/>
</div>
<div class="lyrico-lyrics-wrapper">Katinka Mari Kulasa Kunka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katinka Mari Kulasa Kunka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Kanta Vaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Kanta Vaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maha Muduru Tenka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maha Muduru Tenka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Oo Jinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Oo Jinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Avunu Idi ooRaavana Lanka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avunu Idi ooRaavana Lanka"/>
</div>
<div class="lyrico-lyrics-wrapper">Guri chusi Mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guri chusi Mari"/>
</div>
<div class="lyrico-lyrics-wrapper">kottaku Jai Ganta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottaku Jai Ganta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aree Aapinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aree Aapinka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadikasale Shanka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadikasale Shanka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallinchu Vadi Choopu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallinchu Vadi Choopu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maroo Vanka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maroo Vanka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ehe Agu Ika Aagadika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ehe Agu Ika Aagadika"/>
</div>
<div class="lyrico-lyrics-wrapper">Adu Thadaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adu Thadaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai Thappadika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai Thappadika"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalu Ika Buridi Laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalu Ika Buridi Laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudirenika Adhurunika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudirenika Adhurunika"/>
</div>
<div class="lyrico-lyrics-wrapper">Route Pattaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route Pattaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manatho Adu Ika Aadadika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manatho Adu Ika Aadadika"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidara Cheddaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidara Cheddaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sira Chukkalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sira Chukkalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopu Kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopu Kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tara Taralugaa Dunnukune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tara Taralugaa Dunnukune"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Polaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Polaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are Bagunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are Bagunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nishaa Ekkesinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nishaa Ekkesinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee lanka Ku Sarikadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee lanka Ku Sarikadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ra Kishkinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra Kishkinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Bagundi Anduke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bagundi Anduke"/>
</div>
<div class="lyrico-lyrics-wrapper">Memochindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Memochindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naku Vunnadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naku Vunnadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Doochukellamandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doochukellamandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Telisindi Mande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telisindi Mande"/>
</div>
<div class="lyrico-lyrics-wrapper">Telakekkindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telakekkindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Talakindulaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talakindulaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Vachesindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Vachesindi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkaina Sira Chukkayaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkaina Sira Chukkayaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Adupu Darakunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adupu Darakunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaku Ekkutundi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaku Ekkutundi"/>
</div>
<div class="lyrico-lyrics-wrapper">ARe Endukala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ARe Endukala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Ila Chuttumuttala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Ila Chuttumuttala"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweetkala Madakila Cheemalundala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweetkala Madakila Cheemalundala"/>
</div>
<div class="lyrico-lyrics-wrapper">Are thondarala Andarila Nuvvu Undala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are thondarala Andarila Nuvvu Undala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila Thondaralo Inthalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila Thondaralo Inthalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Yellu Nindala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Yellu Nindala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sira Chukkalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sira Chukkalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopu Kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopu Kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tara Taralugaa Dunnukune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tara Taralugaa Dunnukune"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Polaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Polaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyyy Kuduke Ladainchu seenuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyy Kuduke Ladainchu seenuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunuke Udayinchu Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunuke Udayinchu Podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ala Adukuni Padukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala Adukuni Padukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazaa Cheseddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazaa Cheseddham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ala Bhaddumani Siggu Honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala Bhaddumani Siggu Honey"/>
</div>
<div class="lyrico-lyrics-wrapper">Peggey Lepeddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peggey Lepeddham"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhaley Undi Ani Andukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhaley Undi Ani Andukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallam Yeseddham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallam Yeseddham"/>
</div>
<div class="lyrico-lyrics-wrapper">Galass Attukuni Fatuumani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galass Attukuni Fatuumani"/>
</div>
<div class="lyrico-lyrics-wrapper">Malli Maroti Eddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malli Maroti Eddhaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sira Chukkalatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sira Chukkalatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalane"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopu Kalalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopu Kalalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tara Taralugaa Dunnukune
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tara Taralugaa Dunnukune"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu Polaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu Polaalu"/>
</div>
</pre>
