---
title: "vengamavan song lyrics"
album: "Natpe Thunai"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "D. Parthiban Desingu"
path: "/albums/natpe-thunai-lyrics"
song: "Vengamavan"
image: ../../images/albumart/natpe-thunai.jpg
date: 2019-04-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ECV3Jck-Dt8"
type: "mass"
singers:
  - Chinna Ponnu
  - Hiphop Tamizha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vengamavan Avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vengamavan Avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththaiyala Nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththaiyala Nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththama Varaangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththama Varaangale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nendhu Vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nendhu Vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattu kuttiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattu kuttiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetta Poraangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetta Poraangale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kya Re Enna Settinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya Re Enna Settinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Bayathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Bayathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambellaam Swettinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambellaam Swettinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Aalu Munnaala Nothinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aalu Munnaala Nothinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavapora Macha Saavaporada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavapora Macha Saavaporada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pack Your Bags and Leave Home
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pack Your Bags and Leave Home"/>
</div>
<div class="lyrico-lyrics-wrapper">Man i Got You Dethroned
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man i Got You Dethroned"/>
</div>
<div class="lyrico-lyrics-wrapper">I got Enemies all around Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I got Enemies all around Me"/>
</div>
<div class="lyrico-lyrics-wrapper">But Man I’m in the Safe Zone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Man I’m in the Safe Zone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cos i got Friends Who Hold My Back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cos i got Friends Who Hold My Back"/>
</div>
<div class="lyrico-lyrics-wrapper">No Ain’t Goin Back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Ain’t Goin Back"/>
</div>
<div class="lyrico-lyrics-wrapper">Haters Wanna Slow My Track
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haters Wanna Slow My Track"/>
</div>
<div class="lyrico-lyrics-wrapper">But Man I’m Heading My Pack
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But Man I’m Heading My Pack"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">We A Tribe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We A Tribe"/>
</div>
<div class="lyrico-lyrics-wrapper">We A Tribe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We A Tribe"/>
</div>
<div class="lyrico-lyrics-wrapper">We Gon To Take You By Surprise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Gon To Take You By Surprise"/>
</div>
<div class="lyrico-lyrics-wrapper">We Gon Make You Fall in Line
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Gon Make You Fall in Line"/>
</div>
<div class="lyrico-lyrics-wrapper">Step Aside This My Fight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Step Aside This My Fight"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">These Ain’t Fans Brotha Brotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="These Ain’t Fans Brotha Brotha"/>
</div>
<div class="lyrico-lyrics-wrapper">This My Family For Life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This My Family For Life"/>
</div>
<div class="lyrico-lyrics-wrapper">This Ain’t Gangs Brotha Brotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This Ain’t Gangs Brotha Brotha"/>
</div>
<div class="lyrico-lyrics-wrapper">I Got Enemies By My Side
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Got Enemies By My Side"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpe Enthan Balam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpe Enthan Balam"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpe Enthan Valam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpe Enthan Valam"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpirikkum Varaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpirikkum Varaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan illai Thanimaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan illai Thanimaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So En Pagaivarum Anji Nadungidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So En Pagaivarum Anji Nadungidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal Natpu Sarithiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal Natpu Sarithiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaivarum Samam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaivarum Samam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mathipathu Engal Gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mathipathu Engal Gunam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnaala Poga Nenaikkura Engala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaala Poga Nenaikkura Engala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaal Thalla Nenachaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaal Thalla Nenachaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaala Saathikkanumnu Nenaikkara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaala Saathikkanumnu Nenaikkara"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga Kanavugala Neenga Azhichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Kanavugala Neenga Azhichaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viluga Viluga Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viluga Viluga Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilangu Vilangu Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilangu Vilangu Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumba Thirumba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumba Thirumba"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa Elunthu Elunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Elunthu Elunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Nippom Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Nippom Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodachalu Koduthaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodachalu Koduthaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Mela Rendu Veppom Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Mela Rendu Veppom Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vengaiyamavan  Avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vengaiyamavan  Avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththaiyala Nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththaiyala Nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththama Vanthaalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththama Vanthaalume"/>
</div>
<div class="lyrico-lyrics-wrapper">Nendhu Utta Aattukuttiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nendhu Utta Aattukuttiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetta Ponaalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetta Ponaalume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kya Re Enna Settinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya Re Enna Settinga"/>
</div>
<div class="lyrico-lyrics-wrapper">illa Bayathula Odambellaam Swettinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa Bayathula Odambellaam Swettinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Aalu Munnaala Nothing ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aalu Munnaala Nothing ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aava maatta Summa Scene na Podatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aava maatta Summa Scene na Podatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meendum Orumurai Enga Meesaiya Murukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meendum Orumurai Enga Meesaiya Murukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthurukken Muduncha Thaduththu Niruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthurukken Muduncha Thaduththu Niruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thadukka Nenachavanukku Eduthu Unarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thadukka Nenachavanukku Eduthu Unarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Sample Sambavam Manda Baththiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Sample Sambavam Manda Baththiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Konja Naala Konjam Cool la Irukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Konja Naala Konjam Cool la Irukkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kobaththa Yeththaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobaththa Yeththaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolaaru Kudukkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaaru Kudukkaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Erimalai Kolamba Nee Riceku Ooththaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erimalai Kolamba Nee Riceku Ooththaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poriyala Pola Porinchu Karugiduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poriyala Pola Porinchu Karugiduva"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarinchu Erunchuduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarinchu Erunchuduva"/>
</div>
<div class="lyrico-lyrics-wrapper">Viragu Aduppadiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viragu Aduppadiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Edupudikellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Edupudikellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Vizhum Adigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Vizhum Adigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammunu Irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammunu Irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Seendi Paaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Seendi Paaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Savakuliya Neeye Thondi Paaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Savakuliya Neeye Thondi Paaththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Anubavippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Anubavippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Podiyan Ungala Setharadippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Podiyan Ungala Setharadippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready ya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready ya"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinja Vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinja Vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudida Pudida Pudida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudida Pudida Pudida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">These Ain’t Fans Brotha Brotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="These Ain’t Fans Brotha Brotha"/>
</div>
<div class="lyrico-lyrics-wrapper">This My Family For Life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This My Family For Life"/>
</div>
<div class="lyrico-lyrics-wrapper">This Ain’t Gangs Brotha Brotha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This Ain’t Gangs Brotha Brotha"/>
</div>
<div class="lyrico-lyrics-wrapper">I Got Enemies By My Side
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Got Enemies By My Side"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Got Haters Everywhere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Got Haters Everywhere"/>
</div>
<div class="lyrico-lyrics-wrapper">But I Don’t Care About That
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But I Don’t Care About That"/>
</div>
<div class="lyrico-lyrics-wrapper">I Got Friends Who Hold My Back
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Got Friends Who Hold My Back"/>
</div>
<div class="lyrico-lyrics-wrapper">Man I’m Coming With My Pack
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Man I’m Coming With My Pack"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpe Thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpe Thunai"/>
</div>
</pre>
