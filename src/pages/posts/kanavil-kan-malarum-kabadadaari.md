---
title: "kanavil kan malarum song lyrics"
album: "Kabadadaari"
artist: "Simon K.King"
lyricist: "Ku. Karthick"
director: "Pradeep Krishnamoorthy"
path: "/albums/kabadadaari-song-lyrics"
song: "Kanavil Kan Malarum"
image: ../../images/albumart/kabadadaari.jpg
date: 2021-01-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ETzZJqj15xA"
type: "Melody"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanavil Kan Malarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil Kan Malarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravil Un Madiyil… Athu Thavaghum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravil Un Madiyil… Athu Thavaghum"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthidhaai Oru Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthidhaai Oru Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thodarum… Anuthinamum Uyir Uthirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thodarum… Anuthinamum Uyir Uthirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam… Un Porvayil Naan Saigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam… Un Porvayil Naan Saigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Penne Unaal… Naan Theigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Unaal… Naan Theigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkadi En Vaasalil Un Vaasanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkadi En Vaasalil Un Vaasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaasathil Un Thoranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaasathil Un Thoranai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavil Kan Malarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil Kan Malarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravil Un Madiyil… Athu Thavaghum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravil Un Madiyil… Athu Thavaghum"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthithaai Oru Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthithaai Oru Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thodarum… Anuthinamum Uyir Uthirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thodarum… Anuthinamum Uyir Uthirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Un Porvayil Naan Saigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Un Porvayil Naan Saigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Penne Unaal… Naan Theigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Unaal… Naan Theigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkadi En Vaasalil Un Vaasanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkadi En Vaasalil Un Vaasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaasathil Un Thoranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaasathil Un Thoranai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagal Athu Theeratheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal Athu Theeratheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravadhu Aajham Poga… Ethu En Idamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravadhu Aajham Poga… Ethu En Idamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodigalum Mullaai Maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodigalum Mullaai Maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirvaraii Ulle Paaya… Valithan Sugamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirvaraii Ulle Paaya… Valithan Sugamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Porkkalam Unarnthen… Naan Porida Maranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porkkalam Unarnthen… Naan Porida Maranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalai Nenjil… Selai Povaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalai Nenjil… Selai Povaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Modadhe… Unnaal En
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Modadhe… Unnaal En"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavil Kan Malarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil Kan Malarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravil Un Madiyil… Athu Thavaghum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravil Un Madiyil… Athu Thavaghum"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthithaai Oru Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthithaai Oru Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thodarum… Anuthinamum Uyir Uthirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thodarum… Anuthinamum Uyir Uthirum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siragugal Thedithedi… Azuthathu Kaatrain Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragugal Thedithedi… Azuthathu Kaatrain Kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvoor Mazhaiyo…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvoor Mazhaiyo…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivugal Thedithedi… Uruguthu Endhan Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivugal Thedithedi… Uruguthu Endhan Kangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhu En Pizhayio…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhu En Pizhayio…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thooralin Thuliyil Naan Soodamai Erindhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooralin Thuliyil Naan Soodamai Erindhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanneer Sindhum Vaane En Mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneer Sindhum Vaane En Mel"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer Sinthaadhe Oyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Sinthaadhe Oyaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavil Kan Malarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil Kan Malarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravil Un Madiyil… Athu Thavaghum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravil Un Madiyil… Athu Thavaghum"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthithaai Oru Payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthithaai Oru Payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai Thodarum… Anuthinamum Uyir Uthirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai Thodarum… Anuthinamum Uyir Uthirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam Un Porvayil Naan Saigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam Un Porvayil Naan Saigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Penne Unaal… Naan Theigiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penne Unaal… Naan Theigiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkadi En Vaasalil Un Vaasanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkadi En Vaasalil Un Vaasanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaasathil Un Thoranai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaasathil Un Thoranai"/>
</div>
</pre>
