---
title: "ramanna katha song lyrics"
album: "NTR Mahanayakudu"
artist: "M.M. Keeravani"
lyricist: "M.M. Keeravani"
director: "Krish Jagarlamudi"
path: "/albums/ntr-mahanayakudu-lyrics"
song: "Ramanna Katha"
image: ../../images/albumart/ntr-mahanayakudu.jpg
date: 2019-02-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/n_PbviwiSjE"
type: "devotional"
singers:
  - Chitra
  - Sunitha Upadrashta
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ummadi Logita Sayodyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ummadi Logita Sayodyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimmakuru O Ayodyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimmakuru O Ayodyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ummadi Logita Sayodyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ummadi Logita Sayodyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimmakuru O Ayodyaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimmakuru O Ayodyaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kammani Paala Manasulu Penchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kammani Paala Manasulu Penchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Kommaku Poovula Pandaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kommaku Poovula Pandaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu Jathi Thalache e katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu Jathi Thalache e katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vini Murisiponi Manasuntadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vini Murisiponi Manasuntadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu Jathi Thalache e katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu Jathi Thalache e katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vini Murisiponi Manasuntadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vini Murisiponi Manasuntadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chakradaarigaa Pujalandukonunnadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakradaarigaa Pujalandukonunnadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakram Tippe Nayakudy Ranunnadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakram Tippe Nayakudy Ranunnadu"/>
</div>
<div class="lyrico-lyrics-wrapper">E Dwichakra Vahanamarohinchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Dwichakra Vahanamarohinchi "/>
</div>
<div class="lyrico-lyrics-wrapper">Trivikrama Sodharudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trivikrama Sodharudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Annala Dhammula Ammala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annala Dhammula Ammala "/>
</div>
<div class="lyrico-lyrics-wrapper">Akkala Prema Ku Varasudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkala Prema Ku Varasudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yendari-Kendari Kendari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendari-Kendari Kendari "/>
</div>
<div class="lyrico-lyrics-wrapper">Kendari-kapadbhandavudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kendari-kapadbhandavudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yendari-Kendari Kendari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendari-Kendari Kendari "/>
</div>
<div class="lyrico-lyrics-wrapper">Kendari-Kapadbhandavudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kendari-Kapadbhandavudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu Jathi Thalache e katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu Jathi Thalache e katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vini Murisiponi Manasuntadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vini Murisiponi Manasuntadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu Jathi Thalache e katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu Jathi Thalache e katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vini Murisiponi Manasuntadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vini Murisiponi Manasuntadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ganga Sagara Sangama Goshanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganga Sagara Sangama Goshanu "/>
</div>
<div class="lyrico-lyrics-wrapper">Veegipoyanu Jana Gosha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veegipoyanu Jana Gosha"/>
</div>
<div class="lyrico-lyrics-wrapper">Taraka Ramam Rama Tarakam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taraka Ramam Rama Tarakam "/>
</div>
<div class="lyrico-lyrics-wrapper">Cherika yerigina bhaasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherika yerigina bhaasha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Krishnaveni Paravalla SakshiGa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Krishnaveni Paravalla SakshiGa "/>
</div>
<div class="lyrico-lyrics-wrapper">Putrotsaham Pongindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putrotsaham Pongindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeevitha Nataka Rangamlo Oka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeevitha Nataka Rangamlo Oka "/>
</div>
<div class="lyrico-lyrics-wrapper">Thandri Pathra Vudhayinchindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandri Pathra Vudhayinchindhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu Jathi Thalache e katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu Jathi Thalache e katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vini Murisiponi Manasuntadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vini Murisiponi Manasuntadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu Jathi Thalache e katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu Jathi Thalache e katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vini Murisiponi Manasuntadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vini Murisiponi Manasuntadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asesha Andhrula Hrudayalanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asesha Andhrula Hrudayalanu "/>
</div>
<div class="lyrico-lyrics-wrapper">PariPalenche Oka Yogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="PariPalenche Oka Yogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppativaraku Ye Maha Rajuku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppativaraku Ye Maha Rajuku "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhakkindhanthati Bhogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhakkindhanthati Bhogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamantha Thamuga Varinchi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamantha Thamuga Varinchi "/>
</div>
<div class="lyrico-lyrics-wrapper">Vacchina Avakasalanu Thyagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacchina Avakasalanu Thyagam"/>
</div>
<div class="lyrico-lyrics-wrapper">arameesala Dhora Nagammaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arameesala Dhora Nagammaku "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhorikenu O Udyogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhorikenu O Udyogam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu Jathi Thalache e katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu Jathi Thalache e katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vini Murisiponi Manasuntadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vini Murisiponi Manasuntadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Telugu Jathi Thalache e katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telugu Jathi Thalache e katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama Rama Ramanna Katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama Rama Ramanna Katha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vini Murisiponi Manasuntadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vini Murisiponi Manasuntadha"/>
</div>
</pre>
