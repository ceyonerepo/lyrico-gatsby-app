---
title: "naan enbadhu female song lyrics"
album: "Oru Mugathirai"
artist: "Premkumar Sivaperuman"
lyricist: "unknown"
director: "R Senthil Nadan"
path: "/albums/kattappava-kanom-lyrics"
song: "Naan Enbadhu Female"
image: ../../images/albumart/kattappava-kanom.jpg
date: 2017-03-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OYncNLjeI6I"
type: "mass"
singers:
  -	Kharesma Ravichandran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nan enbathu naana illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan enbathu naana illai"/>
</div>
<div class="lyrico-lyrics-wrapper">athu nee than nee than nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu nee than nee than nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">viralil ulagam ennul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralil ulagam ennul"/>
</div>
<div class="lyrico-lyrics-wrapper">ellaigal illai nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellaigal illai nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">world is a social 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="world is a social "/>
</div>
<div class="lyrico-lyrics-wrapper">network now ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="network now ini"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyal thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyal thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">face book il thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="face book il thane"/>
</div>
<div class="lyrico-lyrics-wrapper">namma life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma life"/>
</div>
<div class="lyrico-lyrics-wrapper">odum nodi yavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odum nodi yavum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaiyil ullathu neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaiyil ullathu neram"/>
</div>
<div class="lyrico-lyrics-wrapper">nada munne nadanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nada munne nadanthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">munne nadanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne nadanthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">munne kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">un pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pinne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee pogira paathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pogira paathai"/>
</div>
<div class="lyrico-lyrics-wrapper">athu than unathu saalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu than unathu saalai"/>
</div>
<div class="lyrico-lyrics-wrapper">unnil vaalvil pagale illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnil vaalvil pagale illai"/>
</div>
<div class="lyrico-lyrics-wrapper">imai kathavugal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imai kathavugal "/>
</div>
<div class="lyrico-lyrics-wrapper">mooduvathu illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mooduvathu illai"/>
</div>
<div class="lyrico-lyrics-wrapper">good night solla matom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="good night solla matom"/>
</div>
<div class="lyrico-lyrics-wrapper">mid night life eh pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mid night life eh pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">nam food pasta pizza
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam food pasta pizza"/>
</div>
<div class="lyrico-lyrics-wrapper">thuli kavalaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuli kavalaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">ethuvum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuvum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">namma youth life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma youth life"/>
</div>
<div class="lyrico-lyrics-wrapper">romba jolly da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba jolly da"/>
</div>
<div class="lyrico-lyrics-wrapper">ada puyalai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada puyalai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">namma ratham oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma ratham oru"/>
</div>
<div class="lyrico-lyrics-wrapper">semma speed da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semma speed da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odum nodi yavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odum nodi yavum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaiyil ullathu neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaiyil ullathu neram"/>
</div>
<div class="lyrico-lyrics-wrapper">nada munne nadanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nada munne nadanthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">munne nadanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne nadanthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">munne kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">un pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pinne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethuvum ulagil namake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethuvum ulagil namake"/>
</div>
<div class="lyrico-lyrics-wrapper">kondatam thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondatam thane"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavo nijamo kavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavo nijamo kavale"/>
</div>
<div class="lyrico-lyrics-wrapper">pirakum nimidam namake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pirakum nimidam namake"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosam thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosam thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">iravum pagalum namathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iravum pagalum namathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ithayathil pookum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayathil pookum"/>
</div>
<div class="lyrico-lyrics-wrapper">pookal veril nee thedathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookal veril nee thedathe"/>
</div>
<div class="lyrico-lyrics-wrapper">alaiyodu suvadum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alaiyodu suvadum "/>
</div>
<div class="lyrico-lyrics-wrapper">pogum thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odum nodi yavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odum nodi yavum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaiyil ullathu neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaiyil ullathu neram"/>
</div>
<div class="lyrico-lyrics-wrapper">nada munne nadanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nada munne nadanthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">munne nadanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne nadanthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">munne kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">un pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pinne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan enbathu naana illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan enbathu naana illai"/>
</div>
<div class="lyrico-lyrics-wrapper">athu nee than nee than nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu nee than nee than nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">viralil ulagam ennul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralil ulagam ennul"/>
</div>
<div class="lyrico-lyrics-wrapper">ellaigal illai nanba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellaigal illai nanba"/>
</div>
<div class="lyrico-lyrics-wrapper">world is a social 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="world is a social "/>
</div>
<div class="lyrico-lyrics-wrapper">network now ini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="network now ini"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyal thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyal thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">face book il thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="face book il thane"/>
</div>
<div class="lyrico-lyrics-wrapper">namma life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma life"/>
</div>
<div class="lyrico-lyrics-wrapper">odum nodi yavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odum nodi yavum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kaiyil ullathu neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kaiyil ullathu neram"/>
</div>
<div class="lyrico-lyrics-wrapper">nada munne nadanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nada munne nadanthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">munne nadanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne nadanthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">munne kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munne kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">un pinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pinne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ilaingargal vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilaingargal vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">idam face book than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idam face book than"/>
</div>
<div class="lyrico-lyrics-wrapper">antha vaalkai thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha vaalkai thane"/>
</div>
<div class="lyrico-lyrics-wrapper">nam aatharam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam aatharam "/>
</div>
<div class="lyrico-lyrics-wrapper">ulagathai kaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagathai kaana "/>
</div>
<div class="lyrico-lyrics-wrapper">vaa nanba antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaa nanba antha"/>
</div>
<div class="lyrico-lyrics-wrapper">vidiyale nam thodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidiyale nam thodum"/>
</div>
<div class="lyrico-lyrics-wrapper">thooram antha kadalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thooram antha kadalum"/>
</div>
<div class="lyrico-lyrics-wrapper">neelam illai antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelam illai antha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam ellai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam ellai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">antha ellaiyai kadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha ellaiyai kadanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pogalam sarithiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogalam sarithiram"/>
</div>
<div class="lyrico-lyrics-wrapper">padaithu nam koodalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padaithu nam koodalam"/>
</div>
</pre>
