---
title: "jaana song lyrics"
album: "Maa Vintha Gaadha Vinuma"
artist: "Joy - Sricharan Pakala - Rohit"
lyricist: "Siddu Jonnalagadda - Aditya Mandala"
director: "Aditya Mandala"
path: "/albums/maa-vintha-gaadha-vinuma-lyrics"
song: "Jaana"
image: ../../images/albumart/maa-vintha-gaadha-vinuma.jpg
date: 2020-11-13
lang: telugu
youtubeLink: "https://www.youtube.com/embed/sAdSt64Np-0"
type: "love"
singers:
  - Ayaan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ohoo ee dhooram Dhegharayyela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo ee dhooram Dhegharayyela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinthala marindhe Na navvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinthala marindhe Na navvala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kontha lo esari Saripettedhe ledhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kontha lo esari Saripettedhe ledhala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha lope poni Kadhe maari nee peru lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha lope poni Kadhe maari nee peru lona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">peree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peree"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalupuko na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalupuko na na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Telusuko na na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telusuko na na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnu choose choope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu choose choope"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanu reppa Vaale lope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanu reppa Vaale lope"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee nimisham Nemadayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nimisham Nemadayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninu naa vayipu Ala nadipincheyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu naa vayipu Ala nadipincheyana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu cherelope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu cherelope"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee navvu maare lope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee navvu maare lope"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa manusulo matapayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa manusulo matapayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa thera theseyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa thera theseyana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jana nijamgana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana nijamgana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neetho shruthini kalipesana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho shruthini kalipesana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaga maari nanundipona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaga maari nanundipona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamgana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamgana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jarige varasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarige varasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namesayna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namesayna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhem ayna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhem ayna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veganni ala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veganni ala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Penchesaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penchesaina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee nimisham Nemmadhayela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nimisham Nemmadhayela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinthala maarindhe na navvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinthala maarindhe na navvala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okasari Piliche pillupe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okasari Piliche pillupe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prathi saari vinipincheelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi saari vinipincheelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala lalala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prathi saari Tadithe thalupee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi saari Tadithe thalupee"/>
</div>
<div class="lyrico-lyrics-wrapper">Lala lalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala lalala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oka sari Tericheseyavaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka sari Tericheseyavaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee dhooram Dhegharayyela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee dhooram Dhegharayyela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vinthala Maarindhe naa navvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinthala Maarindhe naa navvala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kontha lo esari Saripettedhe ledhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kontha lo esari Saripettedhe ledhala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha lope poni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha lope poni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhe maari nee peru lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhe maari nee peru lona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa peru ee Kalupukonana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa peru ee Kalupukonana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee varnam Telusukonana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee varnam Telusukonana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnu choose choope kanu reppa vaale lope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu choose choope kanu reppa vaale lope"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee nimisham nemadayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee nimisham nemadayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninu naa vayipu Ala nadipincheyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu naa vayipu Ala nadipincheyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu chere lope nee navvu mare lope
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu chere lope nee navvu mare lope"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa manusulo Matapayi aa thera theseyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa manusulo Matapayi aa thera theseyanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jana Nijamgana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana Nijamgana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neetho Shruthini Kalipesana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Shruthini Kalipesana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaga maari nenundipona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaga maari nenundipona"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijamgana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamgana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jarige varasa Namesayna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarige varasa Namesayna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Janna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhem ayana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhem ayana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veganni ala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veganni ala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">penchesaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penchesaina"/>
</div>
</pre>
