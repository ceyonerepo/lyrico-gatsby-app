---
title: "bavaani varugira song lyrics"
album: "Ponnar Shankar"
artist: "Ilaiyaraaja"
lyricist: "Vairamuthu"
director: "Thiagarajan"
path: "/albums/ponnar-shankar-lyrics"
song: "Bavaani Varugira"
image: ../../images/albumart/ponnar-shankar.jpg
date: 2011-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4z1xKnaiY4c"
type: "happy"
singers:
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ae bavani varuguraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae bavani varuguraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Periya paandi chellaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periya paandi chellaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathaa enga aathaa hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaa enga aathaa hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karagam eduthu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karagam eduthu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karagam eduthu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karagam eduthu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karunaiyodu kan therandhu paappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karunaiyodu kan therandhu paappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbam theerppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbam theerppaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavil vandha kaatchi thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil vandha kaatchi thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan viyandhu ingu kandaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan viyandhu ingu kandaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naavil vandhu nindru thaan engammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naavil vandhu nindru thaan engammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla saedhi solla kandaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla saedhi solla kandaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonna sol endrendrum valikkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna sol endrendrum valikkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">En naadhamae vaedham pol olikkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En naadhamae vaedham pol olikkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonna sol endrendrum valikkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna sol endrendrum valikkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">En naadhamae vaedham pol olikkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En naadhamae vaedham pol olikkumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae bavani varuguraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae bavani varuguraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Periya paandi chellaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periya paandi chellaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathaa enga aathaa hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaa enga aathaa hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karagam eduthu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karagam eduthu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karagam eduthu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karagam eduthu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Karunaiyodu kan therandhu paappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karunaiyodu kan therandhu paappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunbam theerppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunbam theerppaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa.aaaaa.aaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa.aaaaa.aaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edukka yedukka varum pudhaiyal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukka yedukka varum pudhaiyal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukku irukku thaen paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukku irukku thaen paattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lallaai lallae lallaai lallae laallae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lallaai lallae lallaai lallae laallae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eduthu eduthu padi ila maan kuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu eduthu padi ila maan kuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhikkum kudhikkum oor kaettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhikkum kudhikkum oor kaettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lallaai lallae lallaai lallae laallae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lallaai lallae lallaai lallae laallae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marudha mala malaiya vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudha mala malaiya vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Erangudhu neer maegamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangudhu neer maegamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasai ellaam thazhuvi vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasai ellaam thazhuvi vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkudhu nam raagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkudhu nam raagamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho thaer yaeri aathaa varuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho thaer yaeri aathaa varuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava poo maari ponnaa pozhivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava poo maari ponnaa pozhivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edukka edukka varum pudhaiyal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukka edukka varum pudhaiyal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukku irukku thaen paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukku irukku thaen paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduthu eduthu padi ila maan kuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu eduthu padi ila maan kuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhikkum kudhikkum oor kaettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhikkum kudhikkum oor kaettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kashta pattu kashta pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kashta pattu kashta pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattu maettil ozhachom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattu maettil ozhachom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishta pattu kanna kaatta koodaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishta pattu kanna kaatta koodaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishta pattu kanna kaatta koodaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishta pattu kanna kaatta koodaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katti vecha panjeduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti vecha panjeduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Patteduthu padaichom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patteduthu padaichom"/>
</div>
<div class="lyrico-lyrics-wrapper">Patta paadu panju pola pogaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta paadu panju pola pogaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patta paadu panju pola pogaadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta paadu panju pola pogaadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pacha mala paari vechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha mala paari vechu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattini nonbirundhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattini nonbirundhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponga paana pongi vara paaru aathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponga paana pongi vara paaru aathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponga paana pongi vara paaru aathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponga paana pongi vara paaru aathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurinji koothaadum enga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurinji koothaadum enga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga mannula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga mannula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulava kaetkindra thirunaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulava kaetkindra thirunaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae kumari ponnunga kulichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae kumari ponnunga kulichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koondhal katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondhal katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Karagam yedutthu varum perunaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karagam yedutthu varum perunaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaappu katti kaiyila kanganam katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaappu katti kaiyila kanganam katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootthu paattum thodangumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootthu paattum thodangumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maelam kotti mangala mathalam thatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maelam kotti mangala mathalam thatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Maada veedhi magizhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maada veedhi magizhumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raavellaam poo vaanam pattaasu palagaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavellaam poo vaanam pattaasu palagaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellaandi ammaa aasi sollaendi poovaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellaandi ammaa aasi sollaendi poovaayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edukka edukka varum pudhaiyal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukka edukka varum pudhaiyal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukku irukku thaen paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukku irukku thaen paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduthu eduthu padi ila maan kuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu eduthu padi ila maan kuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhikkum kudhikkum oor kaettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhikkum kudhikkum oor kaettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malliga mottaagi malarndhu kulunginaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malliga mottaagi malarndhu kulunginaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakkum poo maala aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakkum poo maala aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manakkum poo maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakkum poo maala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannavan chinnavan yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannavan chinnavan yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathaa thol meedhum aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaa thol meedhum aadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovu pola ponnunga porappu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovu pola ponnunga porappu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooppadainja pozhudhilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooppadainja pozhudhilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maala pola idangal maaralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maala pola idangal maaralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Amman kaattum vazhiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amman kaattum vazhiyilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarukku un maala yaarukku en maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukku un maala yaarukku en maala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellaandi ammaa thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellaandi ammaa thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nal vaazhvu tharuvaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nal vaazhvu tharuvaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edukka edukka varum pudhaiyal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukka edukka varum pudhaiyal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukku irukku thaen paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukku irukku thaen paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduthu eduthu padi ila maan kuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu eduthu padi ila maan kuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhikkum kudhikkum oor kaettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhikkum kudhikkum oor kaettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marudha mala malaiya vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudha mala malaiya vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Erangudhu neer maegamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erangudhu neer maegamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasai ellaam thazhuvi vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasai ellaam thazhuvi vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkudhu nam raagamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkudhu nam raagamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho thaer yaeri aathaa varuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho thaer yaeri aathaa varuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava poo maari ponnaa pozhivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava poo maari ponnaa pozhivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edukka edukka varum pudhaiyal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukka edukka varum pudhaiyal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukku irukku thaen paattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukku irukku thaen paattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduthu eduthu padi ila maan kuyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu eduthu padi ila maan kuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhikkum kudhikkum oor kaettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhikkum kudhikkum oor kaettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haei..haeihaei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haei..haeihaei"/>
</div>
<div class="lyrico-lyrics-wrapper">Haei haei haei haei haeiiiiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haei haei haei haei haeiiiiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi hoi hi hoi hoi hoi hoi hoi ho hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hi hoi hoi hoi hoi hoi ho hoi"/>
</div>
</pre>
