---
title: "vilagaadhe enadhuyirae song lyrics"
album: "Oru Kuppai Kathai"
artist: "Joshua Sridhar"
lyricist: "Na. Muthukumar"
director: "Kaali Rangasamy"
path: "/albums/oru-kuppai-kathai-lyrics"
song: "Vilagaadhe Enadhuyirae"
image: ../../images/albumart/oru-kuppai-kathai.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pFzd9hlk-_o"
type: "melody"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ooooooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooooooooooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oooooooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oooooooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilagaadhae enadhuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaadhae enadhuyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagaadhae enadhuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaadhae enadhuyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ponaal adi piriyaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ponaal adi piriyaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhagaamal en kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagaamal en kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkaadhae nee veliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkaadhae nee veliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi nee ponaal udan piriyaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi nee ponaal udan piriyaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal poiyillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal poiyillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai solla vazhiyillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai solla vazhiyillaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee indri naanillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee indri naanillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kadavul thantha varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kadavul thantha varam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil vandhavudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil vandhavudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirindhu pogindradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirindhu pogindradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilagaadhae enadhuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaadhae enadhuyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagaadhae enadhuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaadhae enadhuyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ponaal adi piriyaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ponaal adi piriyaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhagaamal en kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagaamal en kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkaadhae nee veliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkaadhae nee veliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi nee ponaal udan piriyaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi nee ponaal udan piriyaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhuyiraeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyiraeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa haaaahaa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa haaaahaa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa haa aaahaa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa haa aaahaa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaa aahaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaa aahaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaa aahaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaa aahaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maanae yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanae yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan mounam puriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan mounam puriyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thetra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thetra"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mozhiyum kaiyilillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mozhiyum kaiyilillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanal un mel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanal un mel"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla kaadhalum kurayavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla kaadhalum kurayavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi theriyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi theriyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ozhi theriyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ozhi theriyaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oruvidha kaatil maatikonden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oruvidha kaatil maatikonden"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai ariyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai ariyaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sellum podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sellum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam kondenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam kondenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam en kaadhal undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam en kaadhal undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhil sollidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil sollidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal poiyillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal poiyillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai solla vazhiyillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai solla vazhiyillaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbae anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee indri naanillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee indri naanillaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada kadavul thantha varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada kadavul thantha varam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil vandhavudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil vandhavudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirindhu pogindradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirindhu pogindradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haaaaahaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaahaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaaahaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaaahaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanimai theevil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai theevil"/>
</div>
<div class="lyrico-lyrics-wrapper">En natkal oduthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En natkal oduthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae vizhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugamae theriyudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugamae theriyudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaoru naal varuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaoru naal varuvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru ellam engudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru ellam engudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru pillai thavarai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru pillai thavarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaayarivazhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaayarivazhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu pol ennai mannippaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu pol ennai mannippaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaiveli innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiveli innum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodida neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodida neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandippaayo sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandippaayo sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam en kaadhal undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam en kaadhal undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhil sollidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil sollidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vilagaadhae enadhuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaadhae enadhuyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilagaadhae enadhuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilagaadhae enadhuyirae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ponaal adi piriyaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ponaal adi piriyaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoo o
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo o"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagaamal en kiliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagaamal en kiliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkaadhae nee veliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkaadhae nee veliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi nee ponaal udan piriyaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi nee ponaal udan piriyaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Enadhuyiraeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyiraeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmmmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmmmmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmm mm mmm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm mm mmm mm mm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhuyirae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhuyirae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmmmmmmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmmmmmmmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmm mm mmm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm mm mmm mm mm"/>
</div>
</pre>
