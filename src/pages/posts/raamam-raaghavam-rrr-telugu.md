---
title: "raamam raaghavam song lyrics"
album: "RRR Telugu"
artist: "Maragathamani"
lyricist: "K Shiva Dutta"
director: "S.S. Rajamouli "
path: "/albums/rrr-telugu-lyrics"
song: "Raamam Raaghavam"
image: ../../images/albumart/rrr-telugu.jpg
date: 2022-03-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Cn-o7RzUPpU"
type: "mass"
singers:
  - Vijay Prakash
  - Chandana Bala Kalyan
  - Charu Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamam Raaghavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamam Raaghavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranadheeram Raajasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranadheeram Raajasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rudhra Dhanussama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rudhra Dhanussama"/>
</div>
<div class="lyrico-lyrics-wrapper">Samana Swadhanushtankhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samana Swadhanushtankhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhayam Bhrantha Saathravam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayam Bhrantha Saathravam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramam Raghavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramam Raghavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranadheeram Raajasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranadheeram Raajasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raamam Raaghavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamam Raaghavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranadheeram Raajasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranadheeram Raajasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ghandeeva Mukhta Punkhanupunka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghandeeva Mukhta Punkhanupunka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sharaparamparaha Davaleesatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sharaparamparaha Davaleesatham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raamam Raaghavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raamam Raaghavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranadheeram Raajasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranadheeram Raajasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hasthinapuram Samasthatithasti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasthinapuram Samasthatithasti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbhastaladi Charan Natarajam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbhastaladi Charan Natarajam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nataraajam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nataraajam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hasthinapuram Samasthatithasti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hasthinapuram Samasthatithasti"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbhastaladi Charan Natarajam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbhastaladi Charan Natarajam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramam Raghavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramam Raghavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranadheeram Raajasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranadheeram Raajasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ramam Raghavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ramam Raghavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranadheeram Raajasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranadheeram Raajasam"/>
</div>
</pre>
