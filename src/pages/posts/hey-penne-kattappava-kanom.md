---
title: "hey penne song lyrics"
album: "Kattappava Kanom"
artist: "Santhosh Dhayanidhi"
lyricist: "Uma Devi"
director: "Mani Seiyon"
path: "/albums/kattappava-kanom-lyrics"
song: "Hey Penne"
image: ../../images/albumart/kattappava-kanom.jpg
date: 2017-03-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/iobiaJVp2Lg"
type: "love"
singers:
  -	Sid Sriram
  - Alisha Thomas
  - Aishwarya Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Then Ootrudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Then Ootrudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella Yaazh Meetudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella Yaazh Meetudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannala Nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala Nee "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Pachakuththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Pachakuththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjellam Panjaagi Ponadhenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjellam Panjaagi Ponadhenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrodu Kaatraagum Kaatraadipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrodu Kaatraagum Kaatraadipol"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Unnodu Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Unnodu Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Penne Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Penne Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kanda Pinney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kanda Pinney"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Nalla Neram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Nalla Neram "/>
</div>
<div class="lyrico-lyrics-wrapper">Yendru Thondrudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendru Thondrudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Minnum Ponney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Minnum Ponney"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Kulle Undhan Bimbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Kulle Undhan Bimbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengo Yennai Kondu Pogudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengo Yennai Kondu Pogudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikkara Veyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkara Veyila "/>
</div>
<div class="lyrico-lyrics-wrapper">Pol Uththu Paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pol Uththu Paakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkadi Kulira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkadi Kulira "/>
</div>
<div class="lyrico-lyrics-wrapper">Pol Vandhu Eerkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pol Vandhu Eerkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Verellam Pookiradhey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verellam Pookiradhey "/>
</div>
<div class="lyrico-lyrics-wrapper">Poovellam Verkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovellam Verkiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolaaru Idhayathile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaaru Idhayathile "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Thithikudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thithikudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhil Mella Kadhal Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil Mella Kadhal Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatril Yettri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Yettri "/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna Kooti Pogura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna Kooti Pogura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nooru Kaalal Nanjam Oda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Kaalal Nanjam Oda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatu Theeyai Yennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu Theeyai Yennai "/>
</div>
<div class="lyrico-lyrics-wrapper">Paththa Veikura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paththa Veikura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan Mazhaiyai Pol 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Mazhaiyai Pol "/>
</div>
<div class="lyrico-lyrics-wrapper">Then Pozhindhaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Then Pozhindhaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Meen Kannala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meen Kannala "/>
</div>
<div class="lyrico-lyrics-wrapper">Oon Kalandhaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oon Kalandhaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Penne Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Penne Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kanda Pinney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kanda Pinney"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Nalla Neram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Nalla Neram "/>
</div>
<div class="lyrico-lyrics-wrapper">Yendru Thondrudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendru Thondrudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatriniley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatriniley"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Geetham Kaatriniley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Geetham Kaatriniley"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatriniley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatriniley"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Geetham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Geetham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey ye Penne Penne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ye Penne Penne "/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kanda Pinney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kanda Pinney"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Nalla Neram 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Nalla Neram "/>
</div>
<div class="lyrico-lyrics-wrapper">Yendru Thondrudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yendru Thondrudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Minnum Ponney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Minnum Ponney"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu Kulle Undhan Bimbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Kulle Undhan Bimbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengo Yennai Kondu Pogudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengo Yennai Kondu Pogudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikkara Veyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkara Veyila "/>
</div>
<div class="lyrico-lyrics-wrapper">Pol Uththu Paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pol Uththu Paakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkadi Kulira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkadi Kulira "/>
</div>
<div class="lyrico-lyrics-wrapper">Pol Vandhu Eerkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pol Vandhu Eerkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Verellam Pookiradhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verellam Pookiradhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Poovellam Verkiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovellam Verkiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolaaru Idhayathile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaaru Idhayathile "/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Thithikudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Thithikudhe"/>
</div>
</pre>
