---
title: "kadugalavu song lyrics"
album: "Kadugu"
artist: "S N Arunagiri"
lyricist: "Madhan Karky"
director: "Vijay Milton"
path: "/albums/kadugu-lyrics"
song: "Kadugalavu"
image: ../../images/albumart/kadugu.jpg
date: 2017-03-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qpH0rz8r3ik"
type: "happy"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Size-il andha vaanam perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Size-il andha vaanam perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna moodikittaa sirusaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna moodikittaa sirusaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Size-il indha vaazhkkai sirusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Size-il indha vaazhkkai sirusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna moodumvara perusaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna moodumvara perusaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaichaa unakku malaidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaichaa unakku malaidhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uli eduthaa adhuvum silaidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uli eduthaa adhuvum silaidhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilai nuniyil paniyum azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilai nuniyil paniyum azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukkulaiyum adangum ulagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukkulaiyum adangum ulagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaiyaiyo bhoomikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyaiyo bhoomikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oru kadugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oru kadugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaiyaiyo saamikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyaiyo saamikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyae kadugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiyae kadugu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Size-il andha vaanam perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Size-il andha vaanam perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna moodikittaa sirusaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna moodikittaa sirusaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Size-il indha vaazhkkai sirusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Size-il indha vaazhkkai sirusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna moodumvara perusaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna moodumvara perusaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanoda thuliyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanoda thuliyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennoda valiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennoda valiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhoorathil irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoorathil irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasathai thurathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasathai thurathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellamae pudhusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamae pudhusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannukku parisaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukku parisaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parisa pirichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parisa pirichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rusichu rasichuTheerthaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rusichu rasichuTheerthaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thudi thudikkira nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudi thudikkira nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kanavu thekki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kanavu thekki"/>
</div>
<div class="lyrico-lyrics-wrapper">Padapadakkira rekkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padapadakkira rekkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha virichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha virichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu bhoomiya nokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu bhoomiya nokki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paraparakkira kaaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paraparakkira kaaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru panjappola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru panjappola"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduaduththadhu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduaduththadhu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee payanam pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee payanam pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogam ellaam perusudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam ellaam perusudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikka therinjaa ellaam kadugalavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikka therinjaa ellaam kadugalavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikka therinjaa ellaam kadugalavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikka therinjaa ellaam kadugalavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Size-il andha vaanam perusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Size-il andha vaanam perusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna moodikittaa sirusaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna moodikittaa sirusaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Size-il indha vaazhkkai sirusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Size-il indha vaazhkkai sirusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna moodumvara perusaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna moodumvara perusaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaichaa unakku malaidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaichaa unakku malaidhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uli eduthaa adhuvum silaidhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uli eduthaa adhuvum silaidhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilai nuniyil paniyum azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilai nuniyil paniyum azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukkulaiyum adangum ulagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukkulaiyum adangum ulagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaiyaiyo bhoomikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyaiyo bhoomikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oru kadugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee oru kadugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaiyaiyo saamikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaiyaiyo saamikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoomiyae kadugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoomiyae kadugu"/>
</div>
</pre>
