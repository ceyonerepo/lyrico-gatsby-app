---
title: "avan paathu sirikala song lyrics"
album: "Kodiyil Oruvan"
artist: "	Nivas K. Prasanna - Harish arjun"
lyricist: "Mohan Rajan"
director: "Ananda Krishnan"
path: "/albums/kodiyil-oruvan-lyrics"
song: "Avan Paathu Sirikala"
image: ../../images/albumart/kodiyil-oruvan.jpg
date: 2021-09-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/yjND4pZQozk"
type: "love"
singers:
  - Malvi Sundaresan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pesum dhooram nindraane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum dhooram nindraane"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa kadhal kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa kadhal kondene"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvaano haaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvaano haaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan paathu sirikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan paathu sirikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan parakka thodangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan parakka thodangitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan pesi sirikalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan pesi sirikalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan uruga thodangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan uruga thodangitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan jaadai kaattala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan jaadai kaattala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sariya thodangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sariya thodangitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kooda nadakkalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kooda nadakkalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan polamba thodangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan polamba thodangitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan thirumbi paakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thirumbi paakala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan virumba thodangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan virumba thodangitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan manasu puriyalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan manasu puriyalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan mayanga thodangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan mayanga thodangitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan kanna kaattala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kanna kaattala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan karaiya thodangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan karaiya thodangitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kaiya pudikka thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kaiya pudikka thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kanavu kandutten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kanavu kandutten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avana naan paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana naan paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kuzhanthaiya kuthippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kuzhanthaiya kuthippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan ennai paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan ennai paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kumariya rasippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kumariya rasippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avana naan paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana naan paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kuzhanthaiya kuthippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kuzhanthaiya kuthippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan ennai paartha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan ennai paartha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kumariya rasippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kumariya rasippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan paathu sirikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan paathu sirikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan parakka thodangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan parakka thodangitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan pesi sirikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan pesi sirikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan uruga thodangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan uruga thodangitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan jaadai kaattala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan jaadai kaattala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan sariya thodangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan sariya thodangitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kooda nadakkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kooda nadakkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan polamba thodangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan polamba thodangitten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukulla pesi sirichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla pesi sirichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaiyellam thedi kuvichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyellam thedi kuvichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandapadi aadi thudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandapadi aadi thudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellam unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam unnale"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukulla katti thudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla katti thudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">En nizhala otti rasichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nizhala otti rasichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennennavo solla nenachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennavo solla nenachen"/>
</div>
<div class="lyrico-lyrics-wrapper">Solli solli ennai tholaichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solli solli ennai tholaichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadigaaram paarkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadigaaram paarkama"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakaaga irukkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakaaga irukkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Gana neram pirinjalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gana neram pirinjalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganamaa naan uruguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganamaa naan uruguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodu vittu koodu paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodu vittu koodu paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal thedhi paakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal thedhi paakkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otti nadakkala katti pudikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otti nadakkala katti pudikkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanalum nenjooram eanga thodangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum nenjooram eanga thodangitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnum puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhedho naanum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhedho naanum thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thodangitten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thodangitten"/>
</div>
</pre>
