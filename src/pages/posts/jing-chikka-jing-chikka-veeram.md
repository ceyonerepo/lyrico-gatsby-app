---
title: "jing chikka jing chikka song lyrics"
album: "Veeram"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Siva"
path: "/albums/veeram-lyrics"
song: "Jing Chikka Jing Chikka"
image: ../../images/albumart/veeram.jpg
date: 2014-01-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DHgRRTyAEpM"
type: "Festival"
singers:
  - Pushpavanam Kuppusamy
  - Magizhini Manimaaran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Akka Maare Anna Maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka Maare Anna Maare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiya Maare Aaththa Maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiya Maare Aaththa Maare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooru Kanam Namma Oore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Kanam Namma Oore"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maama Maare Machaan Maare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama Maare Machaan Maare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machinichi Naathanare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machinichi Naathanare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakanaum Enna Pakathoore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakanaum Enna Pakathoore"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiye Thaaru Maara Thaavura Mosalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye Thaaru Maara Thaavura Mosalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjukulla Ninnukittu Oothura Visulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukulla Ninnukittu Oothura Visulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuminchu Konda Potta Koovura Kuyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuminchu Konda Potta Koovura Kuyilu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Thandavaalam Gandamaaga Otura Raiyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thandavaalam Gandamaaga Otura Raiyilu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kurumbu Paiya Pulla Kurumbu Paiya Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbu Paiya Pulla Kurumbu Paiya Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kennathu Kulla Vanthu Kappal Vidatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kennathu Kulla Vanthu Kappal Vidatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiruttu Paiya Pulla Thiruttu Paiya Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruttu Paiya Pulla Thiruttu Paiya Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thorantha Veetukulla Kannam Podatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorantha Veetukulla Kannam Podatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kaal Patta Mann Alli Paakaittil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaal Patta Mann Alli Paakaittil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vechuruken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechuruken"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandukaatha Kambi Neetatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandukaatha Kambi Neetatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mama Ippadi Konjuthey Yenma Ippadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mama Ippadi Konjuthey Yenma Ippadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Minjura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minjura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyayo Aambala Ice Vandi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyayo Aambala Ice Vandi Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Ice Vechey Ice Vechey Kavuthuduvaan Girlla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Ice Vechey Ice Vechey Kavuthuduvaan Girlla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valathu Kanirukkum Pennoruthi Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valathu Kanirukkum Pennoruthi Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Edathu Kannu Meyumappa Innoruthi Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Edathu Kannu Meyumappa Innoruthi Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Pullaiyar Koyil Kozhukatta Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pullaiyar Koyil Kozhukatta Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kannam Rendu Vechurukka Kazhuthuku Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannam Rendu Vechurukka Kazhuthuku Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Aduppu Mela Vecha Paal Chatti Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Aduppu Mela Vecha Paal Chatti Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Pongurutha Paathu Paathu Poonaiyachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pongurutha Paathu Paathu Poonaiyachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagu Paiya Pulla Azhagu Paiya Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagu Paiya Pulla Azhagu Paiya Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valaiya Viruchutu Kaathirukaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiya Viruchutu Kaathirukaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kaal Vella Kai Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaal Vella Kai Vella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthathum Pechey Varala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthathum Pechey Varala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pappa Peepee Paa Paa Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pappa Peepee Paa Paa Po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyo Pechu Moochu Illama Kedakaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyo Pechu Moochu Illama Kedakaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethanu Parthu Seiyen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanu Parthu Seiyen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaai Illa Poochinu Paavam Paatha Pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai Illa Poochinu Paavam Paatha Pochu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Mutha Kathi Vechu Enna Keechuduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Mutha Kathi Vechu Enna Keechuduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keechu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keechu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ora Kannaala Pakura Saatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ora Kannaala Pakura Saatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu Thappana Paarva En Thavani Saatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Thappana Paarva En Thavani Saatchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Santhanam Saarayam Senthuta Kalava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Santhanam Saarayam Senthuta Kalava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Saayalil Paathendi Yelettu Nelava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Saayalil Paathendi Yelettu Nelava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iduppil Sumakkura Vaanavil Valava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppil Sumakkura Vaanavil Valava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">NaEngeyum Paakkalaye Ippadi Patta Alava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="NaEngeyum Paakkalaye Ippadi Patta Alava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kirukku Paiya Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukku Paiya Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kirukku Paiya Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirukku Paiya Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuruku Vazhiyila Seena Podaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruku Vazhiyila Seena Podaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Kazhuvura Meenula Nazhuvura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Kazhuvura Meenula Nazhuvura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenapola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyavittu Thulli Odaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyavittu Thulli Odaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Kena Siruki Un Manasu Enna Kalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Kena Siruki Un Manasu Enna Kalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Manasula Vechuruken Ambuttu Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Manasula Vechuruken Ambuttu Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Koocham Vanthu Thadukuthu Vaai Vittu Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Koocham Vanthu Thadukuthu Vaai Vittu Pesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiye Usura Pesaiyithu Nee Sirukum Osa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiye Usura Pesaiyithu Nee Sirukum Osa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Unna Vechu Nenjukulla Seiyuren Poosa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Unna Vechu Nenjukulla Seiyuren Poosa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakkan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jing Chikka Jing Chikka Jing Chikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jing Chikka Jing Chikka Jing Chikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jingidi Chakkan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jingidi Chakkan"/>
</div>
</pre>
