---
title: "ayya sami song lyrics"
album: "Keni"
artist: "M. Jayachandran"
lyricist: "B.K. Harinarayanan - Palani Bharathi"
director: "M.A. Nishad"
path: "/albums/keni-song-lyrics"
song: "Ayya Sami"
image: ../../images/albumart/keni.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uSXtbFgR5hQ"
type: "happy"
singers:
  - K.J. Yesudas
  - S.P. Balasubrahmanyam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aiyaa saami nammal onnae saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa saami nammal onnae saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma boomi onnu vaanam onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma boomi onnu vaanam onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammal onnae saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammal onnae saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaa saami idhu namma boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa saami idhu namma boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazha kaathukkingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazha kaathukkingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhi yaedhu baedham yaedhu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi yaedhu baedham yaedhu saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathukkulla vaatha pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathukkulla vaatha pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai nenju neendhudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai nenju neendhudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi saara kaathu nenjukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi saara kaathu nenjukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkaraiya thoovudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkaraiya thoovudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoovudhadi thoovudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoovudhadi thoovudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoovudhadi thoovudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoovudhadi thoovudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukkulla sakkaraiya thoovudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla sakkaraiya thoovudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaa saami nammal onnae saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa saami nammal onnae saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma boomi onnu vaanam onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma boomi onnu vaanam onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammal onnae saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammal onnae saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meena veyil sooriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meena veyil sooriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Laavu thanna chandhiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laavu thanna chandhiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Meena veyil sooriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meena veyil sooriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Laavu thanna chandhiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laavu thanna chandhiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammudaedhaanu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammudaedhaanu saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandal mannin vaasamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandal mannin vaasamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada kandrin naesamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada kandrin naesamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnukkulla onnu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnukkulla onnu saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeyum onnu novum onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyum onnu novum onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannilottu vembi veena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilottu vembi veena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu neerum onnu saami saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu neerum onnu saami saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyaa saami idhu namma boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa saami idhu namma boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu aadu maadu kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu aadu maadu kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma chellam thaanae saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma chellam thaanae saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadugindra paravaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadugindra paravaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal surakkum karavaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal surakkum karavaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oi paadugindra paravaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oi paadugindra paravaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal surakkum karavaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal surakkum karavaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthi varum sondham saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthi varum sondham saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vella manal paadamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vella manal paadamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellam ulla solaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellam ulla solaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammudaiyaedhaanu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammudaiyaedhaanu saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koondukkullae eeram kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondukkullae eeram kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaergal pola pinni kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaergal pola pinni kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum vaazhkkai sorgam saami saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum vaazhkkai sorgam saami saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaa saami nammal onnae saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa saami nammal onnae saami"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma boomi onnu vaanam onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma boomi onnu vaanam onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammal onnae saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammal onnae saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi hoi hoi hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi hoi hoi hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaa saami idhu namma boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa saami idhu namma boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazha kaathukkingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazha kaathukkingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadhi yaedhu baedham yaedhu saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhi yaedhu baedham yaedhu saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gopurathu moolaiylae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gopurathu moolaiylae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kokkirandu koodudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokkirandu koodudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi saara kaatthu nenjukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi saara kaatthu nenjukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkaraiya thoovudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkaraiya thoovudhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoovudhadi thoovudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoovudhadi thoovudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoovudhadi thoovudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoovudhadi thoovudhadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjukkulla sakkaraiya thoovudhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla sakkaraiya thoovudhadi"/>
</div>
</pre>
