---
title: "chukkala chunni song lyrics"
album: "SR Kalyanamandapam"
artist: "Chaitan Bharadwaj"
lyricist: "Bhaskarabhatla Ravi Kumar"
director: "Sridhar Gade"
path: "/albums/sr-kalyanamandapam-lyrics"
song: "Chukkala Chunni"
image: ../../images/albumart/sr-kalyanamandapam.jpg
date: 2021-08-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/CXgMtDQMwwU"
type: "love"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Tutu rutu tutu-tu tutu rutu tutu-tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tutu rutu tutu-tu tutu rutu tutu-tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tutu rutu tutu-tu tu-tutu-tu tutu-tutu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tutu rutu tutu-tu tu-tutu-tu tutu-tutu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkala chunnike na gundeni kattave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkala chunnike na gundeni kattave"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa nelakasamlo arre gira gira tippesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa nelakasamlo arre gira gira tippesave"/>
</div>
<div class="lyrico-lyrics-wrapper">Muvvala patteeke naa pranam chuttave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muvvala patteeke naa pranam chuttave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvevelle daarantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvevelle daarantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Are ghallu ghallu moginchave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are ghallu ghallu moginchave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vechha vechhani oopirithoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechha vechhani oopirithoti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vukkiri bikkiri chesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vukkiri bikkiri chesave"/>
</div>
<div class="lyrico-lyrics-wrapper">Undipo undipo undipo nathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undipo undipo undipo nathone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoyare hoyare hoya hoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyare hoyare hoya hoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne valle ne valle pichodila thayaraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne valle ne valle pichodila thayaraya"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoyare hoyare hoya hoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyare hoyare hoya hoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne valle ne valle nalo nene ghallanthaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne valle ne valle nalo nene ghallanthaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aye kottha kottha chitralanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye kottha kottha chitralanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippude chustunnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippude chustunnanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Guttuga dhachuko lenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guttuga dhachuko lenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dappe kotti cheppalenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dappe kotti cheppalenu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaleni anandanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaleni anandanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkadine moya lenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkadine moya lenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koddiga sayam vasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koddiga sayam vasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Panchukundam nuvvu nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchukundam nuvvu nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasepu nuvvu kannarpaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasepu nuvvu kannarpaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnilo nannu chustune vunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnilo nannu chustune vunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasepu matadaku kougilla kavyam rasukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasepu matadaku kougilla kavyam rasukunta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh edarila vunde nalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh edarila vunde nalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sindhu nadhai pongave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindhu nadhai pongave"/>
</div>
<div class="lyrico-lyrics-wrapper">Undipo undipo eppudu nathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undipo undipo eppudu nathone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoyare hoyare hoya hoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyare hoyare hoya hoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne valle ne valle pichodila thayyaraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne valle ne valle pichodila thayyaraya"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoyare hoyare hoya hoya hoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyare hoyare hoya hoya hoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne valle ne valle nalo nene gallanthaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne valle ne valle nalo nene gallanthaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhadhane bharinchadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhadhane bharinchadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhulonchi bayataki radam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhulonchi bayataki radam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chala chala kastam ani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chala chala kastam ani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ento antha antuntare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ento antha antuntare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vallaki teluso ledho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallaki teluso ledho"/>
</div>
<div class="lyrico-lyrics-wrapper">Hayini bharinchadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hayini bharinchadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthakanna kastam kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthakanna kastam kada"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhuku nene sakshyam kada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhuku nene sakshyam kada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inthala nenu navvindi ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthala nenu navvindi ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthala nannu paresukoledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthala nannu paresukoledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthala ne junkalaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthala ne junkalaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasenadu oogaledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasenadu oogaledhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae dhayi dhayi antu vunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae dhayi dhayi antu vunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandamamai vachahhave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandamamai vachahhave"/>
</div>
<div class="lyrico-lyrics-wrapper">Undipo undipo thoduga nathone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undipo undipo thoduga nathone"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoyare hoyare hoya hoya hoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoyare hoyare hoya hoya hoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne valle ne valle pichodila thayyaraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne valle ne valle pichodila thayyaraya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne valle ne valle nalo nene gallanthaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne valle ne valle nalo nene gallanthaya"/>
</div>
</pre>
