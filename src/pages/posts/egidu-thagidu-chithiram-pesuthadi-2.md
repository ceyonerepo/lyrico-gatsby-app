---
title: "egidu thagidu song lyrics"
album: "Chithiram Pesuthadi 2"
artist: "Sajan Madhav"
lyricist: "Piriyan"
director: "Rajan Madhav"
path: "/albums/chithiram-pesuthadi-2-lyrics"
song: "Egidu Thagidu"
image: ../../images/albumart/chithiram-pesuthadi-2.jpg
date: 2019-02-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/T6rBWHgLt2I"
type: "happy"
singers:
  - Suchitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yei Egidu Thagidu Rarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Egidu Thagidu Rarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Elaakki Raarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaakki Raarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Asanju Varum Thaerae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asanju Varum Thaerae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Thaeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Thaeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Raathirukku Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Raathirukku Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsaara Kaeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaara Kaeni"/>
</div>
<div class="lyrico-lyrics-wrapper">Minu Minukkura Maeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minu Minukkura Maeni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaadu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaadu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikki Mukki Kannula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki Mukki Kannula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkikitta Allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkikitta Allu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththamara Kallunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththamara Kallunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukulla Thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulla Thallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Sikki Mukki Kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sikki Mukki Kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sikkikitta Allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sikkikitta Allu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Oththamara Kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Oththamara Kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ullukulla Thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ullukulla Thallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Soppanathu Sundariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Soppanathu Sundariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka Veikka Naanum Varavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka Veikka Naanum Varavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Egidu Thagidu Rarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Egidu Thagidu Rarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Elaakki Raarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaakki Raarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Asanju Varum Thaerae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asanju Varum Thaerae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Thaeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Thaeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Raathirukku Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Raathirukku Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsaara Kaeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaara Kaeni"/>
</div>
<div class="lyrico-lyrics-wrapper">Minu Minukkura Maeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minu Minukkura Maeni"/>
</div>
<div class="lyrico-lyrics-wrapper">Manthaagini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manthaagini"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Unakkum Enakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Unakkum Enakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkum Nerukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkum Nerukkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyum Varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudiyum Varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sala Sala Sala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sala Sala Sala Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiya Vidiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiya Vidiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilakku Eriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilakku Eriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anainja Nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anainja Nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala Kala Kala Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala Kala Kala Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natchathira Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natchathira Kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dam Daaladichu Kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dam Daaladichu Kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella Vidum Nottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella Vidum Nottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Dam Aadupuli Atam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dam Aadupuli Atam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natchathira Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natchathira Kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Daaladichu Kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Daaladichu Kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mella Vidum Nottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mella Vidum Nottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Aadupuli Atam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Aadupuli Atam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekku Thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekku Thappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottu Putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottavittaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottavittaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Egidu Thagidu Rarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Egidu Thagidu Rarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Elaakki Raarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaakki Raarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Asanju Varum Thaerae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asanju Varum Thaerae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Thaeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Thaeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Raathirukku Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Raathirukku Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsaara Kaeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaara Kaeni"/>
</div>
<div class="lyrico-lyrics-wrapper">Minu Minukkura Maeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minu Minukkura Maeni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaadu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaadu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei Idhu Thaan Valakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Idhu Thaan Valakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Thaan Thayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaan Thayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhuvaa Mayakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhuvaa Mayakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoda Thoda Thodarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoda Thoda Thodarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udambu Muzhukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambu Muzhukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kirakkam Paravum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kirakkam Paravum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodiya Suthi Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodiya Suthi Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Padapada Pada Padarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padapada Pada Padarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poothirukkum Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothirukkum Kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatula Thaen Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatula Thaen Koodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottukulla Thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottukulla Thaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poondhu Vilayaadu Poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poondhu Vilayaadu Poo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poothirukkum Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poothirukkum Kaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu Kaatula Thaen Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Kaatula Thaen Koodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaen Koottukulla Thaedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaen Koottukulla Thaedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Poondhu Vilayaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Poondhu Vilayaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aththai Peththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththai Peththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththai Ennai Allikavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththai Ennai Allikavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaa Aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Egidu Thagidu Rarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egidu Thagidu Rarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Elaakki Raarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaakki Raarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Asanju Varum Thaerae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asanju Varum Thaerae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Thaeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Thaeru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Raathirukku Raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Raathirukku Raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Minsaara Kaeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minsaara Kaeni"/>
</div>
<div class="lyrico-lyrics-wrapper">Minu Minukkura Maeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minu Minukkura Maeni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondaadu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaadu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikki Mukki Kannula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki Mukki Kannula"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkikitta Allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkikitta Allu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththamara Kallunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththamara Kallunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukulla Thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukulla Thallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Sikki Mukki Kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sikki Mukki Kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sikkikitta Allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sikkikitta Allu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Oththamara Kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Oththamara Kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ullukulla Thallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ullukulla Thallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Soppanathu Sundariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Soppanathu Sundariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka Veikka Naanum Varavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka Veikka Naanum Varavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaa"/>
</div>
</pre>
