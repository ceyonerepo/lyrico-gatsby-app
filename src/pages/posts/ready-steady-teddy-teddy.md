---
title: "ready steady teddy song lyrics"
album: "teddy"
artist: "D Imman"
lyricist: "madhan karky"
director: "shakti soundar rajan"
path: "/albums/teddy-song-lyrics"
song: "ready steady teddy"
image: ../../images/albumart/teddy.jpg
date: 2021-03-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kOAI-iI-2R4"
type: "affection"
singers:
  - Mark thomas
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">No time to reap in buddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No time to reap in buddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Look who’s around
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look who’s around"/>
</div>
<div class="lyrico-lyrics-wrapper">Teddy is back in town, Oo, Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teddy is back in town, Oo, Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bullets can’t kill me, baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bullets can’t kill me, baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Play if you dare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Play if you dare"/>
</div>
<div class="lyrico-lyrics-wrapper">Can even touch my hair, Ho Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Can even touch my hair, Ho Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Mr. Stranger…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Mr. Stranger…"/>
</div>
<div class="lyrico-lyrics-wrapper">I’m an encyclopedia for danger… Danger
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I’m an encyclopedia for danger… Danger"/>
</div>
<div class="lyrico-lyrics-wrapper">Punches for starter…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punches for starter…"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh main course ‘nd won’t matter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh main course ‘nd won’t matter"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">If you hug me… I’d be cozy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you hug me… I’d be cozy"/>
</div>
<div class="lyrico-lyrics-wrapper">If you bug me… I’d go crazy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If you bug me… I’d go crazy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">This is my This is my… This is my
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This is my This is my… This is my"/>
</div>
<div class="lyrico-lyrics-wrapper">This is my… privacy policy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="This is my… privacy policy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready steady teddy… Ready steady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready steady teddy… Ready steady"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready steady teddy… Rock ‘n’ roll already
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready steady teddy… Rock ‘n’ roll already"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready steady teddy… Ready steady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready steady teddy… Ready steady"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready steady teddy… Rock ‘n’ roll already
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready steady teddy… Rock ‘n’ roll already"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Let me tell you a story
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let me tell you a story"/>
</div>
<div class="lyrico-lyrics-wrapper">Long time ago…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Long time ago…"/>
</div>
<div class="lyrico-lyrics-wrapper">There lived a rowdy baby, Ho Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="There lived a rowdy baby, Ho Oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">When things were gettin dirty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When things were gettin dirty"/>
</div>
<div class="lyrico-lyrics-wrapper">My baby would smile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My baby would smile"/>
</div>
<div class="lyrico-lyrics-wrapper">Cas that’s the time to party, Ho Oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cas that’s the time to party, Ho Oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Landmines on dance floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Landmines on dance floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Your heart beats so fast… can ya beat slow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Your heart beats so fast… can ya beat slow"/>
</div>
<div class="lyrico-lyrics-wrapper">No time to fear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No time to fear"/>
</div>
<div class="lyrico-lyrics-wrapper">She’d put fire in your rear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She’d put fire in your rear"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Try kissin me… I’m your beary
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Try kissin me… I’m your beary"/>
</div>
<div class="lyrico-lyrics-wrapper">Try messin me… You’ll be sorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Try messin me… You’ll be sorry"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">That is da That is da… That is da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That is da That is da… That is da"/>
</div>
<div class="lyrico-lyrics-wrapper">That is da… End of our story
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That is da… End of our story"/>
</div>
</pre>
