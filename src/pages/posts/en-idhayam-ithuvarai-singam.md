---
title: "en idhayam ithuvarai song lyrics"
album: "Singam"
artist: "Devi Sri Prasad"
lyricist: "Na. Muthukumar"
director: "Hari Gopalakrishnan"
path: "/albums/singam-lyrics"
song: "En Idhayam Ithuvarai"
image: ../../images/albumart/singam.jpg
date: 2010-05-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YzA_4fg-iqs"
type: "Love"
singers:
  - Suchitra
  - Tippu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En Idhayam Idhuvarai Thudithathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Idhuvarai Thudithathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Thudikkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Thudikkiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manasu Idhuvarai Parandhadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu Idhuvarai Parandhadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Parakkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Parakkiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Edhanaal Edhanaal Theriyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Edhanaal Edhanaal Theriyavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhanaal Pidikkirdhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhanaal Pidikkirdhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Sugama Valiya Puraiyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Sugama Valiya Puraiyavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Sugamum Konjam Valiyum Serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Sugamum Konjam Valiyum Serndhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thurathugiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurathugiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Idhayam Idhuvarai Thudithathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Idhuvarai Thudithathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Thudikkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Thudikkiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manasu Idhuvarai Parandhadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu Idhuvarai Parandhadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Parakkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Parakkiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kootathil Nindraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootathil Nindraalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaiye Theduthu Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaiye Theduthu Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottraiyaai Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottraiyaai Ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnudan Nadakkudhu Kaalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudan Nadakkudhu Kaalgal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achchamey Illadha Pechiley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchamey Illadha Pechiley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayangudhu Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangudhu Nenjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Michamey Illaamal Unnidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Michamey Illaamal Unnidam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhen Thanjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhen Thanjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhaavani Modhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaavani Modhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saayudhey Theradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saayudhey Theradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendadi Naaladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendadi Naaladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooruadi Iluththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooruadi Iluththaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Idhayam Idhuvarai Thudithathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Idhuvarai Thudithathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Thudikkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Thudikkiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manasu Idhuvarai Parandhadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu Idhuvarai Parandhadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Parakkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Parakkiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnidam Eppodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnidam Eppodhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urimaiyaai Palagidavendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiyaai Palagidavendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vairamey Aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vairamey Aanaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholaitthida Thoondum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaitthida Thoondum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvarai En Nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvarai En Nenjil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illavey Illai Bayangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illavey Illai Bayangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irandu Naal Paartheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandu Naal Paartheney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mirattudhey Undhan Gunangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mirattudhey Undhan Gunangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iththanai Naatkalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iththanai Naatkalaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paduththadhum Uranginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paduththadhum Uranginen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irandunaal Kanaviley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandunaal Kanaviley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Kandu Vizhithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kandu Vizhithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Ithayam Idhuvarai Thudithathillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ithayam Idhuvarai Thudithathillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Thudikkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Thudikkiradhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manasu Idhuvarai Parandhadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasu Idhuvarai Parandhadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ippo Parakkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Parakkiradhey"/>
</div>
</pre>
