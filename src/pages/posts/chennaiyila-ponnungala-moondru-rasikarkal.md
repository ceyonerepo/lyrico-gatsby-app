---
title: "chennaiyila ponnungala song lyrics"
album: "Moondru Rasikarkal"
artist: "Ronnie Raphael"
lyricist: "Charu Hariharan"
director: "Shebi"
path: "/albums/moondru-rasikarkal-lyrics"
song: "Chennaiyila Ponnungala"
image: ../../images/albumart/moondru-rasikarkal.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/O7S0ck-2lY4"
type: "happy"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">chennaiyila ponnungala kanduka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chennaiyila ponnungala kanduka"/>
</div>
<div class="lyrico-lyrics-wrapper">ava 6 inch heal ah konjam kanduka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava 6 inch heal ah konjam kanduka"/>
</div>
<div class="lyrico-lyrics-wrapper">lowest jeans neyum potuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lowest jeans neyum potuka"/>
</div>
<div class="lyrico-lyrics-wrapper">ooruku vaa nee chennaiku vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooruku vaa nee chennaiku vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">smart ah hair ah strite pannika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="smart ah hair ah strite pannika"/>
</div>
<div class="lyrico-lyrics-wrapper">ada jollu vuda vai pannika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada jollu vuda vai pannika"/>
</div>
<div class="lyrico-lyrics-wrapper">beach orama site adikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beach orama site adikka"/>
</div>
<div class="lyrico-lyrics-wrapper">ooruku vaa nee chennaiku vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooruku vaa nee chennaiku vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chennaiyila ponnungala kanduka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chennaiyila ponnungala kanduka"/>
</div>
<div class="lyrico-lyrics-wrapper">ava 6 inch heal ah konjam kanduka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava 6 inch heal ah konjam kanduka"/>
</div>
<div class="lyrico-lyrics-wrapper">lowest jeans neyum potuka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lowest jeans neyum potuka"/>
</div>
<div class="lyrico-lyrics-wrapper">ooruku vaa nee chennaiku vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooruku vaa nee chennaiku vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">smart ah hair ah strite pannika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="smart ah hair ah strite pannika"/>
</div>
<div class="lyrico-lyrics-wrapper">ada jollu vuda vai pannika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada jollu vuda vai pannika"/>
</div>
<div class="lyrico-lyrics-wrapper">beach orama site adikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="beach orama site adikka"/>
</div>
<div class="lyrico-lyrics-wrapper">ooruku vaa nee chennaiku vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooruku vaa nee chennaiku vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">cent potu unna iluppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cent potu unna iluppa"/>
</div>
<div class="lyrico-lyrics-wrapper">ava internet il messge adipa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava internet il messge adipa"/>
</div>
<div class="lyrico-lyrics-wrapper">whatsup la ava nadippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="whatsup la ava nadippa"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal solli putta cut adippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal solli putta cut adippa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">loosu paiyan ivan tholla thangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="loosu paiyan ivan tholla thangala"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan queue ninnum pakathula etala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan queue ninnum pakathula etala"/>
</div>
<div class="lyrico-lyrics-wrapper">heart inch fast aachu nikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heart inch fast aachu nikala"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalukkula sikkave illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalukkula sikkave illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">loosu paiyan ivan tholla thangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="loosu paiyan ivan tholla thangala"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan queue ninnum pakathula etala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan queue ninnum pakathula etala"/>
</div>
<div class="lyrico-lyrics-wrapper">heart inch fast aachu nikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heart inch fast aachu nikala"/>
</div>
<div class="lyrico-lyrics-wrapper">kathalukkula sikkave illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalukkula sikkave illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">night fulla waste achu thoongala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="night fulla waste achu thoongala"/>
</div>
<div class="lyrico-lyrics-wrapper">love battery kulla charge ethum nikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love battery kulla charge ethum nikala"/>
</div>
<div class="lyrico-lyrics-wrapper">aeni vachum pakathula ettala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aeni vachum pakathula ettala"/>
</div>
<div class="lyrico-lyrics-wrapper">sugar thotu thotum thithikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugar thotu thotum thithikala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paaka paaka bp aeruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaka paaka bp aeruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnu pakkam vantha kick aeruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnu pakkam vantha kick aeruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">heart um heart um onnu seruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heart um heart um onnu seruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">kai thotathume pathikuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai thotathume pathikuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ponnungala noolu onu puduchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnungala noolu onu puduchan"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan hero va oorukulla nadichan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan hero va oorukulla nadichan"/>
</div>
<div class="lyrico-lyrics-wrapper">vambu sanda potukitu thirunjan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambu sanda potukitu thirunjan"/>
</div>
<div class="lyrico-lyrics-wrapper">annan vanthuta oodi tholanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annan vanthuta oodi tholanja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ponnungala noolu onu puduchan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnungala noolu onu puduchan"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan hero va oorukulla nadichan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan hero va oorukulla nadichan"/>
</div>
<div class="lyrico-lyrics-wrapper">vambu sanda potukitu thirunjan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vambu sanda potukitu thirunjan"/>
</div>
<div class="lyrico-lyrics-wrapper">annan vanthuta oodi tholanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annan vanthuta oodi tholanja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">love panni ullukulla rasichan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="love panni ullukulla rasichan"/>
</div>
<div class="lyrico-lyrics-wrapper">ava kannalagil sikikitu thavichan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava kannalagil sikikitu thavichan"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukulla sollama thudichan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukulla sollama thudichan"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna katikitu alaiyuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna katikitu alaiyuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooru oorama neyum vaa maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru oorama neyum vaa maa"/>
</div>
<div class="lyrico-lyrics-wrapper">udanja nenja konjam otti thaa maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udanja nenja konjam otti thaa maa"/>
</div>
<div class="lyrico-lyrics-wrapper">paakamale pora hema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paakamale pora hema"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal solli vida vetkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal solli vida vetkama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">chennai chennai ada chennai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chennai chennai ada chennai than"/>
</div>
<div class="lyrico-lyrics-wrapper">unai vaala vaikum idam chennai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai vaala vaikum idam chennai than"/>
</div>
<div class="lyrico-lyrics-wrapper">life ah cool aakuthu chennai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life ah cool aakuthu chennai than"/>
</div>
<div class="lyrico-lyrics-wrapper">intha oora pola ethum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha oora pola ethum illa"/>
</div>
</pre>
