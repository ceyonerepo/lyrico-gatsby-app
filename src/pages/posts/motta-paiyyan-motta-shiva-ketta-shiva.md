---
title: "motta paiyyan song lyrics"
album: "Motta Shiva Ketta Shiva"
artist: "Amresh Ganesh"
lyricist: "Sai Ramani"
director: "Sai Ramani"
path: "/albums/motta-shiva-ketta-shiva-lyrics"
song: "Motta Paiyyan"
image: ../../images/albumart/motta-shiva-ketta-shiva.jpg
date: 2017-03-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/s6U5H7B7w2w"
type: "mass"
singers:
  -	Raghava Lawrence
  - Suchitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hua hua hua hua hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hua hua hua hua hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on come on come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on come on come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on come on"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hua hua hua hua hoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hua hua hua hua hoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on come on come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on come on come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Come on come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on come on"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyyan paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyyan paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyyan paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyyan paiyyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyyan paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyyan paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyyan paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyyan paiyyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala madakkittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala madakkittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittam pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittam pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure-a ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure-a ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti thookkittan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti thookkittan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haiyayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">haiyayyo haiyayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haiyayyo haiyayyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Local ivan massu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local ivan massu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan classu mixudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan classu mixudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkal super starunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkal super starunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru veppen daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru veppen daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyayyo haiyayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyayyo haiyayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyayyo hai hai hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyayyo hai hai hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Style-a vandhu ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Style-a vandhu ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyyae pudikkalanu sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyae pudikkalanu sonna"/>
</div>
<div class="lyrico-lyrics-wrapper">Massaa vandhu ninna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Massaa vandhu ninna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa I love you sonna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa I love you sonna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soppu sundharai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soppu sundharai"/>
</div>
<div class="lyrico-lyrics-wrapper">Soppana sundhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soppana sundhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodi puttaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodi puttaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">En kanavu sundhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kanavu sundhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Enkitta maattikittaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enkitta maattikittaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyayyo haiyayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyayyo haiyayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyayyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyyan paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyyan paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyyan paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyyan paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta motta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta motta"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta motta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta motta"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyyan paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyyan paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyyan paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyyan paiyyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mam ma mam ma moo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mam ma mam ma moo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mam ma moo mam ma moo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mam ma moo mam ma moo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mam ma mam ma moo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mam ma mam ma moo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mam ma moo mam ma moo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mam ma moo mam ma moo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbangal alli allli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbangal alli allli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkaamal kodukiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaamal kodukiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrundrum solli solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrundrum solli solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbangal alli allli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbangal alli allli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketkaamal kodukiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkaamal kodukiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrundrum solli solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrundrum solli solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne seigiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne seigiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ne nijamaai vazhgiraiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne nijamaai vazhgiraiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhalai verukkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhalai verukkirai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhiga anbai kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhiga anbai kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasikka vaikkiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikka vaikkiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mam ma mam ma moo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mam ma mam ma moo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mam ma moo mam ma moo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mam ma moo mam ma moo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mam ma mam ma moo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mam ma mam ma moo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mam ma moo mam ma moo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mam ma moo mam ma moo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodu konjipesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu konjipesa"/>
</div>
<div class="lyrico-lyrics-wrapper">En ullam nerungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ullam nerungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaala kaadhal veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala kaadhal veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallaaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallaaduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodu konjipesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu konjipesa"/>
</div>
<div class="lyrico-lyrics-wrapper">En ullam nerungudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ullam nerungudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaala kaadhal veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala kaadhal veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallaaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallaaduthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unmoochu kaatril naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmoochu kaatril naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Parandhu pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un udhadu pattathaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un udhadu pattathaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan urugi pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan urugi pogiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyyan paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyyan paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyyan paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyyan paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta motta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta motta"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta motta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta motta"/>
</div>
<div class="lyrico-lyrics-wrapper">Motta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyyan paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyyan paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyyan paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyyan paiyyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala madakkittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala madakkittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thittam pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittam pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure-a ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure-a ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thatti thookkittan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thatti thookkittan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Local ivan massu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Local ivan massu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan classu mixudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan classu mixudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkal super starunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkal super starunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru veppen daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru veppen daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyayyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyyan paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyyan paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta paiyyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiyyan paiyyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiyyan paiyyan"/>
</div>
</pre>
