---
title: "ore pakal song lyrics"
album: "Drushyam 2"
artist: "Anil Johnson"
lyricist: "Vinayak Sasikumar"
director: "Jeethu Joseph"
path: "/albums/drushyam-2-lyrics"
song: "Ore Pakal"
image: ../../images/albumart/drushyam-2.jpg
date: 2021-11-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/LUj1rICKi0M"
type: "melody"
singers:
  - Zonobia Safar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oreyyyyy pakal oreyyyy irul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oreyyyyy pakal oreyyyy irul"/>
</div>
<div class="lyrico-lyrics-wrapper">Atheyyyy veyil atheyyy nizhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atheyyyy veyil atheyyy nizhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Etho kalam dhoore thedunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etho kalam dhoore thedunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerathee janmam neelunnu neelunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerathee janmam neelunnu neelunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yamangalil rapadikal moolunnuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamangalil rapadikal moolunnuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ravormmakal thenmariyay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravormmakal thenmariyay"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounangalum bharangalay theerunnuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounangalum bharangalay theerunnuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum kalam dhoore thedunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum kalam dhoore thedunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerathee janmam neelunnu neelunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerathee janmam neelunnu neelunnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee yathrathan kal padukal manjeedumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee yathrathan kal padukal manjeedumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Swapnangalam azhangalil thazhneedumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swapnangalam azhangalil thazhneedumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Veshangalil kolangalay mareedumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veshangalil kolangalay mareedumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum kaalam dhoore thedunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum kaalam dhoore thedunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerathee janmam neelunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerathee janmam neelunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelunnu neelunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelunnu neelunnu"/>
</div>
</pre>
