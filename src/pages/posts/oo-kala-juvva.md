---
title: "oo kala song lyrics"
album: "Juvva"
artist: "M M Keeravani"
lyricist: "Wasistha Varma"
director: "Trikoti Peta"
path: "/albums/juvva-lyrics"
song: "Oo Kala"
image: ../../images/albumart/juvva.jpg
date: 2018-02-23
lang: telugu
youtubeLink: "https://www.youtube.com/embed/_UJpTfCB2Nc"
type: "love"
singers:
  -	Damini dhatya
  - Kala dhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">oo kala etu ra ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo kala etu ra ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa jathai yuvaranil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa jathai yuvaranil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye dhooram addunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye dhooram addunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu cheraga vastunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu cheraga vastunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yekanthamlo thoduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yekanthamlo thoduga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenantu ikapaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenantu ikapaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee needai vuntunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee needai vuntunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee manamai poyenthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee manamai poyenthala"/>
</div>
<div class="lyrico-lyrics-wrapper">Eevela yedho mayaga undhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eevela yedho mayaga undhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhi moyalekundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhi moyalekundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemaindhi evela nalona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaindhi evela nalona"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho mayaga undhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho mayaga undhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhi moyalekundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhi moyalekundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemaindhi evela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaindhi evela"/>
</div>
<div class="lyrico-lyrics-wrapper">maimarapinchelaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maimarapinchelaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Itu ra ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itu ra ila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yentha apina allarapadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yentha apina allarapadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee alochana konthsepina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee alochana konthsepina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalo nelena yentha apina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalo nelena yentha apina"/>
</div>
<div class="lyrico-lyrics-wrapper">Allarapadhe nee alochana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allarapadhe nee alochana"/>
</div>
<div class="lyrico-lyrics-wrapper">Konthsepaina nalo nelena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konthsepaina nalo nelena"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasa raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasa raasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Noorellu nkosame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noorellu nkosame"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa shwasa okate asha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa shwasa okate asha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thovunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thovunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalantu adugesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalantu adugesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliso theliyayaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliso theliyayaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase needhai poyindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase needhai poyindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai veedani naa aropranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai veedani naa aropranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvandhi kalam dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvandhi kalam dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Karigaye okatayyea
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karigaye okatayyea"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Evela yedho mayaga undhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evela yedho mayaga undhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhi moyalekundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhi moyalekundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemaindhi evela nalone yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaindhi evela nalone yedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayaga undhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayaga undhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhi moyalekundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhi moyalekundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemaindhi evela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemaindhi evela"/>
</div>
<div class="lyrico-lyrics-wrapper">Maimarapinchelaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maimarapinchelaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo kala itu ra ila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo kala itu ra ila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee kshanam nijamavvaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee kshanam nijamavvaga"/>
</div>
</pre>
