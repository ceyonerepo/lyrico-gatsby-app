---
title: "natpu song lyrics"
album: "RRR Tamil"
artist: "Maragathamani"
lyricist: "Madhan Karky"
director: "S.S. Rajamouli "
path: "/albums/rrr-tamil-lyrics"
song: "Natpu"
image: ../../images/albumart/rrr-tamil.jpg
date: 2022-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0p4mGx7QIU0"
type: "Friendship"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Puliyum appedanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puliyum appedanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyalum oru ongalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalum oru ongalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Punalum madaivayilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punalum madaivayilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Puzhamum perupookkayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puzhamum perupookkayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pularum irul vaanamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pularum irul vaanamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh Natpaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Natpaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engagilum paarthathu undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engagilum paarthathu undo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee neerudan sernthathu undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee neerudan sernthathu undo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar yaar inai vizhimbuvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar yaar inai vizhimbuvar"/>
</div>
<div class="lyrico-lyrics-wrapper">Enbadhai solvar undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enbadhai solvar undo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara tham thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara tham thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum thara thum thum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum thara thum thum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara tham thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara tham thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum thara thum thum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum thara thum thum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum thara thum thum thum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum thara thum thum thum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalutheriya kairodu natpai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalutheriya kairodu natpai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhugum oru thaaliliyum natpai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhugum oru thaaliliyum natpai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavum oru vizhipunarvum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavum oru vizhipunarvum "/>
</div>
<div class="lyrico-lyrics-wrapper">karam koodiya kadhai undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karam koodiya kadhai undo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara tham thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara tham thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum thara thum thum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum thara thum thum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara tham thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara tham thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum thara thum thum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum thara thum thum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum thara thum thum thum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum thara thum thum thum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasi aarum pagaivanai kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi aarum pagaivanai kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam ingu magizhvathu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam ingu magizhvathu yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyora kaanal kanneeril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyora kaanal kanneeril"/>
</div>
<div class="lyrico-lyrics-wrapper">Poiyum meiyaagutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiyum meiyaagutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karaiyil than nigazhlinai kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyil than nigazhlinai kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagai thedi alaivathu yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagai thedi alaivathu yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhiyamaiyaale mann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhiyamaiyaale mann"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingum inbam undagutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingum inbam undagutho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedalin uruvangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalin uruvangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaiyum endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiyum endre"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyarkai ezhithiyatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyarkai ezhithiyatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedalin payanangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalin payanangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Inaiyum endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaiyum endre"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayangal ezhithiyatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayangal ezhithiyatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engagilum paarthathu undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engagilum paarthathu undo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thee neerudan sernthathu undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee neerudan sernthathu undo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar yaar inai vizhimbuvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar yaar inai vizhimbuvar"/>
</div>
<div class="lyrico-lyrics-wrapper">Enbadhai solvar undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enbadhai solvar undo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara tham thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara tham thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum thara thum thum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum thara thum thum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara tham thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara tham thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum thara thum thum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum thara thum thum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum thara thum thum thum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum thara thum thum thum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalutheriya kairodu natpai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalutheriya kairodu natpai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhugum oru thaaliliyum natpai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhugum oru thaaliliyum natpai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaiyum adhu kuriyizhakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaiyum adhu kuriyizhakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravadiya kadhai undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravadiya kadhai undo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara tham thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara tham thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum thara thum thum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum thara thum thum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara tham thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara tham thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum thara thum thum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum thara thum thum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thum thara thum thum thum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thum thara thum thum thum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalutheriya kairodu natpai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalutheriya kairodu natpai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhugum oru thaaliliyum natpai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhugum oru thaaliliyum natpai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhugum oru sitrembum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhugum oru sitrembum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadiya kadhai undo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadiya kadhai undo"/>
</div>
</pre>
