---
title: "yadumaghiye song lyrics"
album: "Seedan"
artist: "Dhina"
lyricist: "Pa. Vijay"
director: "Subramaniam Siva"
path: "/albums/seedan-lyrics"
song: "Yadumaghiye"
image: ../../images/albumart/seedan.jpg
date: 2011-02-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/upJq65E5lXk"
type: "happy"
singers:
  - Shankar Mahadevan
  - Kavita Krishnamurthy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkita thakitta thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkita thakitta thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkita thakitta thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkita thakitta thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkita thakitta thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkita thakitta thakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagana jagana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jagana jagana nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaah rara rara raraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaah rara rara raraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera theera naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera theera naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rara rara raraa raraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara rara raraa raraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera theera naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera theera naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaadhumaagiyae vedhamaagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaadhumaagiyae vedhamaagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirkkum kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirkkum kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal enbadhum kadavul enbadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal enbadhum kadavul enbadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin meeralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin meeralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh yaadhumaagiyae vedhamaagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yaadhumaagiyae vedhamaagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirkkum kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirkkum kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal enbadhum kadavul enbadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal enbadhum kadavul enbadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin meeralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin meeralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallodu varum kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallodu varum kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarum bakthiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarum bakthiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannodu varum kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannodu varum kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum sakthiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum sakthiyaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arivaai manamae arindhaal nalamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivaai manamae arindhaal nalamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagasasa nisanini panipapa mapamama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagasasa nisanini panipapa mapamama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamagaga sagasasa ga ma pa ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamagaga sagasasa ga ma pa ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Agalum bayamae anaiththum jeyamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agalum bayamae anaiththum jeyamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh yaadhumaagiyae vedhamaagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yaadhumaagiyae vedhamaagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirkkum kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirkkum kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal enbadhum kadavul enbadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal enbadhum kadavul enbadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin meeralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin meeralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaah rara rara raraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaah rara rara raraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera theera naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera theera naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rara rara raraa raraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara rara raraa raraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera theera naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera theera naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa ni sa ni dha ma pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ni sa ni dha ma pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ri sa ga ri sa ni sa ni dha ma pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ri sa ga ri sa ni sa ni dha ma pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa ni sa ni dha ma pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa ni sa ni dha ma pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ri sa ga ri sa ni sa ni dha ma pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ri sa ga ri sa ni sa ni dha ma pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tharigida thathakku thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigida thathakku thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathakku thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathakku thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathakku thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathakku thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharigida thaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigida thaththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadham vandhadhu veliyidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadham vandhadhu veliyidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedham vandhandhu oliyidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedham vandhandhu oliyidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naavum vandhadhu mozhiyidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naavum vandhadhu mozhiyidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaavum vandhadhu avanidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum vandhadhu avanidam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En manam engum malarcholai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manam engum malarcholai"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan thandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan thandhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaiyudhirkaalam adhil ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaiyudhirkaalam adhil ingu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen vandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen vandhadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhilum edhilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhilum edhilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru nanmai irukkumena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru nanmai irukkumena"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduththukkolga en annamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduththukkolga en annamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaikka ninaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaikka ninaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu nadakkum nadakkumena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu nadakkum nadakkumena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambum ullam verum munnamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambum ullam verum munnamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arivaai manamae arindhaal nalamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivaai manamae arindhaal nalamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagasasa nisanini panipapa mapamama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagasasa nisanini panipapa mapamama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamagaga sagasasa ga ma pa ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamagaga sagasasa ga ma pa ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Agalum bayamae anaiththum jeyamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agalum bayamae anaiththum jeyamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaam thakkita thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaam thakkita thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakitta thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakitta thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakitta thaam thakitta thakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakitta thaam thakitta thakitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh yaadhumaagiyae vedhamaagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yaadhumaagiyae vedhamaagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirkkum kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirkkum kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal enbadhum kadavul enbadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal enbadhum kadavul enbadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin meeralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin meeralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhom thakkita thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhom thakkita thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhom thakkita thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhom thakkita thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhom thakkita thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhom thakkita thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhom thakkita thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhom thakkita thakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhom thaka kitta thaa kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhom thaka kitta thaa kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhom tha kitta thakka kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhom tha kitta thakka kitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakakitta dhomkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakakitta dhomkitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhom thakkita thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhom thakkita thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhom thakkita thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhom thakkita thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhom thakkita thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhom thakkita thakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhom thakkita thakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhom thakkita thakka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalum iniyum thaan en seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum iniyum thaan en seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodungolum poottumae en seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodungolum poottumae en seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambi odivaa nadandhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambi odivaa nadandhidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini undhan vedhanai kadandhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini undhan vedhanai kadandhidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En ninaivodum kanavodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En ninaivodum kanavodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan sindhanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan sindhanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna irundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna irundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkindru nidham nindhanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkindru nidham nindhanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhu unadhu manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhu unadhu manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarum unarum vidham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarum unarum vidham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigazhum nigazhum idhu saththiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigazhum nigazhum idhu saththiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uruga uruga ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruga uruga ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuga varuga udal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuga varuga udal"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhava adhavan thatthuvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhava adhavan thatthuvam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arivaai manamae arindhaal nalamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivaai manamae arindhaal nalamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagasasa nisanini panipapa mapamama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagasasa nisanini panipapa mapamama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamagaga sagasasa ga ma pa ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamagaga sagasasa ga ma pa ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Agalum bayamae anaiththum jeyamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agalum bayamae anaiththum jeyamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaam thakkita thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaam thakkita thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakitta thaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakitta thaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakitta thaam thakitta thakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakitta thaam thakitta thakitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh yaadhumaagiyae vedhamaagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yaadhumaagiyae vedhamaagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirkkum kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirkkum kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal enbadhum kadavul enbadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal enbadhum kadavul enbadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin meeralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin meeralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh yaadhumaagiyae vedhamaagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yaadhumaagiyae vedhamaagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirkkum kaadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirkkum kaadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal enbadhum kadavul enbadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal enbadhum kadavul enbadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin meeralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin meeralae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aa aa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aa aa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rara rara raraa raraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara rara raraa raraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera theera naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera theera naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rara rara raraa raraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rara rara raraa raraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Theera theera naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera theera naa"/>
</div>
</pre>
