---
title: "life happy song lyrics"
album: "Odu Raja Odu"
artist: "Tosh Nanda"
lyricist: "Kavithran"
director: "Nishanth Ravindaran - Jathin Sanker Raj"
path: "/albums/odu-raja-odu-lyrics"
song: "Life Happy"
image: ../../images/albumart/odu-raja-odu.jpg
date: 2018-08-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4wWS0DrLMAg"
type: "happy"
singers:
  - Abishek
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Happy Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy Happy Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy Happy Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy Happy Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy Happy Happy Happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dimpa Lakdi Bimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Lakdi Bimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bimpa Lakdi Dimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bimpa Lakdi Dimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimpa Lakdi Bimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Lakdi Bimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimpa Dimpa Dimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Dimpa Dimpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dimpa Lakdi Bimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Lakdi Bimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bimpa Lakdi Dimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bimpa Lakdi Dimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimpa Lakdi Bimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Lakdi Bimpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life-u Semma Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-u Semma Happy Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli Kuthipom Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli Kuthipom Happy Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattamboochi Kootam Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattamboochi Kootam Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthu Thirivom Vanavil Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthu Thirivom Vanavil Mele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boomikku Oru Puttai Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomikku Oru Puttai Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnai Thaandi Podu Route
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnai Thaandi Podu Route"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavin Mele Paadal Kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavin Mele Paadal Kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinmeen Pidipaai Thoondil Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinmeen Pidipaai Thoondil Pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thattu Thadaikal Yeathum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thattu Thadaikal Yeathum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu Kaaval Onnum Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu Kaaval Onnum Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai Illai Illai Thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai Illai Illai Thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Thodakkam Andha Mudivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Thodakkam Andha Mudivu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeathum Inge Marmam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeathum Inge Marmam Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dimpa Lakdi Bimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Lakdi Bimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bimpa Lakdi Dimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bimpa Lakdi Dimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimpa Lakdi Bimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Lakdi Bimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimpa Dimpa Dimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Dimpa Dimpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dimpa Lakdi Bimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Lakdi Bimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bimpa Lakdi Dimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bimpa Lakdi Dimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimpa Lakdi Bimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Lakdi Bimpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Happy Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy Happy Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy Happy Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy Happy Happy Happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Happy Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy Happy Happy"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy Happy Happy Happy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy Happy Happy Happy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dimpa Lakdi Bimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Lakdi Bimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bimpa Lakdi Dimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bimpa Lakdi Dimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimpa Lakdi Bimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Lakdi Bimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimpa Dimpa Dimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Dimpa Dimpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dimpa Lakdi Bimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Lakdi Bimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bimpa Lakdi Dimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bimpa Lakdi Dimpa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dimpa Lakdi Bimpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dimpa Lakdi Bimpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Hey Hey"/>
</div>
</pre>
