---
title: "ninnu chuda song lyrics"
album: "Oka Chinna Viramam"
artist: "Bharath Manchiraju"
lyricist: "Ganesh Salaadi"
director: "Sundeep Cheguri"
path: "/albums/oka-chinna-viramam-lyrics"
song: "Ninnu Chuda"
image: ../../images/albumart/oka-chinna-viramam.jpg
date: 2020-02-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/oiJXRvyWIPo"
type: "happy"
singers:
  - Yadu Krishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ninnu chudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu chudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Praanamunde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Praanamunde "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu cheraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu cheraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu chudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu chudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Praanamunde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Praanamunde "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu cheraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu cheraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannalle Naapai Ila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannalle Naapai Ila "/>
</div>
<div class="lyrico-lyrics-wrapper">Dukave Shishirma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dukave Shishirma"/>
</div>
<div class="lyrico-lyrics-wrapper">Hrudayam Sandramayenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hrudayam Sandramayenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">AgsiPadega Neevalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AgsiPadega Neevalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Akash Vidhullon Navvu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akash Vidhullon Navvu "/>
</div>
<div class="lyrico-lyrics-wrapper">Tandanukoni Vachchindila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tandanukoni Vachchindila"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Darinostoone Vennelni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Darinostoone Vennelni "/>
</div>
<div class="lyrico-lyrics-wrapper">Laagesi Ila Cherenugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laagesi Ila Cherenugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninnu chudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu chudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Praanamunde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Praanamunde "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu cheraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu cheraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu chudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu chudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu chudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Praanamunde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Praanamunde "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu cheraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu cheraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Gundello Ee Savvadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gundello Ee Savvadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnavuga Ede Cheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnavuga Ede Cheri"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolanni Nee Perune 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolanni Nee Perune "/>
</div>
<div class="lyrico-lyrics-wrapper">Pettukunnave Ninnu Choosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettukunnave Ninnu Choosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Maata cheranga Naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maata cheranga Naa "/>
</div>
<div class="lyrico-lyrics-wrapper">Vinulku Vindedo Mandedola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinulku Vindedo Mandedola"/>
</div>
<div class="lyrico-lyrics-wrapper">Untune Vayyalalugalila Kaugiliga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Untune Vayyalalugalila Kaugiliga"/>
</div>
</pre>
