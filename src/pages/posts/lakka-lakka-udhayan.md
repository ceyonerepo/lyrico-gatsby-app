---
title: "lakka lakka song lyrics"
album: "Udhayan"
artist: "Manikanth Kadri"
lyricist: "Vaali - Yugabharathi - Annamalai - Surya - Muthamil"
director: "Chaplin"
path: "/albums/udhayan-lyrics"
song: "Lakka Lakka"
image: ../../images/albumart/udhayan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Prasanna
  - Darshana KT
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Lakkaa lakkaa melirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakkaa lakkaa melirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">lakkaa lakkaa keezhirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lakkaa lakkaa keezhirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">enga enga kaadhalunnu nee solvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga enga kaadhalunnu nee solvaayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lakkaa lakkaa maela illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakkaa lakkaa maela illa"/>
</div>
<div class="lyrico-lyrics-wrapper">lakkaa lakkaa keezha illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lakkaa lakkaa keezha illa"/>
</div>
<div class="lyrico-lyrics-wrapper">angamellaam kaadhalundu aadharippaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angamellaam kaadhalundu aadharippaayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaraappa neekkikko machaana neekkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaraappa neekkikko machaana neekkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">koorathila ninnu tholaiyakoodaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koorathila ninnu tholaiyakoodaathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sari sari sari sari sari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari sari sari sari sari "/>
</div>
<div class="lyrico-lyrics-wrapper">sari sari sari erichala moottaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari sari sari erichala moottaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravukku saamandhi kaalaikku sevvandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravukku saamandhi kaalaikku sevvandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">vaangittu naan vaaren vachikka vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaangittu naan vaaren vachikka vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollaama sonnaalum seiyaama senjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaama sonnaalum seiyaama senjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">kannaalam kattaama seraadhu joadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannaalam kattaama seraadhu joadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singaare singaare singaare singaare singaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaare singaare singaare singaare singaare"/>
</div>
<div class="lyrico-lyrics-wrapper">singaare singaare singaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singaare singaare singaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Singaare singaare singaare singaare singaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaare singaare singaare singaare singaare"/>
</div>
<div class="lyrico-lyrics-wrapper">singaare singaare singaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="singaare singaare singaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee anaikka thodangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee anaikka thodangu"/>
</div>
<div class="lyrico-lyrics-wrapper">thavaredhum illa velagaadha pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavaredhum illa velagaadha pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singaare singaare singaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaare singaare singaare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thavichaa pudichaa thodappovadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thavichaa pudichaa thodappovadhilla"/>
</div>
<div class="lyrico-lyrics-wrapper">thoda thevaiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoda thevaiyilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada summa summa kala katturiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada summa summa kala katturiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vambaa vambaa kuzhi vetturiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambaa vambaa kuzhi vetturiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sari sari sari sari sari sari sari sari sari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sari sari sari sari sari sari sari sari sari"/>
</div>
<div class="lyrico-lyrics-wrapper">erichala moottaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="erichala moottaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppaadu pattaalum engey nee thottaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppaadu pattaalum engey nee thottaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">thappaagamaatteney poadaadey moodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappaagamaatteney poadaadey moodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeham mukkaadu ittaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeham mukkaadu ittaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">munthaanai suttaalum eppoadhum nee thaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munthaanai suttaalum eppoadhum nee thaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">ennoada megi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennoada megi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee vanappa maraikka very yeriduchu pasi koodiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vanappa maraikka very yeriduchu pasi koodiduchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sindaarey sindaarey sindaarey sindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sindaarey sindaarey sindaarey sindaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee parichaa puzhinjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee parichaa puzhinjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">varuvenaa machi tharuvena pichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varuvenaa machi tharuvena pichi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye indha indha idhu thappu pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye indha indha idhu thappu pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye thallu thallu tharam onnum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thallu thallu tharam onnum illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei sari sari sari sari sari sari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei sari sari sari sari sari sari"/>
</div>
<div class="lyrico-lyrics-wrapper">irukkudhu veraaru ennoadu kai sera koadaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukkudhu veraaru ennoadu kai sera koadaari"/>
</div>
<div class="lyrico-lyrics-wrapper">neeppanna poovoadu naaraaga povene vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeppanna poovoadu naaraaga povene vaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaroadu neer poala aiyaa nee poanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaroadu neer poala aiyaa nee poanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">maaraadhu en kaadhal naaney un joadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaraadhu en kaadhal naaney un joadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lakkaa lakkaa melirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakkaa lakkaa melirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">lakkaa lakkaa keezhirukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lakkaa lakkaa keezhirukka"/>
</div>
<div class="lyrico-lyrics-wrapper">enga enga kaadhalunnu nee solvaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga enga kaadhalunnu nee solvaayaa"/>
</div>
</pre>
