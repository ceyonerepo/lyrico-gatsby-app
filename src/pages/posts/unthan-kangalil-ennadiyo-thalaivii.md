---
title: "unthan kangalil ennadiyo song lyrics"
album: "Thalaivii"
artist: "G.V. Prakash Kumar"
lyricist: "Madhan Karky"
director: "A.L. Vijay"
path: "/albums/thalaivii-song-lyrics"
song: "Unthan Kangalil Ennadiyo"
image: ../../images/albumart/thalaivii.jpg
date: 2021-09-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ws9Do_uym9I"
type: "love"
singers:
  - Nakul Abhyankar
  - Niranjana Ramanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unthan kangalil ennadiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan kangalil ennadiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnaal minnidum or kanaavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnaal minnidum or kanaavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vannaa kanavil vanthathu yaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannaa kanavil vanthathu yaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho chinnavar enbavaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho chinnavar enbavaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannam rendil chinnam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannam rendil chinnam enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan ennavaar thanthathuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan ennavaar thanthathuvo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam konjam kenjum nenjai paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam konjam kenjum nenjai paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmai poole angal undu nooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmai poole angal undu nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayirathil naan oruvan enbaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayirathil naan oruvan enbaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai irunthal yaar ethavum solvaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai irunthal yaar ethavum solvaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udaal vida ullam thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaal vida ullam thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu ondradum mandrada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu ondradum mandrada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakkam vaara vetkam tharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam vaara vetkam tharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enthan kovamthai ondradu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enthan kovamthai ondradu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vizhiyil naan irunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vizhiyil naan irunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Verenna vendumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verenna vendumadi"/>
</div>
</pre>
