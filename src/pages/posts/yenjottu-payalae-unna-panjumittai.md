---
title: "yenjottu payalae unna song lyrics"
album: "Panjumittai"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "S.P. Mohan"
path: "/albums/panjumittai-lyrics"
song: "Yenjottu Payalae Unna"
image: ../../images/albumart/panjumittai.jpg
date: 2018-06-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/e-Zl1QCF4aU"
type: "sad"
singers:
  - Diwakar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yenjottu payale unna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenjottu payale unna "/>
</div>
<div class="lyrico-lyrics-wrapper">enathuku nooga vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enathuku nooga vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">pothatha puthiyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothatha puthiyaale"/>
</div>
<div class="lyrico-lyrics-wrapper">pookarunthu poga vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookarunthu poga vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">nilalaa thodarntha unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilalaa thodarntha unna"/>
</div>
<div class="lyrico-lyrics-wrapper">nisama purunjukala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nisama purunjukala"/>
</div>
<div class="lyrico-lyrics-wrapper">kadala iruntha natpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadala iruntha natpa"/>
</div>
<div class="lyrico-lyrics-wrapper">kinaru valangikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kinaru valangikala"/>
</div>
<div class="lyrico-lyrics-wrapper">nee illa ulagatha naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee illa ulagatha naan"/>
</div>
<div class="lyrico-lyrics-wrapper">nenache pathathu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenache pathathu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee illa ulagam onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee illa ulagam onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">irukka theriya villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukka theriya villa"/>
</div>
<div class="lyrico-lyrics-wrapper">unna vitta enaku etho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna vitta enaku etho"/>
</div>
<div class="lyrico-lyrics-wrapper">yaru intha boomiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaru intha boomiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">pogatha enna vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogatha enna vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">soorina ulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soorina ulla "/>
</div>
<div class="lyrico-lyrics-wrapper">yenjottu payale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenjottu payale"/>
</div>
</pre>
