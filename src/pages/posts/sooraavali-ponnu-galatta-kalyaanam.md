---
title: "sooraavali ponnu song lyrics"
album: "Galatta Kalyaanam"
artist: "A R Rahman "
lyricist: "Vivek"
director: "Aanand L Rai"
path: "/albums/galatta-kalyaanam-song-lyrics"
song: "Sooraavali Ponnu"
image: ../../images/albumart/galatta-kalyaanam.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-cdmso_xAYw"
type: "happy"
singers:
  - AR Ameen
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Oram Po Oram Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Oram Po Oram Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Thalli Po Thalli Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thalli Po Thalli Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Koova Sonna Urumbuthu Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koova Sonna Urumbuthu Po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Bomma Koduthe Bomma Koduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Bomma Koduthe Bomma Koduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adakki Vacha Adakki Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakki Vacha Adakki Vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalamuraiya Thalamuraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalamuraiya Thalamuraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanduthu Po Dur Dur
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanduthu Po Dur Dur"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achukku Pichukku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achukku Pichukku Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinji Pichukku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinji Pichukku Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooraavali Ponnu Iva Doi Doi Doi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooraavali Ponnu Iva Doi Doi Doi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koondellam Marikkatha Kattu Kili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondellam Marikkatha Kattu Kili"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanavilla Vesam Kattum Minnal Ozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanavilla Vesam Kattum Minnal Ozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyani Paathalum Deepavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyani Paathalum Deepavali"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen Endru Per Konda Sooraavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Endru Per Konda Sooraavali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silu Silunu Varuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silu Silunu Varuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Seetti Adikkum Nilava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Seetti Adikkum Nilava"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooraavali Ponnu Iva Doi Doi Doi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooraavali Ponnu Iva Doi Doi Doi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Palasellam Kathari Kathari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Palasellam Kathari Kathari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Ilakkanam Ponnukku Sithari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Ilakkanam Ponnukku Sithari"/>
</div>
<div class="lyrico-lyrics-wrapper">Adakki Vachavan Othadi Othadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakki Vachavan Othadi Othadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuthu Muraicha Gethadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuthu Muraicha Gethadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kootathil Nikkura Gunamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootathil Nikkura Gunamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikka Pakkura Inamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikka Pakkura Inamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vediya Pathu Bayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vediya Pathu Bayama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikka Vachathe Ava Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikka Vachathe Ava Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silu Silunu Varuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silu Silunu Varuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Seetti Adikkum Nilava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Seetti Adikkum Nilava"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooraavali Ponnu Iva Doi Doi Doi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooraavali Ponnu Iva Doi Doi Doi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Oram Po Oram Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Oram Po Oram Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Thalli Po Thalli Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thalli Po Thalli Po"/>
</div>
<div class="lyrico-lyrics-wrapper">Koova Sonna Urumbuthu Po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koova Sonna Urumbuthu Po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Bomma Koduthe Bomma Koduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Bomma Koduthe Bomma Koduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adakki Vacha Adakki Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adakki Vacha Adakki Vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalamuraiya Thalamuraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalamuraiya Thalamuraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanduthu Po Dur Dur
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanduthu Po Dur Dur"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achukku Pichukku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achukku Pichukku Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudinji Pichukku Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinji Pichukku Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooraavali Ponnu Iva Doi Doi Doi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooraavali Ponnu Iva Doi Doi Doi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koondellam Marikkatha Kattu Kili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondellam Marikkatha Kattu Kili"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanavilla Vesam Kattum Minnal Ozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanavilla Vesam Kattum Minnal Ozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyani Paathalum Deepavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyani Paathalum Deepavali"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen Endru Per Konda Sooraavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Endru Per Konda Sooraavali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silu Silunu Varuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silu Silunu Varuva"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Seetti Adikkum Nilava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Seetti Adikkum Nilava"/>
</div>
<div class="lyrico-lyrics-wrapper">Sooraavali Ponnu Iva Doi Doi Doi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooraavali Ponnu Iva Doi Doi Doi"/>
</div>
</pre>
