---
title: "kannala poduraale song lyrics"
album: "Seeru"
artist: "D. Imman"
lyricist: "Viveka - RJ Vijay"
director: "Rathnasiva"
path: "/albums/seeru-lyrics"
song: "Kannala Poduraale"
image: ../../images/albumart/seeru.jpg
date: 2020-02-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qXPmHTbQ88Q"
type: "Love"
singers:
  - Nakash Aziz
  - RJ Vijay
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anaivarukkum Vanakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaivarukkum Vanakkam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indraiya Raasi Palan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indraiya Raasi Palan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mesam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Appa Urgentukku Kaasu Ketta Kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Appa Urgentukku Kaasu Ketta Kuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rendu Vattiku Thaan Kuduppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Vattiku Thaan Kuduppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rishabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rishabam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalilla Kaathaadi Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalilla Kaathaadi Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalillaama Single Ah Suthuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalillaama Single Ah Suthuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mithunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mithunam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boss Solra Mokka Joke ku Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boss Solra Mokka Joke ku Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthu Vizhunthu Sirippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthu Vizhunthu Sirippa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appa Adutha Increment Unakkuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appa Adutha Increment Unakkuthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simmam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bike Side Mirrorla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bike Side Mirrorla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandiyayum Konjam Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandiyayum Konjam Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Your Day will be Funny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Your Day will be Funny"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Poduraaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Poduraaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaantha Oosi Gaantha Oosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaantha Oosi Gaantha Oosi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Pinnaala Uyir Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pinnaala Uyir Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya Veesi Kaiya Veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Veesi Kaiya Veesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Poduraaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Poduraaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaantha Oosi Gaantha Oosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaantha Oosi Gaantha Oosi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Pinnaala Uyir Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pinnaala Uyir Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya Veesi Kaiya Veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Veesi Kaiya Veesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavaada Paravainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaada Paravainga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Raasi Enna Raasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Raasi Enna Raasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathukko Ellaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukko Ellaame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Easy Romba Easy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Easy Romba Easy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mookaanda Meesa Molachuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mookaanda Meesa Molachuruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu Kazhutha Vayasu Aayiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu Kazhutha Vayasu Aayiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnunna Enna Purunchiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnunna Enna Purunchiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puttu Puttu Vaikkuren Kelu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttu Puttu Vaikkuren Kelu Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Poduraaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Poduraaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaantha Oosi Gaantha Oosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaantha Oosi Gaantha Oosi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Pinnaala Uyir Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pinnaala Uyir Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya Veesi Kaiya Veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Veesi Kaiya Veesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pannuva Naarathar Kalagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannuva Naarathar Kalagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life Romba Sogusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Romba Sogusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Rumbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Rumbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simmam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Thunbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Thunbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Rumbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Rumbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Simmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simmam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Thunbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Thunbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanda Selavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanda Selavu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manda Kasaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manda Kasaayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mithuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mithuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raasi Mental Case su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasi Mental Case su"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thulaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raasi Paathi Loosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasi Paathi Loosu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathi Loosu Paathi Loosu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathi Loosu Paathi Loosu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avaloda Raasi Mesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaloda Raasi Mesam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Pinnaal Poyidaatha Aayidum Mosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pinnaal Poyidaatha Aayidum Mosam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aamaam Aamaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaam Aamaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aamaam Machi Aamaam Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaam Machi Aamaam Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivaloda Raasi Meenam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivaloda Raasi Meenam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iva Kooda Nee Pesa Pathu Vaai Venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva Kooda Nee Pesa Pathu Vaai Venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aamaam Aamaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaam Aamaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aamaam Machi Aamaam Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaam Machi Aamaam Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaga Raasi Ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaga Raasi Ponnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Gaanda Pesi Alaivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Gaanda Pesi Alaivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aamaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Risaba Raasi Kaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Risaba Raasi Kaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Reel Ah Vittu Thirivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Reel Ah Vittu Thirivaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aamaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viruchigam Sattunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viruchigam Sattunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murichukum Urava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murichukum Urava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veetukkum Naattukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetukkum Naattukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adangaatha Parava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangaatha Parava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanni Raasi Ponnuna Aamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni Raasi Ponnuna Aamam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yosi Pala Thadava Aamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yosi Pala Thadava Aamam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athu Two Piecil Suthumada Aamaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Two Piecil Suthumada Aamaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattaathu Pudava Aamaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattaathu Pudava Aamaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Poduraaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Poduraaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaantha Oosi Gaantha Oosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaantha Oosi Gaantha Oosi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Pinnaala Uyir Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pinnaala Uyir Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya Veesi Kaiya Veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Veesi Kaiya Veesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Poduraaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Poduraaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gaantha Oosi Gaantha Oosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaantha Oosi Gaantha Oosi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Pinnaala Uyir Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pinnaala Uyir Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya Veesi Kaiya Veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Veesi Kaiya Veesi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paavaada Paravainga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paavaada Paravainga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Raasi Enna Raasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Raasi Enna Raasi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathukko Ellaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukko Ellaame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romba Easy Romba Easy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba Easy Romba Easy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mookaanda Meesa Molachuruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mookaanda Meesa Molachuruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu Kazhutha Vayasu Aayiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu Kazhutha Vayasu Aayiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnunna Enna Purunchiruchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnunna Enna Purunchiruchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puttu Puttu Vaikkuren Kelu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttu Puttu Vaikkuren Kelu Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love u Thuru Puducha Thagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love u Thuru Puducha Thagaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpe Udaiyaatha Bimbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpe Udaiyaatha Bimbam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yosuchu Solra Alavukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yosuchu Solra Alavukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inaikku Unakku Onnum illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inaikku Unakku Onnum illama"/>
</div>
</pre>
