---
title: "aaya sutta vada song lyrics"
album: "Rajavin Paarvai Raniyin Pakkam"
artist: "Leander Lee Marty"
lyricist: "Vignesh K Jeyapal"
director: "Azhagu Raj"
path: "/albums/rajavin-paarvai-raniyin-pakkam-lyrics"
song: "Aaya Sutta Vada"
image: ../../images/albumart/rajavin-paarvai-raniyin-pakkam.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9NvzbaJ0dc8"
type: "mass"
singers:
  - Christopher Stanley
  - Ajesh
  - Gana Ulaganathan
  - Leander Lee Marty
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">anna nagarinile 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anna nagarinile "/>
</div>
<div class="lyrico-lyrics-wrapper">bannu coffeyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bannu coffeyile"/>
</div>
<div class="lyrico-lyrics-wrapper">nenaikura velaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenaikura velaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">nenachena katha onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenachena katha onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna katha anna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna katha anna"/>
</div>
<div class="lyrico-lyrics-wrapper">nalla irunthavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalla irunthavana"/>
</div>
<div class="lyrico-lyrics-wrapper">fulla kavutha katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fulla kavutha katha"/>
</div>
<div class="lyrico-lyrics-wrapper">joraa tirunja paiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="joraa tirunja paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">pejaru aana katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pejaru aana katha"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnungala thiyaa na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnungala thiyaa na"/>
</div>
<div class="lyrico-lyrics-wrapper">kathi paduna case aaguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathi paduna case aaguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kutramulla nenju 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutramulla nenju "/>
</div>
<div class="lyrico-lyrics-wrapper">kuru kurukuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuru kurukuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">meter onnum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meter onnum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu oru sappa kathal katha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu oru sappa kathal katha"/>
</div>
<div class="lyrico-lyrics-wrapper">aaya sutta vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaya sutta vada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koodave ava oor suthuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodave ava oor suthuva"/>
</div>
<div class="lyrico-lyrics-wrapper">kathula ava poo suthuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathula ava poo suthuva"/>
</div>
<div class="lyrico-lyrics-wrapper">atm ma use pannuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atm ma use pannuva"/>
</div>
<div class="lyrico-lyrics-wrapper">cute ah pesi close pannuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cute ah pesi close pannuva"/>
</div>
<div class="lyrico-lyrics-wrapper">pakamale thana vantha kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakamale thana vantha kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">patha vodane pichukithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patha vodane pichukithe"/>
</div>
<div class="lyrico-lyrics-wrapper">pathathume than vantha kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathathume than vantha kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">pakka pakka thane puttukichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakka pakka thane puttukichi"/>
</div>
<div class="lyrico-lyrics-wrapper">odu odu nikkatha odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu nikkatha odu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnu kaiyil sikkatha odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnu kaiyil sikkatha odu"/>
</div>
<div class="lyrico-lyrics-wrapper">kelu kelu nee unna paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelu kelu nee unna paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal avalukku kelikoothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal avalukku kelikoothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odu odu nikkatha odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu nikkatha odu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnu kaiyil sikkatha odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnu kaiyil sikkatha odu"/>
</div>
<div class="lyrico-lyrics-wrapper">kelu kelu nee unna paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelu kelu nee unna paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal avalukku kelikoothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal avalukku kelikoothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aaya suta vada kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaya suta vada kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">aaya suta vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaya suta vada"/>
</div>
<div class="lyrico-lyrics-wrapper">kakka kooda thookatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kakka kooda thookatha"/>
</div>
<div class="lyrico-lyrics-wrapper">palaya aaya suta vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palaya aaya suta vada"/>
</div>
<div class="lyrico-lyrics-wrapper">aaya suta vada kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaya suta vada kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">aaya suta vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaya suta vada"/>
</div>
<div class="lyrico-lyrics-wrapper">paazha pona love vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paazha pona love vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">tholacha vaazhkai ota vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholacha vaazhkai ota vada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannale pathale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannale pathale "/>
</div>
<div class="lyrico-lyrics-wrapper">thoolagum manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoolagum manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kallala thaakurale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallala thaakurale"/>
</div>
<div class="lyrico-lyrics-wrapper">appavi pola than naduchale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appavi pola than naduchale"/>
</div>
<div class="lyrico-lyrics-wrapper">pavi oppari vaikama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pavi oppari vaikama "/>
</div>
<div class="lyrico-lyrics-wrapper">vuudu katti aada porene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vuudu katti aada porene"/>
</div>
<div class="lyrico-lyrics-wrapper">adra ye aaya ye vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adra ye aaya ye vada"/>
</div>
<div class="lyrico-lyrics-wrapper">ye aaya ye vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye aaya ye vada"/>
</div>
<div class="lyrico-lyrics-wrapper">ye aaya ye vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye aaya ye vada"/>
</div>
<div class="lyrico-lyrics-wrapper">ye aaya ye vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye aaya ye vada"/>
</div>
<div class="lyrico-lyrics-wrapper">ye aaya ye vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye aaya ye vada"/>
</div>
<div class="lyrico-lyrics-wrapper">ye aaya ye vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye aaya ye vada"/>
</div>
<div class="lyrico-lyrics-wrapper">ye aaya ye vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye aaya ye vada"/>
</div>
<div class="lyrico-lyrics-wrapper">ye aaya sutta vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye aaya sutta vada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aaya suta vada kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaya suta vada kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">aaya suta vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaya suta vada"/>
</div>
<div class="lyrico-lyrics-wrapper">kakka kooda thookatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kakka kooda thookatha"/>
</div>
<div class="lyrico-lyrics-wrapper">palaya aaya suta vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="palaya aaya suta vada"/>
</div>
<div class="lyrico-lyrics-wrapper">aaya suta vada kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaya suta vada kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">aaya suta vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaya suta vada"/>
</div>
<div class="lyrico-lyrics-wrapper">paazha pona love vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paazha pona love vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">tholacha vaazhkai ota vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholacha vaazhkai ota vada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odu odu nikkatha odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu nikkatha odu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnu kaiyil sikkatha odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnu kaiyil sikkatha odu"/>
</div>
<div class="lyrico-lyrics-wrapper">kelu kelu nee unna paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelu kelu nee unna paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal avalukku kelikoothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal avalukku kelikoothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">odu odu nikkatha odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odu odu nikkatha odu"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnu kaiyil sikkatha odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnu kaiyil sikkatha odu"/>
</div>
<div class="lyrico-lyrics-wrapper">kelu kelu nee unna paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelu kelu nee unna paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kathal avalukku kelikoothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal avalukku kelikoothu"/>
</div>
</pre>
