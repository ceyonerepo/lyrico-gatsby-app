---
title: "vaa vaa kaama song lyrics"
album: "Tamizh Padam 2"
artist: "N. Kannan"
lyricist: "Thiyaru"
director: "CS Amudhan"
path: "/albums/tamizh-padam-2-lyrics"
song: "Vaa Vaa Kaama"
image: ../../images/albumart/tamizh-padam-2.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Um6KMhg83IA"
type: "love"
singers:
  - Ujjayinee Roy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thathaa tharum dhegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathaa tharum dhegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pariru nathuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pariru nathuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathaa tharum paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathaa tharum paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">Pararu thirura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pararu thirura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa vaa kaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa kaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu yaar than raama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu yaar than raama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondral paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondral paavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thindral theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thindral theerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodivittu ilamottu thudikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodivittu ilamottu thudikkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathana viral pattu vedikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathana viral pattu vedikkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Idaithottu pudhu mettu padikkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaithottu pudhu mettu padikkatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullaasanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullaasanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa vaa kaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa vaa kaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu yaaru than raama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu yaaru than raama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondral paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondral paavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thindral theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thindral theerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaaahaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaaahaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooru vagai sugam kaanalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru vagai sugam kaanalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Noolidayil porul thedalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noolidayil porul thedalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayamenna koodava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayamenna koodava"/>
</div>
<div class="lyrico-lyrics-wrapper">Veera soora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veera soora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadai enum baaram thevaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadai enum baaram thevaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaiyidu sevai paarayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaiyidu sevai paarayya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar ival poo magal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar ival poo magal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna roja"/>
</div>
<div class="lyrico-lyrics-wrapper">Urugida vidu ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urugida vidu ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu rasam eduho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu rasam eduho"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugidu neethan neeraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugidu neethan neeraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithira paarvai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithira paarvai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithiya bodhai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithiya bodhai naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innaal poona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaal poona"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru ponnaal vaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru ponnaal vaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamam theeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam theeya"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaaval neeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaaval neeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagal endrum iravendrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal endrum iravendrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaiyathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruva sugam kolla thadai yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruva sugam kolla thadai yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilam kandru balam kondu vilaiyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilam kandru balam kondu vilaiyadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai nadhi karai meeralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai nadhi karai meeralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aandavanum ithil seralaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aandavanum ithil seralaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai alli soodava maarbin melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai alli soodava maarbin melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanavarai neenthum dhegamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanavarai neenthum dhegamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayul varai kaanum mogamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayul varai kaanum mogamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodalil poovudal veenai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodalil poovudal veenai polae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thazhuvidu enai ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thazhuvidu enai ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Thathumpidum suvaiha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathumpidum suvaiha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nazhuvidum nooladai thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazhuvidum nooladai thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Malligai solai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malligai solai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manmatha leelai naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manmatha leelai naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara rara tha ta taa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara rara tha ta taa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara rara tha ta taa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara rara tha ta taa"/>
</div>
</pre>
