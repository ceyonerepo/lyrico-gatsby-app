---
title: "once upon a time song lyrics"
album: "Vikram"
artist: "Anirudh Ravichander"
lyricist: "Heisenberg"
director: "Lokesh Kanagaraj"
path: "/albums/vikram-lyrics"
song: "Once Upon A Time"
image: ../../images/albumart/vikram.jpg
date: 2022-06-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/3ZXQwz-sE-U"
type: "mass"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Once upon a time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Once upon a time"/>
</div>
<div class="lyrico-lyrics-wrapper">There lived a ghost
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="There lived a ghost"/>
</div>
<div class="lyrico-lyrics-wrapper">He is known to be a killer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He is known to be a killer"/>
</div>
<div class="lyrico-lyrics-wrapper">And fear the most
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And fear the most"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">The eagle is coming
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The eagle is coming"/>
</div>
<div class="lyrico-lyrics-wrapper">You better start running
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You better start running"/>
</div>
<div class="lyrico-lyrics-wrapper">His blood is rushing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="His blood is rushing"/>
</div>
<div class="lyrico-lyrics-wrapper">Stunnin and gunning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stunnin and gunning"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">The eagle is coming
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The eagle is coming"/>
</div>
<div class="lyrico-lyrics-wrapper">You better start running
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You better start running"/>
</div>
<div class="lyrico-lyrics-wrapper">His blood is rushing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="His blood is rushing"/>
</div>
<div class="lyrico-lyrics-wrapper">Stunnin and gunning
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stunnin and gunning"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Once upon a time
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Once upon a time"/>
</div>
<div class="lyrico-lyrics-wrapper">There lived a ghost
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="There lived a ghost"/>
</div>
<div class="lyrico-lyrics-wrapper">He is known to be a killer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He is known to be a killer"/>
</div>
<div class="lyrico-lyrics-wrapper">And fear the most
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And fear the most"/>
</div>
</pre>
