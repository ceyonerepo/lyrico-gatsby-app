---
title: "korameesam polisoda song lyrics"
album: "Krack"
artist: "S. Thaman"
lyricist: "Ramajogayya Sastry"
director: "Gopichand Malineni"
path: "/albums/krack-lyrics"
song: "Korameesam Polisoda - E Janamulo Neeku"
image: ../../images/albumart/krack.jpg
date: 2021-01-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/t-UXozzpGRg"
type: "love"
singers:
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">E Janamalo Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Janamalo Neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">E Mandhu Pettindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Mandhu Pettindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Janta Kattindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Janta Kattindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onti Meedha Khaakhee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onti Meedha Khaakhee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asalantu Thaanantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalantu Thaanantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Korake Puttindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Korake Puttindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thalupu Thattindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalupu Thattindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Kori Vethiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Kori Vethiki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Anda Chusindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Anda Chusindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nethekki Koorchundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nethekki Koorchundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannelli Pommandhi Savathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannelli Pommandhi Savathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ravantha Nee Pakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravantha Nee Pakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Chotivanantaandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chotivanantaandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Potlata Kosthaandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potlata Kosthaandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhandetthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhandetthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Sangathendho O Kaastha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Sangathendho O Kaastha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Thelchukora Penimiti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Thelchukora Penimiti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Korameesam Polisoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korameesam Polisoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Koncham Chussukora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Koncham Chussukora"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Meedhi Nakashathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Meedhi Nakashathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Neetho Undaneeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Neetho Undaneeraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">E Janamalo Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Janamalo Neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">E Mandhu Pettindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Mandhu Pettindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Janta Kattindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Janta Kattindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onti Meedha Kaakhee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onti Meedha Kaakhee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asalantu Thaanantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalantu Thaanantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Korake Puttindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Korake Puttindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thalupu Thattindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalupu Thattindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Kori Vethiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Kori Vethiki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panilo Padithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panilo Padithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekedhi Guruthu Raadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekedhi Guruthu Raadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvalaa Velithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvalaa Velithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakemo Voosupodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakemo Voosupodhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palavarintha Pulakarintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palavarintha Pulakarintha"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheroka Sagamuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheroka Sagamuga"/>
</div>
<div class="lyrico-lyrics-wrapper">Samayamantha Neeve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samayamantha Neeve"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakraminchinaavoora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakraminchinaavoora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">E Gullo E Ganta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Gullo E Ganta"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinipinchina Gaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinipinchina Gaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnega Ne Thalachukuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnega Ne Thalachukuntaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melloni Soothraani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melloni Soothraani"/>
</div>
<div class="lyrico-lyrics-wrapper">Muppoddhu Thadimesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muppoddhu Thadimesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kshemame Korukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kshemame Korukunta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na Lokamantha Santho Shamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Lokamantha Santho Shamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho Voonnadhanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho Voonnadhanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Korameesam Polisoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korameesam Polisoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Koncham Chussukora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Koncham Chussukora"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde Meedhi Nakashathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde Meedhi Nakashathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Neetho Undaneeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Neetho Undaneeraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">E Janamalo Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Janamalo Neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">E Mandhu Pettindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Mandhu Pettindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Janta Kattindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Janta Kattindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onti Meedha Kaakhee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onti Meedha Kaakhee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Asalantu Thaanantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalantu Thaanantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Korake Puttindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Korake Puttindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thalupu Thattindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalupu Thattindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeri Kori Vethiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeri Kori Vethiki"/>
</div>
</pre>
