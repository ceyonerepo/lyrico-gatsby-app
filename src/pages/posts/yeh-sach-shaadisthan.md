---
title: "Yeh Sach song lyrics"
album: "Shaadisthan"
artist: "Nakul Sharma - Sahil Bhatia"
lyricist: "Sahil Bhatia - Nakul Sharma - Apurv Dogra"
director: "Raj Singh Chaudhary"
path: "/albums/shaadisthan-lyrics"
song: "Yeh Sach"
image: ../../images/albumart/shaadisthan.jpg
date: 2021-06-11
lang: hindi
youtubeLink: "https://www.youtube.com/embed/2CFaobfcNDE"
type: "happy"
singers:
  - Mansheel Gujral
  - Ajay Jayanthi
  - Apurv Dogra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Baatein Teri Sun Ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baatein Teri Sun Ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyu Hil Gaya Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyu Hil Gaya Re"/>
</div>
<div class="lyrico-lyrics-wrapper">Thede To Ho Ja Aakhir Paas
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thede To Ho Ja Aakhir Paas"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah Sach Tu Kar Naa Paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Sach Tu Kar Naa Paaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baatein Teri Jooothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baatein Teri Jooothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Waadein Kar Jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waadein Kar Jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Zinda Hai Tu Kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zinda Hai Tu Kya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankein Khol Ke Sach
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankein Khol Ke Sach"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu So Na Paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu So Na Paye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Woh Kismat Ke Khel Mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh Kismat Ke Khel Mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakab Hai ,Jo Kabil Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakab Hai ,Jo Kabil Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Is Soch Se Tu Aage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is Soch Se Tu Aage"/>
</div>
<div class="lyrico-lyrics-wrapper">Irade Hai Shamil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irade Hai Shamil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sach Ki Sooraj Se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sach Ki Sooraj Se"/>
</div>
<div class="lyrico-lyrics-wrapper">Ab Aa Tu Mil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab Aa Tu Mil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raatein Ye Kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raatein Ye Kaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Khaali Yeh Sanaate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khaali Yeh Sanaate"/>
</div>
<div class="lyrico-lyrics-wrapper">Khamoshi Ka Shhor Aakhir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khamoshi Ka Shhor Aakhir"/>
</div>
<div class="lyrico-lyrics-wrapper">Tuhjko Nigal Na Jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tuhjko Nigal Na Jaaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kya The Stud Tero Kahani Ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya The Stud Tero Kahani Ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naya Rang Teri Jubaani Ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naya Rang Teri Jubaani Ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Zinda Hai To Uth
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zinda Hai To Uth"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Time Hain Sach Batane Ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Time Hain Sach Batane Ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakh Chupaye Sach Nahi Chupta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakh Chupaye Sach Nahi Chupta"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi Ilaaj Nahi Is Andhepan Ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi Ilaaj Nahi Is Andhepan Ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Time Ki Baat Hai Kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Time Ki Baat Hai Kya"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Tere Sath Hai Beta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Tere Sath Hai Beta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jooth Ke Pair Nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jooth Ke Pair Nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh To Bhadkti Aag Hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh To Bhadkti Aag Hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Jisme Jal Rah Aaaj Jai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jisme Jal Rah Aaaj Jai"/>
</div>
<div class="lyrico-lyrics-wrapper">Haan Aaj Hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan Aaj Hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naag Hai Dass Jayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naag Hai Dass Jayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Is Dal Dal Me Tu Fass Jayega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is Dal Dal Me Tu Fass Jayega"/>
</div>
<div class="lyrico-lyrics-wrapper">Maang Le Jitni Bhi Duae Tu Chahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maang Le Jitni Bhi Duae Tu Chahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Par Tera Bhagwan Tuhjko Sun Na Payega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Par Tera Bhagwan Tuhjko Sun Na Payega"/>
</div>
<div class="lyrico-lyrics-wrapper">Tera Hak Tera Sath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera Hak Tera Sath"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Kar Tu Dooji Baat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Kar Tu Dooji Baat"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu Hai Sacha Ujaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu Hai Sacha Ujaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mita De Joothi Raat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mita De Joothi Raat"/>
</div>
<div class="lyrico-lyrics-wrapper">Shamasaha Dega Khak
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shamasaha Dega Khak"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu Zinda Hai Ya Raakh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu Zinda Hai Ya Raakh"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaise To Yeah Sach Hi Hai Tera Aaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaise To Yeah Sach Hi Hai Tera Aaj"/>
</div>
<div class="lyrico-lyrics-wrapper">Sach Hi Tera Aaj
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sach Hi Tera Aaj"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baatein Teri Sun Ke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baatein Teri Sun Ke"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyu Hil Gaya Re
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyu Hil Gaya Re"/>
</div>
<div class="lyrico-lyrics-wrapper">Thede To Ho Ja Aakhir Paas
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thede To Ho Ja Aakhir Paas"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah Sach Tu Kar Naa Paaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Sach Tu Kar Naa Paaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baatein Teri Jooothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baatein Teri Jooothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Waadein Kar Jaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waadein Kar Jaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Zinda Hai Tu Kya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zinda Hai Tu Kya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankein Khol Ke Sach
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankein Khol Ke Sach"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu So Na Paye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu So Na Paye"/>
</div>
</pre>
