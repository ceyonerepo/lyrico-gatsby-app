---
title: "ye ooru ye oore song lyrics"
album: "Palasa 1978"
artist: "Raghu Kunche"
lyricist: "Bhaskarabhatla Ravi Kumar"
director: "Karuna Kumar"
path: "/albums/palasa-1978-lyrics"
song: "Ye Ooru Ye Oore"
image: ../../images/albumart/palasa-1978.jpg
date: 2020-03-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/asr0Wx9sqRc"
type: "happy"
singers:
  - Vaikom Vijayalakshmi
  - Raju Jamuku Asirayya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye Ooru Ye Oore Vale Bhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Ooru Ye Oore Vale Bhaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Needi Ye Oore Vale Bhaama Needi Ye Oore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needi Ye Oore Vale Bhaama Needi Ye Oore"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seekakulam Jila Jillaloo Palasa Maa Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seekakulam Jila Jillaloo Palasa Maa Ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthaava Palasa Maa Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthaava Palasa Maa Ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhogattaletetey Mee Oori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhogattaletetey Mee Oori"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhogattalinketey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhogattalinketey"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Dukku Chustune Chepthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Dukku Chustune Chepthava"/>
</div>
<div class="lyrico-lyrics-wrapper">Okasari Vintane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okasari Vintane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Ooru Ye Oore Vale Bhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Ooru Ye Oore Vale Bhaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Needi Ye Oore Vale Bhaama Needi Ye Oore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needi Ye Oore Vale Bhaama Needi Ye Oore"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palasa Maa Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palasa Maa Ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasthaava Palasa Maa Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasthaava Palasa Maa Ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bugatha Inu Inu Sebuthaa Inu Inu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bugatha Inu Inu Sebuthaa Inu Inu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Oori Vaibhogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Oori Vaibhogam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okathaa Inu Inu Seputhaa Inu Inu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okathaa Inu Inu Seputhaa Inu Inu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Oori Vaibhogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Oori Vaibhogam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jamukalakunda Vayinchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jamukalakunda Vayinchana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekuru Kondani Chupinchana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekuru Kondani Chupinchana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yetiki Avathala Yepugu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetiki Avathala Yepugu "/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhigina Pachhaani Saubhagyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhigina Pachhaani Saubhagyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeedi Thotala Singaraalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeedi Thotala Singaraalu"/>
</div>
<div class="lyrico-lyrics-wrapper">AA Yerra Cheruvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AA Yerra Cheruvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegire Kongala Kolaataalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegire Kongala Kolaataalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeedipappu Peru Chepithe Chaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeedipappu Peru Chepithe Chaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Palasa Vilasam Guruthotadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palasa Vilasam Guruthotadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Deshalanni Thella Bangaramla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deshalanni Thella Bangaramla"/>
</div>
<div class="lyrico-lyrics-wrapper">Bavinche Viluvaina Pantey Idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bavinche Viluvaina Pantey Idhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenthati Vantaina Idhunte OO Andham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthati Vantaina Idhunte OO Andham"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenthati Vadaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenthati Vadaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeni Ruchike Dhaasoham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeni Ruchike Dhaasoham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekkalu Mukkalu Chemata Chukkalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalu Mukkalu Chemata Chukkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Makeenammaki Naivedhyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makeenammaki Naivedhyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Basthalethe Vasthadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basthalethe Vasthadhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">AA Kondalu Yeragavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AA Kondalu Yeragavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andharilanti Musthabulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharilanti Musthabulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Basthalethe Vasthadhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Basthalethe Vasthadhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">AA Kondalu Yeragavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AA Kondalu Yeragavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andharilanti Musthabulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharilanti Musthabulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Portala Puruti Gadde Idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Portala Puruti Gadde Idhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaitanya Geethala Gonthe Idhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaitanya Geethala Gonthe Idhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhlochina Chaduvula Thalle Idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhlochina Chaduvula Thalle Idhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Janane Thalu Udhayinche Ille Idhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janane Thalu Udhayinche Ille Idhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valasa Pittalaki Thele Neelapooramu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valasa Pittalaki Thele Neelapooramu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadayathralaki Modalu Ichapuramu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadayathralaki Modalu Ichapuramu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachani Cheerani Katina Nelaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachani Cheerani Katina Nelaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nudhutuna Thorupu Sinduram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nudhutuna Thorupu Sinduram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam manam Bharampuram Anukunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam manam Bharampuram Anukunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Varasalu Kalupukukupodha Tharam Tharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varasalu Kalupukukupodha Tharam Tharam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Davudu Ichina Varam Varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Davudu Ichina Varam Varam"/>
</div>
<div class="lyrico-lyrics-wrapper">AA Netthuru Badhulu Preme 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AA Netthuru Badhulu Preme "/>
</div>
<div class="lyrico-lyrics-wrapper">Pongenu Naram Naram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongenu Naram Naram"/>
</div>
</pre>
