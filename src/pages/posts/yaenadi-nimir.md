---
title: "yaenadi song lyrics"
album: "Nimir"
artist: "Darbuka Siva - B. Ajaneesh Loknath"
lyricist: "Mohan Rajan"
director: "Priyadarshan"
path: "/albums/nimir-lyrics"
song: "Yaenadi"
image: ../../images/albumart/nimir.jpg
date: 2018-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_yleKqJPFFU"
type: "love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaenadi yaenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaenadi yaenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thottu yen indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thottu yen indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal adiththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal adiththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyadi neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyadi neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaividam sollaamal neeyaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaividam sollaamal neeyaagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavilum kaanaadha pennaa ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilum kaanaadha pennaa ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo ayyo manam thindaadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo ayyo manam thindaadudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal murai pennaaga naan kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal murai pennaaga naan kaangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Idho idho manam kondaadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho idho manam kondaadudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaenadi yaenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaenadi yaenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thottu yen indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thottu yen indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal adiththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal adiththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyadi neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyadi neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaividam sollaamal neeyaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaividam sollaamal neeyaagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyil pesi povaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyil pesi povaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamal naan paarkkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamal naan paarkkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaamal naan paarkkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaamal naan paarkkaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyalai veesi pogindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalai veesi pogindraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralodu kaadhal serththaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralodu kaadhal serththaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaradiyo needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradiyo needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkullae unnaiththaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkullae unnaiththaan "/>
</div>
<div class="lyrico-lyrics-wrapper">naan thedinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan thedinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaradiyo nee yaaradiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaradiyo nee yaaradiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkullae naan indru poraadinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkullae naan indru poraadinen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedum kangal thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum kangal thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu unnai mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu unnai mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadum nenjam paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadum nenjam paadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un perai mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un perai mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhum pennae podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum pennae podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nenjai kondraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nenjai kondraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum kannae podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum kannae podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ennai vendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ennai vendraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaenadi yaenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaenadi yaenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thottu yen indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thottu yen indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal adiththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal adiththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyadi neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyadi neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaividam sollaamal neeyaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaividam sollaamal neeyaagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malarai polae melithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarai polae melithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjodu nee thondrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjodu nee thondrinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavai polae aanaayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavai polae aanaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam aanen yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam aanen yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhaliyo needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhaliyo needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamedhu nizhaledhu solvaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamedhu nizhaledhu solvaayadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Devadhaiyo en devadhaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devadhaiyo en devadhaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini mudhal enai endru kaanbeno naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini mudhal enai endru kaanbeno naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedum kangal thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedum kangal thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu unnai mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu unnai mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadum nenjam paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadum nenjam paadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un perai mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un perai mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhum pennae podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum pennae podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nenjai kondraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nenjai kondraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum kannae podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum kannae podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ennai vendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ennai vendraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaenadi yaenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaenadi yaenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thottu yen indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thottu yen indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal adiththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal adiththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyadi neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyadi neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaividam sollaamal neeyaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaividam sollaamal neeyaagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavilum kaanaadha pennaa ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavilum kaanaadha pennaa ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo ayyo manam thindaadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo ayyo manam thindaadudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal murai pennaaga naan kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal murai pennaaga naan kaangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Idho idho manam kondaadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idho idho manam kondaadudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaenadi yaenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaenadi yaenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thottu yen indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thottu yen indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal adiththaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal adiththaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyadi neeyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyadi neeyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaividam sollaamal neeyaagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaividam sollaamal neeyaagiren"/>
</div>
</pre>
