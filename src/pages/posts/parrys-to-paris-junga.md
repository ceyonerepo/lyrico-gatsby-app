---
title: "parrys to paris song lyrics"
album: "Junga"
artist: "Siddharth Vipin"
lyricist: "Lalithanand"
director: "Gokul"
path: "/albums/junga-lyrics"
song: "Parrys To Paris"
image: ../../images/albumart/junga.jpg
date: 2018-07-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tUpp-uTGSAE"
type: "happy"
singers:
  - Anthony Daasan
  - Kalpana Raghavendar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannamma control-u pannamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma control-u pannamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma kai meeri polaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma kai meeri polaamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma thedi vaaranmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma thedi vaaranmaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannamma thookka poraanmaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamma thookka poraanmaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Budget flight-u yerittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budget flight-u yerittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu flight-u maarittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu flight-u maarittaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattuchoru kattittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattuchoru kattittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattu paatta meerittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattu paatta meerittaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelikkai raajiyathin ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelikkai raajiyathin ahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadikkai raani ivalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadikkai raani ivalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Korikkai vinnapam podaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korikkai vinnapam podaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival kannai kaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival kannai kaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooral thoorum aagayamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooral thoorum aagayamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saadhikka thaan ivan porandhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadhikka thaan ivan porandhanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Success-ukkae oru bayam varudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Success-ukkae oru bayam varudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Call panni call taxi yeraamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Call panni call taxi yeraamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan kaalae taxi nadadaa enbaanae aeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan kaalae taxi nadadaa enbaanae aeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Sombu thanni eduthitaaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sombu thanni eduthitaaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Micham panni kulichittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Micham panni kulichittaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parrys to parisVerai leval-u poyittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to parisVerai leval-u poyittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellaikkaran asandhuttaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaikkaran asandhuttaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Don-ukku don-u ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don-ukku don-u ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey ivanukku eedu evan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ivanukku eedu evan"/>
</div>
<div class="lyrico-lyrics-wrapper">Airoppa kaanadha hero ivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Airoppa kaanadha hero ivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan lesa paathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan lesa paathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasaa kottum maasaanavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasaa kottum maasaanavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thevaigalae ival ulagamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaigalae ival ulagamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thedalgalae ival varai padamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalgalae ival varai padamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodaana kodigal vaai pesumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodaana kodigal vaai pesumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ival kaasum pesa ketka vendumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival kaasum pesa ketka vendumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-il karuthaa irundhuttaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-il karuthaa irundhuttaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Eiffel tower-ah osanthuttaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eiffel tower-ah osanthuttaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Bun-eh tea-la nenachittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bun-eh tea-la nenachittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Dinner adhula mudichittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dinner adhula mudichittaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Coat-u suit-u maatittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coat-u suit-u maatittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Get-up ellaam maatittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get-up ellaam maatittaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Target fix-u pannittaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Target fix-u pannittaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Parrys to paris
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parrys to paris"/>
</div>
<div class="lyrico-lyrics-wrapper">Master plan-eh poottuttaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Master plan-eh poottuttaan"/>
</div>
</pre>
