---
title: "kaalangal karaigirathe song lyrics"
album: "Calls"
artist: "Thameem Ansari"
lyricist: "K.Bhuvana - Jegadeesan - Dr. Nandhudasan"
director: "J. Sabarish"
path: "/albums/calls-lyrics"
song: "Kaalangal Karaigirathe"
image: ../../images/albumart/calls.jpg
date: 2021-02-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SVMCDRpzzIE"
type: "Affection"
singers:
  - Ajaey shravan
  - Padmaja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kaalangal karaigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalangal karaigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">pala kaayangal tharugirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala kaayangal tharugirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">neengaamal ennulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neengaamal ennulle"/>
</div>
<div class="lyrico-lyrics-wrapper">sila ennangal varugirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila ennangal varugirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">thayodume nan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayodume nan than"/>
</div>
<div class="lyrico-lyrics-wrapper">ada irunthalume yen nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada irunthalume yen nan"/>
</div>
<div class="lyrico-lyrics-wrapper">neduntholaivaga irukindane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neduntholaivaga irukindane"/>
</div>
<div class="lyrico-lyrics-wrapper">piriyamale nan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piriyamale nan than"/>
</div>
<div class="lyrico-lyrics-wrapper">pesamale yen nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesamale yen nan"/>
</div>
<div class="lyrico-lyrics-wrapper">ada po engiren nan than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada po engiren nan than"/>
</div>
<div class="lyrico-lyrics-wrapper">en solladalai kondadiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en solladalai kondadiye"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir valgiral amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir valgiral amma"/>
</div>
<div class="lyrico-lyrics-wrapper">idhai mulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhai mulu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kandaal malai thano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kandaal malai thano"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir puru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir puru"/>
</div>
<div class="lyrico-lyrics-wrapper">oru sollal pani thano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sollal pani thano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalangal karaigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalangal karaigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">pala kaayangal tharugirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala kaayangal tharugirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">neengaamal ennulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neengaamal ennulle"/>
</div>
<div class="lyrico-lyrics-wrapper">sila ennangal varugirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila ennangal varugirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">paaramale neeyum senrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaramale neeyum senrai"/>
</div>
<div class="lyrico-lyrics-wrapper">pagalondru en valvil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagalondru en valvil "/>
</div>
<div class="lyrico-lyrics-wrapper">nijam illaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijam illaye"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyin ulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyin ulle"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">vali kaangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali kaangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum nan"/>
</div>
<div class="lyrico-lyrics-wrapper">un marbodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un marbodu"/>
</div>
<div class="lyrico-lyrics-wrapper">poo manjam ondrai thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo manjam ondrai thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalatil mellisai korthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalatil mellisai korthu"/>
</div>
<div class="lyrico-lyrics-wrapper">thalai kodha neeyum vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalai kodha neeyum vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">saayamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saayamale"/>
</div>
<div class="lyrico-lyrics-wrapper">un tholgalil eri naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un tholgalil eri naam"/>
</div>
<div class="lyrico-lyrics-wrapper">oor sutrum kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor sutrum kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">pogaamale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogaamale "/>
</div>
<div class="lyrico-lyrics-wrapper">irunthale pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irunthale pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">theeramale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeramale "/>
</div>
<div class="lyrico-lyrics-wrapper">intha maayapidi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha maayapidi"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai mayakuthu melum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai mayakuthu melum"/>
</div>
<div class="lyrico-lyrics-wrapper">ooyamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooyamale"/>
</div>
<div class="lyrico-lyrics-wrapper">poyaana yaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poyaana yaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam muluthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam muluthum"/>
</div>
<div class="lyrico-lyrics-wrapper">eerathil nanaigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eerathil nanaigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">aethum solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aethum solla"/>
</div>
<div class="lyrico-lyrics-wrapper">orathil munaigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orathil munaigirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalangal karaigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalangal karaigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">pala kaayangal tharugirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala kaayangal tharugirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">neengaamal ennulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neengaamal ennulle"/>
</div>
<div class="lyrico-lyrics-wrapper">sila ennangal varugirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sila ennangal varugirathe"/>
</div>
</pre>
