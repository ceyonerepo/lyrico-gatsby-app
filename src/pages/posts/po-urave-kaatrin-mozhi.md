---
title: "po urave song lyrics"
album: "Kaatrin Mozhi"
artist: "AH Kaashif"
lyricist: "Madhan Karky"
director: "Radha Mohan"
path: "/albums/kaatrin-mozhi-lyrics"
song: "Po Urave"
image: ../../images/albumart/kaatrin-mozhi.jpg
date: 2018-11-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JdVPh0vu2zI"
type: "sad"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaa aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee un vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee un vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkena oor nilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena oor nilavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee un paadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee un paadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkendrae un poongaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkendrae un poongaatru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan en koothal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan en koothal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanaiyatha mounangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanaiyatha mounangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan nam koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan nam koodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimai neekkum padalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai neekkum padalgal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un punnagayin pinnaniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un punnagayin pinnaniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Silaril sogam eppothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silaril sogam eppothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar endrae nee ariyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar endrae nee ariyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayangalil mazhaiyanaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayangalil mazhaiyanaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan endrae kandum yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan endrae kandum yen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhiyamaal neengi poonaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhiyamaal neengi poonaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Po uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee unthan kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee unthan kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thurathiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurathiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Po uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraganinthu nee unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraganinthu nee unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangalai uthariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangalai uthariyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Po uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee unthan kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee unthan kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thurathiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurathiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Po uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraganinthu nee unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraganinthu nee unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangalai uthariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangalai uthariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattrangal athaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattrangal athaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorangal ithaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorangal ithaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">En siru idhayam pazhaguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En siru idhayam pazhaguthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee attra iravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee attra iravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettukul thuravu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettukul thuravu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen intha uravu vilaguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen intha uravu vilaguthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu nilai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu nilai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum malai andro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum malai andro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu malai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu malai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru mazhai yendro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru mazhai yendro"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha nodigal kanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha nodigal kanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enavae uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enavae uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Saththamittu sollivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththamittu sollivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththamittu thallivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththamittu thallivittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Po uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee unthan kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee unthan kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thurathiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurathiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Po uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraganinthu nee unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraganinthu nee unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganangalai uthariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganangalai uthariyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Po uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enai maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee unthan kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee unthan kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thurathiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurathiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Po uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po uravae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraganinthu nee unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraganinthu nee unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganangalai uthariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganangalai uthariyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Po uravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po uravae"/>
</div>
</pre>
