---
title: "athan varuvaga song lyrics"
album: "Dum Dum Dum"
artist: "Karthik Raja"
lyricist: "Na. Muthukumar - Vaali - Pa. Vijay"
director: "Azhagam Perumal"
path: "/albums/dum-dum-dum-song-lyrics"
song: "Athan Varuvaga Oru"
image: ../../images/albumart/dum-dum-dum.jpg
date: 2001-04-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gSRT-rWqIc8"
type: "happy"
singers:
  - Tippu
  - Malgudi Subha
  - Harini
  - Chitra Sivaraman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ga Ga Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ri Ga Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ri Ga Pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ri Ga Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ri Ga Pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Pa Dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Pa Dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Pa Dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Pa Dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sa Ga Pa Ma Ga Pa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa Ga Pa Ma Ga Pa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Ma Ga Ri Sa Ri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ma Ga Ri Sa Ri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ga Ga Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Ga Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaa Ga Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaa Ga Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ri Ga Pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ri Ga Pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ru Tu Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ru Tu Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga Pa Dha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga Pa Dha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pe Pe Pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pe Pe Pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa Dha Saaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Dha Saaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa Dha Saa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa Dha Saa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athaan Varuvaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaan Varuvaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Muththam Tharuvaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Muththam Tharuvaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">En Achcham Vetkam Koocham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Achcham Vetkam Koocham"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Alli Rusipaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Alli Rusipaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athaan Varuvaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaan Varuvaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Muththam Tharuvaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Muththam Tharuvaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">En Achcham Vetkam Koocham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Achcham Vetkam Koocham"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Alli Rusipaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Alli Rusipaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhavai Saathinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhavai Saathinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Jannal Thirapaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannal Thirapaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Jannala Saatha Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannala Saatha Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasilaiyee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasilaiyee"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Kaana Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Kaana Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Kanngala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Kanngala"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhraman Senjadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhraman Senjadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seriyilaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seriyilaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Amaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palum Pudhu Thenum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palum Pudhu Thenum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paagum Kasapagha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paagum Kasapagha"/>
</div>
<div class="lyrico-lyrics-wrapper">Avugha Thaan Ennakku Inipaagha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avugha Thaan Ennakku Inipaagha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athaan Varuvaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaan Varuvaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Muththam Tharuvaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Muththam Tharuvaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">En Achcham Vetkam Koocham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Achcham Vetkam Koocham"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Alli Rusipaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Alli Rusipaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athaan Varuvaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaan Varuvaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Muththam Tharuvaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Muththam Tharuvaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">En Achcham Vetkam Koocham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Achcham Vetkam Koocham"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Alli Rusipaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Alli Rusipaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sabash Konnuta Di Amma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sabash Konnuta Di Amma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avugha Vanthu Ninale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avugha Vanthu Ninale"/>
</div>
<div class="lyrico-lyrics-wrapper">Seriyaa Kaathu Ketkaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seriyaa Kaathu Ketkaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhusa Paarvai Teriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhusa Paarvai Teriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Olungha Pesa Mudiyathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olungha Pesa Mudiyathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaga Motham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaga Motham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Endral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Endral"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthugala Kutham Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthugala Kutham Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthugala Kuthathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthugala Kuthathila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolambuthu Sitham Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolambuthu Sitham Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Azhagan Enakaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Azhagan Enakaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennakku Munne Irrupane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennakku Munne Irrupane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avugaa Enna Sonnanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avugaa Enna Sonnanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha Naan Solla Matengaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha Naan Solla Matengaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Avugha Enna Thanthanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avugha Enna Thanthanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa Poththi Vechengaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaa Poththi Vechengaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sithan Kuda Kaadhalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithan Kuda Kaadhalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Buththi Maaruvane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buththi Maaruvane"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala Mara Utchiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala Mara Utchiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonjal Aadu Vaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal Aadu Vaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Siripiigha Aluviiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siripiigha Aluviiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiruukaaga Thirivigha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiruukaaga Thirivigha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Theri Athaan Varuvaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Theri Athaan Varuvaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Muththam Tharuvaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Muththam Tharuvaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Achcham Vetkam Koocham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Achcham Vetkam Koocham"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Alli Rusipaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Alli Rusipaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Theri Athaan Varuvaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Theri Athaan Varuvaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Muththam Tharuvaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Muththam Tharuvaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Achcham Vetkam Koocham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Achcham Vetkam Koocham"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Alli Rusipaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Alli Rusipaane"/>
</div>
</pre>
