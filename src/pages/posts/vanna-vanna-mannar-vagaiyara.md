---
title: "vanna vanna song lyrics"
album: "Mannar Vagaiyara"
artist: "Jakes Bejoy"
lyricist: "Mani Amudhavan"
director: "G. Boopathy Pandian"
path: "/albums/mannar-vagaiyara-lyrics"
song: "Vanna Vanna"
image: ../../images/albumart/mannar-vagaiyara.jpg
date: 2018-01-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KPRZE0T5Dxg"
type: "happy"
singers:
  - Keshav Vinod
  - Deepika
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vanna vanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna vanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Valayal adi chinna kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valayal adi chinna kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinungum padi mattungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinungum padi mattungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga thanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga thanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaiyal adi thangi kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyal adi thangi kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Minungum padi pottungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minungum padi pottungadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla nalla sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla nalla sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee solli kodu pullai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee solli kodu pullai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullirukkum pullai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullirukkum pullai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu kettu nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu kettu nadakkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bandham pogum pathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandham pogum pathai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada boomi panthu mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada boomi panthu mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum kunjom naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum kunjom naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu vechi sirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu vechi sirikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraaroo paattu sattham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaroo paattu sattham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekka pogathu veedellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekka pogathu veedellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathi un magana pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathi un magana pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa pogathu oorellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa pogathu oorellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekka pogathu uravellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekka pogathu uravellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanna vanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanna vanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Valayal adi chinna kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valayal adi chinna kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinungum padi mattungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinungum padi mattungadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga thanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga thanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaiyal adi thangi kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyal adi thangi kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Minungum padi pottungadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minungum padi pottungadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thathi thathi thaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thathi thathi thaavi"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjil vanthu modhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjil vanthu modhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Alli sendru kaiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alli sendru kaiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan ennai anaipan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan ennai anaipan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vittu pona sondham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu pona sondham"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu ellam onna serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu ellam onna serum"/>
</div>
<div class="lyrico-lyrics-wrapper">Singakutty vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singakutty vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada namma inaippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada namma inaippan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraaroo paattu sattham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraaroo paattu sattham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekka pogathu veedellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekka pogathu veedellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathi un magana pathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathi un magana pathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa pogathu oorellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa pogathu oorellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekka pogathu uravellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekka pogathu uravellaam"/>
</div>
</pre>
