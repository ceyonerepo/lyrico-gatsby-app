---
title: "sarpetta - kadama unna song lyrics"
album: "Simba"
artist: "Vishal Chandrasekhar"
lyricist: "Charukesh Sekar"
director: "Arvind Sridhar"
path: "/albums/simba-lyrics"
song: "Sarpetta - Kadama Unna"
image: ../../images/albumart/simba.jpg
date: 2019-01-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RiMgIoKIMWk"
type: "happy"
singers:
  - Anthony Daasan
  - Arunraja Kamaraj
  - Vishal Chandrasekhar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ladies And Gentleman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ladies And Gentleman"/>
</div>
<div class="lyrico-lyrics-wrapper">Welcome To Simbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome To Simbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Your Trip Is Going To Begin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Your Trip Is Going To Begin"/>
</div>
<div class="lyrico-lyrics-wrapper">Simbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Simbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">So Please Hold On To Your Seat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Please Hold On To Your Seat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadama Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadama Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaimaa Panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaimaa Panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhka Sema Funnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhka Sema Funnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gun-a Ninnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gun-a Ninnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhomsam Pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhomsam Pannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaikum Nee Winnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaikum Nee Winnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poda Life Oru Lottery
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda Life Oru Lottery"/>
</div>
<div class="lyrico-lyrics-wrapper">Jai Po Maa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jai Po Maa Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadaa Kalvaikkum Idam Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadaa Kalvaikkum Idam Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Nada Thaan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Nada Thaan Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Da Da Dada Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Da Da Dada Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga Sarpettaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Sarpettaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda Kollu Betta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Kollu Betta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Enga Thottaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Enga Thottaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikum Therikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikum Therikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Vethu Vetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vethu Vetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Dynamite Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Dynamite Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthum Banna Kite Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthum Banna Kite Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Poga Puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Poga Puriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Sarpettaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Sarpettaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda Kollu Betta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Kollu Betta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Enga Thottaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Enga Thottaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikum Therikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikum Therikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Vethu Vetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vethu Vetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Dynamite Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Dynamite Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthum Banna Kite Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthum Banna Kite Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Poga Puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Poga Puriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhuula Sukkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuula Sukkuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Irangiputaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irangiputaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Kodi Parakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Kodi Parakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sara Vedi Vedikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara Vedi Vedikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudave Sanni Onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudave Sanni Onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thothikichu Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thothikichu Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Jatti Varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Jatti Varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Lathi Kizhikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Lathi Kizhikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rummy La Dick Adika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rummy La Dick Adika"/>
</div>
<div class="lyrico-lyrics-wrapper">Vennum Munna Munna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennum Munna Munna"/>
</div>
<div class="lyrico-lyrics-wrapper">Jọkera Kooda Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jọkera Kooda Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Kanna Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Kanna Kanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">NTR Balaiya La
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="NTR Balaiya La"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonnadhu Enna Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnadhu Enna Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Trainku Sundu Viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trainku Sundu Viral"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothum Kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum Kanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Sarpettaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Sarpettaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda Kollu Betta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Kollu Betta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Enga Thottaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Enga Thottaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikum Therikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikum Therikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Vethu Vetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vethu Vetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Dynamite-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Dynamite-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthum Banna Kite Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthum Banna Kite Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Poga Puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Poga Puriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mallaka Padutha Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallaka Padutha Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadu Parakkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadu Parakkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothaatha Murattukaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothaatha Murattukaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalu Karakkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalu Karakkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beedanu Sonna Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beedanu Sonna Mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayyu Sevakkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayyu Sevakkuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooda-nu Vittu Puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooda-nu Vittu Puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dabbu Thaan Kedaikuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dabbu Thaan Kedaikuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhayatha Urutti Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhayatha Urutti Poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Rendu Aaru Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Rendu Aaru Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthutta Neeyum Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthutta Neeyum Thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Power Star Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Power Star Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rajasir Paata Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajasir Paata Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavellam Koothu Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavellam Koothu Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaikum Namma Life Fu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaikum Namma Life Fu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondattam Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondattam Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Sarpettaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Sarpettaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oda Kollu Betta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oda Kollu Betta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Enga Thottaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Enga Thottaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Therikum Therikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therikum Therikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga Vethu Vetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga Vethu Vetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa Dynamite Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa Dynamite Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthum Banna Kite Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthum Banna Kite Ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Poga Puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Poga Puriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarpettaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarpettaah"/>
</div>
<div class="lyrico-lyrics-wrapper">Bettaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bettaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thottaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarpettahh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarpettahh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Despite All The Distrubances
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Despite All The Distrubances"/>
</div>
<div class="lyrico-lyrics-wrapper">U Have Reached The Destination Safely
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Have Reached The Destination Safely"/>
</div>
<div class="lyrico-lyrics-wrapper">Thank You For Travelling With Simbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thank You For Travelling With Simbaa"/>
</div>
</pre>
