---
title: "semmaan magalai song lyrics"
album: "Vaazhl"
artist: "Pradeep Kumar"
lyricist: "Arunagirinathar"
director: "Arun Prabu Purushothaman"
path: "/albums/vaazhl-lyrics"
song: "Semmaan Magalai"
image: ../../images/albumart/vaazhl.jpg
date: 2021-07-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/krzRJ_HP8n8"
type: "happy"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">semmaan magalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semmaan magalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudum thirudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudum thirudan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">semmaan magalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semmaan magalai"/>
</div>
<div class="lyrico-lyrics-wrapper">thirudum thirudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudum thirudan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bemmaan murugan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bemmaan murugan"/>
</div>
<div class="lyrico-lyrics-wrapper">piravaan iravaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piravaan iravaan"/>
</div>
<div class="lyrico-lyrics-wrapper">bemmaan murugan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bemmaan murugan"/>
</div>
<div class="lyrico-lyrics-wrapper">piravaan iravaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piravaan iravaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">summa iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa iru"/>
</div>
<div class="lyrico-lyrics-wrapper">summa iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa iru"/>
</div>
<div class="lyrico-lyrics-wrapper">summa iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa iru"/>
</div>
<div class="lyrico-lyrics-wrapper">sol ara endralume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sol ara endralume"/>
</div>
<div class="lyrico-lyrics-wrapper">amma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amma "/>
</div>
<div class="lyrico-lyrics-wrapper">porul ondrum arinthilane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porul ondrum arinthilane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mei maranthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mei maranthen"/>
</div>
<div class="lyrico-lyrics-wrapper">mei unarnthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mei unarnthen"/>
</div>
</pre>
