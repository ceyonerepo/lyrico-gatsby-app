---
title: "kaadhal kanmani song lyrics"
album: "Bachelor"
artist: "G.V. Prakash Kumar"
lyricist: "Nithish"
director: "Sathish Selvakumar"
path: "/albums/bachelor-lyrics"
song: "Kaadhal Kanmani"
image: ../../images/albumart/bachelor.jpg
date: 2021-12-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/e84gy5NColI"
type: "love"
singers:
  - G.V. Prakash Kumar
  - Swagatha S Krishnan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Amor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amor"/>
</div>
<div class="lyrico-lyrics-wrapper">Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love"/>
</div>
<div class="lyrico-lyrics-wrapper">My lust
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My lust"/>
</div>
<div class="lyrico-lyrics-wrapper">Is love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eyy ey eyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy ey eyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy eyy ey hey eyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy eyy ey hey eyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal kanmani sentheeyil minmini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kanmani sentheeyil minmini"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaakkai thedalin andhaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaakkai thedalin andhaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya maalaiyil theya thedalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya maalaiyil theya thedalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaya kaayuthae sentheeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaya kaayuthae sentheeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saalaiyilae oor kadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalaiyilae oor kadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhipokkan naanaanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhipokkan naanaanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhaiyilae poo thazhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaiyilae poo thazhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan thooral neeyaavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan thooral neeyaavaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eyy ey eyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy ey eyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy eyy ey hey eyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy eyy ey hey eyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mm mm mm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm mm mm mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mm m mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm m mm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo oo oo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo oo oo oo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En muththangal theeradha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En muththangal theeradha da"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha kaalamum podhadha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha kaalamum podhadha da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muththangal mandraadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththangal mandraadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mazhalai aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mazhalai aagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Michangal kettanaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Michangal kettanaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru mazhaiyai maarinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peru mazhaiyai maarinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porvaikkul poi kalainthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porvaikkul poi kalainthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Marambizhaigal meettuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marambizhaigal meettuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaar vizhiyum imai polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaar vizhiyum imai polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam manjam kooduvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam manjam kooduvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagalodu irul korkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalodu irul korkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irul korkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul korkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Maalai pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavom vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavom vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavom vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavom vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal ondril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal ondril"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal ondril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal ondril"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaigalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaigalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaigalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaigalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppom vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppom vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eyy ey eyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy ey eyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy eyy ey hey eyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eyy eyy ey hey eyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hooo ooo ooo ooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hooo ooo ooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo ooo oo oo oo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo ooo oo oo oo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal kanmani sentheeyil minmini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kanmani sentheeyil minmini"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaakkai thedalin andhaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaakkai thedalin andhaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya maalaiyil theya thedalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya maalaiyil theya thedalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaya kaayuthae sentheeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaya kaayuthae sentheeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saalaiyilae oor kadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalaiyilae oor kadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhipokkan naanaanaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhipokkan naanaanaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhaiyilae poo thazhuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaiyilae poo thazhuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan thooral neeyaavaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan thooral neeyaavaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amor"/>
</div>
<div class="lyrico-lyrics-wrapper">Love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love"/>
</div>
<div class="lyrico-lyrics-wrapper">My lust
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My lust"/>
</div>
<div class="lyrico-lyrics-wrapper">Is love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Is love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum naana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana"/>
</div>
</pre>
