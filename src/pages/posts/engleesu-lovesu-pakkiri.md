---
title: "engleesu lovesu song lyrics"
album: "Pakkiri"
artist: "Amit Trivedi"
lyricist: "Madhan Karky"
director: "Ken Scott"
path: "/albums/pakkiri-lyrics"
song: "Engleesu Lovesu"
image: ../../images/albumart/pakkiri.jpg
date: 2019-06-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/YbByGNevgc0"
type: "happy"
singers:
  - Dhanush
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ae Kaathey Edhirkaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Kaathey Edhirkaathey"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaasang Konjam Maathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaasang Konjam Maathen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Naethey En Nethey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Naethey En Nethey"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaasala Nee Chaathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaasala Nee Chaathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathu Ala Veesathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathu Ala Veesathen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathu Nikkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu Nikkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaartha Onnu Nee Emmela Veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaartha Onnu Nee Emmela Veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Engleesu Loves Udhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engleesu Loves Udhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Engleesu Loves Udhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engleesu Loves Udhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesu Nee Pesu De
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesu Nee Pesu De"/>
</div>
<div class="lyrico-lyrics-wrapper">Chutham Aavenadee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chutham Aavenadee"/>
</div>
<div class="lyrico-lyrics-wrapper">Artham Kithamellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Artham Kithamellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Venaamadee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Venaamadee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo Aasa Oaraasadhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Aasa Oaraasadhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekka Peraasadhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekka Peraasadhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkka Pooraavume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkka Pooraavume"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Ongoodathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Ongoodathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Pulla Paravaalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Pulla Paravaalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Annandhanni Venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Annandhanni Venaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandha Poravaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandha Poravaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Moochukkaathum Venaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Moochukkaathum Venaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paathukkirundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paathukkirundhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorgam Adhudhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorgam Adhudhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaartha Onnu Nee Emmela Veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaartha Onnu Nee Emmela Veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Engleesu Loves Udhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engleesu Loves Udhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Engleesu Loves Udhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engleesu Loves Udhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Engleesu Loves Udhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engleesu Loves Udhen"/>
</div>
<div class="lyrico-lyrics-wrapper">Engleesu Loves Udhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engleesu Loves Udhen"/>
</div>
</pre>
