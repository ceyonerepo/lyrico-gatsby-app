---
title: 'petta paraak song lyrics'
album: 'Petta'
artist: 'Anirudh Ravichander'
lyricist: 'Vivek'
director: 'Karthik Subbaraj'
path: '/albums/petta-song-lyrics'
song: 'Petta Paraak'
image: ../../images/albumart/petta.jpg
date: 2019-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dG84Kn5ehHA"
type: 'mass'
singers: 
- Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Konjam othingiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam othingiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi pathingiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Odi pathingiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Varathu thalaivaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Varathu thalaivaru"/>
</div>
  <div class="lyrico-lyrics-wrapper">Petta paraak
<input type="checkbox" class="lyrico-select-lyric-line" value="Petta paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kai otharuthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai otharuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla patharuthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla patharuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Morattu kaalai ivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Morattu kaalai ivan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Petta paraak (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Petta paraak"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vettai aadavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vettai aadavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veriyodu suthuraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Veriyodu suthuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pettaiyil puliyaagavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un pettaiyil puliyaagavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaraama vanthu nippaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Asaraama vanthu nippaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">On koottai yeriyae
<input type="checkbox" class="lyrico-select-lyric-line" value="On koottai yeriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Padam kaata pogiraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Padam kaata pogiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thookathil porandaalumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thookathil porandaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan pera solla veppaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan pera solla veppaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Konjam othingiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam othingiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi pathingiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Odi pathingiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Varathu thalaivaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Varathu thalaivaru"/>
</div>
  <div class="lyrico-lyrics-wrapper">Petta paraak
<input type="checkbox" class="lyrico-select-lyric-line" value="Petta paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kai otharuthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai otharuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla patharuthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla patharuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Morattu kaalai ivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Morattu kaalai ivan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Petta paraak
<input type="checkbox" class="lyrico-select-lyric-line" value="Petta paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Keela sarichiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Keela sarichiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alikka thudichiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Alikka thudichiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Murachu mela varaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Murachu mela varaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Petta paraak
<input type="checkbox" class="lyrico-select-lyric-line" value="Petta paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ivan vilunthutaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivan vilunthutaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena nenachiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ena nenachiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeyikka porandhavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Jeyikka porandhavan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Petta paraa…aak
<input type="checkbox" class="lyrico-select-lyric-line" value="Petta paraa…aak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Paa..paa..aaa.paaraak
<input type="checkbox" class="lyrico-select-lyric-line" value="Paa..paa..aaa.paaraak"/>
</div>
<div class="lyrico-lyrics-wrapper">Paa..paa..aaa.paaraak
<input type="checkbox" class="lyrico-select-lyric-line" value="Paa..paa..aaa.paaraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Amaidhiya vechu alakkathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Amaidhiya vechu alakkathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal adikkira ariguri ithu thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Puyal adikkira ariguri ithu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi vizha oru nodi thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idi vizha oru nodi thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna mudikkira neramum athu thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna mudikkira neramum athu thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pagai yeduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pagai yeduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee erinchaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee erinchaalae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Yeh yoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeh yoo"/>
</div>
  <div class="lyrico-lyrics-wrapper">Adha adikki oru
<input type="checkbox" class="lyrico-select-lyric-line" value="Adha adikki oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayutham seivaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Aayutham seivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhai mudichida ninaikathae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhai mudichida ninaikathae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Yeh yoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Yeh yoo"/>
</div>
  <div class="lyrico-lyrics-wrapper">Indha sooriyana urasida venaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha sooriyana urasida venaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vettai aadavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vettai aadavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veriyodu suthuraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Veriyodu suthuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pettaiyil puliyaagavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un pettaiyil puliyaagavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Asaraama vanthu nippaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Asaraama vanthu nippaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">On koottai yeriyae
<input type="checkbox" class="lyrico-select-lyric-line" value="On koottai yeriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Padam kaata pogiraan
<input type="checkbox" class="lyrico-select-lyric-line" value="Padam kaata pogiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thookathil porandaalumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thookathil porandaalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan pera solla veppaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan pera solla veppaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Konjam othingiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam othingiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi pathingiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Odi pathingiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Varathu thalaivaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Varathu thalaivaru"/>
</div>
  <div class="lyrico-lyrics-wrapper">Petta paraak
<input type="checkbox" class="lyrico-select-lyric-line" value="Petta paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kai otharuthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai otharuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla patharuthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla patharuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Morattu kaalai ivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Morattu kaalai ivan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Petta paraak
<input type="checkbox" class="lyrico-select-lyric-line" value="Petta paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Othingiru pathingiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Othingiru pathingiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Varathu thalaivaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Varathu thalaivaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai otharuthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai otharuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla patharuthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla patharuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Morattu kaalai ivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Morattu kaalai ivan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Petta paaraaak…..
<input type="checkbox" class="lyrico-select-lyric-line" value="Petta paaraaak….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey yoo….(2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey yoo…"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Konjam othingiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam othingiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi pathingiru
<input type="checkbox" class="lyrico-select-lyric-line" value="Odi pathingiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Varathu thalaivaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Varathu thalaivaru"/>
</div>
  <div class="lyrico-lyrics-wrapper">Petta paraak
<input type="checkbox" class="lyrico-select-lyric-line" value="Petta paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kai otharuthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai otharuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla patharuthaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla patharuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Morattu kaalai ivan
<input type="checkbox" class="lyrico-select-lyric-line" value="Morattu kaalai ivan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Petta paraak
<input type="checkbox" class="lyrico-select-lyric-line" value="Petta paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Keela sarichiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Keela sarichiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alikka thudichiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Alikka thudichiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Murachu mela varaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Murachu mela varaan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Petta paraak
<input type="checkbox" class="lyrico-select-lyric-line" value="Petta paraak"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ivan vilunthutaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Ivan vilunthutaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ena nenachiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ena nenachiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeyikka porandhavan
<input type="checkbox" class="lyrico-select-lyric-line" value="Jeyikka porandhavan"/>
</div>
  <div class="lyrico-lyrics-wrapper">Petta paraa…aak
<input type="checkbox" class="lyrico-select-lyric-line" value="Petta paraa…aak"/>
</div>
</pre>