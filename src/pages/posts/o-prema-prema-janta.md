---
title: "o prema song lyrics"
album: "Prema Janta"
artist: "Nikhilesh Thogari"
lyricist: "Nikhilesh Thogari"
director: "Nikhilesh Thogari"
path: "/albums/prema-janta-lyrics"
song: "O Prema"
image: ../../images/albumart/prema-janta.jpg
date: 2019-06-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/s-RnXABWriA"
type: "sad"
singers:
  - Vijay Yesudasu
  - Sunitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">oo premaa kalale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo premaa kalale "/>
</div>
<div class="lyrico-lyrics-wrapper">alasi alasi pothu vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alasi alasi pothu vunna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kosam kanule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kosam kanule "/>
</div>
<div class="lyrico-lyrics-wrapper">terachi terachi chusthu vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="terachi terachi chusthu vunna"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve lekunte naku thodevare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve lekunte naku thodevare"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvve nenokate anukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvve nenokate anukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">uvve rakunte naku lerevare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uvve rakunte naku lerevare"/>
</div>
<div class="lyrico-lyrics-wrapper">netho vunte chalu anukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netho vunte chalu anukunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oo premaa kalale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo premaa kalale "/>
</div>
<div class="lyrico-lyrics-wrapper">alasi alasi pothu vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alasi alasi pothu vunna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kosam kanule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kosam kanule "/>
</div>
<div class="lyrico-lyrics-wrapper">terachi terachi chusthu vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="terachi terachi chusthu vunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">etuvaipu chusthunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etuvaipu chusthunna "/>
</div>
<div class="lyrico-lyrics-wrapper">neede prathi roopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neede prathi roopam"/>
</div>
<div class="lyrico-lyrics-wrapper">naludikkulu vintunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naludikkulu vintunna"/>
</div>
<div class="lyrico-lyrics-wrapper">nechiru darahasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nechiru darahasam"/>
</div>
<div class="lyrico-lyrics-wrapper">aluperugaka chesthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aluperugaka chesthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">nekai poratam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nekai poratam"/>
</div>
<div class="lyrico-lyrics-wrapper">paravashamo parihasamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravashamo parihasamo"/>
</div>
<div class="lyrico-lyrics-wrapper">aa devuni vatam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa devuni vatam "/>
</div>
<div class="lyrico-lyrics-wrapper">ee janmalona chesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee janmalona chesina"/>
</div>
<div class="lyrico-lyrics-wrapper">ye neram ee janmalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye neram ee janmalo"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu chesenu dooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu chesenu dooram"/>
</div>
<div class="lyrico-lyrics-wrapper">ee janmalona chesina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee janmalona chesina"/>
</div>
<div class="lyrico-lyrics-wrapper">ye neram ee janmalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye neram ee janmalo"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu chesenu dooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu chesenu dooram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oo premaa kalale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo premaa kalale "/>
</div>
<div class="lyrico-lyrics-wrapper">alasi alasi pothu vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alasi alasi pothu vunna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kosam kanule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kosam kanule "/>
</div>
<div class="lyrico-lyrics-wrapper">terachi terachi chusthu vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="terachi terachi chusthu vunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ne korina wratha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne korina wratha "/>
</div>
<div class="lyrico-lyrics-wrapper">phalitham nuvve anukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="phalitham nuvve anukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavaraku nee thodai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavaraku nee thodai"/>
</div>
<div class="lyrico-lyrics-wrapper">ravalanukunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ravalanukunna "/>
</div>
<div class="lyrico-lyrics-wrapper">kona voopiri tho unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kona voopiri tho unna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kosam kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kosam kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">kona sagadu ee dheham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kona sagadu ee dheham"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvleni janma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvleni janma"/>
</div>
<div class="lyrico-lyrics-wrapper">thala ratha rase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala ratha rase"/>
</div>
<div class="lyrico-lyrics-wrapper">aa brahma kaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa brahma kaina"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu nannu kalipithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu nannu kalipithe"/>
</div>
<div class="lyrico-lyrics-wrapper">dakkunu punyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dakkunu punyam"/>
</div>
<div class="lyrico-lyrics-wrapper">thala ratha rase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thala ratha rase"/>
</div>
<div class="lyrico-lyrics-wrapper">aa brahma kaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa brahma kaina"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnu nannu kalipithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnu nannu kalipithe"/>
</div>
<div class="lyrico-lyrics-wrapper">dakkunu punyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dakkunu punyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oo premaa kalale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo premaa kalale "/>
</div>
<div class="lyrico-lyrics-wrapper">alasi alasi pothu vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alasi alasi pothu vunna"/>
</div>
<div class="lyrico-lyrics-wrapper">nee kosam kanule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee kosam kanule "/>
</div>
<div class="lyrico-lyrics-wrapper">terachi terachi chusthu vunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="terachi terachi chusthu vunna"/>
</div>
</pre>
