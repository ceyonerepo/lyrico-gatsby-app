---
title: "the dirty song lyrics"
album: "Mithai"
artist: "Vivek Sagar"
lyricist: "Traditional Hyderabad song"
director: "Prashant Kumar"
path: "/albums/mithai-lyrics"
song: "The Dirty"
image: ../../images/albumart/mithai.jpg
date: 2019-02-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wW5JX4ppqtk"
type: "happy"
singers:
  - Hafiza B
  - Sahera
  - Sulthana
  - Habedha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Saali Ke Baag Ku Jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saali Ke Baag Ku Jaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Waha Ke Nimbu Churana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waha Ke Nimbu Churana"/>
</div>
<div class="lyrico-lyrics-wrapper">Saali Ke Baag Ku Jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saali Ke Baag Ku Jaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Waha Ke Nimbu Churana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waha Ke Nimbu Churana"/>
</div>
<div class="lyrico-lyrics-wrapper">Waha Ka Mussa Deewana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waha Ka Mussa Deewana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Dil Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Dil Wahi Lagega"/>
</div>
<div class="lyrico-lyrics-wrapper">Waha Ka Mussa Deewana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waha Ka Mussa Deewana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Dil Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Dil Wahi Lagega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buddhi Mangthi Hai Kalakand
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhi Mangthi Hai Kalakand"/>
</div>
<div class="lyrico-lyrics-wrapper">Buddha Detha Hai Dhanadhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddha Detha Hai Dhanadhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Buddhi Mangthi Hai Kalakand
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhi Mangthi Hai Kalakand"/>
</div>
<div class="lyrico-lyrics-wrapper">Buddha Detha Hai Dhanadhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddha Detha Hai Dhanadhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Waha Ka Waha Ka Waha Ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waha Ka Waha Ka Waha Ka"/>
</div>
<div class="lyrico-lyrics-wrapper">Waha Waha Waha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waha Waha Waha"/>
</div>
<div class="lyrico-lyrics-wrapper">Waha Ka Issa Deewana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waha Ka Issa Deewana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Dil Wahe Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Dil Wahe Lagega"/>
</div>
<div class="lyrico-lyrics-wrapper">Waha Ka Issa Deewana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waha Ka Issa Deewana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Dil Wahe Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Dil Wahe Lagega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Budhiya Budhiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budhiya Budhiya "/>
</div>
<div class="lyrico-lyrics-wrapper">Gadiya Chala Lenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadiya Chala Lenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Jirra Mey Baitthe Kone Mey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Jirra Mey Baitthe Kone Mey"/>
</div>
<div class="lyrico-lyrics-wrapper">Haau Kali Ghar Mey Baitthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haau Kali Ghar Mey Baitthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Budhiya Budhiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budhiya Budhiya "/>
</div>
<div class="lyrico-lyrics-wrapper">Gadiya Chala Lenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadiya Chala Lenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabuthar Bola Guttergu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabuthar Bola Guttergu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabuthar Bola Guttergu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabuthar Bola Guttergu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabuthar Bola Guttergu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabuthar Bola Guttergu"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Dil Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Dil Wahi Lagega"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Dil Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Dil Wahi Lagega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saali Ke Unchi Pishani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saali Ke Unchi Pishani"/>
</div>
<div class="lyrico-lyrics-wrapper">Saali Ke Unchi Pishani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saali Ke Unchi Pishani"/>
</div>
<div class="lyrico-lyrics-wrapper">Saali Ke Unchi Pishani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saali Ke Unchi Pishani"/>
</div>
<div class="lyrico-lyrics-wrapper">Bari Mehfil Pahchani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bari Mehfil Pahchani"/>
</div>
<div class="lyrico-lyrics-wrapper">Bari Mehfil Pahchani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bari Mehfil Pahchani"/>
</div>
<div class="lyrico-lyrics-wrapper">Wo Hai Daulat Ki Nashani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wo Hai Daulat Ki Nashani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Dil Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Dil Wahi Lagega"/>
</div>
<div class="lyrico-lyrics-wrapper">Wo Hai Daulat Ki Nashani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wo Hai Daulat Ki Nashani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Dil Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Dil Wahi Lagega"/>
</div>
<div class="lyrico-lyrics-wrapper">Wahi Lagega Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wahi Lagega Wahi Lagega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Budhiya Budhiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budhiya Budhiya "/>
</div>
<div class="lyrico-lyrics-wrapper">Gadiya Chala Lenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadiya Chala Lenge"/>
</div>
<div class="lyrico-lyrics-wrapper">Budhiya Budhiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Budhiya Budhiya "/>
</div>
<div class="lyrico-lyrics-wrapper">Gadiya Chala Lenge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadiya Chala Lenge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saali Ke Lambi Hai Choti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saali Ke Lambi Hai Choti"/>
</div>
<div class="lyrico-lyrics-wrapper">Choti Ka Punna Hai Kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choti Ka Punna Hai Kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Saali Ke Lambi Hai Choti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saali Ke Lambi Hai Choti"/>
</div>
<div class="lyrico-lyrics-wrapper">Choti Ka Punna Hai Kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choti Ka Punna Hai Kaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Soon Lo Gadgad Gadgad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soon Lo Gadgad Gadgad"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadgad Gadgad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadgad Gadgad"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadgad Gad Gadgad Gad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadgad Gad Gadgad Gad"/>
</div>
<div class="lyrico-lyrics-wrapper">Soon Lo Gadgad Ghotala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soon Lo Gadgad Ghotala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Dil Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Dil Wahi Lagega"/>
</div>
<div class="lyrico-lyrics-wrapper">Soon Lo Gadgad Ghotala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soon Lo Gadgad Ghotala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Dil Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Dil Wahi Lagega"/>
</div>
<div class="lyrico-lyrics-wrapper">Wahi Lagega Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wahi Lagega Wahi Lagega"/>
</div>
<div class="lyrico-lyrics-wrapper">Saali Ke Baag Ku Jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saali Ke Baag Ku Jaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Waha Ke Nimbu Churana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waha Ke Nimbu Churana"/>
</div>
<div class="lyrico-lyrics-wrapper">Saali Ke Baag Ku Jaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saali Ke Baag Ku Jaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Waha Ke Nimbu Churana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waha Ke Nimbu Churana"/>
</div>
<div class="lyrico-lyrics-wrapper">Waha Ka Mussa Deewana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waha Ka Mussa Deewana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Dil Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Dil Wahi Lagega"/>
</div>
<div class="lyrico-lyrics-wrapper">Waha Ka Mussa Deewana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waha Ka Mussa Deewana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Dil Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Dil Wahi Lagega"/>
</div>
<div class="lyrico-lyrics-wrapper">Waha Ka Mussa Deewana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Waha Ka Mussa Deewana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mera Dil Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mera Dil Wahi Lagega"/>
</div>
<div class="lyrico-lyrics-wrapper">Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wahi Lagega"/>
</div>
<div class="lyrico-lyrics-wrapper">Wahi Lagega Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wahi Lagega Wahi Lagega"/>
</div>
<div class="lyrico-lyrics-wrapper">Wahi Lagega Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wahi Lagega Wahi Lagega"/>
</div>
<div class="lyrico-lyrics-wrapper">Wahi Lagega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wahi Lagega"/>
</div>
</pre>
