---
title: "sheikpeta chandini song lyrics"
album: "Captain Rana Prathap"
artist: "Shran"
lyricist: "Chandra Bose "
director: "Haranath Policherla"
path: "/albums/captain-rana-prathap-lyrics"
song: "Sheikpeta Chandini"
image: ../../images/albumart/captain-rana-prathap.jpg
date: 2019-06-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Azi_Rap8LX4"
type: "happy"
singers:
  - Geetha Madhuri
  - Damini Bhatla
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">chamanthini kanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chamanthini kanu"/>
</div>
<div class="lyrico-lyrics-wrapper">poobanthini kanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poobanthini kanu"/>
</div>
<div class="lyrico-lyrics-wrapper">mandhakini kanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mandhakini kanu"/>
</div>
<div class="lyrico-lyrics-wrapper">marumallenu kanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marumallenu kanu"/>
</div>
<div class="lyrico-lyrics-wrapper">soudhamini kanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soudhamini kanu"/>
</div>
<div class="lyrico-lyrics-wrapper">sampangini kanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sampangini kanu"/>
</div>
<div class="lyrico-lyrics-wrapper">balamani kanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="balamani kanu"/>
</div>
<div class="lyrico-lyrics-wrapper">chinthamanini asale kanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinthamanini asale kanu"/>
</div>
<div class="lyrico-lyrics-wrapper">marevvaru nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marevvaru nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">sheikpeta sheikpeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheikpeta sheikpeta"/>
</div>
<div class="lyrico-lyrics-wrapper">sheikpeta chandini nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheikpeta chandini nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">sheiku chesi choopisthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheiku chesi choopisthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">sheikpeta chandini nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheikpeta chandini nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">sheiku chesi choopisthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheiku chesi choopisthanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">dabbulaku padanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dabbulaku padanu"/>
</div>
<div class="lyrico-lyrics-wrapper">daabulaku padanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daabulaku padanu"/>
</div>
<div class="lyrico-lyrics-wrapper">dhamkeelaku padanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhamkeelaku padanu"/>
</div>
<div class="lyrico-lyrics-wrapper">chamkeelaku padanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chamkeelaku padanu"/>
</div>
<div class="lyrico-lyrics-wrapper">thophalaku padanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thophalaku padanu"/>
</div>
<div class="lyrico-lyrics-wrapper">thopulaku padanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thopulaku padanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sheikpeta sheikpeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheikpeta sheikpeta"/>
</div>
<div class="lyrico-lyrics-wrapper">sheikpeta chandini nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheikpeta chandini nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">sheiku chesi choopisthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheiku chesi choopisthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">sheikpeta chandini nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheikpeta chandini nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">sheiku chesi choopisthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheiku chesi choopisthanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee meesam choosanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee meesam choosanu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne icei poayanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne icei poayanu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee roopam choosanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee roopam choosanu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne neerai kariganu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne neerai kariganu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee cheyye choosanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee cheyye choosanu"/>
</div>
<div class="lyrico-lyrics-wrapper">kalapalanukunnanu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalapalanukunnanu "/>
</div>
<div class="lyrico-lyrics-wrapper">nee chathi choosanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee chathi choosanu"/>
</div>
<div class="lyrico-lyrics-wrapper">nilavalanukunnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilavalanukunnanu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee pogare choosanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pogare choosanu"/>
</div>
<div class="lyrico-lyrics-wrapper">pagadalanukunnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pagadalanukunnanu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee teguve choosanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee teguve choosanu"/>
</div>
<div class="lyrico-lyrics-wrapper">ne thagananukunnanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne thagananukunnanu"/>
</div>
<div class="lyrico-lyrics-wrapper">choosanu choosanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choosanu choosanu"/>
</div>
<div class="lyrico-lyrics-wrapper">manasantha pichchekki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasantha pichchekki"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vashamaipoyanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vashamaipoyanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sheikpeta sheikpeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheikpeta sheikpeta"/>
</div>
<div class="lyrico-lyrics-wrapper">sheikpeta chandini nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheikpeta chandini nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">sheiku chesinonne choopisthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheiku chesinonne choopisthane"/>
</div>
<div class="lyrico-lyrics-wrapper">sheikpeta chandini nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheikpeta chandini nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">sheiku chesinonne choopisthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheiku chesinonne choopisthane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee gunde nakinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee gunde nakinka"/>
</div>
<div class="lyrico-lyrics-wrapper">golkonda kotanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="golkonda kotanta"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thodu naakinka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thodu naakinka"/>
</div>
<div class="lyrico-lyrics-wrapper">ladu bajaranka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ladu bajaranka"/>
</div>
<div class="lyrico-lyrics-wrapper">nee dhamme naa inta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dhamme naa inta"/>
</div>
<div class="lyrico-lyrics-wrapper">dham biryani anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dham biryani anta"/>
</div>
<div class="lyrico-lyrics-wrapper">nee pyare na nota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee pyare na nota"/>
</div>
<div class="lyrico-lyrics-wrapper">kurbani ka meeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurbani ka meeta"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvunte naa baata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvunte naa baata"/>
</div>
<div class="lyrico-lyrics-wrapper">koti abidsanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koti abidsanta"/>
</div>
<div class="lyrico-lyrics-wrapper">nee jante rayyanka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee jante rayyanka"/>
</div>
<div class="lyrico-lyrics-wrapper">nampalli railanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nampalli railanta"/>
</div>
<div class="lyrico-lyrics-wrapper">saradhaga avunante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saradhaga avunante"/>
</div>
<div class="lyrico-lyrics-wrapper">sarikotha charminaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarikotha charminaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kattinehisthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattinehisthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">saradhaga avunante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saradhaga avunante"/>
</div>
<div class="lyrico-lyrics-wrapper">sarikotha charminaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarikotha charminaru"/>
</div>
<div class="lyrico-lyrics-wrapper">kattinehisthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattinehisthanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sheikpeta sheikpeta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheikpeta sheikpeta"/>
</div>
<div class="lyrico-lyrics-wrapper">sheikpeta chandini nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheikpeta chandini nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">sheiku chesinonne choopisthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheiku chesinonne choopisthane"/>
</div>
<div class="lyrico-lyrics-wrapper">sheikpeta chandini nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheikpeta chandini nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">sheiku chesinonne choopisthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sheiku chesinonne choopisthane"/>
</div>
</pre>
