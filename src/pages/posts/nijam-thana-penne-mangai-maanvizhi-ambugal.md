---
title: "nijam thana penne song lyrics"
album: "Mangai Maanvizhi Ambugal"
artist: "Thameem Ansari"
lyricist: "Vno - Arungopal"
director: "Vino"
path: "/albums/mangai-maanvizhi-ambugal-lyrics"
song: "Nijam Thana Penne"
image: ../../images/albumart/mangai-maanvizhi-ambugal.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gqGc2BPrxYA"
type: "love"
singers:
  - Haricharan
  - Padmalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nijam Thaana Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Thaana Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruginil Vandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginil Vandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Silirthu Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Silirthu Konden"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Pennaeoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Pennaeoh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Pola Endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pola Endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvaaya Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvaaya Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethir Paarthu Nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paarthu Nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatkal Ellaam Anbae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatkal Ellaam Anbae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan Kangal Kaanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Kangal Kaanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirukkiren Naalum Naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirukkiren Naalum Naanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagaratha Nodigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaratha Nodigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Porum Thodukiren Kadikarathudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porum Thodukiren Kadikarathudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Maalai Nerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Maalai Nerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai Orathil Maramaai Nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai Orathil Maramaai Nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kangal Kandathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kangal Kandathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhanthai Polavae Thulli Kuthikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhanthai Polavae Thulli Kuthikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Naanum Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naanum Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenohoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenohoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Ullae Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Ullae Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Thuralaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Thuralaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uraiyadum Nodigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraiyadum Nodigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Urainthu Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urainthu Pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Vaarthaigal Paniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Vaarthaigal Paniyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesatha Nodigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesatha Nodigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesana Urasalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesana Urasalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambal Aagiren Analaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambal Aagiren Analaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnodu Nadakkayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Nadakkayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnil Parakiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnil Parakiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Vaanathil Vinmeenaagaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Vaanathil Vinmeenaagaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vennilavu Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vennilavu Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Megamaga Soolnthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Megamaga Soolnthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pen Ivalai Neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Ivalai Neethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kanniyathaal Vendrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanniyathaal Vendrai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaahaaahaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaahaaahaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haahaaoooheeheehheeyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haahaaoooheeheehheeyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaa Aaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaa Aaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaa Aaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaa Aaaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saalai Rayilaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai Rayilaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinamum Nee Ennil Thodarnthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinamum Nee Ennil Thodarnthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalai Veyilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai Veyilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Muluthum Nee Ennnil Padarnthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muluthum Nee Ennnil Padarnthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai Mayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Mayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thogai Nee Virithaai Enakaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thogai Nee Virithaai Enakaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Unadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Unadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithil Enna Kelvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithil Enna Kelvi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Poovil Theanai Thedum Vandaai Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Poovil Theanai Thedum Vandaai Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Palaivanathil Pootha Muthal Rojavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Palaivanathil Pootha Muthal Rojavae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thevai Neethan Anbae Unakai Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thevai Neethan Anbae Unakai Naanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkulum Naanum Vandhaal Sollen Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkulum Naanum Vandhaal Sollen Pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uraiyadum Nodigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uraiyadum Nodigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Urainthu Pogiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urainthu Pogiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Vaarthaigal Paniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Vaarthaigal Paniyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesatha Nodigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesatha Nodigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Lesana Urasalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lesana Urasalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambal Aagiren Analaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambal Aagiren Analaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu Nadakkayil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu Nadakkayil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnil Parakiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnil Parakiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Vaanathil Vinmeenaagaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Vaanathil Vinmeenaagaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan Kangal Kaanavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Kangal Kaanavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirukkiren Naalum Naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirukkiren Naalum Naanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagaratha Nodigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaratha Nodigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Porum Thodukiren Kadikarathudan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porum Thodukiren Kadikarathudan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Maalai Nerathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Maalai Nerathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai Orathil Maramaai Nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai Orathil Maramaai Nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kangal Kandathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kangal Kandathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhanthai Polavae Thulli Kuthikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhanthai Polavae Thulli Kuthikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenohoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenohoho"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Naanum Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Naanum Kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenohoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenohoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Ullae Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Ullae Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal Thuralaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal Thuralaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijam Thaana Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Thaana Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam Thaana Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Thaana Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruginil Vandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginil Vandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruginil Vandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginil Vandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Silirthu Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Silirthu Konden"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum Silirthu Konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Silirthu Konden"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaan Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethaan Pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaaaaaa"/>
</div>
</pre>
