---
title: "oh my angel song lyrics"
album: "Yuvan Yuvathi"
artist: "Vijay Antony"
lyricist: "Annamalai"
director: "G.N.R. Kumaravelan"
path: "/albums/yuvan-yuvathi-lyrics"
song: "Oh My Angel"
image: ../../images/albumart/yuvan-yuvathi.jpg
date: 2011-08-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/HkPSwOfAWjE"
type: "love"
singers:
  - Vijay Antony
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unnai Marakkaamal Iruppadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Marakkaamal Iruppadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Irakkaamal Irukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakkaamal Irukkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En Imaigal Moodum Bodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Imaigal Moodum Bodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mugam Paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugam Paarkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Marakkaamal Iruppadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Marakkaamal Iruppadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Irakkaamal Irukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakkaamal Irukkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En Imaigal Moodum Bodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Imaigal Moodum Bodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mugam Paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugam Paarkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Marakkaamal Iruppadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Marakkaamal Iruppadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Irakkaamal Irukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakkaamal Irukkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En Imaigal Moodum Bodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Imaigal Moodum Bodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mugam Paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugam Paarkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kannil Vizhundha Naalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannil Vizhundha Naalil"/>
</div>
<div class="lyrico-lyrics-wrapper">En Amaidhi Kalainthathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Amaidhi Kalainthathadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Kallai Yerindha Kulamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Kallai Yerindha Kulamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Alai Vandhu Ezhundhadhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Alai Vandhu Ezhundhadhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kangalil Uyir Vandhu Kasigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangalil Uyir Vandhu Kasigiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Kaadhal Kodutha Vali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Kaadhal Kodutha Vali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Kadalinai Oru Thuli Pirigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Kadalinai Oru Thuli Pirigiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ennai Pirindha Nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Pirindha Nodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Marakkaamal Iruppadhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Marakkaamal Iruppadhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Irakkaamal Irukkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakkaamal Irukkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">En Imaigal Moodum Bodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Imaigal Moodum Bodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Mugam Paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugam Paarkkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Udalai Pirindhu Veliyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Udalai Pirindhu Veliyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Uyirdhaan Alaiyuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Uyirdhaan Alaiyuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Mattum Ingae Thaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Mattum Ingae Thaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Idhayam Valikkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayam Valikkuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udalukku Oru Murai Maranam Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udalukku Oru Murai Maranam Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manam Dhinam Saaguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manam Dhinam Saaguthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naraghathai Pol En Vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naraghathai Pol En Vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nyaabagam Kolludhadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nyaabagam Kolludhadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh My Angel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh My Angel"/>
</div>
</pre>
