---
title: "chukkala melam song lyrics"
album: "Sridevi Soda Center"
artist: "Mani Sharma"
lyricist: "Kalyan Chakravarthy"
director: "Karuna Kumar"
path: "/albums/sridevi-soda-center-lyrics"
song: "Chukkala Melam"
image: ../../images/albumart/sridevi-soda-center.jpg
date: 2021-08-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/FI77xc70CeI"
type: "love"
singers:
  - Anurag Kulakarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chukkalamelam dhikkula thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalamelam dhikkula thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Okataye ee sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okataye ee sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasantham nee sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasantham nee sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkuva ledu ekkuva kadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkuva ledu ekkuva kadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkuva unte jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkuva unte jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa sontham vaasantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa sontham vaasantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkaraleka akkuna chere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkaraleka akkuna chere"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakkani choraveraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkani choraveraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekkalu vesi mukkalu chesthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekkalu vesi mukkalu chesthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Viluva marugeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viluva marugeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Chukkalamelam dhikkula thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Chukkalamelam dhikkula thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Okataye ee sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okataye ee sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasantham nee sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasantham nee sontham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bathuku padhuguritho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathuku padhuguritho"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuku padidhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuku padidhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadaka naluguritho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadaka naluguritho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisi nadavamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisi nadavamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnanthanga chudara mari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnanthanga chudara mari"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnadhe sneham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnadhe sneham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nammakanga saagara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammakanga saagara "/>
</div>
<div class="lyrico-lyrics-wrapper">kada dhaka o nestham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kada dhaka o nestham"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Chukkalamelam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Chukkalamelam "/>
</div>
<div class="lyrico-lyrics-wrapper">dhikkula thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhikkula thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Okataye ee sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okataye ee sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasantham nee sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasantham nee sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkuva ledu ekkuva kadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkuva ledu ekkuva kadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkuva unte jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkuva unte jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa sontham vaasantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa sontham vaasantham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalathale daati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalathale daati"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalupu dhooranne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalupu dhooranne"/>
</div>
<div class="lyrico-lyrics-wrapper">Koratha yepaate 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koratha yepaate "/>
</div>
<div class="lyrico-lyrics-wrapper">kolatha vey dhaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolatha vey dhaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kastamocchi nerpina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kastamocchi nerpina "/>
</div>
<div class="lyrico-lyrics-wrapper">tholi mucchati ee maata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholi mucchati ee maata"/>
</div>
<div class="lyrico-lyrics-wrapper">Istapadatam nerchuko 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Istapadatam nerchuko "/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalamelam dhikkula thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalamelam dhikkula thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Okataye ee sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okataye ee sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasantham nee sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasantham nee sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkuva ledu ekkuva kadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkuva ledu ekkuva kadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkuva unte jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkuva unte jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa sontham vaasantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa sontham vaasantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Viluvicchi prathi chotaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viluvicchi prathi chotaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Chukkalamelam dhikkula thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Chukkalamelam dhikkula thaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Okataye ee sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okataye ee sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasantham nee sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasantham nee sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakkuva ledu ekkuva kadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakkuva ledu ekkuva kadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkuva unte jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkuva unte jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa sontham vaasantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa sontham vaasantham"/>
</div>
</pre>
