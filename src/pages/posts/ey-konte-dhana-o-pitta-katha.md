---
title: "ey konte dhana song lyrics"
album: "O Pitta Katha"
artist: "Pravin Lakkaraju"
lyricist: "Sreejo"
director: "Chendu Muddhu"
path: "/albums/o-pitta-katha-lyrics"
song: "Ey Konte Dhana"
image: ../../images/albumart/o-pitta-katha.jpg
date: 2020-03-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/EG_BOB7WqpI"
type: "love"
singers:
  - Sreenivas Joyusula
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye kontey daana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye kontey daana"/>
</div>
<div class="lyrico-lyrics-wrapper">andala maina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andala maina"/>
</div>
<div class="lyrico-lyrics-wrapper">gundethoti aataladakey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundethoti aataladakey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ye chilipi kadaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye chilipi kadaley"/>
</div>
<div class="lyrico-lyrics-wrapper">neethoti modaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neethoti modaley"/>
</div>
<div class="lyrico-lyrics-wrapper">kunukuraani kaanukeyavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kunukuraani kaanukeyavey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">choopupaina prema 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choopupaina prema "/>
</div>
<div class="lyrico-lyrics-wrapper">vaali aaviraindey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaali aaviraindey"/>
</div>
<div class="lyrico-lyrics-wrapper">thappu needi kaadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thappu needi kaadu "/>
</div>
<div class="lyrico-lyrics-wrapper">anddam antukunnade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anddam antukunnade"/>
</div>
<div class="lyrico-lyrics-wrapper">aakeleyakunna lona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aakeleyakunna lona "/>
</div>
<div class="lyrico-lyrics-wrapper">korukuthunnade manusuney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="korukuthunnade manusuney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manusu nee prema alleney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusu nee prema alleney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye konti daana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye konti daana"/>
</div>
<div class="lyrico-lyrics-wrapper">andala maina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andala maina"/>
</div>
<div class="lyrico-lyrics-wrapper">gundethoti aataladakey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gundethoti aataladakey"/>
</div>
</pre>
