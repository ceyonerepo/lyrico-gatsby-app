---
title: 'kaathodu kaathanen song lyrics'
album: 'Jail'
artist: 'G V Prakash Kumar'
lyricist: 'Kabilan'
director: 'G Vasanthabalan'
path: '/albums/jail-song-lyrics'
song: 'Kaathodu Kaathanen'
image: ../../images/albumart/jail.jpg
date: 2021-12-09
lang: tamil
singers: 
- Dhanush
- Aditi Rao Hydari
youtubeLink: "https://www.youtube.com/embed/LUO4KPeVjzc"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Oh pennae oh pennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh pennae oh pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaanae neethaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Neethaanae neethaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh pennae oh pennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh pennae oh pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethaanae neethaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Neethaanae neethaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaathodu kaathaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaathodu kaathaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae un moochaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannae un moochaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerodu neeraanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Neerodu neeraanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kooda meenaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kooda meenaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaagitham polae un mela
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaagitham polae un mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Oviyam varaiyum nagamaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Oviyam varaiyum nagamaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogathil pennae unnaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Mogathil pennae unnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththangal vaazhum mugamaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Muththangal vaazhum mugamaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ilai maraivil malarnthirundhom
<input type="checkbox" class="lyrico-select-lyric-line" value="Ilai maraivil malarnthirundhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai thuliyai kalanthirundhom
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhai thuliyai kalanthirundhom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaathodu kaathaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaathodu kaathaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae un moochaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannae un moochaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerodu neeraanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Neerodu neeraanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kooda meenaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kooda meenaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naa nana naa…(4 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa nana naa…"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nana naa..(3 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Nana naa.."/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ilaiyil malarin kai regai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ilaiyil malarin kai regai"/>
</div>
<div class="lyrico-lyrics-wrapper">Imaigal yaavum mayil thogai
<input type="checkbox" class="lyrico-select-lyric-line" value="Imaigal yaavum mayil thogai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aayiram aandugal aanaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Aayiram aandugal aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandha vanmam maravenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Aanandha vanmam maravenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mazhalai polavae madiyil thavazhndha
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhalai polavae madiyil thavazhndha"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayakkam theeravae illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Mayakkam theeravae illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Irandu perumae ini mel yaaro
<input type="checkbox" class="lyrico-select-lyric-line" value="Irandu perumae ini mel yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivan kaigalil pillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Iraivan kaigalil pillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kanmani poo pookka
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanmani poo pookka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vidhaiyaanom
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal vidhaiyaanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaaman naatkuripil
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaaman naatkuripil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal kadhaiyaanom..mmm
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal kadhaiyaanom..mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaathodu kaathaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaathodu kaathaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae un moochaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannae un moochaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerodu neeraanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Neerodu neeraanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kooda meenaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kooda meenaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Poovin meedhu koothaadum
<input type="checkbox" class="lyrico-select-lyric-line" value="Poovin meedhu koothaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Bodhai vandu pol aanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Bodhai vandu pol aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pullin meedhu boomiyai pol
<input type="checkbox" class="lyrico-select-lyric-line" value="Pullin meedhu boomiyai pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan baaram naan kanden
<input type="checkbox" class="lyrico-select-lyric-line" value="Undhan baaram naan kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idhazhin aatrilae kudhikkum podhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhazhin aatrilae kudhikkum podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaigal enbathae illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Karaigal enbathae illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Karaigal illai parava illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Karaigal illai parava illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalae kaadhalin ellai
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadalae kaadhalin ellai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Both  Vervai thuligalilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Both  Vervai thuligalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nanaithaayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennai nanaithaayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam norunga thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhayam norunga thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukki anaithaayae
<input type="checkbox" class="lyrico-select-lyric-line" value="Irukki anaithaayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaathodu kaathaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaathodu kaathaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannae un moochaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannae un moochaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerodu neeraanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Neerodu neeraanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kooda meenaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kooda meenaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Both  Aayiram aasaigal thalaatta
<input type="checkbox" class="lyrico-select-lyric-line" value="Both  Aayiram aasaigal thalaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Un maarbinil mella vizhundhenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un maarbinil mella vizhundhenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal moodiyae nadanthathellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhigal moodiyae nadanthathellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanden rasithen sugamaanen
<input type="checkbox" class="lyrico-select-lyric-line" value="Kanden rasithen sugamaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Both  Ilai maraivil malarnthirundhom
<input type="checkbox" class="lyrico-select-lyric-line" value="Both  Ilai maraivil malarnthirundhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai thuliyai kalanthirundhom
<input type="checkbox" class="lyrico-select-lyric-line" value="Mazhai thuliyai kalanthirundhom"/>
</div>
</pre>