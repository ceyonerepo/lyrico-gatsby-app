---
title: "engada pona female song lyrics"
album: "Abhiyum Anuvum"
artist: "Dharan Kumar"
lyricist: "Madhan Karky"
director: "B.R. Vijayalakshmi"
path: "/albums/abhiyum-anuvum-lyrics"
song: "Engada Pona Female"
image: ../../images/albumart/abhiyum-anuvum.jpg
date: 2018-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TjZGhvrgrl8"
type: "sad"
singers:
  - Nikhita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pesiya Vaarthaigal Kasakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesiya Vaarthaigal Kasakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesiya Paarvaigal Arukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesiya Paarvaigal Arukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosiya Theendalgal Erikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosiya Theendalgal Erikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Erivom Urave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erivom Urave "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrai Maatrida Thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrai Maatrida Thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrai Thaandida Ninaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrai Thaandida Ninaika"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Thondridum Kadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Thondridum Kadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadapom Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadapom Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kelvikuri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kelvikuri "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennul Valargirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennul Valargirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathil Karaikirathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathil Karaikirathe "/>
</div>
<div class="lyrico-lyrics-wrapper">Karaivom Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaivom Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttru Pulli Ena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttru Pulli Ena "/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkkai Virigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkkai Virigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Paranthiduvọm 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Paranthiduvọm "/>
</div>
<div class="lyrico-lyrics-wrapper">Urave Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urave Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engada Pona Engadi Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pona Engadi Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Engada Pona Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pona Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthada Valikuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthada Valikuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engada Pọna Engadi Pọna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pọna Engadi Pọna"/>
</div>
<div class="lyrico-lyrics-wrapper">Engada Pọna Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pọna Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthada Valikuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthada Valikuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadiya Kaankalil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadiya Kaankalil "/>
</div>
<div class="lyrico-lyrics-wrapper">Paadiya Pulveli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadiya Pulveli"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhediya Vinmeen 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhediya Vinmeen "/>
</div>
<div class="lyrico-lyrics-wrapper">Koodiya Raathiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodiya Raathiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaiyu Enai Viddu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaiyu Enai Viddu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninkavillai Urave Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninkavillai Urave Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruthiyin Varayilum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruthiyin Varayilum "/>
</div>
<div class="lyrico-lyrics-wrapper">Uruthikal Seithathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruthikal Seithathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuthiya Yaavume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuthiya Yaavume "/>
</div>
<div class="lyrico-lyrics-wrapper">Puluthiyaay Kidappathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puluthiyaay Kidappathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Avaikalai Methiththe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avaikalai Methiththe "/>
</div>
<div class="lyrico-lyrics-wrapper">Naamum Nadanthiduvom Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamum Nadanthiduvom Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anru Kadhal Naam Thurakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anru Kadhal Naam Thurakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Inru Arukil Nee Irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inru Arukil Nee Irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Eppadi Marakka Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Eppadi Marakka Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthalanugalin Vidayaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthalanugalin Vidayaay"/>
</div>
<div class="lyrico-lyrics-wrapper">Anu Anuvaay Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anu Anuvaay Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnil Valivathu Sariyaa Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnil Valivathu Sariyaa Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Kelvikuri Ennul Valargirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kelvikuri Ennul Valargirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathil Karaikirathe Karaivom Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathil Karaikirathe Karaivom Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttru Pulli Ena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttru Pulli Ena "/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkkai Virigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkkai Virigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Athil Paranthiduvọm Urave Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athil Paranthiduvọm Urave Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesiya Vaarthaigal Kasakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesiya Vaarthaigal Kasakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesiya Paarvaigal Arukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesiya Paarvaigal Arukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosiya Theendalgal Erikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosiya Theendalgal Erikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Erivom Urave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erivom Urave "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netrai Maatrida Thudikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netrai Maatrida Thudikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrai Thaandida Ninaika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrai Thaandida Ninaika"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai Thondridum Kadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Thondridum Kadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadapom Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadapom Urave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engada Pona Engadi Pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pona Engadi Pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Engada Pona Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pona Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthada Valikuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthada Valikuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engada Pona Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pona Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthada Valikuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthada Valikuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engada Pọna Engadi Pọna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pọna Engadi Pọna"/>
</div>
<div class="lyrico-lyrics-wrapper">Engada Pọna Ennai Vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engada Pọna Ennai Vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">Valikuthada Valikuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valikuthada Valikuthada"/>
</div>
</pre>
