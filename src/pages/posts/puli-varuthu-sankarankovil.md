---
title: "puli varuthu song lyrics"
album: "Sankarankovil"
artist: "Rajini"
lyricist: "Snehan"
director: "Palanivel Raja"
path: "/albums/sankarankovil-lyrics"
song: "Puli Varuthu"
image: ../../images/albumart/sankarankovil.jpg
date: 2011-08-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tG8XxE01bns"
type: "love"
singers:
  - Priyadharshini
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Puli varudhu puli varudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli varudhu puli varudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">unna kadichithinna puli varudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kadichithinna puli varudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli varudhu puli varudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli varudhu puli varudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">unna kadichithinna puli varudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kadichithinna puli varudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nari varudhu nari varudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nari varudhu nari varudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">unna urinji thinna narivarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna urinji thinna narivarudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattikkammaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikkammaatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vidamaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vidamaatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattikkamaatten Ongitta naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikkamaatten Ongitta naan"/>
</div>
<div class="lyrico-lyrics-wrapper">adangudi neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangudi neethaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un moagathil medhakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un moagathil medhakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai nee verukkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai nee verukkaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En aanmai endrumey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aanmai endrumey "/>
</div>
<div class="lyrico-lyrics-wrapper">unnidam thoarkkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnidam thoarkkaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puli varudhu puli varudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli varudhu puli varudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">unna kadichithinna puli varudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kadichithinna puli varudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nari varudhu nari varudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nari varudhu nari varudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">unna urinji thinna narivarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna urinji thinna narivarudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poiyaa vaazhuren naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poiyaa vaazhuren naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalil poiyum venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalil poiyum venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkamey enakku illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkamey enakku illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbuthaan irukku ulley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbuthaan irukku ulley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadhu soodhu koabam ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadhu soodhu koabam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda pirandhadhu poaividu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda pirandhadhu poaividu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal mattum poadhum enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal mattum poadhum enakku"/>
</div>
<div class="lyrico-lyrics-wrapper">saaindhu kola thoal kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saaindhu kola thoal kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan vettaiyaadum inandhaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vettaiyaadum inandhaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thookki erindhu poamaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thookki erindhu poamaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un koabamum enakku sagandhaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un koabamum enakku sagandhaaney"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vaazhumvaraiyil varuveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vaazhumvaraiyil varuveney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna nambida venaam naano padu paavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nambida venaam naano padu paavi"/>
</div>
<div class="lyrico-lyrics-wrapper">ada sonnaakkelu neeyo appaavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada sonnaakkelu neeyo appaavi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puli varudhu puli varudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli varudhu puli varudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">unna kadichithinna puli varudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kadichithinna puli varudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nari varudhu nari varudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nari varudhu nari varudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">unna urinji thinna narivarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna urinji thinna narivarudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagiya manmadhan kedaippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya manmadhan kedaippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbiley unnidam thoarppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbiley unnidam thoarppaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Poarkkalamaanadhu vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poarkkalamaanadhu vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Poarilum kaakkumey un kai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poarilum kaakkumey un kai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boadhai ennum aatril vizhundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boadhai ennum aatril vizhundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">paadhai thavariye poagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadhai thavariye poagiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raaja boadhai vendum unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raaja boadhai vendum unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Raani naanum tharugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raani naanum tharugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theru Oram kidakkum mannaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru Oram kidakkum mannaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">oru sirppam aaga mudiyaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru sirppam aaga mudiyaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru sirppam endru aanaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru sirppam endru aanaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">siru pullum adhiley mulaikkaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru pullum adhiley mulaikkaadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kannaamoochi aattam aadaadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kannaamoochi aattam aadaadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">ada naragathil vandhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada naragathil vandhu "/>
</div>
<div class="lyrico-lyrics-wrapper">sorkkam thedaadhey hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sorkkam thedaadhey hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puli varudhu puli varudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli varudhu puli varudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">unna kadichithinna puli varudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna kadichithinna puli varudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nari varudhu nari varudhu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nari varudhu nari varudhu "/>
</div>
<div class="lyrico-lyrics-wrapper">unna urinji thinna narivarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna urinji thinna narivarudhu"/>
</div>
</pre>
