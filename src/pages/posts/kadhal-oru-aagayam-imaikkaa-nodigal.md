---
title: "kadhal oru aagayam song lyrics"
album: "Imaikkaa Nodigal"
artist: "Hiphop Tamizha"
lyricist: "Mohanrajan"
director: "R. Ajay Gnanamuthu"
path: "/albums/imaikkaa-nodigal-lyrics"
song: "Kadhal Oru Aagayam"
image: ../../images/albumart/imaikkaa-nodigal.jpg
date: 2018-08-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kmhrT7fPYO4"
type: "sad"
singers:
  - Teejay
  - Al-Rufian
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaadhal Oru Aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Oru Aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Endrum Veezhvadhu Illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Endrum Veezhvadhu Illaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer Oru Venmegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Oru Venmegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhaamal Irupadhum Illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhaamal Irupadhum Illaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalukkulle Meen Azhudhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalukkulle Meen Azhudhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Meen Kanneer Veliye Theriyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meen Kanneer Veliye Theriyadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Mella Nee Unarndhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Mella Nee Unarndhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaadhal Endrum Piriyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaadhal Endrum Piriyathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Oru Aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Oru Aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Endrum Veezhvadhu Illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Endrum Veezhvadhu Illaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer Oru Venmegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Oru Venmegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhaamal Irupadhum Illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhaamal Irupadhum Illaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhayam Ketkum Kaadhalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Ketkum Kaadhalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Verethaiyum Kettida Theriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verethaiyum Kettida Theriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbai Ketkum Kaadhalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbai Ketkum Kaadhalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhegam Thaangida Mudiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhegam Thaangida Mudiyadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Medum Pallam Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medum Pallam Illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Paadhai Ingu Kidaiyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paadhai Ingu Kidaiyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirivum Thuyaram Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirivum Thuyaram Illaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kaadhalin Aalam Puriyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kaadhalin Aalam Puriyadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Oru Aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Oru Aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Endrum Veezhvadhu Illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Endrum Veezhvadhu Illaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer Oru Venmegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Oru Venmegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhaamal Irupadhum Illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhaamal Irupadhum Illaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalukkulle Meen Azhudhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalukkulle Meen Azhudhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Meen Kanneer Veliye Theriyadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meen Kanneer Veliye Theriyadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai Mella Nee Unarndhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Mella Nee Unarndhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaadhal Endrum Piriyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaadhal Endrum Piriyathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Oru Aagayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Oru Aagayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu Endrum Veezhvadhu Illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu Endrum Veezhvadhu Illaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneer Oru Venmegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Oru Venmegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veezhaamal Irupadhum Illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veezhaamal Irupadhum Illaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illaiyadi Illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaiyadi Illaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaiyadi Illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaiyadi Illaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illaiyadi"/>
</div>
</pre>
