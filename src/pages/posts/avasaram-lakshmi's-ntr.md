---
title: "avasaram song lyrics"
album: "lakshmis ntr"
artist: "Kalyani Malik"
lyricist: "Sira Sri"
director: "Ram Gopal Varma - Agasthya Manju"
path: "/albums/lakshmi's-ntr-lyrics"
song: "Avasaram"
image: ../../images/albumart/lakshmis-ntr.jpg
date: 2019-03-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/rgug1OopuMM"
type: "melody"
singers:
  - Wilson Herald
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Avasaram Avasaram Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaram Avasaram Avasaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Avasaram Avasaram Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaram Avasaram Avasaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasthulanni Panche Varake Thandri Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasthulanni Panche Varake Thandri Avasaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Varakatnam Icche Varake Mama Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varakatnam Icche Varake Mama Avasaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuragam Athmiyathalu Mayamatalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuragam Athmiyathalu Mayamatalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avasaram Theere Varake Bandhamavasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaram Theere Varake Bandhamavasaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avasaram Avasaram Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaram Avasaram Avasaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Avasaram Avasaram Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaram Avasaram Avasaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kayalamme Roju Chettu Peru Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kayalamme Roju Chettu Peru Avasaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Sedhadhiru Velaa Dhaani Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sedhadhiru Velaa Dhaani Avasaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aakuralu Kaalamocchi Modibarithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aakuralu Kaalamocchi Modibarithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yavvadiki Dhaani Baadha Ledhu Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yavvadiki Dhaani Baadha Ledhu Avasaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avasaram Avasaram Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaram Avasaram Avasaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Avasaram Avasaram Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaram Avasaram Avasaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhikaram Vunnanadu Raju Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaram Vunnanadu Raju Avasaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikastha Poyinapudu Ledhu Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikastha Poyinapudu Ledhu Avasaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Bathikundaga Gundenentho Baadhapettina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bathikundaga Gundenentho Baadhapettina"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyaka Dhanda Vesi Dhandamasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyaka Dhanda Vesi Dhandamasaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avasaram Avasaram Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaram Avasaram Avasaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Avasaram Avasaram Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaram Avasaram Avasaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Avasaram Avasaram Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaram Avasaram Avasaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Avasaram Avasaram Avasaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasaram Avasaram Avasaram"/>
</div>
</pre>
