---
title: "pallello kala undhi song lyrics"
album: "Yatra"
artist: "K"
lyricist: "Sirivennela Sitaramasastri"
director: "Mahi V Raghav"
path: "/albums/yatra-lyrics"
song: "Pallello Kala Undhi"
image: ../../images/albumart/yatra.jpg
date: 2019-02-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/paZ7MjpsVjU"
type: "melody"
singers:
  - S.P. Balasubrahmanyam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pallello Kala Undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallello Kala Undhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pantallo Kalimundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pantallo Kalimundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Cheppe Matallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Cheppe Matallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Viluvemundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viluvemundhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallallo Neerundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallallo Neerundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ollantha Chamatundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ollantha Chamatundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Chemmaki Chigurinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Chemmaki Chigurinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Polamemundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamemundhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinukivvani Mabbundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinukivvani Mabbundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Malakivvani Mannundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malakivvani Mannundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuninchani Karuvundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuninchani Karuvundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkemundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkemundhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raithega Rajantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raithega Rajantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anagane Emaindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anagane Emaindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Edo Nindalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Edo Nindalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinabaduthondhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinabaduthondhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anudhinam Prathikshanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anudhinam Prathikshanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhulemivvani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhulemivvani"/>
</div>
<div class="lyrico-lyrics-wrapper">Prashnagaa Maarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prashnagaa Maarene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodavali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pairukaa Purugukaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pairukaa Purugukaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Evariki Melani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evariki Melani"/>
</div>
<div class="lyrico-lyrics-wrapper">Telusunaa Vishamayee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telusunaa Vishamayee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandukii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandukii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallello Kala Undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallello Kala Undhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pantallo Kalimundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pantallo Kalimundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Cheppe Matallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Cheppe Matallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Viluvemundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viluvemundhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallallo Neerundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallallo Neerundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ollantha Chamatundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ollantha Chamatundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Chemmaki Chigurinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Chemmaki Chigurinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Polamemundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamemundhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varivenne Virisenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varivenne Virisenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gramaseema Vaadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gramaseema Vaadithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Venne Nilichena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Venne Nilichena"/>
</div>
<div class="lyrico-lyrics-wrapper">Raithupegu Maadithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raithupegu Maadithe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nammukunna Nelatalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nammukunna Nelatalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerralugaa Cheelithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerralugaa Cheelithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammukunna Jeevalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammukunna Jeevalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Kabelaki Cherithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kabelaki Cherithe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Cheviki Vinabadavem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Cheviki Vinabadavem"/>
</div>
<div class="lyrico-lyrics-wrapper">Palle Talli Goshalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palle Talli Goshalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvariki Kanapadavem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvariki Kanapadavem"/>
</div>
<div class="lyrico-lyrics-wrapper">Chillu Padina Aasalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillu Padina Aasalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallello Kala Undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallello Kala Undhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pantallo Kalimundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pantallo Kalimundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Cheppe Matallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Cheppe Matallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Viluvemundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viluvemundhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallallo Neerundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallallo Neerundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ollantha Chamatundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ollantha Chamatundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Chemmaki Chigurinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Chemmaki Chigurinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Polamemundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamemundhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinukivvani Mabbundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinukivvani Mabbundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Malakivvani Mannundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malakivvani Mannundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuninchani Karuvundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuninchani Karuvundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkemundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkemundhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallello Kala Undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallello Kala Undhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pantallo Kalimundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pantallo Kalimundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Cheppe Matallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Cheppe Matallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Viluvemundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viluvemundhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallallo Neerundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallallo Neerundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ollantha Chamatundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ollantha Chamatundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Chemmaki Chigurinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Chemmaki Chigurinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Polamemundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Polamemundhi"/>
</div>
</pre>
