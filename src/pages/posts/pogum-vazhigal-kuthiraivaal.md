---
title: "pogum vazhigal song lyrics"
album: "Kuthiraivaal"
artist: "Pradeep Kumar"
lyricist: "Uma Devi"
director: "Manoj Leonel Jahson - Shyam Sunder"
path: "/albums/kuthiraivaal-lyrics"
song: "Pogum Vazhigal"
image: ../../images/albumart/kuthiraivaal.jpg
date: 2022-03-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SmjFGtkgKqk"
type: "happy"
singers:
  - Anthony Daasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pogum Vazhigalengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Vazhigalengum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thagam Theerkum Solai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagam Theerkum Solai Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhum Malaigal Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhum Malaigal Engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aruvi Paadum Geetham Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruvi Paadum Geetham Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aduthathai Ennamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduthathai Ennamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kidaipathai Puthinkay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaipathai Puthinkay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anuthinam Pasai Kalaiparuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuthinam Pasai Kalaiparuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanum Katchil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum Katchil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaavum Maruthey!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Maruthey!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhvin Maayam Enna?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvin Maayam Enna?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nookam Neenkiyay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nookam Neenkiyay!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yekkam Indiryay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkam Indiryay!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valum Vinthai Naane!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valum Vinthai Naane!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerchin Yennalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerchin Yennalgal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machu Vannalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machu Vannalgal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pesum Minnal Naane!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum Minnal Naane!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeril Nillungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeril Nillungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanil Sellungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanil Sellungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaavum Unmai Thane!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Unmai Thane!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oodum Pachin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodum Pachin"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paathai Sellungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathai Sellungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogum Sorgam Naane!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogum Sorgam Naane!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karchai Neekiyay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karchai Neekiyay!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paatcai Padungal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatcai Padungal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuchal Ramgam Thanay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchal Ramgam Thanay!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veil Siripilay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veil Siripilay!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Thirakiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thirakiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malai Thuliay!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Thuliay!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Thurakiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thurakiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerchin Nathi Yavume!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerchin Nathi Yavume!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalai Malai Maralam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalai Malai Maralam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanalin Nijam Yavume!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanalin Nijam Yavume!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kartil Vidai Theydalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kartil Vidai Theydalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelum Malaiaakiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelum Malaiaakiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veelum Vithaiaakiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veelum Vithaiaakiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veergal Ennai Theydalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veergal Ennai Theydalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verum Kartchil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum Kartchil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal Seeralam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Seeralam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadai! Peringu Kadai!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadai! Peringu Kadai!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaarai Nee Vaarai!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarai Nee Vaarai!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irrulinai Palagidum Pouthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irrulinai Palagidum Pouthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marinthavai Mulaithudum Thola!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marinthavai Mulaithudum Thola!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kathavugal Thiranthidum Pouthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathavugal Thiranthidum Pouthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Ondru Alaithidum Vaa! Vaa!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Ondru Alaithidum Vaa! Vaa!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aduthathai Ennamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduthathai Ennamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kidaipathai Puthinkay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaipathai Puthinkay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anuthinam Pasai Kalaiparuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuthinam Pasai Kalaiparuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanum Katchil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum Katchil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaavum Maruthey!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Maruthey!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhvin Maayam Enna?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvin Maayam Enna?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valuam Sattangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valuam Sattangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vallvin Vattangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallvin Vattangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaavum Melvathenna!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum Melvathenna!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niyayam Indriyay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyayam Indriyay"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niyayam Ondraya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyayam Ondraya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nengal Kanpathena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nengal Kanpathena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarum Ondirlay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarum Ondirlay"/>
</div>
<div class="lyrico-lyrics-wrapper">Naamum Ondrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naamum Ondrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthal Inbam Thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthal Inbam Thane"/>
</div>
</pre>
