---
title: "needhi nadhantu song lyrics"
album: "Tuck Jagadish"
artist: "S. Thaman"
lyricist: "Kalyan Chakravarthy"
director: "Shiva Nirvana"
path: "/albums/tuck-jagadish-lyrics"
song: "Needhi Nadhantu"
image: ../../images/albumart/tuck-jagadish.jpg
date: 2021-09-10
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wsUSJ5pbitE"
type: "happy"
singers:
  - Sri Krishna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Needi naadantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needi naadantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvu geetha daati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvu geetha daati"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavali neelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavali neelona"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishwa naruni reethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishwa naruni reethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needi naadantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needi naadantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvu geetha daati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvu geetha daati"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavali neelona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavali neelona"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishwa naruni reethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishwa naruni reethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Talupule karigi eevela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talupule karigi eevela"/>
</div>
<div class="lyrico-lyrics-wrapper">Molakaale veliki raavala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molakaale veliki raavala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalathaale chediri poyeela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalathaale chediri poyeela"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamathaale bathuku nindelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamathaale bathuku nindelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo chigurumani kacheri chesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo chigurumani kacheri chesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacchindi choodu darjaaga sarkaare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacchindi choodu darjaaga sarkaare"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo chamatathadi challaripoga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo chamatathadi challaripoga"/>
</div>
<div class="lyrico-lyrics-wrapper">Challangaa veeche mellanga neepainele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challangaa veeche mellanga neepainele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodabuttani thodu neevura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodabuttani thodu neevura"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema panchina katha needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema panchina katha needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chamma cherani gunde gutiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamma cherani gunde gutiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuna tecchinaa katha needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuna tecchinaa katha needhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raatha raasina brahma raathani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raatha raasina brahma raathani"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarchivesinaa katha needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarchivesinaa katha needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Daarithappinaa chanti biddaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daarithappinaa chanti biddaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma cheppinaa katha needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma cheppinaa katha needhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodabuttani thodu neevura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodabuttani thodu neevura"/>
</div>
<div class="lyrico-lyrics-wrapper">Prema panchina katha needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prema panchina katha needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chamma cherani gunde gutiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamma cherani gunde gutiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuna tecchina katha needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuna tecchina katha needhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raatha raasina brahma raathani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raatha raasina brahma raathani"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarchivesina katha needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarchivesina katha needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Daarithappina chanti biddaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daarithappina chanti biddaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma cheppina katha needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma cheppina katha needhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Needi naadantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Needi naadantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Niluvu geetha daati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niluvu geetha daati"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavali neelonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavali neelonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vishwaa naruni reethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vishwaa naruni reethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neepai gelavalani chera nishini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neepai gelavalani chera nishini"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeke teliyalani daacha nusini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeke teliyalani daacha nusini"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkanali thoduga maate nilichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkanali thoduga maate nilichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeke kanupapala paate avani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeke kanupapala paate avani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Visirithe paasamedhaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visirithe paasamedhaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethikithe velaku dhorikena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethikithe velaku dhorikena"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadabade adugu needhaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadabade adugu needhaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilabade varaku nenunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilabade varaku nenunnaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo prathi okadi punadhilona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo prathi okadi punadhilona"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnadulera cheeddodu manchodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnadulera cheeddodu manchodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo telusukoni manninchi chudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo telusukoni manninchi chudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee anthatodu inkevvaduntadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee anthatodu inkevvaduntadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodabuttani thodu neevuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodabuttani thodu neevuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Premaa panchina katha needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premaa panchina katha needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chamma cherani gunde gutiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamma cherani gunde gutiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuna tecchinaa katha needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuna tecchinaa katha needhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raatha raasina brahma raathani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raatha raasina brahma raathani"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarchivesinaa katha needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarchivesinaa katha needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Daarithappinaa chanti biddaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daarithappinaa chanti biddaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma cheppinaa katha needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma cheppinaa katha needhi"/>
</div>
</pre>
