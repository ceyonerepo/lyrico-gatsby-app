---
title: "it’s my kinda day song lyrics"
album: "Oh My Dog"
artist: "Nivas K Prasanna"
lyricist: "Super Subu"
director: "Sarov Shanmugam"
path: "/albums/oh-my-dog-lyrics"
song: "It’s My Kinda Day"
image: ../../images/albumart/oh-my-dog.jpg
date: 2022-04-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/1RzRgHjA-nw"
type: "happy"
singers:
  - Ajeesh Sivakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Lotta time i gotta play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lotta time i gotta play"/>
</div>
<div class="lyrico-lyrics-wrapper">Lotta stories to say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lotta stories to say"/>
</div>
<div class="lyrico-lyrics-wrapper">Boy i tell you its my kinda day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boy i tell you its my kinda day"/>
</div>
<div class="lyrico-lyrics-wrapper">Lotta memories to make
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lotta memories to make"/>
</div>
<div class="lyrico-lyrics-wrapper">Lotta theories to break
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lotta theories to break"/>
</div>
<div class="lyrico-lyrics-wrapper">Bruh i tell you thats my kinda day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bruh i tell you thats my kinda day"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey rule ellaam illaadha veedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey rule ellaam illaadha veedu"/>
</div>
<div class="lyrico-lyrics-wrapper">School ellaam illaadha ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="School ellaam illaadha ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru naal kedacha its my kinda day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru naal kedacha its my kinda day"/>
</div>
<div class="lyrico-lyrics-wrapper">En toys ellaam ennoda aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En toys ellaam ennoda aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Birds ellaam ennoda paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Birds ellaam ennoda paadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenachu paathaa thats my kinda day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenachu paathaa thats my kinda day"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey vambu valappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey vambu valappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanda pudipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda pudipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu kudupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu kudupen"/>
</div>
<div class="lyrico-lyrics-wrapper">That’s how i play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That’s how i play"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam padipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam padipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam nadipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam nadipen"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam odaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam odaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">That’s how i slay it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That’s how i slay it"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I just love my life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I just love my life"/>
</div>
<div class="lyrico-lyrics-wrapper">I just love my life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I just love my life"/>
</div>
<div class="lyrico-lyrics-wrapper">I just love my life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I just love my life"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Komba dhaan maattivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komba dhaan maattivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaala dhaan aattivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaala dhaan aattivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda fun u panna odiyaa odiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda fun u panna odiyaa odiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eriend ellaam koottikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eriend ellaam koottikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu dhaa kaattikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu dhaa kaattikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda fun-u panna odiyaa odiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda fun-u panna odiyaa odiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh its my kinda day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh its my kinda day"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ink ellaam thelichittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ink ellaam thelichittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onaanellaam pudichittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onaanellaam pudichittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Setta pannum neram idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setta pannum neram idhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaalellaam suruttitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaalellaam suruttitu"/>
</div>
<div class="lyrico-lyrics-wrapper">Landhu ellam adakitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Landhu ellam adakitu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga sonna enna naanga seiradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga sonna enna naanga seiradhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">So many dreams
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So many dreams"/>
</div>
<div class="lyrico-lyrics-wrapper">Too many hopes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too many hopes"/>
</div>
<div class="lyrico-lyrics-wrapper">So many worlds
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So many worlds"/>
</div>
<div class="lyrico-lyrics-wrapper">Too many hopes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too many hopes"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">School ellaam ennoda gang u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="School ellaam ennoda gang u"/>
</div>
<div class="lyrico-lyrics-wrapper">Class ellaam pattaasu bang u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Class ellaam pattaasu bang u"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluthi potta thats my kinda day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthi potta thats my kinda day"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey suru suruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey suru suruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Para parappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Para parappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Semma sirippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma sirippu"/>
</div>
<div class="lyrico-lyrics-wrapper">That’s how we rule it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That’s how we rule it"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam kozhupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam kozhupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam mathippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam mathippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam poruppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam poruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">That’s how we rule it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="That’s how we rule it"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I just love my life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I just love my life"/>
</div>
<div class="lyrico-lyrics-wrapper">Komba dhaan maattivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komba dhaan maattivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaala dhaan aattivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaala dhaan aattivittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I just love my life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I just love my life"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda fun u panna odiyaa odiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda fun u panna odiyaa odiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend ellaam koottikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend ellaam koottikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu dhaa kaattikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu dhaa kaattikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda fun u panna odiyaa odiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda fun u panna odiyaa odiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lotta time i gotta play
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lotta time i gotta play"/>
</div>
<div class="lyrico-lyrics-wrapper">Lotta stories to say
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lotta stories to say"/>
</div>
<div class="lyrico-lyrics-wrapper">Boy i tell you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boy i tell you"/>
</div>
<div class="lyrico-lyrics-wrapper">Thats my kinda day
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thats my kinda day"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Komba dhaan maattivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komba dhaan maattivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaala dhaan aattivittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaala dhaan aattivittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda fun u panna odiyaa odiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda fun u panna odiyaa odiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Friend ellaam koottikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend ellaam koottikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gethu dhaa kaattikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gethu dhaa kaattikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda fun u panna odiyaa odiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda fun u panna odiyaa odiyaa"/>
</div>
</pre>
