---
title: 'nee partha vizhigal song lyrics'
album: '3'
artist: 'Anirudh Ravichander'
lyricist: 'Dhanush'
director: 'Soundharya Rajinikanth'
path: '/albums/3-song-lyrics'
song: 'Nee Partha Vizhigal'
image: ../../images/albumart/3.jpg
date: 2012-03-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_IuJsaMqtqc"
type: 'duet'
singers: 
- Vijay Yesudas
- Shweta Mohan
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
    <div class="lyrico-lyrics-wrapper">Nee partha vizhigal..nee partha nodigal
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee partha vizhigal..nee partha nodigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm…kettaalum varuma..ketkaadha varama
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm…kettaalum varuma..ketkaadha varama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idhu pothuma..idhil avasarama
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu pothuma..idhil avasarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum venduma…adhil nirainthiduma
<input type="checkbox" class="lyrico-select-lyric-line" value="Innum venduma…adhil nirainthiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam paarthanaal nam vasam varuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Naam paarthanaal nam vasam varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thaanguma en vizhigalil mudhal vali
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyir thaanguma en vizhigalil mudhal vali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nijamadi pennae tholaivinil unnai
<input type="checkbox" class="lyrico-select-lyric-line" value="Nijamadi pennae tholaivinil unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavinil kanden nadamaada
<input type="checkbox" class="lyrico-select-lyric-line" value="Nilavinil kanden nadamaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiyadi pennae varaimurai illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Valiyadi pennae varaimurai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhaikkiraai ennai medhuvaaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Vadhaikkiraai ennai medhuvaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee partha vizhigal..nee partha nodigal
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee partha vizhigal..nee partha nodigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm…kettaalum varuma..ketkaadha varama
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm…kettaalum varuma..ketkaadha varama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nizhal tharum ival paarvai
<input type="checkbox" class="lyrico-select-lyric-line" value="Nizhal tharum ival paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi engum ini thevai
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazhi engum ini thevai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirae uyirae uyir nee than endraal
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirae uyirae uyir nee than endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Udanae varuvaai udal saagum munnaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Udanae varuvaai udal saagum munnaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Analindri kulir veesum
<input type="checkbox" class="lyrico-select-lyric-line" value="Analindri kulir veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu enthan sirai vasam
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithu enthan sirai vasam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ithil nee mattum vendum pennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithil nee mattum vendum pennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nijamadi pennae tholaivinil unnai
<input type="checkbox" class="lyrico-select-lyric-line" value="Nijamadi pennae tholaivinil unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavinil kanden nadamaada
<input type="checkbox" class="lyrico-select-lyric-line" value="Nilavinil kanden nadamaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Valiyadi pennae varaimurai illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Valiyadi pennae varaimurai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhaikkiraai ennai medhuvaaga
<input type="checkbox" class="lyrico-select-lyric-line" value="Vadhaikkiraai ennai medhuvaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee partha vizhigal..nee partha nodigal
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee partha vizhigal..nee partha nodigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm…kettaalum varuma..ketkaadha varama
<input type="checkbox" class="lyrico-select-lyric-line" value="Hmm…kettaalum varuma..ketkaadha varama"/>
</div>
  <div class="lyrico-lyrics-wrapper">Idhu pothuma..idhil avasarama
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu pothuma..idhil avasarama"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum venduma…adhil nirainthiduma
<input type="checkbox" class="lyrico-select-lyric-line" value="Innum venduma…adhil nirainthiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam paarthanaal nam vasam varuma
<input type="checkbox" class="lyrico-select-lyric-line" value="Naam paarthanaal nam vasam varuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir thaanguma…ohhooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyir thaanguma…ohhooo"/>
</div>
</pre>