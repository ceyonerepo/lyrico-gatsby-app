---
title: "naana thaana song lyrics"
album: "Thaanaa Serndha Koottam"
artist: "Anirudh Ravichander"
lyricist: "Vignesh Shivan"
director: "Vignesh Shivan"
path: "/albums/thaanaa-serndha-koottam-lyrics"
song: "Naana Thaana"
image: ../../images/albumart/thaanaa-serndha-koottam.jpg
date: 2018-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/qlOPl2uxKbs"
type: "love"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oru pattamboochiya utta paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pattamboochiya utta paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettadha dhoorathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettadha dhoorathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pattamboochiya utta paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pattamboochiya utta paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettadha dhoorathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettadha dhoorathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey naana thaana veena ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey naana thaana veena ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyae illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyae illayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada aana oona kaana ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aana oona kaana ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyae illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyae illayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kutti size-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kutti size-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Buss-u vaanam koluthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buss-u vaanam koluthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju naduvula niruthitta oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju naduvula niruthitta oruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pattamboochiya utta paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pattamboochiya utta paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettadha dhoorathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettadha dhoorathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kattu koppanavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kattu koppanavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaan kadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaan kadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja dhoosi thatti eduthu dhaan niruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja dhoosi thatti eduthu dhaan niruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo nera ulla vandhu dera poda pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo nera ulla vandhu dera poda pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaaralama oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaaralama oruthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaeee munnala porava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaeee munnala porava"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaalaa paakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaalaa paakaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaeee andha kaanaala paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaeee andha kaanaala paakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Saakkulla thaakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakkulla thaakaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaeee munnala porava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaeee munnala porava"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaalaa paakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaalaa paakaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaeee aeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaeee aeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaala thaakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaala thaakaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey naana thaana huh huh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey naana thaana huh huh"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana thaana veena ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana thaana veena ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyae illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyae illayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada aana oona kaana ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aana oona kaana ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyae illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyae illayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kitta thatta keranguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta thatta keranguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Keela eranguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keela eranguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam kolambi tholayuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam kolambi tholayuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edaagooda nelivaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edaagooda nelivaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava polivayyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava polivayyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathu polambi tholayuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathu polambi tholayuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summavae sirikkirenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summavae sirikkirenn"/>
</div>
<div class="lyrico-lyrics-wrapper">Summavae sirikkirenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summavae sirikkirenn"/>
</div>
<div class="lyrico-lyrics-wrapper">Mela parakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mela parakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Neja vayasa marakkurenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neja vayasa marakkurenn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konaala dhaan nadakkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konaala dhaan nadakkuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollam kirukuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollam kirukuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana manasa maraikkuren aann aann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aana manasa maraikkuren aann aann"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaeee munnala porava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaeee munnala porava"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaalaa paakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaalaa paakaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaeee andha kaanaala paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaeee andha kaanaala paakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Saakkulla thaakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakkulla thaakaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaeee munnala porava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaeee munnala porava"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaalaa paakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaalaa paakaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaeee aeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaeee aeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaala thaakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaala thaakaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey naana thaana huh huh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey naana thaana huh huh"/>
</div>
<div class="lyrico-lyrics-wrapper">Naana thaana veena ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana thaana veena ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sariyae illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sariyae illayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada aana oona kaana ponaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aana oona kaana ponaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhiyae illayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhiyae illayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kutti size-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kutti size-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Buss-u vaanam koluthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buss-u vaanam koluthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenju naduvula niruthitta oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenju naduvula niruthitta oruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pattamboochiya utta paaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pattamboochiya utta paaruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettadha dhoorathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettadha dhoorathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kattu koppanavanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kattu koppanavanana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaan kadathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaan kadathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja dhoosi thatti edthu dhaan niruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja dhoosi thatti edthu dhaan niruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo nera ulla vandhu dera poda pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo nera ulla vandhu dera poda pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaaralama oruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaaralama oruthi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaeee munnala porava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaeee munnala porava"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaalaa paakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaalaa paakaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaeee andha kaanaala paakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaeee andha kaanaala paakura"/>
</div>
<div class="lyrico-lyrics-wrapper">Saakkulla thaakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saakkulla thaakaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaeee munnala porava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaeee munnala porava"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnaalaa paakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnaalaa paakaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaeee aeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaeee aeee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanaala thaakaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanaala thaakaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyaeee azhagaeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyaeee azhagaeee"/>
</div>
</pre>
