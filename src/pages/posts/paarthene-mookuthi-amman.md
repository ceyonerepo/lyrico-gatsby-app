---
title: "Paarthene lyrics"
album: "Mookuthi Amman"
artist: "Girishh Gopalakrishnan"
lyricist: "Pa Vijay"
director: "RJ Balaji  & NJ Saravanan"
path: "/albums/mookuthi-amman-song-lyrics"
song: "Paarthene (Amman Song)"
image: ../../images/albumart/mookuthi-amman.jpg
date: 2020-11-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EEs4mY7YpTU"
type: "devotional"
singers:
  - Jairam Balasubramanian
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paarthene uyirin vazhiye
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarthene uyirin vazhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar kannum kaana mugame
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar kannum kaana mugame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal endru ninaithen unaiye
<input type="checkbox" class="lyrico-select-lyric-line" value="Kal endru ninaithen unaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yaar endru sonnai maname thaan neeya
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee yaar endru sonnai maname thaan neeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethil nee irunthai
<input type="checkbox" class="lyrico-select-lyric-line" value="Ethil nee irunthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Engo marainthai
<input type="checkbox" class="lyrico-select-lyric-line" value="Engo marainthai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai thedi azhainthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Unai thedi azhainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakulle therinthai
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakulle therinthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu podhum enakku veru varangal nooru venduma
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu podhum enakku veru varangal nooru venduma"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraiva idhu thaan niraiva
<input type="checkbox" class="lyrico-select-lyric-line" value="Iraiva idhu thaan niraiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarthen unaiye unaiye
<input type="checkbox" class="lyrico-select-lyric-line" value="Unarthen unaiye unaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthen enaiye enaiye
<input type="checkbox" class="lyrico-select-lyric-line" value="Maranthen enaiye enaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthene uyirin vazhiye
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarthene uyirin vazhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar kannum kaana mugame
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar kannum kaana mugame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal endru ninaithen unaiye
<input type="checkbox" class="lyrico-select-lyric-line" value="Kal endru ninaithen unaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yaar endru sonnai maname thaan neeya
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee yaar endru sonnai maname thaan neeya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedhangal moththam modhi
<input type="checkbox" class="lyrico-select-lyric-line" value="Vedhangal moththam modhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaagangal niththam seithu
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaagangal niththam seithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poojaikkum bhakthi athilum
<input type="checkbox" class="lyrico-select-lyric-line" value="Poojaikkum bhakthi athilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai kaanalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasi endru than mun vanthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pasi endru than mun vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai yenthi ketkum bodhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kai yenthi ketkum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Than unavai thanthal kooda
<input type="checkbox" class="lyrico-select-lyric-line" value="Than unavai thanthal kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kaanalam
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnai kaanalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai kaana pazha kodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Unai kaana pazha kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu vaari iraikirargal
<input type="checkbox" class="lyrico-select-lyric-line" value="Ingu vaari iraikirargal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhithaga unai sehra
<input type="checkbox" class="lyrico-select-lyric-line" value="Ezhithaga unai sehra"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu yaaru ninaikirargal
<input type="checkbox" class="lyrico-select-lyric-line" value="Ingu yaaru ninaikirargal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhangaram athil nee illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Azhangaram athil nee illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Agangaram manathil illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Agangaram manathil illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli kallam kabadam kalanthidatha anbil irukkirai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thulli kallam kabadam kalanthidatha anbil irukkirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unarthen unaiye unaiye
<input type="checkbox" class="lyrico-select-lyric-line" value="Unarthen unaiye unaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranthen enaiye enaiye
<input type="checkbox" class="lyrico-select-lyric-line" value="Maranthen enaiye enaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Agam nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Agam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Jagam nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Jagam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuvaana ulagin agalam nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Anuvaana ulagin agalam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumbin idhaya ozhi nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Erumbin idhaya ozhi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalirin thudikai ganamum nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalirin thudikai ganamum nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram kai undu endral
<input type="checkbox" class="lyrico-select-lyric-line" value="Aayiram kai undu endral"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee oru kai thara koodatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee oru kai thara koodatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeraayiram kan kondai
<input type="checkbox" class="lyrico-select-lyric-line" value="Eeraayiram kan kondai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un oru kan enai paaratha
<input type="checkbox" class="lyrico-select-lyric-line" value="Un oru kan enai paaratha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil saran adainthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnil saran adainthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini nee gadhiye
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini nee gadhiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthene uyirin vazhiye
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarthene uyirin vazhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar kannum kaana mugame
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaar kannum kaana mugame"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal endru ninaithen unaiye
<input type="checkbox" class="lyrico-select-lyric-line" value="Kal endru ninaithen unaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yaar endru sonnai maname thaan neeya
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee yaar endru sonnai maname thaan neeya"/>
</div>
</pre>
