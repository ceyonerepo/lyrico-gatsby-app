---
title: "mayakkuraane song lyrics"
album: "Annanukku Jai"
artist: "Arrol Corelli"
lyricist: "Rajkumar"
director: "Rajkumar"
path: "/albums/annanukku-jai-lyrics"
song: "Mayakkuraane"
image: ../../images/albumart/annanukku-jai.jpg
date: 2018-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GwbAOb4CpNA"
type: "love"
singers:
  - Andrea Jeremiah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Mayakkuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaikkuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaikkuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidama sagasam pannuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidama sagasam pannuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Viththa kaaturan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viththa kaaturan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rasikkiraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikkiraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuppuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuppuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En veli thandiyae seenduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veli thandiyae seenduran"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala thookkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala thookkuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanjaadayil kal veesuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanjaadayil kal veesuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallaaduthae thaangamalae manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallaaduthae thaangamalae manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaikkuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaikkuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidama sagasam pannuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidama sagasam pannuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Viththa kaaturan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viththa kaaturan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rasikkiraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikkiraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuppuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuppuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En veli thandiyae seenduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veli thandiyae seenduran"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala thookkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala thookkuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soora kaathu veesiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soora kaathu veesiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaagitha kappala aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagitha kappala aaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaatta saattamaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaatta saattamaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaliban paarvayil saayuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaliban paarvayil saayuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna thee mootta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna thee mootta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatu paiyan kaiyil naan maatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatu paiyan kaiyil naan maatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna pinnanu pathi yeriyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna pinnanu pathi yeriyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Settai nooru seyyura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Settai nooru seyyura"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkava moraikkava thalaiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkava moraikkava thalaiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoondil pottu izhukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondil pottu izhukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhagava thediyae varavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhagava thediyae varavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koobam thaabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koobam thaabam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai kondatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai kondatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthu thaanae kaadhal undaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthu thaanae kaadhal undaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam unnalae inbam thunbamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam unnalae inbam thunbamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaikkuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaikkuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidama sagasam pannuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidama sagasam pannuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Viththa kaaturan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viththa kaaturan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rasikkiraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikkiraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuppuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuppuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En veli thandiyae seenduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veli thandiyae seenduran"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala thookkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala thookkuran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanjaadayil kal veesuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanjaadayil kal veesuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallaaduthae thaangaamalae manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallaaduthae thaangaamalae manam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaikkuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaikkuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidama sagasam pannuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidama sagasam pannuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Viththa kaaturan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viththa kaaturan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rasikkiraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikkiraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuppuraanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuppuraanae"/>
</div>
<div class="lyrico-lyrics-wrapper">En veli thandiyae seenduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En veli thandiyae seenduran"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala thookkuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala thookkuran"/>
</div>
</pre>
