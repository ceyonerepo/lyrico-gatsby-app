---
title: 'maathare song lyrics'
album: 'Bigil'
artist: 'A R Rahman'
lyricist: 'Vivek'
director: 'Atlee'
path: '/albums/bigil-song-lyrics'
song: 'Maathare'
image: ../../images/albumart/bigil.jpg
date: 2019-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AWWKiVp8cTU"
type: 'motivation'
singers: 
- A R Rahman
- Chinmayi
- Madhura Dhara Thalluri
- Vithusayini
- Akshara
- Sireesha 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Maathare maathare maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Maathare maathare maathare"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathare maathare maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Maathare maathare maathare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangaiyae mangaiyae maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Mangaiyae mangaiyae maathare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangaiyae mangaiyae maathare (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Mangaiyae mangaiyae maathare"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaadhal thammai izhivu seiyium
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal thammai izhivu seiyium"/>
</div>
<div class="lyrico-lyrics-wrapper">Madamai kolutha sabatham seidhom
<input type="checkbox" class="lyrico-select-lyric-line" value="Madamai kolutha sabatham seidhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Indro madamai valakkirom
<input type="checkbox" class="lyrico-select-lyric-line" value="Indro madamai valakkirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadhar udal dhaan koluthinom
<input type="checkbox" class="lyrico-select-lyric-line" value="Maadhar udal dhaan koluthinom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aaanin ulagil visira pattom
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaanin ulagil visira pattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounam pesa padaikka pattom
<input type="checkbox" class="lyrico-select-lyric-line" value="Mounam pesa padaikka pattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalavae illa viduthalai
<input type="checkbox" class="lyrico-select-lyric-line" value="Aalavae illa viduthalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaanal iravaagum nodivarai
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaanal iravaagum nodivarai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 &   <div class="lyrico-lyrics-wrapper">Maathare maathare maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Maathare maathare maathare"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathare maathare maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Maathare maathare maathare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangaiyae mangaiyae maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Mangaiyae mangaiyae maathare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangaiyae mangaiyae maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Mangaiyae mangaiyae maathare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Maarbagam pola endhan
<input type="checkbox" class="lyrico-select-lyric-line" value="Maarbagam pola endhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manathirkkoru uruvam
<input type="checkbox" class="lyrico-select-lyric-line" value="Manathirkkoru uruvam"/>
</div>
<div class="lyrico-lyrics-wrapper">Irunthirunthaal dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Irunthirunthaal dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaninam paarthidum adhaiyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Aaninam paarthidum adhaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadhigalin pergalilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nadhigalin pergalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazha vidum kootathilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vazha vidum kootathilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poruthiduvai manamae poruthidumae
<input type="checkbox" class="lyrico-select-lyric-line" value="Poruthiduvai manamae poruthidumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 &   <div class="lyrico-lyrics-wrapper">Maathare maathare maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Maathare maathare maathare"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathare maathare maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Maathare maathare maathare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangaiyae mangaiyae maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Mangaiyae mangaiyae maathare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangaiyae mangaiyae maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Mangaiyae mangaiyae maathare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kannaal urasigiraar
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannaal urasigiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Balam kondu nasukkugirar
<input type="checkbox" class="lyrico-select-lyric-line" value="Balam kondu nasukkugirar"/>
</div>
<div class="lyrico-lyrics-wrapper">Valimai varam enavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Valimai varam enavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesaiyai ettrugiraar
<input type="checkbox" class="lyrico-select-lyric-line" value="Meesaiyai ettrugiraar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aanmai adhu meesai mudi
<input type="checkbox" class="lyrico-select-lyric-line" value="Aanmai adhu meesai mudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Orathilae poopathillai
<input type="checkbox" class="lyrico-select-lyric-line" value="Orathilae poopathillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennai ne ganniyamaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Pennai ne ganniyamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarpathilae thulirukkum (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Paarpathilae thulirukkum"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 &   <div class="lyrico-lyrics-wrapper">Maathare maathare maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Maathare maathare maathare"/>
</div>
<div class="lyrico-lyrics-wrapper">Maathare maathare maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Maathare maathare maathare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangaiyae mangaiyae maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Mangaiyae mangaiyae maathare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangaiyae mangaiyae maathare
<input type="checkbox" class="lyrico-select-lyric-line" value="Mangaiyae mangaiyae maathare"/>
</div>
</pre>