---
title: "nimirnthu nill song lyrics"
album: "Chasing"
artist: "Thashi"
lyricist: "Muthulingam"
director: "Veerakumar"
path: "/albums/chasing-song-lyrics"
song: "Nimirthu Nill"
image: ../../images/albumart/chasing.jpg
date: 2021-04-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/CVQe2UFbTVk"
type: "Motivational"
singers:
  - Kalpana
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nimirndhu nill thunindhu sell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimirndhu nill thunindhu sell"/>
</div>
<div class="lyrico-lyrics-wrapper">nimirndhu nill thunindhu sell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimirndhu nill thunindhu sell"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagai vell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagai vell"/>
</div>
<div class="lyrico-lyrics-wrapper">nimirndhu nill thunindhu sell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimirndhu nill thunindhu sell"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagai vell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagai vell"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethilum thuninthu selluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethilum thuninthu selluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">ethaiyum ulagil velluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethaiyum ulagil velluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">sattam olungai kaappom endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattam olungai kaappom endru"/>
</div>
<div class="lyrico-lyrics-wrapper">savaal vitu solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savaal vitu solluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">ethilum thuninthu selluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethilum thuninthu selluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">ethaiyum ulagil velluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethaiyum ulagil velluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">sattam olungai kaappom endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattam olungai kaappom endru"/>
</div>
<div class="lyrico-lyrics-wrapper">savaal vitu solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savaal vitu solluvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaaval thuraiyin perumai thannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaval thuraiyin perumai thannai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathu nirppom endrume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathu nirppom endrume"/>
</div>
<div class="lyrico-lyrics-wrapper">kadamai kanniyam kattuppatai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadamai kanniyam kattuppatai"/>
</div>
<div class="lyrico-lyrics-wrapper">potrum namathu nenjame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potrum namathu nenjame"/>
</div>
<div class="lyrico-lyrics-wrapper">puliyai pola veeram ullathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puliyai pola veeram ullathu"/>
</div>
<div class="lyrico-lyrics-wrapper">poovai engal ullame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovai engal ullame"/>
</div>
<div class="lyrico-lyrics-wrapper">kanuvu kaati uthavum gunathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanuvu kaati uthavum gunathil"/>
</div>
<div class="lyrico-lyrics-wrapper">karunai pongum vellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karunai pongum vellame"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaiyai inge udaikave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaiyai inge udaikave"/>
</div>
<div class="lyrico-lyrics-wrapper">sarithirangal padaikave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarithirangal padaikave"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu yugathai ellupa vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu yugathai ellupa vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">purachi pengal naangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="purachi pengal naangale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethilum thuninthu selluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethilum thuninthu selluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">ethaiyum ulagil velluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethaiyum ulagil velluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">sattam olungai kaappom endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattam olungai kaappom endru"/>
</div>
<div class="lyrico-lyrics-wrapper">savaal vitu solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savaal vitu solluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">sattam olungai kaappom endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattam olungai kaappom endru"/>
</div>
<div class="lyrico-lyrics-wrapper">savaal vitu solluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savaal vitu solluvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veetai aalum nangaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veetai aalum nangaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">naatai naalai aalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naatai naalai aalume"/>
</div>
<div class="lyrico-lyrics-wrapper">jansi rani velu nachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jansi rani velu nachi"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhkai namathu paadame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhkai namathu paadame"/>
</div>
<div class="lyrico-lyrics-wrapper">kutram seiyum kootangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutram seiyum kootangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">kottam panni adakuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottam panni adakuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">nermai thavarum nenjangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nermai thavarum nenjangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">ner valiyil thirupuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ner valiyil thirupuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">moodanuku thandanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moodanuku thandanai"/>
</div>
<div class="lyrico-lyrics-wrapper">koduka vendum nichayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koduka vendum nichayam"/>
</div>
<div class="lyrico-lyrics-wrapper">oruvar kooda thappi sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oruvar kooda thappi sella"/>
</div>
<div class="lyrico-lyrics-wrapper">uthava maattom sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthava maattom sathiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethilum thuninthu selluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethilum thuninthu selluvom"/>
</div>
<div class="lyrico-lyrics-wrapper">ethaiyum ulagil velluvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethaiyum ulagil velluvom"/>
</div>
</pre>
