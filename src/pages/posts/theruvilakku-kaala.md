---
title: "theruvilakku song lyrics"
album: "Kaala"
artist: "Santhosh Narayanan"
lyricist: "Dopeadelicz - Logan"
director: "Pa. Ranjith"
path: "/albums/kaala-lyrics"
song: "Theruvilakku"
image: ../../images/albumart/kaala.jpg
date: 2018-06-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/tfVIO71Pbgc"
type: "mass"
singers:
  - Dopeadelicz
  - Muthamil
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Theru velakku velachutula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru velakku velachutula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga munneri varuvoom uyarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga munneri varuvoom uyarathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndha kooda varumaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndha kooda varumaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaaru padaipom intha nagarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaaru padaipom intha nagarathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theru velakku velachutula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru velakku velachutula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga munneri varuvoom uyarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga munneri varuvoom uyarathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndha kooda varumaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndha kooda varumaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaaru padaipom intha nagarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaaru padaipom intha nagarathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Welcome to my hood
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Welcome to my hood"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithuthanae namma idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuthanae namma idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhukku sakkadai irundhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhukku sakkadai irundhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga manam sutha manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga manam sutha manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu podathae namma idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu podathae namma idam"/>
</div>
<div class="lyrico-lyrics-wrapper">City-ila naduvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="City-ila naduvula"/>
</div>
<div class="lyrico-lyrics-wrapper">Jathi matham ellam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jathi matham ellam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna vaazhuvomae theruvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna vaazhuvomae theruvula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kondaattam nikkaathu inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondaattam nikkaathu inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Hip hop kalachaaram ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hip hop kalachaaram ingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnal mudiyum thozha balathoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnal mudiyum thozha balathoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada melae melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada melae melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevvurukullae paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvurukullae paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala oviyamthaan unnai kattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala oviyamthaan unnai kattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paakkuriyae namma idathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paakkuriyae namma idathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala maatram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala maatram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veetukkul irundhaal pulambi pulambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetukkul irundhaal pulambi pulambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundha mattum pattathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irundha mattum pattathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkenna thevai thozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkenna thevai thozha"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaiyum neeyum endrum marakaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaiyum neeyum endrum marakaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Munna dhan neeyum vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munna dhan neeyum vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un idam dhan munnae varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un idam dhan munnae varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrumaiyaga ninnu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrumaiyaga ninnu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Athupola illa oru balam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athupola illa oru balam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En raagam thaniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En raagam thaniyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaiyoda kettuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaiyoda kettuko"/>
</div>
<div class="lyrico-lyrics-wrapper">En paadal unnidathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En paadal unnidathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanban phone-il kettukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban phone-il kettukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Puratchiyaa pesum bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puratchiyaa pesum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha neeyum kettuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha neeyum kettuko"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikkai illaiyendral ingaeVanthu paathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikkai illaiyendral ingaeVanthu paathukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theru velakku velachutula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru velakku velachutula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga munneri varuvoom uyarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga munneri varuvoom uyarathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndha kooda varumaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndha kooda varumaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaaru padaipom intha nagarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaaru padaipom intha nagarathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theru velakku velachutula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru velakku velachutula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga munneri varuvoom uyarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga munneri varuvoom uyarathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndha kooda varumaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndha kooda varumaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaaru padaipom intha nagarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaaru padaipom intha nagarathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala kuninja kaalam pooyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala kuninja kaalam pooyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai nimithi ippa vaazhuromla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai nimithi ippa vaazhuromla"/>
</div>
<div class="lyrico-lyrics-wrapper">Othumaiya irukkura varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othumaiya irukkura varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru uthaviyum thevai illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru uthaviyum thevai illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maadi mela maadi illattiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadi mela maadi illattiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna iruppom therukkodiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna iruppom therukkodiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaippa nambi vaazhura naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaippa nambi vaazhura naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathi paakkura saathi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathi paakkura saathi illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samathuvam piranthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samathuvam piranthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Viduthalai kidaithida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduthalai kidaithida"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozha kaiyinainthu pooradu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha kaiyinainthu pooradu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal palithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal palithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarithiram thiruthuvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarithiram thiruthuvom"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarnthu nirkkum ini naam naadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarnthu nirkkum ini naam naadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unavu aadai veedu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unavu aadai veedu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalviyum unathu adippadai thevai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalviyum unathu adippadai thevai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athai nee unarnthu padithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athai nee unarnthu padithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paritchai mudithu yeru medai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paritchai mudithu yeru medai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vangu kaithattal parisu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangu kaithattal parisu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theru velakku velachutula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru velakku velachutula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga munneri varuvoom uyarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga munneri varuvoom uyarathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndha kooda varumaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndha kooda varumaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaaru padaipom intha nagarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaaru padaipom intha nagarathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theru velakku velachutula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theru velakku velachutula"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga munneri varuvoom uyarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga munneri varuvoom uyarathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhndha kooda varumaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhndha kooda varumaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Varalaaru padaipom intha nagarathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varalaaru padaipom intha nagarathula"/>
</div>
</pre>
