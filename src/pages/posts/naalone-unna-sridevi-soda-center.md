---
title: "naalone unna song lyrics"
album: "Sridevi Soda Center"
artist: "Mani Sharma"
lyricist: "Kalyan Chakravarthy"
director: "Karuna Kumar"
path: "/albums/sridevi-soda-center-lyrics"
song: "Naalone Unna"
image: ../../images/albumart/sridevi-soda-center.jpg
date: 2021-08-27
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Huma1G1r2k8"
type: "melody"
singers:
  - Anurag Kulakarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naalone unna neelone nena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalone unna neelone nena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee dhooram inka nammalekunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee dhooram inka nammalekunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethone unna ninna monna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethone unna ninna monna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhanna maata nedu nijamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhanna maata nedu nijamena"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimishalu leni kalam edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimishalu leni kalam edho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nirasincha leka neerasinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirasincha leka neerasinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Nisileni tella cheekatedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nisileni tella cheekatedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Chithi leka manta lekane rachinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chithi leka manta lekane rachinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye karagaram kanaledhu ee dooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye karagaram kanaledhu ee dooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanarani neram kaalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanarani neram kaalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye geetha saram vinipinchani vairam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye geetha saram vinipinchani vairam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi rasina shlokam shokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi rasina shlokam shokama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalone unna neelone nena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalone unna neelone nena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee dhooram inka nammalekunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee dhooram inka nammalekunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethone unna ninna monna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethone unna ninna monna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhanna maata nedu nijamena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhanna maata nedu nijamena"/>
</div>
<div class="lyrico-lyrics-wrapper">Vivarinchaleni baasha edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vivarinchaleni baasha edho"/>
</div>
<div class="lyrico-lyrics-wrapper">Prakatincha leka mooga boye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prakatincha leka mooga boye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhiga minga leni badha yedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhiga minga leni badha yedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Vodhigi undaleka kannu dhaati poye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vodhigi undaleka kannu dhaati poye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye karagaram kanaledhu ee dooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye karagaram kanaledhu ee dooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanarani neram kaalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanarani neram kaalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye geetha saram vinipinchani vairam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye geetha saram vinipinchani vairam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi rasina shlokam shokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhi rasina shlokam shokama"/>
</div>
</pre>
