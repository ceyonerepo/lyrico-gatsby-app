---
title: "madura marikozhundhae song lyrics"
album: "Chekka Chivantha Vaanam"
artist: "AR Rahman"
lyricist: "Traditional"
director: "Mani Ratnam"
path: "/albums/chekka-chivantha-vaanam-lyrics"
song: "Madura Marikozhundhae"
image: ../../images/albumart/chekka-chivantha-vaanam.jpg
date: 2018-09-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/dXuyZoE8Dnc"
type: "melody"
singers:
  - Swetha Mohan
  - Anuradha Sriram
  - Aparna Narayanan
  - Madhumitha Shankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaahaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaahaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaahaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaahaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaahaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madura marikozhundhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madura marikozhundhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manalooru thaazham poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalooru thaazham poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivagangai panneerae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivagangai panneerae"/>
</div>
<div class="lyrico-lyrics-wrapper">Serurathu entha kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serurathu entha kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serurathu entha kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serurathu entha kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai manam koosuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai manam koosuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Amburuvi paayuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amburuvi paayuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesa manam nenjinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesa manam nenjinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppu thanalaaguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppu thanalaaguthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai manam koosuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai manam koosuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Amburuvi paayuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amburuvi paayuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nesa manam nenjinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nesa manam nenjinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neruppu thanalaaguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppu thanalaaguthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkam pidikuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkam pidikuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enusuru poguthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enusuru poguthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookkam koranjathadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkam koranjathadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Durai magalai kaanamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Durai magalai kaanamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Elumicham pazham pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elumicham pazham pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppeyarum oru vayathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppeyarum oru vayathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru seitha theevinaiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru seitha theevinaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalukkoru desam aanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalukkoru desam aanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalukkoru desam aanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalukkoru desam aanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madura marikozhundhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madura marikozhundhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manalooru thaazham poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manalooru thaazham poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivagangai panneerae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivagangai panneerae"/>
</div>
<div class="lyrico-lyrics-wrapper">Serurathu entha kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serurathu entha kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serurathu entha kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serurathu entha kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Serurathu entha kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serurathu entha kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaahaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaahaaaaaaa"/>
</div>
</pre>
