---
title: "kal irundha song lyrics"
album: "Pandrikku Nandri Solli"
artist: "Suren Vikhash"
lyricist: "Arul Theeran"
director: "Bala Aran"
path: "/albums/pandrikku-nandri-solli-lyrics"
song: "Kal Irundha"
image: ../../images/albumart/pandrikku-nandri-solli.jpg
date: 2022-02-04
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Nikitha Gandhi
  - Abijith Rao
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kal iruntha naaya kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kal iruntha naaya kanom"/>
</div>
<div class="lyrico-lyrics-wrapper">naai iruntha kalla kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naai iruntha kalla kanom"/>
</div>
<div class="lyrico-lyrics-wrapper">kal iruntha naaya kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kal iruntha naaya kanom"/>
</div>
<div class="lyrico-lyrics-wrapper">naai iruntha kalla kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naai iruntha kalla kanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kasaapu kadaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasaapu kadaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">kalutha neetuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalutha neetuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kooru keta kurumbaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooru keta kurumbaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">aata aduchu kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aata aduchu kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">elumba kaducha thinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elumba kaducha thinga"/>
</div>
<div class="lyrico-lyrics-wrapper">sirikkum nariyin palangaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirikkum nariyin palangaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kalutha neetuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalutha neetuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kooru keta kurumbaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooru keta kurumbaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">aata aduchu kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aata aduchu kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">elumba kaducha thinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elumba kaducha thinga"/>
</div>
<div class="lyrico-lyrics-wrapper">sirikkum nariyin palangaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sirikkum nariyin palangaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yei egiri sethari sethari kathari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei egiri sethari sethari kathari "/>
</div>
<div class="lyrico-lyrics-wrapper">kathari oodu munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathari oodu munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">yei urundu pirandu meendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei urundu pirandu meendu"/>
</div>
<div class="lyrico-lyrics-wrapper">muduva vaa thalladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muduva vaa thalladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yei egiri sethari sethari kathari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei egiri sethari sethari kathari "/>
</div>
<div class="lyrico-lyrics-wrapper">kathari oodu munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathari oodu munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">yei urundu pirandu meendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei urundu pirandu meendu"/>
</div>
<div class="lyrico-lyrics-wrapper">muduva vaa thalladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muduva vaa thalladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ivan kannukulla irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan kannukulla irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">irukkum verum bullet
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukkum verum bullet"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan kai pattu veliya vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan kai pattu veliya vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">odaiyattum kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odaiyattum kallu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kal iruntha naaya kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kal iruntha naaya kanom"/>
</div>
<div class="lyrico-lyrics-wrapper">naai iruntha kalla kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naai iruntha kalla kanom"/>
</div>
<div class="lyrico-lyrics-wrapper">kal iruntha naaya kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kal iruntha naaya kanom"/>
</div>
<div class="lyrico-lyrics-wrapper">naai iruntha kalla kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naai iruntha kalla kanom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yei oh oh oh odatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei oh oh oh odatha"/>
</div>
<div class="lyrico-lyrics-wrapper">yei oh oh oh odatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei oh oh oh odatha"/>
</div>
<div class="lyrico-lyrics-wrapper">yei olinju maranju aduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei olinju maranju aduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">pudichu oo daathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudichu oo daathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaalaki oodum vattam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalaki oodum vattam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">yei vaalthu paarka nattam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei vaalthu paarka nattam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">yei neyum nanum nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei neyum nanum nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">idam mukkonam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idam mukkonam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaalaki oodum vattam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalaki oodum vattam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">yei vaalthu paarka nattam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei vaalthu paarka nattam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">yei neyum nanum nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei neyum nanum nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">idam mukkonam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idam mukkonam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannamoochi aatam kannai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannamoochi aatam kannai"/>
</div>
<div class="lyrico-lyrics-wrapper">katti aada maatom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti aada maatom"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kannai kaati mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kannai kaati mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">aadum aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadum aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">kaaki thoppi kootam kan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaaki thoppi kootam kan"/>
</div>
<div class="lyrico-lyrics-wrapper">muluchi kadama aathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muluchi kadama aathum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyil kaasu paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyil kaasu paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kadamaiku aathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadamaiku aathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kal iruntha naaya kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kal iruntha naaya kanom"/>
</div>
<div class="lyrico-lyrics-wrapper">naai iruntha kalla kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naai iruntha kalla kanom"/>
</div>
<div class="lyrico-lyrics-wrapper">kal iruntha naaya kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kal iruntha naaya kanom"/>
</div>
<div class="lyrico-lyrics-wrapper">naai iruntha kalla kanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naai iruntha kalla kanom"/>
</div>
</pre>
