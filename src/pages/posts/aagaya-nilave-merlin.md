---
title: "aagaya nilave song lyrics"
album: "Merlin"
artist: "Ganesh Raghavendra"
lyricist: "Yugabharathi"
director: "Keera"
path: "/albums/merlin-song-lyrics"
song: "Aagaya Nilave"
image: ../../images/albumart/merlin.jpg
date: 2018-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BpT4uduOHkw"
type: "melody"
singers:
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aagaya Nilave Unn Azhagodu Uravaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaya Nilave Unn Azhagodu Uravaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbaadha Vinmeengal Yaedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbaadha Vinmeengal Yaedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boologa Malarae Unn Ezhilodu Vilayada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boologa Malarae Unn Ezhilodu Vilayada"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbaadha Poongatrum Yaedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbaadha Poongatrum Yaedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanum Naanum Thanimaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum Naanum Thanimaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarum Illa Kodumaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarum Illa Kodumaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Theeyin Naduvinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Theeyin Naduvinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayul Veyum Ilamayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayul Veyum Ilamayilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaya Nilave Unn Azhagodu Uravaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaya Nilave Unn Azhagodu Uravaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbaadha Vinmeengal Yaedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbaadha Vinmeengal Yaedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalennum Vaarthayae Karaiyaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalennum Vaarthayae Karaiyaanadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum Indha Koorayae Sirayaanadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum Indha Koorayae Sirayaanadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai Nilavaai Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Nilavaai Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anudhinamum Thaeindhaenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anudhinamum Thaeindhaenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaedalodu Vaazhvadhae Nilayaanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaedalodu Vaazhvadhae Nilayaanadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannirendum Meedhilae Kudai Saayudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannirendum Meedhilae Kudai Saayudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mungum Bodhu Uyir Moochum Mungividudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mungum Bodhu Uyir Moochum Mungividudhae"/>
</div>
</pre>
