---
title: "naadilla song lyrics"
album: "Singam Puli"
artist: "Mani Sharma"
lyricist: "Viveka"
director: "Sai Ramani"
path: "/albums/singam-puli-lyrics"
song: "Naadilla"
image: ../../images/albumart/singam-puli.jpg
date: 2011-03-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Pl_moouZ_0U"
type: "happy"
singers:
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naadillaa Kingudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadillaa Kingudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukko Paathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukko Paathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thottaa Sangudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thottaa Sangudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Naadillaa Kingudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Naadillaa Kingudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukko Paathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukko Paathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thottaa Sangudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thottaa Sangudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Pola Iruppen Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Pola Iruppen Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Munnadi Sirichaa Sirippen Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Munnadi Sirichaa Sirippen Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaadi Pola Iruppen Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Pola Iruppen Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Munnadi Sirichaa Sirippen Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Munnadi Sirichaa Sirippen Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbukku Anbudaa Vambukku Vambudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbukku Anbudaa Vambukku Vambudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadillaa Kingudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadillaa Kingudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukko Paathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukko Paathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thottaa Sangudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thottaa Sangudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Naadillaa Kingudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Naadillaa Kingudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukko Paathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukko Paathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thottaa Sangudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thottaa Sangudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottam Veedunnu Vaangkittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottam Veedunnu Vaangkittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaadhe Thalaikeezhaavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaadhe Thalaikeezhaavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Konjundu Aadittaa Podhumpaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Konjundu Aadittaa Podhumpaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekkooda Unadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekkooda Unadhilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottam Veedunnu Vaangkittaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottam Veedunnu Vaangkittaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaadhe Thalaikeezhaavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaadhe Thalaikeezhaavo"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi Konjundu Aadittaa Podhumpaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi Konjundu Aadittaa Podhumpaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekkooda Unadhilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekkooda Unadhilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththaiyaa Neettunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththaiyaa Neettunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththamaa Saayura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththamaa Saayura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aal Naan Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aal Naan Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oththaiyaa Ninnudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththaiyaa Ninnudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooraiye Velluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooraiye Velluven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Bayamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Bayamilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppodhum Unmaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhum Unmaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Solluven Solluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solluven Solluven"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappedhum Paarthuttaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappedhum Paarthuttaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kobamaa Thulluven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobamaa Thulluven"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaradi Osara Kadappaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradi Osara Kadappaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Modhinaa Odaiyum Perum Paara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Modhinaa Odaiyum Perum Paara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadillaa Kingudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadillaa Kingudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukko Paathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukko Paathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thottaa Sangudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thottaa Sangudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Naadillaa Kingudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Naadillaa Kingudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukko Paathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukko Paathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thottaa Sangudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thottaa Sangudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oora Yeikkindra Aalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Yeikkindra Aalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaam Podum Ulagamadaa Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaam Podum Ulagamadaa Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaikkum Vargathin Vervaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaikkum Vargathin Vervaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaana Palan Venum Dhevudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaana Palan Venum Dhevudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oora Yeikkindra Aalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora Yeikkindra Aalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Salaam Podum Ulagamadaa Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salaam Podum Ulagamadaa Oh"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhaikkum Vargathin Vervaikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhaikkum Vargathin Vervaikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Undaana Palan Venum Dhevudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undaana Palan Venum Dhevudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhaiyin Kookkural Yaarukkum Ketkkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhaiyin Kookkural Yaarukkum Ketkkala"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Sollappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Sollappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhura Boomikku Unnaala Yedhaachum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhura Boomikku Unnaala Yedhaachum"/>
</div>
<div class="lyrico-lyrics-wrapper">Payan Onnu Irukkanum Paa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Payan Onnu Irukkanum Paa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaasu Poladhaan Pattunnu Pesuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaasu Poladhaan Pattunnu Pesuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Veembukku Modhunaa Vettiye Veesuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veembukku Modhunaa Vettiye Veesuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaarkkum Ellaam Venumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaarkkum Ellaam Venumadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhai Ellaarum Nenaichaa Nadakkumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhai Ellaarum Nenaichaa Nadakkumadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadillaa Kingudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadillaa Kingudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukko Paathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukko Paathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thottaa Sangudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thottaa Sangudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Naadillaa Kingudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Naadillaa Kingudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathukko Paathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathukko Paathukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thottaa Sangudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thottaa Sangudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaadi Pola Iruppen Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Pola Iruppen Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Munnadi Sirichaa Sirippen Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Munnadi Sirichaa Sirippen Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaadi Pola Iruppen Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaadi Pola Iruppen Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">En Munnadi Sirichaa Sirippen Daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Munnadi Sirichaa Sirippen Daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbukku Anbudaa Vambukku Vambudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbukku Anbudaa Vambukku Vambudaa"/>
</div>
</pre>
