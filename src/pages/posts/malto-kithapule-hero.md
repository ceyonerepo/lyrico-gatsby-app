---
title: 'malto kithapule song lyrics'
album: 'Hero'
artist: 'Yuvan Shankar Raja'
lyricist: 'Rokesh'
director: 'P.S.Mithran'
path: '/albums/hero'
song: 'Malto Kithapule'
image: ../../images/albumart/hero.jpg
date: 2019-12-20
lang: tamil
singers: 
- Shyam Viswanathan
youtubeLink: "https://www.youtube.com/embed/NHzwno5Y2XA"
type: 'mass'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Malto kithapuleh
<input type="checkbox" class="lyrico-select-lyric-line" value="Malto kithapuleh"/>
</div>
<div class="lyrico-lyrics-wrapper">Giltha aalaana pulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Giltha aalaana pulla"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hey velattu
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey velattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aana alert-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Aana alert-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Light-ah silent-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Light-ah silent-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Moracha kandippa vaettu
<input type="checkbox" class="lyrico-select-lyric-line" value="Moracha kandippa vaettu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hey repeat-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey repeat-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichaa reveat-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Adichaa reveat-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Illa veruppu
<input type="checkbox" class="lyrico-select-lyric-line" value="Illa veruppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa romba sirappu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa romba sirappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla galla kattum da
<input type="checkbox" class="lyrico-select-lyric-line" value="Nalla galla kattum da"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam namma pozhappu
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinam namma pozhappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ada vena adhuppu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada vena adhuppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha adchi kozhappu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adha adchi kozhappu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee scene-ah podadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee scene-ah podadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum thaana theluppu
<input type="checkbox" class="lyrico-select-lyric-line" value="Varum thaana theluppu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Therikkanum top-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Therikkanum top-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalakkanum maapu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kalakkanum maapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalakanam illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thalakanam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee safe-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee safe-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Setting-u sharp-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Setting-u sharp-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikkuna gap-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Sikkuna gap-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Thattuna veppom
<input type="checkbox" class="lyrico-select-lyric-line" value="Thattuna veppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala aappu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pala aappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vetti veerapilla
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti veerapilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti modhi paaru da dhilla
<input type="checkbox" class="lyrico-select-lyric-line" value="Mutti modhi paaru da dhilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey assault-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey assault-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudtha result-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Kudtha result-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Era achamilla
<input type="checkbox" class="lyrico-select-lyric-line" value="Era achamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuthu nippom da vella
<input type="checkbox" class="lyrico-select-lyric-line" value="Edhuthu nippom da vella"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey talent-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey talent-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Irundha support-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Irundha support-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Angigaram illaama dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Angigaram illaama dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla therama
<input type="checkbox" class="lyrico-select-lyric-line" value="Nalla therama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru velai illa moolaiyila
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru velai illa moolaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongum nelama
<input type="checkbox" class="lyrico-select-lyric-line" value="Thoongum nelama"/>
</div>
<div class="lyrico-lyrics-wrapper">Peru pinna naal ezhuthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Peru pinna naal ezhuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Serndha perumai
<input type="checkbox" class="lyrico-select-lyric-line" value="Serndha perumai"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha qualification kammiyaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Andha qualification kammiyaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh kodumai
<input type="checkbox" class="lyrico-select-lyric-line" value="Life-eh kodumai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee correctaaga uzhaicha
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee correctaaga uzhaicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum valima
<input type="checkbox" class="lyrico-select-lyric-line" value="Varum valima"/>
</div>
<div class="lyrico-lyrics-wrapper">Vida muyarchiya pannu da
<input type="checkbox" class="lyrico-select-lyric-line" value="Vida muyarchiya pannu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam pudhumai
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhinam pudhumai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaa da oorellam paakkattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaa da oorellam paakkattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aruma
<input type="checkbox" class="lyrico-select-lyric-line" value="Un aruma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada salikkaama seiyivom
<input type="checkbox" class="lyrico-select-lyric-line" value="Ada salikkaama seiyivom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala magima
<input type="checkbox" class="lyrico-select-lyric-line" value="Pala magima"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Semester-um illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Semester-um illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Professor-um illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Professor-um illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Narukkunu iruppom
<input type="checkbox" class="lyrico-select-lyric-line" value="Narukkunu iruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene-u mela
<input type="checkbox" class="lyrico-select-lyric-line" value="Scene-u mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Gate-um thaan illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Gate-um thaan illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poottum thaan illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Poottum thaan illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaiyum udaiyum thannaala
<input type="checkbox" class="lyrico-select-lyric-line" value="Thadaiyum udaiyum thannaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Jolly jamayila
<input type="checkbox" class="lyrico-select-lyric-line" value="Jolly jamayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai kamaaya sella
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaazhkai kamaaya sella"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey sikkaama
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey sikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Poren nikkaama
<input type="checkbox" class="lyrico-select-lyric-line" value="Poren nikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaeli onnumilla
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaeli onnumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaeli pannaakka thollai
<input type="checkbox" class="lyrico-select-lyric-line" value="Gaeli pannaakka thollai"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey vudaama
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey vudaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Adippom thodaama
<input type="checkbox" class="lyrico-select-lyric-line" value="Adippom thodaama"/>
</div>
</pre>