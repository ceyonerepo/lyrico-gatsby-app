---
title: 'idhalin oru oram song lyrics'
album: '3'
artist: 'Anirudh Ravichander'
lyricist: 'Dhanush'
director: 'Soundharya Rajinikanth'
path: '/albums/3-song-lyrics'
song: 'Idhalin oru oram'
image: ../../images/albumart/3.jpg
date: 2012-03-30
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/lZORMUufA_Y'
type: 'love'
singers: 
- Ajesh Ashok
- Anirudh Ravichander
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Idhazhin oru oram sirithaai anbae
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhazhin oru oram sirithaai anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamaai ithu pothum sirippaai anbae
<input type="checkbox" class="lyrico-select-lyric-line" value="Nijamaai ithu pothum sirippaai anbae"/>
</div>
<div class="lyrico-lyrics-wrapper">En naadiyai silirka vaithaai
<input type="checkbox" class="lyrico-select-lyric-line" value="En naadiyai silirka vaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En iravellaam velicham thanthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="En iravellaam velicham thanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En aan garvam maranthindru
<input type="checkbox" class="lyrico-select-lyric-line" value="En aan garvam maranthindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un munnae paniya vaithaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Un munnae paniya vaithaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sollu nee i love you
<input type="checkbox" class="lyrico-select-lyric-line" value="Sollu nee i love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaan en kurinji poo
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thaan en kurinji poo"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal endrum true
<input type="checkbox" class="lyrico-select-lyric-line" value="En kaadhal endrum true"/>
</div>
<div class="lyrico-lyrics-wrapper">Will make sure you never feel go
<input type="checkbox" class="lyrico-select-lyric-line" value="Will make sure you never feel go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh ellaam marathu un pinnaal varuven
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh ellaam marathu un pinnaal varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sammathithaal naan nilavaiyum tharuven
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee sammathithaal naan nilavaiyum tharuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nizhal tharai padum dhooram nadanthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Un nizhal tharai padum dhooram nadanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha nodiyai thaan kavithaiyai varainthen
<input type="checkbox" class="lyrico-select-lyric-line" value="Antha nodiyai thaan kavithaiyai varainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh pennae en kannae senthenae vaa munnae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh pennae en kannae senthenae vaa munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirukkul peyarai vaithai
<input type="checkbox" class="lyrico-select-lyric-line" value="En uyirukkul peyarai vaithai"/>
</div>
<div class="lyrico-lyrics-wrapper">En naadiyai silirka vaithaai
<input type="checkbox" class="lyrico-select-lyric-line" value="En naadiyai silirka vaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En iravellaam velicham thanthaai
<input type="checkbox" class="lyrico-select-lyric-line" value="En iravellaam velicham thanthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En aan garvam maranthindru
<input type="checkbox" class="lyrico-select-lyric-line" value="En aan garvam maranthindru"/>
</div>
<div class="lyrico-lyrics-wrapper">Un munnae paniya vaithaai …vaithaaiii…
<input type="checkbox" class="lyrico-select-lyric-line" value="Un munnae paniya vaithaai …vaithaaiii…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oh pennae en kannae senthenae vaa munnae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh pennae en kannae senthenae vaa munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyirukkul peyarai vaithai (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="En uyirukkul peyarai vaithai"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sollu nee i love you
<input type="checkbox" class="lyrico-select-lyric-line" value="Sollu nee i love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaan en kurinji poo
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thaan en kurinji poo"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal endrum true
<input type="checkbox" class="lyrico-select-lyric-line" value="En kaadhal endrum true"/>
</div>
<div class="lyrico-lyrics-wrapper">Will make sure you never feel go
<input type="checkbox" class="lyrico-select-lyric-line" value="Will make sure you never feel go"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sollu nee i love you..oh penne
<input type="checkbox" class="lyrico-select-lyric-line" value="Sollu nee i love you..oh penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaan en kurinji poo..oh penne
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thaan en kurinji poo..oh penne"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal endrum true..oh penne
<input type="checkbox" class="lyrico-select-lyric-line" value="En kaadhal endrum true..oh penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Will make sure you never feel go
<input type="checkbox" class="lyrico-select-lyric-line" value="Will make sure you never feel go"/>
</div>
</pre>