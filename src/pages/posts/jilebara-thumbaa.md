---
title: "jilebara song lyrics"
album: "Thumbaa"
artist: "Vivek–Mervin"
lyricist: "Ku. Karthik"
director: "Harish Ram L.H."
path: "/albums/thumbaa-lyrics"
song: "Jilebara"
image: ../../images/albumart/thumbaa.jpg
date: 2019-06-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/08e2WEq5UUU"
type: "happy"
singers:
  - Vivek Siva
  - Mervin Solomon
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Chilla Chilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Chilla Chilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atho Vara Itho Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atho Vara Itho Vara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokki Pottu Izhukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokki Pottu Izhukura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Vitta Suththikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Vitta Suththikira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Chilla Chilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Chilla Chilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atho Vara Itho Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atho Vara Itho Vara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokki Pottu Izhukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokki Pottu Izhukkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Vitta Suthikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Vitta Suthikira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thappara Bul Bul Thaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thappara Bul Bul Thaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pannura Akkaporaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannura Akkaporaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkana  Scene ah Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkana  Scene ah Potta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kizhipom Da Daara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhipom Da Daara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dammara Dakkabulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dammara Dakkabulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaataadha Mukkabulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaataadha Mukkabulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nachunu Route ah Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachunu Route ah Pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuven Bejaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuven Bejaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Puliyathaan Nekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Puliyathaan Nekka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuri Vecha Sokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuri Vecha Sokka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poriyaa Di Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poriyaa Di Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Escape-u Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Escape-u Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silver Note Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silver Note Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sillarai Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillarai Potta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkuva Sharpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkuva Sharpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pickuppu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pickuppu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Chilla Chilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Chilla Chilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atho Vara Itho Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atho Vara Itho Vara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokki Pottu Izhukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokki Pottu Izhukura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Vitta Suthikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Vitta Suthikira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Chilla Chilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Chilla Chilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atho Vara Itho Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atho Vara Itho Vara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokki Pottu Izhukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokki Pottu Izhukura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Vitta Suthikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Vitta Suthikira"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Oru Vaatti Thaan Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Oru Vaatti Thaan Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namma Lifefu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Lifefu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga Ethukku Risku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Ethukku Risku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thagaraaru Venam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagaraaru Venam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kelambu Kelambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kelambu Kelambu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summa Nee Nillu Padharama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Nee Nillu Padharama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Iththanai Allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Iththanai Allu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhilla Nee Vaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhilla Nee Vaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnagalaamey Cheer Upu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnagalaamey Cheer Upu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennamo Etho Nee Sollura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo Etho Nee Sollura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeraliyey Enga Buththikulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeraliyey Enga Buththikulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nambi Thaan Nikkuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambi Thaan Nikkuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Munnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Munnala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ipdi Vittuta Paavam Intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipdi Vittuta Paavam Intha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paya Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paya Pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Thappara Bul Bul Thaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Thappara Bul Bul Thaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pannura Akkaporaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannura Akkaporaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkana Scena Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkana Scena Potta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kizhipom Da Daara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kizhipom Da Daara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dammara Dakkabulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dammara Dakkabulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaataadha Mukkabulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaataadha Mukkabulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nachunu Routa Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachunu Routa Pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuven Bejaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuven Bejaara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Puliyathaan Nekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Puliyathaan Nekka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuri Vecha Sokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuri Vecha Sokka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poriyaa Di Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poriyaa Di Naanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Escapepu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Escapepu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Silver Note Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silver Note Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sillarai Potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillarai Potta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikkuva Sharpa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikkuva Sharpa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pickuppu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pickuppu Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Don’t Stop
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t Stop"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Jilebara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Jilebara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jilebara Chilla Chilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jilebara Chilla Chilla"/>
</div>
</pre>
