---
title: "kanne en kannazhage song lyrics"
album: "kanaa"
artist: "Dhibu Ninan Thomas"
lyricist: "Arunraja Kamaraj"
director: " Arunraja Kamaraj"
path: "/albums/kanaa-lyrics"
song: "Kanne En Kannazhage"
image: ../../images/albumart/kanaa.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/zZLJKV6bPzE"
type: "sad"
singers:
  - Kapil Kapilan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanne En Kannazhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne En Kannazhage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneerum Podhumadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerum Podhumadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponne Adi Ponnazhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponne Adi Ponnazhage"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnerum Neramadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnerum Neramadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam Kanaa Ahhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Kanaa Ahhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam Kanaa Kaanbathum Paavamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Kanaa Kaanbathum Paavamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhaigal Maaridhaam Pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaigal Maaridhaam Pogumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanjamillaa Solai Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjamillaa Solai Ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paalai Polaaguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalai Polaaguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhamillaa Anbu Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhamillaa Anbu Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadum Nam Kangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadum Nam Kangale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariraaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariraaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariraaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariraaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariraaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariraaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ambaari oonjalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ambaari oonjalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodu Aadiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodu Aadiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Aasai Un Swasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Aasai Un Swasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endraanathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endraanathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sellaadha Thoorangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sellaadha Thoorangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sendraaga Vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendraaga Vendume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Aasai Vellaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aasai Vellaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoongaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Vaanam Meedhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Vaanam Meedhile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneerin Megame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerin Megame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam Vaazhkai Thediye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Vaazhkai Thediye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peigindra Nerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peigindra Nerame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanjamillaa Solai Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanjamillaa Solai Ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paalai Polaaguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalai Polaaguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andhamillaa Anbu Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhamillaa Anbu Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadum Nam Kangale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadum Nam Kangale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam Kanaa Kaanbathum Paavamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Kanaa Kaanbathum Paavamo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhaigal Maaridhaam Pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaigal Maaridhaam Pogumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariraaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariraaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariraaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariraaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariraaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariraaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariraaraariro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraariro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraariro"/>
</div>
</pre>
