---
title: "appadi podu podu podu song lyrics"
album: "Gilli"
artist: "Vidyasagar"
lyricist: "Pa. Vijay"
director: "Dharani"
path: "/albums/gilli-lyrics"
song: "Appadi Podu Podu Podu"
image: ../../images/albumart/gilli.jpg
date: 2004-04-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Hk-iALT4vzU"
type: "love"
singers:
  - KK
  - Anuradha Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Appudi Podu Podu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appudi Podu Podu Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Asathi Podu Kannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asathi Podu Kannaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippudi Podu Podu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippudi Podu Podu Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuthu Podu Kaiyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthu Podu Kaiyaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnoda Ooru Sutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnoda Ooru Sutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu Moota Yerikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu Moota Yerikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnoada Kanna Pothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnoada Kanna Pothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaamoochi Aada Varen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaamoochi Aada Varen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Nadai Podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Nadai Podhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Konjam Venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Konjam Venuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Nadai Podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Nadai Podhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Konjam Venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Konjam Venuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Indha Nadai Podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Indha Nadai Podhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Konjam Venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Konjam Venuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appudi Podu Podu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appudi Podu Podu Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Asathi Podu Kannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asathi Podu Kannaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippudi Podu Podu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippudi Podu Podu Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuthu Podu Kaiyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthu Podu Kaiyaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manasila Nee Ninaaikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manasila Nee Ninaaikkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Azhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kanavila Nee Muzhikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kanavila Nee Muzhikkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Adada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Adada"/>
</div>
<div class="lyrico-lyrics-wrapper">En Udhaatula Nee Inikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Udhaatula Nee Inikkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Nijamthaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Nijamthaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Usurula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Usurula"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thudikkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thudikkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Azhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Azhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vayasula Nee Paduthiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vayasula Nee Paduthiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Medhuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Medhuva"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kazhuthula Nee Manakkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kazhuthula Nee Manakkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Adhuthaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Adhuthaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Paatha Santhosathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paatha Santhosathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Madanga Poothirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Madanga Poothirunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thotta Achathila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thotta Achathila"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu Thadathaan Verthirunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu Thadathaan Verthirunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnoda Kannangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnoda Kannangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaakaa Kadi Naan Kadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakaa Kadi Naan Kadikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Kaadhu Pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Kaadhu Pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella Kadi Nee Kadikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella Kadi Nee Kadikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Vayasu Podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vayasu Podhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Konjam Venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Konjam Venuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Vayasu Podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vayasu Podhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Konjam Venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Konjam Venuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Vayasu Podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Vayasu Podhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Konjam Venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Konjam Venuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Appudi Podu Podu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Appudi Podu Podu Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Asathi Podu Kannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asathi Podu Kannaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippudi Podu Podu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippudi Podu Podu Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuthu Podu Kaiyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthu Podu Kaiyaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thikka Vekkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikka Vekkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinara Vekkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thinara Vekkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Medhuvaa Vikka Vekkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Medhuvaa Vikka Vekkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Viyarka Vekkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viyarka Vekkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ennathaan Vathatha Vekkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennathaan Vathatha Vekkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhanga Vekkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhanga Vekkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Sarithaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Sarithaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikka Vekkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikka Vekkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevakka Vekkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevakka Vekkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Joraa Sokka Vekkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Joraa Sokka Vekkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Sozhala Vekkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sozhala Vekkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Azhagha Paththa Vekkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Azhagha Paththa Vekkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhara Vekkiriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhara Vekkiriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu Muraithaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu Muraithaanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otha Paarvai Nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Paarvai Nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oosi Noolum Korkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oosi Noolum Korkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Theththu Pallu Sirippil Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theththu Pallu Sirippil Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathu Nilavu Therikkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathu Nilavu Therikkuthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thai Thai-nu Aadikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thai Thai-nu Aadikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnodu Naanum Varen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnodu Naanum Varen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nai Nai-nu Pesikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nai Nai-nu Pesikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Onn Kooda Sernthu Varen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onn Kooda Sernthu Varen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Aatam Podhumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Aatam Podhumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Konjam Venumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Konjam Venumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Aatam Podhumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Aatam Podhumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Konjam Venumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Konjam Venumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Aatam Podhumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Aatam Podhumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Konjam Venumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Konjam Venumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoi Appudi Podu Podu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Appudi Podu Podu Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Asathi Podu Kannaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asathi Podu Kannaale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippudi Podu Podu Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippudi Podu Podu Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Izhuthu Podu Kaiyaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhuthu Podu Kaiyaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnoda Ooru Sutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnoda Ooru Sutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu Moota Yerikkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu Moota Yerikkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnoada Kanna Pothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnoada Kanna Pothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaamoochi Aada Varen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaamoochi Aada Varen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Nadai Podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Nadai Podhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Konjam Venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Konjam Venuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Nadai Podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Nadai Podhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Konjam Venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Konjam Venuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aye Indha Nadai Podhuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aye Indha Nadai Podhuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Konjam Venuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Konjam Venuma"/>
</div>
</pre>
