---
title: "andama andama song lyrics"
album: "KS100"
artist: "Navaneeth Chari"
lyricist: "Bhashya Sree"
director: "Sher"
path: "/albums/ks100-lyrics"
song: "Andama Andama"
image: ../../images/albumart/ks100.jpg
date: 2019-07-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/FFO_sAvFZ2A"
type: "love"
singers:
  - Hemachandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">andhama andhama andhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhama andhama andhama"/>
</div>
<div class="lyrico-lyrics-wrapper">sannani sogasula sandrama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sannani sogasula sandrama"/>
</div>
<div class="lyrico-lyrics-wrapper">priyathama priyathama priyathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="priyathama priyathama priyathama"/>
</div>
<div class="lyrico-lyrics-wrapper">paruvame perigina virahama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruvame perigina virahama"/>
</div>
<div class="lyrico-lyrics-wrapper">vayyarama neeli mayurama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayyarama neeli mayurama"/>
</div>
<div class="lyrico-lyrics-wrapper">vedhinchake kanne pramadhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedhinchake kanne pramadhama"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatesi poke chooputhoti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatesi poke chooputhoti"/>
</div>
<div class="lyrico-lyrics-wrapper">konte thilottama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konte thilottama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">andhama andhama andhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhama andhama andhama"/>
</div>
<div class="lyrico-lyrics-wrapper">sannani sogasula sandrama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sannani sogasula sandrama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nayanam nallani sarpam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nayanam nallani sarpam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee choope oka kshudram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee choope oka kshudram"/>
</div>
<div class="lyrico-lyrics-wrapper">nadume metthane pushpam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadume metthane pushpam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vompe oka swargam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vompe oka swargam"/>
</div>
<div class="lyrico-lyrics-wrapper">nadiche vasantham nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadiche vasantham nee "/>
</div>
<div class="lyrico-lyrics-wrapper">yadhalo cheyanna yagnam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yadhalo cheyanna yagnam"/>
</div>
<div class="lyrico-lyrics-wrapper">oopiri theese oo valayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oopiri theese oo valayam"/>
</div>
<div class="lyrico-lyrics-wrapper">kougili chaale endhuku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kougili chaale endhuku "/>
</div>
<div class="lyrico-lyrics-wrapper">maru jananam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru jananam "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">andhama andhama andhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhama andhama andhama"/>
</div>
<div class="lyrico-lyrics-wrapper">sannani sogasula sandrama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sannani sogasula sandrama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sarasam aade samayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarasam aade samayam"/>
</div>
<div class="lyrico-lyrics-wrapper">yetu chudake nestham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yetu chudake nestham"/>
</div>
<div class="lyrico-lyrics-wrapper">arpinchesey sarvam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arpinchesey sarvam"/>
</div>
<div class="lyrico-lyrics-wrapper">cheseddam oo yuddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheseddam oo yuddam"/>
</div>
<div class="lyrico-lyrics-wrapper">midhunam madhanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="midhunam madhanam "/>
</div>
<div class="lyrico-lyrics-wrapper">nee adharam ahi madhuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee adharam ahi madhuram"/>
</div>
<div class="lyrico-lyrics-wrapper">thanuvulu chese ee jagadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanuvulu chese ee jagadam"/>
</div>
<div class="lyrico-lyrics-wrapper">thapanalu perugunu bhadram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thapanalu perugunu bhadram"/>
</div>
<div class="lyrico-lyrics-wrapper">nee bidiyam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee bidiyam "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">andhama andhama andhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhama andhama andhama"/>
</div>
<div class="lyrico-lyrics-wrapper">sannani sogasula sandrama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sannani sogasula sandrama"/>
</div>
<div class="lyrico-lyrics-wrapper">priyathama priyathama priyathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="priyathama priyathama priyathama"/>
</div>
<div class="lyrico-lyrics-wrapper">paruvame perigina virahama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruvame perigina virahama"/>
</div>
</pre>
