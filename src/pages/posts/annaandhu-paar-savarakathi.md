---
title: "annaandhu paar song lyrics"
album: "Savarakathi"
artist: "Arrol Corelli"
lyricist: "Thamizhachi Thangapandian"
director: "G.R. Adithya"
path: "/albums/savarakathi-lyrics"
song: "Annaandhu Paar"
image: ../../images/albumart/savarakathi.jpg
date: 2018-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DHBP49dlL48"
type: "happy"
singers:
  - Madhu Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Annaandhu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaandhu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kangalil natchathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kangalil natchathiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraindhu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraindhu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvae oru poi siththiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvae oru poi siththiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veen endral vithaiyaaga maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veen endral vithaiyaaga maaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen endral vidaiyaaga maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen endral vidaiyaaga maaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan endral naamaagi eeindraduuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan endral naamaagi eeindraduuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaandhu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaandhu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kangalil natchathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kangalil natchathiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraindhu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraindhu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvae oru poi siththiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvae oru poi siththiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sadhi irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhi irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivithi irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivithi irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mei kannil poimai theettathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mei kannil poimai theettathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mull irunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mull irunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koor kallirunthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koor kallirunthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paatham pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paatham pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi maarathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi maarathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sumai illa uravu engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumai illa uravu engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuyarillaa naalai undaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuyarillaa naalai undaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrae kondaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrae kondaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagai endral paniyaaga maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagai endral paniyaaga maaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinai endral sunaiyaaga maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinai endral sunaiyaaga maaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Irul endral sudaraagi vilaiyaadu uu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irul endral sudaraagi vilaiyaadu uu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaandhu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaandhu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kangalil natchathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kangalil natchathiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovaaga maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovaaga maaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalai yenbadhillai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai yenbadhillai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasam veesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasam veesu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siragaaga maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragaaga maaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam yenbadhillai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam yenbadhillai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Megam urasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megam urasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valiyilla thaaimai engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valiyilla thaaimai engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal illaa thooni undaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal illaa thooni undaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum poraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum poraadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vali endraal madiyaaga maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vali endraal madiyaaga maaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayam endral kodiyaaga maaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayam endral kodiyaaga maaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhai endral araththodu thallaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhai endral araththodu thallaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaandhu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaandhu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kangalil natchathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kangalil natchathiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaraindhu paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraindhu paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvae oru poi siththiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvae oru poi siththiram"/>
</div>
</pre>
