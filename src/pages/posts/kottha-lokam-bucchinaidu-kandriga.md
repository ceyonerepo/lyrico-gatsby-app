---
title: "kottha lokam song lyrics"
album: "Bucchinaidu Kandriga"
artist: "Mihiraamsh"
lyricist: "Battu Vijay Kumar"
director: "Krishna Poluru"
path: "/albums/bucchinaidu-kandriga-lyrics"
song: "Kottha Lokam"
image: ../../images/albumart/bucchinaidu-kandriga.jpg
date: 2020-08-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/NDzK8RrtKVw"
type: "love"
singers:
  - Sai Charan
  - Aditi Bhavaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kottha Lokam Chuddaam Chuddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Lokam Chuddaam Chuddhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Konte Dhaarullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konte Dhaarullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhaala Yuddham Cheddhaam Cheddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhaala Yuddham Cheddhaam Cheddhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Muddhu Maikamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muddhu Maikamlo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undipodhaam Kougilla Intlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undipodhaam Kougilla Intlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimpukundhaam Nishaani Ontlo Vedi Shwasallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimpukundhaam Nishaani Ontlo Vedi Shwasallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Niddharinka Maaneddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niddharinka Maaneddhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottham Ekaanthamlone Undhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottham Ekaanthamlone Undhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddharokatai Podhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddharokatai Podhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jantai Raa Raa Raa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jantai Raa Raa Raa Aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siggu Champe Pandhem Veddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siggu Champe Pandhem Veddhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chimma Cheekatlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chimma Cheekatlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayassu Cheppe Paatam Vindhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayassu Cheppe Paatam Vindhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paita Chaatullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paita Chaatullo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadumuthu Deham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadumuthu Deham"/>
</div>
<div class="lyrico-lyrics-wrapper">Thega Regani Moham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thega Regani Moham"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerani Thaaham Thamakapu Dhaaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerani Thaaham Thamakapu Dhaaham"/>
</div>
<div class="lyrico-lyrics-wrapper">Poorthyga Theercheddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poorthyga Theercheddhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy Korika Entho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Korika Entho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulaasa Kosarulu Anthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulaasa Kosarulu Anthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virahamunanthaa Velese 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virahamunanthaa Velese "/>
</div>
<div class="lyrico-lyrics-wrapper">Veduka Cheddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veduka Cheddhaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theepi Gaayalennainaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theepi Gaayalennainaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Theeram Cheraalantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeram Cheraalantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Baadhe Haaye Ayyelaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baadhe Haaye Ayyelaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Baanam Sandhinchaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baanam Sandhinchaalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Niddharinka Maaneddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niddharinka Maaneddhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottham Ekaanthamlone Undhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottham Ekaanthamlone Undhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddharokatai Podhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddharokatai Podhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jantai RaaRaa RaaRaa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jantai RaaRaa RaaRaa Aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eedu Pette Mantallonaa Aapasopaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedu Pette Mantallonaa Aapasopaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkapothai Thadipe Thadilo Janta Snaanaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkapothai Thadipe Thadilo Janta Snaanaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmmm Sogasula Sothram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmm Sogasula Sothram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasi Paruvapu Padhyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasi Paruvapu Padhyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamudi Kaavyam Valapula Vedham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamudi Kaavyam Valapula Vedham"/>
</div>
<div class="lyrico-lyrics-wrapper">Okatigaa Chadhiveddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatigaa Chadhiveddhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvula Thaapam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvula Thaapam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sukhaala Chematalu Sedhyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukhaala Chematalu Sedhyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranayapu Aathram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranayapu Aathram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaage Undani Nithyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaage Undani Nithyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho Pagalu Raathrantoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Pagalu Raathrantoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadhilo Thedaa Ledhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadhilo Thedaa Ledhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalupe Moosei Rojanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalupe Moosei Rojanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarasam Saagiddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarasam Saagiddhaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niddharinka Maaneddhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niddharinka Maaneddhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mottham Ekaanthamlone Undhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottham Ekaanthamlone Undhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Iddharokatai Podhaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iddharokatai Podhaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jantai Raa Raa Raa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jantai Raa Raa Raa Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha Lokam Chuddaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Lokam Chuddaam "/>
</div>
<div class="lyrico-lyrics-wrapper">Chuddhaam Konte Dhaarullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuddhaam Konte Dhaarullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkapothai Thadipe Thadilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkapothai Thadipe Thadilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Janta Snaanaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janta Snaanaalu"/>
</div>
</pre>