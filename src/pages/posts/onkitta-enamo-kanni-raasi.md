---
title: "onkitta enamo song lyrics"
album: "Kanni Raasi"
artist: "Vishal Chandrasekhar"
lyricist: "Yugabharathi"
director: "Muthukumaran"
path: "/albums/kanni-raasi-lyrics"
song: "Onkitta Enamo"
image: ../../images/albumart/kanni-raasi.jpg
date: 2020-12-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_H2LFrYgHTc"
type: "love"
singers:
  - Sathya Prakash
  - Kalyaninair
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ahaaa aaahaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaa aaahaaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mmm hmmm mm hmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm hmmm mm hmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onkitta ennamo irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onkitta ennamo irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha ottu mothamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha ottu mothamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu kaattadi enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu kaattadi enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadiya marandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadiya marandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavani edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavani edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aippasi mazhai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aippasi mazhai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ini unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ini unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaiyil nee nanaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyil nee nanaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanenae kirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanenae kirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onkitta ennamo irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onkitta ennamo irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha ottu mothamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha ottu mothamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu kaattanum enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu kaattanum enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadiya marandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadiya marandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aavani edhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aavani edhukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aippasi mazhai thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aippasi mazhai thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ini unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ini unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaiyil nee nanaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiyil nee nanaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanenae kirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanenae kirukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm hhmm mm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hhmm mm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaa aa hmm mmm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa aa hmm mmm mm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaraaya mazhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaraaya mazhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Poovae nee pozhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovae nee pozhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeratha bodhai yerum enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeratha bodhai yerum enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho ariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho ariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pooraavum theriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pooraavum theriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalladurenae naanum adhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalladurenae naanum adhukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kummiruttu velaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kummiruttu velaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Chimminiya nee irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chimminiya nee irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekka chakka aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekka chakka aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai anaikkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai anaikkuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooda nee kodhicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooda nee kodhicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollaama parichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollaama parichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasaangum pannamaatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasaangum pannamaatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaiyaiyum virichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaiyaiyum virichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onkitta ennamo irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onkitta ennamo irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha ottu mothamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha ottu mothamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu kaattadi enakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu kaattadi enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanoda nelavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanoda nelavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayoda unavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayoda unavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Seraama pona seidhi irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seraama pona seidhi irukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eerezhu ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eerezhu ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Illadha pozhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illadha pozhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkooda vaazhven naanum serukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda vaazhven naanum serukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uchchi thottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchchi thottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadham varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadham varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppozhuthum un nenaippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppozhuthum un nenaippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appalaththa pola ennai norukkuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appalaththa pola ennai norukkuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaana nee izhuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana nee izhuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaambulam koduthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaambulam koduthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaannu solla maatten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaannu solla maatten"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangippen karuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangippen karuthaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm hhmm mm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hhmm mm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa aaa aa hmm mmm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa aaa aa hmm mmm mm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm hhmm mm mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hhmm mm mmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm mm mm hmm mmm mm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm mm mm hmm mmm mm"/>
</div>
</pre>
