---
title: 'maari gethu song lyrics'
album: 'Maari 2'
artist: 'Yuvan Shankar Raja'
lyricist: 'Yuvan Shankar Raja'
director: 'Balaji Mohan'
path: '/albums/maari2-song-lyrics'
song: 'Maari Gethu'
image: ../../images/albumart/Maari-2.jpg
date: 2018-12-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_Z-wletdIEw"
type: 'Mass'
singers: 
- Yuvan Shankar Raja
- Dhanush
- ChinnaponnuV.M. Mahalingam
- V.M. Mahalingam
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  (<div class="lyrico-lyrics-wrapper">Intro music)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intro music)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
()
<div class="lyrico-lyrics-wrapper">Ei Bigilu adichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Bigilu adichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevulu piriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevulu piriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanja vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanja vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dealu kiliyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dealu kiliyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sozhatti vechaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sozhatti vechaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mommy daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mommy daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Face theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Face theriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei Bigilu adichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Bigilu adichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevulu piriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevulu piriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanja vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanja vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dealu kiliyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dealu kiliyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sozhatti vechaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sozhatti vechaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Face theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Face theriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sellvaakku sellaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sellvaakku sellaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pappu vevaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pappu vevaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanikkattu raaja daa nammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanikkattu raaja daa nammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
(<div class="lyrico-lyrics-wrapper">Ei nammaalu)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei nammaalu)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee friendshipaa vandhaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee friendshipaa vandhaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyaa irunthaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyaa irunthaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuraiyae tharuvaandaa nammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraiyae tharuvaandaa nammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
(<div class="lyrico-lyrics-wrapper">Ei nammaalu)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei nammaalu)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eii sellvaakku sellaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eii sellvaakku sellaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pappu vevaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pappu vevaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanikkattu raaja daa nammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanikkattu raaja daa nammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
(<div class="lyrico-lyrics-wrapper">Ei nammaalu)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei nammaalu)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee friendshipaa vandhaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee friendshipaa vandhaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyaa irunthaakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyaa irunthaakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Usuraiyae tharuvaandaa nammaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usuraiyae tharuvaandaa nammaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
(<div class="lyrico-lyrics-wrapper">Ei nammaalu)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei nammaalu)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maari nee nallavara kettavara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari nee nallavara kettavara"/>
</div>
<div class="lyrico-lyrics-wrapper">Therilaiyae baa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therilaiyae baa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hahaha hahaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahaha hahaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei velagi velagi velagi velagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei velagi velagi velagi velagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oram podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oram podaa"/>
</div>
(<div class="lyrico-lyrics-wrapper">Podaa)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaa)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O vairu erinja thanni ooththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O vairu erinja thanni ooththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
(<div class="lyrico-lyrics-wrapper">Podaa)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaa)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
(<div class="lyrico-lyrics-wrapper">Ei naanga vera maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei naanga vera maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan enga aalu maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan enga aalu maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Eii maari eii maari eii maari eee)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eii maari eii maari eii maari eee)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gada gada kakkada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gada gada kakkada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nada nada nannada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nada nada nannada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigarigar ingilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigarigar ingilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilainjarinpada aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilainjarinpada aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pada pada pappaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada pada pappaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Norngidum thattada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Norngidum thattada"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vanthu nee neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vanthu nee neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudaathadaa vadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudaathadaa vadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei yeriduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei yeriduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei Durr
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Durr"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
(<div class="lyrico-lyrics-wrapper">Massu yeriduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Massu yeriduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Graceu yeriduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Graceu yeriduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesham podaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham podaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaetti kilinjiruchu)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaetti kilinjiruchu)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ma ma ma ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ma ma ma ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
(<div class="lyrico-lyrics-wrapper">Matter maattikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matter maattikichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sketchuu maattikichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sketchuu maattikichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Planu gleenu-ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Planu gleenu-ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathula pichikichu)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathula pichikichu)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennodadhellaam undhu nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennodadhellaam undhu nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettadhu ellaam naan koduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettadhu ellaam naan koduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaanda onnum ella nanbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaanda onnum ella nanbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholodu thozhanaa naan iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholodu thozhanaa naan iruppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey velagi velagi velagi velagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey velagi velagi velagi velagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oram podaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oram podaa"/>
</div>
(<div class="lyrico-lyrics-wrapper">Podaa)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaa)"/>
</div>
<div class="lyrico-lyrics-wrapper">O vairu erinja thanni ooththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O vairu erinja thanni ooththi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram poda"/>
</div>
(<div class="lyrico-lyrics-wrapper">Podaa)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaa)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei naanga vera maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei naanga vera maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan enga aalu maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan enga aalu maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei maari ei maari ei maari..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei maari ei maari ei maari.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei maari geththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei maari geththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei maari geththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei maari geththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oram oththe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oram oththe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oram oththe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oram oththe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei geththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei geththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei geththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei geththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei oththe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei oththe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ei oththe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei oththe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ei Bigilu adichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ei Bigilu adichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindhaa jindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindhaa jindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevulu piriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevulu piriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindhaa jindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindhaa jindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanja vutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanja vutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindhaa jindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindhaa jindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dealu kiliyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dealu kiliyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindhaa jindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindhaa jindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maari onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maari onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindhaa jindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindhaa jindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sozhatti vechaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sozhatti vechaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindhaa jindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindhaa jindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mommy daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mommy daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindhaa jindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindhaa jindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Face theriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Face theriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindhaa jindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindhaa jindhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey jindhaa jindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey jindhaa jindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindha thakkudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindha thakkudi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jindhaa jindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindhaa jindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jindhaa thakkudi (x3)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindhaa thakkudi (x3)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinthatha jinthatha(x2)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinthatha jinthatha(x2)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jindhaa jindhaa(x8)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jindhaa jindhaa(x8)"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru edathula vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru edathula vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru scene poduradhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru scene poduradhu"/>
</div>
</pre>