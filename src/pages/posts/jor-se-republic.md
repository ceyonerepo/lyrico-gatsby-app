---
title: "jor se song lyrics"
album: "Republic"
artist: "Mani Sharma"
lyricist: "Suddala Ashok Teja"
director: "Deva Katta"
path: "/albums/republic-lyrics"
song: "Jor Se"
image: ../../images/albumart/republic.jpg
date: 2021-10-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/RXFxzl0J45w"
type: "mass"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chiguru chinthala meedha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiguru chinthala meedha"/>
</div>
<div class="lyrico-lyrics-wrapper">Raama silakalooy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raama silakalooy"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagale dhiginayi chudu sandravankalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagale dhiginayi chudu sandravankalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Seruku pilladu suse choopu surukulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seruku pilladu suse choopu surukulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalki buggala meedha siggu marakaloy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalki buggala meedha siggu marakaloy"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudabodhama aadabothama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudabodhama aadabothama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sudabodhamaa aadabodhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudabodhamaa aadabodhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye seeyi seyyi kalipi serabodhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye seeyi seyyi kalipi serabodhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudabodhamaa aadabodhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudabodhamaa aadabodhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye seeyi seyyi kalipi serabodhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye seeyi seyyi kalipi serabodhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Jorsey baarsey therasaapa jaarsey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jorsey baarsey therasaapa jaarsey"/>
</div>
<div class="lyrico-lyrics-wrapper">Padavaninka jorsey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padavaninka jorsey"/>
</div>
<div class="lyrico-lyrics-wrapper">Jorsey baarsey therasaapa jaarsey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jorsey baarsey therasaapa jaarsey"/>
</div>
<div class="lyrico-lyrics-wrapper">Padavaninka jorsey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padavaninka jorsey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dama dama jathara pandugaroy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dama dama jathara pandugaroy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuma ghuma puvvula dhandalu vey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuma ghuma puvvula dhandalu vey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulaku kache talliki jai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulaku kache talliki jai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvutho porli dhandamu chey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvutho porli dhandamu chey"/>
</div>
<div class="lyrico-lyrics-wrapper">Dama dama jathara pandugaroy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dama dama jathara pandugaroy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghuma ghuma puvvula dhandalu vey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghuma ghuma puvvula dhandalu vey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulaku kache talliki jai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulaku kache talliki jai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanuvutho porli dhandamu chey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanuvutho porli dhandamu chey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennello kallu yeru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennello kallu yeru "/>
</div>
<div class="lyrico-lyrics-wrapper">thaana maaduthunnadhanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaana maaduthunnadhanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Eldhaama eldhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eldhaama eldhaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarassuthoni sandhurudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarassuthoni sandhurudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarasamaduthunnadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarasamaduthunnadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Eldhaama eldhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eldhaama eldhaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaali sempa gilluthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaali sempa gilluthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Poolu siggu padathayanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poolu siggu padathayanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Eldhaama eldhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eldhaama eldhaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Valasa pacchulocchi neella 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valasa pacchulocchi neella "/>
</div>
<div class="lyrico-lyrics-wrapper">holi jallukuntaayanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="holi jallukuntaayanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudabodhamaa aadabodhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudabodhamaa aadabodhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye seeyi seyyi kalipi serabodhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye seeyi seyyi kalipi serabodhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudabodhamaa aadabodhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudabodhamaa aadabodhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye seeyi seyyi kalipi serabodhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye seeyi seyyi kalipi serabodhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Jorsey baarsey therasaapa jaarsey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jorsey baarsey therasaapa jaarsey"/>
</div>
<div class="lyrico-lyrics-wrapper">Padavaninka jorsey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padavaninka jorsey"/>
</div>
<div class="lyrico-lyrics-wrapper">Jorsey baarsey therasaapa jaarsey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jorsey baarsey therasaapa jaarsey"/>
</div>
<div class="lyrico-lyrics-wrapper">Padavaninka jorsey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padavaninka jorsey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasupu kunkalu gaache paarvathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasupu kunkalu gaache paarvathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Roopamanta peddintlamma peddintlamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roopamanta peddintlamma peddintlamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolleru biddala kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolleru biddala kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluvaina thallenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluvaina thallenanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Peddintlamma peddintlamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peddintlamma peddintlamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangu rangula prabhalu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangu rangula prabhalu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaarangam aadukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaarangam aadukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Eldhaama eldhaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eldhaama eldhaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye mudupu kattu kunna janta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye mudupu kattu kunna janta"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullu yesukuntayanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullu yesukuntayanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jorsey baarsey therasaapa jaarsey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jorsey baarsey therasaapa jaarsey"/>
</div>
<div class="lyrico-lyrics-wrapper">Padavaninka jorsey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padavaninka jorsey"/>
</div>
</pre>
