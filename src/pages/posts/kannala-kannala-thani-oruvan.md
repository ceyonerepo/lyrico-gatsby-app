---
title: "kannala kannala song lyrics"
album: "Thani Oruvan"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "M. Raja"
path: "/albums/thani-oruvan-lyrics"
song: "Kannala Kannala"
image: ../../images/albumart/thani-oruvan.jpg
date: 2015-08-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7dDeAE2V0oM"
type: "Love"
singers:
  - Kaushik Krish
  - Padmalatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nenjorama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjorama"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Kadhal Thulirumbodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Kadhal Thulirumbodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannorama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannorama"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Kanneer Thuligal Yeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Kanneer Thuligal Yeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaalane En Kannaal Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalane En Kannaal Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaidhaakkida Naan Nenachene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaidhaakkida Naan Nenachene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerula Oru Mai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerula Oru Mai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda Sera Thuduchene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Sera Thuduchene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasula Poongaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Poongaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paakum Dhisaiyil Veesum Bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paakum Dhisaiyil Veesum Bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkunu Oru Desam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkunu Oru Desam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Iruvarum Serndhu Onna Vaalvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Iruvarum Serndhu Onna Vaalvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Kanala En Mela En Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Kanala En Mela En Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeya Erunju Putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya Erunju Putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Solladha Sollala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solladha Sollala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullnenjil Yeno Kalavaram Purunjuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullnenjil Yeno Kalavaram Purunjuputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanala Kanala En Mela En Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanala Kanala En Mela En Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeya Erunju Putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya Erunju Putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Solladha Sollala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solladha Sollala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullnenjil Yeno Kalavaram Purunjuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullnenjil Yeno Kalavaram Purunjuputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Raagam Neethane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Raagam Neethane"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Vaalvin Geetham Naanthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaalvin Geetham Naanthane"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalodu Vaazhvene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalodu Vaazhvene"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Vaalvin Ellai Ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Vaalvin Ellai Ponaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maranthathillai En Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranthathillai En Idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Ninaikka Muppozhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ninaikka Muppozhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Karaiyavillai Un Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karaiyavillai Un Idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangugurene Eppozhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangugurene Eppozhuthum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalangugurene Eppozhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangugurene Eppozhuthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalinaale Ippozhuthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalinaale Ippozhuthum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jannal Oram Thendral Kaatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannal Oram Thendral Kaatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesum Bothile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum Bothile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Rendum Kadhalodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Rendum Kadhalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum Bothile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesum Bothile"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iyarkaiyadhu Viyanthidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyarkaiyadhu Viyanthidume"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Azhagil Dhinam Dhiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Azhagil Dhinam Dhiname"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhai Varume Mazhai Varume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Varume Mazhai Varume"/>
</div>
<div class="lyrico-lyrics-wrapper">En Manathukkul Puyal Varume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manathukkul Puyal Varume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasula Poongaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasula Poongaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paakum Dhisaiyil Veesum Bodhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paakum Dhisaiyil Veesum Bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Namakkunu Oru Desam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkunu Oru Desam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil Iruvarum Serndhu Onna Vaalvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Iruvarum Serndhu Onna Vaalvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Kannaala En Mela En Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Kannaala En Mela En Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeya Erunju Putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya Erunju Putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Solladha Sollala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solladha Sollala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullnenjil Yeno Kalavaram Purunjuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullnenjil Yeno Kalavaram Purunjuputta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Kannaala En Mela En Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Kannaala En Mela En Mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeya Erunju Putta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeya Erunju Putta"/>
</div>
<div class="lyrico-lyrics-wrapper">Solladha Sollala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solladha Sollala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullnenjil Yeno Kalavaram Purunjuputta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullnenjil Yeno Kalavaram Purunjuputta"/>
</div>
</pre>
