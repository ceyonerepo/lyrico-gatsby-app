---
title: "nilladhey nilladhey song lyrics"
album: "Lakshmi"
artist: "Sam CS"
lyricist: "Madhan Karky"
director: "AL Vijay"
path: "/albums/lakshmi-lyrics"
song: "Nilladhey Nilladhey"
image: ../../images/albumart/lakshmi.jpg
date: 2018-08-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Uw4031iUAnQ"
type: "happy"
singers:
  - Sathyaprakash 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nilladhae nilladhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilladhae nilladhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri thozhvi rendum ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri thozhvi rendum ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilladhae nilladhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilladhae nilladhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Netru ondru illai indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru ondru illai indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un nenjamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenjamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un porkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un porkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un veeramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un veeramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aayudham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnul achangal kodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnul achangal kodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekka poraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekka poraadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnul ayyangal yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnul ayyangal yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookka poraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookka poraadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kobam irakkam rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kobam irakkam rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala poraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala poraadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegam nidhaanam rendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegam nidhaanam rendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala poraadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala poraadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unadhoru thadaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhoru thadaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaithidum padaiyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaithidum padaiyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkoru ethiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkoru ethiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkoru pulari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkoru pulari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un nenjamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenjamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un porkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un porkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un veeramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un veeramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aayudham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilladhae nilladhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilladhae nilladhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri thozhvi rendum ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri thozhvi rendum ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilladhae nilladhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilladhae nilladhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Netru ondru illai indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru ondru illai indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sindhum thuliyinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sindhum thuliyinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhi moozhga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhi moozhga"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paarvai oliyinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvai oliyinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhai neezhga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai neezhga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sellum vazhiyinilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sellum vazhiyinilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookal veezhga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookal veezhga"/>
</div>
<div class="lyrico-lyrics-wrapper">Neel vaana veliyenavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neel vaana veliyenavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum vazhga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum vazhga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ovvor iravilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvor iravilum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iranthidu iranthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iranthidu iranthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puththam pudhidhena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puththam pudhidhena"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam dhinam piranthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam dhinam piranthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan mamadhaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan mamadhaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerithidu paninthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerithidu paninthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan kuraigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan kuraigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugazhena aninthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugazhena aninthidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhum manidhanin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhum manidhanin"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivedhu mudivedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivedhu mudivedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muyarchiyai niruthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyarchiyai niruthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimidam athu athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimidam athu athu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unthan thalaiyinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan thalaiyinil"/>
</div>
<div class="lyrico-lyrics-wrapper">Yera poguthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yera poguthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethirigal anivikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethirigal anivikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugudam athu athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugudam athu athu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un nenjamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenjamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un porkalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un porkalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un veeramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un veeramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aayudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aayudham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilladhae nilladhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilladhae nilladhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri thozhvi rendum ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri thozhvi rendum ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilladhae nilladhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilladhae nilladhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Netru ondru illai indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru ondru illai indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye aeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye aeae"/>
</div>
</pre>
