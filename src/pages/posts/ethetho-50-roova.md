---
title: "ethetho song lyrics"
album: "50 Roova"
artist: "VT. Bharathy - VT.Monish"
lyricist: "G. Panneerselvam"
director: "G. Panneerselvam"
path: "/albums/50-roova-lyrics"
song: "Ethetho"
image: ../../images/albumart/50-roova.jpg
date: 2019-12-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0jyHEwoF-Z8"
type: "love"
singers:
  - chitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ethotho ennam varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethotho ennam varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">yen intha maatram ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen intha maatram ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame unthan mugamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame unthan mugamai"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyuthe enthan kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyuthe enthan kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">unnil vilunthen innum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnil vilunthen innum "/>
</div>
<div class="lyrico-lyrics-wrapper">naan eleve illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan eleve illai"/>
</div>
<div class="lyrico-lyrics-wrapper">engo nadanthen thirumbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engo nadanthen thirumbi"/>
</div>
<div class="lyrico-lyrics-wrapper">naan varave illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan varave illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethotho ennam varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethotho ennam varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">yen intha maatram ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen intha maatram ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame unthan mugamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame unthan mugamai"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyuthe enthan kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyuthe enthan kannil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirai ketal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirai ketal "/>
</div>
<div class="lyrico-lyrics-wrapper">unaku tharuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku tharuven"/>
</div>
<div class="lyrico-lyrics-wrapper">unakum serthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakum serthe "/>
</div>
<div class="lyrico-lyrics-wrapper">moochu viduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochu viduven"/>
</div>
<div class="lyrico-lyrics-wrapper">naan endru ondre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan endru ondre"/>
</div>
<div class="lyrico-lyrics-wrapper">illamal ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illamal ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">naan kondathai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan kondathai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">unathaga aanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unathaga aanathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalin kaatidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalin kaatidum"/>
</div>
<div class="lyrico-lyrics-wrapper">puthiyatha nenjile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthiyatha nenjile"/>
</div>
<div class="lyrico-lyrics-wrapper">ketathe ithu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketathe ithu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">pidikuthe un perai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pidikuthe un perai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethotho ennam varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethotho ennam varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">yen intha maatram ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen intha maatram ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame unthan mugamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame unthan mugamai"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyuthe enthan kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyuthe enthan kannil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kurumbu seithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurumbu seithu"/>
</div>
<div class="lyrico-lyrics-wrapper">kobam etren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kobam etren"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai poole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai poole"/>
</div>
<div class="lyrico-lyrics-wrapper">mohathai maatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mohathai maatru"/>
</div>
<div class="lyrico-lyrics-wrapper">theeratha thulli 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeratha thulli "/>
</div>
<div class="lyrico-lyrics-wrapper">thinam thinam venume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam thinam venume"/>
</div>
<div class="lyrico-lyrics-wrapper">thiruda en thunba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiruda en thunba"/>
</div>
<div class="lyrico-lyrics-wrapper">thunbamum inbame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunbamum inbame"/>
</div>
<div class="lyrico-lyrics-wrapper">edukave illaiye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edukave illaiye "/>
</div>
<div class="lyrico-lyrics-wrapper">un mugam thannile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mugam thannile"/>
</div>
<div class="lyrico-lyrics-wrapper">innoru idhayamai thudikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innoru idhayamai thudikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">marakathe ne ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marakathe ne ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethotho ennam varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethotho ennam varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">yen intha maatram ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen intha maatram ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame unthan mugamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame unthan mugamai"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyuthe enthan kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyuthe enthan kannil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnil vilunthen innum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnil vilunthen innum "/>
</div>
<div class="lyrico-lyrics-wrapper">naan eleve illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan eleve illai"/>
</div>
<div class="lyrico-lyrics-wrapper">engo nadanthen thirumbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engo nadanthen thirumbi"/>
</div>
<div class="lyrico-lyrics-wrapper">naan varave illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan varave illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethotho ennam varuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethotho ennam varuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">yen intha maatram ennil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen intha maatram ennil"/>
</div>
<div class="lyrico-lyrics-wrapper">ellame unthan mugamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellame unthan mugamai"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyuthe enthan kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyuthe enthan kannil"/>
</div>
</pre>
