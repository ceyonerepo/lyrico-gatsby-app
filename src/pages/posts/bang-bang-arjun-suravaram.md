---
title: "bang bang song lyrics"
album: "Arjun Suravaram"
artist: "Sam CS"
lyricist: "Varikuppala Yadagiri - Spike"
director: "T Santhosh"
path: "/albums/arjun-suravaram-lyrics"
song: "Bang Bang"
image: ../../images/albumart/arjun-suravaram.jpg
date: 2019-11-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/tTUFF5oDQy0"
type: "happy"
singers:
  - Sam CS
  - Spike
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang"/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang"/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Dorikaavante 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Dorikaavante "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Lifey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Lifey"/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang"/>
</div>
<div class="lyrico-lyrics-wrapper">Urikinchesthaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urikinchesthaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Uchhuloki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchhuloki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang"/>
</div>
<div class="lyrico-lyrics-wrapper">Chikkaavante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikkaavante"/>
</div>
<div class="lyrico-lyrics-wrapper">Chethiloki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethiloki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthikaarestha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthikaarestha "/>
</div>
<div class="lyrico-lyrics-wrapper">Gallapatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gallapatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaada Nakkivunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaada Nakkivunna "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu Viduvaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu Viduvaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalamesi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalamesi "/>
</div>
<div class="lyrico-lyrics-wrapper">Pattukunta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattukunta "/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada Eeda Yeda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada Eeda Yeda "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Dorikinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Dorikinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Chedugudu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Chedugudu "/>
</div>
<div class="lyrico-lyrics-wrapper">Dada Choopin chestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dada Choopin chestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Alupuleni 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Alupuleni "/>
</div>
<div class="lyrico-lyrics-wrapper">Asalu Aata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalu Aata "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gelupu Nuve 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gelupu Nuve "/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">I Come To The City
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Come To The City"/>
</div>
<div class="lyrico-lyrics-wrapper">Im Feeling Da Heat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Im Feeling Da Heat"/>
</div>
<div class="lyrico-lyrics-wrapper">Im Feeling Da Heat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Im Feeling Da Heat"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya Know
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya Know"/>
</div>
<div class="lyrico-lyrics-wrapper">I Keep It Gritty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Keep It Gritty"/>
</div>
<div class="lyrico-lyrics-wrapper">Like Im Running 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Like Im Running "/>
</div>
<div class="lyrico-lyrics-wrapper">Em Streets
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em Streets"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya Im Running Em Streets
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya Im Running Em Streets"/>
</div>
<div class="lyrico-lyrics-wrapper">Keep Diggin Harder 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keep Diggin Harder "/>
</div>
<div class="lyrico-lyrics-wrapper">And Harder
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And Harder"/>
</div>
<div class="lyrico-lyrics-wrapper">Right Into The Mystery
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Right Into The Mystery"/>
</div>
<div class="lyrico-lyrics-wrapper">Getting In Deep
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Getting In Deep"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekatlonaa Cheekati Vaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekatlonaa Cheekati Vaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhayame Nenai Kuduputhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayame Nenai Kuduputhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Antheleka Parigeduthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antheleka Parigeduthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Nenai Kummuthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Nenai Kummuthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkati Okkati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkati Okkati"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekka Leni Tappula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekka Leni Tappula"/>
</div>
<div class="lyrico-lyrics-wrapper">Tadakane Tukkugaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tadakane Tukkugaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Chesi Chooputhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chesi Chooputhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gukkane Tippani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gukkane Tippani "/>
</div>
<div class="lyrico-lyrics-wrapper">Tikka Ukkapothanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tikka Ukkapothanai"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkare Istharaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkare Istharaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Yedure Vasthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedure Vasthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang"/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanananana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanananana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupuleni Asalu Aata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupuleni Asalu Aata "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gelupu Nuvve 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gelupu Nuvve "/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang"/>
</div>
<div class="lyrico-lyrics-wrapper">I Keep Ma Eyes Open
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Keep Ma Eyes Open"/>
</div>
<div class="lyrico-lyrics-wrapper">Whatever Bout To Go 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whatever Bout To Go "/>
</div>
<div class="lyrico-lyrics-wrapper">Down Unspoken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Down Unspoken"/>
</div>
<div class="lyrico-lyrics-wrapper">Living My Life 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Living My Life "/>
</div>
<div class="lyrico-lyrics-wrapper">Harder It Gets
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harder It Gets"/>
</div>
<div class="lyrico-lyrics-wrapper">Tougher The Will Unbroken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tougher The Will Unbroken"/>
</div>
<div class="lyrico-lyrics-wrapper">No Fear Of Flames
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Fear Of Flames"/>
</div>
<div class="lyrico-lyrics-wrapper">Dont Play No Games
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dont Play No Games"/>
</div>
<div class="lyrico-lyrics-wrapper">Aint Got 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aint Got "/>
</div>
<div class="lyrico-lyrics-wrapper">No Time For Jokers
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Time For Jokers"/>
</div>
<div class="lyrico-lyrics-wrapper">Keeping It Real
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keeping It Real"/>
</div>
<div class="lyrico-lyrics-wrapper">Da Truth Is Da Deal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Da Truth Is Da Deal"/>
</div>
<div class="lyrico-lyrics-wrapper">Spreading The Word In Open
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spreading The Word In Open"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanananana Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanananana Naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Alupuleni Asalu Aata 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alupuleni Asalu Aata "/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Ra"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Gelupu Nuvve 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Gelupu Nuvve "/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Raa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Raa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bang Bang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bang Bang"/>
</div>
</pre>
