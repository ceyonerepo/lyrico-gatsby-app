---
title: "setthuppoi pozhachavanda song lyrics"
album: "Madurai Manikuravar"
artist: "Isaignani Ilaiyaraaja"
lyricist: "Kavignar Muthulingam"
director: "K Raajarishi"
path: "/albums/madurai-manikuravar-lyrics"
song: "Setthuppoi Pozhachavanda"
image: ../../images/albumart/madurai-manikuravar.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pvtuj10CUs4"
type: "happy"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">sethupoi pulachavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethupoi pulachavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanaiyum mudichavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanaiyum mudichavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">sethupoi pulachavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethupoi pulachavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanaiyum mudichavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanaiyum mudichavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">sethupoi pulachavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethupoi pulachavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanaiyum mudichavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanaiyum mudichavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">angu irupen inga irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angu irupen inga irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">engum irupen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum irupen da"/>
</div>
<div class="lyrico-lyrics-wrapper">sangaliyil sangu aruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangaliyil sangu aruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sangathi mudipen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangathi mudipen da"/>
</div>
<div class="lyrico-lyrics-wrapper">illathatha sollatha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illathatha sollatha da"/>
</div>
<div class="lyrico-lyrics-wrapper">ilucha vaaya nee sonna sollu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilucha vaaya nee sonna sollu "/>
</div>
<div class="lyrico-lyrics-wrapper">sutham illa pulucha vaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutham illa pulucha vaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">illathatha sollatha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illathatha sollatha da"/>
</div>
<div class="lyrico-lyrics-wrapper">ilucha vaaya nee sonna sollu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilucha vaaya nee sonna sollu "/>
</div>
<div class="lyrico-lyrics-wrapper">sutham illa pulucha vaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutham illa pulucha vaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sethupoi pulachavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethupoi pulachavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanaiyum mudichavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanaiyum mudichavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">sethupoi pulachavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethupoi pulachavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanaiyum mudichavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanaiyum mudichavanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sethavan kaiyila than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethavan kaiyila than"/>
</div>
<div class="lyrico-lyrics-wrapper">irukura bandala pathuko da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukura bandala pathuko da"/>
</div>
<div class="lyrico-lyrics-wrapper">unaya sutha vittu kadavul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaya sutha vittu kadavul"/>
</div>
<div class="lyrico-lyrics-wrapper">oorukul vittu putan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorukul vittu putan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ithu varaikum nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu varaikum nee "/>
</div>
<div class="lyrico-lyrics-wrapper">thirincha ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirincha ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">nee enna padichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee enna padichu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna kilucha appappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna kilucha appappa"/>
</div>
<div class="lyrico-lyrics-wrapper">man pokkil pogum antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="man pokkil pogum antha"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkaiya athu un pokil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkaiya athu un pokil"/>
</div>
<div class="lyrico-lyrics-wrapper">pogathapa muthappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogathapa muthappa"/>
</div>
<div class="lyrico-lyrics-wrapper">puthanjathu ellam puthayal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthanjathu ellam puthayal"/>
</div>
<div class="lyrico-lyrics-wrapper">illa vaa pa antha puthayal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa vaa pa antha puthayal"/>
</div>
<div class="lyrico-lyrics-wrapper">na ethuvum oru puthusum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na ethuvum oru puthusum"/>
</div>
<div class="lyrico-lyrics-wrapper">illa po pa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illa po pa "/>
</div>
<div class="lyrico-lyrics-wrapper">unakum enakum oorukul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakum enakum oorukul"/>
</div>
<div class="lyrico-lyrics-wrapper">yarukum oru katha kutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarukum oru katha kutha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sethupoi pulachavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethupoi pulachavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanaiyum mudichavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanaiyum mudichavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">sethupoi pulachavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethupoi pulachavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanaiyum mudichavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanaiyum mudichavanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethana ingu iruku evan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethana ingu iruku evan"/>
</div>
<div class="lyrico-lyrics-wrapper">atha kaiyila kondu ponan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atha kaiyila kondu ponan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaiyila ullathu ellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaiyila ullathu ellam "/>
</div>
<div class="lyrico-lyrics-wrapper">kadaisiyil vitutu than poran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadaisiyil vitutu than poran"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu varaikum pathathula ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu varaikum pathathula ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">pathalum yarkum pothuvaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathalum yarkum pothuvaga"/>
</div>
<div class="lyrico-lyrics-wrapper">puthiyiyla aerala itha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthiyiyla aerala itha "/>
</div>
<div class="lyrico-lyrics-wrapper">purunjavanga menakeduthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="purunjavanga menakeduthu "/>
</div>
<div class="lyrico-lyrics-wrapper">sonnalum ada namma sanam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonnalum ada namma sanam "/>
</div>
<div class="lyrico-lyrics-wrapper">kettu innum thirunthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kettu innum thirunthala"/>
</div>
<div class="lyrico-lyrics-wrapper">pora vali pogum antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pora vali pogum antha"/>
</div>
<div class="lyrico-lyrics-wrapper">aadu maadu pola poguthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadu maadu pola poguthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagam innum aduchu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam innum aduchu "/>
</div>
<div class="lyrico-lyrics-wrapper">piduchu mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piduchu mela"/>
</div>
<div class="lyrico-lyrics-wrapper">ada idupu adupu idaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada idupu adupu idaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">iruku namma polapu ithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruku namma polapu ithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sethupoi pulachavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethupoi pulachavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanaiyum mudichavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanaiyum mudichavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">sethupoi pulachavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethupoi pulachavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanaiyum mudichavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanaiyum mudichavanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">angu irupen inga irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="angu irupen inga irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">engum irupen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum irupen da"/>
</div>
<div class="lyrico-lyrics-wrapper">sangaliyil sangu aruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangaliyil sangu aruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">sangathi mudipen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangathi mudipen da"/>
</div>
<div class="lyrico-lyrics-wrapper">illathatha sollatha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illathatha sollatha da"/>
</div>
<div class="lyrico-lyrics-wrapper">ilucha vaaya nee sonna sollu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilucha vaaya nee sonna sollu "/>
</div>
<div class="lyrico-lyrics-wrapper">sutham illa pulucha vaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutham illa pulucha vaaya"/>
</div>
<div class="lyrico-lyrics-wrapper">illathatha sollatha da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illathatha sollatha da"/>
</div>
<div class="lyrico-lyrics-wrapper">ilucha vaaya nee sonna sollu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilucha vaaya nee sonna sollu "/>
</div>
<div class="lyrico-lyrics-wrapper">sutham illa pulucha vaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutham illa pulucha vaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sethupoi pulachavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethupoi pulachavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanaiyum mudichavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanaiyum mudichavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">sethupoi pulachavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethupoi pulachavanda"/>
</div>
<div class="lyrico-lyrics-wrapper">ethanaiyum mudichavanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethanaiyum mudichavanda"/>
</div>
</pre>
