---
title: 'verithanam song lyrics'
album: 'Bigil'
artist: 'A R Rahman'
lyricist: 'Vivek'
director: 'Atlee'
path: '/albums/bigil-song-lyrics'
song: 'Verithanam'
image: ../../images/albumart/bigil.jpg
date: 2019-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rtd_VveAEaI"
type: 'mass'
singers: 
- Vijay
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Yaaranda aiaiyo yaaranda
<input type="checkbox" class="lyrico-select-lyric-line" value="Yaaranda aiaiyo yaaranda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiaiyaiyo yaaranda aiaiyo yaaranda
<input type="checkbox" class="lyrico-select-lyric-line" value="Aiaiyaiyo yaaranda aiaiyo yaaranda"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga vanthu yaaranda
<input type="checkbox" class="lyrico-select-lyric-line" value="Enga vanthu yaaranda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechikuna pirachana nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Vechikuna pirachana nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurala vuttadhu therinjitaakaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kurala vuttadhu therinjitaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku dhaan daa archana
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakku dhaan daa archana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Avan vara variakkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan vara variakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Voice ah kuduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Voice ah kuduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandu sindu thoguruthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nandu sindu thoguruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan elunthu kilunthu vanttaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Avan elunthu kilunthu vanttaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha deepawali nammalathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Indha deepawali nammalathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kudi irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kudi irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha ha ha verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Ha ha ha verithanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaa ippa localu naa
<input type="checkbox" class="lyrico-select-lyric-line" value="Innaa ippa localu naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma gethaa olaathanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma gethaa olaathanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjikulla kudi irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjikulla kudi irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Sanam Verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma Sanam Verithanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaa ippa localu naa
<input type="checkbox" class="lyrico-select-lyric-line" value="Innaa ippa localu naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma gethaa olaathanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma gethaa olaathanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aama alukka irupom
<input type="checkbox" class="lyrico-select-lyric-line" value="Aama alukka irupom"/>
</div>
  <div class="lyrico-lyrics-wrapper">Verithanam verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Verithanam verithanam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Karuppa kalaiyaruppom
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuppa kalaiyaruppom"/>
</div>
  <div class="lyrico-lyrics-wrapper">Verithanam verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Verithanam verithanam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Onna usuraa iruppom
<input type="checkbox" class="lyrico-select-lyric-line" value="Onna usuraa iruppom"/>
</div>
  <div class="lyrico-lyrics-wrapper">Verithanam verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Verithanam verithanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pullainga irukaanga vera inna vonum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pullainga irukaanga vera inna vonum"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavadi rasaava nippen da
<input type="checkbox" class="lyrico-select-lyric-line" value="Raavadi rasaava nippen da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda killaa mela
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennoda killaa mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei yaarukkum davlundu nee illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Yei yaarukkum davlundu nee illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dowlathaavae nillu en aalu nanbaa nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Dowlathaavae nillu en aalu nanbaa nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nenjikulla kudi irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjikulla kudi irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey namma sanam verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey namma sanam verithanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaa ippa localu naa
<input type="checkbox" class="lyrico-select-lyric-line" value="Innaa ippa localu naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma gethaa yaelaelooo……….
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma gethaa yaelaelooo………."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yei yei yei yei
<input type="checkbox" class="lyrico-select-lyric-line" value="Yei yei yei yei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Maalu maalu maalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Maalu maalu maalu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Thinakkuthathaku
<input type="checkbox" class="lyrico-select-lyric-line" value="Thinakkuthathaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thinaku thathakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Thinaku thathakku"/>
</div>
  <div class="lyrico-lyrics-wrapper">Suranganikka maalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Suranganikka maalu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Tha tha thogurum paa
<input type="checkbox" class="lyrico-select-lyric-line" value="Tha tha thogurum paa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Maalu maalu maalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Maalu maalu maalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suranganikka maalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Suranganikka maalu"/>
</div>
  <div class="lyrico-lyrics-wrapper">Maalu maalu maalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Maalu maalu maalu"/>
</div>
<div class="lyrico-lyrics-wrapper">En thalapathi dhaan dhoolu (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="En thalapathi dhaan dhoolu (2 times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Suraangani suraangani
<input type="checkbox" class="lyrico-select-lyric-line" value="Suraangani suraangani"/>
</div>
<div class="lyrico-lyrics-wrapper">Suraangani suraangani
<input type="checkbox" class="lyrico-select-lyric-line" value="Suraangani suraangani"/>
</div>
  <div class="lyrico-lyrics-wrapper">Suraaganika maalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Suraaganika maalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suranganika maalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Suranganika maalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Suraanganikka maalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Suraanganikka maalu"/>
</div>
<div class="lyrico-lyrics-wrapper">En thalapathi dhaan dhoolu
<input type="checkbox" class="lyrico-select-lyric-line" value="En thalapathi dhaan dhoolu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yei yei yei………
<input type="checkbox" class="lyrico-select-lyric-line" value="Yei yei yei………"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaana kanukkaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaana kanukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru aattam irukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru aattam irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mena minukkaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Mena minukkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru melam irukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru melam irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mannu muttu saalu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannu muttu saalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna vutta yaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna vutta yaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thonga vittu thuvaikkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thonga vittu thuvaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ravusa paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Ravusa paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Koralu vitta nooru
<input type="checkbox" class="lyrico-select-lyric-line" value="Koralu vitta nooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Sondham varum paaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Sondham varum paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu panam ellaam kolaaru
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaasu panam ellaam kolaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennaanda ellam nee thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennaanda ellam nee thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnandaa ellam naan thaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnandaa ellam naan thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma sokku oooru talku
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma sokku oooru talku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbaa nee pallaakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Nanbaa nee pallaakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Yekka ponnu yaelaelaeloo
<input type="checkbox" class="lyrico-select-lyric-line" value="Yekka ponnu yaelaelaeloo"/>
</div>
<div class="lyrico-lyrics-wrapper">Eyy mukka thuttu yaelaelaeloo
<input type="checkbox" class="lyrico-select-lyric-line" value="Eyy mukka thuttu yaelaelaeloo"/>
</div>
<div class="lyrico-lyrics-wrapper">Inna ippo localu naa
<input type="checkbox" class="lyrico-select-lyric-line" value="Inna ippo localu naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma gethaa yaelaelaeloo
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma gethaa yaelaelaeloo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aama alukka irupom
<input type="checkbox" class="lyrico-select-lyric-line" value="Aama alukka irupom"/>
</div>
  <div class="lyrico-lyrics-wrapper">Verithanam verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Verithanam verithanam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Karuppa kalaiyaruppom
<input type="checkbox" class="lyrico-select-lyric-line" value="Karuppa kalaiyaruppom"/>
</div>
  <div class="lyrico-lyrics-wrapper">Verithanam verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Verithanam verithanam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Onna usuraa iruppom
<input type="checkbox" class="lyrico-select-lyric-line" value="Onna usuraa iruppom"/>
</div>
  <div class="lyrico-lyrics-wrapper">Verithanam verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Verithanam verithanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pullainga irukaanga vera inna vonum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pullainga irukaanga vera inna vonum"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavadi rasaava nippen da
<input type="checkbox" class="lyrico-select-lyric-line" value="Raavadi rasaava nippen da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda killaa mela
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennoda killaa mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei yaarukkum davlundu nee illa
<input type="checkbox" class="lyrico-select-lyric-line" value="Yei yaarukkum davlundu nee illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dowlathaavae nillu en aalu nanbaa nee
<input type="checkbox" class="lyrico-select-lyric-line" value="Dowlathaavae nillu en aalu nanbaa nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nenjikulla kudi irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjikulla kudi irukkum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nenjikulla kudi irukkum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjikulla kudi irukkum"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hey namma sanam verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey namma sanam verithanam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Hey namma sanam verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey namma sanam verithanam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Innaa ippa localu naa
<input type="checkbox" class="lyrico-select-lyric-line" value="Innaa ippa localu naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma gethaa olaathanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Namma gethaa olaathanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Music and beats ……………………….
<input type="checkbox" class="lyrico-select-lyric-line" value="Music and beats"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nana nana na na na naa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nana nana na na na naa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Verithanam verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Verithanam verithanam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nana nana na na na naa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nana nana na na na naa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Verithanam verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Verithanam verithanam"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nana nana nan naa na naa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nana nana nan naa na naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaa nan naa nannaananaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Naanaa nan naa nannaananaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Verithanam verithanam
<input type="checkbox" class="lyrico-select-lyric-line" value="Verithanam verithanam"/>
</div>
</pre>