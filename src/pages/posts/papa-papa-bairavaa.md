---
title: "papa papa song lyrics"
album: "Bairavaa"
artist: "Santhosh Narayanan"
lyricist: "Vairamuthu"
director: "Bharathan"
path: "/albums/bairavaa-lyrics"
song: "PaPa PaPa"
image: ../../images/albumart/bairavaa.jpg
date: 2017-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xOlaVl9FAYk"
type: "happy"
singers:
  - Vijay
  - Priyadarshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Papa papa paparappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papa papa paparappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa vaa pa vaa pa vandhuadappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa vaa pa vaa pa vandhuadappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un aalappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un aalappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Top ah top ah dance aadappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Top ah top ah dance aadappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum deep ah deep ah love pannappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum deep ah deep ah love pannappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ne en aalappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne en aalappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbu Kodutha sontha aavi koduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu Kodutha sontha aavi koduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa vambu valatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa vambu valatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada aavi eduppen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aavi eduppen da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathi eduthu buthi theeti mudippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi eduthu buthi theeti mudippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagai kothi mudippen yeppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagai kothi mudippen yeppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engalappa nee engalappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalappa nee engalappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga elaarumae ini ungaalappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga elaarumae ini ungaalappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettavana vetti saaikka vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettavana vetti saaikka vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu kattabomman nee pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu kattabomman nee pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Papa papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papa papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Apadi poodu nalla vanghi kuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apadi poodu nalla vanghi kuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara evan Adada Eh eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara evan Adada Eh eh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariga naatama nadakkum ullooril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariga naatama nadakkum ullooril"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal pol vandhayae puliye puliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal pol vandhayae puliye puliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arivu oru kaiyil aruva maru kaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arivu oru kaiyil aruva maru kaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu dhaan en baani kiliye kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu dhaan en baani kiliye kiliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukku pathu per 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukku pathu per "/>
</div>
<div class="lyrico-lyrics-wrapper">un pole vandhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pole vandhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarukum theengilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarukum theengilla "/>
</div>
<div class="lyrico-lyrics-wrapper">vettri chelva vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettri chelva vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhu edhiri yaar inga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu edhiri yaar inga"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraadi paarunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadi paarunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Veroda veesunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veroda veesunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Urudhi edunga urimai adinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urudhi edunga urimai adinga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullenaiya naan ullenaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullenaiya naan ullenaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga ullamellam naan ullenaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga ullamellam naan ullenaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambi vandha naan nanmai seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambi vandha naan nanmai seiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga nandri podhum yeppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga nandri podhum yeppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullenaiya naan ullenaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullenaiya naan ullenaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga ullamellam naan ullenaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga ullamellam naan ullenaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambi vandha naan nanmai seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambi vandha naan nanmai seiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga nandri podhum yeppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga nandri podhum yeppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Papa papa paparappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papa papa paparappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa vaa pa vaa pa vandhuadappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa vaa pa vaa pa vandhuadappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Papa papa vaapa vaapa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papa papa vaapa vaapa"/>
</div>
<div class="lyrico-lyrics-wrapper">Popa popa top ah top ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Popa popa top ah top ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kelekke illama desaigal moonachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelekke illama desaigal moonachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Emakku nee thaanae kelakku kelakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emakku nee thaanae kelakku kelakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velicham varumattum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velicham varumattum "/>
</div>
<div class="lyrico-lyrics-wrapper">kelakkum karuppu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kelakkum karuppu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Irutta thee vachi koluthu koluthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irutta thee vachi koluthu koluthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru vaarthai sonnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vaarthai sonnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oore un pinnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oore un pinnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaaya munnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaaya munnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yutham seiya vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yutham seiya vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yuththangal illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuththangal illama"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhigasam nikkadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhigasam nikkadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rathangal sindhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rathangal sindhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaga theemai olivadhedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaga theemai olivadhedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullenaiya naan ullenaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullenaiya naan ullenaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga ullamellam naan ullenaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga ullamellam naan ullenaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambi vandha naan nanmai seiven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambi vandha naan nanmai seiven"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga nandri podhum yeppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga nandri podhum yeppa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engalappa nee engalappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalappa nee engalappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingha elaarume ini ungaalappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingha elaarume ini ungaalappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettavana vetti saaikka vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettavana vetti saaikka vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu kattabomman nee pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu kattabomman nee pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engalappa nee engalappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engalappa nee engalappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingha elaarume ini ungaalappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingha elaarume ini ungaalappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettavana vetti saaikka vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettavana vetti saaikka vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu kattabomman nee pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu kattabomman nee pa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbu Kodutha sontha aavi koduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbu Kodutha sontha aavi koduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa vambu valatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa vambu valatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada aavi eduppen da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada aavi eduppen da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathi eduthu buthi theeti mudippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi eduthu buthi theeti mudippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagai kothi mudippen yeppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagai kothi mudippen yeppa"/>
</div>
</pre>
