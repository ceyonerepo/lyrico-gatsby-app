---
title: "nadamadum sudugada song lyrics"
album: "Nil Gavani Sellathey"
artist: "Selvaganesh"
lyricist: "J Francis Kriba"
director: "Anand Chakravarthy"
path: "/albums/nil-gavani-sellathey-lyrics"
song: "Nadamadum Sudugada"
image: ../../images/albumart/nil-gavani-sellathey.jpg
date: 2011-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GuXaLjtwAzs"
type: "mass"
singers:
  - VV Prasanna
  - Krishna Iyer
  - Rashmi Vijayan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Theeppori Theeppori Theeraavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeppori Theeppori Theeraavali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Azhugaikku Idhuthaan Irudhivazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Azhugaikku Idhuthaan Irudhivazhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeppori Theeppori Theeraavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeppori Theeppori Theeraavali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Azhugaikku Idhuthaan Irudhivazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Azhugaikku Idhuthaan Irudhivazhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Vazhi Orey Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vazhi Orey Vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Uyirukkul Paravudhu Pudhu Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Uyirukkul Paravudhu Pudhu Vazhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Vazhi Orey Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vazhi Orey Vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Uyirukkul Paravudhu Pudhu Vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Uyirukkul Paravudhu Pudhu Vazhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadamaadum Sudugaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadamaadum Sudugaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadamaadum Sudugaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadamaadum Sudugaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathaanin Varalaaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathaanin Varalaaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Saathaanin Varalaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathaanin Varalaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naragamthaan Ivan Ooraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naragamthaan Ivan Ooraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naragamthaan Ivan Ooraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naragamthaan Ivan Ooraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam Thaan Ivan Peraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam Thaan Ivan Peraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam Thaan Ivan Peraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam Thaan Ivan Peraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moolaikkul Adithappuyal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolaikkul Adithappuyal Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Murindhu Vizhundha Kadavul Ivano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murindhu Vizhundha Kadavul Ivano"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanakkul Irundha Manidhanai Kondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakkul Irundha Manidhanai Kondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaney Thindra Eman Ivano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaney Thindra Eman Ivano"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Yaaro Ivan Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaaro Ivan Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Saavai Poovaai Varukkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saavai Poovaai Varukkiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Yaaro Ivan Yaaro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaaro Ivan Yaaro "/>
</div>
<div class="lyrico-lyrics-wrapper">Moodiya Thenaai Gudhikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodiya Thenaai Gudhikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadamaadum Sudugaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadamaadum Sudugaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadamaadum Sudugaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadamaadum Sudugaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathaanin Varalaaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathaanin Varalaaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Saathaanin Varalaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathaanin Varalaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naragamthaan Ivan Ooraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naragamthaan Ivan Ooraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naragamthaan Ivan Ooraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naragamthaan Ivan Ooraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam Thaan Ivan Peraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam Thaan Ivan Peraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam Thaan Ivan Peraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam Thaan Ivan Peraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattinikku Vedham Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattinikku Vedham Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasi Vandhaaley Maarum Ellai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasi Vandhaaley Maarum Ellai"/>
</div>
<div class="lyrico-lyrics-wrapper">Netruvarai Ivan Sinnappillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netruvarai Ivan Sinnappillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Indrumudhal Ivan Manidhan Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indrumudhal Ivan Manidhan Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetta Veliyil Patta Maramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetta Veliyil Patta Maramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Seththa Vidhaiyil Vedithuvarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththa Vidhaiyil Vedithuvarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Yaaro Ivan Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaaro Ivan Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaro Ivan Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Ivan Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipboomi Ingey Sutravillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipboomi Ingey Sutravillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheedhum Nandrum Therivadhummillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheedhum Nandrum Therivadhummillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththa Boadhaiyum Inikkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththa Boadhaiyum Inikkiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavulaiyum Kollathudikkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulaiyum Kollathudikkiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivappu Iravai Sillarai Maatri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivappu Iravai Sillarai Maatri"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuppu Payanathil Selavugal Seiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppu Payanathil Selavugal Seiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Yaaro Ivan Yaaro I Va N Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaaro Ivan Yaaro I Va N Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Yaaro Ivan Yaaro I Va N Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaaro Ivan Yaaro I Va N Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadamaadum Sudugaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadamaadum Sudugaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadamaadum Sudugaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadamaadum Sudugaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathaanin Varalaaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathaanin Varalaaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Saathaanin Varalaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathaanin Varalaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naragamthaan Ivan Ooraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naragamthaan Ivan Ooraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naragamthaan Ivan Ooraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naragamthaan Ivan Ooraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam Thaan Ivan Peraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam Thaan Ivan Peraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam Thaan Ivan Peraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam Thaan Ivan Peraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochukkaatrum Vishamaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochukkaatrum Vishamaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgal Paththum Aayudhamaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgal Paththum Aayudhamaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalaneram Atruppoanaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalaneram Atruppoanaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhumboadhey Seththuppoanaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhumboadhey Seththuppoanaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai Urutti Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannai Urutti Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Miratti Vaettai Kuviyalai Pangupirikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miratti Vaettai Kuviyalai Pangupirikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Yaaro Ivan Yaaro, Yaaro Ivan Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaaro Ivan Yaaro, Yaaro Ivan Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththam Chottum Thaazhamboovin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththam Chottum Thaazhamboovin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattil Sutrum Vishappaambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattil Sutrum Vishappaambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottumboadhum Sirikkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottumboadhum Sirikkiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpil Theeyum Mulaikkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpil Theeyum Mulaikkiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgal Punarndhu Pillaippetru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgal Punarndhu Pillaippetru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paeyin Nizhalil Thottil Kattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paeyin Nizhalil Thottil Kattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Yaaro Ivan Yaaro I Va N Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaaro Ivan Yaaro I Va N Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Yaaro Ivan Yaaro, Yaaro Ivan Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaaro Ivan Yaaro, Yaaro Ivan Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththam Chottum Thaazhamboovin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththam Chottum Thaazhamboovin"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattil Sutrum Vishappaambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattil Sutrum Vishappaambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottumboadhum Sirikkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottumboadhum Sirikkiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Natpil Theeyum Mulaikkiradhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpil Theeyum Mulaikkiradhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Viralgal Punarndhu Pillaippetru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgal Punarndhu Pillaippetru"/>
</div>
<div class="lyrico-lyrics-wrapper">Paeyin Nizhalil Thottil Kattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paeyin Nizhalil Thottil Kattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Yaaro Ivan Yaaro I Va N Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Yaaro Ivan Yaaro I Va N Yaaro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadamaadum Sudugaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadamaadum Sudugaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadamaadum Sudugaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadamaadum Sudugaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathaanin Varalaaraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathaanin Varalaaraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Saathaanin Varalaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathaanin Varalaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naragamthaan Ivan Ooraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naragamthaan Ivan Ooraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Naragamthaan Ivan Ooraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naragamthaan Ivan Ooraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam Thaan Ivan Peraa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam Thaan Ivan Peraa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam Thaan Ivan Peraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam Thaan Ivan Peraa"/>
</div>
</pre>
