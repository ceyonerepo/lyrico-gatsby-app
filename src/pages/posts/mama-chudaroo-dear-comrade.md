---
title: "mama chudaroo song lyrics"
album: "Dear Comrade"
artist: "Justin Prabhakaran"
lyricist: "Rehman"
director: "Bharat Kamma"
path: "/albums/dear-comrade-lyrics"
song: "Mama Chudaroo"
image: ../../images/albumart/dear-comrade.jpg
date: 2019-07-26
lang: telugu
youtubeLink: "https://www.youtube.com/embed/U_mREkAuyn4"
type: "happy"
singers:
  - Naresh Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaalam Veiyaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaalam Veiyaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarikotthaga Paate Paadaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarikotthaga Paate Paadaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Melam Oodharo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melam Oodharo"/>
</div>
<div class="lyrico-lyrics-wrapper">Innallaku Idhi Kudhirindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innallaku Idhi Kudhirindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Maama Chudaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama Chudaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha Malli Modhalaiyindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha Malli Modhalaiyindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusthaavendhiro Dharuvesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusthaavendhiro Dharuvesthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduge Veiyaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduge Veiyaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadiyaaram Chudoddhuroo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadiyaaram Chudoddhuroo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Gathakaalam Yegirocchindhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Gathakaalam Yegirocchindhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Saradhaala Sandhallaloo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saradhaala Sandhallaloo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv Dhuniyake Aadeiyaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv Dhuniyake Aadeiyaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sneham Dhooramai (Intha Kaalam)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sneham Dhooramai (Intha Kaalam)"/>
</div>
<div class="lyrico-lyrics-wrapper">Eenade Cheruvai (Gunde Praanam)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eenade Cheruvai (Gunde Praanam)"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyani (000) Gnyapakam (Ooo)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyani (000) Gnyapakam (Ooo)"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundey Thakithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundey Thakithey"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeyani (Hey) Gnyapakam (Hey)
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyani (Hey) Gnyapakam (Hey)"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundey Thakithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundey Thakithey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Thakithey Aa Thakithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Thakithey Aa Thakithey"/>
</div>
<div class="lyrico-lyrics-wrapper">Tha Tha Tha Thakithey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tha Tha Tha Thakithey"/>
</div>
<div class="lyrico-lyrics-wrapper">Gurthundeti Yenno Yenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthundeti Yenno Yenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandhale Velluvalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandhale Velluvalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Munchesthunte Yentha Baagundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munchesthunte Yentha Baagundho"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Sandhoham Lo Manasemaiyindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Sandhoham Lo Manasemaiyindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika Yennalaina Antham Kaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Yennalaina Antham Kaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhalenno Ninnu Nannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhalenno Ninnu Nannu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhisthunte Yentha Baagundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhisthunte Yentha Baagundho"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Bandham Lona Yentha Balamundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Bandham Lona Yentha Balamundho"/>
</div>
</pre>
