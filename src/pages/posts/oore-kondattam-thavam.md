---
title: "oore kondattam song lyrics"
album: "Thavam"
artist: "Srikanth Deva"
lyricist: "Indumathi Pakirisamy"
director: "R Vijay Anand - AR Suriyan"
path: "/albums/thavam-lyrics"
song: "Oore Kondattam"
image: ../../images/albumart/thavam.jpg
date: 2019-11-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xqQrEUK_sps"
type: "happy"
singers:
  - Guru Ayyadurai
  - Suren Sekaran
  - G Ebi
  - Reshmi
  - Varanya Srikanth
  - Sashiya Srikanth
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">malai malai vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai malai vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">malai malai vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai malai vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">malai malai vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai malai vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">malaiye malaiye vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malaiye malaiye vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mannai kakka vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannai kakka vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">payirgal vaduthu vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payirgal vaduthu vaa vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">panjam pokka vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panjam pokka vaa vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oore kondattam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore kondattam "/>
</div>
<div class="lyrico-lyrics-wrapper">ullam santhosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam santhosam "/>
</div>
<div class="lyrico-lyrics-wrapper">engum pongattum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum pongattum "/>
</div>
<div class="lyrico-lyrics-wrapper">mala vara poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mala vara poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">eeri eeratum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeri eeratum "/>
</div>
<div class="lyrico-lyrics-wrapper">pamba neraiyatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pamba neraiyatum"/>
</div>
<div class="lyrico-lyrics-wrapper">kulame kuliratum  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulame kuliratum  "/>
</div>
<div class="lyrico-lyrics-wrapper">mala vara poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mala vara poguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">megame malai megame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megame malai megame"/>
</div>
<div class="lyrico-lyrics-wrapper">malai polivai vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai polivai vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaveri neer kanalai ponathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaveri neer kanalai ponathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ilanthom thavithom vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilanthom thavithom vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">megame kaar megame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megame kaar megame"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam karunai polivai vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam karunai polivai vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vasamai malai vasamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasamai malai vasamai"/>
</div>
<div class="lyrico-lyrics-wrapper">intha tharani selika vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha tharani selika vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neeru nelam neranja ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeru nelam neranja ka"/>
</div>
<div class="lyrico-lyrics-wrapper">eela senam vaalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eela senam vaalume"/>
</div>
<div class="lyrico-lyrics-wrapper">mupogam velanja ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mupogam velanja ka"/>
</div>
<div class="lyrico-lyrics-wrapper">vevasayam valume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vevasayam valume "/>
</div>
<div class="lyrico-lyrics-wrapper">neeru nelam neranja ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeru nelam neranja ka"/>
</div>
<div class="lyrico-lyrics-wrapper">eela senam vaalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eela senam vaalume"/>
</div>
<div class="lyrico-lyrics-wrapper">mupogam velanja ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mupogam velanja ka"/>
</div>
<div class="lyrico-lyrics-wrapper">vevasayam valume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vevasayam valume "/>
</div>
<div class="lyrico-lyrics-wrapper">intha ooru elaiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha ooru elaiya "/>
</div>
<div class="lyrico-lyrics-wrapper">kaakum iyanaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaakum iyanaru "/>
</div>
<div class="lyrico-lyrics-wrapper">karuppa samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuppa samy"/>
</div>
<div class="lyrico-lyrics-wrapper">intha ulagam muthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha ulagam muthum"/>
</div>
<div class="lyrico-lyrics-wrapper">kakum malai tharum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kakum malai tharum "/>
</div>
<div class="lyrico-lyrics-wrapper">varunan kadavul samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varunan kadavul samy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oore kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam santhosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam santhosam "/>
</div>
<div class="lyrico-lyrics-wrapper">engum pongattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum pongattum"/>
</div>
<div class="lyrico-lyrics-wrapper">mala vara poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mala vara poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">eeri eeratum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeri eeratum "/>
</div>
<div class="lyrico-lyrics-wrapper">pamba neraiyatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pamba neraiyatum"/>
</div>
<div class="lyrico-lyrics-wrapper">kulame kuliratum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulame kuliratum "/>
</div>
<div class="lyrico-lyrics-wrapper">mala vara poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mala vara poguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaniko panjam vanthachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniko panjam vanthachu"/>
</div>
<div class="lyrico-lyrics-wrapper">thamilar veeram kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamilar veeram kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">oor pesum katha aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor pesum katha aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kan moodi thungi naal aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan moodi thungi naal aachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaal vayithu kanji kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaal vayithu kanji kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo engaluku kanavachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo engaluku kanavachu"/>
</div>
<div class="lyrico-lyrics-wrapper">valuvanin elutha paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valuvanin elutha paru"/>
</div>
<div class="lyrico-lyrics-wrapper">varalaru puriyum paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varalaru puriyum paru"/>
</div>
<div class="lyrico-lyrics-wrapper">neer indri ulagam mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neer indri ulagam mella"/>
</div>
<div class="lyrico-lyrics-wrapper">saagum paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saagum paru"/>
</div>
<div class="lyrico-lyrics-wrapper">valuvanin elutha paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valuvanin elutha paru"/>
</div>
<div class="lyrico-lyrics-wrapper">varalaru puriyum paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varalaru puriyum paru"/>
</div>
<div class="lyrico-lyrics-wrapper">neer indri ulagam mella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neer indri ulagam mella"/>
</div>
<div class="lyrico-lyrics-wrapper">saagum paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saagum paru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">em patta sonnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="em patta sonnale"/>
</div>
<div class="lyrico-lyrics-wrapper">ethir kalam munnotam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethir kalam munnotam"/>
</div>
<div class="lyrico-lyrics-wrapper">tamil gnanam sollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil gnanam sollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">vingnanam vera enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vingnanam vera enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oore kondattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore kondattam"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam santhosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam santhosam "/>
</div>
<div class="lyrico-lyrics-wrapper">engum pongattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum pongattum"/>
</div>
<div class="lyrico-lyrics-wrapper">mala vara poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mala vara poguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">eeri eeratum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeri eeratum "/>
</div>
<div class="lyrico-lyrics-wrapper">pamba neraiyatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pamba neraiyatum"/>
</div>
<div class="lyrico-lyrics-wrapper">kulame kuliratum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulame kuliratum "/>
</div>
<div class="lyrico-lyrics-wrapper">mala vara poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mala vara poguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">megame malai megame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megame malai megame"/>
</div>
<div class="lyrico-lyrics-wrapper">malai polivai vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai polivai vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaveri neer kanalai ponathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaveri neer kanalai ponathu"/>
</div>
<div class="lyrico-lyrics-wrapper">ilanthom thavithom vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilanthom thavithom vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">megame kaar megame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megame kaar megame"/>
</div>
<div class="lyrico-lyrics-wrapper">konjam karunai polivai vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konjam karunai polivai vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vasamai malai vasamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vasamai malai vasamai"/>
</div>
<div class="lyrico-lyrics-wrapper">intha tharani selika vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha tharani selika vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">neeru nelam neranja ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeru nelam neranja ka"/>
</div>
<div class="lyrico-lyrics-wrapper">eela senam vaalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eela senam vaalume"/>
</div>
<div class="lyrico-lyrics-wrapper">mupogam velanja ka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mupogam velanja ka"/>
</div>
<div class="lyrico-lyrics-wrapper">vevasayam valume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vevasayam valume "/>
</div>
<div class="lyrico-lyrics-wrapper">intha ooru elaiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha ooru elaiya "/>
</div>
<div class="lyrico-lyrics-wrapper">kaakum iyanaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaakum iyanaru "/>
</div>
<div class="lyrico-lyrics-wrapper">karuppa samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuppa samy"/>
</div>
<div class="lyrico-lyrics-wrapper">intha ulagam muthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha ulagam muthum"/>
</div>
<div class="lyrico-lyrics-wrapper">kakum malai tharum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kakum malai tharum "/>
</div>
<div class="lyrico-lyrics-wrapper">varunan kadavul samy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varunan kadavul samy"/>
</div>
</pre>
