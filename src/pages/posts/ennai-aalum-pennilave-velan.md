---
title: "ennai aalum pennilave song lyrics"
album: "Velan"
artist: "Gopi Sundar"
lyricist: "Uma Devi"
director: "Kavin"
path: "/albums/velan-lyrics"
song: "Ennai Aalum Pennilave"
image: ../../images/albumart/velan.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Y7uI_CJ_vDk"
type: "love"
singers:
  - Pradeep Kumar
  - Priyanka NK
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ennai aalum pennilave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai aalum pennilave"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathu vaanam naane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathu vaanam naane"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thaandi enthan vaazhvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thaandi enthan vaazhvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrum illai vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrum illai vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravai nanaikkum paniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravai nanaikkum paniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakalai thirakkum kathire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakalai thirakkum kathire"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyai thaandum azhagai pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyai thaandum azhagai pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namathu paalai vasantham aakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namathu paalai vasantham aakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhve vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhve vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annai thedum pilla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai thedum pilla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum unnai theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum unnai theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbin kaattil kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbin kaattil kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oottril naalum moozhgi poka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oottril naalum moozhgi poka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvil kalanthaay uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvil kalanthaay uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai naan piriyen iniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai naan piriyen iniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alai kadal serkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alai kadal serkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulir manal pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulir manal pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai yenthi kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai yenthi kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Yengum kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yengum kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkena naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkena neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkena neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirantha piraku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirantha piraku"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirithal enbathethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirithal enbathethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravai nanaikkum paniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravai nanaikkum paniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakalai thirakkum kathire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakalai thirakkum kathire"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhiyai neram tholainthu pokum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyai neram tholainthu pokum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namathu paalai vasantham aakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namathu paalai vasantham aakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhve vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhve vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annai thedum pilla pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai thedum pilla pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanum unnai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanum unnai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thaandi enthan vaazhvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thaandi enthan vaazhvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondrum illai vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondrum illai vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvil kalanthaay uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvil kalanthaay uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai naan piriyen iniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai naan piriyen iniye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhiyai neram tholainthu pokum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhiyai neram tholainthu pokum"/>
</div>
<div class="lyrico-lyrics-wrapper">Namathu paalai vasantham aakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namathu paalai vasantham aakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhve vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhve vaa vaa"/>
</div>
</pre>
