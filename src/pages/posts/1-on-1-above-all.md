---
title: "1 on 1 song lyrics"
album: "Above All"
artist: "Gur Sidhu"
lyricist: "Jassa Dhillon"
director: "Japjeet Dhillon"
path: "/albums/above-all-lyrics"
song: "1 on 1"
image: ../../images/albumart/above-all.jpg
date: 2021-05-11
lang: punjabi
youtubeLink: "https://www.youtube.com/embed/zk04E79riMQ"
type: "Mass"
singers:
  - Jassa Dhillon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Rank ae pound'an te wadde aa khwaab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rank ae pound'an te wadde aa khwaab"/>
</div>
<div class="lyrico-lyrics-wrapper">Down ae dhillon te kehnde aa sab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Down ae dhillon te kehnde aa sab"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho theek ae jiddan vi changge aa maahde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho theek ae jiddan vi changge aa maahde"/>
</div>
<div class="lyrico-lyrics-wrapper">Bande je sidhe te tedhe vi baahle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bande je sidhe te tedhe vi baahle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ankhan ton padh de aa lagge jo shakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ankhan ton padh de aa lagge jo shakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Horaan de sir te feel ni chakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Horaan de sir te feel ni chakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh chakki ae bit te poori ae balle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh chakki ae bit te poori ae balle"/>
</div>
<div class="lyrico-lyrics-wrapper">Squad ae lit te baaki ae thalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Squad ae lit te baaki ae thalle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh jinne vi auna aa kalle nu kalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh jinne vi auna aa kalle nu kalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jinne vi auna aa kalle nu kalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jinne vi auna aa kalle nu kalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalle nu kalle ho kalle nu kalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalle nu kalle ho kalle nu kalle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho saade na ik na piche na chaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho saade na ik na piche na chaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Hikk ch vajjde addi na laali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hikk ch vajjde addi na laali"/>
</div>
<div class="lyrico-lyrics-wrapper">Loafer look te poori ae thukk
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loafer look te poori ae thukk"/>
</div>
<div class="lyrico-lyrics-wrapper">Pehle ishare naa lende aa chukk
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehle ishare naa lende aa chukk"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haare aa ghatt te jitte aa wadh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haare aa ghatt te jitte aa wadh"/>
</div>
<div class="lyrico-lyrics-wrapper">Haande te maare aa tere ch kath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haande te maare aa tere ch kath"/>
</div>
<div class="lyrico-lyrics-wrapper">Sukke na mude aa jinne vi aaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sukke na mude aa jinne vi aaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaara ne vajjeya waang vajaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaara ne vajjeya waang vajaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hash te cash te wadhu lutaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hash te cash te wadhu lutaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jidde di nakhro kargi bye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jidde di nakhro kargi bye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jidde di nakhro kargi bye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jidde di nakhro kargi bye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Feel aa less te life ae chess
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feel aa less te life ae chess"/>
</div>
<div class="lyrico-lyrics-wrapper">Taanhi tan laad machayi ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taanhi tan laad machayi ee"/>
</div>
<div class="lyrico-lyrics-wrapper">Jatt di beat te leni aa hauke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jatt di beat te leni aa hauke"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaad tan maybe aayi ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaad tan maybe aayi ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rehnde aa shoot te shooter shikaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rehnde aa shoot te shooter shikaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakke nishane te pakke likhari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakke nishane te pakke likhari"/>
</div>
<div class="lyrico-lyrics-wrapper">Aun de vaari dekhi garrari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aun de vaari dekhi garrari"/>
</div>
<div class="lyrico-lyrics-wrapper">Khullugi naddi vi gaddi di baari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khullugi naddi vi gaddi di baari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho pind'an ton uthe aa uthe barole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho pind'an ton uthe aa uthe barole"/>
</div>
<div class="lyrico-lyrics-wrapper">Shor ae kabbe te ohne hi raule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shor ae kabbe te ohne hi raule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sidhu te gunn di bamb aa jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sidhu te gunn di bamb aa jodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sikh te shauk te nasal di ghodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikh te shauk te nasal di ghodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Umar ae thodi vaddi aa trip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umar ae thodi vaddi aa trip"/>
</div>
<div class="lyrico-lyrics-wrapper">Vibe ae vibe ae vibe ae sick
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vibe ae vibe ae vibe ae sick"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lokan di sadd'di yaara di chandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokan di sadd'di yaara di chandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Car'an si 3 main hor le aandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Car'an si 3 main hor le aandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Loat aa kamm na laggi scheme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loat aa kamm na laggi scheme"/>
</div>
<div class="lyrico-lyrics-wrapper">Kill vi kite na dekhe dream
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kill vi kite na dekhe dream"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saal ae dooja te mainstream
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saal ae dooja te mainstream"/>
</div>
<div class="lyrico-lyrics-wrapper">Saal ae dooja te mainstream
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saal ae dooja te mainstream"/>
</div>
<div class="lyrico-lyrics-wrapper">Peak te ghumdi yaaran di team
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peak te ghumdi yaaran di team"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Frame ch chaubar ae game ch kinne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Frame ch chaubar ae game ch kinne"/>
</div>
<div class="lyrico-lyrics-wrapper">Jehda ae mannda jatt de seene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jehda ae mannda jatt de seene"/>
</div>
<div class="lyrico-lyrics-wrapper">Baaz na kithon ralde aa cheene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaz na kithon ralde aa cheene"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinne vi rakhe aa yaar nagine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinne vi rakhe aa yaar nagine"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jinne vi rakhe aa yaar nagine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jinne vi rakhe aa yaar nagine"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baazi vi layi ae, dooni chadhayi ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baazi vi layi ae, dooni chadhayi ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Changge record na back dikhayi ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Changge record na back dikhayi ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Todeya naake, khulle je khaate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Todeya naake, khulle je khaate"/>
</div>
<div class="lyrico-lyrics-wrapper">Soch ton pare jo hind pugayi ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soch ton pare jo hind pugayi ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unchi ae deak te unche aa jack
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unchi ae deak te unche aa jack"/>
</div>
<div class="lyrico-lyrics-wrapper">Desi vi poore te aundi ae dakk
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Desi vi poore te aundi ae dakk"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheez ae munde di rees na koyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheez ae munde di rees na koyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Check je kare te fees na koyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Check je kare te fees na koyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yaara di yaari ch lease na koyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yaara di yaari ch lease na koyi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh charcha vi to khaali si challe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh charcha vi to khaali si challe"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugge ni kacche aa daam aa palle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugge ni kacche aa daam aa palle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho jinne vi auna aa kalle nu kalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jinne vi auna aa kalle nu kalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jinne vi auna aa kalle nu kalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jinne vi auna aa kalle nu kalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalle nu kalle..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalle nu kalle.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalle nu kalle, oh kalle nu kalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalle nu kalle, oh kalle nu kalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh kalle nu kalle, oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh kalle nu kalle, oh"/>
</div>
</pre>
