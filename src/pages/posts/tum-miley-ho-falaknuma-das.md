---
title: "tum miley ho song lyrics"
album: "Falaknuma Das"
artist: "Vivek Sagar"
lyricist: "Farooq Bakshi"
director: "Vishwak Sen"
path: "/albums/falaknuma-das-lyrics"
song: "Tum Miley Ho"
image: ../../images/albumart/falaknuma-das.jpg
date: 2019-05-31
lang: telugu
youtubeLink: "https://www.youtube.com/embed/hE1AyyCcAHg"
type: "happy"
singers:
  - Kailash Kher
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">deh nain bin chandra rain bin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deh nain bin chandra rain bin"/>
</div>
<div class="lyrico-lyrics-wrapper">dariyaa lahar binaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dariyaa lahar binaa"/>
</div>
<div class="lyrico-lyrics-wrapper">saagar thahar binaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saagar thahar binaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tum mile ho muje 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tum mile ho muje "/>
</div>
<div class="lyrico-lyrics-wrapper">achaanak yoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achaanak yoon"/>
</div>
<div class="lyrico-lyrics-wrapper">tum mile ho muje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tum mile ho muje"/>
</div>
<div class="lyrico-lyrics-wrapper">achaanak yoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achaanak yoon"/>
</div>
<div class="lyrico-lyrics-wrapper">kaab hai ya koyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaab hai ya koyi"/>
</div>
<div class="lyrico-lyrics-wrapper">haqiqat hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haqiqat hai"/>
</div>
<div class="lyrico-lyrics-wrapper">abhee dil ko yaqeen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="abhee dil ko yaqeen"/>
</div>
<div class="lyrico-lyrics-wrapper">nahin aathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nahin aathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">tum mile ho muje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tum mile ho muje"/>
</div>
<div class="lyrico-lyrics-wrapper">achaanak yoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="achaanak yoon"/>
</div>
</pre>
