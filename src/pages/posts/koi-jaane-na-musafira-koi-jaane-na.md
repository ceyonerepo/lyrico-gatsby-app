---
title: "koi jaane na - musafira song lyrics"
album: "Koi Jaane Na"
artist: "Amaal Mallik"
lyricist: "Kumaar"
director: "Amin Hajee"
path: "/albums/koi-jaane-na-lyrics"
song: "Koi Jaane Na - Musafira"
image: ../../images/albumart/koi-jaane-na.jpg
date: 2021-03-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/2mcnCK3gmq4"
type: "title song"
singers:
  - Armaan Malik
  - Tulsi Kumar
  - Amaal Mallik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Musafira sa dil mera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musafira sa dil mera"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu mili tham gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu mili tham gaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Meharban jo tu hua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meharban jo tu hua"/>
</div>
<div class="lyrico-lyrics-wrapper">Khushi mili gham gaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khushi mili gham gaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pehle sa kuch bhi hai nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pehle sa kuch bhi hai nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lagun main khud ko ajnabi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lagun main khud ko ajnabi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere kareeb hun naseeb yun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere kareeb hun naseeb yun"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadke hai abhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhadke hai abhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yahi ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahi ishq hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yahi ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahi ishq hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi jaane na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi jaane na"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi jaane na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi jaane na"/>
</div>
<div class="lyrico-lyrics-wrapper">Yahi ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahi ishq hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yahi ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahi ishq hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yahi ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahi ishq hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi jaane na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi jaane na"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi jaane na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi jaane na"/>
</div>
<div class="lyrico-lyrics-wrapper">Yahi ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahi ishq hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho.. Ho..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho.. Ho.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tujhpe hi ja rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhpe hi ja rahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh nazar jaane kyun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh nazar jaane kyun"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho raha ajeeb sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho raha ajeeb sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh asar jaane kyun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh asar jaane kyun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tujhpe hi ja rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhpe hi ja rahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh nazar jaane kyun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh nazar jaane kyun"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho raha ajeeb sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho raha ajeeb sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh asar jaane kyun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh asar jaane kyun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raahein yeh kehti hai sabhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raahein yeh kehti hai sabhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Baatein yeh kehni thi kabhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baatein yeh kehni thi kabhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tere kareeb hun naseeb yun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tere kareeb hun naseeb yun"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadke hai abhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhadke hai abhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yahi ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahi ishq hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yahi ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahi ishq hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi jaane na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi jaane na"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi jaane na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi jaane na"/>
</div>
<div class="lyrico-lyrics-wrapper">Yahi ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahi ishq hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yahi ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahi ishq hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yahi ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahi ishq hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi jaane na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi jaane na"/>
</div>
<div class="lyrico-lyrics-wrapper">Koi jaane na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koi jaane na"/>
</div>
<div class="lyrico-lyrics-wrapper">Yahi ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahi ishq hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yahi ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahi ishq hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yahi ishq hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahi ishq hai"/>
</div>
</pre>
