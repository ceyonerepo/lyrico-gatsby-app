---
title: "nee naan song lyrics"
album: "Mankatha"
artist: "Yuvan Shankar Raja"
lyricist: "Niranjan Bharathi"
director: "Venkat Prabhu"
path: "/albums/mankatha-lyrics"
song: "Nee Naan"
image: ../../images/albumart/mankatha.jpg
date: 2011-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SMw2vxvd6DM"
type: "love"
singers:
  - S.P.B. Charan
  - Bhavatharini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannadi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanjaadai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanjaadai Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Veedu Nee Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veedu Nee Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jannal Naan En Thedal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jannal Naan En Thedal Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thevai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thevai Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Paadal Nee Un Varthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadal Nee Un Varthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Paathi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paathi Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Paathi Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paathi Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Jeevan Nee Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Jeevan Nee Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thegam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegam Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kangal Nee Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangal Nee Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Ullam Nee Un Ennam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ullam Nee Un Ennam Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannodu Vaa Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannodu Vaa Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moga Thalam Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moga Thalam Podu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Hey Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Hey Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Indru Vaanodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Indru Vaanodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megangal Theendamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megangal Theendamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottu Sella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Sella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Meni Nee Un Aadai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Meni Nee Un Aadai Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pechu Nee Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pechu Nee Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Medai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medai Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Paathai Nee Un Paatham Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paathai Nee Un Paatham Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Thendral Nee Un Vasam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thendral Nee Un Vasam Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Nee Indru Unarnthu Konde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Nee Indru Unarnthu Konde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Ennodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Ennodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thodarnthu Nan Kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodarnthu Nan Kanden"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etho Ethetho Nadanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etho Ethetho Nadanthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Nindren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanam Mele Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Mele Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paranthu Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthu Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sendren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kangal Oyaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kangal Oyaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjai Theeyil Thalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjai Theeyil Thalla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanjaadai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanjaadai Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Veedu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Veedu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Jannal Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Jannal Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Thedal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thedal Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Thevai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thevai Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Paadaal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paadaal Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vaarthai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vaarthai Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhuram Elame Udainthu Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhuram Elame Udainthu Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baaram Ellame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram Ellame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valarnthu Noyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarnthu Noyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeram Kondadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeram Kondadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalaignaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaignaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eeram Manmele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeram Manmele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhunthu Theeyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthu Theeyaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeratha Por Ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeratha Por Ondru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thanthu Ennai Vella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thanthu Ennai Vella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Meni Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Meni Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Aadai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aadai Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pechu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pechu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Medai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Medai Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Paathai Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Paathai Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Paatham Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Paatham Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Thendral Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thendral Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Vasam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Vasam Naan"/>
</div>
</pre>
