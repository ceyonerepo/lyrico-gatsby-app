---
title: "izhutha izhupukkusingari song lyrics"
album: "Pakka"
artist: "C. Sathya"
lyricist: "Kabilan"
director: "S.S. Surya"
path: "/albums/pakka-song-lyrics"
song: "Izhutha Izhupukku"
image: ../../images/albumart/pakka.jpg
date: 2018-04-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UmTVN7iGP2M"
type: "love"
singers:
  - Shivai Vyas
  - Priya Himesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Izhutha izhupukku vaadi pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhutha izhupukku vaadi pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithukku mela naan enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithukku mela naan enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhuthukku keela onnum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuthukku keela onnum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaliya kattu da thattaampulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaliya kattu da thattaampulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandaikozhi pasichirikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaikozhi pasichirikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Saapidathaan varavaa ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapidathaan varavaa ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondakaari idupukku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondakaari idupukku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotthu saavi nee thaan illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotthu saavi nee thaan illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izhutha izhupukku vaadi pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhutha izhupukku vaadi pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithukku mela naan enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithukku mela naan enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhuthukku keela onnum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuthukku keela onnum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaliya kattu da thattaampulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaliya kattu da thattaampulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathaadi azhagiya theevae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaadi azhagiya theevae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aththa petha athisaya poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aththa petha athisaya poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marudhaani saandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marudhaani saandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai arachi poosadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai arachi poosadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kili pola ulara venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kili pola ulara venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vanthu alaya venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vanthu alaya venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un meesa mudiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un meesa mudiyaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja oraisi koosathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja oraisi koosathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannukulla kaadhalathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannukulla kaadhalathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandu pidichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandu pidichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nattu vacha parkalaamnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nattu vacha parkalaamnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nandu pidichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nandu pidichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesayaala koondhalathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesayaala koondhalathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Cindu mudichen mudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cindu mudichen mudichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vathikuchi kaiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vathikuchi kaiya "/>
</div>
<div class="lyrico-lyrics-wrapper">thottu Pathikittenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thottu Pathikittenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttha vacha ponnu onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttha vacha ponnu onna"/>
</div>
<div class="lyrico-lyrics-wrapper">Suthikittenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suthikittenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uthangara othadama othikittenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uthangara othadama othikittenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Othikittenae othikittenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othikittenae othikittenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Othikittenae naanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othikittenae naanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izhutha izhupukku vaadi pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhutha izhupukku vaadi pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithukku mela naan enna solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithukku mela naan enna solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhuthukku keela onnum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhuthukku keela onnum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaaliya kattu da thattaampulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaliya kattu da thattaampulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandaikozhi pasichirikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaikozhi pasichirikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Saapidathaan varavaa ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saapidathaan varavaa ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kondakaari idupukku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kondakaari idupukku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotthu saavi nee thaan illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotthu saavi nee thaan illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkum enakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum enakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakku vazhakku varumbothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakku vazhakku varumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumbothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unavu urakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unavu urakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimai yethuvum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai yethuvum "/>
</div>
<div class="lyrico-lyrics-wrapper">virumbaathu Virumbaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virumbaathu Virumbaathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">heyyyy eyyyyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="heyyyy eyyyyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkum enakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkum enakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakku vazhakku varumbothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakku vazhakku varumbothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumbothu Unavu urakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumbothu Unavu urakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimai yethuvum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai yethuvum "/>
</div>
<div class="lyrico-lyrics-wrapper">virumbaathu Virumbaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virumbaathu Virumbaathu"/>
</div>
</pre>
