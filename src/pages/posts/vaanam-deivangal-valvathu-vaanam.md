---
title: "vaanam deivangal valvathu song lyrics"
album: "Vaanam"
artist: "Yuvan Shankar Raja"
lyricist: "Na. Muthukumar"
director: "Krish"
path: "/albums/vaanam-lyrics"
song: "Vaanam Deivangal Valvathu"
image: ../../images/albumart/vaanam.jpg
date: 2011-04-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jBYiQuyX0ZY"
type: "happy"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaanam mmmmuhuuuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam mmmmuhuuuu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam vaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheivam vaazhvadhu engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheivam vaazhvadhu engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheivam vaazhvadhu engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheivam vaazhvadhu engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavarugalai unarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavarugalai unarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhan nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhan nenjil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalinaal moodivitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalinaal moodivitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal indru thirakkiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal indru thirakkiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirandhavudan vazhiyidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirandhavudan vazhiyidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam kanneer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam kanneer"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum vanangum dheivam engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum vanangum dheivam engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum vanangum dheivam engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum vanangum dheivam engae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheivam vaazhvadhu engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheivam vaazhvadhu engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheivam vaazhvadhu engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheivam vaazhvadhu engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavarugalai unarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavarugalai unarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhan nenjil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhan nenjil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalinaal moodivitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalinaal moodivitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal indru thirakkiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal indru thirakkiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirandhavudan vazhiyidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirandhavudan vazhiyidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam kanneer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam kanneer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aduthavan kannil inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduthavan kannil inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanbadhum kaadhalthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanbadhum kaadhalthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini ivan nenjil illai baaram oo….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini ivan nenjil illai baaram oo…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanakkaaga vaazhvadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakkaaga vaazhvadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi eeram maatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi eeram maatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandha pokkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandha pokkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan paavam gangaiyil theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan paavam gangaiyil theera"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru naalum vanangum nam dheivam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru naalum vanangum nam dheivam"/>
</div>
<div class="lyrico-lyrics-wrapper">Engae irukkiradhu oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae irukkiradhu oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalum vanangum dheivam engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum vanangum dheivam engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalum vanangum dheivam engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalum vanangum dheivam engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam mmm vaanam mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam mmm vaanam mmm"/>
</div>
</pre>
