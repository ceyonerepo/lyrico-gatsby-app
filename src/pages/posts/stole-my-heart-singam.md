---
title: "stole my heart song lyrics"
album: "Singam"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari Gopalakrishnan"
path: "/albums/singam-lyrics"
song: "Stole My Heart" 
image: ../../images/albumart/singam.jpg
date: 2010-05-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oHZ_VpLV48k"
type: "Love"
singers:
  - Shaan
  - Megha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Oru vaarthai mozhiyaalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Oru vaarthai mozhiyaalae "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai uruga vaithaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai uruga vaithaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai uruga vaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai uruga vaithaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru paarvai vazhiyaalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru paarvai vazhiyaalae "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nerungi vittaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nerungi vittaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nerungi vittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nerungi vittaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru minnal idipolae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru minnal idipolae "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thudikkavittaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thudikkavittaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thudikkavittaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thudikkavittaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Oru kaadhal varathaalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kaadhal varathaalae "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai yethetho seidhuvittaal…aaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai yethetho seidhuvittaal…aaah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">She stole my heart 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She stole my heart "/>
</div>
<div class="lyrico-lyrics-wrapper">She stole my heart 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She stole my heart "/>
</div>
<div class="lyrico-lyrics-wrapper">She stole my heart 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She stole my heart "/>
</div>
<div class="lyrico-lyrics-wrapper">She stole my little little heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She stole my little little heart"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellai vellaiyaai iravugal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellai vellaiyaai iravugal "/>
</div>
<div class="lyrico-lyrics-wrapper">Kollai kollaiyaai kanavugal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollai kollaiyaai kanavugal "/>
</div>
<div class="lyrico-lyrics-wrapper">Konja konjamaai karaigiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja konjamaai karaigiren "/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae un kaadhalaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae un kaadhalaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna chinnathaai aasaigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna chinnathaai aasaigal "/>
</div>
<div class="lyrico-lyrics-wrapper">Putham puthithaai kavithaigal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putham puthithaai kavithaigal "/>
</div>
<div class="lyrico-lyrics-wrapper">Latcham latchamaai thondruthae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Latcham latchamaai thondruthae "/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae un seigaiyaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae un seigaiyaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru saaral mazhaiyaalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru saaral mazhaiyaalae "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nanaiya vaithaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nanaiya vaithaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nanaiya vaithaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nanaiya vaithaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Puyalaaga urumaari 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalaaga urumaari "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai verodu saaithuvittaan…aan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai verodu saaithuvittaan…aan.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">He stole my heart 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He stole my heart "/>
</div>
<div class="lyrico-lyrics-wrapper">He stole my heart 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He stole my heart "/>
</div>
<div class="lyrico-lyrics-wrapper">He stole my little little heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He stole my little little heart"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh nenjin araigal thirakkiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh nenjin araigal thirakkiren "/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai adhilae niraikkiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai adhilae niraikkiren "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai muzhuthaai marakkiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai muzhuthaai marakkiren "/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae un kaadhalaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae un kaadhalaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai enniyae vasikkiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai enniyae vasikkiren "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai athanaal rasikkiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai athanaal rasikkiren "/>
</div>
<div class="lyrico-lyrics-wrapper">Thannan thaniyae mithakkiren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannan thaniyae mithakkiren "/>
</div>
<div class="lyrico-lyrics-wrapper">Anbae un seigaiyaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbae un seigaiyaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh thalai kaal thaan puriyaamal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh thalai kaal thaan puriyaamal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thavikkavaithaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thavikkavaithaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thavikkavaithaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thavikkavaithaal "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalai ganamaai nadanthae thaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalai ganamaai nadanthae thaan "/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai yethetho seithuvittaal…aaal…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai yethetho seithuvittaal…aaal…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">She stole my heart 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She stole my heart "/>
</div>
<div class="lyrico-lyrics-wrapper">She stole my heart 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She stole my heart "/>
</div>
<div class="lyrico-lyrics-wrapper">She stole my little little heart
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="She stole my little little heart"/>
</div>
</pre>
