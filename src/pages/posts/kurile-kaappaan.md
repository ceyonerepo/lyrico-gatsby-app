---
title: 'kurila kurile song lyrics'
album: 'Kaappaan'
artist: 'Harris Jayaraj'
lyricist: 'Kabilan'
director: 'K V Anand'
path: '/albums/kaappaan-song-lyrics'
song: 'Kurile Kurile'
image: ../../images/albumart/kaappaan.jpg
date: 2019-09-20
lang: tamil
singers: 
- Javed Ali
- Darshana KT
youtubeLink: "https://www.youtube.com/embed/wuin8ZrwqzY"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Pona maasam paartha nila
<input type="checkbox" class="lyrico-select-lyric-line" value="Pona maasam paartha nila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu ulaa vandhaalae oyyaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennodu ulaa vandhaalae oyyaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kurilae kurilae kuyilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kurilae kurilae kuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedilae nedilae kulale yele
<input type="checkbox" class="lyrico-select-lyric-line" value="Nedilae nedilae kulale yele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vedalae vedalae udalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vedalae vedalae udalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodala thodala thodalae melae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thodala thodala thodalae melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un meesa mudiyaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Un meesa mudiyaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meththa thachaane
<input type="checkbox" class="lyrico-select-lyric-line" value="Meththa thachaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam vechaanae machaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Muththam vechaanae machaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pona maasam paartha nila
<input type="checkbox" class="lyrico-select-lyric-line" value="Pona maasam paartha nila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu ulaa vandhaalae oyyaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennodu ulaa vandhaalae oyyaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kurilae kurilae kuyilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kurilae kurilae kuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedilae nedilae kulale yele
<input type="checkbox" class="lyrico-select-lyric-line" value="Nedilae nedilae kulale yele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vedalae vedalae udalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vedalae vedalae udalae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naan thodala thodala
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan thodala thodala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodalae melae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thodalae melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un kuchi iduppu kulungarappo
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kuchi iduppu kulungarappo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachu nachu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nachu nachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa sola solaya rusuchidava
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa sola solaya rusuchidava"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichu pichu (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Pichu pichu"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naan athirasamtha
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan athirasamtha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vecha thanga kinnamaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vecha thanga kinnamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Para para ee erumbaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Para para ee erumbaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna moikuriyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna moikuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naan mora morappaa ulla
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan mora morappaa ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanja kaathaadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Maanja kaathaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Karu karu koonthalilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Karu karu koonthalilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maati sikkikichae
<input type="checkbox" class="lyrico-select-lyric-line" value="Maati sikkikichae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee ondikatta naa ottikitten
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee ondikatta naa ottikitten"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illanaakaa naa sethuputen
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee illanaakaa naa sethuputen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kurilae kurilae kuyilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kurilae kurilae kuyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nedilae nedilae kulale yele
<input type="checkbox" class="lyrico-select-lyric-line" value="Nedilae nedilae kulale yele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vedalae vedalae udalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vedalae vedalae udalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodala thodala thodalae melae
<input type="checkbox" class="lyrico-select-lyric-line" value="Thodala thodala thodalae melae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Rumm bumm bumm bumm bola
<input type="checkbox" class="lyrico-select-lyric-line" value="Rumm bumm bumm bumm bola"/>
</div>
<div class="lyrico-lyrics-wrapper">Rumm gumm gumm gumm gola
<input type="checkbox" class="lyrico-select-lyric-line" value="Rumm gumm gumm gumm gola"/>
</div>
<div class="lyrico-lyrics-wrapper">Rumm bumm bumm bumm bola
<input type="checkbox" class="lyrico-select-lyric-line" value="Rumm bumm bumm bumm bola"/>
</div>
<div class="lyrico-lyrics-wrapper">See oo maaro oo maaro oo maaro
<input type="checkbox" class="lyrico-select-lyric-line" value="See oo maaro oo maaro oo maaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaro maaro maaro maaro
<input type="checkbox" class="lyrico-select-lyric-line" value="Maaro maaro maaro maaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Rumm bumm bumm bumm bola
<input type="checkbox" class="lyrico-select-lyric-line" value="Rumm bumm bumm bumm bola"/>
</div>
<div class="lyrico-lyrics-wrapper">Rumm gumm gumm gumm gola
<input type="checkbox" class="lyrico-select-lyric-line" value="Rumm gumm gumm gumm gola"/>
</div>
<div class="lyrico-lyrics-wrapper">Rumm bumm bumm bumm bola
<input type="checkbox" class="lyrico-select-lyric-line" value="Rumm bumm bumm bumm bola"/>
</div>
<div class="lyrico-lyrics-wrapper">See oo maaro oo maaro oo maaro
<input type="checkbox" class="lyrico-select-lyric-line" value="See oo maaro oo maaro oo maaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaro maaro maaro maaro oh yeah
<input type="checkbox" class="lyrico-select-lyric-line" value="Maaro maaro maaro maaro oh yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un tholu patta thaan
<input type="checkbox" class="lyrico-select-lyric-line" value="Un tholu patta thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Osanthirukkum thotta petta
<input type="checkbox" class="lyrico-select-lyric-line" value="Osanthirukkum thotta petta"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kadha kadhappaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan kadha kadhappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulurikku yetha ola kotta
<input type="checkbox" class="lyrico-select-lyric-line" value="Kulurikku yetha ola kotta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee oru thinusaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee oru thinusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesum pechu ellaamae
<input type="checkbox" class="lyrico-select-lyric-line" value="Pesum pechu ellaamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadugudu aaduthadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Sadugudu aaduthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil oodukatti
<input type="checkbox" class="lyrico-select-lyric-line" value="Nenjil oodukatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aah naan veda vedappa
<input type="checkbox" class="lyrico-select-lyric-line" value="Aah naan veda vedappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan pakkam vandhenae
<input type="checkbox" class="lyrico-select-lyric-line" value="Undhan pakkam vandhenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thuli vervaiyilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru thuli vervaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna paththa vechaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Enna paththa vechaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un pakkam vandhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Un pakkam vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ooma padam
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan ooma padam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un paarvai mattum
<input type="checkbox" class="lyrico-select-lyric-line" value="Un paarvai mattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu pesum paadam
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhu pesum paadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kurilae kurilae kuyilae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kurilae kurilae kuyilae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Nedilae nedilae kulale yele
<input type="checkbox" class="lyrico-select-lyric-line" value="Nedilae nedilae kulale yele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vedalae vedalae udalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vedalae vedalae udalae"/>
</div>
  <div class="lyrico-lyrics-wrapper">Aah thodala thodala thodalae hoo
<input type="checkbox" class="lyrico-select-lyric-line" value="Aah thodala thodala thodalae hoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pona maasam paartha nila
<input type="checkbox" class="lyrico-select-lyric-line" value="Pona maasam paartha nila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennodu ulaa vandhaalae oyyaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennodu ulaa vandhaalae oyyaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Meesa mudiyaalae
<input type="checkbox" class="lyrico-select-lyric-line" value="Meesa mudiyaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Meththa thachaane
<input type="checkbox" class="lyrico-select-lyric-line" value="Meththa thachaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam vechaanae machaanae
<input type="checkbox" class="lyrico-select-lyric-line" value="Muththam vechaanae machaanae"/>
</div>
</pre>