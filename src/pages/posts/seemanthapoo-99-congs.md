---
title: "seemanthapoo song lyrics"
album: "99 Songs"
artist: "A.R. Rahman"
lyricist: "Kutty Revathi"
director: "Vishwesh Krishnamoorthy"
path: "/albums/99-songs-lyrics"
song: "Seemanthapoo"
image: ../../images/albumart/99-songs.jpg
date: 2021-04-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Xg0_ZcKuBWI"
type: "Celebration"
singers:
  - A.R. Rahman
  - Jonita Gandhi
  - Sharanya Srinivas
  - Sireesha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">naadhirdhirthom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadhirdhirthom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">dhirthom thom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhirthom thom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">naadhirdhirthom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadhirdhirthom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">paar... ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paar... ival"/>
</div>
<div class="lyrico-lyrics-wrapper">naadhirdhirthom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadhirdhirthom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">dhirthom thom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhirthom thom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">naadhirdhirthom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadhirdhirthom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">poothaale...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poothaale..."/>
</div>
<div class="lyrico-lyrics-wrapper">pa ni sa ma ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa ni sa ma ga"/>
</div>
<div class="lyrico-lyrics-wrapper">sa ma pa ni dha pa ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa ma pa ni dha pa ma"/>
</div>
<div class="lyrico-lyrics-wrapper">pa ma ni sa ga ma pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa ma ni sa ga ma pa"/>
</div>
<div class="lyrico-lyrics-wrapper">ga ma pa sa ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ga ma pa sa ni"/>
</div>
<div class="lyrico-lyrics-wrapper">aa... o...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa... o..."/>
</div>
<div class="lyrico-lyrics-wrapper">aa... aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa... aah"/>
</div>
<div class="lyrico-lyrics-wrapper">poothaale...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poothaale..."/>
</div>
<div class="lyrico-lyrics-wrapper">poove mellum poothale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poove mellum poothale"/>
</div>
<div class="lyrico-lyrics-wrapper">thayai maara poraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayai maara poraale"/>
</div>
<div class="lyrico-lyrics-wrapper">poovin thegam kondale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovin thegam kondale"/>
</div>
<div class="lyrico-lyrics-wrapper">thaimai noombai etharle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaimai noombai etharle"/>
</div>
<div class="lyrico-lyrics-wrapper">punnagayin seemanthapoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnagayin seemanthapoo"/>
</div>
<div class="lyrico-lyrics-wrapper">ivale...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivale..."/>
</div>
<div class="lyrico-lyrics-wrapper">punnagaiyin seemantha...poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnagaiyin seemantha...poo"/>
</div>
<div class="lyrico-lyrics-wrapper">ivale....
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivale...."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirin sumaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin sumaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">velli maruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velli maruthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">yeno yeno oru udal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno yeno oru udal"/>
</div>
<div class="lyrico-lyrics-wrapper">aanathe maayam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanathe maayam enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirin paadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin paadal"/>
</div>
<div class="lyrico-lyrics-wrapper">aararo aariraroo...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aararo aariraroo..."/>
</div>
<div class="lyrico-lyrics-wrapper">vaalaiyal podum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalaiyal podum "/>
</div>
<div class="lyrico-lyrics-wrapper">thaalame thaalelo....  
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalame thaalelo....  "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sesuvin asaivai kanavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sesuvin asaivai kanavil"/>
</div>
<div class="lyrico-lyrics-wrapper">rasipai thiyagum vimbathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rasipai thiyagum vimbathil"/>
</div>
<div class="lyrico-lyrics-wrapper">thavipai... 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavipai... "/>
</div>
<div class="lyrico-lyrics-wrapper">uyirin sumaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin sumaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">velli maruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velli maruthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">yeno yeno oru udal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno yeno oru udal"/>
</div>
<div class="lyrico-lyrics-wrapper">aanathe maayam enaa...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanathe maayam enaa..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poove mellum poothale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poove mellum poothale"/>
</div>
<div class="lyrico-lyrics-wrapper">thayai maara poraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayai maara poraale"/>
</div>
<div class="lyrico-lyrics-wrapper">poovin thegam kondale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovin thegam kondale"/>
</div>
<div class="lyrico-lyrics-wrapper">thaimai noombai etharle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaimai noombai etharle"/>
</div>
<div class="lyrico-lyrics-wrapper">punnagayin seemanthapoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnagayin seemanthapoo"/>
</div>
<div class="lyrico-lyrics-wrapper">ivale...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivale..."/>
</div>
<div class="lyrico-lyrics-wrapper">punnagaiyin seemantha...poo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="punnagaiyin seemantha...poo"/>
</div>
<div class="lyrico-lyrics-wrapper">ivale....
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivale...."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirin sumaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirin sumaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">velli maruthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velli maruthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">yeno yeno oru udal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeno yeno oru udal"/>
</div>
<div class="lyrico-lyrics-wrapper">aanathe maayam enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanathe maayam enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyirai vaithum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirai vaithum"/>
</div>
<div class="lyrico-lyrics-wrapper">vallai kappum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vallai kappum"/>
</div>
<div class="lyrico-lyrics-wrapper">vallai kaappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vallai kaappu"/>
</div>
<div class="lyrico-lyrics-wrapper">pulipai thedi naavile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulipai thedi naavile"/>
</div>
<div class="lyrico-lyrics-wrapper">ekame... 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekame... "/>
</div>
<div class="lyrico-lyrics-wrapper">inimai suvaikka manathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inimai suvaikka manathai"/>
</div>
<div class="lyrico-lyrics-wrapper">ellapai thaalatum kanavugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellapai thaalatum kanavugal"/>
</div>
<div class="lyrico-lyrics-wrapper">vallarpal...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vallarpal..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poove mellum poothale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poove mellum poothale"/>
</div>
<div class="lyrico-lyrics-wrapper">thayai maara poraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thayai maara poraale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naadhirdhirthom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadhirdhirthom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">dhirthom thom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhirthom thom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">naadhirdhirthom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadhirdhirthom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">paar... ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paar... ival"/>
</div>
<div class="lyrico-lyrics-wrapper">naadhirdhirthom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadhirdhirthom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">dhirthom thom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhirthom thom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">naadhirdhirthom thana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadhirdhirthom thana"/>
</div>
<div class="lyrico-lyrics-wrapper">poothaale...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poothaale..."/>
</div>
<div class="lyrico-lyrics-wrapper">pa ni sa ma ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa ni sa ma ga"/>
</div>
<div class="lyrico-lyrics-wrapper">sa ma pa ni dha pa ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sa ma pa ni dha pa ma"/>
</div>
<div class="lyrico-lyrics-wrapper">pa ma ni sa ga ma pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pa ma ni sa ga ma pa"/>
</div>
<div class="lyrico-lyrics-wrapper">ga ma pa sa ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ga ma pa sa ni"/>
</div>
<div class="lyrico-lyrics-wrapper">aa... o...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa... o..."/>
</div>
<div class="lyrico-lyrics-wrapper">aa... aah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa... aah"/>
</div>
<div class="lyrico-lyrics-wrapper">poothaale...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poothaale..."/>
</div>
</pre>
