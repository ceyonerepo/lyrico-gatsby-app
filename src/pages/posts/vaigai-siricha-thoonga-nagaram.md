---
title: "vaigai siricha song lyrics"
album: "Thoonga Nagaram"
artist: "Sundar C Babu"
lyricist: "S Gnanakaravel"
director: "Gaurav Narayanan"
path: "/albums/thoonga-nagaram-lyrics"
song: "Vaigai Siricha"
image: ../../images/albumart/thoonga-nagaram.jpg
date: 2011-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WjiDzhlllJs"
type: "happy"
singers:
  - Palghat Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaigai sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaigai sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongaanagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongaanagaram"/>
</div>
<div class="lyrico-lyrics-wrapper">vaandum moraichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaandum moraichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongaa nagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongaa nagaram"/>
</div>
<div class="lyrico-lyrics-wrapper">edhukkum thuninjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhukkum thuninjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongaa nagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongaa nagaram"/>
</div>
<div class="lyrico-lyrics-wrapper">dhukkam tholainjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhukkam tholainjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thoonga nagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoonga nagaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sangathamizha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sangathamizha"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongaa nagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongaa nagaram"/>
</div>
<div class="lyrico-lyrics-wrapper">landhu thamizhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="landhu thamizhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongaa nagaram.
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongaa nagaram."/>
</div>
<div class="lyrico-lyrics-wrapper">edhiri merandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhiri merandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongaa nagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongaa nagaram"/>
</div>
<div class="lyrico-lyrics-wrapper">namma madhura thaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma madhura thaandaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongaa nagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongaa nagaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maan koottam alaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maan koottam alaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Periyaar Bussu nilaiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periyaar Bussu nilaiyam"/>
</div>
<div class="lyrico-lyrics-wrapper">pasangaloada idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasangaloada idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">paayum pulipola maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paayum pulipola maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thettarukku poanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thettarukku poanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">A/C keesi venaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A/C keesi venaam"/>
</div>
<div class="lyrico-lyrics-wrapper">visiladikkum kaaththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="visiladikkum kaaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">vandhu visirividum Fan-aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhu visirividum Fan-aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seemakkalla kadandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seemakkalla kadandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pazhakkada valarndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazhakkada valarndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">neththikkulla natpupoala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neththikkulla natpupoala"/>
</div>
<div class="lyrico-lyrics-wrapper">neraiyumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neraiyumadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">madhuraiya muzhusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="madhuraiya muzhusaa"/>
</div>
<div class="lyrico-lyrics-wrapper">melirundhu rasichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="melirundhu rasichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">koaburangal naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koaburangal naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">serndhu thaamaraiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serndhu thaamaraiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyumadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyumadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaigai sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaigai sirichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emanoadu moadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emanoadu moadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">adangaadha veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adangaadha veeram"/>
</div>
<div class="lyrico-lyrics-wrapper">natpukku munney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natpukku munney"/>
</div>
<div class="lyrico-lyrics-wrapper">adhu pooppoala maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhu pooppoala maarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naduchaamam kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naduchaamam kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">koraiyaadhu rousu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koraiyaadhu rousu"/>
</div>
<div class="lyrico-lyrics-wrapper">andhaikke muzhikka nanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andhaikke muzhikka nanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Class eduppoam bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Class eduppoam bossu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thalakula thamukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalakula thamukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">siththiraiyil sirakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siththiraiyil sirakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">thalluvandi poattaakkooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalluvandi poattaakkooda"/>
</div>
<div class="lyrico-lyrics-wrapper">thangandhaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangandhaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thattaneri kanakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thattaneri kanakku"/>
</div>
<div class="lyrico-lyrics-wrapper">kadaisiyil irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadaisiyil irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">adhuvarai aadikkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhuvarai aadikkadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">appuramaa sanguthaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="appuramaa sanguthaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaigai sirichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaigai sirichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongaanagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongaanagaram"/>
</div>
<div class="lyrico-lyrics-wrapper">vaandum moraichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaandum moraichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongaa nagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongaa nagaram"/>
</div>
<div class="lyrico-lyrics-wrapper">edhukkum thuninjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhukkum thuninjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thoongaa nagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoongaa nagaram"/>
</div>
<div class="lyrico-lyrics-wrapper">dhukkam tholainjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhukkam tholainjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">thoonga nagaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoonga nagaram"/>
</div>
</pre>
