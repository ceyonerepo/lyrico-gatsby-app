---
title: "pennoruthi song lyrics"
album: "Gemini"
artist: "Bharathwaj"
lyricist: "Vairamuthu"
director: "Saran"
path: "/albums/gemini-lyrics"
song: "Pennoruthi"
image: ../../images/albumart/gemini.jpg
date: 2002-04-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/022B-diSSqY"
type: "love"
singers:
  - S. P. Balasubrahmanyam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pen Oruthi Pen Oruthi Padaithu Vittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Oruthi Pen Oruthi Padaithu Vittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidathil Ennidathil Anupi Vittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidathil Ennidathil Anupi Vittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu Ennai Uzhayil Yetrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu Ennai Uzhayil Yetrinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerupuku Selaikatti Anupi Vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupuku Selaikatti Anupi Vaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavuku Vanmuraigal Kattru Koduthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavuku Vanmuraigal Kattru Koduthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kannil En Oosi Yetrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannil En Oosi Yetrinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Brahmma Oh Brahmma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmma Oh Brahmma"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Thaguma Idhu Thaguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaguma Idhu Thaguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo Idhu Varamaa Saabamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Idhu Varamaa Saabamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Brahmma Oh Brahmma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmma Oh Brahmma"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Thaguma Idhu Thaguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaguma Idhu Thaguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo Idhu Varamaa Saabamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Idhu Varamaa Saabamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen Oruthi Pen Oruthi Padaithu Vittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Oruthi Pen Oruthi Padaithu Vittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidathil Ennidathil Anupi Vittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidathil Ennidathil Anupi Vittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu Ennai Uzhayil Yetrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu Ennai Uzhayil Yetrinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalile Boutham Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalile Boutham Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathil Samanam Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathil Samanam Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarvai Mattum Kolaigal Seiya Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Mattum Kolaigal Seiya Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parkalilum Karunai Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parkalilum Karunai Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhangalil Dheivam Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhangalil Dheivam Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Punnagaiyo Uyirai Thinna Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagaiyo Uyirai Thinna Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal Endru Ninaithen Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Endru Ninaithen Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Puyal Kattum Kayiraai Vandhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Kattum Kayiraai Vandhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai Endru Ninaithen Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai Endru Ninaithen Ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Malligaiyaal Malaiyai Saaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malligaiyaal Malaiyai Saaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Netri Pottil Ennai Urutti Vaithaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netri Pottil Ennai Urutti Vaithaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Brahmma Oh Brahmma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmma Oh Brahmma"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Thaguma Idhu Thaguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaguma Idhu Thaguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo Idhu Varamaa Saabamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Idhu Varamaa Saabamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen Oruthi Pen Oruthi Padaithu Vittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Oruthi Pen Oruthi Padaithu Vittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidathil Ennidathil Anupi Vittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidathil Ennidathil Anupi Vittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu Ennai Uzhayil Yetrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu Ennai Uzhayil Yetrinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagal Ellam Karupaai Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal Ellam Karupaai Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravellam Vellai Aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravellam Vellai Aaga"/>
</div>
<div class="lyrico-lyrics-wrapper">En Vaazhvil Yedhedho Maatramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Vaazhvil Yedhedho Maatramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayyo Uzhaga Urundai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyo Uzhaga Urundai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Vayitril Sutruvadhenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Vayitril Sutruvadhenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Achacho Thondai Varaiyil Yerumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achacho Thondai Varaiyil Yerumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erimalaiyin Kondai Mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erimalaiyin Kondai Mele"/>
</div>
<div class="lyrico-lyrics-wrapper">Rojaavai Nattaval Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rojaavai Nattaval Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Ennum Kanavaai Vazhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Ennum Kanavaai Vazhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">En Dhesam Pugunthaval Yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Dhesam Pugunthaval Yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Siruga Siruga Uyirai Parugi Sendrale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruga Siruga Uyirai Parugi Sendrale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Brahmma Oh Brahmma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmma Oh Brahmma"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Thaguma Idhu Thaguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaguma Idhu Thaguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo Idhu Varamaa Saabamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Idhu Varamaa Saabamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pen Oruthi Pen Oruthi Padaithu Vittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pen Oruthi Pen Oruthi Padaithu Vittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennidathil Ennidathil Anupi Vittai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennidathil Ennidathil Anupi Vittai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu Ennai Uzhayil Yetrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu Ennai Uzhayil Yetrinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nerupuku Selaikatti Anupi Vaithaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerupuku Selaikatti Anupi Vaithaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavuku Vanmuraigal Kattru Koduthaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavuku Vanmuraigal Kattru Koduthaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kannil Yen Oosi Yetrinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannil Yen Oosi Yetrinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Brahmma Oh Brahmma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmma Oh Brahmma"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Thaguma Idhu Thaguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaguma Idhu Thaguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo Idhu Varamaa Saabamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Idhu Varamaa Saabamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Brahmma Oh Brahmma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brahmma Oh Brahmma"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Thaguma Idhu Thaguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Thaguma Idhu Thaguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo Idhu Varamaa Saabamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo Idhu Varamaa Saabamaa"/>
</div>
</pre>
