---
title: "hoyna hoyna song lyrics"
album: "Nani's Gang Leader"
artist: "Anirudh Ravichander"
lyricist: "Anantha Sriram - Inno Genga"
director: "Vikram Kumar"
path: "/albums/nani's-gang-leader-lyrics"
song: "Hoyna Hoyna"
image: ../../images/albumart/nanis-gang-leader.jpg
date: 2019-09-13
lang: telugu
youtubeLink: "https://www.youtube.com/embed/91EzD9VgwGk"
type: "happy"
singers:
  - Inno Genga
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veyrey Kottha Bhoomipai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyrey Kottha Bhoomipai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnanaa!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnanaa!"/>
</div>
<div class="lyrico-lyrics-wrapper">Eydho Vintha Raagamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eydho Vintha Raagamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnaanaa!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnaanaa!"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyrey Kottha Bhoomipai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyrey Kottha Bhoomipai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnanaa!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnanaa!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eydho Vintha Raagamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eydho Vintha Raagamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnaanaa!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnaanaa!"/>
</div>
<div class="lyrico-lyrics-wrapper">Palikey Paala Guvvatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palikey Paala Guvvatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuulikey Poola Kommatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuulikey Poola Kommatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Kasirey Vennelammatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kasirey Vennelammatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Sneyham Cheysaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sneyham Cheysaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Egirey Paalavellitho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egirey Paalavellitho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadiche Gaaju Bommatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiche Gaaju Bommatho"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandham Mundhu Janmadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandham Mundhu Janmadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Eydho Bahusaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eydho Bahusaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoynaa Hoynaa Hoynaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoynaa Hoynaa Hoynaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoynaa Hoynaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoynaa Hoynaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoynaa Hoynaa Hoynaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoynaa Hoynaa Hoynaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ika Yedheymainaa Meetho Chindhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Yedheymainaa Meetho Chindhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyyanaa Veyyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyyanaa Veyyanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoynaa Hoynaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoynaa Hoynaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoynaa Hoynaa Hoynaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoynaa Hoynaa Hoynaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakaalam Meetho Kaalakshepam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakaalam Meetho Kaalakshepam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyanaa Cheyyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyanaa Cheyyanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Think I Caught The Feels This
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Think I Caught The Feels This"/>
</div>
<div class="lyrico-lyrics-wrapper">Summer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summer"/>
</div>
<div class="lyrico-lyrics-wrapper">Bae You’Re One Of A Kind
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bae You’Re One Of A Kind"/>
</div>
<div class="lyrico-lyrics-wrapper">No Other
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Other"/>
</div>
<div class="lyrico-lyrics-wrapper">Be My Sweetie Be My Sugar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Be My Sweetie Be My Sugar"/>
</div>
<div class="lyrico-lyrics-wrapper">Had Enough As A One Side Lover
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Had Enough As A One Side Lover"/>
</div>
<div class="lyrico-lyrics-wrapper">Think I Caught The Feels This
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Think I Caught The Feels This"/>
</div>
<div class="lyrico-lyrics-wrapper">Summer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summer"/>
</div>
<div class="lyrico-lyrics-wrapper">Bae You’Re One Of A Kind
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bae You’Re One Of A Kind"/>
</div>
<div class="lyrico-lyrics-wrapper">No Other
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No Other"/>
</div>
<div class="lyrico-lyrics-wrapper">Be My Sweetie Be My Sugar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Be My Sweetie Be My Sugar"/>
</div>
<div class="lyrico-lyrics-wrapper">Had Enough As A One Side Lover
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Had Enough As A One Side Lover"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaah!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaah!"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Jeevithaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Jeevithaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendo Prayaanamundhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendo Prayaanamundhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaari Veysinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaari Veysinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitti Paadhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitti Paadhamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Jaathakaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Jaathakaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendo Bhaagamundhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendo Bhaagamundhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaati Cheppina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaati Cheppina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinni Praanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinni Praanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundellona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundellona"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendo Vaippey Choopi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendo Vaippey Choopi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sambaraana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sambaraana"/>
</div>
<div class="lyrico-lyrics-wrapper">Munchavey Neysthamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munchavey Neysthamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalo Naakey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalo Naakey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendo Rropam Choopi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendo Rropam Choopi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheevinchindhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheevinchindhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Meelo Ponge Preyma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meelo Ponge Preyma"/>
</div>
<div class="lyrico-lyrics-wrapper">Veligey Veydukavaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veligey Veydukavaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisey Kaanukavvanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisey Kaanukavvanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedhavullona Nimpanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedhavullona Nimpanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirudharahaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirudharahaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Evaro Raasinattugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evaro Raasinattugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jarigey Naatakaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarigey Naatakaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Merugulu Dhinni Veyyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merugulu Dhinni Veyyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Iha Naa Veysham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iha Naa Veysham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoynaa Hoynaa Hoynaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoynaa Hoynaa Hoynaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoynaa Hoynaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoynaa Hoynaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoynaa Hoynaa Hoynaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoynaa Hoynaa Hoynaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ika Yedheymainaa Meetho Chindhulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika Yedheymainaa Meetho Chindhulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyyanaa Veyyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyyanaa Veyyanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hoynaa Hoynaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoynaa Hoynaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoynaa Hoynaa Hoynaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoynaa Hoynaa Hoynaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakaalam Meetho Kaalakshepam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakaalam Meetho Kaalakshepam"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyanaa Cheyyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyanaa Cheyyanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyrey Kottha Bhoomipai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyrey Kottha Bhoomipai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnanaa!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnanaa!"/>
</div>
<div class="lyrico-lyrics-wrapper">Eydho Vintha Raagamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eydho Vintha Raagamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnaanaa!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnaanaa!"/>
</div>
<div class="lyrico-lyrics-wrapper">Veyrey Kottha Bhoomipai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veyrey Kottha Bhoomipai"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnanaa!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnanaa!"/>
</div>
<div class="lyrico-lyrics-wrapper">Eydho Vintha Raagamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eydho Vintha Raagamey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vinnaanaa!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vinnaanaa!"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm Hmm Hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Hmm Hmm"/>
</div>
</pre>
