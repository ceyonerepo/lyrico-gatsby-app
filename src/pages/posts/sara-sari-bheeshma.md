---
title: "sara sari song lyrics"
album: "Bheeshma"
artist: "Mahati Swara Sagar"
lyricist: "Sri Mani"
director: "Venky Kudumula"
path: "/albums/bheeshma-lyrics"
song: "Sara Sari"
image: ../../images/albumart/bheeshma.jpg
date: 2020-02-21
lang: telugu
youtubeLink: "https://www.youtube.com/embed/LZQk37undYo"
type: "love"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Na kalale nee roopam lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na kalale nee roopam lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedurayye nizama maya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedurayye nizama maya"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevevo uhalu nalo modalayye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevevo uhalu nalo modalayye"/>
</div>
<div class="lyrico-lyrics-wrapper">Na manse ningini dhaati yegirenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na manse ningini dhaati yegirenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizama maya ee kshaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizama maya ee kshaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Adbuthamedho edho jarigenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adbuthamedho edho jarigenule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedo yedo cheppalanipisthonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedo yedo cheppalanipisthonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve nuvve kawalanipisthonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve nuvve kawalanipisthonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Inka edho adagalanipisthonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inka edho adagalanipisthonde"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetho roju undalanipisthonde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetho roju undalanipisthonde"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho nalone navvukuntunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho nalone navvukuntunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nathone undanantunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nathone undanantunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naake ne kothaga unnna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naake ne kothaga unnna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee valle, nee valle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee valle, nee valle"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho nee vente needanauthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho nee vente needanauthane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvunde jaadanauthane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvunde jaadanauthane"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvunte chalanipinche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvunte chalanipinche"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayedho challave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayedho challave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sara sari gundello dinchave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sari gundello dinchave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari mari maikamlo munchave ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari mari maikamlo munchave ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Aina sare ee bhadha bagundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aina sare ee bhadha bagundhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anukonidhe maniruvuri parichayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukonidhe maniruvuri parichayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oho jathapadamani manakila raasundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho jathapadamani manakila raasundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathi chedi ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathi chedi ilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venake thiragadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venake thiragadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Alavatuga nakela marindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alavatuga nakela marindhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaleni thondaredho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaleni thondaredho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu those nee vaipila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu those nee vaipila"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapleni vegamedho naalopala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapleni vegamedho naalopala"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha kaalam naaku natho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha kaalam naaku natho"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha godave raaledhila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha godave raaledhila"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu kalise roju varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu kalise roju varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yerojila lene ilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerojila lene ilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sara sari gundello dinchave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sara sari gundello dinchave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mari mari maikamlo munchave ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari mari maikamlo munchave ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Aina sare ee bhadha bagundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aina sare ee bhadha bagundhe"/>
</div>
</pre>
