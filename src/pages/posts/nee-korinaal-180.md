---
title: "nee korinaal song lyrics"
album: "180"
artist: "Sharreth"
lyricist: "Madhan Karky"
director: "Jayendra Panchapakesan"
path: "/albums/180-lyrics"
song: "Nee Korinaal"
image: ../../images/albumart/180.jpg
date: 2011-06-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/PmYylOWeIWY"
type: "love"
singers:
  - Karthik
  - Vineeth Sreenivasan
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Korinaal Vaanam Maaradha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Korinaal Vaanam Maaradha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Theeramalae Maegam Thooraadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Theeramalae Maegam Thooraadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeye Indriyae Nee Ennai Vaattinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeye Indriyae Nee Ennai Vaattinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Jannalai Adaithadaithu Pennae Oadaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Jannalai Adaithadaithu Pennae Oadaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeye Indriyae Nee Ennai Vaattinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeye Indriyae Nee Ennai Vaattinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Jannalai Adaithadaithu Pennae Oadaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Jannalai Adaithadaithu Pennae Oadaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odum Odum Asaiyaathodum Azhagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum Odum Asaiyaathodum Azhagiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Odum Odum Asaiyaa Thodum Odum Odum Odum Azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odum Odum Asaiyaa Thodum Odum Odum Odum Azhagiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Korinaal Vaanam Maaraatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Korinaal Vaanam Maaraatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Theeramale Megam Thooraatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Theeramale Megam Thooraatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandum Theendidaa Naan Bothi Saathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandum Theendidaa Naan Bothi Saathiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Meethi Paathi Bimba Poove Pattu Pogaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Meethi Paathi Bimba Poove Pattu Pogaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kandum Theendidaa Naan Bodhi Saathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandum Theendidaa Naan Bodhi Saathiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Meedhi Paadhi Bimba Poovae Pattu Pogaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Meedhi Paadhi Bimba Poovae Pattu Pogaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bothai Oorum Idhazhin Oram Parugava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bothai Oorum Idhazhin Oram Parugava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bothai Oorum Idhazhin Oram Oram Parugava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bothai Oorum Idhazhin Oram Oram Parugava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Korinaal Vaanam Maaraadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Korinaal Vaanam Maaraadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Theeramalae Maegam Thuraadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Theeramalae Maegam Thuraadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Korinaal Vaanam Maaraadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Korinaal Vaanam Maaraadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam Theeramalae Maegam Thuraadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam Theeramalae Maegam Thuraadhaa"/>
</div>
</pre>
