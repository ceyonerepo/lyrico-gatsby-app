---
title: "chey savaari song lyrics"
album: "Savaari"
artist: "Shekar Chandra"
lyricist: "Ramanjaneyulu"
director: "Saahith Mothkuri"
path: "/albums/savaari-lyrics"
song: "Chey Savaari"
image: ../../images/albumart/savaari.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/gpeawnZUrXs"
type: "happy"
singers:
  - Sid Watkins
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">udainche suryue nidrantu mande
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udainche suryue nidrantu mande"/>
</div>
<div class="lyrico-lyrics-wrapper">nadireye datutu kiranula savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadireye datutu kiranula savaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">agseti sandrame alupantu apade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="agseti sandrame alupantu apade"/>
</div>
<div class="lyrico-lyrics-wrapper">daridaka sagutu keratal savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="daridaka sagutu keratal savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">chikatle chilchukuntu sankelle tenchukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chikatle chilchukuntu sankelle tenchukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">takaloy ningi anchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takaloy ningi anchu"/>
</div>
<div class="lyrico-lyrics-wrapper">padi padi talpadi nilbadga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padi padi talpadi nilbadga"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvu savaari chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvu savaari chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">padale rekklavwale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padale rekklavwale"/>
</div>
<div class="lyrico-lyrics-wrapper">nili ningaina chetikandale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nili ningaina chetikandale"/>
</div>
<div class="lyrico-lyrics-wrapper">savaari chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savaari chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">deekkule dhikkarinchale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deekkule dhikkarinchale"/>
</div>
<div class="lyrico-lyrics-wrapper">chukkalannunna tench tevale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chukkalannunna tench tevale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bhugolanne tiragakani apgaligedi avarass
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhugolanne tiragakani apgaligedi avarass"/>
</div>
<div class="lyrico-lyrics-wrapper">nili meghanne kurvakani ajna vesina agadule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nili meghanne kurvakani ajna vesina agadule"/>
</div>
<div class="lyrico-lyrics-wrapper">paluru cheppe maata vinukoku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paluru cheppe maata vinukoku"/>
</div>
<div class="lyrico-lyrics-wrapper">naluguru valele darikellipoku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naluguru valele darikellipoku"/>
</div>
<div class="lyrico-lyrics-wrapper">averiki nuvvu bantuvanukoku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="averiki nuvvu bantuvanukoku"/>
</div>
<div class="lyrico-lyrics-wrapper">jagatiki nuvvu rajuvanukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jagatiki nuvvu rajuvanukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">savaari chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savaari chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">padale rekklavwale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padale rekklavwale"/>
</div>
<div class="lyrico-lyrics-wrapper">nili ningaina chetikandale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nili ningaina chetikandale"/>
</div>
<div class="lyrico-lyrics-wrapper">savaari chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savaari chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">deekkule dhikkarinchale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deekkule dhikkarinchale"/>
</div>
<div class="lyrico-lyrics-wrapper">chukkalannunna tench tevale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chukkalannunna tench tevale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gamyam neekai nadavadule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gamyam neekai nadavadule"/>
</div>
<div class="lyrico-lyrics-wrapper">durmentain targadule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="durmentain targadule"/>
</div>
<div class="lyrico-lyrics-wrapper">kant niruke kargadule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kant niruke kargadule"/>
</div>
<div class="lyrico-lyrics-wrapper">jali ledule otamike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jali ledule otamike"/>
</div>
<div class="lyrico-lyrics-wrapper">modalvkunda phurti avadantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modalvkunda phurti avadantu"/>
</div>
<div class="lyrico-lyrics-wrapper">aduguku mundu tappatdugantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aduguku mundu tappatdugantu"/>
</div>
<div class="lyrico-lyrics-wrapper">malupulu kooda sphurti anukuntu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malupulu kooda sphurti anukuntu"/>
</div>
<div class="lyrico-lyrics-wrapper">yuddha chese sainyam laagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yuddha chese sainyam laagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">savaari chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savaari chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">padale rekklavwale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padale rekklavwale"/>
</div>
<div class="lyrico-lyrics-wrapper">nili ningaina chetikandale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nili ningaina chetikandale"/>
</div>
<div class="lyrico-lyrics-wrapper">savaari chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="savaari chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">chei savaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chei savaari"/>
</div>
<div class="lyrico-lyrics-wrapper">deekkule dhikkarinchale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="deekkule dhikkarinchale"/>
</div>
<div class="lyrico-lyrics-wrapper">chukkalannunna tench tevale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chukkalannunna tench tevale"/>
</div>
</pre>
