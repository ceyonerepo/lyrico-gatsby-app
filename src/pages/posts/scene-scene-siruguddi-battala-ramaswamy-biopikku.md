---
title: "scene scene siruguddi song lyrics"
album: "Battala Ramaswamy Biopikku"
artist: "Ram Narayan"
lyricist: "Vasudeva Murthy"
director: "Rama Narayanan"
path: "/albums/battala-ramaswamy-biopikku-lyrics"
song: "Scene Scene Siruguddi - ooru leche mundara"
image: ../../images/albumart/battala-ramaswamy-biopikku.jpg
date: 2021-05-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/4DLU2aAQnVc"
type: "happy"
singers:
  - Ishaq vali
  - Spoorthi Jithendhra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ooru leche mundara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru leche mundara"/>
</div>
<div class="lyrico-lyrics-wrapper">kokko kodi kootalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokko kodi kootalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">eedu koose mundara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eedu koose mundara"/>
</div>
<div class="lyrico-lyrics-wrapper">edaina untundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edaina untundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">padduguke mundara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padduguke mundara"/>
</div>
<div class="lyrico-lyrics-wrapper">oorege pittala gumpalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorege pittala gumpalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">muddulaade mundara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muddulaade mundara"/>
</div>
<div class="lyrico-lyrics-wrapper">edaina untundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edaina untundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">petalo unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="petalo unna"/>
</div>
<div class="lyrico-lyrics-wrapper">pedda kotalo unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pedda kotalo unna"/>
</div>
<div class="lyrico-lyrics-wrapper">datti maatarattage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="datti maatarattage"/>
</div>
<div class="lyrico-lyrics-wrapper">untundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="untundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">manchi sootulo unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manchi sootulo unna"/>
</div>
<div class="lyrico-lyrics-wrapper">potti cosilo unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potti cosilo unna"/>
</div>
<div class="lyrico-lyrics-wrapper">okate heetaruntundaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okate heetaruntundaa "/>
</div>
<div class="lyrico-lyrics-wrapper">seppeai dhanna dhanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seppeai dhanna dhanna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">gentachu gentachu dookachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gentachu gentachu dookachu"/>
</div>
<div class="lyrico-lyrics-wrapper">goppa saradaagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goppa saradaagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">gillachu geerachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gillachu geerachu"/>
</div>
<div class="lyrico-lyrics-wrapper">tikka mudiraaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tikka mudiraaka"/>
</div>
<div class="lyrico-lyrics-wrapper">gentachu dookachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gentachu dookachu"/>
</div>
<div class="lyrico-lyrics-wrapper">goppa saradaagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="goppa saradaagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">gillachu geerachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gillachu geerachu"/>
</div>
<div class="lyrico-lyrics-wrapper">tikka mudiraaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tikka mudiraaka"/>
</div>
<div class="lyrico-lyrics-wrapper">alladichelaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alladichelaa "/>
</div>
<div class="lyrico-lyrics-wrapper">edainaa seyyali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edainaa seyyali"/>
</div>
<div class="lyrico-lyrics-wrapper">ee lokam choosela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee lokam choosela"/>
</div>
<div class="lyrico-lyrics-wrapper">mana jenda paatali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana jenda paatali"/>
</div>
<div class="lyrico-lyrics-wrapper">gurranikkoncham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gurranikkoncham"/>
</div>
<div class="lyrico-lyrics-wrapper">kallale eyyali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallale eyyali"/>
</div>
<div class="lyrico-lyrics-wrapper">ille peeki pandillese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ille peeki pandillese"/>
</div>
<div class="lyrico-lyrics-wrapper">golapeyyali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="golapeyyali"/>
</div>
</pre>
