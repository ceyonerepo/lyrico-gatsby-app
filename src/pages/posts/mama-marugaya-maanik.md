---
title: "mama marugaya song lyrics"
album: "Maanik"
artist: "Dharan Kumar"
lyricist: "Mirchi Vijay"
director: "Martyn"
path: "/albums/maanik-lyrics"
song: "Mama Marugaya"
image: ../../images/albumart/maanik.jpg
date: 2019-01-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QktVrmDCwE4"
type: "happy"
singers:
  - Mirchi Vijay
  - Ma Ka Pa Anand
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">machi sharpa vey ne iruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machi sharpa vey ne iruda"/>
</div>
<div class="lyrico-lyrics-wrapper">nama opponent piece piece da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nama opponent piece piece da"/>
</div>
<div class="lyrico-lyrics-wrapper">we are alwaysu full force da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="we are alwaysu full force da"/>
</div>
<div class="lyrico-lyrics-wrapper">mama pathaley love moodu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mama pathaley love moodu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">atharaan otharaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="atharaan otharaan"/>
</div>
<div class="lyrico-lyrics-wrapper">patharaan setharan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patharaan setharan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tarak murak scene'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tarak murak scene'u"/>
</div>
<div class="lyrico-lyrics-wrapper">ye mamanaru ganu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye mamanaru ganu"/>
</div>
<div class="lyrico-lyrics-wrapper">natukaunan patukunan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natukaunan patukunan"/>
</div>
<div class="lyrico-lyrics-wrapper">maatikinan nitikina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maatikinan nitikina"/>
</div>
<div class="lyrico-lyrics-wrapper">machan thanda massu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="machan thanda massu"/>
</div>
<div class="lyrico-lyrics-wrapper">yen mamanaru close'u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen mamanaru close'u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maama marugaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama marugaya"/>
</div>
<div class="lyrico-lyrics-wrapper">moracha hogaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moracha hogaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">dandanaka darikiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dandanaka darikiya"/>
</div>
<div class="lyrico-lyrics-wrapper">night mela tarikiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="night mela tarikiya"/>
</div>
<div class="lyrico-lyrics-wrapper">poda ungaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda ungaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poda ungaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda ungaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mama marugaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mama marugaya"/>
</div>
<div class="lyrico-lyrics-wrapper">moracha hogaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moracha hogaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">dandanaka darikiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dandanaka darikiya"/>
</div>
<div class="lyrico-lyrics-wrapper">night mele tarikiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="night mele tarikiya"/>
</div>
<div class="lyrico-lyrics-wrapper">poda ungaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda ungaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poda ungaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda ungaayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yei jinthaa tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei jinthaa tha"/>
</div>
<div class="lyrico-lyrics-wrapper">jinukku jithatha jack
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jinukku jithatha jack"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maama marugaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama marugaya"/>
</div>
<div class="lyrico-lyrics-wrapper">moracha hogaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moracha hogaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">dandanaka darikiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dandanaka darikiya"/>
</div>
<div class="lyrico-lyrics-wrapper">night mela tarikiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="night mela tarikiya"/>
</div>
<div class="lyrico-lyrics-wrapper">poda ungaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda ungaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poda ungaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda ungaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mama marugaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mama marugaya"/>
</div>
<div class="lyrico-lyrics-wrapper">moracha hogaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moracha hogaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">dandanaka darikiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dandanaka darikiya"/>
</div>
<div class="lyrico-lyrics-wrapper">night mele tarikiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="night mele tarikiya"/>
</div>
<div class="lyrico-lyrics-wrapper">poda ungaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda ungaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poda ungaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda ungaayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ponna petha ma ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponna petha ma ma"/>
</div>
<div class="lyrico-lyrics-wrapper">scene'a poda la ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="scene'a poda la ma"/>
</div>
<div class="lyrico-lyrics-wrapper">maala potten naa maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maala potten naa maa"/>
</div>
<div class="lyrico-lyrics-wrapper">maama putiye mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama putiye mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">matta panney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="matta panney"/>
</div>
<div class="lyrico-lyrics-wrapper">thakitu gumukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thakitu gumukku"/>
</div>
<div class="lyrico-lyrics-wrapper">kaali panney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaali panney"/>
</div>
<div class="lyrico-lyrics-wrapper">takitu gumukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takitu gumukku"/>
</div>
<div class="lyrico-lyrics-wrapper">adichi norukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adichi norukku"/>
</div>
<div class="lyrico-lyrics-wrapper">takitu gumukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takitu gumukku"/>
</div>
<div class="lyrico-lyrics-wrapper">takitu gumukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takitu gumukku"/>
</div>
<div class="lyrico-lyrics-wrapper">takita gumukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="takita gumukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mama marugaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mama marugaya"/>
</div>
<div class="lyrico-lyrics-wrapper">mama mams yee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mama mams yee"/>
</div>
<div class="lyrico-lyrics-wrapper">moracha hogaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moracha hogaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">ye mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye mama"/>
</div>
<div class="lyrico-lyrics-wrapper">dandanaka darikiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dandanaka darikiya"/>
</div>
<div class="lyrico-lyrics-wrapper">night mela tarikiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="night mela tarikiya"/>
</div>
<div class="lyrico-lyrics-wrapper">poda ungaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda ungaayaa"/>
</div>
<div class="lyrico-lyrics-wrapper">poda ungaayaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda ungaayaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maama marugaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maama marugaya"/>
</div>
<div class="lyrico-lyrics-wrapper">mama mams yee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mama mams yee"/>
</div>
<div class="lyrico-lyrics-wrapper">moracha hogaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moracha hogaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">ye mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye mama"/>
</div>
<div class="lyrico-lyrics-wrapper">dandaka darikiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dandaka darikiya"/>
</div>
<div class="lyrico-lyrics-wrapper">night mela tarikiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="night mela tarikiya"/>
</div>
<div class="lyrico-lyrics-wrapper">poda ungaayaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda ungaayaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">mama mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mama mama"/>
</div>
<div class="lyrico-lyrics-wrapper">muthathana munji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muthathana munji"/>
</div>
<div class="lyrico-lyrics-wrapper">uththa pathatha jack
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uththa pathatha jack"/>
</div>
<div class="lyrico-lyrics-wrapper">naalu moonu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalu moonu"/>
</div>
<div class="lyrico-lyrics-wrapper">naapathi naalu jack
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naapathi naalu jack"/>
</div>
<div class="lyrico-lyrics-wrapper">yen akka ponnu alamelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen akka ponnu alamelu"/>
</div>
<div class="lyrico-lyrics-wrapper">jack jack jack jack
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jack jack jack jack"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">life is a single wicketu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life is a single wicketu da"/>
</div>
<div class="lyrico-lyrics-wrapper">adichaley free hitu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adichaley free hitu da"/>
</div>
<div class="lyrico-lyrics-wrapper">sketch perusavey nee podu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sketch perusavey nee podu da"/>
</div>
<div class="lyrico-lyrics-wrapper">yedha pannalum oorey pakkanum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yedha pannalum oorey pakkanum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thatrom thukrom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thatrom thukrom"/>
</div>
<div class="lyrico-lyrics-wrapper">therikrom galikron
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therikrom galikron"/>
</div>
<div class="lyrico-lyrics-wrapper">gunaigiri jaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gunaigiri jaru"/>
</div>
<div class="lyrico-lyrics-wrapper">yen machan semma joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen machan semma joru"/>
</div>
<div class="lyrico-lyrics-wrapper">aadurom aadurom 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadurom aadurom "/>
</div>
<div class="lyrico-lyrics-wrapper">padurom kondadurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padurom kondadurom"/>
</div>
<div class="lyrico-lyrics-wrapper">world full ah news
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="world full ah news"/>
</div>
<div class="lyrico-lyrics-wrapper">naana thanda massu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naana thanda massu"/>
</div>
</pre>
