---
title: "eswara song lyrics"
album: "Uppena"
artist: "Devi Sri Prasad"
lyricist: "Chandrabose"
director: "Buchi Babu Sana"
path: "/albums/uppena-lyrics"
song: "Eswara Parameshwara"
image: ../../images/albumart/uppena.jpg
date: 2021-02-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/o1S97DsI3jI"
type: "sad"
singers:
  - Devi Sri Prasad
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Eshwara Parameshwara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eshwara Parameshwara "/>
</div>
<div class="lyrico-lyrics-wrapper">Chudara Itu Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudara Itu Chudara"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu Kannula Manishi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu Kannula Manishi "/>
</div>
<div class="lyrico-lyrics-wrapper">Brathukunu Gunde Kannutho Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathukunu Gunde Kannutho Chudara"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuta Padani Vedhanalanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuta Padani Vedhanalanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nudhuti Kannutho Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nudhuti Kannutho Chudara"/>
</div>
<div class="lyrico-lyrics-wrapper">Eshwara Parameshwara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eshwara Parameshwara "/>
</div>
<div class="lyrico-lyrics-wrapper">Chudara Itu Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudara Itu Chudara"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhari Edho Theeramedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhari Edho Theeramedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamanamedho Gamyamedho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamanamedho Gamyamedho"/>
</div>
<div class="lyrico-lyrics-wrapper">Letha Premala Lothu Entho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Letha Premala Lothu Entho"/>
</div>
<div class="lyrico-lyrics-wrapper">Leni Kannutho Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leni Kannutho Chudara"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheekatedho Veluthuredho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekatedho Veluthuredho"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchu Edho Manta Edho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchu Edho Manta Edho"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokamerugani Premakathani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokamerugani Premakathani"/>
</div>
<div class="lyrico-lyrics-wrapper">Loni Kannutho Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loni Kannutho Chudara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eshwara Parameshwara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eshwara Parameshwara "/>
</div>
<div class="lyrico-lyrics-wrapper">Chudara Itu Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudara Itu Chudara"/>
</div>
<div class="lyrico-lyrics-wrapper">Eshwara Parameshwara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eshwara Parameshwara "/>
</div>
<div class="lyrico-lyrics-wrapper">Chudara Itu Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudara Itu Chudara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu Rasina Rathalichchata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Rasina Rathalichchata"/>
</div>
<div class="lyrico-lyrics-wrapper">Marchuthu Yemarchuthuntey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marchuthu Yemarchuthuntey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nelapaina Vinthalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nelapaina Vinthalanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi Kannutho Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi Kannutho Chudara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eshwara Parameshwara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eshwara Parameshwara "/>
</div>
<div class="lyrico-lyrics-wrapper">Chudara Itu Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudara Itu Chudara"/>
</div>
<div class="lyrico-lyrics-wrapper">Masakabarina Kantipapaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masakabarina Kantipapaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Musugu Theese Velugu Laga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musugu Theese Velugu Laga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalamadigina Kathina Prashnaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalamadigina Kathina Prashnaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhuluvai Edhuravvara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhuluvai Edhuravvara"/>
</div>
<div class="lyrico-lyrics-wrapper">Eshwara Parameshwara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eshwara Parameshwara"/>
</div>
<div class="lyrico-lyrics-wrapper">Chudara Itu Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudara Itu Chudara"/>
</div>
<div class="lyrico-lyrics-wrapper">Eshwara Parameshwara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eshwara Parameshwara"/>
</div>
<div class="lyrico-lyrics-wrapper">Chudara Itu Chudara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudara Itu Chudara"/>
</div>
</pre>
