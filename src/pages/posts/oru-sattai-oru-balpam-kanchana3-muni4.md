---
title: "oru sattai oru balpam song lyrics"
album: "Kanchana3-Muni4"
artist: "S. Thaman"
lyricist: "Saravedi Saran"
director: "Raghava Lawrence"
path: "/albums/kanchana3-muni4-lyrics"
song: "Oru Sattai Oru Balpam"
image: ../../images/albumart/kanchana3-muni4.jpg
date: 2019-04-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Dw9EsKHPW6U"
type: "Love"
singers:
  - Saravedi Saran
  - Srinidhi S
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaa Velaadalaama Kallaangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa Velaadalaama Kallaangaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Veralu Vutta Vellaangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Veralu Vutta Vellaangaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gorigalikka Mundhirikkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gorigalikka Mundhirikkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Kona Koppara Goyyakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kona Koppara Goyyakkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyaa Muiyaa Vellaattuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyaa Muiyaa Vellaattuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mattikkinaa Darrraaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattikkinaa Darrraaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Damaal Dumaal Dreamaandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damaal Dumaal Dreamaandu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattikinaa Vuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattikinaa Vuvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Sattai Oru Balpam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sattai Oru Balpam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Pantu Oru Pena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Pantu Oru Pena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Sattai Oru Balpam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Sattai Oru Balpam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Pantu Oru Pena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Pantu Oru Pena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jalsa Ice Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalsa Ice Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee En Vairakutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee En Vairakutty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siricha Sokka Thangam Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siricha Sokka Thangam Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Correctu Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Correctu Maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei Girrunuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei Girrunuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasa Maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Maathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Surrunuu S0odu Yethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surrunuu S0odu Yethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mulzhusaa Enna Thandhendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulzhusaa Enna Thandhendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whiteaa Irukkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whiteaa Irukkuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lightaa Sirikkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lightaa Sirikkuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Weightaa Idikkiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weightaa Idikkiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iduppula Madikkiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppula Madikkiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm Jillunu Paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Jillunu Paakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sallunu Thookkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sallunu Thookkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya Vechu Seendura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vechu Seendura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaiya Thoondura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiya Thoondura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala Thalanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Thalanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadandhuvandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandhuvandhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kicku Yeruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kicku Yeruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Unnadaiyaa Azhaga Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Unnadaiyaa Azhaga Paathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mindu Maarudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mindu Maarudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Black And Whiteu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Black And Whiteu Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thalaila Vaikkura Roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalaila Vaikkura Roja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Black And White Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Black And White Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thalaila Vaikkura Roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalaila Vaikkura Roja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaangadi Gulaabjaammu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangadi Gulaabjaammu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adalaam Triple Gameu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adalaam Triple Gameu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alzhagaa Sollitharenn Yethukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alzhagaa Sollitharenn Yethukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Mela Kaiya Vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Mela Kaiya Vachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannula Kuduppa Ichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannula Kuduppa Ichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palichinnu Bulbu Eriyum Paathukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palichinnu Bulbu Eriyum Paathukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haei Dama Damaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haei Dama Damaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gama Gamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gama Gamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beerah Pola Vara Chumma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beerah Pola Vara Chumma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhu Nippen Unakku Ethiradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu Nippen Unakku Ethiradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haei Kelambura Polambura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haei Kelambura Polambura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kitta Vandhaa Thayangura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Vandhaa Thayangura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Readyah Keethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Readyah Keethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whiteu Kuthiradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whiteu Kuthiradaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Udambu Fullah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Udambu Fullah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Macham Iruntha Kanni Raasi Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Macham Iruntha Kanni Raasi Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Vaangi Vacha Thabelaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Vaangi Vacha Thabelaava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ukkaanji Vaasidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkaanji Vaasidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Black And Whiteu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Black And Whiteu Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thalaila Vaikkura Roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalaila Vaikkura Roja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Black And White Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Black And White Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thalaila Vaikkura Roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalaila Vaikkura Roja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada En Karuppu Machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada En Karuppu Machaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu Pera Mayakki Vechaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu Pera Mayakki Vechaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhaga Alli Tharen Vaangikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhaga Alli Tharen Vaangikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moonu Perum Gundu Malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu Perum Gundu Malli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Povathinga Thalli Thalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povathinga Thalli Thalli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumah Pola Mella Vandhu Ottikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumah Pola Mella Vandhu Ottikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Eranguda Nerungada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Eranguda Nerungada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unaku Yetha Karumbudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaku Yetha Karumbudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadichipaaru Ellam Inikkumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadichipaaru Ellam Inikkumda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Appadiya Eppdiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Appadiya Eppdiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadalaama Kabdiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadalaama Kabdiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottupaaru Lifeuh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottupaaru Lifeuh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jolikkum Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolikkum Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chikku Chikku Chikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikku Chikku Chikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chikku Bukku Vella Kokku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chikku Bukku Vella Kokku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thotta Silukkudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotta Silukkudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Unnudaiya Lipsu Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Unnudaiya Lipsu Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dairy Milku daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dairy Milku daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Black And Whitu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Black And Whitu Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thalaila Vaikkura Roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalaila Vaikkura Roja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Black And White Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Black And White Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thalaila Vaikkura Roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalaila Vaikkura Roja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jalsaa Ice Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalsaa Ice Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee En Vairakutty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee En Vairakutty"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirichcha Sokka Thangam Dii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirichcha Sokka Thangam Dii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Di Di Di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Di Di Di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haei Girrunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haei Girrunu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasa Maathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Maathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Surrunu Soodu Yethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surrunu Soodu Yethi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhusaa Enna Thandhendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhusaa Enna Thandhendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aama Machchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama Machchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Whiteaa Irukkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whiteaa Irukkuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lightaa Sirikkuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lightaa Sirikkuraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Weightaa Idikkiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weightaa Idikkiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iduppula Madikkiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iduppula Madikkiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm Jillunu Paakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm Jillunu Paakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sallunu Thookkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sallunu Thookkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya Vecchu Seendura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Vecchu Seendura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasaiya Thoondura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaiya Thoondura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thala Thalanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala Thalanu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadandhuvandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadandhuvandhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kicku Yeruthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kicku Yeruthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Unnadaiyaa Azhaga Paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Unnadaiyaa Azhaga Paathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mindu Maarudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mindu Maarudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Black And Whitu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Black And Whitu Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thalaila Vaikkura Roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalaila Vaikkura Roja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Black And Whitu Raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Black And Whitu Raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Thalaila Vaikkura Roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thalaila Vaikkura Roja"/>
</div>
</pre>
