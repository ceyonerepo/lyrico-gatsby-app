---
title: "oru deivam song lyrics"
album: "Kannathil Muthamittal"
artist: "A. R. Rahman"
lyricist: "Vairamuthu"
director: "Mani Ratnam"
path: "/albums/kannathil-muthamittal-song-lyrics"
song: "Oru Deivam Thantha Poove"
image: ../../images/albumart/kannathil-muthamittal.jpg
date: 2002-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/crR9XoGVQqk"
type: "happy"
singers:
  - Chinmayi
  - P. Jayachandran
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nenjil Jil Jil Jil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Jil Jil Jil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathil Dhil Dhil Dhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathil Dhil Dhil Dhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathil Muthamittaal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathil Muthamittaal Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathil Muthamittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathil Muthamittaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Jil Jil Jil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Jil Jil Jil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathil Dhil Dhil Dhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathil Dhil Dhil Dhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathil Muthamittaal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathil Muthamittaal Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathil Muthamittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathil Muthamittaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Deivam Thantha Poovey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Deivam Thantha Poovey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Thedal Enna Thaiyey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Thedal Enna Thaiyey"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Deivam Thantha Poovey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Deivam Thantha Poovey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Thedal Enna Thaiyey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Thedal Enna Thaiyey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvu Thodangum Idam Nee Thaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvu Thodangum Idam Nee Thaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvu Thodangum Idam Nee Thaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvu Thodangum Idam Nee Thaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam Mudiyum Idam Nee Thaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam Mudiyum Idam Nee Thaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrai Pola Nee Vanthaiyey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrai Pola Nee Vanthaiyey"/>
</div>
<div class="lyrico-lyrics-wrapper">Swasamaaga Nee Nindraaiyey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swasamaaga Nee Nindraaiyey"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarbil Oorum Uyirey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbil Oorum Uyirey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Deivam Thantha Poovey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Deivam Thantha Poovey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Thedal Enna Thaiyey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Thedal Enna Thaiyey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Jil Jil Jil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Jil Jil Jil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathil Dhil Dhil Dhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathil Dhil Dhil Dhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathil Muthamittaal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathil Muthamittaal Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathil Muthamittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathil Muthamittaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Jil Jil Jil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Jil Jil Jil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathil Dhil Dhil Dhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathil Dhil Dhil Dhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathil Muthamittaal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathil Muthamittaal Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathil Muthamittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathil Muthamittaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enathu Sondham Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enathu Sondham Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Enathu Pagaiyum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enathu Pagaiyum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Malarum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Malarum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvil Mullum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvil Mullum Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chella Mazhaiyum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella Mazhaiyum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Idiyum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Idiyum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Chella Mazhaiyum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chella Mazhaiyum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna Idiyum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Idiyum Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pirantha Udalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirantha Udalum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyum Uyirum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyum Uyirum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pirantha Udalum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pirantha Udalum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyum Uyirum Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyum Uyirum Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam Eendra jananam Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam Eendra jananam Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Deivam Thantha Poovey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Deivam Thantha Poovey"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Thedal Enna Thaiyey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Thedal Enna Thaiyey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjil Jil Jil Jil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Jil Jil Jil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathil Dhil Dhil Dhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathil Dhil Dhil Dhil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathil Muthamittaal Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathil Muthamittaal Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannathil Muthamittaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannathil Muthamittaal"/>
</div>
</pre>
