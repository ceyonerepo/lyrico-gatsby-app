---
title: "ninnu chuse anandamlo song lyrics"
album: "Nani's Gang Leader"
artist: "Anirudh Ravichander"
lyricist: "Anantha Sriram"
director: "Vikram Kumar"
path: "/albums/nani's-gang-leader-lyrics"
song: "Ninnu Chuse Anandamlo"
image: ../../images/albumart/nanis-gang-leader.jpg
date: 2019-09-13
lang: telugu
youtubeLink: "https://www.youtube.com/embed/QlFwVjhllzQ"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Katha Raayadam Modalukaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha Raayadam Modalukaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhu Apudey Elaanti Malupo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhu Apudey Elaanti Malupo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kala Deyniko Telusukoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kala Deyniko Telusukoka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhu Apudey Ideymitalapo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhu Apudey Ideymitalapo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninu Choosey Aanandamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Choosey Aanandamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupaapey Kadalai Ponginadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupaapey Kadalai Ponginadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Taakey Aaraatam Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Taakey Aaraatam Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Neyney Vadileysaanu Kadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Neyney Vadileysaanu Kadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Bhaaramentha Nuvu Mopinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Bhaaramentha Nuvu Mopinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuu Teylikoutu Undhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuu Teylikoutu Undhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninu Choosey Aanandamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Choosey Aanandamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupaapey Kadalai Ponginadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupaapey Kadalai Ponginadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha Raayadam Modalukaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha Raayadam Modalukaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhu Apudey Elaanti Malupo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhu Apudey Elaanti Malupo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anuvanuvuna Onuku Reyginadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuvanuvuna Onuku Reyginadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanabadadhadhi Kanulakey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanabadadhadhi Kanulakey"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugaduguna Aduguthondhi Madhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugaduguna Aduguthondhi Madhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanabadadhadhi Chevulakey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanabadadhadhi Chevulakey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Medhaduki Padhi Melikaleysinadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhaduki Padhi Melikaleysinadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Teliyanidhidhi Telivikey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teliyanidhidhi Telivikey"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhivarakeruganidhi Eymitidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhivarakeruganidhi Eymitidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidharainadhi Nidharakey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidharainadhi Nidharakey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadava Thadava Godavaadinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadava Thadava Godavaadinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagani Thaguvu Padinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagani Thaguvu Padinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiga Vidiga Visiginchinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiga Vidiga Visiginchinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidani Mudulu Padenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidani Mudulu Padenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninu Choosey Aanandamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Choosey Aanandamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupaapey Kadalai Ponginadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupaapey Kadalai Ponginadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Taakey Aaraatam Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Taakey Aaraatam Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Neyney Vadileysaanu Kadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Neyney Vadileysaanu Kadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Bhaaramentha Nuvu Mopinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Bhaaramentha Nuvu Mopinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuu Teylikoutu Undhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuu Teylikoutu Undhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninu Choosey Aanandamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Choosey Aanandamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupaapey Kadalai Ponginadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupaapey Kadalai Ponginadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha Raayadam Modalukaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha Raayadam Modalukaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhu Apudey Elaanti Malupo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhu Apudey Elaanti Malupo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okatokatiga Panulu Panchukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatokatiga Panulu Panchukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Perigina Mana Chanuvuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perigina Mana Chanuvuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Suluvuga Chulakanaga Choodakani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suluvuga Chulakanaga Choodakani"/>
</div>
<div class="lyrico-lyrics-wrapper">Palikenu Prathi Kshanamilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palikenu Prathi Kshanamilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okatokatiga Teralu Tenchukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okatokatiga Teralu Tenchukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharigini Mana Velithini..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharigini Mana Velithini.."/>
</div>
<div class="lyrico-lyrics-wrapper">Porabadi Nuvu Marala Penchakani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porabadi Nuvu Marala Penchakani"/>
</div>
<div class="lyrico-lyrics-wrapper">Arichenu Prathi Kanamilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arichenu Prathi Kanamilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vethiki Vethiki Bathimaalinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethiki Vethiki Bathimaalinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gathamu Thiragabadadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gathamu Thiragabadadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaka Venaka Ani Cheysinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaka Venaka Ani Cheysinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamu Marugubadadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamu Marugubadadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninu Choosey Aanandamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Choosey Aanandamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupaapey Kadalai Ponginadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupaapey Kadalai Ponginadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Taakey Aaraatam Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Taakey Aaraatam Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Neyney Vadileysaanu Kadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Neyney Vadileysaanu Kadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Bhaaramentha Nuvu Mopinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Bhaaramentha Nuvu Mopinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasuu Teylikoutu Undhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasuu Teylikoutu Undhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninu Choosey Aanandamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Choosey Aanandamlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupaapey Kadalai Ponginadey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupaapey Kadalai Ponginadey"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha Raayadam Modalukaaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha Raayadam Modalukaaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhu Apudey Elaanti Malupo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhu Apudey Elaanti Malupo"/>
</div>
</pre>
