---
title: "my wifeu romba beautifulu song lyrics"
album: "Panjumittai"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "S.P. Mohan"
path: "/albums/panjumittai-lyrics"
song: "My Wifeu Romba Beautifulu"
image: ../../images/albumart/panjumittai.jpg
date: 2018-06-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MlIyZyR0vQM"
type: "love"
singers:
  - Diwakar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">My Wife -Uh Romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Wife -Uh Romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Beautiful -Uh Beautiful -Uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beautiful -Uh Beautiful -Uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Laala Life -Uh Aayiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Laala Life -Uh Aayiduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Color -Uh Full -Uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color -Uh Full -Uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Pudichathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Pudichathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanakkunnu Odhukki Vaippaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakkunnu Odhukki Vaippaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanakku Pudichathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakku Pudichathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhukki Vaippaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhukki Vaippaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasa Purinchi Ava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Purinchi Ava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakka Marandhu Iruppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakka Marandhu Iruppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhiyam Ezhundhu Thiruppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhiyam Ezhundhu Thiruppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Wow Wow Wow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wow Wow Wow"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Wife -Uh Romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Wife -Uh Romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Beautiful -Uh Beautiful -Uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beautiful -Uh Beautiful -Uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Laala Life -Uh Aayiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Laala Life -Uh Aayiduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Color -Uh Full -Uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color -Uh Full -Uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kal Konaa Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal Konaa Haan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kal Konaa Kamar Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kal Konaa Kamar Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Thaannu Kadhai Yum Vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Thaannu Kadhai Yum Vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda Kaasai Ellaam Karaippaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda Kaasai Ellaam Karaippaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaala Avathi Pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Avathi Pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Porenae Nadaiya Kattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porenae Nadaiya Kattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endraalae Ennai Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endraalae Ennai Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moraippaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moraippaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennudaiya Pechai Ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennudaiya Pechai Ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Maattaa Lae Maattaa Lae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattaa Lae Maattaa Lae"/>
</div>
<div class="lyrico-lyrics-wrapper">Verung Kaiyyaa Vandhu Ninnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verung Kaiyyaa Vandhu Ninnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Peyaagi Povaa Lae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peyaagi Povaa Lae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichchaalum Pudichchaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichchaalum Pudichchaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Velangkaama Adangkaama Irundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velangkaama Adangkaama Irundhaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Wife -Uh Romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Wife -Uh Romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Beautiful -Uh Beautiful -Uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beautiful -Uh Beautiful -Uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Laala Life -Uh Aayiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Laala Life -Uh Aayiduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Color -Uh Full -Uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color -Uh Full -Uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Summaavae Irundhu Puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaavae Irundhu Puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soraakka Marandhu Puttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soraakka Marandhu Puttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poi Parcel Vaangi Yaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poi Parcel Vaangi Yaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Parappaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parappaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallaa Vae Minukki Kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallaa Vae Minukki Kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kada Vaaya Adhakki Kittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada Vaaya Adhakki Kittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Night -Uh Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Night -Uh Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tv Paarthu Sirippaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tv Paarthu Sirippaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppadiyo Vaazha Aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadiyo Vaazha Aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattenae Pattenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattenae Pattenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponjaathiyaal Naanum Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponjaathiyaal Naanum Ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kettenae Kettenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettenae Kettenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Udhaichchaalum Midhichchaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udhaichchaalum Midhichchaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Ennai Madhikaama Nadanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Ennai Madhikaama Nadanthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Wife -Uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Wife -Uh"/>
</div>
<div class="lyrico-lyrics-wrapper">My Wife -Uh Romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Wife -Uh Romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Beautiful -Uh Beautiful -Uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beautiful -Uh Beautiful -Uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Laala Life -Uh Aayiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Laala Life -Uh Aayiduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Color -Uh Full -Uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Color -Uh Full -Uh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Pudichathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Pudichathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanakkunnu Odhukki Vaippaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakkunnu Odhukki Vaippaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanakku Pudichathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakku Pudichathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhukki Vaippaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhukki Vaippaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasa Purinchi Ava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Purinchi Ava"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakka Marandhu Iruppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakka Marandhu Iruppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhiyam Ezhundhu Thiruppaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhiyam Ezhundhu Thiruppaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Wow Wow Wow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wow Wow Wow"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Wife -Uh Romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Wife -Uh Romba"/>
</div>
<div class="lyrico-lyrics-wrapper">Beautiful -Uh Beautiful -Uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beautiful -Uh Beautiful -Uh"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava Laala Life -Uh Aayiduchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Laala Life -Uh Aayiduchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Blue-Vu -Uh Full -Uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Blue-Vu -Uh Full -Uh"/>
</div>
</pre>
