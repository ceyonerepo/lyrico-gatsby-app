---
title: "enga pona song lyrics"
album: "Pakka"
artist: "C. Sathya"
lyricist: "Yugabharathi"
director: "S.S. Surya"
path: "/albums/pakka-song-lyrics"
song: "Enga Pona"
image: ../../images/albumart/pakka.jpg
date: 2018-04-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/j5p-Nqie_fM"
type: "sad"
singers:
  - Reema
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enga pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enga pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enga pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ettuthikkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ettuthikkum "/>
</div>
<div class="lyrico-lyrics-wrapper">unna thedi oduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna thedi oduran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unna kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unna kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nool atthu vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool atthu vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">pattam pola aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam pola aaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalamellam en usuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamellam en usuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee iruntha sogam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee iruntha sogam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannedhirae nee therinjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannedhirae nee therinjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera onnum theva illaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera onnum theva illaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enga pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enga pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ettuthikkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ettuthikkum "/>
</div>
<div class="lyrico-lyrics-wrapper">unna thedi oduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna thedi oduran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unna kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unna kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nool atthu vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool atthu vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">pattam pola aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam pola aaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahaaaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaa aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonna sollum needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna sollum needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En sondham bandham needhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En sondham bandham needhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illayenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illayenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna theeyil poduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna theeyil poduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu patti soozha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu patti soozha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan katti vacha maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan katti vacha maala"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaiya vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaiya vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponadhenna soozhnila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponadhenna soozhnila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanama nee maraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanama nee maraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannerril naan karaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannerril naan karaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Venamae intha vazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venamae intha vazhkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee enga pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee enga pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan ettuthikkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan ettuthikkum "/>
</div>
<div class="lyrico-lyrics-wrapper">unna thedi oduran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna thedi oduran"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unna kaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unna kaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nool atthu vittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool atthu vittu "/>
</div>
<div class="lyrico-lyrics-wrapper">pattam pola aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam pola aaduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalamellam en usuraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamellam en usuraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee iruntha sogam illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee iruntha sogam illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannedhirae nee therinjaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannedhirae nee therinjaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera onnum theva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vera onnum theva illa"/>
</div>
</pre>
