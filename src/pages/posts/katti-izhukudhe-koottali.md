---
title: "katti izhukudhe song lyrics"
album: "Koottali"
artist: "Britto Michael"
lyricist: "Ekadesi"
director: "SK Mathi"
path: "/albums/koottali-song-lyrics"
song: "Katti Izhukudhe"
image: ../../images/albumart/koottali.jpg
date: 2018-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ieMQmyJ4Y48"
type: "love"
singers:
  - Krish Britto
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">katti izhukuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti izhukuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">sundi alikuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sundi alikuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">un vizhi enna thingala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vizhi enna thingala"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam thadukuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam thadukuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam adaikuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam adaikuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu enna verum thoovala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu enna verum thoovala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thada thada endre uyir 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thada thada endre uyir "/>
</div>
<div class="lyrico-lyrics-wrapper">meethu rail ena uruluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meethu rail ena uruluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">pada pada endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pada pada endre"/>
</div>
<div class="lyrico-lyrics-wrapper">manam yeno adukuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam yeno adukuthada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un paarvai valai veesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai valai veesi"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir vangi yeno ponaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir vangi yeno ponaye"/>
</div>
<div class="lyrico-lyrics-wrapper">sokkinen nanadi yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokkinen nanadi yenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">un koonthal karuvagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un koonthal karuvagam"/>
</div>
<div class="lyrico-lyrics-wrapper">oru poovai vaithu pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru poovai vaithu pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">parkaiyil ul manam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parkaiyil ul manam "/>
</div>
<div class="lyrico-lyrics-wrapper">elunthu aaduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elunthu aaduthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iruthayam kondu katril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruthayam kondu katril"/>
</div>
<div class="lyrico-lyrics-wrapper">motha imayamum sirithanathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="motha imayamum sirithanathe"/>
</div>
<div class="lyrico-lyrics-wrapper">enakena vantha kadhal seigai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakena vantha kadhal seigai"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavinai thanthu ponale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavinai thanthu ponale"/>
</div>
<div class="lyrico-lyrics-wrapper">udalil por thoduthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udalil por thoduthal"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirai nee eduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirai nee eduthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nadai pathai kadai oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadai pathai kadai oram"/>
</div>
<div class="lyrico-lyrics-wrapper">un parvai ennai thodum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un parvai ennai thodum neram"/>
</div>
<div class="lyrico-lyrics-wrapper">nirkiren yenadi yenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nirkiren yenadi yenadi"/>
</div>
<div class="lyrico-lyrics-wrapper">un moochu kaatru ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un moochu kaatru ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">en thegam thaanai thoda venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thegam thaanai thoda venum"/>
</div>
<div class="lyrico-lyrics-wrapper">thagathil en manam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagathil en manam "/>
</div>
<div class="lyrico-lyrics-wrapper">malai ketkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai ketkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kidu kidu nedu megam pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidu kidu nedu megam pola"/>
</div>
<div class="lyrico-lyrics-wrapper">ithayathin niram maruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayathin niram maruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">mithakiren un kadhal mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mithakiren un kadhal mele"/>
</div>
<div class="lyrico-lyrics-wrapper">iyarkaiyin vilai eruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyarkaiyin vilai eruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">manathai koduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manathai koduthai"/>
</div>
<div class="lyrico-lyrics-wrapper">maranam ne thaduthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranam ne thaduthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">katti izhukuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti izhukuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">sundi alikuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sundi alikuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">un vizhi enna thingala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un vizhi enna thingala"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam thadukuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam thadukuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">ullam adaikuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam adaikuthey"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu enna verum thoovala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu enna verum thoovala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thada thada endre uyir 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thada thada endre uyir "/>
</div>
<div class="lyrico-lyrics-wrapper">meethu rail ena uruluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meethu rail ena uruluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">pada pada endre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pada pada endre"/>
</div>
<div class="lyrico-lyrics-wrapper">manam yeno adukuthada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam yeno adukuthada"/>
</div>
</pre>
