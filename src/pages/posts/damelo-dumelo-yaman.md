---
title: "damelo dumelo song lyrics"
album: "Yaman"
artist: "Vijay Antony"
lyricist: "P Vetriselvan"
director: "Jeeva Shankar"
path: "/albums/yaman-lyrics"
song: "Damelo Dumelo"
image: ../../images/albumart/yaman.jpg
date: 2017-02-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/F-zGAfJKRZA"
type: "happy"
singers:
  - Saranya
  - Maria Roe Vincent
  - El Fe
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Damelo Dumelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damelo Dumelo"/>
</div>
<div class="lyrico-lyrics-wrapper">Damelo Dumelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damelo Dumelo "/>
</div>
<div class="lyrico-lyrics-wrapper">Damelo Dumelo Enjoy Polymelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damelo Dumelo Enjoy Polymelo "/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamai Paruvam Irukkum Bozhudhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamai Paruvam Irukkum Bozhudhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Piravi Payanai Muzhudhai Adaindhidu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piravi Payanai Muzhudhai Adaindhidu "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikum Kaatrai Pol 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikum Kaatrai Pol "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhisai Yavum Parandhu Sellu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai Yavum Parandhu Sellu "/>
</div>
<div class="lyrico-lyrics-wrapper">Uyaram Edhuvum Illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram Edhuvum Illai "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Neeye Mudivu Pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Neeye Mudivu Pannu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Damelo Dumelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damelo Dumelo "/>
</div>
<div class="lyrico-lyrics-wrapper">Damelo Dumelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damelo Dumelo "/>
</div>
<div class="lyrico-lyrics-wrapper">Damelo Dumelo Enjoy Polymelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damelo Dumelo Enjoy Polymelo "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manadhil Amaidhi Irundhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhil Amaidhi Irundhal "/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhkai Dhan Inithidume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkai Dhan Inithidume "/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai Marandhal Yaavum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai Marandhal Yaavum "/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaga Therindhidume 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaga Therindhidume "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogam Endre Solle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Endre Solle "/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Illai Yendru Sol 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Illai Yendru Sol "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Pogira Pokil Punnagai Veese 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Pogira Pokil Punnagai Veese "/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram Idhaiyam Vel 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Idhaiyam Vel "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Patri Kavalai Vendam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Patri Kavalai Vendam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalar Thooki Sel 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalar Thooki Sel "/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ninaipadhai Pola 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ninaipadhai Pola "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkai Ondrum Siramam Illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Ondrum Siramam Illai "/>
</div>
<div class="lyrico-lyrics-wrapper">Damelo Dumelo Damelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damelo Dumelo Damelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Damelo Dumelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damelo Dumelo"/>
</div>
<div class="lyrico-lyrics-wrapper">Damelo Dumelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damelo Dumelo "/>
</div>
<div class="lyrico-lyrics-wrapper">Damelo Dumelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damelo Dumelo "/>
</div>
<div class="lyrico-lyrics-wrapper">Here It Comes From Outer Space
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Here It Comes From Outer Space"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Around The World Now
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Around The World Now"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpai Nambu Nanban 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpai Nambu Nanban "/>
</div>
<div class="lyrico-lyrics-wrapper">Illamal Evanum Illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illamal Evanum Illai "/>
</div>
<div class="lyrico-lyrics-wrapper">Aano Penno Pazhagu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aano Penno Pazhagu "/>
</div>
<div class="lyrico-lyrics-wrapper">Friendshipil Kanaku Illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendshipil Kanaku Illai "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbai Mattum Pazhagi Paaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbai Mattum Pazhagi Paaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazkai Inikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazkai Inikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizhaigal Kooda Azhagai Theriyum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizhaigal Kooda Azhagai Theriyum "/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum Rusikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum Rusikum "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazum Bodhe Ella Sugamum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazum Bodhe Ella Sugamum "/>
</div>
<div class="lyrico-lyrics-wrapper">Anubavi Daa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubavi Daa "/>
</div>
<div class="lyrico-lyrics-wrapper">Orumurai Thaane Idha Piravi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orumurai Thaane Idha Piravi "/>
</div>
<div class="lyrico-lyrics-wrapper">Enjoy Pannu Damelo Dumelo Damelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjoy Pannu Damelo Dumelo Damelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Damelo Dumelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damelo Dumelo"/>
</div>
<div class="lyrico-lyrics-wrapper">Damelo Dumelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damelo Dumelo "/>
</div>
<div class="lyrico-lyrics-wrapper">Damelo Dumelo Enjoy Polymelo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Damelo Dumelo Enjoy Polymelo "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilamai Paruvam Irukkum Bozhudhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamai Paruvam Irukkum Bozhudhe "/>
</div>
<div class="lyrico-lyrics-wrapper">Piravipayanai Muzhudhai Adaindhidu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piravipayanai Muzhudhai Adaindhidu "/>
</div>
<div class="lyrico-lyrics-wrapper">Adikum Kaatraipol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikum Kaatraipol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhisai Yavum Parandhu Selu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhisai Yavum Parandhu Selu "/>
</div>
<div class="lyrico-lyrics-wrapper">Uyaram Edhum Illai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram Edhum Illai "/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Neeye Mudivu Pannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Neeye Mudivu Pannu"/>
</div>
</pre>
