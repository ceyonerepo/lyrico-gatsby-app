---
title: "istam song lyrics"
album: "Khiladi"
artist: "Devi Sri Prasad"
lyricist: "Shree Mani"
director: "Ramesh Varma"
path: "/albums/khiladi-lyrics"
song: "Istam"
image: ../../images/albumart/khiladi.jpg
date: 2022-02-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/BMmQSraC_oo"
type: "happy"
singers:
  - Hari Priya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chinnapudu naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnapudu naaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma gorumuddha istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma gorumuddha istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaastedhigaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaastedhigaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Bamma gorintaku istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bamma gorintaku istam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ballokelle vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ballokelle vela"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu jallu ante istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu jallu ante istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paitesinaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paitesinaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugguleyyadam istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugguleyyadam istam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kottha avakaya mukkante istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha avakaya mukkante istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakka inti poola mokkante istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka inti poola mokkante istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthakante nenu ante naku istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthakante nenu ante naku istam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaani ippudu naaku okate istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani ippudu naaku okate istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi naakosam nuvvu pade kastam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi naakosam nuvvu pade kastam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thellarangane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thellarangane"/>
</div>
<div class="lyrico-lyrics-wrapper">Vechanaina coffee istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vechanaina coffee istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullasam penche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullasam penche"/>
</div>
<div class="lyrico-lyrics-wrapper">Swatchamaina sophie istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swatchamaina sophie istam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adham mundhara naaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adham mundhara naaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Andham adhadam istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andham adhadam istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa andham chusi lokham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa andham chusi lokham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aha oho ante istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aha oho ante istam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Goduguleni vela vaanante istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goduguleni vela vaanante istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veluguleni vela taaralu istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veluguleni vela taaralu istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nidara rani vela jola paata istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidara rani vela jola paata istam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaani ippudu naaku okate istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani ippudu naaku okate istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi naakosam nuvvu pade kastam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi naakosam nuvvu pade kastam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Reppala thalupu moosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppala thalupu moosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu kanadame istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu kanadame istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhiki hathukupoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhiki hathukupoye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathalu vinadamante istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathalu vinadamante istam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chethi gaajulu chese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chethi gaajulu chese"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipi allarante istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipi allarante istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaali muvvalu cheppe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaali muvvalu cheppe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotha kaburulante istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotha kaburulante istam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oohalni penche ekantham istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohalni penche ekantham istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oopirini penche chirugali istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oopirini penche chirugali istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pranamiche gunde chappudentho istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranamiche gunde chappudentho istam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaani ippudu naaku okate istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaani ippudu naaku okate istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhi naakosam nuvvu pade kastam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhi naakosam nuvvu pade kastam"/>
</div>
</pre>
