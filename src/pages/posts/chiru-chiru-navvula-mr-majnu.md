---
title: "chiru chiru navvula song lyrics"
album: "Mr. Majnu"
artist: "S. Thaman"
lyricist: "Sri Mani"
director: "Venky Atluri"
path: "/albums/mr-majnu-lyrics"
song: "Chiru Chiru Navvula"
image: ../../images/albumart/mr-majnu.jpg
date: 2019-01-25
lang: telugu
youtubeLink: "https://www.youtube.com/embed/BDKw1q2QsDg"
type: "happy"
singers:
  - Tushar Joshi
  - Koti Salur
  - Ramya Behara
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chiru chiru navvula sandadi saayamtram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiru chiru navvula sandadi saayamtram"/>
</div>
<div class="lyrico-lyrics-wrapper">pandaga kaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandaga kaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chura chura choopula chaatuna chilipidanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chura chura choopula chaatuna chilipidanam"/>
</div>
<div class="lyrico-lyrics-wrapper">urakalu veyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urakalu veyadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Samgeetam paliketi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samgeetam paliketi"/>
</div>
<div class="lyrico-lyrics-wrapper">ee nimisham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee nimisham"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello chilikindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello chilikindi"/>
</div>
<div class="lyrico-lyrics-wrapper">santosham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santosham"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppatiki teeraduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppatiki teeraduga"/>
</div>
<div class="lyrico-lyrics-wrapper">prema runam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prema runam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalahaane cheripese
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalahaane cheripese"/>
</div>
<div class="lyrico-lyrics-wrapper">ee tarunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee tarunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa painunna taarallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa painunna taarallo"/>
</div>
<div class="lyrico-lyrics-wrapper">dooraalennunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dooraalennunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuraagaala aakaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuraagaala aakaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">annitini kalipenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annitini kalipenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiru chiru navvula sandadi saayamtram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiru chiru navvula sandadi saayamtram"/>
</div>
<div class="lyrico-lyrics-wrapper">pandaga kaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandaga kaadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna prema daachalekaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna prema daachalekaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna prema daachalekaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna prema daachalekaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tana maatalne daachindi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tana maatalne daachindi"/>
</div>
<div class="lyrico-lyrics-wrapper">o hrudayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o hrudayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnamaata cheppalekaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnamaata cheppalekaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ilaa mounamga nilchunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilaa mounamga nilchunte"/>
</div>
<div class="lyrico-lyrics-wrapper">em laabham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="em laabham"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnavi marache varame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnavi marache varame"/>
</div>
<div class="lyrico-lyrics-wrapper">undi kadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undi kadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Repati aduge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repati aduge"/>
</div>
<div class="lyrico-lyrics-wrapper">netiki vaaradhigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="netiki vaaradhigaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chuttoota chuttaala sandadilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttoota chuttaala sandadilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Parivaaram perigeti pandirilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parivaaram perigeti pandirilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa painunna taarallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa painunna taarallo"/>
</div>
<div class="lyrico-lyrics-wrapper">dooraalennunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dooraalennunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuraagaala aakaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuraagaala aakaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">annitini kalipenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annitini kalipenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buddhimantudalle veede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buddhimantudalle veede"/>
</div>
<div class="lyrico-lyrics-wrapper">tana choopulto maayedo chesaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tana choopulto maayedo chesaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Venna dongalaanti vaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venna dongalaanti vaade"/>
</div>
<div class="lyrico-lyrics-wrapper">kanne manasulni dochese soggaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne manasulni dochese soggaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaaram penchi allari chestaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaaram penchi allari chestaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedekkada unte akkada vedukale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedekkada unte akkada vedukale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee aananda sandoha samayaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee aananda sandoha samayaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saritoogavepaati sirulainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saritoogavepaati sirulainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa painunna taarallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa painunna taarallo"/>
</div>
<div class="lyrico-lyrics-wrapper">dooraalennunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dooraalennunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuraagaala aakaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuraagaala aakaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">annitini kalipenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annitini kalipenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiru chiru navvula sandadi saayamtram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiru chiru navvula sandadi saayamtram"/>
</div>
<div class="lyrico-lyrics-wrapper">pandaga kaadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pandaga kaadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chura chura choopula chaatuna chilipidanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chura chura choopula chaatuna chilipidanam"/>
</div>
<div class="lyrico-lyrics-wrapper">urakalu veyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urakalu veyadaa"/>
</div>
</pre>
