---
title: "priya priya song lyrics"
album: "A1"
artist: "Santhosh Narayanan"
lyricist: "Rokesh"
director: "Johnson K"
path: "/albums/a1-lyrics"
song: "Priya Priya"
image: ../../images/albumart/a1.jpg
date: 2019-07-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NT3_MPHucDU"
type: "love"
singers:
  - Gana Muthu
  - Isaivani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa koorukatta kuppama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa koorukatta kuppama"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkooda vanththa nippenma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda vanththa nippenma"/>
</div>
<div class="lyrico-lyrics-wrapper">Noovama daavu adippenma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noovama daavu adippenma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee road u kadai curry vadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee road u kadai curry vadai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan unakku yetha methu vadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan unakku yetha methu vadai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkama kadhalippoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkama kadhalippoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaithunu poidiviya ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaithunu poidiviya ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruntha irukki kattipen maam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruntha irukki kattipen maam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa priya priya ennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa priya priya ennai "/>
</div>
<div class="lyrico-lyrics-wrapper">serthupiya di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serthupiya di"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa free ah free ah free ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa free ah free ah free ah "/>
</div>
<div class="lyrico-lyrics-wrapper">ennai paarthupiya di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai paarthupiya di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dey paiya paiya paiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dey paiya paiya paiya "/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum palana kedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum palana kedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa aiyya aiyya aiyya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa aiyya aiyya aiyya "/>
</div>
<div class="lyrico-lyrics-wrapper">naanum unnoda jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum unnoda jodi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethurula vanthu nee daaladikkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethurula vanthu nee daaladikkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Bussunu poonthu en kichitte maara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bussunu poonthu en kichitte maara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhe thottu naan pinnadiya yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhe thottu naan pinnadiya yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadiya thattu suththalam vaa oore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadiya thattu suththalam vaa oore"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedinu varuven voottanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedinu varuven voottanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kunthinu pesa varom thaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunthinu pesa varom thaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nasty illatha ennanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nasty illatha ennanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Paiya kadhale sollu kaathanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paiya kadhale sollu kaathanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh priya priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh priya priya"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh priya priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh priya priya"/>
</div>
<div class="lyrico-lyrics-wrapper">Priya priya ennada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priya priya ennada"/>
</div>
<div class="lyrico-lyrics-wrapper">Free ah priya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Free ah priya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa priya priya ennai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa priya priya ennai "/>
</div>
<div class="lyrico-lyrics-wrapper">serthupiya di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serthupiya di"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa free ah free ah free ah 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa free ah free ah free ah "/>
</div>
<div class="lyrico-lyrics-wrapper">ennai paarthupiya di
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai paarthupiya di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dey paiya paiya paiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dey paiya paiya paiya "/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum palana kedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum palana kedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa aiyya aiyya aiyya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa aiyya aiyya aiyya "/>
</div>
<div class="lyrico-lyrics-wrapper">naanum unnoda jodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum unnoda jodi"/>
</div>
</pre>
