---
title: "kaattu karuvamulla song lyrics"
album: "Panjumittai"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "S.P. Mohan"
path: "/albums/panjumittai-lyrics"
song: "Kaattu Karuvamulla"
image: ../../images/albumart/panjumittai.jpg
date: 2018-06-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gcityvUFGbw"
type: "happy"
singers:
  - T.L. Maharajan
  - Nithyasree Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kaatu karuva mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu karuva mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthuriye pinju pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthuriye pinju pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">mulla eduka vaayendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mulla eduka vaayendi"/>
</div>
<div class="lyrico-lyrics-wrapper">munnala munnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnala munnale"/>
</div>
<div class="lyrico-lyrics-wrapper">mull edutha naan tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mull edutha naan tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">poo maala poo maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo maala poo maala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatu karuva mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu karuva mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthalaiye kanni pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthalaiye kanni pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">mull eduka sollatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mull eduka sollatha"/>
</div>
<div class="lyrico-lyrics-wrapper">kannala kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannala kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">mull eduka nee paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mull eduka nee paru"/>
</div>
<div class="lyrico-lyrics-wrapper">vera aala vera aala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vera aala vera aala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ayya ayya ayya ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya ayya ayya ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">ayya ayya ayya ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya ayya ayya ayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaada koluthiraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaada koluthiraye"/>
</div>
<div class="lyrico-lyrics-wrapper">kattalaga kekuraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattalaga kekuraye"/>
</div>
<div class="lyrico-lyrics-wrapper">thaali kodukum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaali kodukum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum thara maaten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum thara maaten"/>
</div>
<div class="lyrico-lyrics-wrapper">robam alunjalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="robam alunjalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nerungi vara maten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerungi vara maten"/>
</div>
<div class="lyrico-lyrics-wrapper">pothum niruthi vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pothum niruthi vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">pecha valakaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pecha valakaatha"/>
</div>
<div class="lyrico-lyrics-wrapper">satham vadipathu pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham vadipathu pola"/>
</div>
<div class="lyrico-lyrics-wrapper">aala kavukatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala kavukatha"/>
</div>
<div class="lyrico-lyrics-wrapper">serum nenapu iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="serum nenapu iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">oola anupi vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oola anupi vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">maarum iva manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarum iva manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">maala koduthu vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maala koduthu vidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ayya ayya ayya ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya ayya ayya ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">ayya ayya ayya ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya ayya ayya ayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaatu karuva mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu karuva mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthuriye pinju pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthuriye pinju pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">mulla eduka vaayendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mulla eduka vaayendi"/>
</div>
<div class="lyrico-lyrics-wrapper">munnala munnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munnala munnale"/>
</div>
<div class="lyrico-lyrics-wrapper">mull edutha naan tharen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mull edutha naan tharen"/>
</div>
<div class="lyrico-lyrics-wrapper">poo maala poo maala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poo maala poo maala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ayya ayya ayya ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya ayya ayya ayya"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatu karuva mulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu karuva mulla"/>
</div>
<div class="lyrico-lyrics-wrapper">oh oh oh oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh"/>
</div>
<div class="lyrico-lyrics-wrapper">ayya ayya ayya ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya ayya ayya ayya"/>
</div>
</pre>
