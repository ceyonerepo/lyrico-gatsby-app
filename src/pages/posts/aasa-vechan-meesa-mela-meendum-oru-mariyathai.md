---
title: "aasa vechan meesa mela song lyrics"
album: "Meendum Oru Mariyathai"
artist: "N. R. Raghunanthan - Sharran Surya - Yuvan Shankar Raja"
lyricist: "Na. Muthukumar"
director: "Bharathiraja"
path: "/albums/meendum-oru-mariyathai-lyrics"
song: "Aasa Vechan Meesa Mela"
image: ../../images/albumart/meendum-oru-mariyathai.jpg
date: 2020-02-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/uiZ8J4JPfN0"
type: "love"
singers:
  - Priya Himesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aasa vechan meesa mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa vechan meesa mela"/>
</div>
<div class="lyrico-lyrics-wrapper">meesa vacha maman mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meesa vacha maman mela"/>
</div>
<div class="lyrico-lyrics-wrapper">silvanda ponvandaa mathiputane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silvanda ponvandaa mathiputane"/>
</div>
<div class="lyrico-lyrics-wrapper">antha allinagaram kulathu mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha allinagaram kulathu mela"/>
</div>
<div class="lyrico-lyrics-wrapper">thulli odum keluthi pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thulli odum keluthi pola"/>
</div>
<div class="lyrico-lyrics-wrapper">alai alaiyai enna ipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alai alaiyai enna ipo"/>
</div>
<div class="lyrico-lyrics-wrapper">sutha vitane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sutha vitane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethathula pakum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethathula pakum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">pesa thonuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesa thonuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">un kitathula nerungum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kitathula nerungum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam thorathuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam thorathuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un saaya veti thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un saaya veti thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">nan pothikitu padupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan pothikitu padupen"/>
</div>
<div class="lyrico-lyrics-wrapper">ne sethu vacha arivai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne sethu vacha arivai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">pullaiyakki kodupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullaiyakki kodupen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">anga vachen inga vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anga vachen inga vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">enga vachen kanala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enga vachen kanala"/>
</div>
<div class="lyrico-lyrics-wrapper">tholanjathu nu therunja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tholanjathu nu therunja"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnum manasu thedala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnum manasu thedala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kannu vachen kannam vachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu vachen kannam vachen"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku athu puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku athu puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">pambarama suthurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pambarama suthurene"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku mudiyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aatukutti naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatukutti naan"/>
</div>
<div class="lyrico-lyrics-wrapper">enna patti pothu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna patti pothu than"/>
</div>
<div class="lyrico-lyrics-wrapper">un aasa theera than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aasa theera than"/>
</div>
<div class="lyrico-lyrics-wrapper">enna vetti potuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna vetti potuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ne sanju pakum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne sanju pakum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">nan sarinju pogurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan sarinju pogurene"/>
</div>
<div class="lyrico-lyrics-wrapper">ena thooki thooki chellam konju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena thooki thooki chellam konju"/>
</div>
<div class="lyrico-lyrics-wrapper">kenji kekurene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kenji kekurene"/>
</div>
</pre>
