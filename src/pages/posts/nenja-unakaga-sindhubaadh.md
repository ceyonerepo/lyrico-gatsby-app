---
title: "nenja unakaga song lyrics"
album: "Sindhubaadh"
artist: "Yuvan Shankar Raja"
lyricist: "Vivek"
director: "S.U. Arun Kumar"
path: "/albums/sindhubaadh-lyrics"
song: "Nenja Unakaga"
image: ../../images/albumart/sindhubaadh.jpg
date: 2019-06-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bzeegaj9lS0"
type: "love"
singers:
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nenja Unakaaga Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja Unakaaga Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhukki Vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhukki Vechen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engum Kodukkkama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engum Kodukkkama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senjum Sethukkama Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senjum Sethukkama Vitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kora Nilavaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kora Nilavaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Kidaikaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kidaikaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhaa Vaettiyila Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaa Vaettiyila Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neththi Kottula Konja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neththi Kottula Konja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edam Keppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edam Keppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Otha Nila Kenikkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otha Nila Kenikkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sitterumbbu Seenikkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitterumbbu Seenikkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Vaiya Maarukkulla Verattaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vaiya Maarukkulla Verattaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maththu Vecha Morukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththu Vecha Morukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattikitta Eee Ah Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikitta Eee Ah Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Suththi Vaaren Pulla Mayakkaatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Suththi Vaaren Pulla Mayakkaatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukulla Unna Vechchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukulla Unna Vechchu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nitham Nitham Kadhalichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nitham Nitham Kadhalichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motta Veiyil Sutta Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motta Veiyil Sutta Kooda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Theruvil Thee Kulichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Theruvil Thee Kulichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kitta Thatta Unna Kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta Thatta Unna Kaattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ella Sollum Segarichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ella Sollum Segarichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maththathellam Veesiputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththathellam Veesiputtu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Peasa Arambichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Peasa Arambichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhaa Vaettiyila Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhaa Vaettiyila Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Kappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Kappen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neththi Kottula Konja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neththi Kottula Konja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edam Keppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edam Keppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Nila Kenikkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Nila Kenikkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sitterumbu Seenikkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitterumbu Seenikkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Vaiya Maarukkulla Verataatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vaiya Maarukkulla Verataatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maththu Vecha Morukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththu Vecha Morukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattikitta Eee Ah Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikitta Eee Ah Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Suththi Vaaren Pulla Mayakkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Suththi Vaaren Pulla Mayakkatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oththa Nila Kenikkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oththa Nila Kenikkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sitterumbu Seenikkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitterumbu Seenikkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Vaiya Maarukkulla Verataatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vaiya Maarukkulla Verataatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maththu Vecha Morukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maththu Vecha Morukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maattikitta Eee Ah Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maattikitta Eee Ah Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Suththi Vaaren Pulla Mayakkatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Suththi Vaaren Pulla Mayakkatha"/>
</div>
</pre>
