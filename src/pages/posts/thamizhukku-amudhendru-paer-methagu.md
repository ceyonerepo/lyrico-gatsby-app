---
title: "thamizhukku amudhendru paer song lyrics"
album: "Methagu"
artist: "Praveen Kumar"
lyricist: "Thirukumaran"
director: "T. Kittu"
path: "/albums/methagu-lyrics"
song: "Thamizhukku Amudhendru Paer"
image: ../../images/albumart/methagu.jpg
date: 2021-06-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WzukzJX9nH4"
type: "mass"
singers:
  - Roja Aditya 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thamizhuku amuthendru per antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhuku amuthendru per antha"/>
</div>
<div class="lyrico-lyrics-wrapper">thamil inba thamil engal uyiruku ner
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamil inba thamil engal uyiruku ner"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thamizhuku nilavendru per inba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhuku nilavendru per inba"/>
</div>
<div class="lyrico-lyrics-wrapper">thamil engal samugathin vilaivuku neer
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamil engal samugathin vilaivuku neer"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thamizhuku manamendru per inba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhuku manamendru per inba"/>
</div>
<div class="lyrico-lyrics-wrapper">thamil engal vaalvuku nirumitha oor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamil engal vaalvuku nirumitha oor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thamizhuku mathuvendru per inba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamizhuku mathuvendru per inba"/>
</div>
<div class="lyrico-lyrics-wrapper">thamil engal urimai sem payiruku ver
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamil engal urimai sem payiruku ver"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ettuthogai ettukulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettuthogai ettukulla "/>
</div>
<div class="lyrico-lyrics-wrapper">enpurukum paatukalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enpurukum paatukalai"/>
</div>
<div class="lyrico-lyrics-wrapper">sota sota solli nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sota sota solli nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">sokka vaitha sothi mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sokka vaitha sothi mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pathu paatu sollum mannin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu paatu sollum mannin"/>
</div>
<div class="lyrico-lyrics-wrapper">sathu paatul inthinaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathu paatul inthinaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">vithu vitha nattu engal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vithu vitha nattu engal"/>
</div>
<div class="lyrico-lyrics-wrapper">sothu aana soora mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sothu aana soora mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vinnanam sollum antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnanam sollum antha"/>
</div>
<div class="lyrico-lyrics-wrapper">meinaanam enna vendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meinaanam enna vendru"/>
</div>
<div class="lyrico-lyrics-wrapper">thonnoolin kaapiyathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thonnoolin kaapiyathil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaati vita aathi mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaati vita aathi mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naalaidikul solla vaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalaidikul solla vaitha"/>
</div>
<div class="lyrico-lyrics-wrapper">nalulugin thalarathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalulugin thalarathai"/>
</div>
<div class="lyrico-lyrics-wrapper">eeradikul solla vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeradikul solla vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">ettu varai sollum mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ettu varai sollum mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mooventhar aanda mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mooventhar aanda mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">muchangam kanda mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muchangam kanda mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">paavendhar vendharena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavendhar vendharena"/>
</div>
<div class="lyrico-lyrics-wrapper">paarumendru vaaltha mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarumendru vaaltha mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">porparani paadu mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porparani paadu mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">porilaram paartha mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porilaram paartha mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">aarparitha vangan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aarparitha vangan thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">aatchi seitha anbu mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatchi seitha anbu mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tamil engal ilamaiku paal - inba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil engal ilamaiku paal - inba"/>
</div>
<div class="lyrico-lyrics-wrapper">thamil nalla pugal mikka pulavarku vel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thamil nalla pugal mikka pulavarku vel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tamil engal uyarvuku vaan inba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil engal uyarvuku vaan inba"/>
</div>
<div class="lyrico-lyrics-wrapper">tamil engal asathiku sudar thantha then
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil engal asathiku sudar thantha then"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tamil engal arivuku thol inba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil engal arivuku thol inba"/>
</div>
<div class="lyrico-lyrics-wrapper">tamil engal kavithaiku vayirathin vaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil engal kavithaiku vayirathin vaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">tamil engal piraviku thai inba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil engal piraviku thai inba"/>
</div>
<div class="lyrico-lyrics-wrapper">tamil engal valamika ulamutra thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tamil engal valamika ulamutra thee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thennatin ootru mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thennatin ootru mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">thenin guna maana mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenin guna maana mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">annalil vaaltha padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annalil vaaltha padi"/>
</div>
<div class="lyrico-lyrics-wrapper">innalum vaalum mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innalum vaalum mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaan thondri vitathena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaan thondri vitathena"/>
</div>
<div class="lyrico-lyrics-wrapper">vaiyamullor kanda thinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaiyamullor kanda thinam"/>
</div>
<div class="lyrico-lyrics-wrapper">than thondri vantha mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than thondri vantha mozhi"/>
</div>
<div class="lyrico-lyrics-wrapper">than iraval attra mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="than iraval attra mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vetru mozhi sorkal ethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetru mozhi sorkal ethum"/>
</div>
<div class="lyrico-lyrics-wrapper">eetru kolla thevai indri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eetru kolla thevai indri"/>
</div>
<div class="lyrico-lyrics-wrapper">ootru konda sollai konde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ootru konda sollai konde"/>
</div>
<div class="lyrico-lyrics-wrapper">aatral kanda enga mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatral kanda enga mozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vel veesi vendhar vaagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vel veesi vendhar vaagai"/>
</div>
<div class="lyrico-lyrics-wrapper">vetchi soodi aanda muthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetchi soodi aanda muthal"/>
</div>
<div class="lyrico-lyrics-wrapper">kolam maari kaalam poyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolam maari kaalam poyum"/>
</div>
<div class="lyrico-lyrics-wrapper">maaridatha enga mozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaridatha enga mozhi"/>
</div>
</pre>
