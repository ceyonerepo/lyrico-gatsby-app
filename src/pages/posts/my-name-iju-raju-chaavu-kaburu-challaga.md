---
title: "my name iju raju song lyrics"
album: "Chaavu Kaburu Challaga"
artist: "Jakes Bejoy"
lyricist: "Karunakar Adigarla"
director: "Pegallapati Koushik"
path: "/albums/chaavu-kaburu-challaga-lyrics"
song: "My Name Iju Raju"
image: ../../images/albumart/chaavu-kaburu-challaga.jpg
date: 2021-03-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/uWfP1ZE4PYo"
type: "mass"
singers:
  - L. V. Revanth
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Yetta Yetta Yetta Yetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Yetta Yetta Yetta Yetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Yettaga Puttavuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yettaga Puttavuro"/>
</div>
<div class="lyrico-lyrics-wrapper">Attaa Attaa Attaa Attaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attaa Attaa Attaa Attaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Attaage Pothavuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attaage Pothavuro"/>
</div>
<div class="lyrico-lyrics-wrapper">Itte Itte Itte Itte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itte Itte Itte Itte"/>
</div>
<div class="lyrico-lyrics-wrapper">Itte Feel Aipotharendhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itte Feel Aipotharendhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Satthe Satthe Satthe Satthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satthe Satthe Satthe Satthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Satthe Emouthaadhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satthe Emouthaadhiro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Gaalo Deepam Gundello Pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Gaalo Deepam Gundello Pranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Epudu Thussantundho Evadiki Telusunu Lera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epudu Thussantundho Evadiki Telusunu Lera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontlo Jeevam Kaadhe Mana Sontham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontlo Jeevam Kaadhe Mana Sontham"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnanalu Pandaga Chesi Paadekkei Ra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnanalu Pandaga Chesi Paadekkei Ra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poyevani Ponivaka Nee Edupu Endhukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyevani Ponivaka Nee Edupu Endhukura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Asthi Geesthi Emaina Aadattukupothada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Asthi Geesthi Emaina Aadattukupothada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotalloni Rarajaina Katiki Povala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotalloni Rarajaina Katiki Povala"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Nenu Evadaina Kattello Kalala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Nenu Evadaina Kattello Kalala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Name Iju Raju Basti Bala Raju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Name Iju Raju Basti Bala Raju"/>
</div>
<div class="lyrico-lyrics-wrapper">Chavu Kaburu Challaga Chebutha Prathiroju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chavu Kaburu Challaga Chebutha Prathiroju"/>
</div>
<div class="lyrico-lyrics-wrapper">My Name Iju Raju Basti Bala Raju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Name Iju Raju Basti Bala Raju"/>
</div>
<div class="lyrico-lyrics-wrapper">Chavu Kaburu Challaga Chebutha Prathiroju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chavu Kaburu Challaga Chebutha Prathiroju"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Yetta Yetta Yetta Yetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Yetta Yetta Yetta Yetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Yettaga Puttavuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yettaga Puttavuro"/>
</div>
<div class="lyrico-lyrics-wrapper">Attaa Attaa Attaa Attaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attaa Attaa Attaa Attaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Attaage Pothavuro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attaage Pothavuro"/>
</div>
<div class="lyrico-lyrics-wrapper">Itte Itte Itte Itte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itte Itte Itte Itte"/>
</div>
<div class="lyrico-lyrics-wrapper">Itte Feel Aipotharendhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itte Feel Aipotharendhiro"/>
</div>
<div class="lyrico-lyrics-wrapper">Satthe Satthe Satthe Satthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satthe Satthe Satthe Satthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Satthe Emouthaadhiro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satthe Emouthaadhiro"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chuttam Choopuku Vasthaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chuttam Choopuku Vasthaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pettindhala Thintaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettindhala Thintaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Permanentga Aa Intlone
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Permanentga Aa Intlone"/>
</div>
<div class="lyrico-lyrics-wrapper">Baitayincham Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baitayincham Gaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cinema Poster Choostham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cinema Poster Choostham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho Ticket Teesi Veltham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho Ticket Teesi Veltham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayipoyaka Khurchi Khaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayipoyaka Khurchi Khaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheyyaka Thappadhuga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheyyaka Thappadhuga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaradugula Body Anthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaradugula Body Anthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Addheku Untunnamanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addheku Untunnamanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Body Kompani Vadhileyali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Body Kompani Vadhileyali"/>
</div>
<div class="lyrico-lyrics-wrapper">Time-e Ayipothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time-e Ayipothe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puttetappudu Oopesthaaru Ninne Uyyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puttetappudu Oopesthaaru Ninne Uyyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Poyetappudu Nalugu Vachhi Chakka Moyyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poyetappudu Nalugu Vachhi Chakka Moyyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnannalu Aa Nalugurini Sampaadhinchala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnannalu Aa Nalugurini Sampaadhinchala"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorantha Ninu Ooreginchi Tata Seppala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorantha Ninu Ooreginchi Tata Seppala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Swarganiki Tholimettu Na Bandera Ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swarganiki Tholimettu Na Bandera Ottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvadaina Sachhadante Naake Phone Kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvadaina Sachhadante Naake Phone Kottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Swarganiki Tholimettu Na Bandera Ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swarganiki Tholimettu Na Bandera Ottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvadaina Sachhadante Naake Phone Kottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvadaina Sachhadante Naake Phone Kottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonnu Daughter Avutham Sister Brother Antaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonnu Daughter Avutham Sister Brother Antaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andharithonu Bandhalenno Kalupukupothuntaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharithonu Bandhalenno Kalupukupothuntaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Appullo Muniguntaam Ambani Kalakantaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appullo Muniguntaam Ambani Kalakantaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chillara Kosam Enno Enno Veshale Vestham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chillara Kosam Enno Enno Veshale Vestham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Laifoka Natakamele Mana Actingulu Ayipothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Laifoka Natakamele Mana Actingulu Ayipothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Ooru Peru Makeup Theesi Chekkeyyaalanthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Ooru Peru Makeup Theesi Chekkeyyaalanthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Shivudagne Lekunda Seemaina Kuduthunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Shivudagne Lekunda Seemaina Kuduthunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Antu Mahabaga Edhantham Sebuthaamanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antu Mahabaga Edhantham Sebuthaamanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Ichhina Aa Saame Saavunu Gift Ivvamga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Ichhina Aa Saame Saavunu Gift Ivvamga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayayo Vaddantaavendhayo Sithramga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayayo Vaddantaavendhayo Sithramga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jejjenaka Jejjenakaa Thodunta Nee Enaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jejjenaka Jejjenakaa Thodunta Nee Enaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Puvvullona Mosukelli Poodchesthaa Padha Kodaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvvullona Mosukelli Poodchesthaa Padha Kodaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Jejjenaka Jejjenakaa Thodunta Nee Enaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jejjenaka Jejjenakaa Thodunta Nee Enaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Puvvullona Mosukelli Poodchesthaa Padha Kodaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvvullona Mosukelli Poodchesthaa Padha Kodaka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Name Iju Raju Basti Bala Raju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Name Iju Raju Basti Bala Raju"/>
</div>
<div class="lyrico-lyrics-wrapper">Chavu Kaburu Challaga Chebutha Prathiroju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chavu Kaburu Challaga Chebutha Prathiroju"/>
</div>
</pre>
