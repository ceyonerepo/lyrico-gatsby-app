---
title: "mavale mavale song lyrics"
album: "Theerpugal Virkapadum"
artist: "Prasad SN"
lyricist: "Srikanth Varadan"
director: "Dheran"
path: "/albums/theerpugal-virkapadum-lyrics"
song: "Mavale Mavale"
image: ../../images/albumart/theerpugal-virkapadum.jpg
date: 2021-12-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lPyDTCKI0D0"
type: "happy"
singers:
  - Sean Rolden
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">etharkaga poranthenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="etharkaga poranthenu"/>
</div>
<div class="lyrico-lyrics-wrapper">thelivaaga puriya vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelivaaga puriya vacha"/>
</div>
<div class="lyrico-lyrics-wrapper">enakaana mugavariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakaana mugavariya"/>
</div>
<div class="lyrico-lyrics-wrapper">adaiyalam theriya vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adaiyalam theriya vacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethamaa eimsa panura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethamaa eimsa panura"/>
</div>
<div class="lyrico-lyrics-wrapper">tharama thalaatura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharama thalaatura"/>
</div>
<div class="lyrico-lyrics-wrapper">arivaa pesi pinnura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arivaa pesi pinnura"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagatha alagakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagatha alagakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mavale mavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mavale mavale"/>
</div>
<div class="lyrico-lyrics-wrapper">enna petha mavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna petha mavale"/>
</div>
<div class="lyrico-lyrics-wrapper">en seiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en seiyum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">thaiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaiyum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">mavale mavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mavale mavale"/>
</div>
<div class="lyrico-lyrics-wrapper">enna petha mavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna petha mavale"/>
</div>
<div class="lyrico-lyrics-wrapper">en seiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en seiyum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">thaiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaiyum avale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaaye illa kalanga villa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaye illa kalanga villa"/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiya usura nee irukura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiya usura nee irukura"/>
</div>
<div class="lyrico-lyrics-wrapper">inime nee than enaku pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inime nee than enaku pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjukulla vachu thanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjukulla vachu thanguven"/>
</div>
<div class="lyrico-lyrics-wrapper">un kannukulla naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannukulla naan "/>
</div>
<div class="lyrico-lyrics-wrapper">enna pathen en kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pathen en kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">anukulla unna paathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anukulla unna paathen"/>
</div>
<div class="lyrico-lyrics-wrapper">manna vittu nan ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manna vittu nan ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nilala kooda nippen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilala kooda nippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mavale mavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mavale mavale"/>
</div>
<div class="lyrico-lyrics-wrapper">enna petha mavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna petha mavale"/>
</div>
<div class="lyrico-lyrics-wrapper">en seiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en seiyum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">thaiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaiyum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">mavale mavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mavale mavale"/>
</div>
<div class="lyrico-lyrics-wrapper">enna petha mavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna petha mavale"/>
</div>
<div class="lyrico-lyrics-wrapper">en seiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en seiyum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">thaiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaiyum avale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ethamaa eimsa panura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethamaa eimsa panura"/>
</div>
<div class="lyrico-lyrics-wrapper">tharama thalaatura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharama thalaatura"/>
</div>
<div class="lyrico-lyrics-wrapper">arivaa pesi pinnura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arivaa pesi pinnura"/>
</div>
<div class="lyrico-lyrics-wrapper">ulagatha alagakura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagatha alagakura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en seiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en seiyum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">thaiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaiyum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">en seiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en seiyum avale"/>
</div>
<div class="lyrico-lyrics-wrapper">thaiyum avale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaiyum avale"/>
</div>
</pre>
