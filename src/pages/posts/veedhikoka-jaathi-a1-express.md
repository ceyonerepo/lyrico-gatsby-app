---
title: "veedhikoka jaathi song lyrics"
album: "A1 Express"
artist: "Hiphop Tamizha"
lyricist: "Ramajogayya Shastri"
director: "Dennis Jeevan Kanukolanu"
path: "/albums/a1-express-lyrics"
song: "Veedhikoka jathantu jathiko"
image: ../../images/albumart/a1-express.jpg
date: 2021-03-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Q1XBiKmFuCM"
type: "melody"
singers:
  - Roll Rida
  - Yogi Sekar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Veedhiko jathantu jathiko vidhantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedhiko jathantu jathiko vidhantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidipoyi meerunte balaheenulouthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidipoyi meerunte balaheenulouthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayakunni nenu naku ledhu nyayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayakunni nenu naku ledhu nyayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadukovadame rajakiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadukovadame rajakiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manchodu evadanta cheddodu evadanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchodu evadanta cheddodu evadanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Masthugaa jeethalu vunnodu vunnodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Masthugaa jeethalu vunnodu vunnodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paimetha marigi lanchalu adigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paimetha marigi lanchalu adigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lenonni pidisthe adhi tappu kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lenonni pidisthe adhi tappu kadhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aamyamya gunjesthe adhi tappu kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aamyamya gunjesthe adhi tappu kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu notu tesukoni votu veyyaledha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu notu tesukoni votu veyyaledha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu chesindhenti thappu kadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu chesindhenti thappu kadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhavilo vunna panivanni nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhavilo vunna panivanni nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvochi nannu dabbulivvamante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvochi nannu dabbulivvamante"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenemi cheyanu ednunchi theynu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenemi cheyanu ednunchi theynu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niddhara Niddhara vadhalaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niddhara Niddhara vadhalaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">neeloni tappunu diddharaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeloni tappunu diddharaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodharaa gurivindha nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodharaa gurivindha nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thappu nuvvu Gurthinchalevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thappu nuvvu Gurthinchalevu"/>
</div>
<div class="lyrico-lyrics-wrapper">common man antavu samanyunnantavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="common man antavu samanyunnantavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu thappa lokam anthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu thappa lokam anthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">cheddatavu ne labhaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheddatavu ne labhaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesanni takattu pedathavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesanni takattu pedathavu"/>
</div>
<div class="lyrico-lyrics-wrapper">ekkina kommanu Narukkuntavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ekkina kommanu Narukkuntavu"/>
</div>
<div class="lyrico-lyrics-wrapper">na mantrale veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na mantrale veru"/>
</div>
<div class="lyrico-lyrics-wrapper">na mantrale veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na mantrale veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeru thattukoleru naa pannagamulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeru thattukoleru naa pannagamulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Odipoyedi mee voter le aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odipoyedi mee voter le aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulam matham dhanam vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulam matham dhanam vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">vida dheestha khabr dhar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vida dheestha khabr dhar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa araachakaanikadduledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa araachakaanikadduledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu aapevadu lene ledu saginantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu aapevadu lene ledu saginantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamanthaa ide aata aaduthuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamanthaa ide aata aaduthuntaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Khadhi vesukunna katharnak nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khadhi vesukunna katharnak nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu nammukunte panganamam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu nammukunte panganamam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurchunna kurchiki ney thaali kattestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurchunna kurchiki ney thaali kattestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayidhellak okasari vaddhanna vachestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayidhellak okasari vaddhanna vachestha"/>
</div>
<div class="lyrico-lyrics-wrapper">Maanifesto madatha kajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maanifesto madatha kajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Attaa chupinchi ayipotha rajaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attaa chupinchi ayipotha rajaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa batake poola puja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa batake poola puja"/>
</div>
<div class="lyrico-lyrics-wrapper">Mee thalarathe band baja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mee thalarathe band baja"/>
</div>
<div class="lyrico-lyrics-wrapper">Hakkulni adigesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hakkulni adigesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Badhyathalu marchesi chesevanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badhyathalu marchesi chesevanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Lachachalu dhachesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lachachalu dhachesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pellam pillala machokate lakkesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pellam pillala machokate lakkesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thalli desanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thalli desanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Rupay ki ammesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rupay ki ammesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Arere edhi nee manassakshi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arere edhi nee manassakshi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkadunchavu dhaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkadunchavu dhaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha votunammesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha votunammesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadayipedhi prajalera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadayipedhi prajalera"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhi nee manassakshi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhi nee manassakshi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkadunchavu dhaachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkadunchavu dhaachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha votunammesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha votunammesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadayipedhi prajalera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadayipedhi prajalera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O desha pourudaa, pouruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O desha pourudaa, pouruda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikanaina maarara, aaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikanaina maarara, aaah"/>
</div>
</pre>
