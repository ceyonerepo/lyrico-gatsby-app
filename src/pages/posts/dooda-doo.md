---
title: "dooda song lyrics"
album: "Doo"
artist: "Abhishek — Lawrence"
lyricist: "G. Kumar"
director: "Sriram Padmanabhan"
path: "/albums/doo-lyrics"
song: "Dooda"
image: ../../images/albumart/doo.jpg
date: 2011-08-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/l7WWw5odKHI"
type: "happy"
singers:
  - T. Rajendar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hei thamma vittudu machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei thamma vittudu machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">sarakka vittudu machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarakka vittudu machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">rendum unakku mudiyalannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendum unakku mudiyalannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee Lova-a vittudu machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee Lova-a vittudu machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">vittukkodutha kaadhal ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittukkodutha kaadhal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">rumba super machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rumba super machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">vittum vidaadha kaadhal ippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittum vidaadha kaadhal ippa"/>
</div>
<div class="lyrico-lyrics-wrapper">annam metter machaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annam metter machaan"/>
</div>
<div class="lyrico-lyrics-wrapper">hei sarakku vaangi kodukkalanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hei sarakku vaangi kodukkalanna"/>
</div>
<div class="lyrico-lyrics-wrapper">ada nanbankooda doo da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada nanbankooda doo da"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure-u callu pannumboadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure-u callu pannumboadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">un bettari Low daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un bettari Low daa"/>
</div>
<div class="lyrico-lyrics-wrapper">indha vivaram therinjaa Figare-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha vivaram therinjaa Figare-u"/>
</div>
<div class="lyrico-lyrics-wrapper">viduppa unakkoru doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viduppa unakkoru doo daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unakku doo daa unakku doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku doo daa unakku doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure poanaa vidudaa doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure poanaa vidudaa doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku doo daa unakku doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku doo daa unakku doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">engalukku ellaamey doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalukku ellaamey doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku doo daa unakku doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku doo daa unakku doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">indha pigarunga poavaa soodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha pigarunga poavaa soodaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku doo daa unakku doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku doo daa unakku doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">ava poanaa poaraa poadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava poanaa poaraa poadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal thevai vilambarathai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal thevai vilambarathai "/>
</div>
<div class="lyrico-lyrics-wrapper">kannukkulley paartheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannukkulley paartheney"/>
</div>
<div class="lyrico-lyrics-wrapper">thevadhaiyaa avala thookkum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevadhaiyaa avala thookkum "/>
</div>
<div class="lyrico-lyrics-wrapper">theraai naanum vandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theraai naanum vandheney"/>
</div>
<div class="lyrico-lyrics-wrapper">aalapaarthava aazham paarthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalapaarthava aazham paarthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">boadhaikkulla vizhundheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boadhaikkulla vizhundheney"/>
</div>
<div class="lyrico-lyrics-wrapper">thevaiyilla thevaiyilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevaiyilla thevaiyilla "/>
</div>
<div class="lyrico-lyrics-wrapper">thookkupoada poaneney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookkupoada poaneney"/>
</div>
<div class="lyrico-lyrics-wrapper">Orkut-la Request poattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orkut-la Request poattu"/>
</div>
<div class="lyrico-lyrics-wrapper">avala pudicheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avala pudicheney"/>
</div>
<div class="lyrico-lyrics-wrapper">Face booke-il Face-a paarthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Face booke-il Face-a paarthu "/>
</div>
<div class="lyrico-lyrics-wrapper">odambu ilaicheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odambu ilaicheney"/>
</div>
<div class="lyrico-lyrics-wrapper">avalin kaadhalai kaninikkulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avalin kaadhalai kaninikkulla "/>
</div>
<div class="lyrico-lyrics-wrapper">nozhanji paartheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nozhanji paartheney"/>
</div>
<div class="lyrico-lyrics-wrapper">Password kaettava passaagi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Password kaettava passaagi "/>
</div>
<div class="lyrico-lyrics-wrapper">poannaa peesaaneney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poannaa peesaaneney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unakku doo daa unakku doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku doo daa unakku doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure poanaa vidudaa doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure poanaa vidudaa doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku doo daa unakku doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku doo daa unakku doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">engalukku ellaamey doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalukku ellaamey doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku doo daa unakku doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku doo daa unakku doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">indha pigarunga poavaa soodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha pigarunga poavaa soodaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku doo daa unakku doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku doo daa unakku doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">ava poanaa poaraa poadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava poanaa poaraa poadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei koadai kaala koluthum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei koadai kaala koluthum "/>
</div>
<div class="lyrico-lyrics-wrapper">veyilil poarvaiyaala purinjikkitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyilil poarvaiyaala purinjikkitta"/>
</div>
<div class="lyrico-lyrics-wrapper">kottum mazhaiyil nanaiya cholli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kottum mazhaiyil nanaiya cholli"/>
</div>
<div class="lyrico-lyrics-wrapper">kodaiyai eduththu pudichipputtaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodaiyai eduththu pudichipputtaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aala maathi aala maathum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala maathi aala maathum "/>
</div>
<div class="lyrico-lyrics-wrapper">pengal pazhakkam vendaamey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pengal pazhakkam vendaamey"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanjippoana ilaiya poala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanjippoana ilaiya poala"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathula naan karainjeney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathula naan karainjeney"/>
</div>
<div class="lyrico-lyrics-wrapper">ada nanbaa ini kaadhal vendaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada nanbaa ini kaadhal vendaam"/>
</div>
<div class="lyrico-lyrics-wrapper">manasu thaangalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasu thaangalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal oru vayirukku kedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal oru vayirukku kedu"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkum theriyalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkum theriyalaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">koadu poattu Roadum poattoru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koadu poattu Roadum poattoru"/>
</div>
<div class="lyrico-lyrics-wrapper">koadu poattaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koadu poattaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">ava nenachaa sirippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava nenachaa sirippa"/>
</div>
<div class="lyrico-lyrics-wrapper">nenacha moraippaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenacha moraippaa"/>
</div>
<div class="lyrico-lyrics-wrapper">loosaaneney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="loosaaneney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unakku doo daa unakku doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku doo daa unakku doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Figure poanaa vidudaa doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Figure poanaa vidudaa doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku doo daa unakku doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku doo daa unakku doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">engalukku ellaamey doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engalukku ellaamey doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku doo daa unakku doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku doo daa unakku doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">indha pigarunga poavaa soodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha pigarunga poavaa soodaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unakku doo daa unakku doo daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unakku doo daa unakku doo daa"/>
</div>
<div class="lyrico-lyrics-wrapper">ava poanaa poaraa poadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava poanaa poaraa poadaa"/>
</div>
</pre>
