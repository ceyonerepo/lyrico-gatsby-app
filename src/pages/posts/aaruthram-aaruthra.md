---
title: "aaruthram song lyrics"
album: "Aaruthra"
artist: "Vidyasagar"
lyricist: "Pa Vijay"
director: "Pa Vijay"
path: "/albums/aaruthra-lyrics"
song: "Aaruthram"
image: ../../images/albumart/aaruthra.jpg
date: 2018-08-31
lang: tamil
youtubeLink: 
type: "mass"
singers:
  - Karthik
  - Sriram
  - Narayanan Ravishankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulagam ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagam ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">unaai mirattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaai mirattum"/>
</div>
<div class="lyrico-lyrics-wrapper">odi olinthaal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odi olinthaal "/>
</div>
<div class="lyrico-lyrics-wrapper">innum viratum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum viratum"/>
</div>
<div class="lyrico-lyrics-wrapper">poiyum pagaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poiyum pagaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthu thurathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthu thurathum"/>
</div>
<div class="lyrico-lyrics-wrapper">odaathe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odaathe "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">throgam unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="throgam unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">thurathi adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurathi adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">karunai kettal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karunai kettal"/>
</div>
<div class="lyrico-lyrics-wrapper">kaluthai nerikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaluthai nerikum"/>
</div>
<div class="lyrico-lyrics-wrapper">pathari aluthaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathari aluthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">paarthu rasikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarthu rasikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">oliyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oliyaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">puthan veetu bothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthan veetu bothi"/>
</div>
<div class="lyrico-lyrics-wrapper">marathil ratham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marathil ratham "/>
</div>
<div class="lyrico-lyrics-wrapper">thelithu paarkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelithu paarkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ithayathil eeti nulaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayathil eeti nulaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">sugama endru ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugama endru ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">mugathil natpai kati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugathil natpai kati"/>
</div>
<div class="lyrico-lyrics-wrapper">konde muthugil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konde muthugil "/>
</div>
<div class="lyrico-lyrics-wrapper">kuthi kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthi kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">satru neram kan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satru neram kan "/>
</div>
<div class="lyrico-lyrics-wrapper">moodinaal samaathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moodinaal samaathi"/>
</div>
<div class="lyrico-lyrics-wrapper">katti sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti sellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aru aru aru verudan aru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aru aru aru verudan aru"/>
</div>
<div class="lyrico-lyrics-wrapper">iru iru iru vetkaiyil iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru iru iru vetkaiyil iru"/>
</div>
<div class="lyrico-lyrics-wrapper">thutchanai maru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thutchanai maru"/>
</div>
<div class="lyrico-lyrics-wrapper">echanin eru agni karu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="echanin eru agni karu"/>
</div>
<div class="lyrico-lyrics-wrapper">rutharin uru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rutharin uru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sivame sivame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sivame sivame"/>
</div>
<div class="lyrico-lyrics-wrapper">neruppai urukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neruppai urukku"/>
</div>
<div class="lyrico-lyrics-wrapper">idiyai malaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idiyai malaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">mothi norukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothi norukku"/>
</div>
<div class="lyrico-lyrics-wrapper">innum vetkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum vetkai"/>
</div>
<div class="lyrico-lyrics-wrapper">micham irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="micham irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">oyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oyaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maname namame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maname namame"/>
</div>
<div class="lyrico-lyrics-wrapper">thiraiyai irakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiraiyai irakku"/>
</div>
<div class="lyrico-lyrics-wrapper">niyaayam neethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niyaayam neethi"/>
</div>
<div class="lyrico-lyrics-wrapper">theruvil kidakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theruvil kidakku"/>
</div>
<div class="lyrico-lyrics-wrapper">tharmam thotraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharmam thotraal"/>
</div>
<div class="lyrico-lyrics-wrapper">keethai etharku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keethai etharku"/>
</div>
<div class="lyrico-lyrics-wrapper">paniyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniyaathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">irakka matra oorukulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irakka matra oorukulle"/>
</div>
<div class="lyrico-lyrics-wrapper">ithayam ullavan aemali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayam ullavan aemali"/>
</div>
<div class="lyrico-lyrics-wrapper">mirugam sutrum katukulle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mirugam sutrum katukulle"/>
</div>
<div class="lyrico-lyrics-wrapper">pookal virpavan komali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookal virpavan komali"/>
</div>
<div class="lyrico-lyrics-wrapper">viluntha pothu vila edupan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viluntha pothu vila edupan"/>
</div>
<div class="lyrico-lyrics-wrapper">throgathirku munnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="throgathirku munnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">thannai nambi yutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannai nambi yutham"/>
</div>
<div class="lyrico-lyrics-wrapper">seibavan thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seibavan thaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">vellum raajaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vellum raajaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aru aru aru verudan aru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aru aru aru verudan aru"/>
</div>
<div class="lyrico-lyrics-wrapper">iru iru iru vetkaiyil iru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru iru iru vetkaiyil iru"/>
</div>
<div class="lyrico-lyrics-wrapper">thutchanai maru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thutchanai maru"/>
</div>
<div class="lyrico-lyrics-wrapper">echanin eru agni karu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="echanin eru agni karu"/>
</div>
<div class="lyrico-lyrics-wrapper">rutharin uru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rutharin uru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram ruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram ruthram"/>
</div>
<div class="lyrico-lyrics-wrapper">aaruthram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaruthram"/>
</div>
</pre>
