---
title: "edho manasuna song lyrics"
album: "O Pitta Katha"
artist: "Pravin Lakkaraju"
lyricist: "Sreejo"
director: "Chendu Muddhu"
path: "/albums/o-pitta-katha-lyrics"
song: "Edho Manasuna"
image: ../../images/albumart/o-pitta-katha.jpg
date: 2020-03-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/mRrbLIeiyHE"
type: "love"
singers:
  - Lipsika Bhasyam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Edho mansuna melikey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho mansuna melikey"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo chilipiga meriseyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo chilipiga meriseyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanne parigedamanttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne parigedamanttu"/>
</div>
<div class="lyrico-lyrics-wrapper">paadam aagiponi ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadam aagiponi ante"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ayyo ayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayyo ayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">jaabilamma nannu choosi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaabilamma nannu choosi "/>
</div>
<div class="lyrico-lyrics-wrapper">veliguthondi ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veliguthondi ga"/>
</div>
<div class="lyrico-lyrics-wrapper">nanne thakey vaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne thakey vaana "/>
</div>
<div class="lyrico-lyrics-wrapper">chinuku muthyam alle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinuku muthyam alle"/>
</div>
<div class="lyrico-lyrics-wrapper">maaruthondilaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaruthondilaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">virinchi neenai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="virinchi neenai"/>
</div>
<div class="lyrico-lyrics-wrapper">rachinchukonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rachinchukonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kallalni nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallalni nene"/>
</div>
<div class="lyrico-lyrics-wrapper">poodotana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poodotana"/>
</div>
<div class="lyrico-lyrics-wrapper">merali nenai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="merali nenai"/>
</div>
<div class="lyrico-lyrics-wrapper">mooresipona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mooresipona"/>
</div>
<div class="lyrico-lyrics-wrapper">nijalu inka anddam ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijalu inka anddam ante"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ayyo ayyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayyo ayyo"/>
</div>
<div class="lyrico-lyrics-wrapper">jaabilamma nannu choosi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaabilamma nannu choosi "/>
</div>
<div class="lyrico-lyrics-wrapper">veliguthondi ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veliguthondi ga"/>
</div>
<div class="lyrico-lyrics-wrapper">nanne thakey vaana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanne thakey vaana "/>
</div>
<div class="lyrico-lyrics-wrapper">chinuku muthyam alle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinuku muthyam alle"/>
</div>
<div class="lyrico-lyrics-wrapper">maaruthondilaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaruthondilaaaa"/>
</div>
</pre>
