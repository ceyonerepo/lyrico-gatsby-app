---
title: "oora onna serndhu song lyrics"
album: "Vandi"
artist: "Sooraj S. Kurup"
lyricist: "Snehan"
director: "Rajeesh Bala"
path: "/albums/vandi-song-lyrics"
song: "Oora Onna Serndhu"
image: ../../images/albumart/vandi.jpg
date: 2018-11-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2F9dElZ5Owg"
type: "happy"
singers:
  - Sruthi Lakshmi
  - Sooraj S Kurup
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oora onnu serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora onnu serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno ivan thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno ivan thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uththu paarkiratho yela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uththu paarkiratho yela"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora onnu serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora onnu serndhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna panna pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna panna pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo bayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oora onnu serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora onnu serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno ivan thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno ivan thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uththu paarkiratho yela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uththu paarkiratho yela"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora onnu serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora onnu serndhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engo oduraanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo oduraanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga oduvaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga oduvaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna theduvaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna theduvaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Vena engo oduraanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vena engo oduraanda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna panna pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna panna pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo bayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga poyi nippa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga poyi nippa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo bayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaigalai marakanuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigalai marakanuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakadidaa sarakadidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakadidaa sarakadidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakadidaa sarakadidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakadidaa sarakadidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakadichaa avan parakkalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakadichaa avan parakkalana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakadidaa sarakadidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakadidaa sarakadidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakadidaa sarakadidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakadidaa sarakadidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oothu oothu fulla oothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothu oothu fulla oothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethu yethu nalla yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethu yethu nalla yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai illa manushan intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai illa manushan intha"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyila yaar irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyila yaar irukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maathu maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathu maathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothu maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothu maathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu mutta bodhai yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu mutta bodhai yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai patta ellathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai patta ellathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubavikka kathukadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubavikka kathukadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannamooochi aada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannamooochi aada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi parpom vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi parpom vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Achcham ethirka vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achcham ethirka vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Utcham thoduvom vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utcham thoduvom vaada vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vithiya kadakka vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithiya kadakka vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathiya kadakka vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathiya kadakka vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathiya sumakka vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathiya sumakka vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhiya paadhai vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhiya paadhai vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu vithiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu vithiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Illana sathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illana sathiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamikitta thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamikitta thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu poyi sonniya aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu poyi sonniya aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En manasu mulukka thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasu mulukka thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thaan irukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thaan irukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaiyum sumakkurenae naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaiyum sumakkurenae naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Povum vazhiyellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povum vazhiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhala unakku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhala unakku thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Koda varatumma naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koda varatumma naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaigalai marakanuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigalai marakanuma"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakadidaa sarakadidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakadidaa sarakadidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakadidaa sarakadidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakadidaa sarakadidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakadichaa avan parakkalana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakadichaa avan parakkalana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakadidaa sarakadidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakadidaa sarakadidaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarakadidaa sarakadidaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarakadidaa sarakadidaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oothu oothu fulla oothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothu oothu fulla oothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethu yethu nalla yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethu yethu nalla yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavalai illa manushan intha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai illa manushan intha"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyila yaar irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyila yaar irukka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maathu maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maathu maathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oothu maathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oothu maathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu mutta bodhai yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu mutta bodhai yethu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai patta ellathaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai patta ellathaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubavikka kathukadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubavikka kathukadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaam nadakkum vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam nadakkum vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethaiyum jeyipom vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethaiyum jeyipom vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigal thaandi vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigal thaandi vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thayakkam ethirka vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thayakkam ethirka vaada vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam kadakka vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam kadakka vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnoda vazhiyila vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda vazhiyila vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi irukka vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi irukka vaada vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saami irukka vaada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami irukka vaada vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithu vithiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu vithiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Illana sathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illana sathiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamikitta thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamikitta thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu poyi sonniya aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu poyi sonniya aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oora onnu serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora onnu serndhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeno ivan thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeno ivan thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uththu paarkiratho yela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uththu paarkiratho yela"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora onnu serndhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora onnu serndhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna panna pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna panna pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennennamo bayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennamo bayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennamo bayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora"/>
</div>
<div class="lyrico-lyrics-wrapper">Engo oduraanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo oduraanda"/>
</div>
</pre>