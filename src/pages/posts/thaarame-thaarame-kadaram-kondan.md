---
title: "thaarame thaarame song lyrics"
album: "Kadaram Kondan"
artist: "Ghibran"
lyricist: "Viveka"
director: "Rajesh M. Selva"
path: "/albums/kadaram-kondan-lyrics"
song: "Thaarame Thaarame"
image: ../../images/albumart/kadaram-kondan.jpg
date: 2019-07-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gB1gPmtDohY"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Veredhuvum Thevai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veredhuvum Thevai Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mattum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mattum Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Vaithu Kaathiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Vaithu Kaathiruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennnavaanaaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennnavaanaaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Edhiril Naan Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Edhiril Naan Irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru Naalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru Naalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchi Mudhal Paadham Varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchi Mudhal Paadham Varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesudhu Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesudhu Vaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinamum Aayiram Murai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinamum Aayiram Murai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthu Mudithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthu Mudithaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Paarthida Solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Paarthida Solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Paazhum Manam Yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paazhum Manam Yengum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaramae Thaaramae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaramae Thaaramae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin Vaasamae Vaasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin Vaasamae Vaasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Dhaanae Thaaramae Thaaramae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhaanae Thaaramae Thaaramae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Suvasamae Suvasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Suvasamae Suvasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Uyirae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Uyirae Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Melum Keezhum Aadum Undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melum Keezhum Aadum Undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaya Kannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaya Kannaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaruvedam Poduthu En Naatkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaruvedam Poduthu En Naatkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayul Regai Muzhuvathumaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayul Regai Muzhuvathumaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaeiyum Munnaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaeiyum Munnaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazham Varai Vaazhnthidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazham Varai Vaazhnthidalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalin Ullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalin Ullae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Ulagam Thoolai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Ulagam Thoolai"/>
</div>
<div class="lyrico-lyrics-wrapper">Udaindhu Ponnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udaindhu Ponnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan Oru Thugalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan Oru Thugalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Karai Serpen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Karai Serpen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaramae Thaaramae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaramae Thaaramae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin Vaasamae Vaasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin Vaasamae Vaasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Dhaanae Thaaramae Thaaramae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhaanae Thaaramae Thaaramae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Suvasamae Suvasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Suvasamae Suvasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Uyirae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Uyirae Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Neengidum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Neengidum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum Perum Baaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum Perum Baaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaithodum Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaithodum Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Theemeedhilum Eeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theemeedhilum Eeram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenadakkum Pozhudhu Nizhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenadakkum Pozhudhu Nizhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyil Padaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil Padaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Nizhalai Enadhu Udal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Nizhalai Enadhu Udal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nazhuva Vidaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nazhuva Vidaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perzhagin Melae Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perzhagin Melae Oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thurumbum Thodaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurumbum Thodaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinju Mugam Oru Nodiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinju Mugam Oru Nodiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadakoodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadakoodathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Paarthiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarthiruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal Moodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Moodathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Thaandhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thaandhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum Theriyakoodathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum Theriyakoodathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaaramae Thaaramae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaaramae Thaaramae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhvin Vaasamae Vaasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin Vaasamae Vaasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Dhaanae Thaaramae Thaaramae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhaanae Thaaramae Thaaramae Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Suvasamae Suvasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Suvasamae Suvasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Uyirae Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Uyirae Vaa"/>
</div>
</pre>
