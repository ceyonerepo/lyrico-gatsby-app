---
title: 'po nee po song lyrics'
album: '3'
artist: 'Anirudh Ravichander'
lyricist: 'Dhanush'
director: 'Soundharya Rajinikanth'
path: '/albums/3-song-lyrics'
song: 'Po Nee Po'
image: ../../images/albumart/3.jpg
date: 2012-03-30
lang: tamil
youtubeLink: 'https://www.youtube.com/embed/DnyA_qEbTpw'
type: 'sad'
singers: 
- Mohit Chauhan
- Anirudh Ravichander
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Po nee po
<input type="checkbox" class="lyrico-select-lyric-line" value="Po nee po"/>
</div>
<div class="lyrico-lyrics-wrapper">Po nee po
<input type="checkbox" class="lyrico-select-lyric-line" value="Po nee po"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaaga thavikkindren
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniyaaga thavikkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai vendam anbae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Thunai vendam anbae po"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinamaaga nadakkindren
<input type="checkbox" class="lyrico-select-lyric-line" value="Pinamaaga nadakkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vendaam dhooram po
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyir vendaam dhooram po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thotta idam ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thotta idam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Erigirathu anbae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Erigirathu anbae po"/>
</div>
<div class="lyrico-lyrics-wrapper">Nan poghum nimidanghal
<input type="checkbox" class="lyrico-select-lyric-line" value="Nan poghum nimidanghal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkaaghum anbae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Unkaaghum anbae po"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu vendaam anbae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithu vendaam anbae po"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam thedum pennae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Nijam thedum pennae po"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu vilaiyaada
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirodu vilaiyaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi seidhaai anbae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidhi seidhaai anbae po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oh….
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaniyaaga thavikkindren
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniyaaga thavikkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai vendam anbae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Thunai vendam anbae po"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinamaaga nadakkindren
<input type="checkbox" class="lyrico-select-lyric-line" value="Pinamaaga nadakkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vendaam dhooram po
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyir vendaam dhooram po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Unnaalae uyir vaazhkiren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unnaalae uyir vaazhkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaaga pennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakkaaga pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir kaadhal nee kaattinaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyir kaadhal nee kaattinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maravenae pennae
<input type="checkbox" class="lyrico-select-lyric-line" value="Maravenae pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu varai unnudan vaazhntha en naatkal
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithu varai unnudan vaazhntha en naatkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Marumurai vaazhthida vazhi illaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Marumurai vaazhthida vazhi illaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Irulilai thediya thedalgal ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Irulilai thediya thedalgal ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidiyalai kaanavum vidhi illaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidiyalai kaanavum vidhi illaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oh….
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Podi po podi poo
<input type="checkbox" class="lyrico-select-lyric-line" value="Podi po podi poo"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal puriyalaya
<input type="checkbox" class="lyrico-select-lyric-line" value="En kaadhal puriyalaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nashtam anbae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Un nashtam anbae po"/>
</div>
<div class="lyrico-lyrics-wrapper">En kanavu kalainthaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="En kanavu kalainthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee irunthaai anbae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee irunthaai anbae po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thotta idam ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thotta idam ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Erigirathu anbae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Erigirathu anbae po"/>
</div>
<div class="lyrico-lyrics-wrapper">Nan poghum nimidanghal
<input type="checkbox" class="lyrico-select-lyric-line" value="Nan poghum nimidanghal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkaaghum anbae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Unkaaghum anbae po"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu vendaam anbae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Ithu vendaam anbae po"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam thedum pennae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Nijam thedum pennae po"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu vilaiyaada
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirodu vilaiyaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhi seidhaai anbae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Vidhi seidhaai anbae po"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oh..
<input type="checkbox" class="lyrico-select-lyric-line" value="Oh oh.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thaniyaaga thavikkindren
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaniyaaga thavikkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai vendam anbae po
<input type="checkbox" class="lyrico-select-lyric-line" value="Thunai vendam anbae po"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinamaaga nadakkindren
<input type="checkbox" class="lyrico-select-lyric-line" value="Pinamaaga nadakkindren"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir vendaam dhooram po
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyir vendaam dhooram po"/>
</div>
</pre>