---
title: "kadhal nilaithane song lyrics"
album: "Nagesh Thiraiyarangam"
artist: "Srikanth Deva"
lyricist: "Umadevi"
director: "Mohamad Issack"
path: "/albums/nagesh-thiraiyarangam-lyrics"
song: "Kadhal Nilaithane"
image: ../../images/albumart/nagesh-thiraiyarangam.jpg
date: 2018-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/m8o4rk1v51Q"
type: "love"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kadhal nilai thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal nilai thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir karaithidum kalai thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir karaithidum kalai thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhkai unai sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhkai unai sera"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhi sollave vandhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhi sollave vandhene"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam thirakkamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam thirakkamale"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivu nuzhaithoduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivu nuzhaithoduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam thirakkamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam thirakkamale"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaivu nuzhaithoduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaivu nuzhaithoduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">semmanthane naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="semmanthane naan"/>
</div>
<div class="lyrico-lyrics-wrapper">niranvadaithen unnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niranvadaithen unnal"/>
</div>
<div class="lyrico-lyrics-wrapper">naan niranvadaithen unnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan niranvadaithen unnal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhal nilai thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal nilai thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir karaithidum kalai thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir karaithidum kalai thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhkai unai sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhkai unai sera"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhi sollave vandhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhi sollave vandhene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pogum pathai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum pathai "/>
</div>
<div class="lyrico-lyrics-wrapper">puthithaaga maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthithaaga maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">pookal yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookal yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">puthu kaambil serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puthu kaambil serum"/>
</div>
<div class="lyrico-lyrics-wrapper">intha paavaiyin thevaikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha paavaiyin thevaikal"/>
</div>
<div class="lyrico-lyrics-wrapper">theerume udal maariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theerume udal maariye"/>
</div>
<div class="lyrico-lyrics-wrapper">jeevanum odume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jeevanum odume"/>
</div>
<div class="lyrico-lyrics-wrapper">modukkaatinil mootiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="modukkaatinil mootiye"/>
</div>
<div class="lyrico-lyrics-wrapper">theeyai pol nam kadhalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theeyai pol nam kadhalum"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhvinil neelume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhvinil neelume"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhalai vazhalam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhalai vazhalam vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vaanil vaazhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanil vaazhum"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai megam aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai megam aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">bhoomi paainthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhoomi paainthu "/>
</div>
<div class="lyrico-lyrics-wrapper">nilai maari pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilai maari pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">intha thaamarai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha thaamarai "/>
</div>
<div class="lyrico-lyrics-wrapper">thagangal theeruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagangal theeruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">antha suriyan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha suriyan "/>
</div>
<div class="lyrico-lyrics-wrapper">megamaay maruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megamaay maruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">manam kadhalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam kadhalin"/>
</div>
<div class="lyrico-lyrics-wrapper">aazhathil moozhguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aazhathil moozhguthe"/>
</div>
<div class="lyrico-lyrics-wrapper">nam kadhalum vazhvinil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam kadhalum vazhvinil"/>
</div>
<div class="lyrico-lyrics-wrapper">neelume kadhalaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neelume kadhalaai "/>
</div>
<div class="lyrico-lyrics-wrapper">vazhalam vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhalam vaa"/>
</div>
</pre>
