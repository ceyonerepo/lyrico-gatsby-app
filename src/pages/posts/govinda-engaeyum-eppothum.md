---
title: "govinda song lyrics"
album: "Engaeyum Eppothum"
artist: "C Sathya"
lyricist: "Na Muthukumar"
director: "M. Saravanan"
path: "/albums/engaeyum-eppothum-lyrics"
song: "Govinda"
image: ../../images/albumart/engaeyum-eppothum.jpg
date: 2011-09-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/BBv5jWXQyYA"
type: "happy"
singers:
  - Vijay Prakash
  - Ranina Reddy
  - Boni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennaiyila pudhu ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennaiyila pudhu ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkira morakkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkira morakkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiraththil iva onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiraththil iva onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethukku vanthaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukku vanthaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Imsa thanthaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imsa thanthaalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennaiyila pudhu ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennaiyila pudhu ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkira morakkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkira morakkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiraththil iva onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiraththil iva onnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daddy mummy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daddy mummy"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivalukku vechaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalukku vechaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada enna ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada enna ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodachchalunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodachchalunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pera veppenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pera veppenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
.
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambikka illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambikka illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooda vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottikittaa tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottikittaa tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhatti vidavum manasae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhatti vidavum manasae illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna kodumaiyadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna kodumaiyadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanju pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanju pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Mulagaa ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mulagaa ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotti kidakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotti kidakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhaiyapola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhaiyapola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaramaaga vedichaa ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaramaaga vedichaa ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Paava nelamayada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paava nelamayada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagaayam melae thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagaayam melae thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaana megangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaana megangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaanthu paarkka neramindri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaanthu paarkka neramindri"/>
</div>
<div class="lyrico-lyrics-wrapper">Povathu engaeyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povathu engaeyoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veiyilodu mazhaiyum thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veiyilodu mazhaiyum thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru sernthu vanthathu pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru sernthu vanthathu pol"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha konja nera payanam sendruMudivathu engaeyoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha konja nera payanam sendruMudivathu engaeyoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adadada daddy mummy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adadada daddy mummy"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanukku vechaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanukku vechaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">suththamda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suththamda"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna enna enna ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna enna enna ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumathaanginnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumathaanginnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pera veppenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pera veppenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
.
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kappal vaanga vanthiruppaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kappal vaanga vanthiruppaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chappal vaanga vanthiruppaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chappal vaanga vanthiruppaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Usura vaanga vanthiruppaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura vaanga vanthiruppaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnum puriyalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum puriyalayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Trailer pola mudinthiduvaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trailer pola mudinthiduvaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Train-a pola neendiduvaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Train-a pola neendiduvaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppa ivana iva viduvaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppa ivana iva viduvaalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnum theriyalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum theriyalayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appaavi polaththaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appaavi polaththaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappaaga nenachenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappaaga nenachenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ainooru kelvi kettu kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ainooru kelvi kettu kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala kolraalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aala kolraalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iva iva vantha pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva iva vantha pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha kobam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha kobam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo illaiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo illaiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Iva serthu vaitha sandhegangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iva serthu vaitha sandhegangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Do not do some times times
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do not do some times times"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sagarisa sa sa sa ri ga ma ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagarisa sa sa sa ri ga ma ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Sagarisa sa sa sa ri ga ma ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sagarisa sa sa sa ri ga ma ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Govinda govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennaiyila pudhu ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennaiyila pudhu ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikkira morakkira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikkira morakkira"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiraththil iva onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiraththil iva onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohooo oooo ohoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo oooo ohoooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa nana naa naa nana naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa nana naa naa nana naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana nana naaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana nana naaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Govinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Govinda"/>
</div>
</pre>
