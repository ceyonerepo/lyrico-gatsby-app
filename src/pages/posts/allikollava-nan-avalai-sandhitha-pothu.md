---
title: "allikollava song lyrics"
album: "Nan Avalai Sandhitha Pothu"
artist: "Hithesh Murugavel- Jai"
lyricist: "L G Ravichnadar"
director: "L G Ravichnadar"
path: "/albums/nan-avalai-sandhitha-pothu-lyrics"
song: "Allikollava"
image: ../../images/albumart/nan-avalai-sandhitha-pothu.jpg
date: 2019-12-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_S4BnaY_ug8"
type: "love"
singers:
  - Pawan
  - Saritha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">allikollava allikollava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="allikollava allikollava"/>
</div>
<div class="lyrico-lyrics-wrapper">padukaiyil unnidam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padukaiyil unnidam "/>
</div>
<div class="lyrico-lyrics-wrapper">nan palli kollava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan palli kollava"/>
</div>
<div class="lyrico-lyrics-wrapper">allikollava allikollava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="allikollava allikollava"/>
</div>
<div class="lyrico-lyrics-wrapper">padukaiyil unnidam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padukaiyil unnidam "/>
</div>
<div class="lyrico-lyrics-wrapper">nan palli kollava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan palli kollava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thalli nikiren thathalikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli nikiren thathalikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli poga nanum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli poga nanum "/>
</div>
<div class="lyrico-lyrics-wrapper">ipo thathalikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo thathalikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">thegam vervaiyil kulikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thegam vervaiyil kulikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kama parvaiyil kalikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kama parvaiyil kalikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">kanigal irupathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanigal irupathu "/>
</div>
<div class="lyrico-lyrics-wrapper">paritha than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paritha than"/>
</div>
<div class="lyrico-lyrics-wrapper">iraivan padaithathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iraivan padaithathu "/>
</div>
<div class="lyrico-lyrics-wrapper">pusithida than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pusithida than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanathathai nee kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanathathai nee kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">pennin manam thalladuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pennin manam thalladuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">mel vanathai nee parkave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mel vanathai nee parkave"/>
</div>
<div class="lyrico-lyrics-wrapper">boologathil enai ketkirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boologathil enai ketkirai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">allikollava allikollava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="allikollava allikollava"/>
</div>
<div class="lyrico-lyrics-wrapper">padukaiyil unnidam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padukaiyil unnidam "/>
</div>
<div class="lyrico-lyrics-wrapper">nan palli kollava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan palli kollava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">therai unnai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="therai unnai "/>
</div>
<div class="lyrico-lyrics-wrapper">adiayave thudikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiayave thudikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">bothai konda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothai konda "/>
</div>
<div class="lyrico-lyrics-wrapper">viliyinil moolginen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viliyinil moolginen"/>
</div>
<div class="lyrico-lyrics-wrapper">aadai kulle aadai undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadai kulle aadai undu"/>
</div>
<div class="lyrico-lyrics-wrapper">kandu konden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandu konden"/>
</div>
<div class="lyrico-lyrics-wrapper">aan maganai koram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aan maganai koram"/>
</div>
<div class="lyrico-lyrics-wrapper">kondu yengi nindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondu yengi nindren"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthu vida ennidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthu vida ennidam"/>
</div>
<div class="lyrico-lyrics-wrapper">onnum illai enkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnum illai enkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">ennul ethum ullatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennul ethum ullatha"/>
</div>
<div class="lyrico-lyrics-wrapper">unnidam than ketkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnidam than ketkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thagam ulla aanidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thagam ulla aanidam"/>
</div>
<div class="lyrico-lyrics-wrapper">thanni thara yosana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanni thara yosana"/>
</div>
<div class="lyrico-lyrics-wrapper">mogam vanthu ennidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mogam vanthu ennidam"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli ponal vethanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli ponal vethanai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai konda kalvane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai konda kalvane"/>
</div>
<div class="lyrico-lyrics-wrapper">kannai moodi kondene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannai moodi kondene"/>
</div>
<div class="lyrico-lyrics-wrapper">vinnai partha en kannam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinnai partha en kannam"/>
</div>
<div class="lyrico-lyrics-wrapper">mannai parka vaithaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannai parka vaithaye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bothai ennai thalluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothai ennai thalluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">pethai unnai ketkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pethai unnai ketkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">pathai mari pogave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathai mari pogave"/>
</div>
<div class="lyrico-lyrics-wrapper">vaadai katru veesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaadai katru veesuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">allikollava vaa vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="allikollava vaa vaa vaa"/>
</div>
</pre>
