---
title: "naa manase song lyrics"
album: "Mr And Miss"
artist: "Yashwanth Nag"
lyricist: "Pavan Rachepalli"
director: "T. Ashok Kumar Reddy"
path: "/albums/mr-and-miss-lyrics"
song: "Naa Manase Needhi"
image: ../../images/albumart/mr-and-miss.jpg
date: 2021-01-29
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Siogsm3cYds"
type: "love"
singers:
  - Yashwanth Nag
  - Kamala Manohari
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neeve Naalo Nene Neelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeve Naalo Nene Neelo"/>
</div>
<div class="lyrico-lyrics-wrapper">Elaa Maarindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaa Maarindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Gundello Prema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Gundello Prema"/>
</div>
<div class="lyrico-lyrics-wrapper">Thelisinaa Thananu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thelisinaa Thananu "/>
</div>
<div class="lyrico-lyrics-wrapper">Viduvanaa Badhuladaganaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viduvanaa Badhuladaganaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhina Dhaagindhi Preme Gaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhina Dhaagindhi Preme Gaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Manase Needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manase Needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manase Needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manase Needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manase Needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manase Needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manase Needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manase Needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallee Raanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallee Raanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Raanaa Mallee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raanaa Mallee"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Cheraalani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Cheraalani"/>
</div>
<div class="lyrico-lyrics-wrapper">Antondhi Praanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antondhi Praanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Maayalo Alaku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Maayalo Alaku "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhorikina Theeraanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhorikina Theeraanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Haayilo Nidhura 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Haayilo Nidhura "/>
</div>
<div class="lyrico-lyrics-wrapper">Marachina Pasivaadani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marachina Pasivaadani"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Velanu Vadhili 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Velanu Vadhili "/>
</div>
<div class="lyrico-lyrics-wrapper">Eruganu Nee Dhyaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eruganu Nee Dhyaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Janmaku Bathaka 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Janmaku Bathaka "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhorikina Nee Snehamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhorikina Nee Snehamu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Vedhana Cheliki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Vedhana Cheliki "/>
</div>
<div class="lyrico-lyrics-wrapper">Runapadipoyaananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Runapadipoyaananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thane Lekundaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thane Lekundaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ne Nem Chesdhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne Nem Chesdhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Manase Needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manase Needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manase Needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manase Needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manase Needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manase Needhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Manase Needhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Manase Needhi"/>
</div>
</pre>
