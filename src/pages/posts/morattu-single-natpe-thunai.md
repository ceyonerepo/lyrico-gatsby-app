---
title: "morattu single song lyrics"
album: "Natpe Thunai"
artist: "Hiphop Tamizha"
lyricist: "Hiphop Tamizha"
director: "D. Parthiban Desingu"
path: "/albums/natpe-thunai-lyrics"
song: "Morattu Single"
image: ../../images/albumart/natpe-thunai.jpg
date: 2019-04-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SKmEwaTYkME"
type: "love"
singers:
  - Sathyaprakash
  - Hiphop Tamizha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Malaiyali Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyali Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolaigara Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaigara Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjai Enthan Nenjai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjai Enthan Nenjai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkuvathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkuvathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalil Kathakkali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalil Kathakkali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kerala Paingili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kerala Paingili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjai Kollaiyittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjai Kollaiyittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaththuvathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaththuvathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morattu Single Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu Single Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruntha Ennaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruntha Ennaththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Meratti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Meratti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Merala Vechchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merala Vechchaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morattu Single Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu Single Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruntha Ennaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruntha Ennaththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Meratti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Meratti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Merala Vechchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merala Vechchaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saalai oram Ulla Chinna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai oram Ulla Chinna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulainthigal Idam Summa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulainthigal Idam Summa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjuvathu Pol Nadikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjuvathu Pol Nadikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagi Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagi Alla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paithiyam Pol Naduchatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paithiyam Pol Naduchatha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cute Nu Nenachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cute Nu Nenachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Internetil Kaduppethum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Internetil Kaduppethum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumari Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumari Alla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Sirippula Thimironnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Sirippula Thimironnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkuthu Irukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkuthu Irukkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Pakkam Enna Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pakkam Enna Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilukkuthu Ilukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilukkuthu Ilukkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah I like Her Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah I like Her Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkulla Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaththa Na Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaththa Na Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Enna Mella Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Enna Mella Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Sirippula Thimironnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Sirippula Thimironnu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukkuthu Irukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkuthu Irukkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Pakkam Enna Katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Pakkam Enna Katti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ilukkuthu Ilukkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilukkuthu Ilukkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah I like Her Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah I like Her Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkulla Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaththa Na Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaththa Na Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Enna Mella Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Enna Mella Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ahhh Enna Puduchurukkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahhh Enna Puduchurukkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puduchuruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puduchuruntha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam Siruchuruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Siruchuruma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Simittum Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Simittum Azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavithai Allavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithai Allavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thendral Pola Punnagaiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral Pola Punnagaiyaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal Vanthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Vanthatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Kangalukkul Melody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kangalukkul Melody"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Paththu Nee Kannadichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paththu Nee Kannadichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirukkum Sirippil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirukkum Sirippil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sethari Sarinchu Therikkurendo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethari Sarinchu Therikkurendo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morattu Single Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu Single Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruntha Ennaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruntha Ennaththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Meratti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Meratti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Merala Vechchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merala Vechchaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malaiyali Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malaiyali Penne"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolaigara Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolaigara Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjai Enthan Nenjai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjai Enthan Nenjai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayakkuvathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayakkuvathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Morattu Single Ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Morattu Single Ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iruntha Ennaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruntha Ennaththa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Meratti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Meratti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Merala Vechchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merala Vechchaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangalil Kathakkali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalil Kathakkali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kerala Paingili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kerala Paingili"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenjai Kollaiyittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjai Kollaiyittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaththuvathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaththuvathen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah I like Her Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah I like Her Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkulla Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaththa Na Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaththa Na Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Enna Mella Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Enna Mella Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah I like Her Kangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah I like Her Kangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukkulla Kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukkulla Kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaththa Na Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaththa Na Solla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava Enna Mella Kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava Enna Mella Kolla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kan Simittum Azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Simittum Azhagu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavithai Allavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithai Allavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thendral Pola Punnagaiyaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral Pola Punnagaiyaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyal Vanthatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyal Vanthatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Kangalukkul Melody
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Kangalukkul Melody"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Paththu Nee Kannadichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paththu Nee Kannadichu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirukkum Sirippil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirukkum Sirippil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sethari Sarinchu Therikkurendo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sethari Sarinchu Therikkurendo"/>
</div>
</pre>
