---
title: "yekka yekka yekka song lyrics"
album: "Nadodigal"
artist: "Sundar C Babu"
lyricist: "Na. Muthukumar"
director: "Samuthrakani"
path: "/albums/nadodigal-lyrics"
song: "Yekka Yekka Yekka"
image: ../../images/albumart/nadodigal.jpg
date: 2009-06-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nLB7aq9Cr-A"
type: "Item Song"
singers:
  - Senthildass
  - Chandran
  - M. Sasikumar
  - M.M. Sreelekha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yakkaa Yakkaa Yakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yakkaa Yakkaa Yakkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yakkaa Yakkaa Yakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yakkaa Yakkaa Yakkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellaarumme Nalla Pasanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaarumme Nalla Pasanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kedukkaadhakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedukkaadhakkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnaala Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnaala Munnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Izhukkaadhakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Izhukkaadhakkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mundhanaiyil Sillaraiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhanaiyil Sillaraiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudikkaadhakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudikkaadhakkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koottatha Yerakkattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottatha Yerakkattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koochaththa Orangattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koochaththa Orangattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motchaththil Yerikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motchaththil Yerikittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenju Thulludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenju Thulludhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Paarthukkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Paarthukkittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyaala Korthukkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyaala Korthukkittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanne Vaa Koothukkattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Vaa Koothukkattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho Pannudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho Pannudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yakkaa Yakko Yakkaa Yakko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yakkaa Yakko Yakkaa Yakko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yakkaa Yakkaa Yakkaa Yakkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yakkaa Yakkaa Yakkaa Yakkaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaram Kuraiyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaram Kuraiyaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Biriyaani Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biriyaani Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baaram Kuraiyaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram Kuraiyaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadaiyaani Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaiyaani Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoondi Murukkeaththum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoondi Murukkeaththum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thirugaani Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirugaani Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Muzhusaaga Alli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Muzhusaaga Alli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellamme Chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamme Chellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellamme Chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamme Chellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattavamdi Ennaikkumme
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattavamdi Ennaikkumme"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadal Thaandaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadal Thaandaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangamme Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamme Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangamme Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamme Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kettakkettappaiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kettakkettappaiyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Kittappogaadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kittappogaadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mainaraa Meajaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainaraa Meajaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayasa Sollu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasa Sollu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mudinjadha Kodukkurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudinjadha Kodukkurom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasa Sollu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasa Sollu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Edukkavaa Kudukkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Edukkavaa Kudukkavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edatha Chollu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edatha Chollu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eduththadhai Kodukkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduththadhai Kodukkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eduthu Chollu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduthu Chollu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koottatha Yerakkattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottatha Yerakkattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koochaththa Orangattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koochaththa Orangattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motchaththil Yerikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motchaththil Yerikkittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenju Thulluthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenju Thulluthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Paarthukkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Paarthukkittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyaala Korthukkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyaala Korthukkittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanne Vaa Koothukkattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Vaa Koothukkattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho Pannuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho Pannuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Nenja Noolaakki Thiriyaakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Nenja Noolaakki Thiriyaakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naadi Narambellaam Veriyaakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadi Narambellaam Veriyaakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Kallaakki Kurippaakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kallaakki Kurippaakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum Ennenna Ethirpaakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Ennenna Ethirpaakkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellamme Chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamme Chellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chellamme Chellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellamme Chellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Chinna Chinna Sillaringa Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Chinna Sillaringa Inge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Selladhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selladhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangamme Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamme Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangamme Thangam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangamme Thangam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sillaraiya Pola Nottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sillaraiya Pola Nottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saththam Podathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththam Podathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakku Nee Onakku Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakku Nee Onakku Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanakku Edhukkudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakku Edhukkudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avasiyam Irukkudhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avasiyam Irukkudhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anaichi Irukkudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaichi Irukkudaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidiyira Varaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidiyira Varaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velakka Anaikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velakka Anaikkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varavu Nee Selavu Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varavu Nee Selavu Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanakka Mudikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakka Mudikkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koottatha Yerakkattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottatha Yerakkattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koochaththa Orangattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koochaththa Orangattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motchaththil Yerikkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motchaththil Yerikkittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Nenju Thulluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenju Thulluthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaala Paarthukkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala Paarthukkittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiyaala Korthukkittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyaala Korthukkittu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanne Vaa Koothukkattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanne Vaa Koothukkattu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho Pannudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho Pannudhe"/>
</div>
</pre>
