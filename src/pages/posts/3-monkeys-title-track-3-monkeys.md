---
title: "3 monkeys title track song lyrics"
album: "3 Monkeys"
artist: "Anil kumar G"
lyricist: "ShreeMani"
director: "Anil Kumar G"
path: "/albums/3-monkeys-lyrics"
song: "3 Monkeys Title Track - Spiderman"
image: ../../images/albumart/3-monkeys.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/PU3k9a5ize4"
type: "title track"
singers:
  - Ramya NSK
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Spiderman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Spiderman"/>
</div>
<div class="lyrico-lyrics-wrapper">Superman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superman"/>
</div>
<div class="lyrico-lyrics-wrapper">Ironman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ironman"/>
</div>
<div class="lyrico-lyrics-wrapper">Soooo Sorry
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soooo Sorry"/>
</div>
<div class="lyrico-lyrics-wrapper">Mimmalne Thaladanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mimmalne Thaladanne"/>
</div>
3 <div class="lyrico-lyrics-wrapper">Monkeys Vacchare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monkeys Vacchare"/>
</div>
<div class="lyrico-lyrics-wrapper">Mister Been Ki Dupelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mister Been Ki Dupelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaplin Kanna Chaakulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaplin Kanna Chaakulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pikacho Kanna Keechulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pikacho Kanna Keechulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meekanna Lazy Crazy Kothulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meekanna Lazy Crazy Kothulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Too too too hehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too too too hehe"/>
</div>
<div class="lyrico-lyrics-wrapper">Too too too Hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too too too Hoho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Too too too hehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too too too hehe"/>
</div>
<div class="lyrico-lyrics-wrapper">Too too too Hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too too too Hoho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Too too too hehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too too too hehe"/>
</div>
<div class="lyrico-lyrics-wrapper">Too too too Hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too too too Hoho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podhunne Lesthavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhunne Lesthavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadabidi Chesthavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadabidi Chesthavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Tho Poteega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Tho Poteega"/>
</div>
<div class="lyrico-lyrics-wrapper">Parugeduhuntavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parugeduhuntavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Life Ante Ento Teliyaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Ante Ento Teliyaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Tikamaka Padathavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tikamaka Padathavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Robola Panichesthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robola Panichesthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Smile ye Marchevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smile ye Marchevu"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvullo Vunndahi Zindagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvullo Vunndahi Zindagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvesthu Podham Mundhuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvesthu Podham Mundhuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalasyam Inka Dheniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalasyam Inka Dheniki"/>
</div>
<div class="lyrico-lyrics-wrapper">E Roje Vacchey Ee Cinimaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="E Roje Vacchey Ee Cinimaki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
3 <div class="lyrico-lyrics-wrapper">Monkeys 3 monkeys 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monkeys 3 monkeys "/>
</div>
3 <div class="lyrico-lyrics-wrapper">Monkeys 3 monkeys
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monkeys 3 monkeys"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Too too too hehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too too too hehe"/>
</div>
<div class="lyrico-lyrics-wrapper">Too too too Hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too too too Hoho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Too too too hehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too too too hehe"/>
</div>
<div class="lyrico-lyrics-wrapper">Too too too Hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too too too Hoho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Too too too hehe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too too too hehe"/>
</div>
<div class="lyrico-lyrics-wrapper">Too too too Hoho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Too too too Hoho"/>
</div>
</pre>
