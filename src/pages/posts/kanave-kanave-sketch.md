---
title: "kanave kanave song lyrics"
album: "Sketch"
artist: "S. Thaman"
lyricist: "Vijay Chandar"
director: "Vijay Chandar"
path: "/albums/sketch-lyrics"
song: "Kanave Kanave"
image: ../../images/albumart/sketch.jpg
date: 2018-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/JZGJXF3l_x8"
type: "love"
singers:
  - Vikram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kanave Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhikkum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhikkum Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Paravai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Paravai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragai Virithu Parakkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai Virithu Parakkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniye Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniye Thaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaigirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaigirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaivil Thooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivil Thooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhugirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhugirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Nanaiya Nanaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Nanaiya Nanaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli Vilagi Poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli Vilagi Poguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Thaane Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Thaane Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthe Karaindhene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthe Karaindhene"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkaamal Kaalgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkaamal Kaalgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan Pinne Selgirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan Pinne Selgirathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meithaano Poithaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meithaano Poithaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Naane Kettene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Naane Kettene"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Penne Adi Penne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Penne Adi Penne"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Vasiyam Seithaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Vasiyam Seithaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kannukulla Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannukulla Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Immaikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Immaikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirunthen Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirunthen Na"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjukulla Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukulla Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachu Thaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachu Thaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikki Mukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikki Mukki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindaaaduren Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaaaduren Na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kannukulla Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannukulla Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Immaikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Immaikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirunthen Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirunthen Na"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjukulla Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukulla Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachu Thaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachu Thaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikki Mukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikki Mukki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindaaaduren Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaaaduren Na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podaathe Kaadhal Gate Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaathe Kaadhal Gate Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Weight Aagum Endhan Heartu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weight Aagum Endhan Heartu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cute Ah Nee Paarkkum Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cute Ah Nee Paarkkum Podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaanaa Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaanaa Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Strawberry Kanna Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Strawberry Kanna Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaachu Moochu Kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodaachu Moochu Kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoram Yellow Rice Su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoram Yellow Rice Su"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaama Poche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaama Poche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podaathe Kaadhal Gate Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaathe Kaadhal Gate Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Weight Aagum Endhan Heartu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weight Aagum Endhan Heartu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cute Ah Nee Paarkkum Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cute Ah Nee Paarkkum Podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaanaa Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaanaa Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Strawberry Kanna Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Strawberry Kanna Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaachu Moochu Kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodaachu Moochu Kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoram Yellow Rice Su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoram Yellow Rice Su"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaama Poche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaama Poche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanave Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave Kanave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanave Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhikkum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhikkum Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Paravai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Paravai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragai Virithu Parakkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai Virithu Parakkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaniye Thaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniye Thaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaigirene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaigirene"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholaivil Thooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholaivil Thooral"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhugirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhugirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Nanaiya Nanaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Nanaiya Nanaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondruthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondruthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuli Vilagi Poguthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuli Vilagi Poguthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Swag Swag Swag Swag
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swag Swag Swag Swag"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvaiyaale Vendraval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvaiyaale Vendraval"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarthaiyaale Kondraval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarthaiyaale Kondraval"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyaa Neeyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyaa Neeyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Memaiyaana Pennidam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Memaiyaana Pennidam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanmaiyaaga Minnidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanmaiyaaga Minnidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kannukulla Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannukulla Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Immaikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Immaikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirunthen Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirunthen Na"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjukulla Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukulla Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachu Thaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachu Thaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikki Mukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikki Mukki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindaaaduren Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaaaduren Na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kannukulla Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannukulla Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachathaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachathaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Immaikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Immaikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathirunthen Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathirunthen Na"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjukulla Unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjukulla Unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Vachu Thaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vachu Thaithen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikki Mukki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikki Mukki"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindaaaduren Na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaaaduren Na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podaathe Kaadhal Gate Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaathe Kaadhal Gate Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Weight Aagum Endhan Heartu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weight Aagum Endhan Heartu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cute Ah Nee Paarkkum Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cute Ah Nee Paarkkum Podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaanaa Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaanaa Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Strawberry Kanna Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Strawberry Kanna Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaachu Moochu Kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodaachu Moochu Kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoram Yellow Rice Su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoram Yellow Rice Su"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaama Poche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaama Poche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podaathe Kaadhal Gate Tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podaathe Kaadhal Gate Tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Weight Aagum Endhan Heartu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weight Aagum Endhan Heartu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cute Ah Nee Paarkkum Podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cute Ah Nee Paarkkum Podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaanaa Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaanaa Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Strawberry Kanna Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Strawberry Kanna Paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodaachu Moochu Kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodaachu Moochu Kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjoram Yellow Rice Su
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjoram Yellow Rice Su"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaama Poche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaama Poche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanave Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhikkum Podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhikkum Podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Paravai Polave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Paravai Polave"/>
</div>
<div class="lyrico-lyrics-wrapper">Siragai Virithu Parakkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragai Virithu Parakkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Swag Swag Swag Swag
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swag Swag Swag Swag"/>
</div>
<div class="lyrico-lyrics-wrapper">Swag Swag Swag Swag
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swag Swag Swag Swag"/>
</div>
<div class="lyrico-lyrics-wrapper">Swag Swag Swag Swag
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swag Swag Swag Swag"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanave Swag Swag
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave Swag Swag"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanave Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanave Kanave"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu Kanave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhu Kanave"/>
</div>
</pre>
