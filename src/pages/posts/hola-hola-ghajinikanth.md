---
title: "hola hola song lyrics"
album: "Ghajinikanth"
artist: "Balamurali Balu"
lyricist: "Madhan Karky"
director: "Santhosh P. Jayakumar"
path: "/albums/ghajinikanth-lyrics"
song: "Hola Hola"
image: ../../images/albumart/ghajinikanth.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/GSn_vt5BKHg"
type: "love"
singers:
  - Benny Dayal
  - M M Manasi
  - Christopher Stanley
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hola la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la hola la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la hola la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama"/>
</div>
<div class="lyrico-lyrics-wrapper">You mama vaa mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You mama vaa mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la hola la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la hola la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama"/>
</div>
<div class="lyrico-lyrics-wrapper">You mama vaa mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You mama vaa mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ball rooma samba mamba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ball rooma samba mamba"/>
</div>
<div class="lyrico-lyrics-wrapper">Onna aadalaam nee vaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna aadalaam nee vaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Youtube-il namma paata potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Youtube-il namma paata potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Trend aagum paaruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trend aagum paaruma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha trump-a konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha trump-a konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Torture panni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Torture panni"/>
</div>
<div class="lyrico-lyrics-wrapper">Tweetu podu mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tweetu podu mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Jupiter-ukku charter flightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jupiter-ukku charter flightu"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyalamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rabbita naan undhan rabbita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rabbita naan undhan rabbita"/>
</div>
<div class="lyrico-lyrics-wrapper">Caarot-ah nee endhan carrota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Caarot-ah nee endhan carrota"/>
</div>
<div class="lyrico-lyrics-wrapper">Cassatta naan undhan cassatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cassatta naan undhan cassatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadichi thinna vaa vaa baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadichi thinna vaa vaa baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama hey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama hey mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama hey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama hey mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la hola la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la hola la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama"/>
</div>
<div class="lyrico-lyrics-wrapper">You mama vaa mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You mama vaa mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la hola la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la hola la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama"/>
</div>
<div class="lyrico-lyrics-wrapper">You mama vaa mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You mama vaa mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey baby baby hottie baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey baby baby hottie baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Dirty dirty baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dirty dirty baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Un sexting pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un sexting pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyo nethu thookam naan kettennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyo nethu thookam naan kettennae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un frankie kulla mutham naalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un frankie kulla mutham naalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Wrap panni nee thandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wrap panni nee thandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha naan kadikka vaai inikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha naan kadikka vaai inikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaaranam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaranam nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rabbita naan undhan rabbita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rabbita naan undhan rabbita"/>
</div>
<div class="lyrico-lyrics-wrapper">Carrota nee endhan carrota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Carrota nee endhan carrota"/>
</div>
<div class="lyrico-lyrics-wrapper">Cassatta naan undhan cassatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cassatta naan undhan cassatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadichi thinna vaa vaa baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadichi thinna vaa vaa baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama hey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama hey mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama hey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama hey mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey aalilaadha beach
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey aalilaadha beach"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda balconyil nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda balconyil nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilayaada sunscreen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilayaada sunscreen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thaan baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thaan baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mel mannum otta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mel mannum otta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvonna naanum thatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvonna naanum thatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketta kanavugal kanden baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketta kanavugal kanden baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puri puriyaadha musica pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puri puriyaadha musica pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lyrica ellam thappa thappa kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lyrica ellam thappa thappa kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Lipum hipum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lipum hipum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Asai podum kooda aaduna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asai podum kooda aaduna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kada kada mada mada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada kada mada mada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavugal ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pada pada pagalula nijamena aaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pada pada pagalula nijamena aaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I believe in that
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I believe in that"/>
</div>
<div class="lyrico-lyrics-wrapper">We should drink to that
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We should drink to that"/>
</div>
<div class="lyrico-lyrics-wrapper">I am only with my shady
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am only with my shady"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama hey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama hey mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama hey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama hey mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama hey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama hey mama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hola la la la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hola la la la"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey mama hey mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey mama hey mama"/>
</div>
</pre>
