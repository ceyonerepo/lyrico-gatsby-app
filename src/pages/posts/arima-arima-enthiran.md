---
title: "arima arima song lyrics"
album: "Enthiran"
artist: "A.R. Rahman"
lyricist: "Vairamuthu"
director: "S. Shankar"
path: "/albums/enthiran-lyrics"
song: "Arima Arima"
image: ../../images/albumart/enthiran.jpg
date: 2010-07-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/5F3H9_pYpYg"
type: "Mass"
singers:
  - Hariharan
  - Sadhana Sargam
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ivan Perai Sonnathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Perai Sonnathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumai Sonnathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumai Sonnathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalum Kadalum Kai Thattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalum Kadalum Kai Thattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Ulagam Thaandiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Ulagam Thaandiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyaram Kondathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram Kondathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavu Nilavu Thalai Muttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Nilavu Thalai Muttum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Azhage Ulagazhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Azhage Ulagazhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Enthiran Enbavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Enthiran Enbavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaippin Uchcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaippin Uchcham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arima Arima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arima Arima"/>
</div>
<div class="lyrico-lyrics-wrapper">Naano Aayiram Arima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naano Aayiram Arima"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pol Pon Maan Kidaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pol Pon Maan Kidaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yamma Summaa Vidumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamma Summaa Vidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rajathi Ulogathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajathi Ulogathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Thee Mooluthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Thee Mooluthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Atlantickai Ootri Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Atlantickai Ootri Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkini Anaiyalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkini Anaiyalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Pachai Thenai Ootru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pachai Thenai Ootru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Ichchai Theeyai Aatru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Ichchai Theeyai Aatru"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Kachai Kaniye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Kachai Kaniye"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandhi Nadathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhi Nadathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattil Ilai Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattil Ilai Pottu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arima Arima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arima Arima"/>
</div>
<div class="lyrico-lyrics-wrapper">Naano Aayiram Arima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naano Aayiram Arima"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pol Pon Maan Kidaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pol Pon Maan Kidaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yamma Summaa Vidumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamma Summaa Vidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Perai Sonnathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Perai Sonnathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumai Sonnathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumai Sonnathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalum Kadalum Kai Thattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalum Kadalum Kai Thattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Ulagam Thaandiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Ulagam Thaandiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyaram Kondathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram Kondathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavu Nilavu Thalai Muttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Nilavu Thalai Muttum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Azhage Ulagazhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Azhage Ulagazhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Enthiran Enbavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Enthiran Enbavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaippin Uchcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaippin Uchcham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sitrinba Narambu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sitrinba Narambu"/>
</div>
<div class="lyrico-lyrics-wrapper">Semitha Irumbil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semitha Irumbil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sattendru Mogam Pongittrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sattendru Mogam Pongittrey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratchasan Vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratchasan Vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasigan Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasigan Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennullam Unnai Kenjitre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennullam Unnai Kenjitre"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennullam Unnai Kenjitre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennullam Unnai Kenjitre"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Manidhan Alla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Manidhan Alla"/>
</div>
<div class="lyrico-lyrics-wrapper">Agrinaiyin Arasan Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Agrinaiyin Arasan Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaamutra Kanini Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamutra Kanini Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinnanjiru Idhayam Thinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinnanjiru Idhayam Thinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Silicon Singam Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silicon Singam Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arima Arima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arima Arima"/>
</div>
<div class="lyrico-lyrics-wrapper">Naano Aayiram Arima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naano Aayiram Arima"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pol Pon Maan Kidaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pol Pon Maan Kidaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yamma Summaa Vidumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamma Summaa Vidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Perai Sonnathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Perai Sonnathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumai Sonnathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumai Sonnathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalum Kadalum Kai Thattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalum Kadalum Kai Thattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Ulagam Thaandiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Ulagam Thaandiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyaram Kondathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram Kondathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavu Nilavu Thalai Muttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Nilavu Thalai Muttum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Azhage Ulagazhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Azhage Ulagazhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Enthiran Enbavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Enthiran Enbavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaippin Uchcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaippin Uchcham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Megathai Uduthun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Megathai Uduthun"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal Thaan Naanendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal Thaan Naanendru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice ikku Ice sai Vaikkaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ice ikku Ice sai Vaikkaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayarellaam Osai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayarellaam Osai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirellaam Aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirellaam Aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Robovai Po Po Vennaathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robovai Po Po Vennaathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Yezham Arivey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Yezham Arivey"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Moolai Thirudugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Moolai Thirudugiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu Unnugiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyirodu Unnugiraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Undu Muditha Micham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Undu Muditha Micham"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethuvo Athuthaan Naan Endraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethuvo Athuthaan Naan Endraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Perai Sonnathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Perai Sonnathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Perumai Sonnathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumai Sonnathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalum Kadalum Kai Thattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalum Kadalum Kai Thattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Ulagam Thaandiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Ulagam Thaandiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyaram Kondathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyaram Kondathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilavu Nilavu Thalai Muttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavu Nilavu Thalai Muttum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Azhage Ulagazhage
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Azhage Ulagazhage"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Enthiran Enbavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Enthiran Enbavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaippin Uchcham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaippin Uchcham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Arrima Arrima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arrima Arrima"/>
</div>
<div class="lyrico-lyrics-wrapper">Naano Aayiram Arima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naano Aayiram Arima"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Pol Pon Maan Kidaithaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pol Pon Maan Kidaithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Yamma Summaa Vidumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamma Summaa Vidumaa"/>
</div>
</pre>
