---
title: "moongil nila song lyrics"
album: "Lens"
artist: "G V Prakash Kumar "
lyricist: "Yugabharathi"
director: "Jayaprakash Radhakrishnan"
path: "/albums/lens-lyrics"
song: "Moongil Nila"
image: ../../images/albumart/lens.jpg
date: 2017-05-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TUsysXz1T-o"
type: "melody"
singers:
  -	Harini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Moongil Nila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Nila "/>
</div>
<div class="lyrico-lyrics-wrapper">Paadum Pudhu Aararo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadum Pudhu Aararo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongaa Vizhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongaa Vizhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Oonjal Idum Poonthero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal Idum Poonthero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaadhiyana Kadhalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaadhiyana Kadhalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidadhu Thoorum Thooralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidadhu Thoorum Thooralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadha Unn Paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadha Unn Paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">Dheepam Yetrum Vazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheepam Yetrum Vazhvilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanalneerum Kooda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanalneerum Kooda "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiserum Nee Varavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiserum Nee Varavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanal Pola Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanal Pola Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhuvaaga Aadavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuvaaga Aadavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mozhiyedhu Kooravae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mozhiyedhu Kooravae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moongil Nila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Nila "/>
</div>
<div class="lyrico-lyrics-wrapper">Paadum Pudhu Aararo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadum Pudhu Aararo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongaa Vizhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongaa Vizhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Oonjal Idum Poonthero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal Idum Poonthero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaadhiyana Kadhalae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaadhiyana Kadhalae "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidadhu Thoorum Thooralae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidadhu Thoorum Thooralae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadha Unn Paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadha Unn Paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">Dheepam Yetrum Vazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheepam Yetrum Vazhvilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oer Iravai Kaanumpadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oer Iravai Kaanumpadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhvomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhvomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraa Sugam Dhegam Thoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraa Sugam Dhegam Thoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Servomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Servomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nooru Nooru Aandu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nooru Nooru Aandu "/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oomaiyaana Neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oomaiyaana Neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhai Podudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai Podudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moongil Nila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moongil Nila "/>
</div>
<div class="lyrico-lyrics-wrapper">Paadum Pudhu Aararo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadum Pudhu Aararo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongaa Vizhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongaa Vizhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Oonjal Idum Poonthero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjal Idum Poonthero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaadhiyana Kadhalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaadhiyana Kadhalai "/>
</div>
<div class="lyrico-lyrics-wrapper">Vidadhu Thoorum Thooralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidadhu Thoorum Thooralai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadha Unn Paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadha Unn Paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">Dheepam Yetrum Vazhvilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheepam Yetrum Vazhvilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roja Idhazh 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja Idhazh "/>
</div>
<div class="lyrico-lyrics-wrapper">Pesaa Kadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaa Kadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesaadho"/>
</div>
<div class="lyrico-lyrics-wrapper">Koondhal Mazhai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koondhal Mazhai "/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai Kulir
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Kulir"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesaadho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesaadho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vervai Meniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vervai Meniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veenai Meetida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veenai Meetida"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Ketkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Ketkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Isai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Isai"/>
</div>
</pre>
