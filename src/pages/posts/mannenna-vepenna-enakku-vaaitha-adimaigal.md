---
title: "mannenna vepenna song lyrics"
album: "Enakku Vaaitha Adimaigal"
artist: "Santhosh Dhayanidhi"
lyricist: "Comedy Bazaar Maran"
director: "Mahendran Rajamani"
path: "/albums/enakku-vaaitha-adimaigal-lyrics"
song: "Mannenna Vepenna"
image: ../../images/albumart/enakku-vaaitha-adimaigal.jpg
date: 2017-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/DeXlJsOfNts"
type: "gaana"
singers:
  - Dolak Jagan
  - Mukesh Mohamed
  - Chennai Boys Choir
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naatla irukkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatla irukkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vootula irukkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vootula irukkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayiram friend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram friend-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan adangaatha friend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan adangaatha friend-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan ayogiya friend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan ayogiya friend-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan anaagareega friend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan anaagareega friend-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan araajaga friend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan araajaga friend-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan aanava friend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan aanava friend-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Moththathula oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moththathula oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Alpamaana friend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alpamaana friend-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend-u .friend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend-u .friend-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Friend-u friend-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friend-u friend-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannenna veppenna velakkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannenna veppenna velakkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan ekkedu kettaathaan enakkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan ekkedu kettaathaan enakkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrraana daarraaana damukkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrraana daarraaana damukkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan kodumpaavi bommaiya koluthenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan kodumpaavi bommaiya koluthenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oora suththum maadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oora suththum maadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan onnaa number fraudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan onnaa number fraudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nenja nakka vandhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenja nakka vandhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee nikkaamalae odu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nikkaamalae odu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalaiya thalaiya aattuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiya thalaiya aattuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Filimathaanae kaattuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Filimathaanae kaattuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinju pona tyre vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinju pona tyre vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Peelaa vandiya ottuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peelaa vandiya ottuvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kaala odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kaala odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan moonja odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan moonja odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththa poren daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththa poren daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonja odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonja odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozha urichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha urichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dolaak thatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dolaak thatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththa poren daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththa poren daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannenna veppenna velakkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannenna veppenna velakkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan ekkedu kettaathaan enakkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan ekkedu kettaathaan enakkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrraana daarraaana damukkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrraana daarraaana damukkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan kodumpaavi bommaiya koluthenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan kodumpaavi bommaiya koluthenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan manushan illa maanikkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan manushan illa maanikkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Solra vaayi konikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solra vaayi konikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi sarukkum yaanaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi sarukkum yaanaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirundha maattaan ennikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirundha maattaan ennikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnunganaa pesuvaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnunganaa pesuvaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pozhudhu varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhudhu varaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan poga maatan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan poga maatan"/>
</div>
<div class="lyrico-lyrics-wrapper">Police-thaan varra varaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police-thaan varra varaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Torture rathaan koduppaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Torture rathaan koduppaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga future-a paperaa kizhippaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga future-a paperaa kizhippaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozh koduppaan thozhanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozh koduppaan thozhanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan pazhatha mattum thinnuputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan pazhatha mattum thinnuputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thola mattum koduppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thola mattum koduppaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kaala odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kaala odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan moonja odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan moonja odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththa poren daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththa poren daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonja odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonja odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya odachiThozha urichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya odachiThozha urichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dolaak thatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dolaak thatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththa poren daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththa poren daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannenna veppenna velakkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannenna veppenna velakkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan ekkedu kettaathaan enakkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan ekkedu kettaathaan enakkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Darrraana daarraaana damukkenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darrraana daarraaana damukkenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan kodumpaavi bommaiya koluthenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan kodumpaavi bommaiya koluthenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan aasappattenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan aasappattenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avastha pattnenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avastha pattnenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Auto bomba akkula vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Auto bomba akkula vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Allal pattenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allal pattenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga railu pettiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga railu pettiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engala kalati vittiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engala kalati vittiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship-la petrol oothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship-la petrol oothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Koluthi vittiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koluthi vittiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakkaa mutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa mutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanna kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna kaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Thonga vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thonga vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thogura Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thogura Vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Palavaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palavaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mutham onnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mutham onnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaam podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaam podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththatha tharuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththatha tharuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanban thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanban thaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kaala odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kaala odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan moonja odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan moonja odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththa poren daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththa poren daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonja odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonja odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozha urichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha urichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dolaak thatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dolaak thatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththa poren daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththa poren daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adangappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu ulaga nadippuda saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu ulaga nadippuda saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machi edhukkuda unakkindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machi edhukkuda unakkindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Friendshippae venaam pa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Friendshippae venaam pa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelamburom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelamburom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhushpudhsa poiyya solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhushpudhsa poiyya solli"/>
</div>
<div class="lyrico-lyrics-wrapper">Potlam kattuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Potlam kattuvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha aattu thola mela pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha aattu thola mela pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enghala vettuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enghala vettuvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan kaala odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan kaala odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan moonja odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan moonja odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththa poren daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththa poren daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaiya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaala odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaala odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonja odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonja odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaya odachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaya odachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozha urichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozha urichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dolaak thatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dolaak thatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuththa poren daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuththa poren daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saagumpodhu kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagumpodhu kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduthavanukku tholla kodukkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduthavanukku tholla kodukkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagavae maattaanugadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagavae maattaanugadaa"/>
</div>
</pre>
