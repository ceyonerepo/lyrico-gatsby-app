---
title: "thangame thangame song lyrics"
album: "Veeram"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Siva"
path: "/albums/veeram-lyrics"
song: "Thangame Thangame"
image: ../../images/albumart/veeram.jpg
date: 2014-01-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/h4q9HxT-t2o"
type: "Love"
singers:
  - Adnan Sami
  - Priyadarshini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hey Pattampoochi Pattampoochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Pattampoochi Pattampoochi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vekkapattu Sirikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkapattu Sirikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kada Meesa Kada Messa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kada Meesa Kada Messa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etti Katti Pidikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti Katti Pidikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothi Vecha Aasai Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothi Vecha Aasai Ellam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathikittu Eriyuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathikittu Eriyuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Ennum Puyal Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Ennum Puyal Ippo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koora Pikka Adikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koora Pikka Adikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochu Nikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Nikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pechu Thikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechu Thikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manassu Sokkuthu Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manassu Sokkuthu Unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasa Pinnudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasa Pinnudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Usura Thinnuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usura Thinnuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yetho Pannudhu Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetho Pannudhu Unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusaaga Enna Aarambicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaaga Enna Aarambicha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavaala Nenja Oora Vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavaala Nenja Oora Vecha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaala Enna Aara Vacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaala Enna Aara Vacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viral Neetti Enna Vegavecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral Neetti Enna Vegavecha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sugamaaga Enna Saagavacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaaga Enna Saagavacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Thaandi Enna Pogavacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Thaandi Enna Pogavacha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangame Thangame Enna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Thangame Enna Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paathathum Nenchile Pongalaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paathathum Nenchile Pongalaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangame Thangame Enna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Thangame Enna Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Sattunu Vaazhkaiye Vannamaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Sattunu Vaazhkaiye Vannamaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochu Nikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Nikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pechu Thikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechu Thikkudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manassu Sokkuthu Unnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manassu Sokkuthu Unnaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vaadagaikku Veedu Thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaadagaikku Veedu Thedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seidhi Naanum Oorukulla Ketten Adi Ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seidhi Naanum Oorukulla Ketten Adi Ketten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Poovil Irukkum Thottil Ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Poovil Irukkum Thottil Ondrai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vasikka Kaali Seidhu veithen Adi Vaithen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vasikka Kaali Seidhu veithen Adi Vaithen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeril Vandhaai Saaral Vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeril Vandhaai Saaral Vandhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neril Vandhaai Maatrugindraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neril Vandhaai Maatrugindraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oyivillamale Thullinene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyivillamale Thullinene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovin Edai Kaatrin Edai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin Edai Kaatrin Edai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sertha Vidai Undhan Edai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sertha Vidai Undhan Edai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnu Kudu Mookku Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu Kudu Mookku Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viralu vaikkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralu vaikkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangame Thangame Enna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Thangame Enna Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paathathum Nenchile Pongalaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paathathum Nenchile Pongalaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangame Thangame Enna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Thangame Enna Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Sattunu Vaazhkaiye Vannamaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Sattunu Vaazhkaiye Vannamaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unthan Kai Ezhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Kai Ezhuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pottu Vaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottu Vaitha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaagithaththil Vaanavillai Paarththen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaagithaththil Vaanavillai Paarththen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Pooththaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pooththaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unthan Kaalgal Vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unthan Kaalgal Vantha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhai Engum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhai Engum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mannedethu Pookalaaga Korthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannedethu Pookalaaga Korthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Serthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Serthaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Oru Kaadhal Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Oru Kaadhal Oru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bodhai Nadhi Bodhai Nadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bodhai Nadhi Bodhai Nadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muzhga Kooda Thevai Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhga Kooda Thevai Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarkkum Pothe Unnai Saaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkkum Pothe Unnai Saaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Oru  Neenda Sathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Oru  Neenda Sathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sogam Tharum Inbam Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sogam Tharum Inbam Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerum Theeyum Paadhi Paadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerum Theeyum Paadhi Paadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Serndhu Paakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serndhu Paakkume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangame Thangame Enna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Thangame Enna Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Paathathum Nenchile Pongalaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Paathathum Nenchile Pongalaachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thangame Thangame Enna Aachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thangame Thangame Enna Aachu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Sattunu Vaazhkaiye Vannamaachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Sattunu Vaazhkaiye Vannamaachu"/>
</div>
</pre>
