---
title: "overa feel pannuren song lyrics"
album: 'Hero'
artist: 'Yuvan Shankar Raja'
lyricist: 'Pa Vijay, Mc Sanna'
director: 'P.S.Mithran'
path: '/albums/hero'
song: "Overa Feel Pannuren"
image: ../../images/albumart/hero.jpg
date: 2019-12-20
lang: tamil
singers: 
- Yuvan Shankar Raja
- MC Sanna
youtubeLink: "https://www.youtube.com/embed/6_Sd-z_fMLo"
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Naa romba over-ah feel pannuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa romba over-ah feel pannuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla fever-ah feel pannuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla fever-ah feel pannuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi caramel nilavae
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi caramel nilavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna romba care pannuren
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna romba care pannuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nee yaar koodavo chat pannura
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee yaar koodavo chat pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Maththa piece ellaam meet pannura
<input type="checkbox" class="lyrico-select-lyric-line" value="Maththa piece ellaam meet pannura"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen <div class="lyrico-lyrics-wrapper">2 GB heart-ah</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Yen <div class="lyrico-lyrics-wrapper">2 GB heart-ah"/></div>
</div>
<div class="lyrico-lyrics-wrapper">Too much-ah heat pannura…
<input type="checkbox" class="lyrico-select-lyric-line" value="Too much-ah heat pannura…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">En cuppu-la kottiya
<input type="checkbox" class="lyrico-select-lyric-line" value="En cuppu-la kottiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ice cream veroru spoonu-ka?
<input type="checkbox" class="lyrico-select-lyric-line" value="Ice cream veroru spoonu-ka?"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhalin dreams-u dhaan
<input type="checkbox" class="lyrico-select-lyric-line" value="En kaadhalin dreams-u dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Innoru padathin sceneu-ka?
<input type="checkbox" class="lyrico-select-lyric-line" value="Innoru padathin sceneu-ka?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adada -daa gappu-la
<input type="checkbox" class="lyrico-select-lyric-line" value="Adada -daa gappu-la"/>
</div>
<div class="lyrico-lyrics-wrapper">America mappila kedaikkuma?
<input type="checkbox" class="lyrico-select-lyric-line" value="America mappila kedaikkuma?"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedachita thaanguma?
<input type="checkbox" class="lyrico-select-lyric-line" value="Kedachita thaanguma?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaadi vaadi vaadi walnut-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaadi vaadi vaadi walnut-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart-ku yen helmet-u?
<input type="checkbox" class="lyrico-select-lyric-line" value="Heart-ku yen helmet-u?"/>
</div>
<div class="lyrico-lyrics-wrapper">Tea-ku ethukku t-shirtu?
<input type="checkbox" class="lyrico-select-lyric-line" value="Tea-ku ethukku t-shirtu?"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum dhaan set-u…
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum naanum dhaan set-u…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vekkam viricha velvet-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Vekkam viricha velvet-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku veppen poo pottu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakku veppen poo pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru moththam koopitu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooru moththam koopitu"/>
</div>
<div class="lyrico-lyrics-wrapper">Take-it-u talk-it-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Take-it-u talk-it-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaaka thooki pona vada
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaaka thooki pona vada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaiyaachae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadhaiyaachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hot sun kooda palla kaati
<input type="checkbox" class="lyrico-select-lyric-line" value="Hot sun kooda palla kaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Elichaachae
<input type="checkbox" class="lyrico-select-lyric-line" value="Elichaachae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sweater pottu-kinnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Sweater pottu-kinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Scene-u kooda potaachae
<input type="checkbox" class="lyrico-select-lyric-line" value="Scene-u kooda potaachae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadha solla ponna thadikkiyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kadha solla ponna thadikkiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilundhaachae
<input type="checkbox" class="lyrico-select-lyric-line" value="Vilundhaachae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Salpaetaa saroja parambarai
<input type="checkbox" class="lyrico-select-lyric-line" value="Salpaetaa saroja parambarai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennama ava circus kaatura
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennama ava circus kaatura"/>
</div>
<div class="lyrico-lyrics-wrapper">Petromax-u lightae dhan venuma?
<input type="checkbox" class="lyrico-select-lyric-line" value="Petromax-u lightae dhan venuma?"/>
</div>
<div class="lyrico-lyrics-wrapper">Tubelight-u pola kaadhal
<input type="checkbox" class="lyrico-select-lyric-line" value="Tubelight-u pola kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu vittu eriyudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Vittu vittu eriyudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Un kooda dhaan ….
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kooda dhaan …."/>
</div>
<div class="lyrico-lyrics-wrapper">Un kooda dhaan tanglish la
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kooda dhaan tanglish la"/>
</div>
<div class="lyrico-lyrics-wrapper">Love solla aasa yeruthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Love solla aasa yeruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Senthamil paiyan enakulla ippo
<input type="checkbox" class="lyrico-select-lyric-line" value="Senthamil paiyan enakulla ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakespeare raththam oorudhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Shakespeare raththam oorudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oorilulla fm ellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Oorilulla fm ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pera paatta poduthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Un pera paatta poduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kitta pesa vaarthaigal thedi
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kitta pesa vaarthaigal thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamizhae tution poguthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Tamizhae tution poguthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Enakku-ennamo aachu di
<input type="checkbox" class="lyrico-select-lyric-line" value="Enakku-ennamo aachu di"/>
</div>
<div class="lyrico-lyrics-wrapper">Puruvam molacha poochedi
<input type="checkbox" class="lyrico-select-lyric-line" value="Puruvam molacha poochedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasanga life-il
<input type="checkbox" class="lyrico-select-lyric-line" value="Pasanga life-il"/>
</div>
<div class="lyrico-lyrics-wrapper">Pub-g aadi paarkum velai yenadi?
<input type="checkbox" class="lyrico-select-lyric-line" value="Pub-g aadi paarkum velai yenadi?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Moon-il senja moonjiya
<input type="checkbox" class="lyrico-select-lyric-line" value="Moon-il senja moonjiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Stone-il senja nenjama
<input type="checkbox" class="lyrico-select-lyric-line" value="Stone-il senja nenjama"/>
</div>
<div class="lyrico-lyrics-wrapper">Crush-u vandhu un nenjil
<input type="checkbox" class="lyrico-select-lyric-line" value="Crush-u vandhu un nenjil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kercheif podavillemma
<input type="checkbox" class="lyrico-select-lyric-line" value="Kercheif podavillemma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vaadi vaadi vaadi walnut-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaadi vaadi vaadi walnut-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart-ku yen helmet-u?
<input type="checkbox" class="lyrico-select-lyric-line" value="Heart-ku yen helmet-u?"/>
</div>
<div class="lyrico-lyrics-wrapper">Tea-ku ethukku t-shirtu?
<input type="checkbox" class="lyrico-select-lyric-line" value="Tea-ku ethukku t-shirtu?"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum dhaan set-u…
<input type="checkbox" class="lyrico-select-lyric-line" value="Neeyum naanum dhaan set-u…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Vekkam viricha velvet-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Vekkam viricha velvet-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku veppen poo pottu
<input type="checkbox" class="lyrico-select-lyric-line" value="Unakku veppen poo pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru moththam koopitu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooru moththam koopitu"/>
</div>
<div class="lyrico-lyrics-wrapper">Take-it-u talk-it-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Take-it-u talk-it-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ava thaan un queen lady
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava thaan un queen lady"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodhuvoma pee pee pee
<input type="checkbox" class="lyrico-select-lyric-line" value="Oodhuvoma pee pee pee"/>
</div>
<div class="lyrico-lyrics-wrapper">Honeymoon-ku nee thaan avala
<input type="checkbox" class="lyrico-select-lyric-line" value="Honeymoon-ku nee thaan avala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ittunu poo ooty ..hey….
<input type="checkbox" class="lyrico-select-lyric-line" value="Ittunu poo ooty ..hey…."/>
</div>
</pre>