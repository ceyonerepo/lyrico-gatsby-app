---
title: 'happy birthday song lyrics'
album: 'Naan Sirithal'
artist: 'Hiphop Tamizha'
lyricist: 'Hiphop Tamizha'
director: 'Raana'
path: '/albums/naan-sirithal-song-lyrics'
song: 'Happy Birthday'
image: ../../images/albumart/naan-sirithal.jpg  
date: 2019-07-19
lang: tamil
singers: 
- Diwakar
- Kaka Balachander
youtubeLink: "https://www.youtube.com/embed/ODJUiHraIlQ"
type: 'happy'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Haaan aaa….hooo ooo
<input type="checkbox" class="lyrico-select-lyric-line" value="Haaan aaa….hooo ooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasukku vettura kathiyellaam
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaasukku vettura kathiyellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasatha kottudhu akkaraiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Paasatha kottudhu akkaraiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala thookkura groupulaiyum
<input type="checkbox" class="lyrico-select-lyric-line" value="Aala thookkura groupulaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anna inikkura sakkaraiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Anna inikkura sakkaraiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Adi aalu adi dhoolu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi aalu adi dhoolu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhu aalu en kadha kelu
<input type="checkbox" class="lyrico-select-lyric-line" value="Pudhu aalu en kadha kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paada nee aadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan paada nee aadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa adhiranum daa
<input type="checkbox" class="lyrico-select-lyric-line" value="Summa adhiranum daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamilnadu
<input type="checkbox" class="lyrico-select-lyric-line" value="Tamilnadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Happy birthday to you
<input type="checkbox" class="lyrico-select-lyric-line" value="Happy birthday to you"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga annanukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Enga annanukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy birthday to you
<input type="checkbox" class="lyrico-select-lyric-line" value="Happy birthday to you"/>
</div>
<div class="lyrico-lyrics-wrapper">En thalaivanukku (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="En thalaivanukku"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Aruva pudiyila
<input type="checkbox" class="lyrico-select-lyric-line" value="Aruva pudiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Anba kulaiyura
<input type="checkbox" class="lyrico-select-lyric-line" value="Anba kulaiyura"/>
</div>
<div class="lyrico-lyrics-wrapper">Rowdy uncle..
<input type="checkbox" class="lyrico-select-lyric-line" value="Rowdy uncle.."/>
</div>
  <div class="lyrico-lyrics-wrapper">How are you?
<input type="checkbox" class="lyrico-select-lyric-line" value="How are you?"/>
</div>
  <div class="lyrico-lyrics-wrapper">Naa thaniya sikkuna
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa thaniya sikkuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaviyaa thavikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Thaviyaa thavikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudiyala machaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Mudiyala machaa"/>
</div>
  <div class="lyrico-lyrics-wrapper">Where are you?
<input type="checkbox" class="lyrico-select-lyric-line" value="Where are you?"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Merati merati
<input type="checkbox" class="lyrico-select-lyric-line" value="Merati merati"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasa pudungum
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaasa pudungum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhadha your honour-nga
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhadha your honour-nga"/>
</div>
<div class="lyrico-lyrics-wrapper">Arumai perumaiyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Arumai perumaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam ariyaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulagam ariyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi kanum oru banner…
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi kanum oru banner…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">AC carula
<input type="checkbox" class="lyrico-select-lyric-line" value="AC carula"/>
</div>
<div class="lyrico-lyrics-wrapper">Oora suthanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Oora suthanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasa vaanginaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaasa vaanginaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aala kuthanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Aala kuthanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Anna adikkura
<input type="checkbox" class="lyrico-select-lyric-line" value="Anna adikkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiya paathu
<input type="checkbox" class="lyrico-select-lyric-line" value="Adiya paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyo amma-nnu
<input type="checkbox" class="lyrico-select-lyric-line" value="Aiyo amma-nnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae kathanum…
<input type="checkbox" class="lyrico-select-lyric-line" value="Oorae kathanum…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Happy birthday to you
<input type="checkbox" class="lyrico-select-lyric-line" value="Happy birthday to you"/>
</div>
<div class="lyrico-lyrics-wrapper">En singathuku
<input type="checkbox" class="lyrico-select-lyric-line" value="En singathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy birthday to you
<input type="checkbox" class="lyrico-select-lyric-line" value="Happy birthday to you"/>
</div>
<div class="lyrico-lyrics-wrapper">En chellathukku (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="En chellathukku (2 times)"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thundu thundaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Thundu thundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetti saikanum daaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Vetti saikanum daaa…"/>
</div>
  <div class="lyrico-lyrics-wrapper">Saadhi madhatha
<input type="checkbox" class="lyrico-select-lyric-line" value="Saadhi madhatha"/>
</div>
  <div class="lyrico-lyrics-wrapper">Piece-u piece-ah
<input type="checkbox" class="lyrico-select-lyric-line" value="Piece-u piece-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Suttu thaakanum daaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Suttu thaakanum daaa…"/>
</div>
  <div class="lyrico-lyrics-wrapper">Ketta nenappaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Ketta nenappaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Mothi paakadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Mothi paakadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ivanodaa …
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee ivanodaa …"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutual friendu
<input type="checkbox" class="lyrico-select-lyric-line" value="Mutual friendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga annan emanodaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Enga annan emanodaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooooh…oh
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooooh…oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Scene-u kaatadhaa
<input type="checkbox" class="lyrico-select-lyric-line" value="Scene-u kaatadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ennaandaa…
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee ennaandaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootti varuvan
<input type="checkbox" class="lyrico-select-lyric-line" value="Kootti varuvan"/>
</div>
<div class="lyrico-lyrics-wrapper">En housing board-ah…
<input type="checkbox" class="lyrico-select-lyric-line" value="En housing board-ah…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooooh..ooooh..oh
<input type="checkbox" class="lyrico-select-lyric-line" value="Ooooh..ooooh..oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Happy birthday to you
<input type="checkbox" class="lyrico-select-lyric-line" value="Happy birthday to you"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga annanukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Enga annanukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy birthday to you
<input type="checkbox" class="lyrico-select-lyric-line" value="Happy birthday to you"/>
</div>
<div class="lyrico-lyrics-wrapper">En thalaivanukku (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="En thalaivanukku"/></div>
</div>
<div class="lyrico-lyrics-wrapper">En thalaivanukku….
<input type="checkbox" class="lyrico-select-lyric-line" value="En thalaivanukku…."/>
</div>
</pre>