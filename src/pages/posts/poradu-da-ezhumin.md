---
title: "poradu da song lyrics"
album: "Ezhumin"
artist: "Ganesh Chandrasekaran"
lyricist: "Pa Vijay"
director: "VP Viji"
path: "/albums/ezhumin-lyrics"
song: "Poradu Da"
image: ../../images/albumart/ezhumin.jpg
date: 2018-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/iap_ufd8tSQ"
type: "mass"
singers:
  - Yogi B
  - Anand Aravindakshan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaada neeyum poradu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada neeyum poradu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnae ninnu vilayadu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnae ninnu vilayadu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri kotta nee thodu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri kotta nee thodu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Poda innum poradu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda innum poradu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vatta vatta vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta vatta vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naama kaiyodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naama kaiyodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta thatta vetri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta thatta vetri"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma kannodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma kannodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Etta etta ezhunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etta etta ezhunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillu thimirodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nillu thimirodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu mutta muyarchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu mutta muyarchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Undu nammodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undu nammodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarala mudiyathu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarala mudiyathu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrukku thadai yethu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrukku thadai yethu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattathil vizhunthalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattathil vizhunthalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avamanam kidaiyathu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avamanam kidaiyathu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada neeyum poradu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada neeyum poradu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnae ninnu vilayadu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnae ninnu vilayadu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri kotta nee thodu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri kotta nee thodu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Poda innum poradu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda innum poradu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tholvi unna sethukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholvi unna sethukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorae unna othukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorae unna othukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam ellam pathakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam ellam pathakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum kalangathae ethukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum kalangathae ethukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga vedikkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vedikkai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikkai paarpavano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkai paarpavano"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum pechu pesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum pechu pesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaai valarpaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai valarpaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vetri paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vetri paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarpavano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarpavano"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanakullae thee valarpaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakullae thee valarpaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada yethukku yethukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada yethukku yethukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha sogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha sogam"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum irukku irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum irukku irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla vegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla vegam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adangaathu paaru un thaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adangaathu paaru un thaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru magudam unathaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru magudam unathaagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarala mudiyathu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarala mudiyathu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrukku thadai yethu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrukku thadai yethu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattathil vizhunthalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattathil vizhunthalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avamanam kidaiyathu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avamanam kidaiyathu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada neeyum poradu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada neeyum poradu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnae ninnu vilayadu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnae ninnu vilayadu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri kotta nee thodu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri kotta nee thodu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Poda innum poradu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda innum poradu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Innum enna bayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum enna bayama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mothi paaru suyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothi paaru suyama"/>
</div>
<div class="lyrico-lyrics-wrapper">Tholvi indri sugama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tholvi indri sugama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada vetri mattum varuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada vetri mattum varuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenja keeri keeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja keeri keeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vithai podu podu unakullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithai podu podu unakullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maramaaga athuvum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maramaaga athuvum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha ulagam kai serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ulagam kai serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini enna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini enna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana yekkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana yekkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhunthaalum enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhunthaalum enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Avamanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avamanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri koottukilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri koottukilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini vaanam adi vaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini vaanam adi vaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarala mudiyathu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarala mudiyathu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrukku thadai yethu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrukku thadai yethu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattathil vizhunthalumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattathil vizhunthalumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Avamanam kidaiyathu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avamanam kidaiyathu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeahhh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeahhh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaada neeyum poradu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaada neeyum poradu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Munnae ninnu vilayadu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnae ninnu vilayadu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetri kotta nee thodu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetri kotta nee thodu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Poda innum poradu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda innum poradu da"/>
</div>
</pre>
