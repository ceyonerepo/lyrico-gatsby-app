---
title: "akashama song lyrics"
album: "Tholu Bommalata"
artist: "Suresh Bobbili"
lyricist: "Chaitanya Prasad"
director: "Viswanath Maganti"
path: "/albums/tholu-bommalata-lyrics"
song: "Akashama"
image: ../../images/albumart/tholu-bommalata.jpg
date: 2019-11-22
lang: telugu
youtubeLink: "https://www.youtube.com/embed/MQhYiIeQwPE"
type: "happy"
singers:
  - Hemachandra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">oh oh oh aakasama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh aakasama"/>
</div>
<div class="lyrico-lyrics-wrapper">harivilly mellaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="harivilly mellaga "/>
</div>
<div class="lyrico-lyrics-wrapper">nelku cheruma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nelku cheruma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh oh oh oh bhulokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh oh oh oh bhulokama"/>
</div>
<div class="lyrico-lyrics-wrapper">anandapu alve 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anandapu alve "/>
</div>
<div class="lyrico-lyrics-wrapper">ningi ki sagoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ningi ki sagoma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh ontrai gudilona 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh ontrai gudilona "/>
</div>
<div class="lyrico-lyrics-wrapper">jayagantal sadilaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jayagantal sadilaga"/>
</div>
<div class="lyrico-lyrics-wrapper">ee jant guvv lochi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee jant guvv lochi "/>
</div>
<div class="lyrico-lyrics-wrapper">valega hayiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valega hayiga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh jamuratrilo jabilli tunkalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh jamuratrilo jabilli tunkalla"/>
</div>
<div class="lyrico-lyrics-wrapper">ee prema kantule duvvega
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee prema kantule duvvega"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh gull gull gull gull gull
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh gull gull gull gull gull"/>
</div>
<div class="lyrico-lyrics-wrapper">antu mogn hradayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antu mogn hradayame"/>
</div>
<div class="lyrico-lyrics-wrapper">jai jai jai jai jai antu adanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jai jai jai jai jai antu adanule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">andmuga anandamuga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andmuga anandamuga "/>
</div>
<div class="lyrico-lyrics-wrapper">uchin pranayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uchin pranayame"/>
</div>
<div class="lyrico-lyrics-wrapper">ashistool akshintal korenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ashistool akshintal korenule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oh ra chilka navvale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oh ra chilka navvale "/>
</div>
<div class="lyrico-lyrics-wrapper">gorink navvale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gorink navvale"/>
</div>
<div class="lyrico-lyrics-wrapper">godari gattu meeda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="godari gattu meeda "/>
</div>
<div class="lyrico-lyrics-wrapper">chettu kuda navvale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chettu kuda navvale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bandhale pachchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bandhale pachchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">tadisenule nulivachchaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tadisenule nulivachchaga"/>
</div>
<div class="lyrico-lyrics-wrapper">bhasall rasukunn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bhasall rasukunn"/>
</div>
<div class="lyrico-lyrics-wrapper">ashalenni trichaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ashalenni trichaga"/>
</div>
</pre>
