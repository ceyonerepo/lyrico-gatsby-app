---
title: "fulltoo tension song lyrics"
album: "Nevalle Nenunna"
artist: "Siddarth Sadasivuni"
lyricist: "Krishna kanth"
director: "Saibaba. M"
path: "/albums/neevalle-nenunna-lyrics"
song: "Fulltoo Tension"
image: ../../images/albumart/neevalle-nenunna.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/NDe2qwN7RhU"
type: "love"
singers:
  - Manoj Sharma
  - Nivin Bedford
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thanna Naa Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Naa Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna Nana Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Nana Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thaana Nana Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thaana Nana Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana Naa Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana Naa Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana Nana Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana Nana Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna Nana Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Nana Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thanna Naa Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thanna Naa Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanana Naa Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanana Naa Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallalona Kalle Petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalona Kalle Petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusindho Pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusindho Pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kickey Ekkina Feelingey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kickey Ekkina Feelingey"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkasari Nanne Chusi Navvindhante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkasari Nanne Chusi Navvindhante"/>
</div>
<div class="lyrico-lyrics-wrapper">Luckye Dhakkadha Darlingey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luckye Dhakkadha Darlingey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heart Beat Lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart Beat Lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Lub Dub Meter
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lub Dub Meter"/>
</div>
<div class="lyrico-lyrics-wrapper">Lock Aipothine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lock Aipothine"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Perinte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Perinte"/>
</div>
<div class="lyrico-lyrics-wrapper">Doubt Doubt Ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doubt Doubt Ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Love Maatare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Love Maatare"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppe Lopale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppe Lopale"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhada Dhada Startye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhada Dhada Startye"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Too Tensioney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Too Tensioney"/>
</div>
<div class="lyrico-lyrics-wrapper">Yesoo No Aney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yesoo No Aney"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Too Tensioney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Too Tensioney"/>
</div>
<div class="lyrico-lyrics-wrapper">Frindship Chedenaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Frindship Chedenaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Too Tensioney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Too Tensioney"/>
</div>
<div class="lyrico-lyrics-wrapper">Breackup Antavaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Breackup Antavaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Too Tensioney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Too Tensioney"/>
</div>
<div class="lyrico-lyrics-wrapper">Miss Avuthavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miss Avuthavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Halfye Fill Ayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Halfye Fill Ayina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Glass Lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Glass Lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Neerey Neena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerey Neena"/>
</div>
<div class="lyrico-lyrics-wrapper">Maro Kaali Nenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maro Kaali Nenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Gunde Lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Gunde Lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Tattoo Nayyanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tattoo Nayyanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaalosthe Poye Haayena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaalosthe Poye Haayena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyaro Pareshaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyaro Pareshaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Pataayinchi Padesaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pataayinchi Padesaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaccchi Hug Isthundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaccchi Hug Isthundho"/>
</div>
<div class="lyrico-lyrics-wrapper">Touchye Thagginsthundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touchye Thagginsthundho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nacchesavantu Naa Notey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nacchesavantu Naa Notey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppeyalo Thappinchalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppeyalo Thappinchalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Fulltoo Tensioney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fulltoo Tensioney"/>
</div>
<div class="lyrico-lyrics-wrapper">yesoo No Aney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yesoo No Aney"/>
</div>
<div class="lyrico-lyrics-wrapper">Fulltoo Tensioney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fulltoo Tensioney"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship Chedenaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship Chedenaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Too Tensioney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Too Tensioney"/>
</div>
<div class="lyrico-lyrics-wrapper">Breackup Antavaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Breackup Antavaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Too Tensioney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Too Tensioney"/>
</div>
<div class="lyrico-lyrics-wrapper">Miss Avuthavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miss Avuthavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallalona Kalle Petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallalona Kalle Petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Chusindho Pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chusindho Pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kickey Ekkina Feelingey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kickey Ekkina Feelingey"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkasari Nanne Chusi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkasari Nanne Chusi"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvindhante Luckye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvindhante Luckye"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhakkadha Darlingey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhakkadha Darlingey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Okay Cheppindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okay Cheppindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanney Mecchindho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanney Mecchindho"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhakaina Ala Thode Raana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhakaina Ala Thode Raana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Prema Lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Prema Lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennalley Ayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennalley Ayina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Rojey Malli Kotthena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Rojey Malli Kotthena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kyaro Pareshaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyaro Pareshaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Pataayinchi Padesaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pataayinchi Padesaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaccchi Hug Isthundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaccchi Hug Isthundho"/>
</div>
<div class="lyrico-lyrics-wrapper">Touchye Thagginsthundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touchye Thagginsthundho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nacchesavantu Naa Notey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nacchesavantu Naa Notey"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheppeyalo Thappinchalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheppeyalo Thappinchalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Fulltoo Tensioney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fulltoo Tensioney"/>
</div>
<div class="lyrico-lyrics-wrapper">yesoo No Aney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yesoo No Aney"/>
</div>
<div class="lyrico-lyrics-wrapper">Fulltoo Tensioney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fulltoo Tensioney"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship Chedenaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship Chedenaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Too Tensioney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Too Tensioney"/>
</div>
<div class="lyrico-lyrics-wrapper">Breackup Antavaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Breackup Antavaney"/>
</div>
<div class="lyrico-lyrics-wrapper">Full Too Tensioney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Too Tensioney"/>
</div>
<div class="lyrico-lyrics-wrapper">Miss Avuthavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miss Avuthavane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanna Naa Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Naa Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna Nana Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Nana Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thaana Nana Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thaana Nana Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaana Naa Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaana Naa Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nana Nana Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nana Nana Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanna Nana Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanna Nana Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Thanna Naa Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Thanna Naa Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanana Naa Nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanana Naa Nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa"/>
</div>
</pre>
