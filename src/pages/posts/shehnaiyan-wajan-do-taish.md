---
title: "shehnaiyan wajan do song lyrics"
album: "Taish"
artist: "Enbee"
lyricist: "Enbee"
director: "Bejoy Nambiar"
path: "/albums/taish-lyrics"
song: "Shehnaiyan Wajan Do"
image: ../../images/albumart/taish.jpg
date: 2020-10-29
lang: hindi
youtubeLink: "https://www.youtube.com/embed/LiblJRcplVE"
type: "happy"
singers:
  - Enbee
  - Raahi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Gall suno club waleyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gall suno club waleyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hunn assi pauna bhangra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hunn assi pauna bhangra"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Buraahhh...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buraahhh..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho ladka hai ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ladka hai ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Or ladki taiyaar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Or ladki taiyaar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Baj gayi hai shaadi wali bell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baj gayi hai shaadi wali bell"/>
</div>
<div class="lyrico-lyrics-wrapper">Hua pyaar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hua pyaar hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udd gaye kabootar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udd gaye kabootar"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaj wedding wali raat hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaj wedding wali raat hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dost saare naachein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dost saare naachein"/>
</div>
<div class="lyrico-lyrics-wrapper">Apne yaar ki baraat me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apne yaar ki baraat me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan ladka hai ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan ladka hai ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur ladki taiyaar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur ladki taiyaar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Baj gayi hai shaadi wali bell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baj gayi hai shaadi wali bell"/>
</div>
<div class="lyrico-lyrics-wrapper">Hua pyaar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hua pyaar hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udd gaye kabootar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udd gaye kabootar"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaj wedding wali raat hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaj wedding wali raat hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dost saare naachein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dost saare naachein"/>
</div>
<div class="lyrico-lyrics-wrapper">Apne yaar ki baraat me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apne yaar ki baraat me"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhangra hai pauna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhangra hai pauna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur giddha lagana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur giddha lagana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo nachna chahta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo nachna chahta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachne doo..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachne doo.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bajne do gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajne do gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur khaane do khana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur khaane do khana"/>
</div>
<div class="lyrico-lyrics-wrapper">jo dil chahta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jo dil chahta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh hone doo..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh hone doo.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shehnaiyan wajan do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shehnaiyan wajan do"/>
</div>
<div class="lyrico-lyrics-wrapper">Shehnaiyan wajan do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shehnaiyan wajan do"/>
</div>
<div class="lyrico-lyrics-wrapper">Shehnaiyan wajan do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shehnaiyan wajan do"/>
</div>
<div class="lyrico-lyrics-wrapper">Shehnaiyan wajan do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shehnaiyan wajan do"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jisko sajna hai sajne do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jisko sajna hai sajne do"/>
</div>
<div class="lyrico-lyrics-wrapper">Mehandi ko bhi rachne do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mehandi ko bhi rachne do"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaj na roko yaaron
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaj na roko yaaron"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil khol ke sabko nachne doo..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil khol ke sabko nachne doo.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chacha chachi maama maasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chacha chachi maama maasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saare floor pe aake nachein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saare floor pe aake nachein"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalenge aaj gaaje baaje
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalenge aaj gaaje baaje"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhol vol ko bajne do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhol vol ko bajne do"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Function hai yeh reason hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Function hai yeh reason hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil se dil ki dedication hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil se dil ki dedication hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Photo shoto banti hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Photo shoto banti hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaj pyar ki celebration hai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaj pyar ki celebration hai.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poori huyi ab rasmein saari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poori huyi ab rasmein saari"/>
</div>
<div class="lyrico-lyrics-wrapper">Bajne do shehnaiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajne do shehnaiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saari family ko ji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saari family ko ji"/>
</div>
<div class="lyrico-lyrics-wrapper">Lakh lakh badhaiyaan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lakh lakh badhaiyaan.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaye ni maaye Ranjha tera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaye ni maaye Ranjha tera"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq mein pai gaya jhalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq mein pai gaya jhalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Party karda ghummda phirda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party karda ghummda phirda"/>
</div>
<div class="lyrico-lyrics-wrapper">Jadon si yeh kallaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadon si yeh kallaa.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaye ni maaye Heer teriye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaye ni maaye Heer teriye"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishq mein pai gayi jhalli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq mein pai gayi jhalli"/>
</div>
<div class="lyrico-lyrics-wrapper">Aithe nachde othe nachde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aithe nachde othe nachde"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoke saare talli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoke saare talli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho ladka hai ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ladka hai ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur ladki taiyaar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur ladki taiyaar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Baj gayi hai shaadi wali bell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baj gayi hai shaadi wali bell"/>
</div>
<div class="lyrico-lyrics-wrapper">Hua pyaar hai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hua pyaar hai.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udd gaye kabootar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udd gaye kabootar"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaj wedding wali raat hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaj wedding wali raat hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dost saare naachein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dost saare naachein"/>
</div>
<div class="lyrico-lyrics-wrapper">Apne yaar ki baraat mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apne yaar ki baraat mein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haan ladka hai ready
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haan ladka hai ready"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur ladki taiyaar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur ladki taiyaar hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Baj gayi hai shaadi wali bell
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baj gayi hai shaadi wali bell"/>
</div>
<div class="lyrico-lyrics-wrapper">Hua pyaar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hua pyaar hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udd gaye kabootar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udd gaye kabootar"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaj wedding wali raat hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaj wedding wali raat hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dost saare naachein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dost saare naachein"/>
</div>
<div class="lyrico-lyrics-wrapper">Apne yaar ki baraat mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apne yaar ki baraat mein"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhangra hai pauna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhangra hai pauna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur giddha lagana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur giddha lagana"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo nachna chahta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo nachna chahta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachne doo..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachne doo.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bajne do gaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bajne do gaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Aur khaane do khana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aur khaane do khana"/>
</div>
<div class="lyrico-lyrics-wrapper">jo dil chahta hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jo dil chahta hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Woh hone do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Woh hone do"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Shehnaiyan wajan do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shehnaiyan wajan do"/>
</div>
<div class="lyrico-lyrics-wrapper">Shehnaiyan wajan do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shehnaiyan wajan do"/>
</div>
<div class="lyrico-lyrics-wrapper">Shehnaiyan wajan do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shehnaiyan wajan do"/>
</div>
<div class="lyrico-lyrics-wrapper">Shehnaiyan wajan do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shehnaiyan wajan do"/>
</div>
<div class="lyrico-lyrics-wrapper">Shehnaiyan wajan do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shehnaiyan wajan do"/>
</div>
<div class="lyrico-lyrics-wrapper">Shehnaiyan wajan do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shehnaiyan wajan do"/>
</div>
</pre>
