---
title: "premante shapamani song lyrics"
album: "Prema Janta"
artist: "Nikhilesh Thogari"
lyricist: "Nikhilesh Thogari"
director: "Nikhilesh Thogari"
path: "/albums/prema-janta-lyrics"
song: "Premante Shapamani"
image: ../../images/albumart/prema-janta.jpg
date: 2019-06-28
lang: telugu
youtubeLink: "https://www.youtube.com/embed/IjMFTJ0Jca4"
type: "love"
singers:
  - Sri krishna
  - Anjana sowmya
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">premante shapamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premante shapamani"/>
</div>
<div class="lyrico-lyrics-wrapper">evaruannar antaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaruannar antaga"/>
</div>
<div class="lyrico-lyrics-wrapper">ayya ye pichalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya ye pichalo"/>
</div>
<div class="lyrico-lyrics-wrapper">ani untaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ani untaru"/>
</div>
<div class="lyrico-lyrics-wrapper">premisthe papamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premisthe papamani"/>
</div>
<div class="lyrico-lyrics-wrapper">evaruannar antaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaruannar antaga"/>
</div>
<div class="lyrico-lyrics-wrapper">papam yedo teliyaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="papam yedo teliyaka"/>
</div>
<div class="lyrico-lyrics-wrapper">ani untaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ani untaru"/>
</div>
<div class="lyrico-lyrics-wrapper">mana jantani adagali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana jantani adagali"/>
</div>
<div class="lyrico-lyrics-wrapper">mana premani chudali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana premani chudali"/>
</div>
<div class="lyrico-lyrics-wrapper">preminchani oka janmunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="preminchani oka janmunte"/>
</div>
<div class="lyrico-lyrics-wrapper">adi janmee kaadani antaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi janmee kaadani antaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">premante shapamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premante shapamani"/>
</div>
<div class="lyrico-lyrics-wrapper">evaruannar antaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaruannar antaga"/>
</div>
<div class="lyrico-lyrics-wrapper">ayya ye pichalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya ye pichalo"/>
</div>
<div class="lyrico-lyrics-wrapper">ani untaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ani untaru"/>
</div>
<div class="lyrico-lyrics-wrapper">premisthe papamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premisthe papamani"/>
</div>
<div class="lyrico-lyrics-wrapper">evaruannar antaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaruannar antaga"/>
</div>
<div class="lyrico-lyrics-wrapper">papam yedo teliyaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="papam yedo teliyaka"/>
</div>
<div class="lyrico-lyrics-wrapper">ani untaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ani untaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">agello mojullo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="agello mojullo "/>
</div>
<div class="lyrico-lyrics-wrapper">evaraina premisthare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaraina premisthare"/>
</div>
<div class="lyrico-lyrics-wrapper">premisthe saripodhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premisthe saripodhe"/>
</div>
<div class="lyrico-lyrics-wrapper">kalakalam nilapale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalakalam nilapale"/>
</div>
<div class="lyrico-lyrics-wrapper">chupullo rupullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chupullo rupullo"/>
</div>
<div class="lyrico-lyrics-wrapper">okataithe premantaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okataithe premantaru"/>
</div>
<div class="lyrico-lyrics-wrapper">premante adikadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premante adikadu"/>
</div>
<div class="lyrico-lyrics-wrapper">manasulu kalavali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasulu kalavali"/>
</div>
<div class="lyrico-lyrics-wrapper">parkullo pabbullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parkullo pabbullo"/>
</div>
<div class="lyrico-lyrics-wrapper">aa manasulu kalavavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa manasulu kalavavule"/>
</div>
<div class="lyrico-lyrics-wrapper">nijamaina premallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijamaina premallo"/>
</div>
<div class="lyrico-lyrics-wrapper">ee anandam vere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee anandam vere"/>
</div>
<div class="lyrico-lyrics-wrapper">adi swargamlo raasundale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi swargamlo raasundale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">premante shapamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premante shapamani"/>
</div>
<div class="lyrico-lyrics-wrapper">evaruannar antaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaruannar antaga"/>
</div>
<div class="lyrico-lyrics-wrapper">ayya ye pichalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya ye pichalo"/>
</div>
<div class="lyrico-lyrics-wrapper">ani untaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ani untaru"/>
</div>
<div class="lyrico-lyrics-wrapper">premisthe papamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premisthe papamani"/>
</div>
<div class="lyrico-lyrics-wrapper">evaruannar antaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaruannar antaga"/>
</div>
<div class="lyrico-lyrics-wrapper">papam yedo teliyaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="papam yedo teliyaka"/>
</div>
<div class="lyrico-lyrics-wrapper">ani untaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ani untaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">premante ivvatame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premante ivvatame"/>
</div>
<div class="lyrico-lyrics-wrapper">prethiphalamedi koraduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="prethiphalamedi koraduga"/>
</div>
<div class="lyrico-lyrics-wrapper">preminche vari sutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="preminche vari sutham"/>
</div>
<div class="lyrico-lyrics-wrapper">premaku pranamata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premaku pranamata"/>
</div>
<div class="lyrico-lyrics-wrapper">premante nammakame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premante nammakame"/>
</div>
<div class="lyrico-lyrics-wrapper">anukshanamu adi maraduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anukshanamu adi maraduga"/>
</div>
<div class="lyrico-lyrics-wrapper">premunte jevithame 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premunte jevithame "/>
</div>
<div class="lyrico-lyrics-wrapper">roju pandagata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roju pandagata"/>
</div>
<div class="lyrico-lyrics-wrapper">medallo middello
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="medallo middello"/>
</div>
<div class="lyrico-lyrics-wrapper">ee premalu dorakavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee premalu dorakavule"/>
</div>
<div class="lyrico-lyrics-wrapper">preminche manasulni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="preminche manasulni"/>
</div>
<div class="lyrico-lyrics-wrapper">ee asthulu konaleve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee asthulu konaleve"/>
</div>
<div class="lyrico-lyrics-wrapper">adi swargamlo raasundale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi swargamlo raasundale"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalikal pavamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalikal pavamani"/>
</div>
<div class="lyrico-lyrics-wrapper">yarusonna rendrare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarusonna rendrare"/>
</div>
<div class="lyrico-lyrics-wrapper">hayyo adi mudthanadane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hayyo adi mudthanadane"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalaro sabamane 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalaro sabamane "/>
</div>
<div class="lyrico-lyrics-wrapper">yarosonna rendrare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yarosonna rendrare"/>
</div>
<div class="lyrico-lyrics-wrapper">pavam avanaasa illa kolai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pavam avanaasa illa kolai"/>
</div>
<div class="lyrico-lyrics-wrapper">mana jantani adagali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana jantani adagali"/>
</div>
<div class="lyrico-lyrics-wrapper">mana premani chudali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mana premani chudali"/>
</div>
<div class="lyrico-lyrics-wrapper">preminchani oka janmunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="preminchani oka janmunte"/>
</div>
<div class="lyrico-lyrics-wrapper">adi janmee kaadani antaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi janmee kaadani antaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">premante shapamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="premante shapamani"/>
</div>
<div class="lyrico-lyrics-wrapper">evaruannar antaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evaruannar antaga"/>
</div>
<div class="lyrico-lyrics-wrapper">ayya ye pichalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ayya ye pichalo"/>
</div>
<div class="lyrico-lyrics-wrapper">ani untaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ani untaru"/>
</div>
</pre>
