---
title: "komma veedi song lyrics"
album: "Jaanu"
artist: "Govind Vasantha"
lyricist: "Sri Mani"
director: "C. Prem Kumar"
path: "/albums/jaanu-lyrics"
song: "Komma Veedi"
image: ../../images/albumart/jaanu.jpg
date: 2020-02-07
lang: telugu
youtubeLink: "https://www.youtube.com/embed/-tu7PSrS2aI"
type: "sad"
singers:
  - Chinmayi
  - Govind Vasantha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Komma Veedi Guvve Veluthondile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Komma Veedi Guvve Veluthondile"/>
</div>
<div class="lyrico-lyrics-wrapper">Puvvu Kanta Neere Kurise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puvvu Kanta Neere Kurise"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma Odi Veede Pasipaapala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma Odi Veede Pasipaapala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekki Vekki Manase Thadise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekki Vekki Manase Thadise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chadive Badike Vesavi Selavulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chadive Badike Vesavi Selavulaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirigi Gudike Raavali Nuvvilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirigi Gudike Raavali Nuvvilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Okkapoota Nijamai Mana Kalalu Ila Aa Aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkapoota Nijamai Mana Kalalu Ila Aa Aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundarunna Kaalam Gadichedi Elaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundarunna Kaalam Gadichedi Elaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Brathuke Gathamai Ee Chotaa Aagelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Brathuke Gathamai Ee Chotaa Aagelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Veedi Choope Veluthodile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Veedi Choope Veluthodile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanta Neeru Thudiche-Devare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanta Neeru Thudiche-Devare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chirunavvule Ika Nannne Vidichenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvule Ika Nannne Vidichenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu Viduvani Ye Nanno Vethikenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu Viduvani Ye Nanno Vethikenule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chiguraashale Ika Shwase Nilipenule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiguraashale Ika Shwase Nilipenule"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Oosule Jathaleka Edabaasele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Oosule Jathaleka Edabaasele"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Nunchi Ninne Vidadheeseti Vidhinainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Nunchi Ninne Vidadheeseti Vidhinainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedhinchi Odinche Inko Janme Varame-Varame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedhinchi Odinche Inko Janme Varame-Varame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam Manam Chero Sagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Manam Chero Sagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chero Dishalle Maarinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chero Dishalle Maarinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oke Swaram Ekaaksharam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oke Swaram Ekaaksharam"/>
</div>
<div class="lyrico-lyrics-wrapper">Chero Padhamlo Cherinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chero Padhamlo Cherinaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvunna Vaipu Thappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvunna Vaipu Thappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopu Thappu Dishanu Choopunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopu Thappu Dishanu Choopunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugulanni Manamu Kalisi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugulanni Manamu Kalisi "/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Dhaari Vidichenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Dhaari Vidichenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maree Maree Ninnadagamandhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maree Maree Ninnadagamandhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Gnaapakaala Uppenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gnaapakaala Uppenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chiraayuvedho Oopirai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chiraayuvedho Oopirai "/>
</div>
<div class="lyrico-lyrics-wrapper">Neekosameduru Choopu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekosameduru Choopu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kavithale Raase Neekai Malli Raaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavithale Raase Neekai Malli Raaa"/>
</div>
</pre>
