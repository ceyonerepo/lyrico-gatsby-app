---
title: "sundari song lyrics"
album: "Kannathil Muthamittal"
artist: "A. R. Rahman"
lyricist: "Vairamuthu"
director: "Mani Ratnam"
path: "/albums/kannathil-muthamittal-song-lyrics"
song: "Sundari Siriya Rettai Vaal"
image: ../../images/albumart/kannathil-muthamittal.jpg
date: 2002-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8ZlIC9pgP0w"
type: "happy"
singers:
  - Hariharan
  - Tippu
  - Sujatha Mohan
  - Karthik
  - Srimathumitha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aanikondu Megaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanikondu Megaththai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikka Mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikka Mudiyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amudhavai Pooti Kulley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amudhavai Pooti Kulley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaikka Mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaikka Mudiyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundari Siriya Rettai Vaal Sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundari Siriya Rettai Vaal Sundari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundari Siriya Rettai Vaal Sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundari Siriya Rettai Vaal Sundari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nacharikum Chittu Kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nacharikum Chittu Kuruvi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rekkai Katti parakum Aruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkai Katti parakum Aruvi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundari Siriya Rettai Vaal Sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundari Siriya Rettai Vaal Sundari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundari Siriya Rettai Vaal Sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundari Siriya Rettai Vaal Sundari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallai Kooda Kanniya Vaiththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallai Kooda Kanniya Vaiththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallai Kaatti Sirrikka Vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallai Kaatti Sirrikka Vaikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanikondu Megaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanikondu Megaththai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikka Mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikka Mudiyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amudhavai Pooti Kulley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amudhavai Pooti Kulley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaikka Mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaikka Mudiyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanikondu Megaththai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanikondu Megaththai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adikka Mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikka Mudiyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amudhavai Pooti Kulley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amudhavai Pooti Kulley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adaikka Mudiyuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaikka Mudiyuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chinna Chinna Kurumbugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna Chinna Kurumbugal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thittamittu Purigiral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thittamittu Purigiral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pongi Varum Kovathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pongi Varum Kovathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Punnagaiyil Thodakiral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Punnagaiyil Thodakiral"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna Kuzhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna Kuzhiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalai Pudhaipal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalai Pudhaipal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jadayil Aagayam Ilupaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jadayil Aagayam Ilupaal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbangallin Ellayum Avaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbangallin Ellayum Avaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thollaitharum Pillaiyum Avaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollaitharum Pillaiyum Avaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Narumana Thendralum Avaldhaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narumana Thendralum Avaldhaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">AlaiyaPidiththu Kayiril Kayiril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AlaiyaPidiththu Kayiril Kayiril"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kattuvathu Nadakkamudindha Seyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattuvathu Nadakkamudindha Seyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivalum Kooda Aada Pirandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivalum Kooda Aada Pirandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Alaiyallavaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaiyallavaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundari Siriya Rettai Vaal Sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundari Siriya Rettai Vaal Sundari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundari Siriya Rettai Vaal Sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundari Siriya Rettai Vaal Sundari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey..Nacharikum Chittu Kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey..Nacharikum Chittu Kuruvi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey..Rekkai Katti parakum Aruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey..Rekkai Katti parakum Aruvi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pal Mulaitha Pattampoochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pal Mulaitha Pattampoochi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannaththai Kadikumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaththai Kadikumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasa Thodu Muththam Thandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasa Thodu Muththam Thandhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parisum Kodukumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parisum Kodukumey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annai Annai Avalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai Annai Avalukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annai Kooda Ivalthaaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annai Kooda Ivalthaaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magalendru Vaiththirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magalendru Vaiththirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mamiyarum Ivaldhaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamiyarum Ivaldhaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palli Vagupill Villi Ivaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palli Vagupill Villi Ivaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padipil Heroine Ivalley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padipil Heroine Ivalley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aayiram Kelvigal yerival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayiram Kelvigal yerival"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Mattum Vidaigalai Arival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Mattum Vidaigalai Arival"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Teacherukku Veetil Vagupedupal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teacherukku Veetil Vagupedupal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivallai Naalai Manakkapogum Asadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivallai Naalai Manakkapogum Asadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Paadu Paduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Paadu Paduvaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ival Paadham Kazhuvum Neeril Samayal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ival Paadham Kazhuvum Neeril Samayal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seyivanoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seyivanoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">No No No No No
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No No No No No"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundari Siriya Rettai Vaal Sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundari Siriya Rettai Vaal Sundari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sundari Siriya Rettai Vaal Sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundari Siriya Rettai Vaal Sundari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallai Kooda Kanniya Vaiththu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallai Kooda Kanniya Vaiththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pallai Kaatti Sirrikka Vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallai Kaatti Sirrikka Vaikkum"/>
</div>
</pre>
