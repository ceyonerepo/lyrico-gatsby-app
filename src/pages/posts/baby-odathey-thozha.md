---
title: "baby odathey song lyrics"
album: "Thozha"
artist: "Gopi Sundar"
lyricist: "Madhan Karky"
director: "Vamsi Paidipally"
path: "/albums/thozha-lyrics"
song: "Baby Odathey"
image: ../../images/albumart/thozha.jpg
date: 2016-03-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/SJm8Xl-vZyg"
type: "Enjoy"
singers:
  - Shanker Mahadevan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Baby Odadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Odadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Nikkadhe Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Nikkadhe Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Ennoda Ippave Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Ennoda Ippave Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Baby Oho Pesadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Baby Oho Pesadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Oho Thoongadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Oho Thoongadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Ennoda Ippove Siri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Ennoda Ippove Siri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salikaadha Saalai Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salikaadha Saalai Oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edukkaadha Vegam Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukkaadha Vegam Oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhukkaana Paatu Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhukkaana Paatu Oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namakkaga Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkaga Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Odathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Odathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Nikkadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Nikkadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Ennoda Ippove
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Ennoda Ippove"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pinnadi Ullatha Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pinnadi Ullatha Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi Kaattum Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi Kaattum Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnadi Ullathelaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnadi Ullathelaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Kan Kaattume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kan Kaattume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seatukku Belt Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seatukku Belt Irukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heartukku Illa Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heartukku Illa Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pichittu Paanjidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichittu Paanjidume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pachai Manjal Sivappu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pachai Manjal Sivappu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ellam Illa High Way My Way
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellam Illa High Way My Way"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poga Koodatha Paadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Koodatha Paadhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnum Ingillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum Ingillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeh Thappu Senja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh Thappu Senja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">U Turn Edu Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="U Turn Edu Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Salikaadha Saalai Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salikaadha Saalai Oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edukkaadha Vegam Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edukkaadha Vegam Oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhukkaana Paatu Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhukkaana Paatu Oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Namakkaaga Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namakkaaga Thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Odathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Odathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Nikkaathey Baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Nikkaathey Baby"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby Ennoda Ippave Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby Ennoda Ippave Vaa Vaa"/>
</div>
</pre>
