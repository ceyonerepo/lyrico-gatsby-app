---
title: "orey oor song lyrics"
album: "Andhra Mess"
artist: "Prashant Pillai"
lyricist: "Kutty Revathi - Mohanraj"
director: "Jai"
path: "/albums/andhra-mess-lyrics"
song: "Orey Oor"
image: ../../images/albumart/andhra-mess.jpg
date: 2018-06-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OBI2CNmiN1w"
type: "melody"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">orey oor muthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orey oor muthale"/>
</div>
<div class="lyrico-lyrics-wrapper">orey oor mudive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orey oor mudive"/>
</div>
<div class="lyrico-lyrics-wrapper">orey oor muthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orey oor muthale"/>
</div>
<div class="lyrico-lyrics-wrapper">orey oor mudive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orey oor mudive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">illatha ondrai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illatha ondrai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalgal odi oyume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalgal odi oyume"/>
</div>
<div class="lyrico-lyrics-wrapper">pollatha unmai ondrai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pollatha unmai ondrai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam kandu sollume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam kandu sollume"/>
</div>
<div class="lyrico-lyrics-wrapper">nilai endru ethum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilai endru ethum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam gnanam othume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam gnanam othume"/>
</div>
<div class="lyrico-lyrics-wrapper">paravaiyin siragaai maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravaiyin siragaai maari"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrum thisaiyai kaatume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrum thisaiyai kaatume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">orey oor muthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orey oor muthale"/>
</div>
<div class="lyrico-lyrics-wrapper">orey oor mudive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orey oor mudive"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sari endrum thavaru endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari endrum thavaru endrum"/>
</div>
<div class="lyrico-lyrics-wrapper">sari endrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari endrum "/>
</div>
<div class="lyrico-lyrics-wrapper">sari endrum ethum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sari endrum ethum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">thavaru endrum ethum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavaru endrum ethum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">ithu kadavul sarukiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu kadavul sarukiya"/>
</div>
<div class="lyrico-lyrics-wrapper">valukkup paathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valukkup paathaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">athu naalum maarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu naalum maarume"/>
</div>
<div class="lyrico-lyrics-wrapper">vana vesam podume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vana vesam podume"/>
</div>
<div class="lyrico-lyrics-wrapper">un pokile poi vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pokile poi vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">yaavum niyaayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaavum niyaayame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">orey oor muthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orey oor muthale"/>
</div>
<div class="lyrico-lyrics-wrapper">orey oor mudive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orey oor mudive"/>
</div>
<div class="lyrico-lyrics-wrapper">orey oor muthale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orey oor muthale"/>
</div>
<div class="lyrico-lyrics-wrapper">orey oor mudive
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orey oor mudive"/>
</div>
</pre>
