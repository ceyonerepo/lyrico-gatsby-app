---
title: "enna ithuvo song lyrics"
album: "Aanandham"
artist: "S. A. Rajkumar"
lyricist: "Viveka"
director: "N. Lingusamy"
path: "/albums/aanandham-lyrics"
song: "Enna Ithuvo"
image: ../../images/albumart/aanandham.jpg
date: 2001-05-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/fScGTPLuQPE"
type: "love"
singers:
  - Hariharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Enna Idhuvo Ennai Chutriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Idhuvo Ennai Chutriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai Oli Vattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai Oli Vattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Mayangi Konjam Paduththaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Mayangi Konjam Paduththaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil Oru Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil Oru Saththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netru Paarthen Nilaa Mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Paarthen Nilaa Mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotru Ponen Yedho Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotru Ponen Yedho Sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yae Thendral Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae Thendral Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Kaadhal Dhaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Kaadhal Dhaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Kangalodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Kangalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Modhal Dhaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Modhal Dhaanadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Idhuvo Ennai Chutriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Idhuvo Ennai Chutriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai Oli Vattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai Oli Vattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Mayangi Konjam Paduththaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Mayangi Konjam Paduththaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil Oru Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil Oru Saththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhalae Vaazhkkaiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalae Vaazhkkaiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedham Endru Aanadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedham Endru Aanadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangalaal Swaasikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangalaal Swaasikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Katru Thandhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katru Thandhadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Boomiyae Suzhalvadhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyae Suzhalvadhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallikkoodam Sonnadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallikkoodam Sonnadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru Dhaan En Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru Dhaan En Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yetrukkondadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yetrukkondadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho Kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Kaadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thalaiyanai Nee Yena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thalaiyanai Nee Yena"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaiththu Kolven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaiththu Kolven"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Naan Thoonginaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Naan Thoonginaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Dhinam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Dhinam Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarbudan Anaiththu Kolven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbudan Anaiththu Kolven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodai Kaala Poongaatraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodai Kaala Poongaatraai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan Vaazhvil Veesuvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan Vaazhvil Veesuvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Idhuvo Ennai Chutriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Idhuvo Ennai Chutriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai Oli Vattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai Oli Vattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Mayangi Konjam Paduththaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Mayangi Konjam Paduththaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil Oru Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil Oru Saththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puththagam Purattinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puththagam Purattinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam Engum Unn Mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam Engum Unn Mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyil Vaazhvadhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyil Vaazhvadhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai Nyaabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Nyaabagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koyilin Vaasalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyilin Vaasalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Seruppai Theduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Seruppai Theduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandadhum Nodiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandadhum Nodiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Bakthan Aaguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bakthan Aaguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oho Kaadhali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oho Kaadhali"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nazhuviya Kaikkuttai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nazhuviya Kaikkuttai"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeduppadhupol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeduppadhupol"/>
</div>
<div class="lyrico-lyrics-wrapper">Saalai Oramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saalai Oramaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nadappadhai Kunindhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nadappadhai Kunindhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Rasiththiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Rasiththiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Paarkkum Naal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarkkum Naal Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaasa Kaatru Thevaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaasa Kaatru Thevaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna Idhuvo Ennai Chutriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Idhuvo Ennai Chutriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhaai Oli Vattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhaai Oli Vattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal Mayangi Konjam Paduththaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Mayangi Konjam Paduththaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavil Oru Saththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil Oru Saththam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netru Paarthen Nilaa Mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netru Paarthen Nilaa Mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thotru Ponen Yedho Sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thotru Ponen Yedho Sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Yae Thendral Pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yae Thendral Pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu Kaadhal Dhaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Kaadhal Dhaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Kangalodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Kangalodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Modhal Dhaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Modhal Dhaanadi"/>
</div>
</pre>
