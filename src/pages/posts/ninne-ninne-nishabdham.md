---
title: "ninne ninne song lyrics"
album: "Nishabdham"
artist: "Gopi Sundar"
lyricist: "Bhaskarabhatla"
director: "Hemant Madhukar"
path: "/albums/nishabdham-lyrics"
song: "Ninne Ninne"
image: ../../images/albumart/nishabdham.jpg
date: 2020-10-02
lang: telugu
youtubeLink: "https://www.youtube.com/embed/uH9T500Hk7c"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ninne ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulalo nimpukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulalo nimpukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Ninne ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Ninne ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulo ompukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulo ompukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aksharalake andhananathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aksharalake andhananathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Premalekalenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premalekalenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaduvukunte nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaduvukunte nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooga saigalo nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooga saigalo nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde lopale goodu kattina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde lopale goodu kattina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipi oosulenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipi oosulenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Telusukunte nee chethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telusukunte nee chethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sparshalo nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sparshalo nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Meghalaki nichena vestunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meghalaki nichena vestunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Chooputho sannaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chooputho sannaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Guchesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guchesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Rainbow ke rangulu poosthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rainbow ke rangulu poosthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Smileutho symphony vinipisthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smileutho symphony vinipisthunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeey Ninne ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeey Ninne ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulalo nimpukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulalo nimpukunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nidhuratho yem pani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nidhuratho yem pani"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuruga nuvvunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuruga nuvvunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Swasatho asalem pani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swasatho asalem pani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee dhyasalo unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee dhyasalo unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Na kallalo oka adugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na kallalo oka adugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Na gundello oka adugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na gundello oka adugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhanga pettesave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhanga pettesave"/>
</div>
<div class="lyrico-lyrics-wrapper">Yem kavalo adugu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yem kavalo adugu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai valapu nadhilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai valapu nadhilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Janta alalai thirugudham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janta alalai thirugudham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meghalaki nichena vestunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meghalaki nichena vestunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Chooputho sannaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chooputho sannaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Guchesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guchesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Rainbow ke rangulu poosthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rainbow ke rangulu poosthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Smileutho symphony vinipisthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smileutho symphony vinipisthunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninne ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulalo nimpukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulalo nimpukunna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pootako ruthuvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pootako ruthuvula"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaratha neekosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaratha neekosam"/>
</div>
<div class="lyrico-lyrics-wrapper">jabilai veliginchava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jabilai veliginchava"/>
</div>
<div class="lyrico-lyrics-wrapper">Na prema aakasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na prema aakasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvventha istam ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvventha istam ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhant kolamanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhant kolamanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Na pranam vere undha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na pranam vere undha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvega na pranam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvega na pranam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bagundhe okari kalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bagundhe okari kalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Okaru dhoori pilavadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okaru dhoori pilavadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninne ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulalo nimpukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulalo nimpukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoo Ninne ninne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoo Ninne ninne"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulo ompukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulo ompukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aksharalake andhananathala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aksharalake andhananathala"/>
</div>
<div class="lyrico-lyrics-wrapper">Premalekalenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premalekalenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaduvukunte nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaduvukunte nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Mooga saigalo nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mooga saigalo nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde lopale goodu kattina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde lopale goodu kattina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipi oosulenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipi oosulenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Telusukunte nee chethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telusukunte nee chethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sparshalo nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sparshalo nene"/>
</div>
<div class="lyrico-lyrics-wrapper">Meghalaki nichena vestunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meghalaki nichena vestunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Chooputho sannaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chooputho sannaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Guchesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guchesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Rainbow ke rangulu poosthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rainbow ke rangulu poosthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Smileutho symphony vinipisthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smileutho symphony vinipisthunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meghalaki nichena vestunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meghalaki nichena vestunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Chooputho sannaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chooputho sannaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Guchesthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guchesthunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Rainbow ke rangulu poosthunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rainbow ke rangulu poosthunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Smileutho symphony vinipisthunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Smileutho symphony vinipisthunte"/>
</div>
</pre>
