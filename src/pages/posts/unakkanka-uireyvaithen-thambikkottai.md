---
title: "unakkanka uireyvaithen song lyrics"
album: "Thambikkottai"
artist: "D. Imman"
lyricist: "Viveka"
director: "R.Rahesh"
path: "/albums/thambikkottai-lyrics"
song: "Unakkanka Uireyvaithen"
image: ../../images/albumart/thambikkottai.jpg
date: 2011-02-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oppyIJ9HdSQ"
type: "love"
singers:
  - Aalap Raju
  - Sriram Parthasarathy
  - Shweta Mohan
  - Widodo
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unakkaaga uyirai vaithen eduththukkolladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga uyirai vaithen eduththukkolladi"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirukkul meththaipoattu paduththukkolladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirukkul meththaipoattu paduththukkolladi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkaaga uyirai vaithen eduththukkolladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga uyirai vaithen eduththukkolladi"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirukkul meththaipoattu paduththukkolladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirukkul meththaipoattu paduththukkolladi"/>
</div>
<div class="lyrico-lyrics-wrapper">mulvelippoattaal kaatril thaan mullum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mulvelippoattaal kaatril thaan mullum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalthaandi poanaalum kaadhal serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalthaandi poanaalum kaadhal serum"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan kangal enadhey illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan kangal enadhey illai"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkulley idhayam illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkulley idhayam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirthavira ondrum illai O
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirthavira ondrum illai O"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkaaga uyiraivaithen eduththukkollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkaaga uyiraivaithen eduththukkollada"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirukkul meththaipoattu paduththukkollada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirukkul meththaipoattu paduththukkollada"/>
</div>
<div class="lyrico-lyrics-wrapper">paadhaigal illaamal payanam poanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paadhaigal illaamal payanam poanen"/>
</div>
<div class="lyrico-lyrics-wrapper">mannoadu pudhaiyaadha veraai aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannoadu pudhaiyaadha veraai aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovin vaasam poagumboadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovin vaasam poagumboadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan vaasam nenjil thoandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan vaasam nenjil thoandum"/>
</div>
<div class="lyrico-lyrics-wrapper">naano indru yedho aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naano indru yedho aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho ho ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ho ho"/>
</div>
</pre>
