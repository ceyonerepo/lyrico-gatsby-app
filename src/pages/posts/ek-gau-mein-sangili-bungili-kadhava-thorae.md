---
title: "ek gau mein song lyrics"
album: "Sangili Bungili Kadhava Thorae"
artist: "Vishal Chandrasekhar"
lyricist: "Pa Vijay"
director: "Ike"
path: "/albums/sangili-bungili-kadhava-thorae-lyrics"
song: "Ek Gau Mein"
image: ../../images/albumart/sangili-bungili-kadhava-thorae.jpg
date: 2017-05-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IvRa-WDLE9A"
type: "happy"
singers:
  -	Silambarasan
  - M M Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ek gavumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek gavumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek gavumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek gavumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek kisaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek kisaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ek kisaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek kisaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Raha tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raha tha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragu tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragu tha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragu ragu raha tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragu ragu raha tha tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ek gavumei ek kisaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek gavumei ek kisaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragu thatha hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragu thatha hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathukitta moththa vithayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathukitta moththa vithayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Erakittaagha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erakittaagha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ek gavumei ek kisaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ek gavumei ek kisaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragu thatha hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragu thatha hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathukitta moththa vithayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathukitta moththa vithayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Erakittaagha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erakittaagha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Touch-u panna sketch-u potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch-u panna sketch-u potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Metchuduveenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Metchuduveenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Natchunoru itchu ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natchunoru itchu ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pitchuruveenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pitchuruveenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaai pella kutty sola ki roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaai pella kutty sola ki roja"/>
</div>
<div class="lyrico-lyrics-wrapper">Manja udadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja udadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee makkar pannadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee makkar pannadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manja udadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manja udadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee makkar pannadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee makkar pannadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragu tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragu tha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragu ku ku ku thatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragu ku ku ku thatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragu tha tha tha tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragu tha tha tha tha tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haiyyo pavam thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyyo pavam thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Bulb-u vaanga thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulb-u vaanga thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pethu pottangalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pethu pottangalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Daddy mummy veena thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daddy mummy veena thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Beep pottukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beep pottukko"/>
</div>
<div class="lyrico-lyrics-wrapper">Fill-up pannikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fill-up pannikko"/>
</div>
<div class="lyrico-lyrics-wrapper">Artham yaarum ketta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Artham yaarum ketta"/>
</div>
<div class="lyrico-lyrics-wrapper">Adithaan othai thaan sari thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adithaan othai thaan sari thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeppa paarthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppa paarthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paattula thittureenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattula thittureenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenga ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenga ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen ya suthi vareenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen ya suthi vareenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Make-up illama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make-up illama"/>
</div>
<div class="lyrico-lyrics-wrapper">Theruvula neenga nadantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theruvula neenga nadantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pei kootam kelambi varuthunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pei kootam kelambi varuthunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Utcha poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utcha poranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thambi utcha poranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thambi utcha poranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokorra koko ko ko ko ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokorra koko ko ko ko ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kokorra koko ko ko ko ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokorra koko ko ko ko ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragu tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragu tha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragu ku ku ku thatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragu ku ku ku thatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragu tha tha tha tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragu tha tha tha tha tha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jambu lingamae jada thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jambu lingamae jada thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Jothi lingamae arrogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jothi lingamae arrogara"/>
</div>
<div class="lyrico-lyrics-wrapper">Jambu lingamae jada thara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jambu lingamae jada thara"/>
</div>
<div class="lyrico-lyrics-wrapper">Jothi lingamae arrogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jothi lingamae arrogara"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaayu lingamae ada buda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaayu lingamae ada buda"/>
</div>
<div class="lyrico-lyrics-wrapper">Panja lingamae mada pada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panja lingamae mada pada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Google search-u la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Google search-u la"/>
</div>
<div class="lyrico-lyrics-wrapper">Un per sonnalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un per sonnalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulaga fraudu- nu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulaga fraudu- nu than"/>
</div>
<div class="lyrico-lyrics-wrapper">Artham varuthu thannalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Artham varuthu thannalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Newton mavalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Newton mavalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokka sonneeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokka sonneeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Wrong-u route-il poi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wrong-u route-il poi"/>
</div>
<div class="lyrico-lyrics-wrapper">Rightta onnu seivom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rightta onnu seivom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Putham puthusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Putham puthusa"/>
</div>
<div class="lyrico-lyrics-wrapper">Route-u pudikuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Route-u pudikuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam vittalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam vittalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga inga idikuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga inga idikuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanna kuzhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanna kuzhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kerchief pottu vechen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kerchief pottu vechen"/>
</div>
<div class="lyrico-lyrics-wrapper">Okey na onnae onnu kudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okey na onnae onnu kudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Umma thariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Umma thariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu summa thariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu summa thariya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokorra koko ko ko ko ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokorra koko ko ko ko ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kokorra koko ko ko ko ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokorra koko ko ko ko ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kokorra koko ko ko ko ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokorra koko ko ko ko ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Kokorra koko ko ko ko ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kokorra koko ko ko ko ko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragu tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragu tha tha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragu ku ku ku thatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragu ku ku ku thatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragu tha tha tha tha tha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragu tha tha tha tha tha"/>
</div>
</pre>
