---
title: "samara shankham song lyrics"
album: "Yatra"
artist: "K"
lyricist: "Sirivennela Sitaramasastri"
director: "Mahi V Raghav"
path: "/albums/yatra-lyrics"
song: "Samara Shankham"
image: ../../images/albumart/yatra.jpg
date: 2019-02-08
lang: telugu
youtubeLink: "https://www.youtube.com/embed/5CVk6AKp5Yc"
type: "mass"
singers:
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee kanulalo kolimai ragile kaledho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kanulalo kolimai ragile kaledho"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijamai telavaraanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijamai telavaraanee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vethikee velugai raanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vethikee velugai raanee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eenati ee suprabatha geetham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eenati ee suprabatha geetham"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekidhe annadhi swaagatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekidhe annadhi swaagatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee sandhelo swarna varna chithram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee sandhelo swarna varna chithram"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopadha alladhe cheranunna lakshyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopadha alladhe cheranunna lakshyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkado paina ldhu yudhamannadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkado paina ldhu yudhamannadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Antharangame kadhanarangamannadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antharangame kadhanarangamannadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Praaname baanamalle tharumuthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praaname baanamalle tharumuthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu neeve jayinchi raara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu neeve jayinchi raara "/>
</div>
<div class="lyrico-lyrics-wrapper">raajashekharaa antunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raajashekharaa antunna "/>
</div>
<div class="lyrico-lyrics-wrapper">nee manasulo mandutendalaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee manasulo mandutendalaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippule cheragani nischayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippule cheragani nischayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gundelo manchu kondalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gundelo manchu kondalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithyamoo nilavanee nammakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithyamoo nilavanee nammakam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vasudhake vandhanam cheyyakundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasudhake vandhanam cheyyakundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi paiki yeguruthunda gelupu jenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi paiki yeguruthunda gelupu jenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashayam netthurai pongakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashayam netthurai pongakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaasaloni samara shankhamaaguthundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaasaloni samara shankhamaaguthundhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eenati ee suprabatha geetham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eenati ee suprabatha geetham"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekidhe annadhi swaagatham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekidhe annadhi swaagatham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee sandhelo swarna varna chithram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee sandhelo swarna varna chithram"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopadha alladhe cheranunna lakshyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopadha alladhe cheranunna lakshyam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkado paina ldhu yudhamannadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkado paina ldhu yudhamannadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Antharangame kadhanarangamannadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antharangame kadhanarangamannadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Praaname baanamalle tharumuthunnadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Praaname baanamalle tharumuthunnadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu neeve jayinchi raara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu neeve jayinchi raara "/>
</div>
<div class="lyrico-lyrics-wrapper">raajashekharaa antunna 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raajashekharaa antunna "/>
</div>
<div class="lyrico-lyrics-wrapper">nee manasulo mandutendalaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee manasulo mandutendalaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippule cheragani nischayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippule cheragani nischayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gundelo manchu kondalaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gundelo manchu kondalaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nithyamoo nilavanee nammakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nithyamoo nilavanee nammakam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vasudhake vandhanam cheyyakundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasudhake vandhanam cheyyakundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi paiki yeguruthunda gelupu jenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi paiki yeguruthunda gelupu jenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashayam netthurai pongakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashayam netthurai pongakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Swaasaloni samara shankhamaaguthundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaasaloni samara shankhamaaguthundhaa"/>
</div>
</pre>
