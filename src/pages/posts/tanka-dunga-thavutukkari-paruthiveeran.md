---
title: "tanka dunga thavutukkari song lyrics"
album: "Paruthiveeran"
artist: "Yuvan Shankar Raja"
lyricist: "Snehan"
director: "Ameer"
path: "/albums/paruthiveeran-lyrics"
song: "Tanka Dunga Thavutukkari"
image: ../../images/albumart/paruthiveeran.jpg
date: 2007-02-23
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7eIXh0gurvc"
type: "celebration"
singers:
  - Kala
  - Lakshmi
  - Pandi
  - Raja
  - Madurai S
  - Saroja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaigai srirangaapurathai serndha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaigai srirangaapurathai serndha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyilaatta kuzhuvinargal engirundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyilaatta kuzhuvinargal engirundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koyil munnbaaga amaindhirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyil munnbaaga amaindhirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandhalukku varumaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhalukku varumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaazhmaiyudan kettukkolgirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaazhmaiyudan kettukkolgirom"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduthpadiyaaga kalkuruchiyai serndha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduthpadiyaaga kalkuruchiyai serndha"/>
</div>
<div class="lyrico-lyrics-wrapper">Naiyaandi melathaarargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naiyaandi melathaarargal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhuvinargal engirundhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhuvinargal engirundhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vegu seekkiramaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vegu seekkiramaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">koyilukku munnbaaga amaindhirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koyilukku munnbaaga amaindhirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandhalukku varumaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhalukku varumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Miga thaazhmaiyudan kettukkolgirom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miga thaazhmaiyudan kettukkolgirom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei danga dunga thavuttukkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei danga dunga thavuttukkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Manga sunga mavusukkaariii..aan…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manga sunga mavusukkaariii..aan…"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei danga dunga thavuttukkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei danga dunga thavuttukkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Manga sunga mavusukkaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manga sunga mavusukkaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Madurai town-ukkullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madurai town-ukkullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen mavusa konjam keladi pullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen mavusa konjam keladi pullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Madurai town-ukkullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madurai town-ukkullae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen mavusa konjam keladi pullae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen mavusa konjam keladi pullae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei allikeerai mallikeerai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei allikeerai mallikeerai"/>
</div>
<div class="lyrico-lyrics-wrapper">Achchambaakkam agathikeerai…. aeee…..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchambaakkam agathikeerai…. aeee….."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppadi eppadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi eppadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei allikeerai mallikeerai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei allikeerai mallikeerai"/>
</div>
<div class="lyrico-lyrics-wrapper">Achchambaakkam agathikeerai….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achchambaakkam agathikeerai…."/>
</div>
<div class="lyrico-lyrics-wrapper">Vakkanaiya… unakku vakkanaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vakkanaiya… unakku vakkanaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Samachchu vaikka…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samachchu vaikka…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appadiyaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appadiyaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennaiya pakkuvamaa paakkanunga…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiya pakkuvamaa paakkanunga…"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaannnn…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaannnn…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada vakkanaiya… ada vakkanaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada vakkanaiya… ada vakkanaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Samachchu vaikka…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samachchu vaikka…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaiya pakkuvamaa paakkanunga…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaiya pakkuvamaa paakkanunga…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akka magalae sornam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka magalae sornam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enn pakkam neeyum varanum…. mm..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn pakkam neeyum varanum…. mm.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhiruvoooom….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhiruvoooom…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Akka magalae sornam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akka magalae sornam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enn pakkam neeyum varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enn pakkam neeyum varanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaman magalae mayilae mayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman magalae mayilae mayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Machchaanukku halo solludi oyilae oyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machchaanukku halo solludi oyilae oyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaman magalae mayilae mayilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaman magalae mayilae mayilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Machchaanukku halo solludi oyilae oyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machchaanukku halo solludi oyilae oyilae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna… engilipish-u thookkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna… engilipish-u thookkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hann… ellaam unnaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hann… ellaam unnaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaththathukku appuranthaen….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaththathukku appuranthaen…."/>
</div>
<div class="lyrico-lyrics-wrapper">Melaiyum keezhaiyum yeruthu eranguthu…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Melaiyum keezhaiyum yeruthu eranguthu…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennathu…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennathu…"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochchuthaan…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochchuthaan…"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaththu machchaan…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaththu machchaan…"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochchu soodaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochchu soodaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Munthi paththikittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munthi paththikittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhi sirichchirum…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhi sirichchirum…"/>
</div>
<div class="lyrico-lyrics-wrapper">Unga love-sa ellaam orangattittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga love-sa ellaam orangattittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthirukkiravangalukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthirukkiravangalukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru vanakkam sollunga…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vanakkam sollunga…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayya vaarum sabaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya vaarum sabaiyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasila periyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasila periyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarum sabaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarum sabaiyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasila periyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasila periyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayya koodum sabaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya koodum sabaiyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunaththula periyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunaththula periyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodum sabaiyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodum sabaiyorae"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunaththula periyorae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunaththula periyorae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanthanaminna vanthanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthanaminna vanthanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vantha sanamellaam kundhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vantha sanamellaam kundhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthanaminna vanthanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthanaminna vanthanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga vantha sanamellaam kundhanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga vantha sanamellaam kundhanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumbozhuthu vaangi vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumbozhuthu vaangi vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakku naru santhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakku naru santhanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varumbozhuthu vaangi vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varumbozhuthu vaangi vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Manakku naru santhanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manakku naru santhanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha santhanaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha santhanaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Poosunga neenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poosunga neenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosamaa kelunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosamaa kelunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhanaththa poosunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhanaththa poosunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga santhosamaa kelunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga santhosamaa kelunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye veththalathaanae podunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye veththalathaanae podunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Visiri irunthaa veesunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visiri irunthaa veesunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Veththalathaanae podunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veththalathaanae podunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Visiri irunthaa veesunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Visiri irunthaa veesunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayya veththalanna veththala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya veththalanna veththala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu veruganooru veththala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu veruganooru veththala"/>
</div>
<div class="lyrico-lyrics-wrapper">Veththalanna veththala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veththalanna veththala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithu veruganooru veththala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithu veruganooru veththala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Atha vaangi vanthae oththaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha vaangi vanthae oththaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaikku kooda paththala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaikku kooda paththala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaangi vanthae oththaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi vanthae oththaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaikku kooda paththala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaikku kooda paththala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ayya neenga peththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayya neenga peththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillai naanunga…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillai naanunga…"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma neenga peththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma neenga peththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillai naanunga…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillai naanunga…"/>
</div>
<div class="lyrico-lyrics-wrapper">Paattil kurai irunthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paattil kurai irunthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga konjam kandukkraatheenga…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga konjam kandukkraatheenga…"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirana thirana thirana thiraanaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirana thirana thirana thiraanaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parama china enga ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parama china enga ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam vanthu singapore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam vanthu singapore"/>
</div>
<div class="lyrico-lyrics-wrapper">Parama china enga ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parama china enga ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkam vanthu singapore
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkam vanthu singapore"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarukku bayanthu naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarukku bayanthu naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhadhippo indha ooru…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhadhippo indha ooru…"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarukku bayanthu naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarukku bayanthu naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhadhippo indha ooru…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhadhippo indha ooru…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daei-a-maina daei-a-maina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daei-a-maina daei-a-maina"/>
</div>
<div class="lyrico-lyrics-wrapper">Daei-a-maina daei-a-losai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daei-a-maina daei-a-losai"/>
</div>
<div class="lyrico-lyrics-wrapper">Daei-a-maina daei-a-maina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daei-a-maina daei-a-maina"/>
</div>
<div class="lyrico-lyrics-wrapper">Daei-a-maina daei-a-losai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daei-a-maina daei-a-losai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daei-a-maina daei-a-maina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daei-a-maina daei-a-maina"/>
</div>
<div class="lyrico-lyrics-wrapper">Daei-a-maina daei-a-losai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daei-a-maina daei-a-losai"/>
</div>
<div class="lyrico-lyrics-wrapper">Daei-a-maina daei-a-maina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daei-a-maina daei-a-maina"/>
</div>
<div class="lyrico-lyrics-wrapper">Daei-a-maina daei-a-losai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daei-a-maina daei-a-losai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanakkamaiyaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanakkamaiyaa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga kadhaiya kekkathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga kadhaiya kekkathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasu koduththu kottuvanthirukkomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasu koduththu kottuvanthirukkomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yempaa aaduravanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yempaa aaduravanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam thalli thalli ninnu aadungappaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam thalli thalli ninnu aadungappaa…"/>
</div>
<div class="lyrico-lyrics-wrapper">Idichchikittu vandhu ninneenganna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idichchikittu vandhu ninneenganna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga paaduravangalukku epdi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga paaduravangalukku epdi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eedu kodutthu aada mudiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eedu kodutthu aada mudiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalla naeram…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla naeram…"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhalla devar paattu paaduvom…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhalla devar paattu paaduvom…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga kula thangoi….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga kula thangoi…."/>
</div>
<div class="lyrico-lyrics-wrapper">Devar kula singoi…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devar kula singoi…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhilmuththu raamalingoi…. oooo… ooo…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhilmuththu raamalingoi…. oooo… ooo…"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhilmuththu raamalingoi….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhilmuththu raamalingoi…."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullam uruga enga thaangoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam uruga enga thaangoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mukkulaththa maavarasingoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukkulaththa maavarasingoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam uruga enga thaangoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam uruga enga thaangoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mukkulaththa maavarasingoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukkulaththa maavarasingoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kundrinadiyilae yirundhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kundrinadiyilae yirundhaar"/>
</div>
<div class="lyrico-lyrics-wrapper">kumaranaiyae ninaiththirundhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kumaranaiyae ninaiththirundhaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kundrinadiyilae yirundhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kundrinadiyilae yirundhaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumaranaiyae ninaiththirundhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumaranaiyae ninaiththirundhaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pasumponnai kaakkavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasumponnai kaakkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarivallal adainthoi…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarivallal adainthoi…"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasumponnai kaakkavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasumponnai kaakkavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarivallal adainthoi…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarivallal adainthoi…"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasumpul thangamaiyyaa… oooo….
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasumpul thangamaiyyaa… oooo…."/>
</div>
<div class="lyrico-lyrics-wrapper">Pasumpul thangamaiyyaa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasumpul thangamaiyyaa…"/>
</div>
</pre>
