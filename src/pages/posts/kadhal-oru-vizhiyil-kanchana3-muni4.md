---
title: "kadhal oru vizhiyil song lyrics"
album: "Kanchana3-Muni4"
artist: "S. Thaman"
lyricist: "Madhan Karky"
director: "Raghava Lawrence"
path: "/albums/kanchana3-muni4-lyrics"
song: "Kadhal Oru Vizhiyil"
image: ../../images/albumart/kanchana3-muni4.jpg
date: 2019-04-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4hDvTgapP6I"
type: "Love"
singers:
  - Neha Venugopal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kadhal Oru Vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Oru Vizhiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Kaana En Nenjam Uraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Kaana En Nenjam Uraiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaamam Oru Vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam Oru Vizhiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Kaana En Dhegam Karaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Kaana En Dhegam Karaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhil Idhazhveithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil Idhazhveithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Aasai Nee Koorinaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aasai Nee Koorinaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvai Mazhaiyaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Mazhaiyaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Maarbil Nee Thooravey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maarbil Nee Thooravey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neruppinil Nanaindhiduvena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppinil Nanaindhiduvena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Anaippinil Erindhiduvena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Anaippinil Erindhiduvena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirugugal Viriththiduvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirugugal Viriththiduvenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Vaanil Kaadhal Puriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Vaanil Kaadhal Puriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urasaley Udalgalin Kodai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasaley Udalgalin Kodai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Viral Thoda Vilagudhu Aadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Viral Thoda Vilagudhu Aadai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravidhu Viragathin Medai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravidhu Viragathin Medai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhil Neeyum Naanum Eriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Neeyum Naanum Eriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadumudhuginil Oorndhu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadumudhuginil Oorndhu Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhoru Viral Engu Nagarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhoru Viral Engu Nagarum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Payanathil Tholaindhen Naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Payanathil Tholaindhen Naaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Naaney Thedugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Naaney Thedugiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unadhidhazhgalin Paadhangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unadhidhazhgalin Paadhangal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Meniyil Nee Vaithu Chella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Meniyil Nee Vaithu Chella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhi Thodarndhida Paarkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi Thodarndhida Paarkiren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Idhayatthil Mudindhadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Idhayatthil Mudindhadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neruppinil Nanaindhiduvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neruppinil Nanaindhiduvenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Anaippinil Erindhiduvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Anaippinil Erindhiduvenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siragugal Virithiduvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siragugal Virithiduvenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Vaanil Kaadhal Puriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Vaanil Kaadhal Puriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paalil Vizhum Thaenai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalil Vizhum Thaenai Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatril Vizhum Osai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril Vizhum Osai Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeril Vizhum Vaanam Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeril Vizhum Vaanam Pola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ennul Vizhundhuvidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennul Vizhundhuvidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhuvarai Vazhi Thaanguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvarai Vazhi Thaanguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Aanmaiyai Naan Kelviketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aanmaiyai Naan Kelviketka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidai Alithida Pogiraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidai Alithida Pogiraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Koorinaal Ennaaguven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Koorinaal Ennaaguven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urasaley Udalgalin Kodai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urasaley Udalgalin Kodai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Viral Thoda Vilagudhu Aadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Viral Thoda Vilagudhu Aadai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravidhu Viragathin Medai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravidhu Viragathin Medai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhil Neeyum Naanum Eriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhil Neeyum Naanum Eriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Oru Vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Oru Vizhiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Kaana En Nenjam Uraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Kaana En Nenjam Uraiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaamam Oru Vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaamam Oru Vizhiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Kaana En Dhegam Karaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Kaana En Dhegam Karaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhil Idhazhveithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhil Idhazhveithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Aasai Nee Koorinaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aasai Nee Koorinaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarvai Mazhaiyaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarvai Mazhaiyaaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Maarbil Nee Thooravey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Maarbil Nee Thooravey"/>
</div>
</pre>
