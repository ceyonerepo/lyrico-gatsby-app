---
title: "thannikulla thee song lyrics"
album: "Nandhi"
artist: "Bharathwaj"
lyricist: "Muthu Vijayan"
director: "Tamilvannan"
path: "/albums/thoonga-nagaram-lyrics"
song: "Thannikulla Thee"
image: ../../images/albumart/thoonga-nagaram.jpg
date: 2011-02-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/xhSDLMjt2Mc"
type: "love"
singers:
  - Karthik
  - Janani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thannikkulla theeppudichadhennav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannikkulla theeppudichadhennav"/>
</div>
<div class="lyrico-lyrics-wrapper">indha thaamarakku vaeru kulam vettav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha thaamarakku vaeru kulam vettav"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaavanikku vidumurai sollavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaavanikku vidumurai sollavo"/>
</div>
<div class="lyrico-lyrics-wrapper">en vetka karai vaettiyila ottavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vetka karai vaettiyila ottavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi vattikkaasu vattikkaasu varavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi vattikkaasu vattikkaasu varavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un kuttikkutti soppanathil varavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kuttikkutti soppanathil varavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada maachum moochum pesum naeram idhuvaa        
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada maachum moochum pesum naeram idhuvaa        "/>
</div>
<div class="lyrico-lyrics-wrapper">Odhadum odhadum irukkakkoodaadhu dhooramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odhadum odhadum irukkakkoodaadhu dhooramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">irundhaa medhakkum vayasu maarippoagumadi baaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irundhaa medhakkum vayasu maarippoagumadi baaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Usurula varum oru vidha nadukkam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurula varum oru vidha nadukkam "/>
</div>
<div class="lyrico-lyrics-wrapper">kazhuthula mudivizhuvadhai thadukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kazhuthula mudivizhuvadhai thadukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ilamaiyaikkandu ilamaiyum varundhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilamaiyaikkandu ilamaiyum varundhum"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu thodarndhaa kasandhidum virundhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu thodarndhaa kasandhidum virundhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi onna enna mundhi Odudhey aasaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi onna enna mundhi Odudhey aasaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">adhai veratti pudichi mudhugil poadanum boosaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhai veratti pudichi mudhugil poadanum boosaithaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vedi vachchi paruthi vedikkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedi vachchi paruthi vedikkumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vedhai poattu kallum molaikkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedhai poattu kallum molaikkumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">muttam muttam thorakkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muttam muttam thorakkumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">anumadhi kaettuththulla azhagiya neeyadaa        
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anumadhi kaettuththulla azhagiya neeyadaa        "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Madaiya odaichi oorukkul pugundha vellamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madaiya odaichi oorukkul pugundha vellamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">nee manasa odaichi enakkul nuzhainja eerammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee manasa odaichi enakkul nuzhainja eerammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sediyila nee poovena irukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sediyila nee poovena irukka"/>
</div>
<div class="lyrico-lyrics-wrapper">parichida naan paththuviral edukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parichida naan paththuviral edukka"/>
</div>
<div class="lyrico-lyrics-wrapper">bayathula nee bakkunnu padhaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayathula nee bakkunnu padhaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam poova ilaikkulla maraikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam poova ilaikkulla maraikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Analum senalum kaikkoarthu nadakkum naeramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Analum senalum kaikkoarthu nadakkum naeramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sudum viralgal urasa penmai arivazhinji poagumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sudum viralgal urasa penmai arivazhinji poagumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru moottai thavitta kaattunaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru moottai thavitta kaattunaa "/>
</div>
<div class="lyrico-lyrics-wrapper">karungaalai kayithil nirkkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karungaalai kayithil nirkkumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">paanjaaley saakku minjumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paanjaaley saakku minjumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pasikkunnu thirududhal paavamillappenney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasikkunnu thirududhal paavamillappenney "/>
</div>
</pre>