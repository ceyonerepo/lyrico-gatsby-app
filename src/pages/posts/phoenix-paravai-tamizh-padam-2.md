---
title: "phoenix paravai song lyrics"
album: "Tamizh Padam 2"
artist: "N. Kannan"
lyricist: "Chandru"
director: "CS Amudhan"
path: "/albums/tamizh-padam-2-lyrics"
song: "Phoenix Paravai"
image: ../../images/albumart/tamizh-padam-2.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZfewP3Vw3EI"
type: "happy"
singers:
  - Srinisha Jayaseelan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nerupil irukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerupil irukum"/>
</div>
<div class="lyrico-lyrics-wrapper">phoenix paravai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="phoenix paravai "/>
</div>
<div class="lyrico-lyrics-wrapper">uyir perum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir perum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethiril ulla thadaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethiril ulla thadaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">ellam ini vidai perum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ellam ini vidai perum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suriyanai naai kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suriyanai naai kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">kuraithalum kadika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuraithalum kadika"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaithaalum athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaithaalum athu"/>
</div>
<div class="lyrico-lyrics-wrapper">nadakathu suriyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadakathu suriyan"/>
</div>
<div class="lyrico-lyrics-wrapper">mathikathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathikathu "/>
</div>
<div class="lyrico-lyrics-wrapper">ithu thanda vaalkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithu thanda vaalkai"/>
</div>
<div class="lyrico-lyrics-wrapper">ini kollum vetkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini kollum vetkai"/>
</div>
<div class="lyrico-lyrics-wrapper">vettriyagum varalaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettriyagum varalaru"/>
</div>
<div class="lyrico-lyrics-wrapper">uthaymaagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthaymaagum"/>
</div>
<div class="lyrico-lyrics-wrapper">meen neeril vaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meen neeril vaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">maan ellam katil vaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maan ellam katil vaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">veri konda vetai ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veri konda vetai ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">engeyum uyir vaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engeyum uyir vaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">intha nilai ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha nilai ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">veeru kondu elugirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veeru kondu elugirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">alagai puyal pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagai puyal pole"/>
</div>
<div class="lyrico-lyrics-wrapper">seeri athu varugirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seeri athu varugirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">itho athu varugirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="itho athu varugirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">sabaiyorke varugirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sabaiyorke varugirathu"/>
</div>
</pre>
