---
title: "anthuleni cruse song song lyrics"
album: "Mithai"
artist: "Vivek Sagar"
lyricist: "Kittu Vissapragada"
director: "Prashant Kumar"
path: "/albums/mithai-lyrics"
song: "Anthuleni (Cruse Song)"
image: ../../images/albumart/mithai.jpg
date: 2019-02-22
lang: telugu
youtubeLink: 
type: "happy"
singers:
  - Sai Shivani
  - Vivek sagar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anthuleni Erakatamlo Thosesinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthuleni Erakatamlo Thosesinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vallakani Porapaate Premaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallakani Porapaate Premaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontaraina Baagunde Yadaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontaraina Baagunde Yadaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Endukanta Edo Thodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endukanta Edo Thodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chittu Lokam Emanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chittu Lokam Emanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakem Paniraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakem Paniraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatho Nene Naakunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatho Nene Naakunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Janteeraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janteeraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andhariki Nacche panilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andhariki Nacche panilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenpudu Chenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenpudu Chenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvaritho Bhandham Kalipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvaritho Bhandham Kalipe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aashakuda Ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aashakuda Ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Magavaadi Neede Naapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magavaadi Neede Naapai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padadam Sadhyam Kaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padadam Sadhyam Kaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pove Pove Maguvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pove Pove Maguvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi Vadilina Vaaru Ee velaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi Vadilina Vaaru Ee velaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Padunaina Gaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Padunaina Gaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mruduvaina Gnaapakam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mruduvaina Gnaapakam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadadhani Preema Shmashanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadadhani Preema Shmashanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Andulona Nenu Brathike Shavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andulona Nenu Brathike Shavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Porapaatu Paduthunnadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porapaatu Paduthunnadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee preemaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee preemaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nela Gunde Cheeli Pothunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nela Gunde Cheeli Pothunte"/>
</div>
<div class="lyrico-lyrics-wrapper">Matti Nannu Kougilinchu Koonantee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matti Nannu Kougilinchu Koonantee"/>
</div>
<div class="lyrico-lyrics-wrapper">Dikkuleni Nenu Evarantee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dikkuleni Nenu Evarantee"/>
</div>
<div class="lyrico-lyrics-wrapper">Daarileni Bhaatasaranthee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daarileni Bhaatasaranthee"/>
</div>
</pre>
