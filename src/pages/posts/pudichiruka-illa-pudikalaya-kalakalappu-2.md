---
title: "pudichiruka illa pudikalaya song lyrics"
album: "Kalakalappu 2"
artist: "Hiphop Tamizha"
lyricist: "Mohan Rajan - Hiphop Tamizha"
director: "Sundar C"
path: "/albums/kalakalappu-2-lyrics"
song: "Pudichiruka illa Pudikalaya"
image: ../../images/albumart/kalakalappu-2.jpg
date: 2018-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-MQLFlSA4Nw"
type: "love"
singers:
  - Hiphop Tamizha
  - Varun Parandhaman
  - Rajan Chelliah
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Pudichiruka illa pudikalaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichiruka illa pudikalaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa pudichatha pola nadikiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa pudichatha pola nadikiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikiriya illa moraikiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikiriya illa moraikiriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Illa moraipathu pola rasikiriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa moraipathu pola rasikiriya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi onnumae puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi onnumae puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi sathiyama teriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi sathiyama teriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaa ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo verai vazhiyum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo verai vazhiyum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyae solliduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae solliduma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yenna naan tubelight-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna naan tubelight-u"/>
</div>
<div class="lyrico-lyrics-wrapper">En heart-u white-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En heart-u white-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethan en sight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethan en sight"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjom Vaa ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjom Vaa ma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seri ethuku fight-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seri ethuku fight-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sonna right-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sonna right-u"/>
</div>
<div class="lyrico-lyrics-wrapper">En life-uke ligh–tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En life-uke ligh–tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hema"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seriya ma Aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seriya ma Aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae Hema Aaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Hema Aaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un mama Aaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un mama Aaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagalama Aaaa aa aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagalama Aaaa aa aa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aishwarya are you ready-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aishwarya are you ready-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Un veeta vittu enkuda than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un veeta vittu enkuda than"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum variya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum variya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aishwarya nee en kaadhaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aishwarya nee en kaadhaliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirukkira siruppula sethayiren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirukkira siruppula sethayiren naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seriya ma Aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seriya ma Aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae Hema Aaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Hema Aaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un mama Aaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un mama Aaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagalama Aaaa aa aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagalama Aaaa aa aa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Partha odan pidikum figurunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Partha odan pidikum figurunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomiyila romba peru porapanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomiyila romba peru porapanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahmanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahmanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa pesa pidikum ponnuga than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa pesa pidikum ponnuga than"/>
</div>
<div class="lyrico-lyrics-wrapper">Adedae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adedae"/>
</div>
<div class="lyrico-lyrics-wrapper">Romba romba kammiya irupanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romba romba kammiya irupanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahmanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahmanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Scooty pulser-eh piece aakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scooty pulser-eh piece aakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru sweety un heart-eh loosu aakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru sweety un heart-eh loosu aakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava beauty un pain-eh lesa aakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava beauty un pain-eh lesa aakum"/>
</div>
<div class="lyrico-lyrics-wrapper">Love-eh life- ah yum mass aakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love-eh life- ah yum mass aakum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi sirikkira silaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi sirikkira silaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesure valaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesure valaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai mattum manasukul nenaipenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai mattum manasukul nenaipenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenaipenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenaipenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Terikkira alaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Terikkira alaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adikkira mazhaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adikkira mazhaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkulae naan vanthu kudhipenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkulae naan vanthu kudhipenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhipenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhipenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Olagathil porakum ponnunga yaarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olagathil porakum ponnunga yaarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkennu porakka mattanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkennu porakka mattanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kidaikira gap-u la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kidaikira gap-u la"/>
</div>
<div class="lyrico-lyrics-wrapper">Nool-eh vidu da machan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool-eh vidu da machan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandippa thirumbi paapanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandippa thirumbi paapanga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aishwarya are you ready-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aishwarya are you ready-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Un veeta vittu enkuda than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un veeta vittu enkuda than"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum variya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum variya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aishwarya nee en kaadhaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aishwarya nee en kaadhaliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirukkira siruppula sethayiren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirukkira siruppula sethayiren naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veeta vittu enkuda than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeta vittu enkuda than"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum variya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum variya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aishwarya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aishwarya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirukkira siruppula sethayiren naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirukkira siruppula sethayiren naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seriya ma Aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seriya ma Aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyae Hema Aaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae Hema Aaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un mama Aaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un mama Aaaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagalama Aaaa aa aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagalama Aaaa aa aa aa"/>
</div>
</pre>
