---
title: "aaru padai song lyrics"
album: "Mappillai"
artist: "Mani Sharma"
lyricist: "Viveka"
director: "Suraj"
path: "/albums/mappillai-lyrics"
song: "Aaru Padai"
image: ../../images/albumart/mappillai.jpg
date: 2011-04-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/H7cFlw3H8Mo"
type: "mass"
singers:
  - Vijay Yesudas
  - Mukesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kodi Malaigaliley Kodukkum Malai Entha Malai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi Malaigaliley Kodukkum Malai Entha Malai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kongumani Naattiniley Kulirntha Malai Entha Malai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kongumani Naattiniley Kulirntha Malai Entha Malai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedivanthor Illamellaam  Sezhikkum Malai Entha Malai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedivanthor Illamellaam  Sezhikkum Malai Entha Malai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Devaathi Devarellaam Thedivarum Marutha Malai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Devaathi Devarellaam Thedivarum Marutha Malai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marutha Malai Marutha Malai Murugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marutha Malai Marutha Malai Murugaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ad by Valueimpression
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ad by Valueimpression"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaru Padai Velmuruga Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru Padai Velmuruga Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appanaku Vaatthiyarey Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appanaku Vaatthiyarey Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutta Pazham Thandavane Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutta Pazham Thandavane Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooranaye Vendravane Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooranaye Vendravane Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanthanukku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanthanukku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumaranaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumaranaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murganaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murganaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthalvanaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthalvanaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaru Padai Velmuruga Va Va Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru Padai Velmuruga Va Va Va"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appanaku Vaaththiyarey Va Va Va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appanaku Vaaththiyarey Va Va Va"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velanuku Arogara Vel Vel Vettrivel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velanuku Arogara Vel Vel Vettrivel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vettrivel Muruganaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettrivel Muruganaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koravar Saathiyila Ponneduthu Kaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koravar Saathiyila Ponneduthu Kaatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthumai Seithaaye Purachi Deivam Neethaaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthumai Seithaaye Purachi Deivam Neethaaya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagam Email’la Paarthathelaam Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Email’la Paarthathelaam Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mothalil Vanthathu Un Mayilthaan Velaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mothalil Vanthathu Un Mayilthaan Velaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Valapuram Vallitha  Idappuram Deivaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valapuram Vallitha  Idappuram Deivaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalakkura Kantha Nee Romba Kilaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalakkura Kantha Nee Romba Kilaadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethireenga Vanthaaley Idaivali Illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethireenga Vanthaaley Idaivali Illaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panthaadum Veeran Neethaanappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panthaadum Veeran Neethaanappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanthanukku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanthanukku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumaranaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumaranaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murganaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murganaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthalvanaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthalvanaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaru Padai Velmuruga Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru Padai Velmuruga Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appanaku Vaatthiyarey Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appanaku Vaatthiyarey Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pazhathai Yaanai Mugan Vaangikondathaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhathai Yaanai Mugan Vaangikondathaley"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pathari Ninraaye Ethukku Intha Munkovam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathari Ninraaye Ethukku Intha Munkovam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamilar Saamiyena Paer Edutha Vaelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamilar Saamiyena Paer Edutha Vaelaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanangi Ninnaale Kooduthaiyaa Urchaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanangi Ninnaale Kooduthaiyaa Urchaagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ganabathi Thumbikkai Unnidathil Nambikkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganabathi Thumbikkai Unnidathil Nambikkai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethukkumey Anjaatha Chutti Payanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukkumey Anjaatha Chutti Payanee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sivanukku Pullathaan Manam Romba Vellathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivanukku Pullathaan Manam Romba Vellathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnoda Paatham Thaedivanthaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnoda Paatham Thaedivanthaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanthanukku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanthanukku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumaranaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumaranaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murganaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murganaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthalvanaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthalvanaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaru Padai Velmuruga Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru Padai Velmuruga Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appanaku Vaatthiyarey Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appanaku Vaatthiyarey Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutta Pazham Thandavane Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutta Pazham Thandavane Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooranaye Vendravane Vaa Vaa Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooranaye Vendravane Vaa Vaa Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanthanukku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanthanukku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumaranaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumaranaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murganaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murganaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthalvanaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthalvanaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanthanukku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanthanukku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kumaranaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumaranaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Murganaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murganaku Arogara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muthalvanaku Arogara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthalvanaku Arogara"/>
</div>
</pre>
