---
title: "kadhal psycho song lyrics"
album: "Saaho"
artist: "Tanishk Bagchi"
lyricist: "Madhan Karky"
director: "Sujeeth"
path: "/albums/saaho-lyrics"
song: "Kadhal Psycho"
image: ../../images/albumart/saaho.jpg
date: 2019-08-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/e_Q3A9CheFM"
type: "happy"
singers:
  - Anirudh Ravichander
  - Dhvani Bhanushali
  - Tanishk Bagchi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ooh Mudhal Sirippil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooh Mudhal Sirippil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaiyatha Edutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaiyatha Edutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adutha Murai Urakkatha Kedutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha Murai Urakkatha Kedutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Theliyavechu Mayakaththa Kodutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theliyavechu Mayakaththa Kodutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pennae Unmel Enakko Kirukoo Kirukoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennae Unmel Enakko Kirukoo Kirukoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Suththa Vitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Suththa Vitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Loop-il Naanum Thirinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loop-il Naanum Thirinjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Kenji Kenji Anju Kilo Koranjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Kenji Kenji Anju Kilo Koranjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Tinder Profile Paththu Konjam Erinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Tinder Profile Paththu Konjam Erinjen"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaiyathil Enakidam Irukoo Irukoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaiyathil Enakidam Irukoo Irukoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Enna Kekkaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Enna Kekkaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Possessive Aagaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Possessive Aagaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagiya Aangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagiya Aangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Konjippesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Konjippesa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anga Konjam Nenjum Broke-o
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga Konjam Nenjum Broke-o"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Paaru Kaadhal Psycho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Paaru Kaadhal Psycho"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Paaru Kaadhal Psycho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Paaru Kaadhal Psycho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Adakkam Ozhukathin Dictionary
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Adakkam Ozhukathin Dictionary"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathadhumae Vanangura Maadhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathadhumae Vanangura Maadhiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Adakkam Ozhukathin Dictionary
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Adakkam Ozhukathin Dictionary"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathadhumae Vanangura Maadhiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathadhumae Vanangura Maadhiri"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Sandhegatha Nee Eri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Sandhegatha Nee Eri"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Almost Seedhai Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Almost Seedhai Dhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohh Kannae Kannae Paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohh Kannae Kannae Paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Dhaan Un Chowkidhaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Dhaan Un Chowkidhaar"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Manasukku Kaaval
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Manasukku Kaaval"/>
</div>
<div class="lyrico-lyrics-wrapper">Udambukku Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udambukku Kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum Tharum Dhegam Dekho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendum Tharum Dhegam Dekho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inga Paaru Kaadhal Psycho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Paaru Kaadhal Psycho"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Paaru Kaadhal Psycho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Paaru Kaadhal Psycho"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga Paaru Kaadhal Psycho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga Paaru Kaadhal Psycho"/>
</div>
</pre>
