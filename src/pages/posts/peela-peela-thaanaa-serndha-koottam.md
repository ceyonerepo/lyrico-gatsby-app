---
title: "peela peela song lyrics"
album: "Thaanaa Serndha Koottam"
artist: "Anirudh Ravichander"
lyricist: "Vignesh Shivan"
director: "Vignesh Shivan"
path: "/albums/thaanaa-serndha-koottam-lyrics"
song: "Peela Peela"
image: ../../images/albumart/thaanaa-serndha-koottam.jpg
date: 2018-01-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/IYw2doWoucs"
type: "love"
singers:
  - Jassie Gift
  - Nakash Aziz
  - Mali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yo poyah peela vidadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo poyah peela vidadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh venna reela suthadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh venna reela suthadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yo poyah peela vidadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo poyah peela vidadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeh venna reela suthadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeh venna reela suthadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peela peela peela peela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peela peela peela peela"/>
</div>
<div class="lyrico-lyrics-wrapper">Peela vudadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peela vudadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Reela reela reela reela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reela reela reela reela"/>
</div>
<div class="lyrico-lyrics-wrapper">Reela suthadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reela suthadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peela peela peela peela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peela peela peela peela"/>
</div>
<div class="lyrico-lyrics-wrapper">Peela vudadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peela vudadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Venna venna venavae venna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venna venna venavae venna"/>
</div>
<div class="lyrico-lyrics-wrapper">Scena podadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scena podadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dai romeo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dai romeo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum jodiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum jodiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaevava varae vaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaevava varae vaava"/>
</div>
<div class="lyrico-lyrics-wrapper">My mamiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My mamiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">En babies-ku mummyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En babies-ku mummyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Varae vaava varae vaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varae vaava varae vaava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha nilavayeraki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha nilavayeraki"/>
</div>
<div class="lyrico-lyrics-wrapper">Alava surukki naan thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alava surukki naan thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha megatha madakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha megatha madakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyila adachitten paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila adachitten paaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peela peela peela peela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peela peela peela peela"/>
</div>
<div class="lyrico-lyrics-wrapper">Peela vudadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peela vudadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Reela reela reela reela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reela reela reela reela"/>
</div>
<div class="lyrico-lyrics-wrapper">Reela suthadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reela suthadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peela peela peela peela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peela peela peela peela"/>
</div>
<div class="lyrico-lyrics-wrapper">Peela vudadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peela vudadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Venna venna venavae venna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venna venna venavae venna"/>
</div>
<div class="lyrico-lyrics-wrapper">Scena podadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scena podadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nirma sundariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirma sundariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poppins pallazhagae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poppins pallazhagae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ujala iduppathaan ullukuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ujala iduppathaan ullukuriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Lifeboy arokiyamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lifeboy arokiyamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Boost energiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boost energiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Goldspot nenjukkulla ilukkuriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goldspot nenjukkulla ilukkuriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaama en rasnavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaama en rasnavae"/>
</div>
<div class="lyrico-lyrics-wrapper">I love you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I love you"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam pakkathilae neeyum vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam pakkathilae neeyum vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhagam theeruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhagam theeruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dyanora raasavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dyanora raasavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Love you too
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love you too"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kawasaki bike-u seatil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kawasaki bike-u seatil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ukkarathaan kathurukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ukkarathaan kathurukken"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaja aaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja aaja"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaja ah ah aajja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja ah ah aajja"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaja aaja aaja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja aaja aaja"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaja honey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaja honey"/>
</div>
<div class="lyrico-lyrics-wrapper">Attam podalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attam podalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peela peela peela peela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peela peela peela peela"/>
</div>
<div class="lyrico-lyrics-wrapper">Peela vudadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peela vudadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Venna venna venavae venna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venna venna venavae venna"/>
</div>
<div class="lyrico-lyrics-wrapper">Scena podadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scena podadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dai romeo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dai romeo"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyum naanum jodiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum naanum jodiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Varaevava varae varae vaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaevava varae varae vaava"/>
</div>
<div class="lyrico-lyrics-wrapper">My mamiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My mamiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">En babies-ku mummyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En babies-ku mummyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Varae vaava varae vaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varae vaava varae vaava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha nilavayerakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha nilavayerakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Alava surukki naan thaaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alava surukki naan thaaren"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha megatha madakki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha megatha madakki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyila adachitten paaren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyila adachitten paaren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appa kitta sollama vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appa kitta sollama vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Attam podalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attam podalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma kitta sollama va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma kitta sollama va"/>
</div>
<div class="lyrico-lyrics-wrapper">Appe tu aagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appe tu aagalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appa kitta sollama va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appa kitta sollama va"/>
</div>
<div class="lyrico-lyrics-wrapper">Attam podalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attam podalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Amma kitta sollama va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amma kitta sollama va"/>
</div>
<div class="lyrico-lyrics-wrapper">Abscond aagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abscond aagalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peela peela peela peela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peela peela peela peela"/>
</div>
<div class="lyrico-lyrics-wrapper">Peela vudadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peela vudadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Reela reela reela reela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reela reela reela reela"/>
</div>
<div class="lyrico-lyrics-wrapper">Reela suthadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reela suthadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Peela peela peela peela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peela peela peela peela"/>
</div>
<div class="lyrico-lyrics-wrapper">Peela vudadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peela vudadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Venna venna venave venna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venna venna venave venna"/>
</div>
<div class="lyrico-lyrics-wrapper">Scena podadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Scena podadha"/>
</div>
</pre>
