---
title: "kozhundhan peru song lyrics"
album: "Meendum Oru Mariyathai"
artist: "N. R. Raghunanthan - Sharran Surya - Yuvan Shankar Raja"
lyricist: "Agathiyan"
director: "Bharathiraja"
path: "/albums/meendum-oru-mariyathai-lyrics"
song: "Kozhundhan Peru"
image: ../../images/albumart/meendum-oru-mariyathai.jpg
date: 2020-02-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/f8gq1ATupFU"
type: "love"
singers:
  - Velmurugan
  - Priya Himesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kozhundhan peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhundhan peru"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhandha saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhandha saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhundhiyala thedum saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhundhiyala thedum saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda vanthen kozhundhiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda vanthen kozhundhiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">aaki putane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaki putane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en kooda vantha ponnu thai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kooda vantha ponnu thai"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhundhan mela aasa pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhundhan mela aasa pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">koodu vitu koodu paanju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodu vitu koodu paanju"/>
</div>
<div class="lyrico-lyrics-wrapper">maati kittale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maati kittale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kokarako sevaluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kokarako sevaluku"/>
</div>
<div class="lyrico-lyrics-wrapper">konja thonuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konja thonuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kopurane sathiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kopurane sathiyama"/>
</div>
<div class="lyrico-lyrics-wrapper">unna theduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna theduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">un meesa mela koola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un meesa mela koola"/>
</div>
<div class="lyrico-lyrics-wrapper">romba sayam patha noolu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba sayam patha noolu"/>
</div>
<div class="lyrico-lyrics-wrapper">nan koothu meda ramasamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan koothu meda ramasamy"/>
</div>
<div class="lyrico-lyrics-wrapper">anjaneyar vaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anjaneyar vaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kozhundhan peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhundhan peru"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhandha saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhandha saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhundhiyala thedum saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhundhiyala thedum saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda vanthen kozhundhiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda vanthen kozhundhiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">aaki putane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaki putane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en kooda vantha ponnu thai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kooda vantha ponnu thai"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhundhan mela aasa pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhundhan mela aasa pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">koodu vitu koodu paanju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodu vitu koodu paanju"/>
</div>
<div class="lyrico-lyrics-wrapper">maati kittale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maati kittale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aala pathu mela pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aala pathu mela pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">jaala pathu aenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaala pathu aenguren"/>
</div>
<div class="lyrico-lyrics-wrapper">vaala poovu thola pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaala poovu thola pola"/>
</div>
<div class="lyrico-lyrics-wrapper">senthu vaa ngura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="senthu vaa ngura"/>
</div>
<div class="lyrico-lyrics-wrapper">neram kaalam paakamale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram kaalam paakamale"/>
</div>
<div class="lyrico-lyrics-wrapper">kootha kathu vesura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootha kathu vesura"/>
</div>
<div class="lyrico-lyrics-wrapper">saama kozhi koovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saama kozhi koovum"/>
</div>
<div class="lyrico-lyrics-wrapper">neram veembu pesura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram veembu pesura"/>
</div>
<div class="lyrico-lyrics-wrapper">seendi paakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seendi paakala"/>
</div>
<div class="lyrico-lyrics-wrapper">unna paandi aadala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna paandi aadala"/>
</div>
<div class="lyrico-lyrics-wrapper">thandi kudhikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thandi kudhikala"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo vendi kekuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo vendi kekuren"/>
</div>
<div class="lyrico-lyrics-wrapper">nan ooru pora theru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan ooru pora theru"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thedi vanthu seru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thedi vanthu seru"/>
</div>
<div class="lyrico-lyrics-wrapper">konja neeru vita more 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konja neeru vita more "/>
</div>
<div class="lyrico-lyrics-wrapper">kooda utharama paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda utharama paru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kozhundhan peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhundhan peru"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhandha saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhandha saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhundhiyala thedum saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhundhiyala thedum saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda vanthen kozhundhiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda vanthen kozhundhiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">aaki putane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aaki putane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">en kooda vantha ponnu thai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kooda vantha ponnu thai"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhundhan mela aasa pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhundhan mela aasa pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">koodu vitu koodu paanju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodu vitu koodu paanju"/>
</div>
<div class="lyrico-lyrics-wrapper">maati kittale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maati kittale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kora paaya neeti potu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kora paaya neeti potu"/>
</div>
<div class="lyrico-lyrics-wrapper">notam nanum pakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="notam nanum pakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">veedu vaasal thottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veedu vaasal thottam"/>
</div>
<div class="lyrico-lyrics-wrapper">thoppu nanum kekala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoppu nanum kekala"/>
</div>
<div class="lyrico-lyrics-wrapper">kovil maadu pola nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovil maadu pola nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">jodi poda thedala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jodi poda thedala"/>
</div>
<div class="lyrico-lyrics-wrapper">moonu vela sothukaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moonu vela sothukaga"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum vaazhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum vaazhala"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa kaatala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa kaatala"/>
</div>
<div class="lyrico-lyrics-wrapper">summa vesam katala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa vesam katala"/>
</div>
<div class="lyrico-lyrics-wrapper">paasam vaikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paasam vaikala"/>
</div>
<div class="lyrico-lyrics-wrapper">unna mosam seiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna mosam seiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">ne aelakaran selai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne aelakaran selai"/>
</div>
<div class="lyrico-lyrics-wrapper">una kekaporen mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="una kekaporen mela"/>
</div>
<div class="lyrico-lyrics-wrapper">ne aeni mela aeni vachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne aeni mela aeni vachi"/>
</div>
<div class="lyrico-lyrics-wrapper">paru vetti vela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paru vetti vela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan kozhundhan mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kozhundhan mela"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa pattu kooda vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa pattu kooda vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">ponnu thaayi koopidama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ponnu thaayi koopidama"/>
</div>
<div class="lyrico-lyrics-wrapper">saapidama kochukalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saapidama kochukalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kozhunthiyalum aasa pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhunthiyalum aasa pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">kozhunthana than manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kozhunthana than manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kulla koopudira 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulla koopudira "/>
</div>
<div class="lyrico-lyrics-wrapper">kovam nu yosikalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovam nu yosikalama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pulli vacha kolathaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulli vacha kolathaye"/>
</div>
<div class="lyrico-lyrics-wrapper">pottu mudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pottu mudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">vacha pulli kolathuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vacha pulli kolathuku"/>
</div>
<div class="lyrico-lyrics-wrapper">micha maavu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="micha maavu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kooda kooda varen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kooda kooda varen"/>
</div>
<div class="lyrico-lyrics-wrapper">oru thaali poda poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru thaali poda poren"/>
</div>
<div class="lyrico-lyrics-wrapper">nan ooru koodi thaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan ooru koodi thaali"/>
</div>
<div class="lyrico-lyrics-wrapper">poda kathirupen paru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poda kathirupen paru"/>
</div>
</pre>
