---
title: "kaatril aadum deepam song lyrics"
album: "C/o Kadhal"
artist: "Sweekar Agasthi"
lyricist: "Karthik Netha"
director: "Hemambar Jasti"
path: "/albums/co-kadhal-lyrics"
song: "Kaatril Aadum Deepam"
image: ../../images/albumart/co-kadhal.jpg
date: 2021-02-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pIw92pWMpKA"
type: "Motivational"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaatril aadum dheepam ooyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatril aadum dheepam ooyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum theerndhaal aattam podaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum theerndhaal aattam podaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodum neeril veezhum thoorazhgal kai serumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodum neeril veezhum thoorazhgal kai serumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarai thedi yaarin yekkangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarai thedi yaarin yekkangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanum yaanum theera padangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanum yaanum theera padangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarum kaalam maatrum neratthil, veraagumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarum kaalam maatrum neratthil, veraagumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theera theera kelvi panthaadume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera theera kelvi panthaadume"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorutthutthu thoo ru ru ru ru ru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorutthutthu thoo ru ru ru ru ru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ru ru ru ru ru ru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ru ru ru ru ru ru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ru ru ru…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ru ru ru…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh rekkai katti ee pokinrathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh rekkai katti ee pokinrathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathaiyaai theeppori
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathaiyaai theeppori"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorsenru sernthidumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorsenru sernthidumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu kutti rendu kannaikatti kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu kutti rendu kannaikatti kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandabadi ingu ododae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandabadi ingu ododae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye thaakum naalaiye yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye thaakum naalaiye yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Patcha mannu onnu ichha unmaiyinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patcha mannu onnu ichha unmaiyinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Utchu kottikittu poraada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utchu kottikittu poraada"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethaagum paathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethaagum paathaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaikolai poovaakki poovaasam kettalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikolai poovaakki poovaasam kettalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesaathe yeppothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesaathe yeppothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thundil nooru pottaal yenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thundil nooru pottaal yenna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum mattaathammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum mattaathammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh.. Kaatril aadum dheepam ooyathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh.. Kaatril aadum dheepam ooyathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrum theerndhaal aattam podaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrum theerndhaal aattam podaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Oodum neeril veezhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oodum neeril veezhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoorazhgal kai serumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoorazhgal kai serumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oornoolile poo aaduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oornoolile poo aaduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar aasai pol aadi veezhumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar aasai pol aadi veezhumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar theendave poo yengutho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar theendave poo yengutho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkathin kaambile vaadi pogumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkathin kaambile vaadi pogumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanni antha pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanni antha pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanni intha pakkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanni intha pakkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Natta naduvile maan kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natta naduvile maan kutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraadi paarkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadi paarkume"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu thannikulla satthu onnumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu thannikulla satthu onnumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu viduthala neeyaaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu viduthala neeyaaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponaale pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponaale pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppothum yemaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppothum yemaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerin komali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerin komali"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasaigal eederumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasaigal eederumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhkaithaan Maarumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaithaan Maarumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh.. Theera Theera Kelvi Pandhaadume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh.. Theera Theera Kelvi Pandhaadume"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Theranaane Naane Naanena 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theranaane Naane Naanena "/>
</div>
<div class="lyrico-lyrics-wrapper">Theranaane Naane Naanena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theranaane Naane Naanena"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Thara Thare Rarare Tharararaah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Thara Thare Rarare Tharararaah"/>
</div>
</pre>
