---
title: "thornidathe song lyrics"
album: "Maniyarayile Ashokan"
artist: "Sreehari K. Nair"
lyricist: "Ajeesh Dasan"
director: "Shamzu Zayba"
path: "/albums/maniyarayile-ashokan-lyrics"
song: "Thornidathe"
image: ../../images/albumart/maniyarayile-ashokan.jpg
date: 2020-08-31
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/i-4TeYyaTyo"
type: "love"
singers:
  - Sreehari K. Nair
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thornidathe manjormakal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thornidathe manjormakal"/>
</div>
<div class="lyrico-lyrics-wrapper">raavu neenthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raavu neenthi"/>
</div>
<div class="lyrico-lyrics-wrapper">nin thoovalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nin thoovalil"/>
</div>
<div class="lyrico-lyrics-wrapper">mizhineer mukilaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mizhineer mukilaay"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhayay adaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhayay adaran"/>
</div>
<div class="lyrico-lyrics-wrapper">thaniye njan varam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaniye njan varam"/>
</div>
<div class="lyrico-lyrics-wrapper">thanalaay arike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanalaay arike"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thee murivaay eriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee murivaay eriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">novake neer malaraay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="novake neer malaraay"/>
</div>
<div class="lyrico-lyrics-wrapper">ennomal poonkatithalaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennomal poonkatithalaay"/>
</div>
<div class="lyrico-lyrics-wrapper">kanivaay kannoram nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanivaay kannoram nee"/>
</div>
<div class="lyrico-lyrics-wrapper">ee anayaa nilavaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ee anayaa nilavaay"/>
</div>
<div class="lyrico-lyrics-wrapper">ravolam njan neeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ravolam njan neeri"/>
</div>
<div class="lyrico-lyrics-wrapper">mozhiyallathe vaa alivaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhiyallathe vaa alivaay"/>
</div>
<div class="lyrico-lyrics-wrapper">eeran chirathumaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eeran chirathumaay"/>
</div>
<div class="lyrico-lyrics-wrapper">chirakaarnnuyaraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chirakaarnnuyaraan"/>
</div>
<div class="lyrico-lyrics-wrapper">kanamaay pozhiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanamaay pozhiyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">piriyaamormmayaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piriyaamormmayaay"/>
</div>
<div class="lyrico-lyrics-wrapper">kanalaay vazhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanalaay vazhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">thengalaayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thengalaayi"/>
</div>
<div class="lyrico-lyrics-wrapper">ennormakal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennormakal"/>
</div>
<div class="lyrico-lyrics-wrapper">maanjidaathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanjidaathe"/>
</div>
<div class="lyrico-lyrics-wrapper">nin thoovalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nin thoovalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thirayaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirayaay"/>
</div>
<div class="lyrico-lyrics-wrapper">uyaraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyaraan"/>
</div>
<div class="lyrico-lyrics-wrapper">kadalaay neeyunarnnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadalaay neeyunarnnu"/>
</div>
<div class="lyrico-lyrics-wrapper">uravaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uravaay"/>
</div>
<div class="lyrico-lyrics-wrapper">ozhbhukaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ozhbhukaan"/>
</div>
<div class="lyrico-lyrics-wrapper">nadhiyaay theernnuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadhiyaay theernnuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">thiriyaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thiriyaay"/>
</div>
<div class="lyrico-lyrics-wrapper">theliyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theliyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">uyiraay neeyananjo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyiraay neeyananjo"/>
</div>
<div class="lyrico-lyrics-wrapper">chiriyaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chiriyaay"/>
</div>
<div class="lyrico-lyrics-wrapper">thuzhayaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuzhayaan"/>
</div>
<div class="lyrico-lyrics-wrapper">thunayaay koodeyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunayaay koodeyo"/>
</div>
<div class="lyrico-lyrics-wrapper">panineerithalaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panineerithalaay"/>
</div>
<div class="lyrico-lyrics-wrapper">veyilaay adaraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyilaay adaraan"/>
</div>
<div class="lyrico-lyrics-wrapper">thirike njan varaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirike njan varaam"/>
</div>
<div class="lyrico-lyrics-wrapper">imapol pidayatharike
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imapol pidayatharike"/>
</div>
</pre>
