---
title: "kannadi poovukku song lyrics"
album: "Enakku Vaaitha Adimaigal"
artist: "Santhosh Dhayanidhi"
lyricist: "Kabilan"
director: "Mahendran Rajamani"
path: "/albums/enakku-vaaitha-adimaigal-lyrics"
song: "Kannadi Poovukku"
image: ../../images/albumart/enakku-vaaitha-adimaigal.jpg
date: 2017-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TaJSQeBrTuo"
type: "love"
singers:
  - Haricharan
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kannadi poovukku vannamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi poovukku vannamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkitta en kadhal sonnathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkitta en kadhal sonnathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounathil vaarthaigal sonthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounathil vaarthaigal sonthamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala kan thoongala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala kan thoongala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thee pol poochedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thee pol poochedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponathae en moochadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponathae en moochadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal thaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam oonchalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam oonchalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadum theradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadum theradi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal kannukullae ettu poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal kannukullae ettu poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal nenjukullae mettu poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal nenjukullae mettu poda"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi panthu pola unthan gnyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi panthu pola unthan gnyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal thaan suthuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal thaan suthuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna paartha uchu kotti poven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paartha uchu kotti poven"/>
</div>
<div class="lyrico-lyrics-wrapper">Pacha pulla aaven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pacha pulla aaven"/>
</div>
<div class="lyrico-lyrics-wrapper">Achu vellam nee thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achu vellam nee thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna ketta kaadhal solla maatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna ketta kaadhal solla maatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja raani seetta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raani seetta"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja kulukki pottalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja kulukki pottalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthikiren parakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthikiren parakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Parava pola naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parava pola naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa tharaiyilum mithakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa tharaiyilum mithakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhantha pola thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhantha pola thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruguren karaiyuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruguren karaiyuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Mezhuga pola naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mezhuga pola naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna iravilum thodaruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna iravilum thodaruven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhala pola naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhala pola naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi poovukku vannamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi poovukku vannamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkitta en kadhal sonnathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkitta en kadhal sonnathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounathil vaarthaigal sonthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounathil vaarthaigal sonthamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala kan thoongala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala kan thoongala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan yaarodu pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaarodu pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee illaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee illaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan yaarodu yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan yaarodu yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sollaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee sollaamalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullukullae kaadhal undaanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukullae kaadhal undaanathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Oorukum natpukum theriyaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukum natpukum theriyaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyodu thaan kaigal serum varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyodu thaan kaigal serum varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaalae naam pesalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaalae naam pesalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuthikiren parakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthikiren parakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Parava pola naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parava pola naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa tharaiyilum mithakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa tharaiyilum mithakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhantha pola thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhantha pola thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruguren karaiyuran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruguren karaiyuran"/>
</div>
<div class="lyrico-lyrics-wrapper">Mezhuga pola naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mezhuga pola naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna iravilum thodaruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna iravilum thodaruven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhala pola naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhala pola naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadi poovukku vannamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadi poovukku vannamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unkitta en kadhal sonnathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkitta en kadhal sonnathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounathil vaarthaigal sonthamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounathil vaarthaigal sonthamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala kan thoongala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala kan thoongala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee thee pol poochedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thee pol poochedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponathae en moochadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponathae en moochadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaal thaanadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaal thaanadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam oonchalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam oonchalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadum theradi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadum theradi"/>
</div>
</pre>
