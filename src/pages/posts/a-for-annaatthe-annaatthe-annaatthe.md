---
title: "a for annaatthe song lyrics"
album: "Annaatthe"
artist: "D.Imman"
lyricist: "Arivu"
director: "Siva"
path: "/albums/annaatthe-lyrics"
song: "A for Annaatthe"
image: ../../images/albumart/annaatthe.jpg
date: 2021-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NA_AMKJ_oF4"
type: "mass"
singers:
  - Arivu
  - Rajinikanth
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naadi nerumbu murukka murukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadi nerumbu murukka murukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Raththam moththam kodhikka kodhikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raththam moththam kodhikka kodhikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Arangam muzhukka therikka therikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arangam muzhukka therikka therikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodanguthu omgara koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodanguthu omgara koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">A for annaatthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A for annaatthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedra nee molamtha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedra nee molamtha"/>
</div>
<div class="lyrico-lyrics-wrapper">Non stop aattam paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Non stop aattam paar"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu folk rapper superstar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu folk rapper superstar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala pala veeti sokka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala pala veeti sokka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naatu puraththu aalu makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatu puraththu aalu makka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalamurai thaangi nirkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalamurai thaangi nirkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Serthu vecha peru makka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Serthu vecha peru makka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thara thara sambavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara thara sambavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani oruvanin thandavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani oruvanin thandavam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhi vidu vazhi vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhi vidu vazhi vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu entry of the country singam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu entry of the country singam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hahahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera dheerana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera dheerana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera naa"/>
</div>
<div class="lyrico-lyrics-wrapper">A for annaatthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A for annaatthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera dheerana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera dheerana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedra nee molamtha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedra nee molamtha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheeraana dheerana dheera dheera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheerana dheera dheera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeraana dheerana dheera dheerana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheerana dheera dheerana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheera dheera dheerana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheera dheera dheerana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Narigal aadi vettai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narigal aadi vettai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharisanam illai kaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharisanam illai kaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Urimaiya mella naatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urimaiya mella naatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalam erangu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalam erangu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thadai unakku oru saattai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai unakku oru saattai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhugu irukkudhu kaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhugu irukkudhu kaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravu irukkudhu ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu irukkudhu ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhammu irukku vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhammu irukku vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">A for annaatthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A for annaatthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedra nee molamtha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedra nee molamtha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera dheerana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera dheerana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera dheerana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera dheerana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thani maramena ninnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thani maramena ninnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Panivadhu illai thanmanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panivadhu illai thanmanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sadhivalai erithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sadhivalai erithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimirndhu nirpathu badhiladi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimirndhu nirpathu badhiladi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neru neruppena mun kovam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neru neruppena mun kovam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadai ethirthida vandhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadai ethirthida vandhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru mudivila nee nindraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru mudivila nee nindraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiri thavudu podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiri thavudu podi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irukaram vazhi thadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukaram vazhi thadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu vazhi poruthida vizhithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu vazhi poruthida vizhithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini unakku ezhithinai sarithiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini unakku ezhithinai sarithiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi adhura adhira ethiri sedhara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi adhura adhira ethiri sedhara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pori parakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pori parakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Por kalam idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Por kalam idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arathinai uruvaakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arathinai uruvaakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru pudhu yugam unadhaakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru pudhu yugam unadhaakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini evan thadupathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini evan thadupathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai pagaipathu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai pagaipathu vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera dheerana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera dheerana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera naa"/>
</div>
<div class="lyrico-lyrics-wrapper">A for annaatthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A for annaatthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera dheerana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera dheerana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedra nee molamtha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedra nee molamtha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera dheerana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera dheerana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Non stop aattam paar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Non stop aattam paar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera dheerana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera dheerana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeraana dheera naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraana dheera naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu folk rapper superstar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu folk rapper superstar"/>
</div>
</pre>
