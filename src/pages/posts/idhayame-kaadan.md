---
title: "idhayame song lyrics"
album: "Kaadan"
artist: "Shantanu Moitra"
lyricist: "Vanamali"
director: "Prabhu Solomon"
path: "/albums/kaadan-song-lyrics"
song: "Idhayame"
image: ../../images/albumart/kaadan.jpg
date: 2021-03-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0Uk8TVKrLg8"
type: "Affection"
singers:
  - Javed Ali
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">idhayame...... eriyuthe.....
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayame...... eriyuthe....."/>
</div>
<div class="lyrico-lyrics-wrapper">uyirum karugiye ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirum karugiye ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">veesum kaatha kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veesum kaatha kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">vidai vanthu sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidai vanthu sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">aadhi malarai kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadhi malarai kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasam vanthu sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasam vanthu sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">oodum aatha kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodum aatha kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">alai vanthu sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alai vanthu sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">oodum megam malaiyai kelu.......
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodum megam malaiyai kelu......."/>
</div>
<div class="lyrico-lyrics-wrapper">kandu vanthu kaathil sollum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandu vanthu kaathil sollum "/>
</div>
<div class="lyrico-lyrics-wrapper">ethu varai pagai thodarnthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethu varai pagai thodarnthidum"/>
</div>
<div class="lyrico-lyrics-wrapper">yar solluvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar solluvar"/>
</div>
<div class="lyrico-lyrics-wrapper">athu varai intha siraiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu varai intha siraiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">yar thetruvaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yar thetruvaar"/>
</div>
<div class="lyrico-lyrics-wrapper">intha kurai ellam nirai aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha kurai ellam nirai aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalam varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalam varai"/>
</div>
<div class="lyrico-lyrics-wrapper">patta karai maranithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="patta karai maranithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">thinam valarpirai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinam valarpirai "/>
</div>
<div class="lyrico-lyrics-wrapper">endro.... endro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endro.... endro"/>
</div>
<div class="lyrico-lyrics-wrapper">lalalalare lalalalare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lalalalare lalalalare"/>
</div>
<div class="lyrico-lyrics-wrapper">lalalalare lalalalare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lalalalare lalalalare"/>
</div>
<div class="lyrico-lyrics-wrapper">ohohohohoh....ohohoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ohohohohoh....ohohoh"/>
</div>
</pre>
