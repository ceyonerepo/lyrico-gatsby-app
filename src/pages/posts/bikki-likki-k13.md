---
title: "bikki likki song lyrics"
album: "K 13"
artist: "Sam C.S."
lyricist: "Sam. C.S - Barath Neelakantan"
director: "Barath Neelakantan"
path: "/albums/k13-lyrics"
song: "Bikki Likki"
image: ../../images/albumart/k13.jpg
date: 2019-05-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NJ6gpiG9QnY"
type: "happy"
singers:
  - Nikhita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bikki Likki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bikki Likki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bikki Likki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bikki Likki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ki Ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki Ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Yera Yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Yera Yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Aasai Thee Thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Aasai Thee Thee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bikki Likki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bikki Likki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bikki Likki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bikki Likki"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Yera Yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Yera Yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Aasai Thee Thee Thee Thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Aasai Thee Thee Thee Thee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bikki Likki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bikki Likki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bikki Likki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bikki Likki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ki Ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki Ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Neram Yera Yera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neram Yera Yera"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjil Aasai Thee Thee Thee Thee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjil Aasai Thee Thee Thee Thee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Get High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get High"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Marakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Marakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnil Enna Thedaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnil Enna Thedaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Get High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get High"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Ennai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ennai Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanil Neendha Pogadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanil Neendha Pogadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hangout
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hangout"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketu Asaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketu Asaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Loopil Loopil Solladha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loopil Loopil Solladha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bikki Likki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bikki Likki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bikki Likki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bikki Likki"/>
</div>
<div class="lyrico-lyrics-wrapper">Bikki Likki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bikki Likki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ki Ki Ki Ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ki Ki Ki Ki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanin Meedheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanin Meedheri"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Thaandi Edhai Thedinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Thaandi Edhai Thedinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Povom Udal Neengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povom Udal Neengi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhai Vendi Ennai Naadinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhai Vendi Ennai Naadinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanin Meedheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanin Meedheri"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Thaandi Edhai Thedinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Thaandi Edhai Thedinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Povom Udal Neengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povom Udal Neengi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kangal Yenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal Yenga"/>
</div>
<div class="lyrico-lyrics-wrapper">Narambim Vazhiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narambim Vazhiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuvai Ullukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuvai Ullukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagam Theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagam Theera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thevai Koora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevai Koora"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravu Nodigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Nodigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku Irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku Irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasai Theera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai Theera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Life Ver Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life Ver Illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Theervillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theervillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">High Aagidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="High Aagidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Naanaagidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Naanaagidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Sernthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Sernthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan Yeridu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Yeridu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanin Meedheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanin Meedheri"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Thaandi Edhai Thedinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Thaandi Edhai Thedinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Povom Udal Neengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povom Udal Neengi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhai Vendi Ennai Naadinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhai Vendi Ennai Naadinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanin Meedheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanin Meedheri"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Thaandi Edhai Thedinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Thaandi Edhai Thedinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Povom Udal Neengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povom Udal Neengi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanin Meedheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanin Meedheri"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Thaandi Edhai Thedinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Thaandi Edhai Thedinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Povom Udal Neengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povom Udal Neengi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhai Vendi Ennai Naadinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhai Vendi Ennai Naadinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanin Meedheri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanin Meedheri"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhai Thaandi Edhai Thedinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhai Thaandi Edhai Thedinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Povom Udal Neengi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povom Udal Neengi"/>
</div>
</pre>
