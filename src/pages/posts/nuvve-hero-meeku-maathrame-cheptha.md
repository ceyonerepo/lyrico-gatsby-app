---
title: "nuvve hero song lyrics"
album: "Meeku Maathrame Cheptha"
artist: "Sivakumar"
lyricist: "Shammeer Sultan - Rakendu Mouli"
director: "Shammeer Sultan"
path: "/albums/meeku-maathrame-cheptha-lyrics"
song: "Nuvve Hero"
image: ../../images/albumart/meeku-maathrame-cheptha.jpg
date: 2019-11-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/8o9N_aVZf0w"
type: "happy"
singers:
  - Anurag Kulkarni
  - Rahul Sipligunj
  - AsurA
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raavoy Raavoy Katha Chebutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavoy Raavoy Katha Chebutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeku Maathrame Cheputhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeku Maathrame Cheputhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chevilo Chinnaga Katha Chebutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chevilo Chinnaga Katha Chebutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeku Maathrame Cheputhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeku Maathrame Cheputhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Lifey Ohh Kathe Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Lifey Ohh Kathe Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kathaki Nuvve Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kathaki Nuvve Hero"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Katha Cheppu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Katha Cheppu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Katha Vippu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Katha Vippu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Katha Naadiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Katha Naadiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Katha Neediga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Katha Neediga"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha Chepukuntu Gadipeddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha Chepukuntu Gadipeddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalashepam Cheseddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalashepam Cheseddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavoy Raavoy Raa Raa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavoy Raavoy Raa Raa "/>
</div>
<div class="lyrico-lyrics-wrapper">Raavoy Raavoy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavoy Raavoy"/>
</div>
<div class="lyrico-lyrics-wrapper">Neethi Yedo Dharamamedo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neethi Yedo Dharamamedo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyayam Cheppaga Nenevaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nyayam Cheppaga Nenevaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchodevadu Cheddodevadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchodevadu Cheddodevadu "/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Kathalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Kathalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavoy Raavoy Katha Chebutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavoy Raavoy Katha Chebutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeku Maathrame Cheputhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeku Maathrame Cheputhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chevilo Chinnaga Katha Chebutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chevilo Chinnaga Katha Chebutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Meeku Maathrame Cheputhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meeku Maathrame Cheputhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anaganaganaga O Rajyamanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anaganaganaga O Rajyamanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Rajyaniki Thala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Rajyaniki Thala "/>
</div>
<div class="lyrico-lyrics-wrapper">Thirigina Raju Anta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirigina Raju Anta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Thikka Dorala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Thikka Dorala "/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu Mokke Manushulantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu Mokke Manushulantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaadi Thaladanne 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadi Thaladanne "/>
</div>
<div class="lyrico-lyrics-wrapper">Puli Laanti Yodhulanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli Laanti Yodhulanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ala Cheppu Prathi Kathalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ala Cheppu Prathi Kathalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Garalamedo Varjanedo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garalamedo Varjanedo"/>
</div>
<div class="lyrico-lyrics-wrapper">Tappu Edo Oppu Edo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tappu Edo Oppu Edo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manthramedo Mosamedo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manthramedo Mosamedo"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchodevado Cheddodevado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchodevado Cheddodevado"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathalaloni Pathralevaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathalaloni Pathralevaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Daagi Unna Losugulenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daagi Unna Losugulenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Nijam Manaku Theliyaduga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijam Manaku Theliyaduga"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kannulalo Thana Katha Veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannulalo Thana Katha Veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thana Kannulalo Nee Katha Veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thana Kannulalo Nee Katha Veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Katha Thana Kannulatho Chusthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Katha Thana Kannulatho Chusthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kathaki Nyayame Veru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kathaki Nyayame Veru"/>
</div>
<div class="lyrico-lyrics-wrapper">Arey Kathalalone Kathanu Maarche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arey Kathalalone Kathanu Maarche"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathi Laani Katha Chebutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathi Laani Katha Chebutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhakudalle Kaadu Neeku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhakudalle Kaadu Neeku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalamai Katha Varnistha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalamai Katha Varnistha"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodo Chevvu Vinakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodo Chevvu Vinakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Maathrame Cheputhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Maathrame Cheputhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavvi Thavvi Vethiki Choodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavvi Thavvi Vethiki Choodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Daaniloni Lothentha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daaniloni Lothentha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeku Naaku Polikentha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeku Naaku Polikentha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalochanalo Theda Yentha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalochanalo Theda Yentha"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha Kathaku Maarpentha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha Kathaku Maarpentha"/>
</div>
<div class="lyrico-lyrics-wrapper">Manchi Cheduku Bhedamentha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manchi Cheduku Bhedamentha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kick Leni Kathalalo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kick Leni Kathalalo "/>
</div>
<div class="lyrico-lyrics-wrapper">Yemundilera Goppa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemundilera Goppa"/>
</div>
<div class="lyrico-lyrics-wrapper">Biginchi Choodu Cheppukora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biginchi Choodu Cheppukora"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathanu Chivari Daaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathanu Chivari Daaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thera Dinchina Taruvatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thera Dinchina Taruvatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Jeevithame Oka Katha Kaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Jeevithame Oka Katha Kaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Thera Dinchina Taruvatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Thera Dinchina Taruvatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Jeevithame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Jeevithame"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Lifey Ohh Kathe Kadhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Lifey Ohh Kathe Kadhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kathaki Nuvve Hero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kathaki Nuvve Hero"/>
</div>
1 2 3 <div class="lyrico-lyrics-wrapper">To The 4
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To The 4"/>
</div>
<div class="lyrico-lyrics-wrapper">We'll Sing A Little Low
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We'll Sing A Little Low"/>
</div>
<div class="lyrico-lyrics-wrapper">To Bring Back The Flow
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="To Bring Back The Flow"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedemaina Arey Yevaremanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedemaina Arey Yevaremanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kathanu Yevaru Vinanantunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kathanu Yevaru Vinanantunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Nammakam Vodalaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Nammakam Vodalaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedemaina Yedemaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedemaina Yedemaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kathalo Nuvve Thopuviraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kathalo Nuvve Thopuviraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kathalo Nuvve Heroviraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kathalo Nuvve Heroviraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Kathalo Nuvve Thopuviraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Kathalo Nuvve Thopuviraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kathalo Nuvve Rajuviraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kathalo Nuvve Rajuviraa"/>
</div>
</pre>
