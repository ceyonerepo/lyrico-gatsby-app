---
title: "Parinda song lyrics"
album: "Saina"
artist: "Amaal Mallik"
lyricist: "Manoj Muntashir"
director: "Amole Gupte"
path: "/albums/saina-lyrics"
song: "Parinda"
image: ../../images/albumart/saina.jpg
date: 2021-03-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/1JPlmLKsrB4"
type: "Mass"
singers:
  - Amaal Mallik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Jalna bhujna, bhujke jalna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jalna bhujna, bhujke jalna"/>
</div>
<div class="lyrico-lyrics-wrapper">Marna jeena, marke jeena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marna jeena, marke jeena"/>
</div>
<div class="lyrico-lyrics-wrapper">Mangne wali cheez nahi yeh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mangne wali cheez nahi yeh"/>
</div>
<div class="lyrico-lyrics-wrapper">Mauka uska jisne chheena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mauka uska jisne chheena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Girna uthna, uthke chalna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girna uthna, uthke chalna"/>
</div>
<div class="lyrico-lyrics-wrapper">Chad ja ambar jeena jeena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chad ja ambar jeena jeena"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaad rahe yeh shart safar ki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaad rahe yeh shart safar ki"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichhe mud ke dekh kabhi na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichhe mud ke dekh kabhi na"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jeet ka junoon hai toh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeet ka junoon hai toh"/>
</div>
<div class="lyrico-lyrics-wrapper">Haar sochna kyun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haar sochna kyun"/>
</div>
<div class="lyrico-lyrics-wrapper">Jab zindagi hai ek hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab zindagi hai ek hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Do baar sochna kyun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do baar sochna kyun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main parinda kyun banun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main parinda kyun banun"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe aasmaan ban'na hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe aasmaan ban'na hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Main ik panna kyun rahun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main ik panna kyun rahun"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe dastaan ban'na hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe dastaan ban'na hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Main parinda kyun banun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main parinda kyun banun"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe aasmaan ban'na hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe aasmaan ban'na hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koyi toh wajah hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koyi toh wajah hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo zid pe adi hai yeh dhadkane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo zid pe adi hai yeh dhadkane"/>
</div>
<div class="lyrico-lyrics-wrapper">Yahin toh wajah hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yahin toh wajah hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kiya jo kisi ne nahi hum kare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiya jo kisi ne nahi hum kare"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Talwaar haath hai mein tere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Talwaar haath hai mein tere"/>
</div>
<div class="lyrico-lyrics-wrapper">De maar sochna kyun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="De maar sochna kyun"/>
</div>
<div class="lyrico-lyrics-wrapper">Jab zindagi hai ek hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jab zindagi hai ek hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Do baar sochna kyun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Do baar sochna kyun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main parinda kyun banun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main parinda kyun banun"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe aasmaan ban'na hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe aasmaan ban'na hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Main ik panna kyun rahun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main ik panna kyun rahun"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhe dastaan ban'na hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhe dastaan ban'na hai"/>
</div>
</pre>
