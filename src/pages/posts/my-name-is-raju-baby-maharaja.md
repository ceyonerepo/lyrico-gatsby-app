---
title: "my name is raju baby song lyrics"
album: "Maharaja"
artist: "D. Imman"
lyricist: "Snehan"
director: "D. Manoharan"
path: "/albums/maharaja-lyrics"
song: "My Name Is Raju Baby"
image: ../../images/albumart/maharaja.jpg
date: 2011-12-30
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OfyXqGCh6D4"
type: "happy"
singers:
  - Benny Dayal
  - Nassar
  - Sricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">My name is Raju baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My name is Raju baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Raju baby Raju baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raju baby Raju baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Girls call me Joju kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girls call me Joju kutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Joju kutti Joju kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joju kutti Joju kutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinandhorum engal kadhavai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinandhorum engal kadhavai "/>
</div>
<div class="lyrico-lyrics-wrapper">thattuthoru fashion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thattuthoru fashion"/>
</div>
<div class="lyrico-lyrics-wrapper">Vingnanam solli tharuthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vingnanam solli tharuthe "/>
</div>
<div class="lyrico-lyrics-wrapper">adhilum oru fusion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhilum oru fusion"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu wonderful world-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu wonderful world-u"/>
</div>
<div class="lyrico-lyrics-wrapper">inge aanum penum gold-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inge aanum penum gold-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu youthful-aana field-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu youthful-aana field-u"/>
</div>
<div class="lyrico-lyrics-wrapper">nanga epodhume bold-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanga epodhume bold-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My name is Raju baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My name is Raju baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Raju baby Raju baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raju baby Raju baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Girls call me Joju kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girls call me Joju kutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Joju kutti Joju kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joju kutti Joju kutti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unlimited sambalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unlimited sambalam"/>
</div>
<div class="lyrico-lyrics-wrapper">IT field kodukudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IT field kodukudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhanaale life-uthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhanaale life-uthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">thalakelaaga maarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalakelaaga maarudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uzhagangal kaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uzhagangal kaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Urchaagam paiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urchaagam paiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukindra varaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukindra varaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai piravi podhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai piravi podhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha vaanam romba palasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha vaanam romba palasu"/>
</div>
<div class="lyrico-lyrics-wrapper">indha boomi romba palasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha boomi romba palasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idha acid oothi alasau
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idha acid oothi alasau"/>
</div>
<div class="lyrico-lyrics-wrapper">naam seivom pudhusaathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam seivom pudhusaathaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My name is Raju baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My name is Raju baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Raju baby Raju baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raju baby Raju baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Girls call me Joju kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girls call me Joju kutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Joju kutti Joju kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joju kutti Joju kutti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">what you gonna do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what you gonna do"/>
</div>
<div class="lyrico-lyrics-wrapper">what you gonna do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what you gonna do"/>
</div>
<div class="lyrico-lyrics-wrapper">When the crazy god 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When the crazy god "/>
</div>
<div class="lyrico-lyrics-wrapper">comes just after you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="comes just after you"/>
</div>
<div class="lyrico-lyrics-wrapper">ease have fun ease have fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ease have fun ease have fun"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chandraayan lalathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandraayan lalathil"/>
</div>
<div class="lyrico-lyrics-wrapper">saathiram yenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saathiram yenada"/>
</div>
<div class="lyrico-lyrics-wrapper">Youth edrum bestu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Youth edrum bestu da"/>
</div>
<div class="lyrico-lyrics-wrapper">idha old ellam wastu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idha old ellam wastu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Fantastic thalaimurai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fantastic thalaimurai"/>
</div>
<div class="lyrico-lyrics-wrapper">over take pannudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="over take pannudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Jealousy -yil perunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jealousy -yil perunga"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam enanamo pesdhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam enanamo pesdhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu one-ve tracku illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu one-ve tracku illa"/>
</div>
<div class="lyrico-lyrics-wrapper">un over speedu tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un over speedu tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru speedu breaker vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru speedu breaker vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">un nilamai enna kandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nilamai enna kandha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My name is Raju baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My name is Raju baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Raju baby Raju baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raju baby Raju baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Girls call me Joju kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Girls call me Joju kutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Joju kutti Joju kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Joju kutti Joju kutti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinandhorum engal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinandhorum engal "/>
</div>
<div class="lyrico-lyrics-wrapper">kadhavai thattuthoru fashion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhavai thattuthoru fashion"/>
</div>
<div class="lyrico-lyrics-wrapper">Vingnanam solli tharuthe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vingnanam solli tharuthe "/>
</div>
<div class="lyrico-lyrics-wrapper">adhilum oru fusion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhilum oru fusion"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu wonderful world-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu wonderful world-u"/>
</div>
<div class="lyrico-lyrics-wrapper">inge aanum penum gold-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inge aanum penum gold-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu youthful-aana field-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu youthful-aana field-u"/>
</div>
<div class="lyrico-lyrics-wrapper">nanga epodhume bold-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanga epodhume bold-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">what you gonna do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what you gonna do"/>
</div>
<div class="lyrico-lyrics-wrapper">what you gonna do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="what you gonna do"/>
</div>
<div class="lyrico-lyrics-wrapper">When the crazy god 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When the crazy god "/>
</div>
<div class="lyrico-lyrics-wrapper">comes just after you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="comes just after you"/>
</div>
<div class="lyrico-lyrics-wrapper">ease have fun ease have fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ease have fun ease have fun"/>
</div>
</pre>
