---
title: "aagave nuvvagave song lyrics"
album: "Paagal"
artist: "Radhan"
lyricist: "Krishna Kanth"
director: "Naressh Kuppili"
path: "/albums/paagal-lyrics"
song: "Aagave Nuvvagave"
image: ../../images/albumart/paagal.jpg
date: 2021-08-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/R2cn8YGWfYc"
type: "sad"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aagave Nuvvagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagave Nuvvagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Poye Oopire Nuvvaapave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poye Oopire Nuvvaapave"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagave Nuvvagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagave Nuvvagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Poye Oopire Nuvvaapave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poye Oopire Nuvvaapave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagave Nuvvagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagave Nuvvagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Poye Oopire Nuvvaapave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poye Oopire Nuvvaapave"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagave Nuvvagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagave Nuvvagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Poye Oopire Nuvvaapave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poye Oopire Nuvvaapave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheragani Nee Navvulane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheragani Nee Navvulane"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuvanuvu Nimpukunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuvanuvu Nimpukunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathinimisham Guruthulane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathinimisham Guruthulane"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruvakane Brathukuthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruvakane Brathukuthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulasale Nidhurodhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulasale Nidhurodhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekorake Vethukuthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekorake Vethukuthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhileluthu Parigedithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhileluthu Parigedithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thegipoye Bandhamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegipoye Bandhamenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thegipoye Bandhamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegipoye Bandhamenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thegipoye Bandhamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegipoye Bandhamenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammalle Nudure Thaaketi Cheye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammalle Nudure Thaaketi Cheye"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarchesi Raathe Vedhinchane Nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarchesi Raathe Vedhinchane Nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Edaaranti Edhake Praanaalu Pose
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edaaranti Edhake Praanaalu Pose"/>
</div>
<div class="lyrico-lyrics-wrapper">Preminchamante Gunde Koshaavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Preminchamante Gunde Koshaavule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugusthunte Kadhalannee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugusthunte Kadhalannee"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaachaanu Baadhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaachaanu Baadhane"/>
</div>
<div class="lyrico-lyrics-wrapper">Reppallo Nilipaane Kanneetine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reppallo Nilipaane Kanneetine"/>
</div>
<div class="lyrico-lyrics-wrapper">Naapaina Choopinchaavu Enaleni Premane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naapaina Choopinchaavu Enaleni Premane"/>
</div>
<div class="lyrico-lyrics-wrapper">Virahaala Nadhilo Visireyake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virahaala Nadhilo Visireyake"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Naa Venakane Nadichaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Naa Venakane Nadichaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Brathukunu Nadipaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Brathukunu Nadipaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Kalalanu Vidichaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Kalalanu Vidichaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenundhe Nee Korake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenundhe Nee Korake"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Ningini Choopaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Ningini Choopaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Egaradame Nerpaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Egaradame Nerpaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Rekkalane Virichaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rekkalane Virichaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Chelime Marichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Chelime Marichi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheragani Nee Navvulane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheragani Nee Navvulane"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuvanuvu Nimpukunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuvanuvu Nimpukunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathi Nimisham Guruthulane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathi Nimisham Guruthulane"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruvakane Brathukuthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruvakane Brathukuthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulasale Nidhurodhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulasale Nidhurodhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekorake Vethukuthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekorake Vethukuthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhileluthu Parigedithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhileluthu Parigedithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thegipoye Bandhamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegipoye Bandhamenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Addamlaa Edhurai Choopaavu Nanne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Addamlaa Edhurai Choopaavu Nanne"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddantu Raaye Visiraavu Naapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddantu Raaye Visiraavu Naapai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve Kaaka Everu Naakantu Lere
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve Kaaka Everu Naakantu Lere"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Dhyaasalone Nanne Munchaavule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Dhyaasalone Nanne Munchaavule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvu Leni Kshaname Naakinka Sunyame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvu Leni Kshaname Naakinka Sunyame"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Venta Lene Ledhu Santoshame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Venta Lene Ledhu Santoshame"/>
</div>
<div class="lyrico-lyrics-wrapper">Bharinchaane Innaallu Telisinaa Mosame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bharinchaane Innaallu Telisinaa Mosame"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakannaa Ninne Baagaa Nammaanule
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakannaa Ninne Baagaa Nammaanule"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey… O Nijamuni Cheppevaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey… O Nijamuni Cheppevaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannodhilithe Nuvvainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannodhilithe Nuvvainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye Sukhamugaa Untaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sukhamugaa Untaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naavalle Kaadasale Ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naavalle Kaadasale Ee "/>
</div>
<div class="lyrico-lyrics-wrapper">Nadumuna Dhooraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadumuna Dhooraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheripokatiga Cheraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheripokatiga Cheraale"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Munupati Kaalaale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Munupati Kaalaale "/>
</div>
<div class="lyrico-lyrics-wrapper">The Adugu Kadhipe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The Adugu Kadhipe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheragani Nee Navvulane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheragani Nee Navvulane"/>
</div>
<div class="lyrico-lyrics-wrapper">Anuvanuvu Nimpukunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anuvanuvu Nimpukunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathinimisham Guruthulane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathinimisham Guruthulane"/>
</div>
<div class="lyrico-lyrics-wrapper">Maruvakane Brathukuthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maruvakane Brathukuthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulasale Nidhurodhile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulasale Nidhurodhile"/>
</div>
<div class="lyrico-lyrics-wrapper">Neekorake Vethukuthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekorake Vethukuthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vadhileluthu Parigedithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vadhileluthu Parigedithe"/>
</div>
<div class="lyrico-lyrics-wrapper">Thegipoye Bandhamenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thegipoye Bandhamenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagave Nuvvagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagave Nuvvagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Poye Oopire Nuvvaapave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poye Oopire Nuvvaapave"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagave Nuvvagave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagave Nuvvagave"/>
</div>
<div class="lyrico-lyrics-wrapper">Poye Oopire Nuvvaapave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poye Oopire Nuvvaapave"/>
</div>
</pre>
