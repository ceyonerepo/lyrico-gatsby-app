---
title: "mathiya seraiyila song lyrics"
album: "Vada Chennai"
artist: "Santhosh Narayanan"
lyricist: "Arivu"
director: "Vetrimaaran"
path: "/albums/vada-chennai-lyrics"
song: "Mathiya Seraiyila"
image: ../../images/albumart/vada-chennai.jpg
date: 2018-10-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/AoTYJRXuyY0"
type: "sad"
singers:
  - Arivu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kallu odaikkiren jailula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu odaikkiren jailula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu muzhikkuren iravula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu muzhikkuren iravula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappichu pogavum vazhiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappichu pogavum vazhiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppa kedaikkumo viduthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppa kedaikkumo viduthala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallu odaikkiren jailula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu odaikkiren jailula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu muzhikkuren iravula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu muzhikkuren iravula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappichu pogavum vazhiyilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappichu pogavum vazhiyilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppa kedaikkumo viduthala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppa kedaikkumo viduthala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathiya seraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathiya seraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan lathiyum udhaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan lathiyum udhaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathiya seraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathiya seraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan lathiyum udhaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan lathiyum udhaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaameenu jaameenuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaameenu jaameenuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanginu ponga veliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanginu ponga veliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga michamirundha kudthuduvaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga michamirundha kudthuduvaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raathirikkuthaan kaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathirikkuthaan kaliya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaameenu jaameenuthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaameenu jaameenuthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanginu ponga veliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanginu ponga veliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga michamirundha kudthuduvaanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga michamirundha kudthuduvaanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Raathirikkuthaan kaliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathirikkuthaan kaliya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vambuthumbu pannuravanlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambuthumbu pannuravanlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedakkuraan ozhungaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedakkuraan ozhungaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa nenju kozhuppu eduthavandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa nenju kozhuppu eduthavandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkuraan erumbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkuraan erumbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vambuthumbu pannuravanlaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambuthumbu pannuravanlaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedakkuraan ozhungaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedakkuraan ozhungaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippa nenju kozhuppu eduthavandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippa nenju kozhuppu eduthavandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkuraan erumbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkuraan erumbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathiya seraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathiya seraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan lathiyum udhaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan lathiyum udhaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathiya seraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathiya seraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan lathiyum udhaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan lathiyum udhaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jailukku puliyamaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jailukku puliyamaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Nezhalu kodukkudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nezhalu kodukkudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anga olinjikittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anga olinjikittu "/>
</div>
<div class="lyrico-lyrics-wrapper">pullainga ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pullainga ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottalam pirikkudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottalam pirikkudhaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoongasolla kosungatholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongasolla kosungatholla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enganu ketka yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enganu ketka yaarumilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoongasolla kosungatholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoongasolla kosungatholla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enganu ketka yaarumilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enganu ketka yaarumilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vathipetti ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vathipetti ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan thatturen eppo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan thatturen eppo "/>
</div>
<div class="lyrico-lyrics-wrapper">thorakkum ulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thorakkum ulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Seththu seththu pozhaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seththu seththu pozhaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaththukutti mirugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaththukutti mirugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathiya seraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathiya seraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan lathiyum udhaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan lathiyum udhaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathiya seraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathiya seraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan lathiyum udhaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan lathiyum udhaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallu odaikkiren jailula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu odaikkiren jailula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu muzhikkuren iravula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu muzhikkuren iravula"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnu manasula edamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnu manasula edamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnaankanni keera pudikkala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnaankanni keera pudikkala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Takkaru naanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaru naanunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga numberu thaanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga numberu thaanunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Takkaru naanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takkaru naanunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Inga numberu thaanunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inga numberu thaanunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathiya seraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathiya seraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan lathiyum udhaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan lathiyum udhaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathiya seraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathiya seraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan lathiyum udhaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan lathiyum udhaiyila"/>
</div>
</pre>
