---
title: "ammamma song lyrics"
album: "Veerapandiyapuram"
artist: "Jai"
lyricist: "Yugabharathi"
director: "Suseenthiran"
path: "/albums/veerapandiyapuram-lyrics"
song: "Ammamma"
image: ../../images/albumart/veerapandiyapuram.jpg
date: 2022-02-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/TAWPFbYL6To"
type: "love"
singers:
  - Haricharan
  - Archana Sabesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ammamma ennaanu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammamma ennaanu theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam boologam puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam boologam puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnunna neethanae ulagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnunna neethanae ulagula"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannil paarthenae kaduvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannil paarthenae kaduvula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalkanda nee pesum azhagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalkanda nee pesum azhagula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalli poo mull neekkum nodiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalli poo mull neekkum nodiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sil vando unnoda nizhalula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil vando unnoda nizhalula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thean unna thavam kidakku usurula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean unna thavam kidakku usurula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru vazhiyila kodi vizhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vazhiyila kodi vizhakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhuthura unna yaaru jeikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhuthura unna yaaru jeikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Mala aruviya pola neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala aruviya pola neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam kaattum devadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam kaattum devadhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinasari unnai paarthu rasikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinasari unnai paarthu rasikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal varuvadha ellaarum viyakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal varuvadha ellaarum viyakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilavarasigal ellaam unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavarasigal ellaam unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai illai eedamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai illai eedamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee mayil aadum paaraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mayil aadum paaraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nada podum poonguyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nada podum poonguyilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai paatta paaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai paatta paaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeru veyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeru veyilu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee malakkoyil serayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee malakkoyil serayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuviyadho kai viralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuviyadho kai viralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai kaaval kaakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai kaaval kaakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhi tamizhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhi tamizhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammamma ennaanu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammamma ennaanu theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam boologam puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam boologam puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponnunna neethanae ulagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnunna neethanae ulagula"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannil paarthenae kaduvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannil paarthenae kaduvula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey uchchi uchchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey uchchi uchchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Velli nilla kannaanadho hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velli nilla kannaanadho hoi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kanda vekkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kanda vekkathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallandhadho ho hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallandhadho ho hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magarasi unnoda mugamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magarasi unnoda mugamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mala ooththa sandhosam tharumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mala ooththa sandhosam tharumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pul poondum un pechil poovaagumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pul poondum un pechil poovaagumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aalillaa kaattukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalillaa kaattukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Anandha thooral thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anandha thooral thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un anba naanum solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un anba naanum solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharuvaar yaar vaarthai kadanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharuvaar yaar vaarthai kadanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee mayil aadum paaraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mayil aadum paaraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nada podum poonguyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nada podum poonguyilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai paatta paaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai paatta paaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeru veyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeru veyilu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee malakkoyil serayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee malakkoyil serayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuviyadho kai viralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuviyadho kai viralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai kaaval kaakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai kaaval kaakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhi tamizhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhi tamizhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru vazhiyilae kodi vizhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru vazhiyilae kodi vizhakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhuthura unna yaara jeikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhuthura unna yaara jeikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Malai aruviya pola neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malai aruviya pola neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paasam kaattum devadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam kaattum devadhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinasari unnai paarthu rasikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinasari unnai paarthu rasikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagal varuvadha ellaarum viyakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagal varuvadha ellaarum viyakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilavarasigal ellam unakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilavarasigal ellam unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Illai illai eedamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai illai eedamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee mayil aadum paaraiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee mayil aadum paaraiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Nada podum poonguyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nada podum poonguyilu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai paatta paaduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai paatta paaduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeru veyilu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeru veyilu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee malakkoyil serayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee malakkoyil serayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuviyadho kai viralu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuviyadho kai viralu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai kaaval kaakkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai kaaval kaakkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadhi tamizhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadhi tamizhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammamma ennaanu theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammamma ennaanu theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagayam boologam puriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam boologam puriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aannunna neethanae ulagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aannunna neethanae ulagula"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannil paarthenae kaduvula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannil paarthenae kaduvula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalkanda nee pesum azhagula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalkanda nee pesum azhagula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalli poo mull neengum nodiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalli poo mull neengum nodiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sil vando unnoda nizhalula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil vando unnoda nizhalula"/>
</div>
<div class="lyrico-lyrics-wrapper">Thean unna thavam kidakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thean unna thavam kidakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavam kidakku usurula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavam kidakku usurula"/>
</div>
</pre>
