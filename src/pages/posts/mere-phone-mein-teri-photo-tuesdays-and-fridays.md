---
title: "mere phone mein teri photo song lyrics"
album: "Tuesdays And Fridays"
artist: "Tony Kakkar"
lyricist: "Tony Kakkar"
director: "Taranveer Singh"
path: "/albums/tuesdays-and-fridays-lyrics"
song: "Mere Phone Mein Teri Photo"
image: ../../images/albumart/tuesdays-and-fridays.jpg
date: 2021-02-19
lang: hindi
youtubeLink: "https://www.youtube.com/embed/TGr4rEtPtDM"
type: "love"
singers:
  - Neha Kakkar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raat bhar tanha rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat bhar tanha rahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Main tujhe miss karti rahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main tujhe miss karti rahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Tasveer teri dekh kar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tasveer teri dekh kar"/>
</div>
<div class="lyrico-lyrics-wrapper">Aankhein bolti par dil maun hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aankhein bolti par dil maun hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere phone mein teri photo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere phone mein teri photo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy puchhe beta kaun hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy puchhe beta kaun hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere phone mein teri photo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere phone mein teri photo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy poochhe beta kaun hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy poochhe beta kaun hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Raat bhar tu soyi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat bhar tu soyi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri kis'se chatting on hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri kis'se chatting on hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dekho khumari, tumko hi maangte hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekho khumari, tumko hi maangte hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Khud se bhi jyada, tumko hi chahte hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khud se bhi jyada, tumko hi chahte hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Na woh Ranbir hai, na woh Shahid hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na woh Ranbir hai, na woh Shahid hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Na woh Kohli naa hi John hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na woh Kohli naa hi John hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere phone mein teri photo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere phone mein teri photo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy puchhe beta kaun hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy puchhe beta kaun hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere phone mein teri photo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere phone mein teri photo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy poochhe beta kaun hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy poochhe beta kaun hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Raat bhar tu soyi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat bhar tu soyi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri kis'se chatting on hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri kis'se chatting on hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa nikal ke saamne tasveer se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa nikal ke saamne tasveer se"/>
</div>
<div class="lyrico-lyrics-wrapper">Tum mile ho jaane jaan taqdeer se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tum mile ho jaane jaan taqdeer se"/>
</div>
<div class="lyrico-lyrics-wrapper">Khwab bhi aane lage hain ajeeb se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khwab bhi aane lage hain ajeeb se"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekhun main tumko sada kareeb se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekhun main tumko sada kareeb se"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kyun bekarari badhti ja rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kyun bekarari badhti ja rahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaan meri tum bin tadapti ja rahi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaan meri tum bin tadapti ja rahi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Yo Yo hai woh, na woh hai Badshah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Yo Yo hai woh, na woh hai Badshah"/>
</div>
<div class="lyrico-lyrics-wrapper">Na Bieber na Akon hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na Bieber na Akon hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere phone mein teri photo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere phone mein teri photo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy puchhe beta kaun hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy puchhe beta kaun hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere phone mein teri photo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere phone mein teri photo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mummy poochhe beta kaun hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mummy poochhe beta kaun hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Raat bhar tu soyi nahi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat bhar tu soyi nahi"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri kis se chatting on hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri kis se chatting on hai"/>
</div>
</pre>
