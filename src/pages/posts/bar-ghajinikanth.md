---
title: "bar song lyrics"
album: "Ghajinikanth"
artist: "Balamurali Balu"
lyricist: "Ku Karthik"
director: "Santhosh P. Jayakumar"
path: "/albums/ghajinikanth-lyrics"
song: "Bar"
image: ../../images/albumart/ghajinikanth.jpg
date: 2018-08-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/RzXtiqf3Oms"
type: "happy"
singers:
  - Rahul Nambiar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vehnam valikidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vehnam valikidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum vitturu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum vitturu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuku mehla naan azhudhuruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuku mehla naan azhudhuruven"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting father’u loving daughter’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting father’u loving daughter’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvula en love’u in danger’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvula en love’u in danger’u"/>
</div>
<div class="lyrico-lyrics-wrapper">En life’u poker’u game’uh pochu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En life’u poker’u game’uh pochu da"/>
</div>
<div class="lyrico-lyrics-wrapper">Naandhane joker card’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naandhane joker card’u"/>
</div>
<div class="lyrico-lyrics-wrapper">En brain’u L board’uh aachu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En brain’u L board’uh aachu da"/>
</div>
<div class="lyrico-lyrics-wrapper">En heart’u rombe pain aachu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En heart’u rombe pain aachu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appeetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appeetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gap-pu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gap-pu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey repeat’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey repeat’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appeetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appeetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gap-pu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gap-pu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey repeat’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey repeat’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appeetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appeetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gap-pu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gap-pu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey repeat’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey repeat’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appeetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appeetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gap-pu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gap-pu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey repeat’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey repeat’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vehnam valikidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vehnam valikidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum vitturu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum vitturu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuku mehla naan azhudhuruven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuku mehla naan azhudhuruven"/>
</div>
<div class="lyrico-lyrics-wrapper">Fighting father’u loving daughter’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fighting father’u loving daughter’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Naduvula en love’u in danger’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naduvula en love’u in danger’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cute’uh en heartulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cute’uh en heartulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru fruit shop-uh vachi pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru fruit shop-uh vachi pona"/>
</div>
<div class="lyrico-lyrics-wrapper">Water falls ah vandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Water falls ah vandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenja theme park’uh maathi pohna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenja theme park’uh maathi pohna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ava daddy’ku un mehla angry moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava daddy’ku un mehla angry moodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada enna seiven en lovely bird’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada enna seiven en lovely bird’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey brandiyil ice cube podu podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey brandiyil ice cube podu podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan azhanum I am very sad’u sad’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan azhanum I am very sad’u sad’u"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendu side’la sharp knife’la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rendu side’la sharp knife’la"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiye vittiten maapu maapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiye vittiten maapu maapu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appeetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appeetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gap-pu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gap-pu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey repeat’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey repeat’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appeetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appeetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gap-pu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gap-pu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey repeat’u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey repeat’u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aapu aapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapu aapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appeetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appeetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gap-pu gap-pu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gap-pu gap-pu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aapu aapu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapu aapu"/>
</div>
<div class="lyrico-lyrics-wrapper">Appeetu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appeetu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gap-pu gap-pu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gap-pu gap-pu"/>
</div>
</pre>
