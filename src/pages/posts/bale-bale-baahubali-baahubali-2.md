---
title: "bale bale baahubali song lyrics"
album: "Baahubali 2"
artist: "M.M. Keeravani"
lyricist: "Madhan Karky"
director: "S.S. Rajamouli"
path: "/albums/baahubali-2-song-lyrics"
song: "Bale Bale Baahubali"
image: ../../images/albumart/baahubali-2.jpg
date: 2017-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/hDyoDTuf0Pc"
type: "Mass Entry"
singers:
  - Daler Mehndi
  - M.M. Keeravani
  - Mounima
---

<pre class="lyrics-native">
  </pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">bale bale bahubali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bale bale bahubali"/>
</div>
<div class="lyrico-lyrics-wrapper">bayam indri payum puli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayam indri payum puli"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bale bale bahubali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bale bale bahubali"/>
</div>
<div class="lyrico-lyrics-wrapper">bayam indri paayum puli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayam indri paayum puli"/>
</div>
<div class="lyrico-lyrics-wrapper">adi idi vedi melath kizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi idi vedi melath kizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">koottaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koottaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thuyaram ellaam uthiratumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyaram ellaam uthiratumey"/>
</div>
<div class="lyrico-lyrics-wrapper">dhisai ettum athirattumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhisai ettum athirattumey"/>
</div>
<div class="lyrico-lyrics-wrapper">seesaa neppikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seesaa neppikko"/>
</div>
<div class="lyrico-lyrics-wrapper">bothaya manasula appiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothaya manasula appiko"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasaa alliko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasaa alliko"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanatha udachida thullikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanatha udachida thullikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thoosaa thattiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoosaa thattiko"/>
</div>
<div class="lyrico-lyrics-wrapper">megatha thalapa kattikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megatha thalapa kattikko"/>
</div>
<div class="lyrico-lyrics-wrapper">raasaa vandhachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raasaa vandhachey"/>
</div>
<div class="lyrico-lyrics-wrapper">avaraium aada sethuko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avaraium aada sethuko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">veroruthi vazhiye vandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veroruthi vazhiye vandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">en uyir nee aanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en uyir nee aanai"/>
</div>
<div class="lyrico-lyrics-wrapper">maarbu naan kondathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarbu naan kondathe"/>
</div>
<div class="lyrico-lyrics-wrapper">undu nee thuyilavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undu nee thuyilavo"/>
</div>
<div class="lyrico-lyrics-wrapper">kandu naan magizhavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandu naan magizhavey"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhigalo...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigalo..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mala ena thottu paarthor solvaarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mala ena thottu paarthor solvaarey"/>
</div>
<div class="lyrico-lyrics-wrapper">puyal ena kando rthigaipare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyal ena kando rthigaipare"/>
</div>
<div class="lyrico-lyrics-wrapper">idi ena kaathil kettor solvaarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idi ena kaathil kettor solvaarey"/>
</div>
<div class="lyrico-lyrics-wrapper">arinthavar baahubali enbaarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arinthavar baahubali enbaarey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">porkalathil theeyaavann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porkalathil theeyaavann"/>
</div>
<div class="lyrico-lyrics-wrapper">thaai madiyil poovaavaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaai madiyil poovaavaan"/>
</div>
<div class="lyrico-lyrics-wrapper">aandavane aanaiyittum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aandavane aanaiyittum"/>
</div>
<div class="lyrico-lyrics-wrapper">thaai itta kottai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaai itta kottai"/>
</div>
<div class="lyrico-lyrics-wrapper">thaandida maattan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaandida maattan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seesaa neppiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seesaa neppiko"/>
</div>
<div class="lyrico-lyrics-wrapper">bothaya manasula appiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothaya manasula appiko"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasaa alliko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasaa alliko"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanatha udachida thullikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanatha udachida thullikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seesaa neppiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seesaa neppiko"/>
</div>
<div class="lyrico-lyrics-wrapper">nerupunnu nee pathikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerupunnu nee pathikko"/>
</div>
<div class="lyrico-lyrics-wrapper">bodhaya manasula appikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bodhaya manasula appikko"/>
</div>
<div class="lyrico-lyrics-wrapper">muzhu nelave ne ethikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muzhu nelave ne ethikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seesaa neppikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seesaa neppikko"/>
</div>
<div class="lyrico-lyrics-wrapper">bothaya manasula appiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothaya manasula appiko"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasaa alliko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasaa alliko"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanatha udachida thullikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanatha udachida thullikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thoosaa thattiko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoosaa thattiko"/>
</div>
<div class="lyrico-lyrics-wrapper">megatha thalapa kattikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="megatha thalapa kattikko"/>
</div>
<div class="lyrico-lyrics-wrapper">raasaa vandhachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="raasaa vandhachey"/>
</div>
<div class="lyrico-lyrics-wrapper">avaraium aada sethukko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="avaraium aada sethukko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seesaa neppikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seesaa neppikko"/>
</div>
<div class="lyrico-lyrics-wrapper">bothaya manasula appikko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bothaya manasula appikko"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bale bale bahubali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bale bale bahubali"/>
</div>
<div class="lyrico-lyrics-wrapper">bayam indri paayum puli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayam indri paayum puli"/>
</div>
<div class="lyrico-lyrics-wrapper">adi idi vedi melath kizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi idi vedi melath kizhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bale bale bahubali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bale bale bahubali"/>
</div>
<div class="lyrico-lyrics-wrapper">bayam indri paayum puli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bayam indri paayum puli"/>
</div>
<div class="lyrico-lyrics-wrapper">adi idi vedi melath kizhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi idi vedi melath kizhi"/>
</div>
<div class="lyrico-lyrics-wrapper">koottaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koottaali"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thuyaram ellaam uthirattumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuyaram ellaam uthirattumey"/>
</div>
<div class="lyrico-lyrics-wrapper">dhisai ettum athirattumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhisai ettum athirattumey"/>
</div>
</pre>
