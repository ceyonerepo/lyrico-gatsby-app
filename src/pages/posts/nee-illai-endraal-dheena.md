---
title: "nee illai endraal song lyrics"
album: "Dheena"
artist: "Yuvan Shankar Raja"
lyricist: "Vaali"
director: "A.R. Murugadoss"
path: "/albums/dheena-lyrics"
song: "Nee Illai Endraal"
image: ../../images/albumart/dheena.jpg
date: 2001-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pb5WHAWLLGA"
type: "love"
singers:
  - Bhavatharini
  - Mahalakshmi Iyer
  - Murugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee Illai Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illai Endraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyil Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyil Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanaville
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaville"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mugam Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugam Paarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooriyan Sirithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyan Sirithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhundhadhinge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhadhinge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kaadhal Endraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kaadhal Endraalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avvaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avvaarthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avvaarthai Pol Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avvaarthai Pol Ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koor Vaalum Kollaadhu Oho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koor Vaalum Kollaadhu Oho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Illai Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illai Endraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyil Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyil Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanaville
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaville"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mugam Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugam Paarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
 <div class="lyrico-lyrics-wrapper">Sooriyan Sirithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyan Sirithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhundhadhinge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhadhinge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kaadhal Endraalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kaadhal Endraalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avvaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avvaarthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avvaarthai Pol Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avvaarthai Pol Ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koor Vaalum Kollaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koor Vaalum Kollaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Illai Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illai Endraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyil Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyil Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanaville Ae Ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaville Ae Ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Anbe Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Anbe Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadai Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadai Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Yetral Enna Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yetral Enna Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idaiyil Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiyil Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Illai Endraal Vaazhkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illai Endraal Vaazhkaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illai Vaanavill
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Vaanavill"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadu Rathiriyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Rathiriyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Pooththiriyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Pooththiriyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oli Nadanamaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli Nadanamaadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pozhuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pozhuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru Yedum Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Yedum Illaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhuthum Illaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthum Illaamal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadal Nooru Ezhudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadal Nooru Ezhudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Mounam Adhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mounam Adhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollum Sollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollum Sollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Ullam Adhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ullam Adhai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mellum Mellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mellum Mellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadu Saamam Adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu Saamam Adhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sellum Sellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sellum Sellum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malar Vaanam Nammai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar Vaanam Nammai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kollum Kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollum Kollum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Aaval Pollaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aaval Pollaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Ammaadi Endrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Ammaadi Endrum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhu Kaaval Kollaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Kaaval Kollaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Illai Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illai Endraal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyil Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyil Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanaville
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanaville"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mugam Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugam Paarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooriyan Sirithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyan Sirithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhundhadhinge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhadhinge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Anbe Un Aadai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Anbe Un Aadai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Yetral Enna Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yetral Enna Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idaiyil Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiyil Indru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Oor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Oor"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mayangum Pala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangum Pala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oviyathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kaigal Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kaigal Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Kaadhalane Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Kaadhalane Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sithirathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sithirathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Kangal Kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kangal Kondu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varainthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varainthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Pole Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Pole Oru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oviyathai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oviyathai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hussein Kooda Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hussein Kooda Ingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaindhadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaindhadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnai Paarthaal Avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarthaal Avan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochu Muttum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu Muttum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Pole Udal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Pole Udal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Verthu Kotum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verthu Kotum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Kaadhal Vandhaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Kaadhal Vandhaale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha Harichandran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha Harichandran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kooda Pala Poigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooda Pala Poigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Solvaane Ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solvaane Ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endraal Vaazhkaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endraal Vaazhkaiyil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illai Vaanaville
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illai Vaanaville"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Mugam Paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Mugam Paarthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooriyan Sirithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooriyan Sirithu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhudhadhinge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhudhadhinge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Endraalum Avvaarthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endraalum Avvaarthai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pollaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pollaadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avvaarthai Pol Ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avvaarthai Pol Ennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koor Vaalum Kolladhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koor Vaalum Kolladhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbe Anbe Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbe Anbe Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadai Endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadai Endru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennai Yetral Enna Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Yetral Enna Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idaiyil Indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idaiyil Indru"/>
</div>
</pre>
