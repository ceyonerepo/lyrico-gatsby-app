---
title: "kaalai adhikaalai song lyrics"
album: "Naduvan"
artist: "Dharan Kumar"
lyricist: "Madhan Karky"
director: "Sharran Kumar"
path: "/albums/naduvan-lyrics"
song: "Kaalai Adhikaalai"
image: ../../images/albumart/naduvan.jpg
date: 2021-09-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/KGExsGVXvuI"
type: "happy"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haaa aaa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaa aaa aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha aaa aaa aaa haa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha aaa aaa aaa haa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai adhikaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai adhikaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaalagal tharai melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaalagal tharai melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelugindra saalaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelugindra saalaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhugindra naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhugindra naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai adhikaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai adhikaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Men pookal vizhi melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Men pookal vizhi melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellaiyaagum boomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaiyaagum boomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaiyaaga naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaiyaaga naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Moochu kaatru thooimaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu kaatru thooimaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Maeni engum neer pooka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maeni engum neer pooka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodai ondrai polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodai ondrai polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Odugindra naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odugindra naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhikaalaiyae adhikaalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaalaiyae adhikaalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal kaadhal nee thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal kaadhal nee thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaalaiyae adhikaalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaalaiyae adhikaalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam dhinam unai mananthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam dhinam unai mananthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhena piranthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhena piranthida"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Undhan perumaigalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan perumaigalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu yaarum ariyavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu yaarum ariyavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhi moodi kondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi moodi kondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha oorum kedakirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha oorum kedakirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neenda iravugalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenda iravugalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan paarvai karaithidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan paarvai karaithidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakendru nee dhinam thondra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakendru nee dhinam thondra"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ullam thudikirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ullam thudikirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaira vaigarai enakkena thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaira vaigarai enakkena thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha thaamarai malar thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha thaamarai malar thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulirthida dhinam dhinam enakkena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulirthida dhinam dhinam enakkena"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalai adhikaalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalai adhikaalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Men pookal vizhi melae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Men pookal vizhi melae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellaiyaagum boomiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaiyaagum boomiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillaiyaaga naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillaiyaaga naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjam konjam vegamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam konjam vegamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratham motham suthamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratham motham suthamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru endhan vaanilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru endhan vaanilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnugindra naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnugindra naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhikaalaiyae adhikaalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaalaiyae adhikaalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudhal kaadhal nee thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudhal kaadhal nee thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhikaalaiyae adhikaalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhikaalaiyae adhikaalaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam dhinam unai mananthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam dhinam unai mananthida"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhidhena piranthida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhidhena piranthida"/>
</div>
</pre>
