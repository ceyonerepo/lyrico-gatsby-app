---
title: "pondattee song lyrics"
album: "Goli Soda 2"
artist: "Achu Rajamani"
lyricist: "Mani Amuthavan"
director: "Vijay Milton"
path: "/albums/goli-soda-2-lyrics"
song: "Pondattee"
image: ../../images/albumart/goli-soda-2.jpg
date: 2018-06-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Z2OYPV2vnaQ"
type: "love"
singers:
  - Achu Rajamani
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adiyae adiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae adiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmmmmm mmmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmmmmm mmmmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathora perazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathora perazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga nee vanthazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga nee vanthazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna paakuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna paakuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla unaruren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla unaruren"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kaadhala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kaadhala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aathadi aatu kutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathadi aatu kutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan podum soapu katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan podum soapu katti"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola manakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pola manakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai ezhukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai ezhukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pogaiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohhh arumbatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohhh arumbatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesaya neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesaya neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Murukiyae thiriya vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Murukiyae thiriya vecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Velangadha yedho onnathan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velangadha yedho onnathan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee velanga vecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee velanga vecha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada ondikatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada ondikatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondikatta naanthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondikatta naanthan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaya katta pola vandhu neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaya katta pola vandhu neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasil un manasa ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasil un manasa ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodi sekiriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodi sekiriyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pondati nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pondati nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondati nee thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondati nee thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellakutti chellakutti thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellakutti chellakutti thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasil un manasa ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasil un manasa ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodi sekiriyae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodi sekiriyae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thom thom thaaananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thom thaaananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thom thaaananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thom thaaananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanae thanaa naa naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanae thanaa naa naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathi en manjanathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathi en manjanathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevvaana sengalathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevvaana sengalathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kootula oru kuruvithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kootula oru kuruvithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Edam theduthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edam theduthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sinnundu kannurutti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinnundu kannurutti"/>
</div>
<div class="lyrico-lyrics-wrapper">Sendhora kai pidichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sendhora kai pidichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa pesalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa pesalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaathotama yedhayavadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathotama yedhayavadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oh kanakka kann adithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oh kanakka kann adithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna yendi paathu pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna yendi paathu pora"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootti konjum kalichu paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootti konjum kalichu paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Michama nee varaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Michama nee varaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pondati nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pondati nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondati nee thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondati nee thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellakutti chellakutti thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellakutti chellakutti thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasil un manasa ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasil un manasa ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodi sekiriyae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodi sekiriyae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pondati nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pondati nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondati nee thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondati nee thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellakutti chellakutti thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellakutti chellakutti thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasil un manasa ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasil un manasa ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodi sekiriyae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodi sekiriyae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyae en azhagiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae en azhagiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen pulla ennai vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen pulla ennai vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga pora nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga pora nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkatha vittu vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vekkatha vittu vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Veeta vittu veliya vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veeta vittu veliya vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un azhagana mogatha naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un azhagana mogatha naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo paakanumnu thudikiranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo paakanumnu thudikiranae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pondati nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pondati nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondati nee thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondati nee thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellakutti chellakutti thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellakutti chellakutti thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasil un manasa ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasil un manasa ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodi sekiriyae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodi sekiriyae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pondati nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pondati nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondati nee thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondati nee thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Chellakutti chellakutti thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chellakutti chellakutti thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">En manasil un manasa ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En manasil un manasa ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodi sekiriyae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodi sekiriyae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thom thom thaaananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thom thaaananaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thom thom thaaananaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thom thom thaaananaa"/>
</div>
</pre>
