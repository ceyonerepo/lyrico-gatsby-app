---
title: 'pandarathi puranam song lyrics'
album: 'Karnan'
artist: 'Santhosh Narayanan'
lyricist: 'Yugabharathi'
director: 'Mari Selvaraj'
path: '/albums/karnan-song-lyrics'
song: 'Pandarathi Puranam'
image: ../../images/albumart/karnan.jpg
date: 2021-04-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-AC263k_7a0"
type: 'celebration'
singers: 
- Deva
- Reetha
- Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

<div class="lyrico-lyrics-wrapper">Yen aalu pandaarathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen aalu pandaarathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduppaana sembaruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduppaana sembaruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaala enna kothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala enna kothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangadicha sakkalathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangadicha sakkalathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen aalu pandaarathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen aalu pandaarathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduppaana sembaruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduppaana sembaruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaala enna kothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala enna kothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangadicha sakkalathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangadicha sakkalathi"/>
</div>
<div class="lyrico-lyrics-wrapper">En kakkathula, en kakkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kakkathula, en kakkathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vecha thunda tholu mela pottuvutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vecha thunda tholu mela pottuvutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoranaiya naanum nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranaiya naanum nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaakibatha ethivutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaakibatha ethivutta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En kakkathula, en kakkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kakkathula, en kakkathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Vecha thunda tholu mela pottuvutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vecha thunda tholu mela pottuvutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoranaiya naanum nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoranaiya naanum nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaakibatha ethivutta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaakibatha ethivutta"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha sittajhagi, antha sittajhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha sittajhagi, antha sittajhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha sittajhagi sottajhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha sittajhagi sottajhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaiya kelu ravuttu vandukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaiya kelu ravuttu vandukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen aalu pandaarathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen aalu pandaarathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Eduppaana sembaruthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eduppaana sembaruthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaala enna kothi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaala enna kothi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalangadicha sakkalathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangadicha sakkalathi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen aalu pandaarathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen aalu pandaarathi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vallanattu malaiyoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallanattu malaiyoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaram oru tharam pathom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaram oru tharam pathom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mullukkaattu moottoram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mullukkaattu moottoram"/>
</div>
<div class="lyrico-lyrics-wrapper">Moochu mutta theneduthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moochu mutta theneduthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kakkulathu pakkathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kakkulathu pakkathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalasamy koiyilila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalasamy koiyilila"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathiyadhaan palikoduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathiyadhaan palikoduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhanam kunkumam poosikittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhanam kunkumam poosikittom"/>
</div>
<div class="lyrico-lyrics-wrapper">En pandaarathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pandaarathi"/>
</div>
<div class="lyrico-lyrics-wrapper">En pandaarathi yemanoda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pandaarathi yemanoda"/>
</div>
<div class="lyrico-lyrics-wrapper">Veettu velakka ethunaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettu velakka ethunaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Erumaiyaattam thirinja payala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erumaiyaattam thirinja payala"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaana mela yethuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaana mela yethuna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Antha mottazhagi, antha mottazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha mottazhagi, antha mottazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha mottazhagi pottazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha mottazhagi pottazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaiya kelu ravuttu vandukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaiya kelu ravuttu vandukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ravuttu vandukku, ravuttu vandukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravuttu vandukku, ravuttu vandukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeii yei podu podu riththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeii yei podu podu riththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei podu podu riththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei podu podu riththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei pod, yei pod
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei pod, yei pod"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei pod, yei pod
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei pod, yei pod"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei riththa riththa pod
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei riththa riththa pod"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei riththa riththa pod
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei riththa riththa pod"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei ritha yei ritha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei ritha yei ritha"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei ritha yei ritha yei pod
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei ritha yei ritha yei pod"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeo podu podu podu podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeo podu podu podu podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Podu podu podu podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podu podu podu podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Riththa ye riththa ye riththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Riththa ye riththa ye riththaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Karuppandhorai manneduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuppandhorai manneduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaara veedu kattikkittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaara veedu kattikkittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Naayikutti naaleduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayikutti naaleduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kozhandhayaakki konjikkittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhandhayaakki konjikkittom"/>
</div>
<div class="lyrico-lyrics-wrapper">Paadhavathi saathisanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhavathi saathisanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Veleduthu vaarumunnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veleduthu vaarumunnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaleduthu sandayida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaleduthu sandayida"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasalila kaathirundhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasalila kaathirundhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En pandaarathi, en pandarathi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pandaarathi, en pandarathi"/>
</div>
<div class="lyrico-lyrics-wrapper">En pandaarathi odamukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En pandaarathi odamukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna ezhavu poondhucho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna ezhavu poondhucho"/>
</div>
<div class="lyrico-lyrics-wrapper">Cholerannu vandha noyi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cholerannu vandha noyi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeman kannedhire thinniduche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeman kannedhire thinniduche"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En mottazhagi pottazhagi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En mottazhagi pottazhagi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhaiya keli ravuttu vandukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhaiya keli ravuttu vandukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ravut.. ravu..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ravut.. ravu.."/>
</div>
</pre>