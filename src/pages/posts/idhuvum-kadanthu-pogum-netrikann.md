---
title: "idhuvum kadanthu pogum song lyrics"
album: "Netrikann"
artist: "Girishh G"
lyricist: "Karthik Netha"
director: "Milind Rau"
path: "/albums/netrikann-song-lyrics"
song: "Idhuvum Kadanthu Pogum"
image: ../../images/albumart/netrikann.jpg
date: 2021-08-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XvvZGkFnJys"
type: "motivational"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhuvum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Kadanthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Kadanthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudari Irulil Yengathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudari Irulil Yengathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Velidhan Kadhavai Mudathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velidhan Kadhavai Mudathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Aru Kalangalum Mari Mari Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Aru Kalangalum Mari Mari Varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iyarkayin Vidhi Idhuve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iyarkayin Vidhi Idhuve"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhiyadha Kayangali Atru Mayangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhiyadha Kayangali Atru Mayangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Anubhavam Koduthidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anubhavam Koduthidume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Katrodu Pogum, Varau Ponal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Katrodu Pogum, Varau Ponal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Edho Or Poovin Thunai Anal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Edho Or Poovin Thunai Anal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudari Sudari Udaindhu Pogadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudari Sudari Udaindhu Pogadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Udane Valigal Maraindhu Pogadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udane Valigal Maraindhu Pogadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila Nal Varaikum Adhai Seendathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Nal Varaikum Adhai Seendathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvai Marakkum Pinn Thondradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvai Marakkum Pinn Thondradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Kadanthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Kadanthu Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Kadanthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Kadanthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Kadanthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum Kadanthu Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhuve Padaikum Adhuve Udaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuve Padaikum Adhuve Udaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manamdhan Oru Kuzhandhaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manamdhan Oru Kuzhandhaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvai Malaum Adhuvai Udhirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvai Malaum Adhuvai Udhirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhupol Indha Kavalaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhupol Indha Kavalaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nalodhorum Edho Marudhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalodhorum Edho Marudhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanum Mannum Vazhum Arudhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanum Mannum Vazhum Arudhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesamal Vazhvai Vazhndhiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesamal Vazhvai Vazhndhiruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Katrodu Pogum Varau Ponal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Katrodu Pogum Varau Ponal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Edho Or Poovin Thunai Anal Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Edho Or Poovin Thunai Anal Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudari Sudari Udaindhu Pogadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudari Sudari Udaindhu Pogadhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Udane Valigal Maraindhu Pogadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udane Valigal Maraindhu Pogadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sila Nal Varaikum Adhai Seendathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sila Nal Varaikum Adhai Seendathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhuvai Marakkum Pinn Thondradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvai Marakkum Pinn Thondradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Kadanthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Kadanthu Pogum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adhuvai Vizhundhe Adhuvai Ezhundhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhuvai Vizhundhe Adhuvai Ezhundhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhandhai Nadai Pazhagudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhandhai Nadai Pazhagudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhal Unarndhe Udale Virindhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhal Unarndhe Udale Virindhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravai Dhisai Amaikudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravai Dhisai Amaikudhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vasanmthan Poovin Parvaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasanmthan Poovin Parvaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Katril Eri Kanum Katchigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katril Eri Kanum Katchigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanamal Veliyaga Parthidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanamal Veliyaga Parthidume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Ootraga Nesam Engo Uruvagume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Ootraga Nesam Engo Uruvagume"/>
</div>
<div class="lyrico-lyrics-wrapper">Perum Katraga Marichendru Uravadume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perum Katraga Marichendru Uravadume"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudari Sudari Velicham Theeradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudari Sudari Velicham Theeradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha Nee Unarndhal Payanam Theeradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha Nee Unarndhal Payanam Theeradhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhage Sudari Ada Yengathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhage Sudari Ada Yengathe"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarin Ninaivil Manam Vadathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarin Ninaivil Manam Vadathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuvum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Kadanthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Kadanthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Kadanthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhuvum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuvum Kadanthu Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadanthu Pogum Kadanthu Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadanthu Pogum Kadanthu Pogum"/>
</div>
</pre>
