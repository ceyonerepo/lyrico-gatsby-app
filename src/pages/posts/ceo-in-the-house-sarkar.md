---
title: "ceo in the house song lyrics"
album: "Sarkar"
artist: "AR Rahman"
lyricist: "Vivek"
director: "AR Murugadoss"
path: "/albums/sarkar-lyrics"
song: "CEO In The House"
image: ../../images/albumart/sarkar.jpg
date: 2018-11-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/u8BxVzRG9bE"
type: "happy"
singers:
  - Nakul Abhyankar
  - Blaaze
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh ho ho oh ho ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh ho ooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ho ho oh ho ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh ho ooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">CEO in the house
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="CEO in the house"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ho ho oh ho ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh ho ooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ho ho oh ho ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh ho ooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">CEO in the house
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="CEO in the house"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ho ho oh ho ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh ho ooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ho ho oh ho ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh ho ooo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I wanna rock like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wanna rock like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Star like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Star like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Fire like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fire like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Gimme some love like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gimme some love like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">High like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="High like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Money like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Money like ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I wanna rock like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wanna rock like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Star like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Star like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Fire like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fire like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Gimme some love like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gimme some love like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">High like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="High like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Money like ah eh eh eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Money like ah eh eh eh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Play ada enai enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Play ada enai enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumaa thunai thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumaa thunai thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan oru yevuganai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru yevuganai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaamae vinai vinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaamae vinai vinai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Play ada enai enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Play ada enai enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumaa thunai thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumaa thunai thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan oru yevuganai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru yevuganai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaamae vinai vinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaamae vinai vinai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ra ra ra ratchasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ra ra ra ratchasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugal ondru ezhunthu nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugal ondru ezhunthu nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Olikkum per ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olikkum per ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">Arangamae adhira vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arangamae adhira vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadukkum kaalam thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadukkum kaalam thaandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu paravi nirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu paravi nirukkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Play ada enai enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Play ada enai enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumaa thunai thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumaa thunai thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan oru yevuganai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru yevuganai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaamae vinai vinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaamae vinai vinai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I wanna rock like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wanna rock like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Star like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Star like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Fire like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fire like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Gimme some love like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gimme some love like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">High like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="High like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Money like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Money like ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ho ho oh ho ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh ho ooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ho ho oh ho ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh ho ooo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottutaa vettri maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottutaa vettri maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma freak out maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma freak out maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga knockout-u maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga knockout-u maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Parthukomaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parthukomaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavil enakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavil enakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru share irukkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru share irukkumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Little ooru irukkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little ooru irukkumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogalaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogalaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh coola oh dealaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh coola oh dealaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh dhoolaOr dollaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh dhoolaOr dollaru "/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai collar varai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai collar varai"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam dhinam kottum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam dhinam kottum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh vaanum oh mannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh vaanum oh mannum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru yellow oh sunnoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru yellow oh sunnoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kedkkum color change aagidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kedkkum color change aagidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I wanna rock like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I wanna rock like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Star like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Star like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Fire like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fire like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Gimme some love like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gimme some love like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">High like ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="High like ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Money like ah eh eh eh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Money like ah eh eh eh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Play ada enai enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Play ada enai enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumaa thunai thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumaa thunai thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan oru yevuganai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru yevuganai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaamae vinai vinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaamae vinai vinai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Play ada enai enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Play ada enai enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumaa thunai thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumaa thunai thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan oru yevuganai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru yevuganai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaamae vinai vinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaamae vinai vinai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ho ho oh ho ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh ho ooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ho ho oh ho ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh ho ooo oo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oxygen pola un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oxygen pola un"/>
</div>
<div class="lyrico-lyrics-wrapper">Pera ellaam illukkanum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pera ellaam illukkanum da"/>
</div>
<div class="lyrico-lyrics-wrapper">Googlae naalikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Googlae naalikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai thedi malaikanum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai thedi malaikanum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Prechanai ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prechanai ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma sumappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma sumappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Success ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Success ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Share pannipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Share pannipom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bhayam vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhayam vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Alara vaipom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alara vaipom"/>
</div>
<div class="lyrico-lyrics-wrapper">Bulls eye targetaai adippom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bulls eye targetaai adippom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">CEO in the House 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="CEO in the House "/>
</div>
<div class="lyrico-lyrics-wrapper">Money ma money maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Money ma money maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Money maa money maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Money maa money maa"/>
</div>
<div class="lyrico-lyrics-wrapper">CEO in the House 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="CEO in the House "/>
</div>
<div class="lyrico-lyrics-wrapper">Money ma money maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Money ma money maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Money maa money maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Money maa money maa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Play ada enai enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Play ada enai enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumaa thunai thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumaa thunai thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan oru yevuganai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru yevuganai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaamae vinai vinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaamae vinai vinai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Play ada enai enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Play ada enai enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumaa thunai thunai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumaa thunai thunai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan oru yevuganai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan oru yevuganai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendaamae vinai vinai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaamae vinai vinai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh ho ho oh ho ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh ho ooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">CEO in the house
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="CEO in the house"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh ho ho oh ho ooo oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh ho ho oh ho ooo oo"/>
</div>
<div class="lyrico-lyrics-wrapper">CEO in the house
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="CEO in the house"/>
</div>
</pre>
