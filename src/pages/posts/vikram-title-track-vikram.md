---
title: "vikram title track song lyrics"
album: "Vikram"
artist: "Anirudh Ravichander"
lyricist: "Vishnu Edavan"
director: "Lokesh Kanagaraj"
path: "/albums/vikram-lyrics"
song: "Vikram"
image: ../../images/albumart/vikram.jpg
date: 2022-06-03
lang: tamil
youtubeLink: "https://www.youtube.com/embed/OsLCY3vz3t8"
type: "title track"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naayagan Meendum Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayagan Meendum Vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettuthikkum Bayam Dhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettuthikkum Bayam Dhane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratchasan Raman Ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratchasan Raman Ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavai Kondavan Ivan Dhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavai Kondavan Ivan Dhane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivan Varalarum Oru Yugam Padum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Varalarum Oru Yugam Padum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Kalai Dhagam Por Vilaiyagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Kalai Dhagam Por Vilaiyagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Adaiyalam Adhu Kadalagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Adaiyalam Adhu Kadalagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pala Por Vendra Oru Peyar Aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Por Vendra Oru Peyar Aagum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarambikalangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarambikalangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakida Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida Thak Dheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thak Dheem Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thak Dheem Thak Dheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakida Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida Thak Dheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thak Dheem Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thak Dheem Thak Dheem"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakida Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida Thak Dheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thak Dheem Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thak Dheem Thak Dheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakida Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida Thak Dheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thak Dheem Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thak Dheem Thak Dheem"/>
</div>
<div class="lyrico-lyrics-wrapper">HaaHaaHaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="HaaHaaHaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vikram Ho Vikram Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikram Ho Vikram Ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah Vikram Ho Vikram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Vikram Ho Vikram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikram Ho Vikram Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikram Ho Vikram Ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah Vikram Ho Vikram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Vikram Ho Vikram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naayagan Meendum Vara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naayagan Meendum Vara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettuthikkum Bayam Dhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettuthikkum Bayam Dhane"/>
</div>
<div class="lyrico-lyrics-wrapper">Ratchasan Raman Ena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratchasan Raman Ena"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavai Kondavan Ivan Dhane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavai Kondavan Ivan Dhane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhundhal Idiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhundhal Idiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhundhal Malayan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhundhal Malayan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivan Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivan Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Por Kalaingan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Por Kalaingan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pala Vaal Thuzhaithum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pala Vaal Thuzhaithum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Vizhundhadhilay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Vizhundhadhilay"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulage Edhirthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulage Edhirthum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Adangavilayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Adangavilayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhu Pol Oruvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu Pol Oruvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Vazhundhadilayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Vazhundhadilayi"/>
</div>
<div class="lyrico-lyrics-wrapper">Avane Muyandrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avane Muyandrum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Maindhadhilayi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Maindhadhilayi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aarambikalangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarambikalangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakida Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida Thak Dheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thak Dheem Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thak Dheem Thak Dheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakida Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida Thak Dheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thak Dheem Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thak Dheem Thak Dheem"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thakida Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida Thak Dheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thak Dheem Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thak Dheem Thak Dheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakida Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakida Thak Dheem"/>
</div>
<div class="lyrico-lyrics-wrapper">Thak Dheem Thak Dheem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thak Dheem Thak Dheem"/>
</div>
<div class="lyrico-lyrics-wrapper">HaaHaaHaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="HaaHaaHaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vikram Ho Vikram Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikram Ho Vikram Ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah Vikram Ho Vikram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Vikram Ho Vikram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vikram Ho Vikram Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vikram Ho Vikram Ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeah Vikram Ho Vikram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah Vikram Ho Vikram"/>
</div>
</pre>
