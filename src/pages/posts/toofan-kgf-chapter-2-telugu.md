---
title: "toofan song lyrics"
album: "KGF Chapter 2"
artist: "Ravi Basrur"
lyricist: "Ramajogayya Sastry"
director: "Prashanth Neel"
path: "/albums/kgf-chapter-2-lyrics"
song: "Toofan"
image: ../../images/albumart/kgf-chapter-2.jpg
date: 2022-04-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/xUe3eR0n380"
type: "mass"
singers:
  - Sri Krishna
  - Prudhvi Chandra
  - Arun Kaundinya
  - Bhaskaruni Sai Charan
  - Santhosh Venky
  - Mohan Krishna
  - Sachin Basrur
  - Ravi Basrur
  - Puneeth Rudranag
  - Manish Dinakar
  - Harini Ivaturi
  - Giridhar Kamath
  - Raksha Kamath
  - Sinchana Kamath
  - Nishanth Kini
  - Bharath Bhat
  - Anagha Nayak
  - Avani Bhat
  - Swathi Kamath
  - Shivanand Nayak
  - Keerthana Basrur
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Samundar mein leher uthi hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samundar mein leher uthi hai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ziddi ziddi hai toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ziddi ziddi hai toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Chattanein bhi kaap rahi hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chattanein bhi kaap rahi hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Ziddi ziddi hai toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ziddi ziddi hai toofan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ziddi hai ziddi hai toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ziddi hai ziddi hai toofan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu kya main kya hat ja hat ja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu kya main kya hat ja hat ja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelugitthi yegasi thodagottinade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelugitthi yegasi thodagottinade"/>
</div>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivametthi alala padagetthinade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivametthi alala padagetthinade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelugitthi yegasi thodagottinade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelugitthi yegasi thodagottinade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivametthi alala padagetthinade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivametthi alala padagetthinade"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarrtrantu eedu adugesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarrtrantu eedu adugesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Udyamisthe aakaramaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udyamisthe aakaramaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Garrantu gadhimi garjisthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Garrantu gadhimi garjisthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaladarinchu bhoogamaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaladarinchu bhoogamaname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Rocky oh Rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Rocky oh Rocky"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Rocky Rocky Rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Rocky Rocky Rocky"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Rocky oh Rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Rocky oh Rocky"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Rocky Rocky Rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Rocky Rocky Rocky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey churrantu churuku muttinchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey churrantu churuku muttinchu"/>
</div>
<div class="lyrico-lyrics-wrapper">Arka thejamaagamaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arka thejamaagamaname"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yerranchu karaku khadgaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yerranchu karaku khadgaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaturu damanamaagamaname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaturu damanamaagamaname"/>
</div>
<div class="lyrico-lyrics-wrapper">Rock rock Rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rock rock Rocky"/>
</div>
<div class="lyrico-lyrics-wrapper">Rock rock Rocky Rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rock rock Rocky Rocky"/>
</div>
<div class="lyrico-lyrics-wrapper">Rock rock Rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rock rock Rocky"/>
</div>
<div class="lyrico-lyrics-wrapper">Rock rock Rocky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rock rock Rocky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee needalo marujanmagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee needalo marujanmagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhairyaaniki jananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhairyaaniki jananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bigithapina pidikilliku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bigithapina pidikilliku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerpinchara jagadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerpinchara jagadam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Swaranam malinam veede aa rendu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swaranam malinam veede aa rendu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yamudai priyathamudai chalagaade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yamudai priyathamudai chalagaade"/>
</div>
<div class="lyrico-lyrics-wrapper">Monagaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monagaadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vairijanula mucchamata munchuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vairijanula mucchamata munchuta"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedu neerchina modhati mucchata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedu neerchina modhati mucchata"/>
</div>
<div class="lyrico-lyrics-wrapper">Vijrumbinchu aa sathuva mundu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vijrumbinchu aa sathuva mundu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu kya main kya hat ja hat ja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu kya main kya hat ja hat ja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelugitthi yegasi thodagottinade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelugitthi yegasi thodagottinade"/>
</div>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivametthi alala padagetthinade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivametthi alala padagetthinade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelugitthi yegasi thodagottinade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelugitthi yegasi thodagottinade"/>
</div>
<div class="lyrico-lyrics-wrapper">Toofan toofan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Toofan toofan"/>
</div>
<div class="lyrico-lyrics-wrapper">Shivametthi alala padagetthinade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shivametthi alala padagetthinade"/>
</div>
</pre>
