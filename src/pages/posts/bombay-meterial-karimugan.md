---
title: "bombay meterial song lyrics"
album: "Karimugan"
artist: "Chella Thangaiah"
lyricist: "unknown"
director: "Chella Thangaiah"
path: "/albums/karimugan-song-lyrics"
song: "Bombay Meterial"
image: ../../images/albumart/karimugan.jpg
date: 2018-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6qQ3y0XOVUE"
type: "happy"
singers:
  - Senthil Ganesh
  - linsi pransis
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">naan bombeyile poranthavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan bombeyile poranthavada"/>
</div>
<div class="lyrico-lyrics-wrapper">pala thesangalai alanthavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pala thesangalai alanthavada"/>
</div>
<div class="lyrico-lyrics-wrapper">cheenaiyila valanthavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheenaiyila valanthavada"/>
</div>
<div class="lyrico-lyrics-wrapper">intha seemaike usanthavada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha seemaike usanthavada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">maana vetkam rosamellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maana vetkam rosamellam"/>
</div>
<div class="lyrico-lyrics-wrapper">manthaiyila parakkavittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manthaiyila parakkavittu"/>
</div>
<div class="lyrico-lyrics-wrapper">vantha thookam marakanunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vantha thookam marakanunu"/>
</div>
<div class="lyrico-lyrics-wrapper">adichchom oru kuvaaturu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adichchom oru kuvaaturu da"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi puduchom sema materu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi puduchom sema materu da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eh machi mater mater nu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eh machi mater mater nu"/>
</div>
<div class="lyrico-lyrics-wrapper">paaduriye enna matru?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paaduriye enna matru?"/>
</div>
<div class="lyrico-lyrics-wrapper">tho paaduren machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tho paaduren machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bombey material
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bombey material"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkavaana kalaru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkavaana kalaru da"/>
</div>
<div class="lyrico-lyrics-wrapper">paathale pothaiyerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathale pothaiyerum"/>
</div>
<div class="lyrico-lyrics-wrapper">sikkalaana figaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikkalaana figaruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bombey material
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bombey material"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkavaana kalaru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkavaana kalaru da"/>
</div>
<div class="lyrico-lyrics-wrapper">paathale pothaiyerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathale pothaiyerum"/>
</div>
<div class="lyrico-lyrics-wrapper">sikkalaana figaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikkalaana figaruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thennee thagathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thennee thagathile"/>
</div>
<div class="lyrico-lyrics-wrapper">moolgi ponom naangada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moolgi ponom naangada"/>
</div>
<div class="lyrico-lyrics-wrapper">kanni thaagam vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanni thaagam vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">kandukkama pongada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kandukkama pongada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuvatarukkule irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuvatarukkule irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">bekku bekku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bekku bekku than"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kooda naama aadunaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kooda naama aadunaale"/>
</div>
<div class="lyrico-lyrics-wrapper">kicku kicku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kicku kicku thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuvatarukkule irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuvatarukkule irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">bekku bekku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bekku bekku than"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kooda naama aadunaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kooda naama aadunaale"/>
</div>
<div class="lyrico-lyrics-wrapper">kicku kicku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kicku kicku thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bombey material
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bombey material"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkavaana kalaru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkavaana kalaru da"/>
</div>
<div class="lyrico-lyrics-wrapper">paathale pothaiyerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathale pothaiyerum"/>
</div>
<div class="lyrico-lyrics-wrapper">sikkalaana figaruda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sikkalaana figaruda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">udal maraiyaa sela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal maraiyaa sela"/>
</div>
<div class="lyrico-lyrics-wrapper">katti avuththu viduvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti avuththu viduvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kadal neraiya aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kadal neraiya aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaati kavuththu puduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaati kavuththu puduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">udal maraiyaa sela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal maraiyaa sela"/>
</div>
<div class="lyrico-lyrics-wrapper">katti avuththu viduvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti avuththu viduvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kadal neraiya aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kadal neraiya aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaati kavuththu puduva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaati kavuththu puduva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanaale meesakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanaale meesakala"/>
</div>
<div class="lyrico-lyrics-wrapper">valachukiruvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valachukiruvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kana kachithama aasakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kana kachithama aasakala"/>
</div>
<div class="lyrico-lyrics-wrapper">vethachu puduvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vethachu puduvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaasu iruntha thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu iruntha thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasu iruntha thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu iruntha thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">unna thediye varuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna thediye varuva"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasu illenaa ava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu illenaa ava"/>
</div>
<div class="lyrico-lyrics-wrapper">oodiye viduvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodiye viduvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuvatarukkule irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuvatarukkule irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">bekku bekku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bekku bekku than"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kooda naama aadunaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kooda naama aadunaale"/>
</div>
<div class="lyrico-lyrics-wrapper">kicku kicku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kicku kicku thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuvatarukkule irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuvatarukkule irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">bekku bekku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bekku bekku than"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kooda naama aadunaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kooda naama aadunaale"/>
</div>
<div class="lyrico-lyrics-wrapper">kicku kicku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kicku kicku thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manja nalla kilangarachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja nalla kilangarachu"/>
</div>
<div class="lyrico-lyrics-wrapper">manja kilangarachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja kilangarachu"/>
</div>
<div class="lyrico-lyrics-wrapper">mugaththa maathuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugaththa maathuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">iva konjam thoranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva konjam thoranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu kushi aethuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu kushi aethuvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manja kilangarachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja kilangarachu"/>
</div>
<div class="lyrico-lyrics-wrapper">mugaththa maathuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugaththa maathuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">iva konjam thoranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva konjam thoranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu kushi aethuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu kushi aethuvaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanja kalanthu vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanja kalanthu vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">kavarchi kaatuvaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavarchi kaatuvaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasu anju pathu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu anju pathu "/>
</div>
<div class="lyrico-lyrics-wrapper">koranjathunaa kambi neetuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koranjathunaa kambi neetuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaasu iruntha thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu iruntha thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">inge kaasu iruntha thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="inge kaasu iruntha thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">unna vesiyum mathipaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna vesiyum mathipaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasu illanaa onna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu illanaa onna"/>
</div>
<div class="lyrico-lyrics-wrapper">eesiyaa marappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eesiyaa marappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuvatarukkule irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuvatarukkule irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">bekku bekku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bekku bekku than"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kooda naama aadunaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kooda naama aadunaale"/>
</div>
<div class="lyrico-lyrics-wrapper">kicku kicku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kicku kicku thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuvatarukkule irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuvatarukkule irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">bekku bekku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bekku bekku than"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kooda naama aadunaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kooda naama aadunaale"/>
</div>
<div class="lyrico-lyrics-wrapper">kicku kicku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kicku kicku thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuvatarukkule irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuvatarukkule irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">bekku bekku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bekku bekku than"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kooda naama aadunaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kooda naama aadunaale"/>
</div>
<div class="lyrico-lyrics-wrapper">kicku kicku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kicku kicku thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kuvatarukkule irukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuvatarukkule irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">bekku bekku than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bekku bekku than"/>
</div>
<div class="lyrico-lyrics-wrapper">iva kooda naama aadunaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva kooda naama aadunaale"/>
</div>
<div class="lyrico-lyrics-wrapper">kicku kicku thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kicku kicku thaan"/>
</div>
</pre>