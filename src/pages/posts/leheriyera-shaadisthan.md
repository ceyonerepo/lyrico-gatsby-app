---
title: "leheriyera song lyrics"
album: "Shaadisthan"
artist: "Nakul Sharma - Sahil Bhatia"
lyricist: "Nakul Sharma, Sahil Bhatia, Ajay Jayanthi & Kunal Singh Chauhan"
director: "Raj Singh Chaudhary"
path: "/albums/shaadisthan-lyrics"
song: "Leheriyera"
image: ../../images/albumart/shaadisthan.jpg
date: 2021-06-11
lang: hindi
youtubeLink: "https://www.youtube.com/embed/HWeyzo4OeL4"
type: "happy"
singers:
  - Isheeta Chakrvarty
  - Swaroop Khan
  - Ajay Jayanthi
  - Apurv Dogra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Meree Soch Alag Par 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meree Soch Alag Par "/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhako pasand mai aayee gayee na 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhako pasand mai aayee gayee na "/>
</div>
<div class="lyrico-lyrics-wrapper">Kuchh aise phase ke is bhavar se bach paenge ham na 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuchh aise phase ke is bhavar se bach paenge ham na "/>
</div>
<div class="lyrico-lyrics-wrapper">Tujhase milu ya na milu ye socha tha kaee baar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujhase milu ya na milu ye socha tha kaee baar "/>
</div>
<div class="lyrico-lyrics-wrapper">Ab mil jo gae to chadhane laga hai laharon sa khumaar 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ab mil jo gae to chadhane laga hai laharon sa khumaar "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Leheriyara leheriyara leheriyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leheriyara leheriyara leheriyara"/>
</div>
<div class="lyrico-lyrics-wrapper">O leheriyara leheriyara leheriyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O leheriyara leheriyara leheriyara"/>
</div>
<div class="lyrico-lyrics-wrapper">Leheriyara leheriyara leheriyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leheriyara leheriyara leheriyara"/>
</div>
<div class="lyrico-lyrics-wrapper">O leheriyara leheriyara leheriyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O leheriyara leheriyara leheriyara"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O mahare leheriyara 900 roopaya rokada sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O mahare leheriyara 900 roopaya rokada sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Mahare leheriyara 900 roopaya rokada sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahare leheriyara 900 roopaya rokada sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ho manne laido re baadila mahara laheriyo sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho manne laido re baadila mahara laheriyo sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ho manne laido laido laido maaro laheriyo sa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho manne laido laido laido maaro laheriyo sa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bekhauph see kashtee hu main 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bekhauph see kashtee hu main "/>
</div>
<div class="lyrico-lyrics-wrapper">Takarau laharon se sada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takarau laharon se sada "/>
</div>
<div class="lyrico-lyrics-wrapper">Apanee zid main too doob mat 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apanee zid main too doob mat "/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dekh too mera jaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dekh too mera jaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bekhauph see kashtee hu main 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bekhauph see kashtee hu main "/>
</div>
<div class="lyrico-lyrics-wrapper">Takarau laharon se sada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takarau laharon se sada "/>
</div>
<div class="lyrico-lyrics-wrapper">Apanee zid main too dub mat 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apanee zid main too dub mat "/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dekh too mera jaha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dekh too mera jaha "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bekhauph see kashtee hu main 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bekhauph see kashtee hu main "/>
</div>
<div class="lyrico-lyrics-wrapper">Takarau laharon se sada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takarau laharon se sada "/>
</div>
<div class="lyrico-lyrics-wrapper">Apanee zid main too dub mat 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apanee zid main too dub mat "/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dekh too mera jaha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dekh too mera jaha "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bekhauph see kashtee hu main 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bekhauph see kashtee hu main "/>
</div>
<div class="lyrico-lyrics-wrapper">Takarau laharon se sada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Takarau laharon se sada "/>
</div>
<div class="lyrico-lyrics-wrapper">Apanee zid main too doob mat 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apanee zid main too doob mat "/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dekh too mera jaha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dekh too mera jaha "/>
</div>
<div class="lyrico-lyrics-wrapper">Od laheriyo mai baajara jaoo sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Od laheriyo mai baajara jaoo sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Od laheriyo mai baajara jaoo sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Od laheriyo mai baajara jaoo sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Chhotee nand re chundadee lau sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhotee nand re chundadee lau sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaaree nand re chundadee lau sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaaree nand re chundadee lau sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maharo laheriyo ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maharo laheriyo ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maharo laheriyo jayapuriye ree baajaar ro sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maharo laheriyo jayapuriye ree baajaar ro sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maharo laheriyo jaisode ree baajaar ro sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maharo laheriyo jaisode ree baajaar ro sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Maharo laheriyo jodhaan ree baajaar ro sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maharo laheriyo jodhaan ree baajaar ro sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Mahare lehariyera 900 roopaya rokada sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahare lehariyera 900 roopaya rokada sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Mahare lehariyera 900 roopaya rokada sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahare lehariyera 900 roopaya rokada sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Manne ledo re baadila maaro laheriyo sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manne ledo re baadila maaro laheriyo sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Manne laeedo laeedo laeedo ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manne laeedo laeedo laeedo ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mahare lehariyera 900 roopaya rokada sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahare lehariyera 900 roopaya rokada sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Mahare lehariyera 900 roopaya rokada sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mahare lehariyera 900 roopaya rokada sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Ho manne ledo re baadila maaro laheriyo sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho manne ledo re baadila maaro laheriyo sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Manne laeedo laeedo laeedo laheriyo sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manne laeedo laeedo laeedo laheriyo sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Manne laido laido laido 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manne laido laido laido "/>
</div>
<div class="lyrico-lyrics-wrapper">Manne laido laido laido
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manne laido laido laido"/>
</div>
<div class="lyrico-lyrics-wrapper">Laido re baadila maara laheriyo sa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laido re baadila maara laheriyo sa "/>
</div>
<div class="lyrico-lyrics-wrapper">Manne laido laido laido 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manne laido laido laido "/>
</div>
<div class="lyrico-lyrics-wrapper">Manne laido laido laido 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manne laido laido laido "/>
</div>
<div class="lyrico-lyrics-wrapper">Leheriyara leheriyara leheriyara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leheriyara leheriyara leheriyara"/>
</div>
</pre>
