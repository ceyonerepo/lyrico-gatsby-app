---
title: "wifeh lovvu song lyrics"
album: "Koditta Idangalai Nirappuga"
artist: "C Sathya"
lyricist: "R Parthiepan"
director: "Parthiban"
path: "/albums/koditta-idangalai-nirappuga-lyrics"
song: "Wifeh Lovvu"
image: ../../images/albumart/koditta-idangalai-nirappuga.jpg
date: 2017-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jpxjlLWNZlc"
type: "love"
singers:
  - Malaysia Ganesan
  - Bhagyaraj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Thaa thaa thath thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa thaa thath thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaa thaa thath thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa thaa thath thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaa thaa thath thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa thaa thath thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaath thaath thaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaath thaath thaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaa thaa thath thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa thaa thath thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaa thaa thath thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa thaa thath thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaa thaa thath thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaa thaa thath thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaath thaath thaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaath thaath thaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My wife is a nice girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My wife is a nice girl"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh your wife is a nice girl
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh your wife is a nice girl"/>
</div>
<div class="lyrico-lyrics-wrapper">My wife is my pondati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My wife is my pondati"/>
</div>
<div class="lyrico-lyrics-wrapper">My girlfriend
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My girlfriend"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wife-ah love-vu pannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wife-ah love-vu pannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh nallaarukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh nallaarukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Wife-ah kiss-u panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wife-ah kiss-u panna"/>
</div>
<div class="lyrico-lyrics-wrapper">God-u bless-u pannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="God-u bless-u pannum"/>
</div>
<div class="lyrico-lyrics-wrapper">Wife-a correct-u pannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wife-a correct-u pannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum merit-u yerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum merit-u yerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaa aanaa aanaa aanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaa aanaa aanaa aanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru wife-nu clear-ah sollaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru wife-nu clear-ah sollaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Clever-a uttu taandaa paavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Clever-a uttu taandaa paavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Illara vaazhkaikae kodaichalada saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illara vaazhkaikae kodaichalada saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wife-ah love-vu pannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wife-ah love-vu pannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh nallaarukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh nallaarukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Wife-ah kiss-u panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wife-ah kiss-u panna"/>
</div>
<div class="lyrico-lyrics-wrapper">God-u bless-u pannum mmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="God-u bless-u pannum mmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wife-u mattundhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wife-u mattundhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-u machiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-u machiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava eppodhumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava eppodhumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan katchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan katchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullaanaalum purushan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullaanaalum purushan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaanaalum kanavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaanaalum kanavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu mudichi potta pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu mudichi potta pinnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava ponnu illa thaaram machiii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ava ponnu illa thaaram machiii"/>
</div>
<div class="lyrico-lyrics-wrapper">Very nice pondaatti pondaatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Very nice pondaatti pondaatti"/>
</div>
<div class="lyrico-lyrics-wrapper">Matraan thottathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matraan thottathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Malligai poovae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malligai poovae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookkum podhu vaasamaa illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkum podhu vaasamaa illa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paalum pazhamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalum pazhamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigalil yendhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigalil yendhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Parkkum podhae paravasa yella
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parkkum podhae paravasa yella"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudichaa kattikkooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichaa kattikkooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudikkalannaa uttudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikkalannaa uttudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha nee uttuputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha nee uttuputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Violence-u pannaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Violence-u pannaadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Purushana pottu thalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purushana pottu thalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Koolipadai vaikaadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koolipadai vaikaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pondaattiya vettiputtu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondaattiya vettiputtu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeyilukku pogadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeyilukku pogadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaru wife-nu clear-ah sollaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru wife-nu clear-ah sollaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Clever-a uttu taandaa paavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Clever-a uttu taandaa paavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Illara vaazhkaikae kodaichalada saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illara vaazhkaikae kodaichalada saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wife-ah love-vu pannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wife-ah love-vu pannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Life-eh nallaarukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life-eh nallaarukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Wife-ah kiss-u panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wife-ah kiss-u panna"/>
</div>
<div class="lyrico-lyrics-wrapper">God-u bless-u pannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="God-u bless-u pannum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Wife-a correct-u pannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wife-a correct-u pannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum merit-u yerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum merit-u yerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaa aanaa aanaa aanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaa aanaa aanaa aanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaaru wife-nu clear-ah sollaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaru wife-nu clear-ah sollaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Clever-a uttu taandaa paavi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Clever-a uttu taandaa paavi"/>
</div>
<div class="lyrico-lyrics-wrapper">Illara vaazhkaikae kodaichalada saami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illara vaazhkaikae kodaichalada saami"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanae naa naa nae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae naa naa nae "/>
</div>
<div class="lyrico-lyrics-wrapper">thannanae naa naa nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannanae naa naa nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae naa naa nae thannaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae naa naa nae thannaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaanae naa naa nae 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae naa naa nae "/>
</div>
<div class="lyrico-lyrics-wrapper">thannanae naa naa nae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thannanae naa naa nae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanae naa naa nae thannaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanae naa naa nae thannaaaaa"/>
</div>
</pre>
