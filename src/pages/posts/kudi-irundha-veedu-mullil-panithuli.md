---
title: "kudi irundha veedu song lyrics"
album: "Mullil Panithuli"
artist: "Benny Pradeep "
lyricist: "Kavingar sura"
director: "NM Jegan"
path: "/albums/mullil-panithuli-lyrics"
song: "Kudi Irundha Veedu"
image: ../../images/albumart/mullil-panithuli.jpg
date: 2021-10-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6nRhKGam0ts"
type: "sad"
singers:
  - Benny Pradeep 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kudi iruntha veedu ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudi iruntha veedu ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">sontham indri pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sontham indri pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda vantha kula saamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda vantha kula saamy"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna moodi pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna moodi pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannugala imaiyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannugala imaiyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">neeyum irunthu kaaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeyum irunthu kaaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">intha ponnugala nirkathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha ponnugala nirkathiya"/>
</div>
<div class="lyrico-lyrics-wrapper">nikka vittu pona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nikka vittu pona"/>
</div>
<div class="lyrico-lyrics-wrapper">vidhi aadum aatathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidhi aadum aatathula"/>
</div>
<div class="lyrico-lyrics-wrapper">pommaigala ivanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pommaigala ivanga"/>
</div>
<div class="lyrico-lyrics-wrapper">mull meedu pani thuliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mull meedu pani thuliya"/>
</div>
<div class="lyrico-lyrics-wrapper">thangavilla ponga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangavilla ponga"/>
</div>
<div class="lyrico-lyrics-wrapper">saabange ivangaluku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saabange ivangaluku"/>
</div>
<div class="lyrico-lyrics-wrapper">varangal endru aacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varangal endru aacha"/>
</div>
<div class="lyrico-lyrics-wrapper">entha saamy vanthu thalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha saamy vanthu thalai"/>
</div>
<div class="lyrico-lyrics-wrapper">ezhutha maathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ezhutha maathi "/>
</div>
<div class="lyrico-lyrics-wrapper">ezhuthum puthusa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ezhuthum puthusa"/>
</div>
</pre>
