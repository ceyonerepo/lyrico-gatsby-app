---
title: "vizhuvathum ezhuvathum song lyrics"
album: "Pon Manickavel"
artist: "D. Imman"
lyricist: "Viveka"
director: "A.C. Mugil Chellappan"
path: "/albums/pon-manickavel-lyrics"
song: "Vizhuvathum Ezhuvathum"
image: ../../images/albumart/pon-manickavel.jpg
date: 2021-11-19
lang: tamil
youtubeLink: "https://www.youtube.com/embed/nvi3T8U0Eys"
type: "sad"
singers:
  - D. Imman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vizhuvathum Ezhuvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhuvathum Ezhuvathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanada Vaazhkaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanada Vaazhkaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhumvarai Idhayathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhumvarai Idhayathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumae Vetkayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumae Vetkayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhithunayaai Ival Nesam Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhithunayaai Ival Nesam Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothum Pothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum Pothumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigalin Oraththu Eerangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigalin Oraththu Eerangal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaainthu Pogumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaainthu Pogumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urangida Oru Madi Irukkirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangida Oru Madi Irukkirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endrumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endrumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkena Anudhinam Thudikkirathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena Anudhinam Thudikkirathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koodaiyil Koodaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodaiyil Koodaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoovidum Megam Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoovidum Megam Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodavae Koodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodavae Koodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhevathai Nizhal Varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhevathai Nizhal Varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raathiri Paadhaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raathiri Paadhaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Veesidum Velicham Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesidum Velicham Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhalan Kaavalan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhalan Kaavalan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aarudhal Vazhi Tharum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aarudhal Vazhi Tharum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhuvathum Ezhuvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhuvathum Ezhuvathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanada Vaazhkaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanada Vaazhkaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhumvarai Idhayathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhumvarai Idhayathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumae Vetkayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumae Vetkayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Erimalai Therippena Seerivaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erimalai Therippena Seerivaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Saambal Aakkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saambal Aakkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiriyin Padaigalai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiriyin Padaigalai "/>
</div>
<div class="lyrico-lyrics-wrapper">Theeramai Modhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeramai Modhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaikkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaikkalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarithiram Muzhuvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarithiram Muzhuvathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeyithavargal Yaaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeyithavargal Yaaradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadaigalai Thurumbena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadaigalai Thurumbena"/>
</div>
<div class="lyrico-lyrics-wrapper">Mathithavargal Thaanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathithavargal Thaanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Soorayaai Modhiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soorayaai Modhiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Porilae Vaagai Soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porilae Vaagai Soodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yelanam Seidhavar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yelanam Seidhavar"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarthida Padhil Kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthida Padhil Kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaigalae Aayutham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigalae Aayutham"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalathai Sondham Aakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalathai Sondham Aakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanilam Thaandiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanilam Thaandiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjida Nadanthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjida Nadanthidu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhuvathum Ezhuvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhuvathum Ezhuvathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaanada Vaazhkaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaanada Vaazhkaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhumvarai Idhayathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhumvarai Idhayathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendumae Vetkayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendumae Vetkayae"/>
</div>
</pre>
