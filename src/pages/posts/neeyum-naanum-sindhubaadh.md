---
title: "neeyum naanum song lyrics"
album: "Sindhubaadh"
artist: "Yuvan Shankar Raja"
lyricist: "Karthik Netha"
director: "S.U. Arun Kumar"
path: "/albums/sindhubaadh-lyrics"
song: "Neeyum Naanum"
image: ../../images/albumart/sindhubaadh.jpg
date: 2019-06-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/g5HL5WitEq4"
type: "love"
singers:
  - Santhosh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Paartha Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Paartha Vaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theigirathey Theigirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theigirathey Theigirathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarbin Meedhu Vaazhum Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbin Meedhu Vaazhum Vaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkirathey Ketkirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkirathey Ketkirathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engey Sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engey Sendraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Mazhai Thuli Nanjaaguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mazhai Thuli Nanjaaguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneer Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Pirivinil Mullaaguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pirivinil Mullaaguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanaliye Kaanaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaliye Kaanaliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaangi Vandha Vaanavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Vandha Vaanavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meettiduven Meettiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meettiduven Meettiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannam Innum Theeravillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Innum Theeravillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Paartha Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Paartha Vaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theigirathey Theigirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theigirathey Theigirathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarbin Meedhu Vaazhum Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbin Meedhu Vaazhum Vaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkirathey Ketkirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkirathey Ketkirathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendaam Endrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendaam Endrey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam Koorum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Koorum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothum Endrey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothum Endrey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadham Vegum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadham Vegum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aanalum Malaithaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanalum Malaithaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeraadha Kadal Thaandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraadha Kadal Thaandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee Thaandi Varuveney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee Thaandi Varuveney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poru Kanmani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poru Kanmani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvennai Veruthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvennai Veruthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vayadheri Naraithaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayadheri Naraithaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Vaadum Nilam Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vaadum Nilam Thedi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varuvenadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oon Ullam Sidhainthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oon Ullam Sidhainthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Uyir Ennai Pirinthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Ennai Pirinthaalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unai Kaana Varuveney Naaney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai Kaana Varuveney Naaney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Theaney Vandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Theaney Vandheney"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engey Sendraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engey Sendraai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Mazhai Thuli Nanjaaguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mazhai Thuli Nanjaaguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanneer Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneer Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Pirivinil Mullaaguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Pirivinil Mullaaguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaanaliye Kaanaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaliye Kaanaliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaangi Vandha Vaanavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaangi Vandha Vaanavillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meettiduven Meettiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meettiduven Meettiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vannam Innum Theeravillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vannam Innum Theeravillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyum Naanum Paartha Vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyum Naanum Paartha Vaanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theigirathey Theigirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theigirathey Theigirathey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maarbin Meedhu Vaazhum Vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarbin Meedhu Vaazhum Vaasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ketkirathey Ketkirathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketkirathey Ketkirathey"/>
</div>
</pre>
