---
title: "vaathil chaari song lyrics"
album: "Trance"
artist: "Jackson Vijayan - Vinayakan"
lyricist: "Vinayak Sasikumar"
director: "Anwar Rasheed"
path: "/albums/trance-lyrics"
song: "Vaathil Chaari"
image: ../../images/albumart/trance.jpg
date: 2020-02-20
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/50YasBKNrJU"
type: "happy"
singers:
  - Shakthisree Gopalan
  - Jackson Vijayan
  - Tony John
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaathil Chaari Vanna Thinkale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaathil Chaari Vanna Thinkale"/>
</div>
<div class="lyrico-lyrics-wrapper">Raavil Enthu Thedi Vannuvo Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavil Enthu Thedi Vannuvo Nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bheethi Kondu Paathi Maanjuvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bheethi Kondu Paathi Maanjuvo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninte Roopam Aardra Bimbamaai En Munnil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninte Roopam Aardra Bimbamaai En Munnil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Njaan Kaanathe Entha Thaalil Pootti Nee Mookamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Njaan Kaanathe Entha Thaalil Pootti Nee Mookamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kannaal Moodum Manthra Theeyil Mayangave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kannaal Moodum Manthra Theeyil Mayangave"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Unmaadam Kinaavo Nero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Unmaadam Kinaavo Nero"/>
</div>
<div class="lyrico-lyrics-wrapper">Novaai Maarumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Novaai Maarumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Swakaaryangal Vimookam Thaane Maayumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Swakaaryangal Vimookam Thaane Maayumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Mulmunayil Naame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Mulmunayil Naame"/>
</div>
<div class="lyrico-lyrics-wrapper">Anyonyam Melle Nokki Nilpoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anyonyam Melle Nokki Nilpoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naam Naamallaathakunno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam Naamallaathakunno"/>
</div>
<div class="lyrico-lyrics-wrapper">Innetho Vichaarangalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innetho Vichaarangalil"/>
</div>
</pre>
