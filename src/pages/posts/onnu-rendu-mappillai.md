---
title: "onnu rendu song lyrics"
album: "Mappillai"
artist: "Mani Sharma"
lyricist: "Pa. Vijay"
director: "Suraj"
path: "/albums/mappillai-lyrics"
song: "Onnu Rendu"
image: ../../images/albumart/mappillai.jpg
date: 2011-04-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NGAZ92NP_rY"
type: "happy"
singers:
  - Mukesh
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Onnu rendu moonudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu rendu moonudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhillirundha vaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhillirundha vaangadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanda saeval naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda saeval naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambusanda izhungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambusanda izhungadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanda vanthaa irangudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda vanthaa irangudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhi paaru veenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhi paaru veenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koothuppaathaen aadum aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothuppaathaen aadum aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty chuvaththil koodum koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty chuvaththil koodum koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Summanaachum sulukkaagaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summanaachum sulukkaagaattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaa varaa pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaa varaa pinnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaiyosa munnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyosa munnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha vantha thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha vantha thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaippaalae kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaippaalae kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathaadi adraang goiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaadi adraang goiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaan varaan pinaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaan varaan pinaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vamboda munaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vamboda munaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaan vanthaan thanaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaan vanthaan thanaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuppaanae kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuppaanae kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothaadi pudraa kaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothaadi pudraa kaiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lai lai lai laiyo hosae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lai lai lai laiyo hosae"/>
</div>
<div class="lyrico-lyrics-wrapper">Lai lai lai laiyo hosae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lai lai lai laiyo hosae"/>
</div>
<div class="lyrico-lyrics-wrapper">Humkumkum ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Humkumkum ahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Humkumkum ahaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Humkumkum ahaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy heyy heyy heyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy heyy heyy heyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Lai lai lai laiyo hosae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lai lai lai laiyo hosae"/>
</div>
<div class="lyrico-lyrics-wrapper">Lai lai lai laiyo hosae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lai lai lai laiyo hosae"/>
</div>
<div class="lyrico-lyrics-wrapper">Laiyosae laiyosae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laiyosae laiyosae"/>
</div>
<div class="lyrico-lyrics-wrapper">Laiyosae laiyosae laiyosae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laiyosae laiyosae laiyosae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un aattam en aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aattam en aattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnaana oorula koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnaana oorula koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Haiyaa haiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haiyaa haiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aahaa ohho paatu vanthaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aahaa ohho paatu vanthaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aatam paatam thookkum thannaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aatam paatam thookkum thannaalae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adichi ninnaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichi ninnaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedichu vanthaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedichu vanthaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennanu sollatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennanu sollatum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasanga kondaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasanga kondaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruva ponnaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruva ponnaada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ila raththam soodaagattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ila raththam soodaagattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaa varaa pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaa varaa pinnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaiyosa munnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyosa munnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha vantha thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha vantha thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaippaalae kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaippaalae kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathaadi adraang goiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaadi adraang goiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaan varaan pinaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaan varaan pinaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vamboda munaalaVanthaan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vamboda munaalaVanthaan "/>
</div>
<div class="lyrico-lyrics-wrapper">vanthaan thanaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanthaan thanaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuppaanae kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuppaanae kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothaadi pudraa kaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothaadi pudraa kaiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on"/>
</div>
<div class="lyrico-lyrics-wrapper">Yo yo yo yo yoyo yoyo yoyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo yo yo yo yoyo yoyo yoyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaai vaarthai thappaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaai vaarthai thappaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaipodum veththala paaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaipodum veththala paaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakku thaakku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakku thaakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattutheeya munna vanthaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattutheeya munna vanthaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Beedi paththa vaipaan nammalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beedi paththa vaipaan nammalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiyae saroja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyae saroja"/>
</div>
<div class="lyrico-lyrics-wrapper">Atha itha sollithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atha itha sollithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi nenja soodaethura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi nenja soodaethura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethiri nanbennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethiri nanbennu"/>
</div>
<div class="lyrico-lyrics-wrapper">Evanum ingilla venumna kaipodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evanum ingilla venumna kaipodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaa varaa pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaa varaa pinnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaiyosa munnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyosa munnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha vantha thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha vantha thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaippaalae kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaippaalae kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathaadi adraang goiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaadi adraang goiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaan varaan pinaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaan varaan pinaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vamboda munaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vamboda munaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaan vanthaan thanaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaan vanthaan thanaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuppaanae kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuppaanae kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothaadi pudraa kaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothaadi pudraa kaiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onnu rendu moonudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnu rendu moonudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhillirundha vaangadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhillirundha vaangadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanda saeval naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda saeval naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Vambusanda izhungadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vambusanda izhungadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sanda vanthaa irangudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sanda vanthaa irangudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Modhi paaru veenga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Modhi paaru veenga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Koothuppaathaen aadum aatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothuppaathaen aadum aatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty chuvaththil koodum koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty chuvaththil koodum koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Summanaachum sulukkaagaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summanaachum sulukkaagaattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaa varaa pinnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaa varaa pinnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaiyosa munnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaiyosa munnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vantha vantha thannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vantha vantha thannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaippaalae kannala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaippaalae kannala"/>
</div>
<div class="lyrico-lyrics-wrapper">Aathaadi adraang goiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aathaadi adraang goiyaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varaan varaan pinaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varaan varaan pinaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vamboda munaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vamboda munaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthaan vanthaan thanaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthaan vanthaan thanaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuppaanae kannaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuppaanae kannaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothaadi pudraa kaiyaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothaadi pudraa kaiyaala"/>
</div>
</pre>
