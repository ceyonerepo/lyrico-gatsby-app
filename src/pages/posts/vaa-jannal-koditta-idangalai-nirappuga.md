---
title: "vaa jannal song lyrics"
album: "Koditta Idangalai Nirappuga"
artist: "C Sathya"
lyricist: "Muthamil"
director: "Parthiban"
path: "/albums/koditta-idangalai-nirappuga-lyrics"
song: "Vaa Jannal"
image: ../../images/albumart/koditta-idangalai-nirappuga.jpg
date: 2017-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZDyayAvJBms"
type: "love"
singers:
  - Sriram Parthasarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Vaa jannal oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa jannal oram"/>
</div>
<div class="lyrico-lyrics-wrapper">Minnal poovaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minnal poovaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannaal theenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannaal theenda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaamal veen poaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaamal veen poaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pennil adangaamal arangerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pennil adangaamal arangerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anal aasai velvikku theeyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anal aasai velvikku theeyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Thikki thikkiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikki thikkiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pugaiyudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pugaiyudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Moolai mangudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moolai mangudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodaamalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodaamalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam sikkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam sikkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Wooh woohu nnuu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wooh woohu nnuu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maraiya maraiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maraiya maraiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Theriyum azhagu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theriyum azhagu"/>
</div>
<div class="lyrico-lyrics-wrapper">SemmeniyoAdaiya adaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="SemmeniyoAdaiya adaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidaiyum ariya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidaiyum ariya"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjengudho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjengudho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuzhaiya nuzhaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuzhaiya nuzhaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamai muyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamai muyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindaadudho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindaadudho"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyil madiyavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyil madiyavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandraadudho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandraadudho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saththamindriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saththamindriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhikkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhikkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Siththam ondriyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siththam ondriyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudhikkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudhikkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muththam muthudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muththam muthudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Wooh wooh ooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wooh wooh ooooo"/>
</div>
</pre>
