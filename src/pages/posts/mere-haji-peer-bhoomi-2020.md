---
title: "mere haji peer song lyrics"
album: "Bhoomi 2020"
artist: "Salim Sulaiman"
lyricist: "Darab Farooqui - Osman Mir - Dhiren Garg"
director: "Shakti Hasija"
path: "/albums/bhoomi-2020-lyrics"
song: "Mere Haji Peer"
image: ../../images/albumart/bhoomi-2020.jpg
date: 2020-12-30
lang: hindi
youtubeLink: "https://www.youtube.com/embed/2FG63JoTgBQ"
type: "happy"
singers:
  - Salim Merchant
  - Osman Mir
  - Raj Pandit
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Maula.. Maula..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maula.. Maula.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dua meri poori kara de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dua meri poori kara de"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere haji peer auliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere haji peer auliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Sifarish tu meri laga de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sifarish tu meri laga de"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere haji peer auliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere haji peer auliya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aauaan ke pukariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aauaan ke pukariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aauaan ke pukariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aauaan ke pukariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghadi hikk na visariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghadi hikk na visariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Muja haji peer auliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muja haji peer auliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aauaan ke pukariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aauaan ke pukariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho haji peer auliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho haji peer auliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aauaan ke pukariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aauaan ke pukariyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My wishes come true
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My wishes come true"/>
</div>
<div class="lyrico-lyrics-wrapper">When I am close to you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When I am close to you"/>
</div>
<div class="lyrico-lyrics-wrapper">I'll be there where you are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'll be there where you are"/>
</div>
<div class="lyrico-lyrics-wrapper">No matter how far
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No matter how far"/>
</div>
<div class="lyrico-lyrics-wrapper">I'll come to you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'll come to you"/>
</div>
<div class="lyrico-lyrics-wrapper">Come to you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come to you"/>
</div>
<div class="lyrico-lyrics-wrapper">My wishes come true
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My wishes come true"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bande ko ab to jazaa de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bande ko ab to jazaa de"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere haji peer auliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere haji peer auliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Zarre ko zariya bana de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zarre ko zariya bana de"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere haji peer auliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere haji peer auliya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aauaan ke pukariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aauaan ke pukariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aauaan ke pukariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aauaan ke pukariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ghadi hikk na visariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ghadi hikk na visariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Muja haji peer auliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muja haji peer auliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aauaan ke pukariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aauaan ke pukariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho haji peer auliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho haji peer auliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aauaan ke pukariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aauaan ke pukariyan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My wishes come true
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My wishes come true"/>
</div>
<div class="lyrico-lyrics-wrapper">Wishes come true
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Wishes come true"/>
</div>
<div class="lyrico-lyrics-wrapper">When I am close to you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="When I am close to you"/>
</div>
<div class="lyrico-lyrics-wrapper">Close to you to you to you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Close to you to you to you"/>
</div>
<div class="lyrico-lyrics-wrapper">I'll be there where you are
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'll be there where you are"/>
</div>
<div class="lyrico-lyrics-wrapper">No matter how far
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No matter how far"/>
</div>
<div class="lyrico-lyrics-wrapper">No matter how far
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No matter how far"/>
</div>
<div class="lyrico-lyrics-wrapper">I'll come to you yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'll come to you yeah"/>
</div>
<div class="lyrico-lyrics-wrapper">I'll come to you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I'll come to you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Skies are cloudy feel your presence
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Skies are cloudy feel your presence"/>
</div>
<div class="lyrico-lyrics-wrapper">In my body when I am close to you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In my body when I am close to you"/>
</div>
<div class="lyrico-lyrics-wrapper">In you plan I won't lose faith
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In you plan I won't lose faith"/>
</div>
<div class="lyrico-lyrics-wrapper">Just take my hand my wishes come true
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just take my hand my wishes come true"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muja haji peer auliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muja haji peer auliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aauaan ke pukariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aauaan ke pukariyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho haji peer auliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho haji peer auliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Aauaan ke pukariyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aauaan ke pukariyan"/>
</div>
</pre>
