---
title: "naa petha saami song lyrics"
album: "Adutha Saattai"
artist: "Justin Prabhakaran"
lyricist: "Thenmozhi Das - Justin Prabhakaran"
director: "M. Anbazhagan"
path: "/albums/adutha-saattai-lyrics"
song: "Naa Petha Saami"
image: ../../images/albumart/adutha-saattai.jpg
date: 2019-11-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rC4ClyqdhQU"
type: "sad"
singers:
  - Resmi Sateesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan Petha Saamy Kanmoodi Thoongaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Petha Saamy Kanmoodi Thoongaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallakkath Thooki Oor Koodi Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallakkath Thooki Oor Koodi Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Petha Saamy Enggaiyaa Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Petha Saamy Enggaiyaa Pora"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padichu Pattam Oola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padichu Pattam Oola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vangi Thuyar Maathuvaennu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vangi Thuyar Maathuvaennu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sonniyae Raasa Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonniyae Raasa Ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Panna Oola Pinni Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panna Oola Pinni Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Porandhu Enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porandhu Enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Yayiyaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yayiyaaaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enga Vandhu Unna Pakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga Vandhu Unna Pakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Maradaippill Unna Saekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maradaippill Unna Saekka"/>
</div>
<div class="lyrico-lyrics-wrapper">Maru Jenmam Undaa Neeyum Sollaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maru Jenmam Undaa Neeyum Sollaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Naan Kadaikka Naadhi Illa Yaedhamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Naan Kadaikka Naadhi Illa Yaedhamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Petha Saamy Kanmoodi Thoongaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Petha Saamy Kanmoodi Thoongaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pallakkath Thooki Oor Koodi Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallakkath Thooki Oor Koodi Poga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Petha Saamy Enggaiyaa Pora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Petha Saamy Enggaiyaa Pora"/>
</div>
</pre>
