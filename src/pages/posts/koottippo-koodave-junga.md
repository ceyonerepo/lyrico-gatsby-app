---
title: "koottippo koodave song lyrics"
album: "Junga"
artist: "Siddharth Vipin"
lyricist: "Lalithanand"
director: "Gokul"
path: "/albums/junga-lyrics"
song: "Koottippo Koodave"
image: ../../images/albumart/junga.jpg
date: 2018-07-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/9RySFYRmYIg"
type: "love"
singers:
  - Sathyaprakash
  - Ranina Reddy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nee yaaro yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yaaro yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindraai yaadhumaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindraai yaadhumaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelaadho innaal thooramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelaadho innaal thooramaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogatha saalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogatha saalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan kaanaa vaanilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan kaanaa vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neradha yedho neralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neradha yedho neralai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anmaiyil nee parthu nirkindra neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anmaiyil nee parthu nirkindra neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Menmaiyaai kai korthu pogavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Menmaiyaai kai korthu pogavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Medhuvaai melnaattu megam yengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Medhuvaai melnaattu megam yengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nagaraa naal vendumae vendumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nagaraa naal vendumae vendumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam thaandiyum koottippo koodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam thaandiyum koottippo koodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalillaa theevugal koottippo koodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalillaa theevugal koottippo koodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanadha veridam koottippo koodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanadha veridam koottippo koodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaadha oridam koottippo koodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaadha oridam koottippo koodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae ae aeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae ae aeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhaaadha soozhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhaaadha soozhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Nigar illaadha mudhal kaatchiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nigar illaadha mudhal kaatchiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae nee thandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae nee thandhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">En vaazhvaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vaazhvaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oli paayum kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oli paayum kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulir yedhedho aasai koottudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulir yedhedho aasai koottudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Adainthenae unnai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adainthenae unnai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaiyaalamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaiyaalamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhaadhi kesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaadhi kesam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondraatha maattramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondraatha maattramae"/>
</div>
<div class="lyrico-lyrics-wrapper">Paaraatha desam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paaraatha desam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaraadha vaasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaraadha vaasamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam thaandiyum koottippo koodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam thaandiyum koottippo koodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalillaa theevugal koottippo koodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalillaa theevugal koottippo koodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanadha veridam koottippo koodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanadha veridam koottippo koodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaadha oridam koottippo koodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaadha oridam koottippo koodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae ae aeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae ae aeae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee yaaro yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yaaro yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee yaaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee yaaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Nindraai yaadhumaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nindraai yaadhumaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelaadho innaal thooramaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelaadho innaal thooramaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogatha saalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogatha saalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon velai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon velai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaan kaanaa vaanilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan kaanaa vaanilai"/>
</div>
<div class="lyrico-lyrics-wrapper">Neradha yedho neralai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neradha yedho neralai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnudan naan serndhu pogindra podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnudan naan serndhu pogindra podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaiyil thol saaya thondrudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaiyil thol saaya thondrudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Unarvil nee poothu nirkindra podhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unarvil nee poothu nirkindra podhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Unaraa oru vaasamae vaasamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unaraa oru vaasamae vaasamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyyyeyeyyyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyyeyeyyyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagayam thaandiyum koottippo koodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagayam thaandiyum koottippo koodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalillaa theevugal koottippo koodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalillaa theevugal koottippo koodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanadha veridam koottippo koodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanadha veridam koottippo koodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhaadha oridam koottippo koodavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhaadha oridam koottippo koodavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae ae aeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae ae aeae"/>
</div>
</pre>
