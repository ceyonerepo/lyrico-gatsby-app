---
title: "ithanai yugamai song lyrics"
album: "Udhayan"
artist: "Manikanth Kadri"
lyricist: "Vaali - Yugabharathi - Annamalai - Surya - Muthamil"
director: "Chaplin"
path: "/albums/udhayan-lyrics"
song: "Ithanai Yugamai"
image: ../../images/albumart/udhayan.jpg
date: 2011-07-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bh-El2E-jsE"
type: "love"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ithanai yougamaai ithanai yougamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithanai yougamaai ithanai yougamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">engey irundhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engey irundhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">otrai nodiyil uyirai thirudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otrai nodiyil uyirai thirudi "/>
</div>
<div class="lyrico-lyrics-wrapper">engo maraindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engo maraindhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam vellai kaagidham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam vellai kaagidham"/>
</div>
<div class="lyrico-lyrics-wrapper">nee adhil ingey Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee adhil ingey Oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">en iru kangal sakkaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en iru kangal sakkaram"/>
</div>
<div class="lyrico-lyrics-wrapper">unai mattum sutridum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai mattum sutridum "/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">idhayam vellai kaagidham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam vellai kaagidham"/>
</div>
<div class="lyrico-lyrics-wrapper">nee adhil ingey Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee adhil ingey Oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">en iru kangal sakkaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en iru kangal sakkaram"/>
</div>
<div class="lyrico-lyrics-wrapper">unai mattum sutridum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai mattum sutridum "/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithanai yougamaai ithanai yougamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithanai yougamaai ithanai yougamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">engey irundhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engey irundhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">otrai nodiyil uyirai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otrai nodiyil uyirai "/>
</div>
<div class="lyrico-lyrics-wrapper">thirudi engo maraindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thirudi engo maraindhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">OhO oru naal mazhaiyaai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OhO oru naal mazhaiyaai "/>
</div>
<div class="lyrico-lyrics-wrapper">oru naal veyilaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru naal veyilaai"/>
</div>
<div class="lyrico-lyrics-wrapper">en mael vizhundhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en mael vizhundhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">mozhiyariyaadha oomai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mozhiyariyaadha oomai "/>
</div>
<div class="lyrico-lyrics-wrapper">poaley mounam sumandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poaley mounam sumandhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadalin thodakkam mazhaithuliyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalin thodakkam mazhaithuliyo"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal thodakkam ival vizhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal thodakkam ival vizhiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">OhO hO pidikkum nerukkam ival mattumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OhO hO pidikkum nerukkam ival mattumey"/>
</div>
<div class="lyrico-lyrics-wrapper">kadaisivaraiyil irukkattumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadaisivaraiyil irukkattumey"/>
</div>
<div class="lyrico-lyrics-wrapper">OhO neeyariyaadha ragasiyamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OhO neeyariyaadha ragasiyamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">unnudan endrum iruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnudan endrum iruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">nee vidum suvaasakaatriniley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee vidum suvaasakaatriniley"/>
</div>
<div class="lyrico-lyrics-wrapper">ennuyir kaatrai eduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennuyir kaatrai eduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam vellai kaagidham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam vellai kaagidham"/>
</div>
<div class="lyrico-lyrics-wrapper">nee adhil engey Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee adhil engey Oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">en iru kangal sarkkaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en iru kangal sarkkaram"/>
</div>
<div class="lyrico-lyrics-wrapper">unai mattum sutridum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai mattum sutridum "/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithanai yougamaai ithanai yougamaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithanai yougamaai ithanai yougamaai"/>
</div>
<div class="lyrico-lyrics-wrapper">engey irundhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engey irundhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">otrai nodiyil uyirai thirudi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otrai nodiyil uyirai thirudi "/>
</div>
<div class="lyrico-lyrics-wrapper">engo maraindhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engo maraindhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaavum kuzhandhai en manamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaavum kuzhandhai en manamo"/>
</div>
<div class="lyrico-lyrics-wrapper">thaayin madiyaai un mugamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayin madiyaai un mugamo"/>
</div>
<div class="lyrico-lyrics-wrapper">un kannin rasigai naan parandheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannin rasigai naan parandheney"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatril engo tholaigindren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatril engo tholaigindren"/>
</div>
<div class="lyrico-lyrics-wrapper">OhO un viral patri naan nadandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OhO un viral patri naan nadandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">ulaginai sutrida thoandrum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulaginai sutrida thoandrum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavugalellaam karaindhaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavugalellaam karaindhaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">nijamaai neethaan vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nijamaai neethaan vendum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">idhayam vellai kaagidham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam vellai kaagidham"/>
</div>
<div class="lyrico-lyrics-wrapper">nee adhil ingey Oviyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee adhil ingey Oviyam"/>
</div>
<div class="lyrico-lyrics-wrapper">en iru kangal sakkaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en iru kangal sakkaram"/>
</div>
<div class="lyrico-lyrics-wrapper">unai mattum sutridum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai mattum sutridum "/>
</div>
<div class="lyrico-lyrics-wrapper">dhinam dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinam dhinam"/>
</div>
</pre>
