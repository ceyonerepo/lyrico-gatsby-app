---
title: "mustaache vestaache song lyrics"
album: "Parris Jeyaraj"
artist: "Santhosh Narayanan"
lyricist: "Asal Kolaar"
director: "Johnson K"
path: "/albums/parris-jeyaraj-lyrics"
song: "Mustaache Vestaache"
image: ../../images/albumart/parris-jeyaraj.jpg
date: 2021-02-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/28xD5CtkI0k"
type: "Enjoy"
singers:
  - Asal Kolaar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Konja neram kaathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja neram kaathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathinelu quater-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathinelu quater-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh paathu oothure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh paathu oothure"/>
</div>
<div class="lyrico-lyrics-wrapper">Inikki happy birthday
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inikki happy birthday"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konja neram kaathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja neram kaathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy birthday
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy birthday"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathinelu quater-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathinelu quater-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy birthday
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy birthday"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh paathu oothure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh paathu oothure"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy birthday
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy birthday"/>
</div>
<div class="lyrico-lyrics-wrapper">Inikki happy birthday
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inikki happy birthday"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh mustaache musataache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh mustaache musataache"/>
</div>
<div class="lyrico-lyrics-wrapper">Full-ah mustaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full-ah mustaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh vestaache vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh vestaache vestaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Kacheri vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kacheri vestaache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh mustaache musataache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh mustaache musataache"/>
</div>
<div class="lyrico-lyrics-wrapper">Full-ah mustaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full-ah mustaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh vestaache vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh vestaache vestaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Kacheri vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kacheri vestaache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanda naai kathuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanda naai kathuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhula vangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhula vangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kambava navuthalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kambava navuthalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Biriyani sombu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Biriyani sombu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh pannatha tension
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh pannatha tension"/>
</div>
<div class="lyrico-lyrics-wrapper">Inikki annatha function
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inikki annatha function"/>
</div>
<div class="lyrico-lyrics-wrapper">Annathe majaavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annathe majaavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mansan san san san
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mansan san san san"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh mustaache musataache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh mustaache musataache"/>
</div>
<div class="lyrico-lyrics-wrapper">Full-ah mustaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full-ah mustaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh vestaache vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh vestaache vestaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Kacheri vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kacheri vestaache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh mustaache musataache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh mustaache musataache"/>
</div>
<div class="lyrico-lyrics-wrapper">Full-ah mustaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full-ah mustaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh vestaache vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh vestaache vestaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Kacheri vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kacheri vestaache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan gopal-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan gopal-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum vaai maal-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum vaai maal-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee keep smile-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee keep smile-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Maama chris gayle-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maama chris gayle-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee kooptonyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kooptonyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga odiyantom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga odiyantom"/>
</div>
<div class="lyrico-lyrics-wrapper">Annan heart-u kaatuvar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annan heart-u kaatuvar"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha anjaneyaraattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha anjaneyaraattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marriage panninu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marriage panninu"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetla irunthidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetla irunthidu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathuva thattuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathuva thattuvan"/>
</div>
<div class="lyrico-lyrics-wrapper">Carpenter-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Carpenter-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avana thorthanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avana thorthanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Goindhana pertanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goindhana pertanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhukellam povalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhukellam povalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Police station-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Police station-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh pannatha da tension
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh pannatha da tension"/>
</div>
<div class="lyrico-lyrics-wrapper">Inikki annathe-ku function
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inikki annathe-ku function"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma annatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma annatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Majaavana mansan san san san
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Majaavana mansan san san san"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh mustaache musataache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh mustaache musataache"/>
</div>
<div class="lyrico-lyrics-wrapper">Full-ah mustaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full-ah mustaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh vestaache vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh vestaache vestaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Kacheri vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kacheri vestaache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh mustaache musataache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh mustaache musataache"/>
</div>
<div class="lyrico-lyrics-wrapper">Full-ah mustaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full-ah mustaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh kuthache kuthache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh kuthache kuthache"/>
</div>
<div class="lyrico-lyrics-wrapper">Thollaiya kuthache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollaiya kuthache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Trials-u mudnji pochu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Trials-u mudnji pochu"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimae thaan en over-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimae thaan en over-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Taste-ah kudukka poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taste-ah kudukka poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivlo naala sober-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivlo naala sober-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivlo naala sober-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivlo naala sober-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivlo naala sober-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivlo naala sober-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">School-u time-u mudnji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="School-u time-u mudnji"/>
</div>
<div class="lyrico-lyrics-wrapper">Inimae thaan en homework-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimae thaan en homework-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Oscar-ah act-ah podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oscar-ah act-ah podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Dark-u night-u joker-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dark-u night-u joker-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakku tough kudukkura dosth-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku tough kudukkura dosth-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma speciality roast-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma speciality roast-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Unakku tough kudukkura dosth-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakku tough kudukkura dosth-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma speciality roast-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma speciality roast-u"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ah pannatha pa tension
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ah pannatha pa tension"/>
</div>
<div class="lyrico-lyrics-wrapper">Inikki annathe ku function
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inikki annathe ku function"/>
</div>
<div class="lyrico-lyrics-wrapper">Annatha majaavana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annatha majaavana"/>
</div>
<div class="lyrico-lyrics-wrapper">Mansan san san san
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mansan san san san"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh mustaache musataache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh mustaache musataache"/>
</div>
<div class="lyrico-lyrics-wrapper">Full-ah mustaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full-ah mustaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh vestaache vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh vestaache vestaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Kacheri vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kacheri vestaache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eh mustaache musataache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh mustaache musataache"/>
</div>
<div class="lyrico-lyrics-wrapper">Full-ah mustaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full-ah mustaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh kuthache kuthache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh kuthache kuthache"/>
</div>
<div class="lyrico-lyrics-wrapper">Thollaiya kuthache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thollaiya kuthache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konja neram kaathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja neram kaathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathinelu quater-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathinelu quater-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh paathu oothure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh paathu oothure"/>
</div>
<div class="lyrico-lyrics-wrapper">Inikki happy birthday
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inikki happy birthday"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh mustaache musataache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh mustaache musataache"/>
</div>
<div class="lyrico-lyrics-wrapper">Full-ah mustaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full-ah mustaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh vestaache vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh vestaache vestaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Kacheri vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kacheri vestaache"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konja neram kaathiru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konja neram kaathiru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pathinelu quater-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pathinelu quater-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh paathu oothure
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh paathu oothure"/>
</div>
<div class="lyrico-lyrics-wrapper">Inikki happy birthday
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inikki happy birthday"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh mustaache musataache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh mustaache musataache"/>
</div>
<div class="lyrico-lyrics-wrapper">Full-ah mustaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full-ah mustaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Eh vestaache vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eh vestaache vestaache"/>
</div>
<div class="lyrico-lyrics-wrapper">Kacheri vestaache
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kacheri vestaache"/>
</div>
</pre>
