---
title: "poombarai vazhbavane song lyrics"
album: "Akka Kuruvi"
artist: "Ilaiyaraaja"
lyricist: "Ilaiyaraaja"
director: "Samy"
path: "/albums/akka-kuruvi-lyrics"
song: "Poombarai Vazhbavane"
image: ../../images/albumart/akka-kuruvi.jpg
date: 2022-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/8n7BF-06DV4"
type: "devotional"
singers:
  - Mukesh
  - Mahalinga VM
  - Velmurugan
  - Sellanguppan Mu Subramany
  - Anitha K
  - Chinnaponnu
  - Magizhini Manimaaran
  - A Dhanalakshmi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">poombarai vaalbavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poombarai vaalbavane"/>
</div>
<div class="lyrico-lyrics-wrapper">pootha kanam kuthipavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pootha kanam kuthipavane"/>
</div>
<div class="lyrico-lyrics-wrapper">poothalathai aalbavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poothalathai aalbavane"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">poombarai vaalbavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poombarai vaalbavane"/>
</div>
<div class="lyrico-lyrics-wrapper">pootha kanam kuthipavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pootha kanam kuthipavane"/>
</div>
<div class="lyrico-lyrics-wrapper">poothalathai aalbavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poothalathai aalbavane"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mel ulagai aalbavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mel ulagai aalbavane"/>
</div>
<div class="lyrico-lyrics-wrapper">keel irangi vanthavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="keel irangi vanthavane"/>
</div>
<div class="lyrico-lyrics-wrapper">aelai emmai kathu arulvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aelai emmai kathu arulvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">potra oru sollum illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potra oru sollum illai"/>
</div>
<div class="lyrico-lyrics-wrapper">pugala oru vaarthai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pugala oru vaarthai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaakinile vanthu arulvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaakinile vanthu arulvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boomiyile nadapathellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyile nadapathellam"/>
</div>
<div class="lyrico-lyrics-wrapper">poruthu irunthu paarpavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poruthu irunthu paarpavane"/>
</div>
<div class="lyrico-lyrics-wrapper">pogum vali kodu thunaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pogum vali kodu thunaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sontha sanam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sontha sanam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">engal thunbam kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal thunbam kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">summa irukku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="summa irukku "/>
</div>
<div class="lyrico-lyrics-wrapper">kidaithathu un thunaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kidaithathu un thunaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">seitha vinai paavangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seitha vinai paavangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">sertha palan enakarulvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sertha palan enakarulvai"/>
</div>
<div class="lyrico-lyrics-wrapper">puniyame thantu arulvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puniyame thantu arulvai"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna thunbam aana pothum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thunbam aana pothum"/>
</div>
<div class="lyrico-lyrics-wrapper">entha sanam enga pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha sanam enga pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">thurathi engum thunai irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thurathi engum thunai irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai velappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">moondreluthu sandai ittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moondreluthu sandai ittu"/>
</div>
<div class="lyrico-lyrics-wrapper">thangaiyile moondrai katti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thangaiyile moondrai katti"/>
</div>
<div class="lyrico-lyrics-wrapper">sangadangal theerpavane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sangadangal theerpavane"/>
</div>
<div class="lyrico-lyrics-wrapper">velappa velappa velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velappa velappa velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">velappa velappa velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velappa velappa velappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vembirunthu nonbirunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vembirunthu nonbirunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">viratham kondu vendi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viratham kondu vendi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pongal ittu poosai ittom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pongal ittu poosai ittom"/>
</div>
<div class="lyrico-lyrics-wrapper">velappa velappa velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velappa velappa velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">velappa velappa velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velappa velappa velappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">evvalavu potiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="evvalavu potiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">poosalum padamaiyilum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poosalum padamaiyilum"/>
</div>
<div class="lyrico-lyrics-wrapper">un poosai otrumai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un poosai otrumai than"/>
</div>
<div class="lyrico-lyrics-wrapper">velappa velappa velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velappa velappa velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">velappa velappa velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velappa velappa velappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai andi vendi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai andi vendi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">odambu engum alagu kuththi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odambu engum alagu kuththi"/>
</div>
<div class="lyrico-lyrics-wrapper">koodi vanthu vengai sonnom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koodi vanthu vengai sonnom"/>
</div>
<div class="lyrico-lyrics-wrapper">velappa velappa velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velappa velappa velappa"/>
</div>
<div class="lyrico-lyrics-wrapper">velappa velappa velappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="velappa velappa velappa"/>
</div>
</pre>
