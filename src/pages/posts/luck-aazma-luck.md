---
title: "luck aazma song lyrics"
album: "Luck"
artist: "Salim–Sulaiman - Amar Mohile"
lyricist: "Shabbir Ahmed"
director: "Soham Shah"
path: "/albums/luck-lyrics"
song: "Luck Aazma"
image: ../../images/albumart/luck.jpg
date: 2009-07-24
lang: hindi
youtubeLink: "https://www.youtube.com/embed/aN7MUjGU3pY"
type: "happy"
singers:
  - Sukhwinder Singh
  - Satya Hinduja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aazma aazma aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma aazma aazma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazma waqt aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma waqt aazma"/>
</div>
<div class="lyrico-lyrics-wrapper">Apni kismat ki baazi aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni kismat ki baazi aazma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Subaha nayi aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Subaha nayi aazma"/>
</div>
<div class="lyrico-lyrics-wrapper">Haath saare aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haath saare aazma"/>
</div>
<div class="lyrico-lyrics-wrapper">Apni kismat ki baazi aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apni kismat ki baazi aazma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zameen se uthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zameen se uthaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Phalak pe bithaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phalak pe bithaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Meharbaan ho jaaye gar khuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meharbaan ho jaaye gar khuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Hawaon mein chahe chirag jalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hawaon mein chahe chirag jalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Meharbaan ho jaaye gar khuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meharbaan ho jaaye gar khuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazma luck aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazma luck aazma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazmaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazmaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazma luck aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazma luck aazma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazmaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazmaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saari kudrat saara aalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saari kudrat saara aalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Tujh pe ho jaayenge yu fida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tujh pe ho jaayenge yu fida"/>
</div>
<div class="lyrico-lyrics-wrapper">Saari duniya saare mausam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saari duniya saare mausam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aise mil jaayenge baakhuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aise mil jaayenge baakhuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazma luck aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazma luck aazma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazmaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazmaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazma luck aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazma luck aazma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazmaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazmaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">It’s not about the money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s not about the money"/>
</div>
<div class="lyrico-lyrics-wrapper">Not about the honesty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Not about the honesty"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t you think it’s rather funny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t you think it’s rather funny"/>
</div>
<div class="lyrico-lyrics-wrapper">Luck Luck Luck is the key
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luck Luck Luck is the key"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">It’s not about the money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s not about the money"/>
</div>
<div class="lyrico-lyrics-wrapper">Not about the honesty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Not about the honesty"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t you think it’s rather funny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t you think it’s rather funny"/>
</div>
<div class="lyrico-lyrics-wrapper">Luck Luck Luck is the key
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luck Luck Luck is the key"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hai tera wo aashiyaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hai tera wo aashiyaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Chu le tu yeh aasmaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chu le tu yeh aasmaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Manzilon se aage nikal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manzilon se aage nikal"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir tere dono yeh jahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir tere dono yeh jahan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zameen se uthaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zameen se uthaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Phalak pe bithaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phalak pe bithaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Meharbaan ho jaaye gar khuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meharbaan ho jaaye gar khuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Hawaon mein chahe chirag jalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hawaon mein chahe chirag jalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Meharbaan ho jaaye gar khuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meharbaan ho jaaye gar khuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazma luck aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazma luck aazma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazmaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazmaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazma luck aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazma luck aazma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazmaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazmaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tedhe mere raaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tedhe mere raaste"/>
</div>
<div class="lyrico-lyrics-wrapper">Sab hain tere vaaste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sab hain tere vaaste"/>
</div>
<div class="lyrico-lyrics-wrapper">Har mushkil aasan bane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Har mushkil aasan bane"/>
</div>
<div class="lyrico-lyrics-wrapper">Chalna tu aahiste
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chalna tu aahiste"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Zameen se uthaaye phalak pe bithaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zameen se uthaaye phalak pe bithaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Meharbaan ho jaaye gar khuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meharbaan ho jaaye gar khuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Hawaon mein chahe chirag jalaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hawaon mein chahe chirag jalaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Meharbaan ho jaaye gar khuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meharbaan ho jaaye gar khuda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazma luck aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazma luck aazma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazmaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazmaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazma luck aazma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazma luck aazma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazma luck aazma aazmaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazma luck aazma aazmaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">It’s not about the money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s not about the money"/>
</div>
<div class="lyrico-lyrics-wrapper">Not about the honesty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Not about the honesty"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t you think it’s rather funny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t you think it’s rather funny"/>
</div>
<div class="lyrico-lyrics-wrapper">Luck Luck Luck is the key
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luck Luck Luck is the key"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">It’s not about the money
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It’s not about the money"/>
</div>
<div class="lyrico-lyrics-wrapper">Not about the honesty
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Not about the honesty"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t you think it’s rather funny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t you think it’s rather funny"/>
</div>
<div class="lyrico-lyrics-wrapper">Luck Luck Luck is the key
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Luck Luck Luck is the key"/>
</div>
</pre>
