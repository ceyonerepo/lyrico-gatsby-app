---
title: "aadaludan paadalai song lyrics"
album: "Motta Shiva Ketta Shiva"
artist: "Amresh Ganesh"
lyricist: "Alangudi Somu"
director: "Sai Ramani"
path: "/albums/motta-shiva-ketta-shiva-lyrics"
song: "Aadaludan Paadalai"
image: ../../images/albumart/motta-shiva-ketta-shiva.jpg
date: 2017-03-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/_Vmju744dOk"
type: "love"
singers:
  -	Shankar Mahadevan
  - Padmalatha
  - Amresh Ganesh
  - Jack Styles
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">You
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You"/>
</div>
<div class="lyrico-lyrics-wrapper">Puratichi thalaivar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puratichi thalaivar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponmana chemmal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponmana chemmal"/>
</div>
<div class="lyrico-lyrics-wrapper">Makkal thilagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makkal thilagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Doctor MGR-in
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doctor MGR-in"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudi irundha kovil song remix
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudi irundha kovil song remix"/>
</div>
<div class="lyrico-lyrics-wrapper">Dedicated to all cinema fans
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dedicated to all cinema fans"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadaludan padalai kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaludan padalai kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasipathilae thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasipathilae thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugam sugam sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam sugam sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishaa haa haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishaa haa haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa haa hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa haa hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadaludan padalai kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaludan padalai kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasipathilae thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasipathilae thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugam sugam sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam sugam sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishaa haa haa haa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishaa haa haa haa"/>
</div>
<div class="lyrico-lyrics-wrapper">Haa haa hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa haa hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai tharum paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai tharum paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam aayiram ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam aayiram ennam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum varum varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum varum varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishaa ishaa ishaa ishaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishaa ishaa ishaa ishaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishaa ishaa hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishaa ishaa hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai tharum paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai tharum paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam aayiram ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam aayiram ennam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum varum varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum varum varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadaludan aadaludan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaludan aadaludan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadaludan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadaludan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduladan padalai kaettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduladan padalai kaettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasipathilae thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasipathilae thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugam sugam sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam sugam sugam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai tharum paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai tharum paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam aayiram ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam aayiram ennam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum varum varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum varum varum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">M G R peoples politician
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="M G R peoples politician"/>
</div>
<div class="lyrico-lyrics-wrapper">Representing the city of lions
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Representing the city of lions"/>
</div>
1936 <div class="lyrico-lyrics-wrapper">to forever
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="to forever"/>
</div>
<div class="lyrico-lyrics-wrapper">Never forgotten it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Never forgotten it"/>
</div>
<div class="lyrico-lyrics-wrapper">You’re the people champion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You’re the people champion"/>
</div>
<div class="lyrico-lyrics-wrapper">The real man ever known
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The real man ever known"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannarugil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannarugil"/>
</div>
<div class="lyrico-lyrics-wrapper">Penmai kudiyera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penmai kudiyera"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai azhagil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai azhagil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilamai thadumaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilamai thadumaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thennai ilaneeril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thennai ilaneeril"/>
</div>
<div class="lyrico-lyrics-wrapper">Padhamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhamaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondru naan tharavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondru naan tharavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhamaaga aaa aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhamaaga aaa aa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Senganiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Senganiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivan pasi aara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivan pasi aara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thindra idam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thindra idam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaenin suvai oora
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaenin suvai oora"/>
</div>
<div class="lyrico-lyrics-wrapper">Pangu pera varavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pangu pera varavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunaiyaaga aa aa aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunaiyaaga aa aa aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manam oonjalin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam oonjalin"/>
</div>
<div class="lyrico-lyrics-wrapper">Meedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poo mazhai thoovida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo mazhai thoovida"/>
</div>
<div class="lyrico-lyrics-wrapper">Uriyavan neethaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uriyavan neethaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dance to the beat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dance to the beat"/>
</div>
<div class="lyrico-lyrics-wrapper">Hit the kicks to win
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hit the kicks to win"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey shake it to the rhythm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey shake it to the rhythm"/>
</div>
<div class="lyrico-lyrics-wrapper">Let the pitch count
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Let the pitch count"/>
</div>
<div class="lyrico-lyrics-wrapper">Drp like its hots and keep
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Drp like its hots and keep"/>
</div>
<div class="lyrico-lyrics-wrapper">The speaker blowing off
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The speaker blowing off"/>
</div>
<div class="lyrico-lyrics-wrapper">Rapping kudiyirundha kovil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rapping kudiyirundha kovil"/>
</div>
<div class="lyrico-lyrics-wrapper">The temple of showing love
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="The temple of showing love"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kallirukkum malarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallirukkum malarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Valaindhaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valaindhaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaippaara madiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaippaara madiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Idam podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idam podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullirukkum ninaivil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullirukkum ninaivil"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagaiyae marandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagaiyae marandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyaadu uh ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyaadu uh ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vimmi varum azhagil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vimmi varum azhagil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadaipodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadaipodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhirukkum manadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhirukkum manadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Edai podu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edai podu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vendiyathai tharalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendiyathai tharalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunivodu aa aa hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunivodu aa aa hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un paarvaiyilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un paarvaiyilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa oorvalam varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa oorvalam varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhumaiyai nee paadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhumaiyai nee paadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaduladan ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduladan ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduladan  ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduladan  ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaduladan padalai kettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaduladan padalai kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasipathilae thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasipathilae thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugam sugam sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam sugam sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishaa haa haa haa hoi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishaa haa haa haa hoi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai tharum paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai tharum paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam aayiram ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam aayiram ennam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum varum varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum varum varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishaa ishaa ishaa ishaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishaa ishaa ishaa ishaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishaa ishaa haaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishaa ishaa haaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai tharum paarvaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai tharum paarvaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellaam aayiram ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellaam aayiram ennam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum varum varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum varum varum"/>
</div>
</pre>
