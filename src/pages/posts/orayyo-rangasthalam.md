---
title: "orayyo song lyrics"
album: "Rangasthalam"
artist: "Devi Sri Prasad"
lyricist: "Chandrabose"
director: "Sukumar"
path: "/albums/rangasthalam-lyrics"
song: "Orayyo"
image: ../../images/albumart/rangasthalam.jpg
date: 2022-06-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/jlJ1Bm7CAGo"
type: "sad"
singers:
  -	Chandrabose
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Orayyooo Naa Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orayyooo Naa Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orayyoo Naa Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orayyoo Naa Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Paalu Pattaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalu Pattaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Buvva Pettaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buvva Pettaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaku Posaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaku Posaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaallu Pisikaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaallu Pisikaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Paade Moyyalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paade Moyyalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Korivi Pettaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korivi Pettaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orayyoo Naa Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orayyoo Naa Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orayyoo Naa Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orayyoo Naa Ayyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Paalu Pattaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalu Pattaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Buvva Pettaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Buvva Pettaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaku Posaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaku Posaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaallu Pisikaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaallu Pisikaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Paade Moyyalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paade Moyyalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Korivi Pettaalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korivi Pettaalaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orayyoo Naa Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orayyoo Naa Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orayyoo Naa Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orayyoo Naa Ayyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaku Daari Soopina Kaallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaku Daari Soopina Kaallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattela Paalayyenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattela Paalayyenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Bhujamu Thattina Sethulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Bhujamu Thattina Sethulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Boodidayipoyenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boodidayipoyenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Kalalu Soosina Kallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Kalalu Soosina Kallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalikamilipoyenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalikamilipoyenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mammu Melukolipina Gonthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mammu Melukolipina Gonthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaada Nidurapoyenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaada Nidurapoyenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Baadhalanodaarcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Baadhalanodaarcha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thodunde Vaadiviraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thodunde Vaadiviraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Baadhanu Oodaarcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Baadhanu Oodaarcha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvunte Baagunduraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvunte Baagunduraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orayyoo Naa Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orayyoo Naa Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orayyoo Naa Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orayyoo Naa Ayyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Dishti Theesaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dishti Theesaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Yennu Nimiraanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennu Nimiraanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Nadaka Nerpaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadaka Nerpaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Badiki Pampaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Badiki Pampaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatiki Pampaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatiki Pampaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sethi Thone 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sethi Thone "/>
</div>
<div class="lyrico-lyrics-wrapper">Mantala Kalapaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mantala Kalapaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orayyoo Naa Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orayyoo Naa Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orayyoo Naa Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orayyoo Naa Ayyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thammudi Nee Kosam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thammudi Nee Kosam "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalladillaadayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalladillaadayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Selli Gunde Neekai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Selli Gunde Neekai "/>
</div>
<div class="lyrico-lyrics-wrapper">Seruvaipoyindayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seruvaipoyindayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanchamloni Methuku 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanchamloni Methuku "/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne Ethikenayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne Ethikenayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kalladdaalu Neekai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kalladdaalu Neekai "/>
</div>
<div class="lyrico-lyrics-wrapper">Kaliyajoochenayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaliyajoochenayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvu Thodigina Sokkaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu Thodigina Sokkaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Neekai Digulu Padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekai Digulu Padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Silaka Koyyakuri 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silaka Koyyakuri "/>
</div>
<div class="lyrico-lyrics-wrapper">Pettukundi Rayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pettukundi Rayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangasthalaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangasthalaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Rangasthalaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rangasthalaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Paatra Mugisenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Paatra Mugisenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vallakaatlo Shoonya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vallakaatlo Shoonya "/>
</div>
<div class="lyrico-lyrics-wrapper">Paatra Modalayyenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatra Modalayyenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Natanaki Kaneeti 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Natanaki Kaneeti "/>
</div>
<div class="lyrico-lyrics-wrapper">Sappatlu Kurisenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappatlu Kurisenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvellotthaanantu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvellotthaanantu "/>
</div>
<div class="lyrico-lyrics-wrapper">Seppevuntaavura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seppevuntaavura"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa Paapapusevikadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa Paapapusevikadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Inapadakuntaadiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inapadakuntaadiraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orayyoo Naa Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orayyoo Naa Ayyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Orayyoo Naa Ayyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orayyoo Naa Ayyaa"/>
</div>
</pre>
