---
title: "na kallaku pattillevandi song lyrics"
album: "Induvadana"
artist: "Shiva Kakani"
lyricist: "Asirayya - Giridhar Ragolu"
director: "MSR"
path: "/albums/induvadana-lyrics"
song: "Na Kallaku Pattillevandi"
image: ../../images/albumart/induvadana.jpg
date: 2022-01-01
lang: telugu
youtubeLink: "https://www.youtube.com/embed/y1pTSuQJ1Qk"
type: "happy"
singers:
  - Sahiti
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naa kaallaki pattilevandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kaallaki pattilevandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa kannolintiki ponandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa kannolintiki ponandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa sethiki gaajulu levandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa sethiki gaajulu levandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa sellolintiki ponandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa sellolintiki ponandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa sevulaki ringulevvandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa sevulaki ringulevvandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa suttaalintiki ponandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa suttaalintiki ponandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naaku ethuseppulu levandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku ethuseppulu levandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Porugollintiki ponandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porugollintiki ponandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninna sepparaadhe gunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna sepparaadhe gunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Monna sepparaadhe gunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monna sepparaadhe gunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu kottu kaada nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu kottu kaada nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siraku padathava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siraku padathava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naaku pattu seerale levandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naaku pattu seerale levandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa jagileedintiki ponandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa jagileedintiki ponandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa yelikungaram ledhandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa yelikungaram ledhandi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkolintiki ponandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkolintiki ponandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninna sepparaadhe gunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninna sepparaadhe gunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Monna sepparaadhe gunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monna sepparaadhe gunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallu kottu kaada nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallu kottu kaada nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagaadha padathaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagaadha padathaava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maa eedhi kurrollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa eedhi kurrollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa vompusompulu soosaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa vompusompulu soosaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa nadumuna seyyetti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa nadumuna seyyetti"/>
</div>
<div class="lyrico-lyrics-wrapper">Itte sappa badipoyaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itte sappa badipoyaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maa voori karannalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa voori karannalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa buggana sukke soosaadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa buggana sukke soosaadu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa buggalu nalipesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa buggalu nalipesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Atta satikila padipoyadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atta satikila padipoyadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa yenakala maga mandha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa yenakala maga mandha "/>
</div>
<div class="lyrico-lyrics-wrapper">tiruguthu untaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tiruguthu untaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa andhaalu kaatese 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa andhaalu kaatese "/>
</div>
<div class="lyrico-lyrics-wrapper">monagaade ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="monagaade ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ittantolni velallo 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ittantolni velallo "/>
</div>
<div class="lyrico-lyrics-wrapper">soosanu mi dhaggara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soosanu mi dhaggara"/>
</div>
<div class="lyrico-lyrics-wrapper">Emundhile kothaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emundhile kothaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bottu billai istha pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bottu billai istha pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattigaajulista pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattigaajulista pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattilattukostaa pila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattilattukostaa pila"/>
</div>
<div class="lyrico-lyrics-wrapper">Naathoti vasthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naathoti vasthava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mukkupudakalista pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mukkupudakalista pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Etthu seppulistaa pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etthu seppulistaa pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattu seeralistaa pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattu seeralistaa pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naakoti isthaava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakoti isthaava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa vonti meedha rangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa vonti meedha rangu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kallu kundda pongu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kallu kundda pongu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa etthu pallamekki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa etthu pallamekki"/>
</div>
<div class="lyrico-lyrics-wrapper">Laaginchu letha bhangu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laaginchu letha bhangu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa nadumu kinda vompu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa nadumu kinda vompu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pedhavi kunna merupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pedhavi kunna merupu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adigindhi thecchi petti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adigindhi thecchi petti"/>
</div>
<div class="lyrico-lyrics-wrapper">Tippesukora soopu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tippesukora soopu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neekundanta raasiste 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neekundanta raasiste "/>
</div>
<div class="lyrico-lyrics-wrapper">swargame needhata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="swargame needhata"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa paitenaka paruvalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa paitenaka paruvalu "/>
</div>
<div class="lyrico-lyrics-wrapper">sonthame neekanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonthame neekanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkevaraina unnaara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkevaraina unnaara"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee poota naa kompaki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee poota naa kompaki "/>
</div>
<div class="lyrico-lyrics-wrapper">ochedhi aa vindhuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ochedhi aa vindhuku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mari ninna seppaledhe gunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mari ninna seppaledhe gunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Monna seppaledhe gunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Monna seppaledhe gunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeti kaada yekaraala thota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeti kaada yekaraala thota"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni peru raasestha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ni peru raasestha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naakundhanta neeke gunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naakundhanta neeke gunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Alakamaani raave gunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alakamaani raave gunta"/>
</div>
<div class="lyrico-lyrics-wrapper">Pampu sheddu kaada neetho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pampu sheddu kaada neetho"/>
</div>
<div class="lyrico-lyrics-wrapper">Panundhi vasthava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panundhi vasthava"/>
</div>
</pre>
