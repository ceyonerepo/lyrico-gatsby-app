---
title: "ey inga paaru song lyrics"
album: "Vellaiilla Pattadhari"
artist: "Anirudh Ravichander"
lyricist: "Dhanush"
director: "Velraj"
path: "/albums/vellaiilla-pattadhari-lyrics"
song: "Ey Inga Paaru"
image: ../../images/albumart/vellaiilla-pattadhari.jpg
date: 2014-02-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/svHICCAeZ9I"
type: "Love"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ey Inga Paaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Inga Paaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Koothu Joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothu Joru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Comedy Yaaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Comedy Yaaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Namma Sir ru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Namma Sir ru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Molaaga Inikkuma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molaaga Inikkuma "/>
</div>
<div class="lyrico-lyrics-wrapper">Vellam Kasakkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellam Kasakkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaakkaa Muttaiyil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaakkaa Muttaiyil "/>
</div>
<div class="lyrico-lyrics-wrapper">Mayiluthaan Porakkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayiluthaan Porakkumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Inga Paaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Inga Paaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Koothu Joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothu Joru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Romansu Yaaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romansu Yaaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Namma Sir ru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Namma Sir ru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kazhutha Kanaikkuma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhutha Kanaikkuma "/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthirai Koraikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthirai Koraikkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Othavaa Karaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Othavaa Karaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Poochedi Pookkumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poochedi Pookkumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Inga Paaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Inga Paaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Koothu Joru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothu Joru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hero Yaaru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hero Yaaru "/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Namma Sir ru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Namma Sir ru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Korangu Parakkuma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korangu Parakkuma "/>
</div>
<div class="lyrico-lyrics-wrapper">Meenu Nadakkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenu Nadakkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adada Raghuvara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Raghuvara "/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkithu Kedaikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkithu Kedaikkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Molaaga Inikkuma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Molaaga Inikkuma "/>
</div>
<div class="lyrico-lyrics-wrapper">Vellam Kasakkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellam Kasakkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kazhutha Kanaikkuma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhutha Kanaikkuma "/>
</div>
<div class="lyrico-lyrics-wrapper">Kuthirai Koraikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuthirai Koraikkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Korangu Parakkuma 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korangu Parakkuma "/>
</div>
<div class="lyrico-lyrics-wrapper">Meenu Nadakkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenu Nadakkuma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adada Raghuvara 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adada Raghuvara "/>
</div>
<div class="lyrico-lyrics-wrapper">Unakkithu Kedaikkuma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkithu Kedaikkuma"/>
</div>
</pre>
