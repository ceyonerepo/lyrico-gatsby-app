---
title: "omg ponnu song lyrics"
album: "Sarkar"
artist: "AR Rahman"
lyricist: "Vivek"
director: "AR Murugadoss"
path: "/albums/sarkar-lyrics"
song: "OMG Ponnu"
image: ../../images/albumart/sarkar.jpg
date: 2018-11-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/kBc4CvJKpEE"
type: "love"
singers:
  - Sid Sriram
  - Jonita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oh oo oo kangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oo oo kangala"/>
</div>
<div class="lyrico-lyrics-wrapper">GR8 kangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="GR8 kangala"/>
</div>
<div class="lyrico-lyrics-wrapper">AKA vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AKA vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sil silaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil silaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh thendrala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh thendrala"/>
</div>
<div class="lyrico-lyrics-wrapper">XOX thendrala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="XOX thendrala"/>
</div>
<div class="lyrico-lyrics-wrapper">L8R minnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="L8R minnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Cindrallaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cindrallaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">OMG ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OMG ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">ILY kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ILY kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">ASAP kooda va nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ASAP kooda va nee"/>
</div>
<div class="lyrico-lyrics-wrapper">BAEneeemaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BAEneeemaa"/>
</div>
<div class="lyrico-lyrics-wrapper">BFF naan maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BFF naan maa"/>
</div>
<div class="lyrico-lyrics-wrapper">ROFL pannalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ROFL pannalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">IMO nininee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IMO nininee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaiyiram ponna paarpa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaiyiram ponna paarpa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">IDK ennanna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IDK ennanna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Evloo pudikkum sollu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evloo pudikkum sollu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">IMO ne mattum thaan dii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IMO ne mattum thaan dii"/>
</div>
<div class="lyrico-lyrics-wrapper">Perazhagi en pondaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perazhagi en pondaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogatha 1mm eppothum nee KIT
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogatha 1mm eppothum nee KIT"/>
</div>
<div class="lyrico-lyrics-wrapper">IDK nee ellana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IDK nee ellana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eppadi vazhven 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eppadi vazhven "/>
</div>
<div class="lyrico-lyrics-wrapper">dii oh ho oh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dii oh ho oh ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oo oo kangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oo oo kangala"/>
</div>
<div class="lyrico-lyrics-wrapper">GR8 kangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="GR8 kangala"/>
</div>
<div class="lyrico-lyrics-wrapper">AKA vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AKA vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sil silaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil silaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh thendrala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh thendrala"/>
</div>
<div class="lyrico-lyrics-wrapper">XOX thendrala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="XOX thendrala"/>
</div>
<div class="lyrico-lyrics-wrapper">L8R minnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="L8R minnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Cindrallaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cindrallaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">OMG ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OMG ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">ILY kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ILY kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">ASAP kooda va nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ASAP kooda va nee"/>
</div>
<div class="lyrico-lyrics-wrapper">BAEneeemaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BAEneeemaa"/>
</div>
<div class="lyrico-lyrics-wrapper">BFF naan maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BFF naan maa"/>
</div>
<div class="lyrico-lyrics-wrapper">ROFL pannalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ROFL pannalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">IMO nininee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IMO nininee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaiyiram ponna paarpa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaiyiram ponna paarpa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">IDK ennanna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IDK ennanna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Evloo pudikkum sollu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evloo pudikkum sollu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">IMO ne mattum thaan dii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IMO ne mattum thaan dii"/>
</div>
<div class="lyrico-lyrics-wrapper">Perazhagi en pondaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perazhagi en pondaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogatha 1mm eppothum nee KIT
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogatha 1mm eppothum nee KIT"/>
</div>
<div class="lyrico-lyrics-wrapper">IDK nee ellana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IDK nee ellana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eppadi vazhven 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eppadi vazhven "/>
</div>
<div class="lyrico-lyrics-wrapper">dii oh ho oh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dii oh ho oh ho"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh oo oo kangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oo oo kangala"/>
</div>
<div class="lyrico-lyrics-wrapper">GR8 kangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="GR8 kangala"/>
</div>
<div class="lyrico-lyrics-wrapper">AKA vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AKA vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sil silaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil silaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Face ah paartha GM aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Face ah paartha GM aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjula saanja GNSD
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjula saanja GNSD"/>
</div>
<div class="lyrico-lyrics-wrapper">Chumchukum chumchukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chumchukum chumchukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Cellu ellaam LOLU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cellu ellaam LOLU"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennoda SH neeyae thaan dii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennoda SH neeyae thaan dii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chumchukum chumchukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chumchukum chumchukum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan out of town na appappo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan out of town na appappo"/>
</div>
<div class="lyrico-lyrics-wrapper">HRU venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="HRU venum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna daily 5 times
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna daily 5 times"/>
</div>
<div class="lyrico-lyrics-wrapper">Kissu missu smiley varanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kissu missu smiley varanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna meet pannina day athu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna meet pannina day athu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellamae en HBD aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellamae en HBD aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna 24×7 konjivitalae SSOU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna 24×7 konjivitalae SSOU"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yemma yemma yemaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemma yemma yemaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yenna worku bhramma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yenna worku bhramma"/>
</div>
<div class="lyrico-lyrics-wrapper">TY sollatumaa CG ne maa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TY sollatumaa CG ne maa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemma yemma yemaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemma yemma yemaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna pathirama TC pannatumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna pathirama TC pannatumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Seralaamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seralaamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">OMG ponnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OMG ponnu"/>
</div>
<div class="lyrico-lyrics-wrapper">ILY kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ILY kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">ASAP kooda varuvenn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ASAP kooda varuvenn"/>
</div>
<div class="lyrico-lyrics-wrapper">BAEneethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BAEneethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">BFF naan thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BFF naan thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">ROFL pannalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ROFL pannalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">IMO nininee nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IMO nininee nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaiyiram ponna paarpa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaiyiram ponna paarpa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">IDK ennanna enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IDK ennanna enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Evloo pudikkum sollu nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evloo pudikkum sollu nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">IMO ne mattum thaan dii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IMO ne mattum thaan dii"/>
</div>
<div class="lyrico-lyrics-wrapper">Perazhagi en pondaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perazhagi en pondaati"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogatha 1mm eppothum nee KIT
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogatha 1mm eppothum nee KIT"/>
</div>
<div class="lyrico-lyrics-wrapper">IDK nee ellana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IDK nee ellana"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan eppadi vazhven 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan eppadi vazhven "/>
</div>
<div class="lyrico-lyrics-wrapper">dii oh ho oh ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dii oh ho oh ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oo oo kangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oo oo kangala"/>
</div>
<div class="lyrico-lyrics-wrapper">GR8 kangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="GR8 kangala"/>
</div>
<div class="lyrico-lyrics-wrapper">AKA vennila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="AKA vennila"/>
</div>
<div class="lyrico-lyrics-wrapper">Sil silaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sil silaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh thendrala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh thendrala"/>
</div>
<div class="lyrico-lyrics-wrapper">XOX thendrala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="XOX thendrala"/>
</div>
<div class="lyrico-lyrics-wrapper">L8R minnala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="L8R minnala"/>
</div>
<div class="lyrico-lyrics-wrapper">Cindralla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cindralla"/>
</div>
</pre>
