---
title: "naan naan song lyrics"
album: "Mahaan"
artist: "Santhosh Narayanan"
lyricist: "Vivek"
director: "Karthik Subbaraj"
path: "/albums/mahaan-lyrics"
song: "Naan Naan"
image: ../../images/albumart/mahaan.jpg
date: 2022-02-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/S3oq6IwPWBw"
type: "happy"
singers:
  - Santhosh Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan naan ezhuvadhu nadandhe theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan ezhuvadhu nadandhe theerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal vara en mughazh nighazhindhe theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal vara en mughazh nighazhindhe theerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu varai porupadhu manandhin veeram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu varai porupadhu manandhin veeram"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaigal paranpathil vizhupadhum serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaigal paranpathil vizhupadhum serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nirandharam maanavan vilagi sendraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nirandharam maanavan vilagi sendraal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbiduvaan ena ariyaa sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbiduvaan ena ariyaa sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyvu mudindhadhum thirumbi vandhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyvu mudindhadhum thirumbi vandhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Arasanukke indha ariyaasanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arasanukke indha ariyaasanam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan naan ezhuvadhu nadandhe theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan naan ezhuvadhu nadandhe theerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naal vara en pugazh nigazhndhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naal vara en pugazh nigazhndhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Parapadhil mudhal padi vizhuvadhu dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parapadhil mudhal padi vizhuvadhu dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Silar vizhavadhe tharayinai idithidathaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silar vizhavadhe tharayinai idithidathaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sirithe vaazhndhavan karathai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirithe vaazhndhavan karathai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam than muthathai podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam than muthathai podum"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhithe vaazhndhavan karathai thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhithe vaazhndhavan karathai thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam than yuthathai podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam than yuthathai podum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ranam nooraaga marodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranam nooraaga marodu"/>
</div>
<div class="lyrico-lyrics-wrapper">vaal keerinaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaal keerinaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevan poraada poraano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevan poraada poraano"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan per dhaan nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan per dhaan nikkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parapadhil mudhal padi vizhuvadhu dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parapadhil mudhal padi vizhuvadhu dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Silar vizhavadhe boomiyai idithidathaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silar vizhavadhe boomiyai idithidathaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Muyal vara kaathidum kazhugugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyal vara kaathidum kazhugugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaan idhu muyal ala 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaan idhu muyal ala "/>
</div>
<div class="lyrico-lyrics-wrapper">puyal adhan siragodippene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puyal adhan siragodippene"/>
</div>
</pre>
