---
title: "seeti maar seeti maar song lyrics"
album: "Radhe"
artist: "Devi Sri Prasad"
lyricist: "Shabbir Ahmed"
director: "Prabhu Deva"
path: "/albums/radhe-lyrics"
song: "Seeti Maar Seeti Maar"
image: ../../images/albumart/radhe.jpg
date: 2021-05-13
lang: hindi
youtubeLink: "https://www.youtube.com/embed/mvgRsyQYWMo"
type: "Entertainment"
singers:
  - Kamaal Khan
  - Iulia Vantur
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Seeti maar, seeti maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeti maar, seeti maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeti maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeti maar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nach le, nach le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nach le, nach le"/>
</div>
<div class="lyrico-lyrics-wrapper">Armaan machle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Armaan machle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are dede dede de dede dede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are dede dede de dede dede"/>
</div>
<div class="lyrico-lyrics-wrapper">Dede dede mujhe pyaar de de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dede dede mujhe pyaar de de"/>
</div>
<div class="lyrico-lyrics-wrapper">Are dede dede de dede dede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are dede dede de dede dede"/>
</div>
<div class="lyrico-lyrics-wrapper">Dede dede baby pyaar de de
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dede dede baby pyaar de de"/>
</div>
<div class="lyrico-lyrics-wrapper">Are dede dede de dede dede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are dede dede de dede dede"/>
</div>
<div class="lyrico-lyrics-wrapper">Ya hum tujhse le le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ya hum tujhse le le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seeti maar seeti maar seeti maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeti maar seeti maar seeti maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeti maar seeti maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeti maar seeti maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jhappi maar jhappi maar jhappi maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jhappi maar jhappi maar jhappi maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhappi maar jhappi maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhappi maar jhappi maar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Disco aayi dance karne
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Disco aayi dance karne"/>
</div>
<div class="lyrico-lyrics-wrapper">Get onto the dance floor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get onto the dance floor"/>
</div>
<div class="lyrico-lyrics-wrapper">Volume thoda loud karke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Volume thoda loud karke"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachenge some more
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachenge some more"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Just dance with me ek baar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Just dance with me ek baar"/>
</div>
<div class="lyrico-lyrics-wrapper">Phir tu bolega once more
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phir tu bolega once more"/>
</div>
<div class="lyrico-lyrics-wrapper">Romance is just in the air
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romance is just in the air"/>
</div>
<div class="lyrico-lyrics-wrapper">So thoda sa come close
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So thoda sa come close"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lele lele le lele baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lele lele le lele baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Lele baby mera pyaar le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lele baby mera pyaar le"/>
</div>
<div class="lyrico-lyrics-wrapper">Lele lele le lele baby
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lele lele le lele baby"/>
</div>
<div class="lyrico-lyrics-wrapper">Le de le de le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le de le de le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seeti maar seeti maar seeti maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeti maar seeti maar seeti maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeti maar seeti maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeti maar seeti maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jhappi maar jhappi maar jhappi maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jhappi maar jhappi maar jhappi maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhappi maar jhappi maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhappi maar jhappi maar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baby teri aankh mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baby teri aankh mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Mujhko kar le frame tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mujhko kar le frame tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Groove karein hum sath mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Groove karein hum sath mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Main hoon tera man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main hoon tera man"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You never ending raat mein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You never ending raat mein"/>
</div>
<div class="lyrico-lyrics-wrapper">Karta hai naughty baat tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karta hai naughty baat tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Itna cute flirt tu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Itna cute flirt tu"/>
</div>
<div class="lyrico-lyrics-wrapper">Main ho gayi teri fan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main ho gayi teri fan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lele lele le lele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lele lele le lele"/>
</div>
<div class="lyrico-lyrics-wrapper">Lele baby mera pyaar le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lele baby mera pyaar le"/>
</div>
<div class="lyrico-lyrics-wrapper">Lele lele le lele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lele lele le lele"/>
</div>
<div class="lyrico-lyrics-wrapper">Le de le de le
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Le de le de le"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seeti maar, seeti maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeti maar, seeti maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeti maar, seeti maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeti maar, seeti maar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seeti maar seeti maar seeti maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeti maar seeti maar seeti maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Seeti maar seeti maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeti maar seeti maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho jhappi maar jhappi maar jhappi maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho jhappi maar jhappi maar jhappi maar"/>
</div>
<div class="lyrico-lyrics-wrapper">Jhappi maar jhappi maar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jhappi maar jhappi maar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seeti maar!
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seeti maar!"/>
</div>
</pre>
