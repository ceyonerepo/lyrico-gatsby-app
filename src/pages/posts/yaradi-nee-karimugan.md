---
title: "yaradi nee song lyrics"
album: "Karimugan"
artist: "Chella Thangaiah"
lyricist: "unknown"
director: "Chella Thangaiah"
path: "/albums/karimugan-song-lyrics"
song: "Yaradi Nee"
image: ../../images/albumart/karimugan.jpg
date: 2018-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Q_xWvf0M5Ps"
type: "love"
singers:
  - Jegathish
  - linsi pransis
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yaaradi nee imbuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaradi nee imbuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalaa engu irunthaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalaa engu irunthaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaradi nee imbuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaradi nee imbuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalaa engu irunthaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalaa engu irunthaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan paakaiyila nee paakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan paakaiyila nee paakala"/>
</div>
<div class="lyrico-lyrics-wrapper">nee paakaiyila naan paakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee paakaiyila naan paakala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adiyei adiyei adiyei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiyei adiyei adiyei"/>
</div>
<div class="lyrico-lyrics-wrapper">azhaga azhaga azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaga azhaga azhaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yaaradi nee imbuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaradi nee imbuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalaa engu irunthaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalaa engu irunthaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">yaaradi nee imbuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yaaradi nee imbuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">naalaa engu irunthaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalaa engu irunthaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mugam kaatinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugam kaatinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">sugam kootinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugam kootinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai ketkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai ketkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">ithayathil nulainthaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithayathil nulainthaayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru paathi naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru paathi naan"/>
</div>
<div class="lyrico-lyrics-wrapper">maru paathi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru paathi nee"/>
</div>
<div class="lyrico-lyrics-wrapper">ena isai paadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena isai paadum"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayathil kalanthaayadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayathil kalanthaayadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee thaane ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaane ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">kalai aakinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalai aakinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan ninaivaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan ninaivaale"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai silai aakinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai silai aakinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">uyiraai irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyiraai irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaiye ninaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaiye ninaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaadhalane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaadhalane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaathali nee imbuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathali nee imbuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">naala engu irunthaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naala engu irunthaayo"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaathali nee imbuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaathali nee imbuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">naala engu irunthaayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naala engu irunthaayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vinmegam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vinmegam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">vidivelli naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vidivelli naan"/>
</div>
<div class="lyrico-lyrics-wrapper">antha isai raagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha isai raagam"/>
</div>
<div class="lyrico-lyrics-wrapper">namakulle karuvaanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namakulle karuvaanathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un thegam naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thegam naan"/>
</div>
<div class="lyrico-lyrics-wrapper">en thegam nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thegam nee"/>
</div>
<div class="lyrico-lyrics-wrapper">intha ulagathail nam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha ulagathail nam "/>
</div>
<div class="lyrico-lyrics-wrapper">kaathal azhagaanathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathal azhagaanathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee thaane ennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaane ennai"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir aakinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir aakinaai"/>
</div>
<div class="lyrico-lyrics-wrapper">unthan ninaivodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan ninaivodu"/>
</div>
<div class="lyrico-lyrics-wrapper">vazha thuyar pokinaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazha thuyar pokinaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">uyiraai irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyiraai irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">unaiye ninaipen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaiye ninaipen"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaathaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaathaliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaathala nee imbuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathala nee imbuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">naala engu irunthayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naala engu irunthayo"/>
</div>
<div class="lyrico-lyrics-wrapper">en kaathala nee imbuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en kaathala nee imbuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">naala engu irunthayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naala engu irunthayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naan paakaiyila nee paakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan paakaiyila nee paakala"/>
</div>
<div class="lyrico-lyrics-wrapper">nee paakaiyila naan paakala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee paakaiyila naan paakala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adiyei adiyei adiyei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adiyei adiyei adiyei"/>
</div>
<div class="lyrico-lyrics-wrapper">azhaga azhaga azhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhaga azhaga azhaga"/>
</div>
</pre>