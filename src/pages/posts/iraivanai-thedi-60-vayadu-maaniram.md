---
title: "iraivanai thedi song lyrics"
album: "60 Vayadu Maaniram"
artist: "Ilaiyaraaja"
lyricist: "Pa Vijay"
director: "Radha Mohan"
path: "/albums/60-vayadu-maaniram-lyrics"
song: "Iraivanai Thedi"
image: ../../images/albumart/60-vayadu-maaniram.jpg
date: 2018-08-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/q0uiwb71B3k"
type: "melody"
singers:
  - Ilaiyaraaja
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Iraivanai thedum ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivanai thedum ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivaroo manithanai thedugiraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivaroo manithanai thedugiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Iraivanai thedum ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivanai thedum ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivaroo manithanai thedugiraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivaroo manithanai thedugiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhu iruppavan thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhu iruppavan thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhan avanae thedugiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhan avanae thedugiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha ulagathil avan kedaipaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ulagathil avan kedaipaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha ulagathilae irupaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha ulagathilae irupaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha ulagathil avan kedaipaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ulagathil avan kedaipaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha ulagathilae irupaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha ulagathilae irupaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanai theduthal polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanai theduthal polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanae thannaiyae thedugiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanae thannaiyae thedugiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaiyae thedi thedi ivargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaiyae thedi thedi ivargal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanai maranthuvitaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanai maranthuvitaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraivanai thedum ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivanai thedum ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivaroo manithanai thedugiraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivaroo manithanai thedugiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhu iruppavan thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhu iruppavan thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhan avanae thedugiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhan avanae thedugiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnum maniyum pooti veithavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnum maniyum pooti veithavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum kodi theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum kodi theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi kodi kuvithu veithavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi kodi kuvithu veithavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottai kodiyai theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottai kodiyai theda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aasai theera aadi paarthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasai theera aadi paarthavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adutha uyaram theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha uyaram theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Utcham thannai thottu nirpavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utcham thannai thottu nirpavan"/>
</div>
<div class="lyrico-lyrics-wrapper">Utcha pugalai theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Utcha pugalai theda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thedalae intha vaazhkaiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thedalae intha vaazhkaiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">Thevaiyaai aanathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thevaiyaai aanathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aayulin moththa kaalamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aayulin moththa kaalamum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thediyae ponathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thediyae ponathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andha iraivan iraivan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha iraivan iraivan"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomikku vandhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomikku vandhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam ondru thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam ondru thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu kedaikka villai podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu kedaikka villai podi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraivanai thedum ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivanai thedum ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivaroo manithanai thedugiraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivaroo manithanai thedugiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhu iruppavan thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhu iruppavan thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhan avanae thedugiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhan avanae thedugiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaattam indri kaatrai polavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaattam indri kaatrai polavae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhum manidham engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhum manidham engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aattam pottu kootam kootiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aattam pottu kootam kootiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aazhum manidhan ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhum manidhan ingae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Konjam manidham nenjil vaithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam manidham nenjil vaithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaan minjum manidhan engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaan minjum manidhan engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam engum vanjam serthu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam engum vanjam serthu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Enjum manidhan ingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enjum manidhan ingae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaarumae illai endruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumae illai endruthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarumae illaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarumae illaiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Karuvilae thondrum uyirellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karuvilae thondrum uyirellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadavulin pillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadavulin pillaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada manidha manidha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada manidha manidha"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaichalai olithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaichalai olithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Amaidhi ondru thedu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amaidhi ondru thedu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu kadavul varaintha kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu kadavul varaintha kodu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraivanai thedum ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivanai thedum ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivaroo manithanai thedugiraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivaroo manithanai thedugiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Manadhu iruppavan thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manadhu iruppavan thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Manidhan avanae thedugiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manidhan avanae thedugiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Intha ulagathil avan kedaipaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ulagathil avan kedaipaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha ulagathilae irupaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha ulagathilae irupaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha ulagathil avan kedaipaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ulagathil avan kedaipaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha ulagathilae irupaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha ulagathilae irupaano"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanai theduthal polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanai theduthal polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivanae thannaiyae thedugiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanae thannaiyae thedugiraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaiyae thedi thedi ivargal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaiyae thedi thedi ivargal"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanai maranthuvitaar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanai maranthuvitaar"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iraivanai thedum ulagathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iraivanai thedum ulagathil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivaroo manithanai thedugiraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivaroo manithanai thedugiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithanai thedugiraar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithanai thedugiraar"/>
</div>
<div class="lyrico-lyrics-wrapper">Manithanai thedugiraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manithanai thedugiraa"/>
</div>
</pre>
