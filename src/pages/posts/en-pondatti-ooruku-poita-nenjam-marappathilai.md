---
title: "en pondatti ooruku poita song lyrics"
album: "Nenjam Marappathilai"
artist: "Yuvan Shankar Raja"
lyricist: "Selvaraghavan"
director: "Selvaraghavan"
path: "/albums/nenjam-marappathilai-song-lyrics"
song: "En Pondatti Ooruku Poita"
image: ../../images/albumart/nenjam-marappathilai.jpg
date: 2021-03-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Qdy7MHeDsm4"
type: "Sad"
singers:
  - Yuvan Shankar Raja
---

<pre class="lyrics-native">
</pre>
<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">En Pondati Ooruku Poita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pondati Ooruku Poita"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pondati Ooruku Poita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pondati Ooruku Poita"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pondati Ooruku Poita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pondati Ooruku Poita"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pondati Ooruku Poita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pondati Ooruku Poita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Theriyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Theriyudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhu Kekkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhu Kekkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Puriyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Puriyudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Theriyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Theriyudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhu Kekkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhu Kekkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Puriyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Puriyudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tyre puncture aaguma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tyre puncture aaguma"/>
</div>
<div class="lyrico-lyrics-wrapper">Id car ah marandhutana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Id car ah marandhutana"/>
</div>
<div class="lyrico-lyrics-wrapper">Boarding card ah marandhutana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boarding card ah marandhutana"/>
</div>
<div class="lyrico-lyrics-wrapper">Flight delay or cancel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Flight delay or cancel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pondati ooruku poitanu kummalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pondati ooruku poitanu kummalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettuku poi na calling bell adicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettuku poi na calling bell adicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Moodevi kadhava tharapalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moodevi kadhava tharapalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
“<div class="lyrico-lyrics-wrapper">Hey baby you reached”
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey baby you reached”"/>
</div>
<div class="lyrico-lyrics-wrapper">All well, Love you too, Bye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All well, Love you too, Bye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Pondati Ooruku Poita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pondati Ooruku Poita"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pondati Ooruku Poita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pondati Ooruku Poita"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pondati Ooruku Poita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pondati Ooruku Poita"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pondati Ooruku Poita
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pondati Ooruku Poita"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannu Theriyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu Theriyudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhu Kekkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhu Kekkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagam Puriyudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagam Puriyudhu"/>
</div>
</pre>
