---
title: "celebrity ayyaadu song lyrics"
album: "ABCD"
artist: "Judah Sandhy"
lyricist: "Bhaskarabhatla"
director: "Sanjeev Reddy"
path: "/albums/abcd-lyrics"
song: "Celebrity Ayyaadu"
image: ../../images/albumart/abcd.jpg
date: 2019-05-17
lang: telugu
youtubeLink: "https://www.youtube.com/embed/0M9NfPiz2GI"
type: "happy"
singers:
  - Rahul Sipligunj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Celebrity ayyade over nightlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Celebrity ayyade over nightlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Celebration modhalaindhe veedi lifelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Celebration modhalaindhe veedi lifelo"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhu kottesi mid night lona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhu kottesi mid night lona"/>
</div>
<div class="lyrico-lyrics-wrapper">Nakka thokedho thokkundade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nakka thokedho thokkundade"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhu janmalo bhakthekkuvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhu janmalo bhakthekkuvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rama kotedho raasuntade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rama kotedho raasuntade"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Celebrity ayyade over nightlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Celebrity ayyade over nightlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Celebration modhalaindhe veedi lifelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Celebration modhalaindhe veedi lifelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye miyya miyya mama miyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye miyya miyya mama miyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichekkali motham mediaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichekkali motham mediaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye miyya miyya mama miyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye miyya miyya mama miyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedekkali wikipidiaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedekkali wikipidiaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anukokunda are adrustam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anukokunda are adrustam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arachethullo paakindhe jarrigoddula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arachethullo paakindhe jarrigoddula"/>
</div>
<div class="lyrico-lyrics-wrapper">Are numisham lo nee aanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are numisham lo nee aanandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa meghallo yegirindhi thaarajuvvala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa meghallo yegirindhi thaarajuvvala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Are mana kosame kanipettare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Are mana kosame kanipettare"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee whatsup face book twitter lani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee whatsup face book twitter lani"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana life lne maarchesthaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana life lne maarchesthaye"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa youtube ticktok instraalanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa youtube ticktok instraalanni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Celebrity ayyade over nightlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Celebrity ayyade over nightlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Celebration modhallaindhe veedi lifelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Celebration modhallaindhe veedi lifelo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye miyya miyya mama miyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye miyya miyya mama miyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichekkali motham mediaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichekkali motham mediaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye miyya miyya mama miyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye miyya miyya mama miyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedekkali wikipidiaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedekkali wikipidiaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvu nee panulu nachayi ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvu nee panulu nachayi ante"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethi mostharu ratings tho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethi mostharu ratings tho"/>
</div>
<div class="lyrico-lyrics-wrapper">Time ivvara thindanikkuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time ivvara thindanikkuda"/>
</div>
<div class="lyrico-lyrics-wrapper">Tightu schedulu selfeelatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tightu schedulu selfeelatho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee name antha ee fame antha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee name antha ee fame antha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ika anthantha anthantha penchukovale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ika anthantha anthantha penchukovale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee craze antha ee janamantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee craze antha ee janamantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee chuttura vunnappude shakkabettale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee chuttura vunnappude shakkabettale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye miyya miyya mama miyya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye miyya miyya mama miyya"/>
</div>
<div class="lyrico-lyrics-wrapper">Pichekkali motham media
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pichekkali motham media"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye miyya miyya mama miyyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye miyya miyya mama miyyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedekkali wikipidiaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedekkali wikipidiaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Celebrity ayyade over nightlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Celebrity ayyade over nightlo"/>
</div>
<div class="lyrico-lyrics-wrapper">Celebration modhallaindhe veedi lifelo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Celebration modhallaindhe veedi lifelo"/>
</div>
</pre>
