---
title: 'kaadhal theevey song lyrics'
album: 'Dharala Prabhu'
artist: 'Sean Roldan'
lyricist: 'Nixy'
director: 'Krishna Marimuthu'
path: '/albums/dharala-prabhu-song-lyrics'
song: 'Kaadhal Theevey'
image: ../../images/albumart/dharala-prabhu.jpg
date: 2020-03-13
lang: tamil
singers: 
- Sid Sriram
youtubeLink: 'https://www.youtube.com/embed/NGkEwWewgmY'
type: 'love'
---

<pre class="lyrics-native">
    Coming Soon
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">Ennodu vaazhvaaiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennodu vaazhvaaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu servaaiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirodu servaaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaigal sernthaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kaigal sernthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaazhuven uru maaruven
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan vaazhuven uru maaruven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennodu vaazhvaaiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennodu vaazhvaaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu servaaiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirodu servaaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaigal sernthaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kaigal sernthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaazhuven maaruven
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan vaazhuven maaruven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaadhal theevae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal theevae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillaaiyodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Nillaaiyodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal seiya vanthenedi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal seiya vanthenedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai paarthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannai paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolladha di
<input type="checkbox" class="lyrico-select-lyric-line" value="Kolladha di"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannai paarkka maranthenadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannai paarkka maranthenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idhu oru vidha porattam
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu oru vidha porattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil oru pudhu vidha maatram
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhayathil oru pudhu vidha maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukkalum un per sollum
<input type="checkbox" class="lyrico-select-lyric-line" value="Anukkalum un per sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayam ennadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Maayam ennadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pani kanavugal naal thorum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pani kanavugal naal thorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani iravugal nadanthal podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thani iravugal nadanthal podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirukkul dhinam aarpaattam
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirukkul dhinam aarpaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyayam ennadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Nyayam ennadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Thannan theevaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Thannan theevaai"/>
</div>
  <div class="lyrico-lyrics-wrapper">Theevaai
<input type="checkbox" class="lyrico-select-lyric-line" value="Theevaai"/>
</div>
  <div class="lyrico-lyrics-wrapper">Pogathadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogathadi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Pogathadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Pogathadi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Thanjam kolla vanthenadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Thanjam kolla vanthenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Konjam vaarthai
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjam vaarthai"/>
</div>
  <div class="lyrico-lyrics-wrapper">Vaarthai
<input type="checkbox" class="lyrico-select-lyric-line" value="Vaarthai"/>
</div>
  <div class="lyrico-lyrics-wrapper">Marathenadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Marathenadi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Marathenadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Marathenadi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Konjum pechil vizhunthenadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Konjum pechil vizhunthenadi"/>
</div>
  <div class="lyrico-lyrics-wrapper">Vizhunthenadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Vizhunthenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Sarigamapa nisa riga ri
<input type="checkbox" class="lyrico-select-lyric-line" value="Sarigamapa nisa riga ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni pa ma ga ri ga ri
<input type="checkbox" class="lyrico-select-lyric-line" value="Ni pa ma ga ri ga ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarigamapa nisa riga ri
<input type="checkbox" class="lyrico-select-lyric-line" value="Sarigamapa nisa riga ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ni pa ma ga ri ga ri
<input type="checkbox" class="lyrico-select-lyric-line" value="Ni pa ma ga ri ga ri"/>
</div>
<div class="lyrico-lyrics-wrapper">Ri ga rii
<input type="checkbox" class="lyrico-select-lyric-line" value="Ri ga rii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Poi unmai
<input type="checkbox" class="lyrico-select-lyric-line" value="Poi unmai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rendum solla thayakkam illai
<input type="checkbox" class="lyrico-select-lyric-line" value="Rendum solla thayakkam illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi nee sirithaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Adi nee sirithaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhil menmaiyae unmaiyae
<input type="checkbox" class="lyrico-select-lyric-line" value="Adhil menmaiyae unmaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pul veliyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Pul veliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru thuligazhaaga pirinthom
<input type="checkbox" class="lyrico-select-lyric-line" value="Iru thuligazhaaga pirinthom"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru kaveriyil
<input type="checkbox" class="lyrico-select-lyric-line" value="Indru kaveriyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam odinom koodinom
<input type="checkbox" class="lyrico-select-lyric-line" value="Naam odinom koodinom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Oodal edhum illaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Oodal edhum illaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal engum illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal engum illaiyadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuraigal edhum illaadha
<input type="checkbox" class="lyrico-select-lyric-line" value="Kuraigal edhum illaadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Endha uravilum nizhai illaiyadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Endha uravilum nizhai illaiyadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Idhu oru vidha porattam
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhu oru vidha porattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayathil oru pudhu vidha maatram
<input type="checkbox" class="lyrico-select-lyric-line" value="Idhayathil oru pudhu vidha maatram"/>
</div>
<div class="lyrico-lyrics-wrapper">Anukkalum un per sollum
<input type="checkbox" class="lyrico-select-lyric-line" value="Anukkalum un per sollum"/>
</div>
<div class="lyrico-lyrics-wrapper">Maayam ennadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Maayam ennadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pani kanavugal naal thorum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pani kanavugal naal thorum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thani iravugal nadanthal podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Thani iravugal nadanthal podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirukkul dhinam aarpaattam
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirukkul dhinam aarpaattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nyayam ennadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Nyayam ennadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Kaadhal theevae
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal theevae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nillaaiyodi
<input type="checkbox" class="lyrico-select-lyric-line" value="Nillaaiyodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal seiya vanthenedi
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal seiya vanthenedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannai paarthu
<input type="checkbox" class="lyrico-select-lyric-line" value="Kannai paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kolladha di
<input type="checkbox" class="lyrico-select-lyric-line" value="Kolladha di"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannai paarkka maranthenadi
<input type="checkbox" class="lyrico-select-lyric-line" value="Mannai paarkka maranthenadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennodu vaazhvaaiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennodu vaazhvaaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu servaaiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirodu servaaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaigal sernthaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kaigal sernthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaazhuven uru maaruven
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan vaazhuven uru maaruven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ennodu vaazhvaaiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Ennodu vaazhvaaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyirodu servaaiyo
<input type="checkbox" class="lyrico-select-lyric-line" value="Uyirodu servaaiyo"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kaigal sernthaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Un kaigal sernthaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaazhuven maaruven
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan vaazhuven maaruven"/>
</div>
</pre>