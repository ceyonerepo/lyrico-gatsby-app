---
title: "little little song lyrics"
album: "Galatta Kalyaanam"
artist: "A R Rahman "
lyricist: "Dhanush"
director: "Aanand L Rai"
path: "/albums/galatta-kalyaanam-song-lyrics"
song: "Little Little"
image: ../../images/albumart/galatta-kalyaanam.jpg
date: 2021-12-24
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vwvcavG_X48"
type: "happy"
singers:
  - Dhanush
  - Sharanya Srinivas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Total Ah Out U Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Total Ah Out U Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Romantic Night U Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Romantic Night U Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukkutha Illaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukkutha Illaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkum Doubt U Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkum Doubt U Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakulla Smile Ukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakulla Smile Ukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pain Ukkum Kadhal Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pain Ukkum Kadhal Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Me Little Little Little
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Me Little Little Little"/>
</div>
<div class="lyrico-lyrics-wrapper">Little Little Little Little
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little Little Little Little"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ore Ore Ore Ore Dear Dear
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ore Ore Ore Ore Dear Dear"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Ma Near Near
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Ma Near Near"/>
</div>
<div class="lyrico-lyrics-wrapper">Yen Ma Fear Fears
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Ma Fear Fears"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollu Cheers Cheers
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollu Cheers Cheers"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Feeling Is Healing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feeling Is Healing"/>
</div>
<div class="lyrico-lyrics-wrapper">No More Dealing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="No More Dealing"/>
</div>
<div class="lyrico-lyrics-wrapper">Going Higher Higher Higher
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Going Higher Higher Higher"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Me Little Little Little
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Me Little Little Little"/>
</div>
<div class="lyrico-lyrics-wrapper">Little Little Little Little
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little Little Little Little"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Never Mind
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Never Mind"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Is Heavy Trouble U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Is Heavy Trouble U"/>
</div>
<div class="lyrico-lyrics-wrapper">If Is True
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="If Is True"/>
</div>
<div class="lyrico-lyrics-wrapper">Venam Heart Is Double In U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venam Heart Is Double In U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Indha Devadas U Doctor Ku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Devadas U Doctor Ku"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Mattum Thaan Medicine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Mattum Thaan Medicine"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kulira Kulira Kulira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulira Kulira Kulira"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vaarthai Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vaarthai Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasukulla Marachu Vaikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasukulla Marachu Vaikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Me Little Little Little
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Me Little Little Little"/>
</div>
<div class="lyrico-lyrics-wrapper">Little Little Little Little
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little Little Little Little"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Me Konjam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Me Konjam Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Konjam Konjam Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Konjam Konjam Konjam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Kannamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Kannamma"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Not Your Pair U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Not Your Pair U"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhukkuma Kolambura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhukkuma Kolambura"/>
</div>
<div class="lyrico-lyrics-wrapper">Aiyyo Venam Bore U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyyo Venam Bore U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thor Man Behind You Is My Aalu Aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thor Man Behind You Is My Aalu Aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharama Mayakkum Avan Magic Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharama Mayakkum Avan Magic Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijama En Heart U Kulla Enna Undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijama En Heart U Kulla Enna Undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Sollu Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sollu Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Poga Thaan Puriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Poga Thaan Puriyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Me Little Little Little
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Me Little Little Little"/>
</div>
<div class="lyrico-lyrics-wrapper">Little Little Little Little
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Little Little Little Little"/>
</div>
</pre>
