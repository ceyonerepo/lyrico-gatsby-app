---
title: "manasulone nilichipoke song lyrics"
album: "Varudu Kaavalenu"
artist: "Vishal Chandrashekhar"
lyricist: "Sirivennela Sitaramasastri"
director: "Lakshmi Sowjanya"
path: "/albums/varudu-kaavalenu-lyrics"
song: "Manasulone Nilichipoke"
image: ../../images/albumart/varudu-kaavalenu.jpg
date: 2021-10-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/fA5kMMJnasA"
type: "love"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Manasulone Nilichipoke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulone Nilichipoke "/>
</div>
<div class="lyrico-lyrics-wrapper">Maimarapula Madhurima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maimarapula Madhurima"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavi Dhaati Veliki Raaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi Dhaati Veliki Raaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedhurendhuke Hrudayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedhurendhuke Hrudayama"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulone Nilichipoke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulone Nilichipoke "/>
</div>
<div class="lyrico-lyrics-wrapper">Maimarapula Madhurima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maimarapula Madhurima"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavi Dhaati Veliki Raaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi Dhaati Veliki Raaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedhurendhuke Hrudayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedhurendhuke Hrudayama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enninnaallilaa Ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enninnaallilaa Ee "/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoboochula Samshayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoboochula Samshayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anni Vaipulaa Venutharime 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anni Vaipulaa Venutharime "/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhunu Choosi Adagadhemi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhunu Choosi Adagadhemi"/>
</div>
<div class="lyrico-lyrics-wrapper">Leniponi Bidiyamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leniponi Bidiyamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oohalone Ooyaloopi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oohalone Ooyaloopi"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaaripoke Samayamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaaripoke Samayamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thadabade Thalapula Thapana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thadabade Thalapula Thapana"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhani Telapakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhani Telapakaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasulone Nilichipoke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulone Nilichipoke "/>
</div>
<div class="lyrico-lyrics-wrapper">Maimarapula Madhurima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maimarapula Madhurima"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavi Dhaati Veliki Raaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi Dhaati Veliki Raaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedhurendhuke Hrudayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedhurendhuke Hrudayama"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulone Nilichipoke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulone Nilichipoke "/>
</div>
<div class="lyrico-lyrics-wrapper">Maimarapula Madhurima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maimarapula Madhurima"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavi Dhaati Veliki Raaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi Dhaati Veliki Raaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedhurendhuke Hrudayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedhurendhuke Hrudayama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raa Priyaa Shasivadanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Priyaa Shasivadanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ani Ye Pilupu Vinabadenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ani Ye Pilupu Vinabadenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanapai Idhi Valana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanapai Idhi Valana"/>
</div>
<div class="lyrico-lyrics-wrapper">Edho Bhramalo Unnaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Bhramalo Unnaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chitike Chevibadi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chitike Chevibadi "/>
</div>
<div class="lyrico-lyrics-wrapper">Thrutilo Mathi Chedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thrutilo Mathi Chedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanaa Yaathana Melipeduthundagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanaa Yaathana Melipeduthundagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naa Prathi Anuvanuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Prathi Anuvanuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sumamai Virise Tholi Ruthuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sumamai Virise Tholi Ruthuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ikapai Naa Prathi Choopu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ikapai Naa Prathi Choopu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanakai Veche Nava Vadhuvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakai Veche Nava Vadhuvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chelime Balapadi Runamai Mudipade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chelime Balapadi Runamai Mudipade"/>
</div>
<div class="lyrico-lyrics-wrapper">Raagaalaapana Modalavuthundagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raagaalaapana Modalavuthundagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasulone Nilichipoke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulone Nilichipoke "/>
</div>
<div class="lyrico-lyrics-wrapper">Maimarapula Madhurima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maimarapula Madhurima"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavi Dhaati Veliki Raaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi Dhaati Veliki Raaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedhurendhuke Hrudayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedhurendhuke Hrudayama"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasulone Nilichipoke 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasulone Nilichipoke "/>
</div>
<div class="lyrico-lyrics-wrapper">Maimarapula Madhurima
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maimarapula Madhurima"/>
</div>
<div class="lyrico-lyrics-wrapper">Pedavi Dhaati Veliki Raaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedavi Dhaati Veliki Raaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Bedhurendhuke Hrudayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedhurendhuke Hrudayama"/>
</div>
</pre>
