---
title: "perenti oorenti song lyrics"
album: "Adbhutham"
artist: "Radhan"
lyricist: "Krishna kanth"
director: "Mallik Ram"
path: "/albums/adbhutham-lyrics"
song: "Perenti Oorenti"
image: ../../images/albumart/adbhutham.jpg
date: 2021-11-19
lang: telugu
youtubeLink: "https://www.youtube.com/embed/MU_6wsigxzw"
type: "love"
singers:
  - Yogisekar
  - Sameera Bharadwaj
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hello Mister Wrong Call Ekkaduntaav Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hello Mister Wrong Call Ekkaduntaav Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Challo Nuvvaithe Smart Girl Kanukkolevaa Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Challo Nuvvaithe Smart Girl Kanukkolevaa Nuvvu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perenti Oorenti Asalinthaki Godaventi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perenti Oorenti Asalinthaki Godaventi"/>
</div>
<div class="lyrico-lyrics-wrapper">Age Enti Size Enti? Arey Koncham Cheppavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Age Enti Size Enti? Arey Koncham Cheppavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorry Sorry Size Kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorry Sorry Size Kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Age College Cheppavaa?
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Age College Cheppavaa?"/>
</div>
<div class="lyrico-lyrics-wrapper">Oreyy Sachhinodaa Ilaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oreyy Sachhinodaa Ilaa "/>
</div>
<div class="lyrico-lyrics-wrapper">Vaagaavante Block Chesesthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaagaavante Block Chesesthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Champesthaa Ardhamaindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Champesthaa Ardhamaindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorry Sorry Block Ante Gurthochhindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorry Sorry Block Ante Gurthochhindhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inthaki Nuvu Fair Gaa Untaavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaki Nuvu Fair Gaa Untaavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Fair & Lovely Batchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fair & Lovely Batchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oreyy Intha Repistuventraa Nuvvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oreyy Intha Repistuventraa Nuvvu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enti Repistaa Hello Hello Nenaatype Kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enti Repistaa Hello Hello Nenaatype Kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sorry Sorry Adhi Auto Type-U
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sorry Sorry Adhi Auto Type-U"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elaa Untaado Vaadevado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaa Untaado Vaadevado"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendauthaado Leka Fake Avuthaado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendauthaado Leka Fake Avuthaado"/>
</div>
<div class="lyrico-lyrics-wrapper">Elaa Untundho Em Vintho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaa Untundho Em Vintho"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalammaaye Leka Maarchina Gontho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalammaaye Leka Maarchina Gontho"/>
</div>
<div class="lyrico-lyrics-wrapper">Elaa Kudhirindho Kalisosthundho Kalagauthundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaa Kudhirindho Kalisosthundho Kalagauthundho"/>
</div>
<div class="lyrico-lyrics-wrapper">Elaa Thagilindho Thanu HonestO Leka Handisthundho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elaa Thagilindho Thanu HonestO Leka Handisthundho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perenti Oorenti Asalinthaki Godaventi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perenti Oorenti Asalinthaki Godaventi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaku Enti Himsa Enti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaku Enti Himsa Enti"/>
</div>
<div class="lyrico-lyrics-wrapper">Arre Chatting Aapavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arre Chatting Aapavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagi Unna Life Lona Kottha Rangulochhenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagi Unna Life Lona Kottha Rangulochhenanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhoora Dhooramunna Gaani Chaala Chaala Maarenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhoora Dhooramunna Gaani Chaala Chaala Maarenanta"/>
</div>
<div class="lyrico-lyrics-wrapper">DarlingU Dhaakaa Poni DailingU Kaa Kahani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="DarlingU Dhaakaa Poni DailingU Kaa Kahani"/>
</div>
<div class="lyrico-lyrics-wrapper">Red Hat Pettaleni Stage Lone Undhi Kaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Red Hat Pettaleni Stage Lone Undhi Kaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Poddhugaala Nunde StartU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddhugaala Nunde StartU"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Phone Lona Bhaathakaani Pattanainaa Pattadhantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Phone Lona Bhaathakaani Pattanainaa Pattadhantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pakkaninkaa Evarunnaa Kaani Adhi Kanna Baabu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakkaninkaa Evarunnaa Kaani Adhi Kanna Baabu"/>
</div>
<div class="lyrico-lyrics-wrapper">Dekhore Jaadhu Ye To Pakkaa Hi Jaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekhore Jaadhu Ye To Pakkaa Hi Jaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Doubtemi Ledhu Antha Quabe Ayithe Kaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doubtemi Ledhu Antha Quabe Ayithe Kaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Okarinokaru Telusukuntu Different Kalalu Kantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okarinokaru Telusukuntu Different Kalalu Kantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kya Ho Raha Hi Yaa Pe Kya Ho Raha Hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kya Ho Raha Hi Yaa Pe Kya Ho Raha Hi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pyaar Raha Hi Pakkaa Pyaar Rahaa Hi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pyaar Raha Hi Pakkaa Pyaar Rahaa Hi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagalidhe Raathridhe Theliyadhe Okate Godavidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalidhe Raathridhe Theliyadhe Okate Godavidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Telupavaa Telupavaa Neeku Ilaage Jaruguthundhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telupavaa Telupavaa Neeku Ilaage Jaruguthundhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaadadigithe Telisindhi Emouthondhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaadadigithe Telisindhi Emouthondhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Thadabaatulone Nenu Unnaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Thadabaatulone Nenu Unnaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Peranukunte Poni Premai Podhugaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peranukunte Poni Premai Podhugaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenanukunte Chaalaa Vaadiki Theliyaaligaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenanukunte Chaalaa Vaadiki Theliyaaligaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mundhulaa Konchamainaa Lene Ledhu Jeevitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundhulaa Konchamainaa Lene Ledhu Jeevitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagadhe Ganthulese Gundelona Sambaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagadhe Ganthulese Gundelona Sambaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Maarinaa Machhukainaa Choopaleni Bhaavame Idhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maarinaa Machhukainaa Choopaleni Bhaavame Idhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Korinaa Korukunna Haayigundhigaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Korinaa Korukunna Haayigundhigaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aeyy Enti Emaalochisthunnaav
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aeyy Enti Emaalochisthunnaav"/>
</div>
<div class="lyrico-lyrics-wrapper">Perenti Oorenti Asalinthaki Godaventi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perenti Oorenti Asalinthaki Godaventi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasthainaa Thaggindhaa Mudrindaa Sodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasthainaa Thaggindhaa Mudrindaa Sodaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Perenti Oorenti Asalinthaki Godaventi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perenti Oorenti Asalinthaki Godaventi"/>
</div>
</pre>
