---
title: "ava en aalu song lyrics"
album: "Dha Dha 87"
artist: "Leander Lee Marty"
lyricist: "Vijay Sri G"
director: "Vijay Sri G"
path: "/albums/dha-dha-87-lyrics"
song: "Ava En Aalu"
image: ../../images/albumart/dha-dha-87.jpg
date: 2019-03-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/likUo1ytzE4"
type: "sad"
singers:
  - 	Velmurugan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ava en aalu en kadha kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava en aalu en kadha kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">sonna vekka kedu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonna vekka kedu daa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ava en aalu en kadha kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava en aalu en kadha kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">sonna vekka kedu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonna vekka kedu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">viduncha vaanam kappal erum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viduncha vaanam kappal erum"/>
</div>
<div class="lyrico-lyrics-wrapper">paampaa pappam paam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paampaa pappam paam"/>
</div>
<div class="lyrico-lyrics-wrapper">paampaa pappam paamppapaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paampaa pappam paamppapaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa bodha nee bodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa bodha nee bodha"/>
</div>
<div class="lyrico-lyrics-wrapper">oore bodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore bodha"/>
</div>
<div class="lyrico-lyrics-wrapper">indha kaadhala sethuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha kaadhala sethuta"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhka tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhka tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">quarteru teru wateru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="quarteru teru wateru"/>
</div>
<div class="lyrico-lyrics-wrapper">podhum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhum pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">enaa vittudu kenjuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaa vittudu kenjuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ullam kolla pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullam kolla pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">unma therinji pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unma therinji pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">jodi dhaadi aachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jodi dhaadi aachi"/>
</div>
<div class="lyrico-lyrics-wrapper">vesham kalanji pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vesham kalanji pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">aayiram ponna paathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram ponna paathen"/>
</div>
<div class="lyrico-lyrics-wrapper">naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal kadavul aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal kadavul aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna"/>
</div>
<div class="lyrico-lyrics-wrapper">vechi vechi senji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechi vechi senji"/>
</div>
<div class="lyrico-lyrics-wrapper">nenja polandhutiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenja polandhutiye"/>
</div>
<div class="lyrico-lyrics-wrapper">makeup potta lockup pannum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="makeup potta lockup pannum"/>
</div>
<div class="lyrico-lyrics-wrapper">sattam kondu varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattam kondu varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal kastam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal kastam"/>
</div>
<div class="lyrico-lyrics-wrapper">enakku thandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakku thandha"/>
</div>
<div class="lyrico-lyrics-wrapper">sathiyama sathiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathiyama sathiyama"/>
</div>
<div class="lyrico-lyrics-wrapper">oora vittu odiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora vittu odiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa bodha nee bodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa bodha nee bodha"/>
</div>
<div class="lyrico-lyrics-wrapper">oore bodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore bodha"/>
</div>
<div class="lyrico-lyrics-wrapper">indha kaadhala sethuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha kaadhala sethuta"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhka tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhka tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">quarteru teru wateru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="quarteru teru wateru"/>
</div>
<div class="lyrico-lyrics-wrapper">podhum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhum pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">enaa vittudu kenjuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaa vittudu kenjuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadhal seththu pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal seththu pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">vayasu kaavi achi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vayasu kaavi achi"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhka dhooram pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhka dhooram pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">oore sirichi pochi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore sirichi pochi"/>
</div>
<div class="lyrico-lyrics-wrapper">aayiram poiyaa sonnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayiram poiyaa sonnen"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">oththa unma solli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oththa unma solli"/>
</div>
<div class="lyrico-lyrics-wrapper">enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna"/>
</div>
<div class="lyrico-lyrics-wrapper">vechi vechi senji
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vechi vechi senji"/>
</div>
<div class="lyrico-lyrics-wrapper">maanam vaangittale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maanam vaangittale"/>
</div>
<div class="lyrico-lyrics-wrapper">vesham pottu mosam panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vesham pottu mosam panna"/>
</div>
<div class="lyrico-lyrics-wrapper">marinala kottam kootuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marinala kottam kootuven"/>
</div>
<div class="lyrico-lyrics-wrapper">enna naasam panna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna naasam panna"/>
</div>
<div class="lyrico-lyrics-wrapper">khosham potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="khosham potta"/>
</div>
<div class="lyrico-lyrics-wrapper">sathiyama sathiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sathiyama sathiyama"/>
</div>
<div class="lyrico-lyrics-wrapper">sethu sethu poyiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethu sethu poyiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ava en aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava en aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">ava en aalupa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava en aalupa"/>
</div>
<div class="lyrico-lyrics-wrapper">aai annachi en aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aai annachi en aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">ava en aalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava en aalu"/>
</div>
<div class="lyrico-lyrics-wrapper">ava en aalu en kadha kelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ava en aalu en kadha kelu"/>
</div>
<div class="lyrico-lyrics-wrapper">sonna vekka kedu daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sonna vekka kedu daa"/>
</div>
<div class="lyrico-lyrics-wrapper">viduncha vaanam kappal erum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viduncha vaanam kappal erum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">naa bodha nee bodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa bodha nee bodha"/>
</div>
<div class="lyrico-lyrics-wrapper">oore bodha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oore bodha"/>
</div>
<div class="lyrico-lyrics-wrapper">indha kaadhala sethuta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha kaadhala sethuta"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhka tholla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhka tholla"/>
</div>
<div class="lyrico-lyrics-wrapper">quarteru teru wateru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="quarteru teru wateru"/>
</div>
<div class="lyrico-lyrics-wrapper">podhum pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="podhum pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">enaa vittudu kenjuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaa vittudu kenjuren"/>
</div>
</pre>
