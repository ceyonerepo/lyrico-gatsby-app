---
title: "idhayam song lyrics"
album: "Onaaigal Jakkiradhai"
artist: "Adheesh Uthrian"
lyricist: "JPR"
director: "JPR"
path: "/albums/onaaigal-jakkiradhai-lyrics"
song: "Idhayam"
image: ../../images/albumart/onaaigal-jakkiradhai.jpg
date: 2018-01-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lDcXSQOLRYs"
type: "happy"
singers:
  - Madhu Balakrishnan 
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">idhayal thudikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayal thudikum "/>
</div>
<div class="lyrico-lyrics-wrapper">satham ketene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham ketene "/>
</div>
<div class="lyrico-lyrics-wrapper">endhaasai magalai parkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhaasai magalai parkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">siragugal illa devathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragugal illa devathaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan udalukul 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan udalukul "/>
</div>
<div class="lyrico-lyrics-wrapper">uyirai kalanthavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirai kalanthavale"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal rendum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal rendum "/>
</div>
<div class="lyrico-lyrics-wrapper">kavithai pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavithai pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">ival mugam parkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival mugam parkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">pookal ellam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookal ellam "/>
</div>
<div class="lyrico-lyrics-wrapper">kaalaiyil malarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalaiyil malarum"/>
</div>
<div class="lyrico-lyrics-wrapper">ival thuyil ezhugaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival thuyil ezhugaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagiya vanavil ival thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagiya vanavil ival thane"/>
</div>
<div class="lyrico-lyrics-wrapper">ival endrum maraiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival endrum maraiya "/>
</div>
<div class="lyrico-lyrics-wrapper">varam ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam ketten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee arugil irunthal nallum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee arugil irunthal nallum"/>
</div>
<div class="lyrico-lyrics-wrapper">nallum thirunaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallum thirunaale"/>
</div>
<div class="lyrico-lyrics-wrapper">nee seiyum kurumbugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee seiyum kurumbugal"/>
</div>
<div class="lyrico-lyrics-wrapper">endrum endrum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endrum endrum "/>
</div>
<div class="lyrico-lyrics-wrapper">sugam thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugam thane"/>
</div>
<div class="lyrico-lyrics-wrapper">meendum neeye 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum neeye "/>
</div>
<div class="lyrico-lyrics-wrapper">magalai vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="magalai vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">nooru jenmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooru jenmam"/>
</div>
<div class="lyrico-lyrics-wrapper">illai endral thevai illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="illai endral thevai illai"/>
</div>
<div class="lyrico-lyrics-wrapper">maru jenmam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maru jenmam"/>
</div>
<div class="lyrico-lyrics-wrapper">thaayin mugam kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayin mugam kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">naan paarthathu illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan paarthathu illaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">un mugam paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mugam paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaimaiyai kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaimaiyai kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">pattam poochi ondru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattam poochi ondru"/>
</div>
<div class="lyrico-lyrics-wrapper">malar ena ninaikudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malar ena ninaikudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">mayangiye saayudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayangiye saayudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">idhayal thudikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayal thudikum "/>
</div>
<div class="lyrico-lyrics-wrapper">satham ketene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham ketene "/>
</div>
<div class="lyrico-lyrics-wrapper">endhaasai magalai parkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhaasai magalai parkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">siragugal illa devathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragugal illa devathaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan udalukul 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan udalukul "/>
</div>
<div class="lyrico-lyrics-wrapper">uyirai kalanthavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirai kalanthavale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un thirumugam paarthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thirumugam paarthal"/>
</div>
<div class="lyrico-lyrics-wrapper">kovil silaiyum kai neetum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovil silaiyum kai neetum"/>
</div>
<div class="lyrico-lyrics-wrapper">un siripoli ketal poovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un siripoli ketal poovum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatrum thalai aatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatrum thalai aatum"/>
</div>
<div class="lyrico-lyrics-wrapper">mugathin meethu valuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugathin meethu valuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">amma nooru theivam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amma nooru theivam"/>
</div>
<div class="lyrico-lyrics-wrapper">anbinale alli thanthai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbinale alli thanthai"/>
</div>
<div class="lyrico-lyrics-wrapper">kodi inbam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodi inbam"/>
</div>
<div class="lyrico-lyrics-wrapper">maathathil oru naal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maathathil oru naal"/>
</div>
<div class="lyrico-lyrics-wrapper">naam kandom pournami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naam kandom pournami"/>
</div>
<div class="lyrico-lyrics-wrapper">ne vantha naalaay
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne vantha naalaay"/>
</div>
<div class="lyrico-lyrics-wrapper">naal thorum pournamy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naal thorum pournamy"/>
</div>
<div class="lyrico-lyrics-wrapper">indha inbam ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indha inbam ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">nee ingu thandhathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee ingu thandhathu"/>
</div>
<div class="lyrico-lyrics-wrapper">nimmathi vandhathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nimmathi vandhathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">idhayal thudikum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayal thudikum "/>
</div>
<div class="lyrico-lyrics-wrapper">satham ketene 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="satham ketene "/>
</div>
<div class="lyrico-lyrics-wrapper">endhaasai magalai parkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhaasai magalai parkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">siragugal illa devathaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siragugal illa devathaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">endhan udalukul 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="endhan udalukul "/>
</div>
<div class="lyrico-lyrics-wrapper">uyirai kalanthavale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirai kalanthavale"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal rendum kavithai pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal rendum kavithai pesum"/>
</div>
<div class="lyrico-lyrics-wrapper">ival mugam parkaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival mugam parkaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">pookal ellam kaalaiyil malarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookal ellam kaalaiyil malarum"/>
</div>
<div class="lyrico-lyrics-wrapper">ival thuyil ezhugaiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival thuyil ezhugaiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagiya vanavil ival thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagiya vanavil ival thane"/>
</div>
<div class="lyrico-lyrics-wrapper">ival endrum maraiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ival endrum maraiya "/>
</div>
<div class="lyrico-lyrics-wrapper">varam ketten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam ketten"/>
</div>
</pre>
