---
title: "theera nadhi song lyrics"
album: "Padaiveeran"
artist: "Karthik Raja"
lyricist: "Dhana Sekaran"
director: "Dhana Sekaran"
path: "/albums/padaiveeran-lyrics"
song: "Theera Nadhi"
image: ../../images/albumart/padaiveeran.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/lF6CwEg0GYs"
type: "sad"
singers:
  - Prithvi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Theera nadhiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theera nadhiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhan vazhi maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhan vazhi maari"/>
</div>
<div class="lyrico-lyrics-wrapper">Povathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Povathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Po po podi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Po po podi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanimai theevil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanimai theevil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan vaazhvathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan vaazhvathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engae povaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engae povaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjam ariyaathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjam ariyaathaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanam polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanam polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal thodaraathaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal thodaraathaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vidinthavudan vinmeen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinthavudan vinmeen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil theriyumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil theriyumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Muzhumathiyum iravil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muzhumathiyum iravil"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi oliyumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi oliyumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohooo ohooo ohhhoooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooo ohooo ohhhoooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thee sudarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thee sudarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagiyaai thagithenUn orr paarvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagiyaai thagithenUn orr paarvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu thee porvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu thee porvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha porattam perinbamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha porattam perinbamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugamaai sudugindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugamaai sudugindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee odu oduvaai pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee odu oduvaai pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaadhal virattidum kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaadhal virattidum kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">En kattu meerum kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kattu meerum kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enai thedi thedivaa endraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enai thedi thedivaa endraai"/>
</div>
<div class="lyrico-lyrics-wrapper">En dhegam kothikkuthu pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En dhegam kothikkuthu pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un meni theendum neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un meni theendum neram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee alanthaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee alanthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeraathu vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeraathu vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu polae enthan mogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu polae enthan mogam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un nenjil adanguven pennae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nenjil adanguven pennae"/>
</div>
<div class="lyrico-lyrics-wrapper">En kaalam muzhuvathum kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En kaalam muzhuvathum kannae"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu thaanae enthan thaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu thaanae enthan thaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu thaanae enthan thaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu thaanae enthan thaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanikirathae thanigirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanikirathae thanigirathae"/>
</div>
</pre>
