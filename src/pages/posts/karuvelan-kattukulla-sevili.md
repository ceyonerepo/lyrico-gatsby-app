---
title: "karuvelan kattukulla song lyrics"
album: "Sevili"
artist: "Unknown"
lyricist: "T M Prabakaran"
director: "R. Ananth"
path: "/albums/sevili-lyrics"
song: "Karuvelan Kattukulla"
image: ../../images/albumart/sevili.jpg
date: 2017-03-31
lang: tamil
youtubeLink: "https://www.youtube.com/embed/k9s90MhMPA0"
type: "love"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">karuvelan kaatukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvelan kaatukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">en karuvaachi pogaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en karuvaachi pogaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">nan parunthaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan parunthaga "/>
</div>
<div class="lyrico-lyrics-wrapper">paranthu vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paranthu vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">unna pakama 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pakama "/>
</div>
<div class="lyrico-lyrics-wrapper">thavichi ninnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavichi ninnen"/>
</div>
<div class="lyrico-lyrics-wrapper">en usurakku usuraga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en usurakku usuraga"/>
</div>
<div class="lyrico-lyrics-wrapper">unna vachu thachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna vachu thachu"/>
</div>
<div class="lyrico-lyrics-wrapper">vachen en nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachen en nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">en nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en nenjukulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aiyo aasa naan pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyo aasa naan pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">un mela thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mela thane"/>
</div>
<div class="lyrico-lyrics-wrapper">romba romba aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba romba aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna paada paduthura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna paada paduthura"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kathal ithu kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kathal ithu kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">un mela thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mela thane"/>
</div>
<div class="lyrico-lyrics-wrapper">romba romba kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba romba kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thaanai ilukuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thaanai ilukuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un patha kolusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un patha kolusu"/>
</div>
<div class="lyrico-lyrics-wrapper">oosa kettu nan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oosa kettu nan "/>
</div>
<div class="lyrico-lyrics-wrapper">mayangi ninen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayangi ninen"/>
</div>
<div class="lyrico-lyrics-wrapper">pulla pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulla pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un paarvai pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paarvai pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">pambaram pol nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pambaram pol nan"/>
</div>
<div class="lyrico-lyrics-wrapper">sulandru ninen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sulandru ninen"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnen ninnen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnen ninnen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">potta kaatu orathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="potta kaatu orathula"/>
</div>
<div class="lyrico-lyrics-wrapper">vetta veyil nerathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetta veyil nerathula"/>
</div>
<div class="lyrico-lyrics-wrapper">ninnen pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnen pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnudaiya sirupikula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnudaiya sirupikula"/>
</div>
<div class="lyrico-lyrics-wrapper">mothama nan mayangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mothama nan mayangi"/>
</div>
<div class="lyrico-lyrics-wrapper">irunthen irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irunthen irunthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi pakathula nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi pakathula nee"/>
</div>
<div class="lyrico-lyrics-wrapper">irukka vekathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukka vekathula"/>
</div>
<div class="lyrico-lyrics-wrapper">nan anaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan anaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">thegam pathama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thegam pathama"/>
</div>
<div class="lyrico-lyrics-wrapper">pathuthadi sola pula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathuthadi sola pula"/>
</div>
<div class="lyrico-lyrics-wrapper">thegam thaniyaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thegam thaniyaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">ilukuthada un ninapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ilukuthada un ninapula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aiyo aasa naan pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiyo aasa naan pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">un mela thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mela thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">romba romba aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba romba aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna paada paduthuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna paada paduthuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan unna patha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan unna patha"/>
</div>
<div class="lyrico-lyrics-wrapper">nerathula ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nerathula ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">maranthu ponen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maranthu ponen"/>
</div>
<div class="lyrico-lyrics-wrapper">pulla pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulla pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oori pona ilavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oori pona ilavan"/>
</div>
<div class="lyrico-lyrics-wrapper">panju pola nanum aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panju pola nanum aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">aanen aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aanen aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">natta nadunj samathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="natta nadunj samathula"/>
</div>
<div class="lyrico-lyrics-wrapper">thookam ketu ponathenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookam ketu ponathenna"/>
</div>
<div class="lyrico-lyrics-wrapper">pulla pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulla pulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kan mulachu pakaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan mulachu pakaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">pakkam vanthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakkam vanthu "/>
</div>
<div class="lyrico-lyrics-wrapper">ninnathu enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninnathu enna"/>
</div>
<div class="lyrico-lyrics-wrapper">yenna yenna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yenna yenna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adi jalli kattu kaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adi jalli kattu kaali"/>
</div>
<div class="lyrico-lyrics-wrapper">pola thulikittu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pola thulikittu "/>
</div>
<div class="lyrico-lyrics-wrapper">oodi vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodi vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">thenum thinaiyatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenum thinaiyatam"/>
</div>
<div class="lyrico-lyrics-wrapper">sernthukiten un ninapula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sernthukiten un ninapula"/>
</div>
<div class="lyrico-lyrics-wrapper">nanum vekathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum vekathula"/>
</div>
<div class="lyrico-lyrics-wrapper">sanjukiten un madiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sanjukiten un madiyila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aiya aasa nanum pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aiya aasa nanum pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">un mela thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mela thaane"/>
</div>
<div class="lyrico-lyrics-wrapper">romba romba aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba romba aasa"/>
</div>
<div class="lyrico-lyrics-wrapper">enna paada paduthuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna paada paduthuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">ada kathal ithu kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada kathal ithu kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">un mela thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un mela thane"/>
</div>
<div class="lyrico-lyrics-wrapper">romba romba kathal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba romba kathal"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thaanai ilukuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thaanai ilukuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thaanai ilukuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thaanai ilukuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">enna paadai paduthuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna paadai paduthuthe"/>
</div>
</pre>
