---
title: "kaala kaalam song lyrics"
album: "Santhoshathil Kalavaram"
artist: "Sivanag"
lyricist: "Kabilan"
director: "Kranthi Prasad"
path: "/albums/santhoshathil-kalavaram-lyrics"
song: "Kaala Kaalam"
image: ../../images/albumart/santhoshathil-kalavaram.jpg
date: 2018-11-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wO-z2k0aCoc"
type: "love"
singers:
  - P Unnikrishnan
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kaala kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaala kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">vanna kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanna kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">naangu kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangu kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal paalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal paalam"/>
</div>
<div class="lyrico-lyrics-wrapper">minnal podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal podum"/>
</div>
<div class="lyrico-lyrics-wrapper">mela thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela thaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru vellai odai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru vellai odai"/>
</div>
<div class="lyrico-lyrics-wrapper">vannamaaga odatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannamaaga odatho"/>
</div>
<div class="lyrico-lyrics-wrapper">manam kaatrai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam kaatrai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">kaigal thati paadatho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaigal thati paadatho"/>
</div>
<div class="lyrico-lyrics-wrapper">poovum vandum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovum vandum"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhalaai poga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhalaai poga "/>
</div>
<div class="lyrico-lyrics-wrapper">poga vaazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poga vaazhalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaala kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaala kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">vanna kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanna kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">naangu kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangu kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal paalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal paalam"/>
</div>
<div class="lyrico-lyrics-wrapper">minnal podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal podum"/>
</div>
<div class="lyrico-lyrics-wrapper">mela thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela thaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nadai palagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadai palagum"/>
</div>
<div class="lyrico-lyrics-wrapper">kulanthai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulanthai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">unai pazhaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai pazhaga"/>
</div>
<div class="lyrico-lyrics-wrapper">arugil vanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arugil vanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">iruvarukum idaiveli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruvarukum idaiveli"/>
</div>
<div class="lyrico-lyrics-wrapper">vendamaame ohohoh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vendamaame ohohoh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kudai pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudai pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">viralkal pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viralkal pole"/>
</div>
<div class="lyrico-lyrics-wrapper">unai pidikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unai pidikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhigal thandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhigal thandhen"/>
</div>
<div class="lyrico-lyrics-wrapper">idharku oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idharku oru"/>
</div>
<div class="lyrico-lyrics-wrapper">mounam solladhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mounam solladhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thuli thuliyai thooral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thuli thuliyai thooral"/>
</div>
<div class="lyrico-lyrics-wrapper">nee dhaane anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dhaane anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">thudithu vidum kaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thudithu vidum kaigal"/>
</div>
<div class="lyrico-lyrics-wrapper">nee dhane anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dhane anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">irudhaya isaiyinile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irudhaya isaiyinile"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal mudhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal mudhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal sangeetham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal sangeetham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaala kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaala kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">vanna kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanna kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">naangu kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangu kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal paalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal paalam"/>
</div>
<div class="lyrico-lyrics-wrapper">minnal podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal podum"/>
</div>
<div class="lyrico-lyrics-wrapper">mela thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela thaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pazha rasathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pazha rasathil"/>
</div>
<div class="lyrico-lyrics-wrapper">vizhugira thenaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhugira thenaai"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai kulathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai kulathil"/>
</div>
<div class="lyrico-lyrics-wrapper">azhagiya meenaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhagiya meenaai"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir kulathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir kulathil "/>
</div>
<div class="lyrico-lyrics-wrapper">vizhundhadhu nee thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vizhundhadhu nee thane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kadar karaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadar karaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">nadakira paadham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadakira paadham"/>
</div>
<div class="lyrico-lyrics-wrapper">alai adithal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alai adithal"/>
</div>
<div class="lyrico-lyrics-wrapper">azhivadhu paavam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="azhivadhu paavam"/>
</div>
<div class="lyrico-lyrics-wrapper">arindhiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arindhiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal yepodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal yepodhum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">mugavariyin kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mugavariyin kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">nee thaane anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee thaane anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">mudhal variyin artham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mudhal variyin artham"/>
</div>
<div class="lyrico-lyrics-wrapper">nee dhaane anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee dhaane anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">oru veyilil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru veyilil"/>
</div>
<div class="lyrico-lyrics-wrapper">iru nizhalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iru nizhalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">irupadhu than kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irupadhu than kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">nam kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nam kaadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaala kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaala kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhal kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhal kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">vaasal yaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaasal yaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">vanna kolam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanna kolam"/>
</div>
<div class="lyrico-lyrics-wrapper">naangu kannil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naangu kannil"/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhal paalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhal paalam"/>
</div>
<div class="lyrico-lyrics-wrapper">minnal podum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal podum"/>
</div>
<div class="lyrico-lyrics-wrapper">mela thaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mela thaalam"/>
</div>
</pre>
