---
title: "priyathama priyathama song lyrics"
album: "Majili"
artist: "Gopi Sunder"
lyricist: "Chaitanya Prasad"
director: "Shiva Nirvana"
path: "/albums/majili-lyrics"
song: "Priyathama Priyathama"
image: ../../images/albumart/majili.jpg
date: 2019-04-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/BpINyS4k7Uw"
type: "happy"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Priyatamaa.. priyatamaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyatamaa.. priyatamaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Palikinadi.. hrudayame sarigamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palikinadi.. hrudayame sarigamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipi nee.. talapulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipi nee.. talapulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Telisinadi.. valupulo madhurimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telisinadi.. valupulo madhurimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheli choopu taakinaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheli choopu taakinaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ulakavaa.. palakavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulakavaa.. palakavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vala vesi.. vechi.. choostunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vala vesi.. vechi.. choostunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorakane.. dorakavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorakane.. dorakavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtamaina sakhudaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtamaina sakhudaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtamaina sakhudaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtamaina sakhudaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Okkasaari choodaraa.. pilladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkasaari choodaraa.. pilladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakkanaina chukkaraa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkanaina chukkaraa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Chakkanaina chukkaraa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkanaina chukkaraa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu korukundiraa.. sundaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu korukundiraa.. sundaraa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Priyatamaa.. priyatamaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyatamaa.. priyatamaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Palikinadi.. hrudayame sarigamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palikinadi.. hrudayame sarigamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipi nee.. talapulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipi nee.. talapulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Telisinadi.. valupulo madhurimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telisinadi.. valupulo madhurimaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee premalo.. aaraadhanai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee premalo.. aaraadhanai.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ne nindugaa.. munigaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ne nindugaa.. munigaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kosame.. raasaanugaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kosame.. raasaanugaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kallato.. priyalekhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kallato.. priyalekhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheruno.. cherado..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheruno.. cherado.."/>
</div>
<div class="lyrico-lyrics-wrapper">Teliyadu.. aa kaanukaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teliyadu.. aa kaanukaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasane.. veedakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasane.. veedakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Venaka padenu.. manasu padina manase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venaka padenu.. manasu padina manase"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishtamaina sakhudaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtamaina sakhudaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtamaina sakhudaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtamaina sakhudaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Okkasaari choodaraa.. pilladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkasaari choodaraa.. pilladaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnaanilaa.. untaanilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaanilaa.. untaanilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee needagaa.. kadadaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee needagaa.. kadadaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneetilo.. kaarteekapoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneetilo.. kaarteekapoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Deepaanniraa.. nuvvulekaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Deepaanniraa.. nuvvulekaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Doorame.. bhaaramai..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doorame.. bhaaramai.."/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaladu.. naa jeevitam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaladu.. naa jeevitam"/>
</div>
<div class="lyrico-lyrics-wrapper">Neevu naa.. cheruvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neevu naa.. cheruvai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilichi masalu madhura kshanamulepudo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilichi masalu madhura kshanamulepudo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Priyatamaa.. priyatamaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyatamaa.. priyatamaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Palikinadi.. hrudayame sarigamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palikinadi.. hrudayame sarigamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chilipi nee.. talapulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chilipi nee.. talapulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Telisinadi.. valupulo madhurimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telisinadi.. valupulo madhurimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheli choopu taakinaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheli choopu taakinaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ulakavaa.. palakavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulakavaa.. palakavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vala vesi.. vechi.. choostunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vala vesi.. vechi.. choostunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dorakane.. dorakavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dorakane.. dorakavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtamaina sakhudaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtamaina sakhudaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtamaina sakhudaa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtamaina sakhudaa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Okkasaari choodaraa.. pilladaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okkasaari choodaraa.. pilladaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chakkanaina chukkaraa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkanaina chukkaraa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Chakkanaina chukkaraa..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chakkanaina chukkaraa.."/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu korukundiraa.. sundaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu korukundiraa.. sundaraa"/>
</div>
</pre>
