---
title: "jayamma song lyrics"
album: "Jayamma Panchayathi"
artist: "M M Keeravani"
lyricist: "Ramajogayya Sastry"
director: "Vijay Kumar Kalivarapu"
path: "/albums/jayamma-panchayathi-lyrics"
song: "Jayamma"
image: ../../images/albumart/jayamma-panchayathi.jpg
date: 2022-05-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Mi2Fm2HKTJ4"
type: "mass"
singers:
  - Sri Krishna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaasintha Bholathanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasintha Bholathanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosintha Jaali Gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosintha Jaali Gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasintha Gandrathanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasintha Gandrathanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosintha Mondi Gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosintha Mondi Gunam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaasintha Bholathanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasintha Bholathanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosintha Jaali Gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosintha Jaali Gunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasintha Gandrathanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasintha Gandrathanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koosintha Mondi Gunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koosintha Mondi Gunam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achhamaina Palletoori Itthanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achhamaina Palletoori Itthanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkadaina Aamedhega Petthanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkadaina Aamedhega Petthanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee Ammoru Thalli Thoofan Mundara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee Ammoru Thalli Thoofan Mundara"/>
</div>
<div class="lyrico-lyrics-wrapper">Thooneegale Manamandaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thooneegale Manamandaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Jayamma Jayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Jayamma Jayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayamma Jayamma Jayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayamma Jayamma Jayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Choose Janam Kallaku Suryakanthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choose Janam Kallaku Suryakanthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalaina Sangathi Nijangaane Verammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalaina Sangathi Nijangaane Verammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Machhaleni Aa Manase Aakasamanthammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machhaleni Aa Manase Aakasamanthammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakka Intiki Lakshmi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka Intiki Lakshmi"/>
</div>
<div class="lyrico-lyrics-wrapper">Panchadara Appu Teerchaledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchadara Appu Teerchaledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Postman Bhadram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Postman Bhadram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaram Rojulnunchi Raadam Ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaram Rojulnunchi Raadam Ledhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enakatlaaga Thookam Thuyyatledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakatlaaga Thookam Thuyyatledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Attaa Yelle Enkaayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attaa Yelle Enkaayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Nannu Soosi Palakarinchaledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu Soosi Palakarinchaledhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poddhuna Gonna Beerakaayal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poddhuna Gonna Beerakaayal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalugintlo Moodu Chedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalugintlo Moodu Chedhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Endhogaani Naa Gaachaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endhogaani Naa Gaachaaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Okka Sangathi Sariggaaledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okka Sangathi Sariggaaledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pho Pho Pho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pho Pho Pho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sivangi Lekkana Lesthadhi Gaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivangi Lekkana Lesthadhi Gaani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sinna Pilla Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sinna Pilla Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ittaaga Enduku Puttinchaado
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ittaaga Enduku Puttinchaado"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Devudike Telusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Devudike Telusu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattasu Theeruna Peluddhi Baboi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattasu Theeruna Peluddhi Baboi"/>
</div>
<div class="lyrico-lyrics-wrapper">Noti Maate Pelusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noti Maate Pelusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayamma Gonthu Legisindho Chaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayamma Gonthu Legisindho Chaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhoojagame SilenceU
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhoojagame SilenceU"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bussuna Aakashamante 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bussuna Aakashamante "/>
</div>
<div class="lyrico-lyrics-wrapper">Kopam Thakadhimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopam Thakadhimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappuna Sallaaripothadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappuna Sallaaripothadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Paapam Thakadhimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapam Thakadhimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bussuna Aakashamante 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bussuna Aakashamante "/>
</div>
<div class="lyrico-lyrics-wrapper">Kopam Thakadhimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kopam Thakadhimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Sappuna Sallaaripothadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sappuna Sallaaripothadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Paapam Thakadhimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapam Thakadhimi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Jayamma Jayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Jayamma Jayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayamma Jayamma Jayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayamma Jayamma Jayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Choose Janam Kallaku Suryakanthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choose Janam Kallaku Suryakanthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalaina Sangathi Nijangaane Verammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalaina Sangathi Nijangaane Verammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Machhaleni Aa Manase Aakasamanthammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machhaleni Aa Manase Aakasamanthammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammadiyammo Jayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadiyammo Jayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammadiyammo Jayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadiyammo Jayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayammo Jayammo Jayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayammo Jayammo Jayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayammo Jayammo Jayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayammo Jayammo Jayamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edurintloni Chantipaapa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edurintloni Chantipaapa "/>
</div>
<div class="lyrico-lyrics-wrapper">Edupinka Aapatledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edupinka Aapatledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Khatarellina Kaantham Koduku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Khatarellina Kaantham Koduku"/>
</div>
<div class="lyrico-lyrics-wrapper">Emayyaado Patthaa Ledu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emayyaado Patthaa Ledu"/>
</div>
<div class="lyrico-lyrics-wrapper">Rachhabanda Hanumanthudiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rachhabanda Hanumanthudiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Endaa Vaana Neede Ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endaa Vaana Neede Ledhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Abbulu Gaari Choolu Gedhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abbulu Gaari Choolu Gedhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Annam Neellu Mudathaledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annam Neellu Mudathaledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Subbaayamma Mogudiki Dhaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Subbaayamma Mogudiki Dhaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimisham Koodaa Paduthaledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimisham Koodaa Paduthaledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Abbabbaa Ee Kashtaalaki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Abbabbaa Ee Kashtaalaki"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthu Ponthu Lene Ledhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthu Ponthu Lene Ledhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pho Pho Pho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pho Pho Pho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ammadiyammo Jayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadiyammo Jayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammadiyammo Jayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammadiyammo Jayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayammo Jayammo Jayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayammo Jayammo Jayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayammo Jayammo Jayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayammo Jayammo Jayamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porugollaki Saayam Cheyyadamante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porugollaki Saayam Cheyyadamante"/>
</div>
<div class="lyrico-lyrics-wrapper">Ishtamandi Thanaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishtamandi Thanaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirigi Saayam Cheyyakapothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirigi Saayam Cheyyakapothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Irakaatame Manaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irakaatame Manaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niddharothe Ottu Pakka Vaalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niddharothe Ottu Pakka Vaalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalla Neellu Thudiche Varaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalla Neellu Thudiche Varaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Attaage Manani Thodundamantadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attaage Manani Thodundamantadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanakochhe Aapadhaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanakochhe Aapadhaku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ledhu Momaatam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhu Momaatam "/>
</div>
<div class="lyrico-lyrics-wrapper">Intaavanta Thakadhimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intaavanta Thakadhimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanadhenanukuntadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanadhenanukuntadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ooroorantha Thakadhimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooroorantha Thakadhimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ledhu Momaatam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ledhu Momaatam "/>
</div>
<div class="lyrico-lyrics-wrapper">Intaavanta Thakadhimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intaavanta Thakadhimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanadhenanukuntadhi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanadhenanukuntadhi "/>
</div>
<div class="lyrico-lyrics-wrapper">Ooroorantha Thakadhimi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooroorantha Thakadhimi"/>
</div>
<div class="lyrico-lyrics-wrapper">Evarememanukunnaa Maro 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evarememanukunnaa Maro "/>
</div>
<div class="lyrico-lyrics-wrapper">Maate Le Dhan Ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maate Le Dhan Ta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyy Jayamma Jayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy Jayamma Jayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Jayamma Jayamma Jayamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jayamma Jayamma Jayamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Choose Janam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choose Janam "/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaku Suryakanthamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaku Suryakanthamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Asalaina Sangathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Asalaina Sangathi "/>
</div>
<div class="lyrico-lyrics-wrapper">Nijangaane Verammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijangaane Verammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Machhaleni Aa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machhaleni Aa "/>
</div>
<div class="lyrico-lyrics-wrapper">Manase Aakasamanthammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase Aakasamanthammaa"/>
</div>
</pre>
