---
title: "nee romba romba song lyrics"
album: "Oorantha Anukuntunnaru"
artist: "	KM Radha Krishnan"
lyricist: "Srihari Mangalampalli"
director: "Balaji Sanala"
path: "/albums/oorantha-anukuntunnaru-lyrics"
song: "Nee Romba Romba"
image: ../../images/albumart/oorantha-anukuntunaaru.jpg
date: 2019-10-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/LhdaUL2vnMs"
type: "happy"
singers:
  - Manisha Eerabathini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">nee romba romba 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee romba romba "/>
</div>
<div class="lyrico-lyrics-wrapper">pinnale paathaa romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnale paathaa romba"/>
</div>
<div class="lyrico-lyrics-wrapper">nine paatha romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nine paatha romba"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnale paathaa romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnale paathaa romba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee romba romba 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee romba romba "/>
</div>
<div class="lyrico-lyrics-wrapper">pinnale paathaa romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnale paathaa romba"/>
</div>
<div class="lyrico-lyrics-wrapper">nine paatha romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nine paatha romba"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnale paathaa romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnale paathaa romba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">bandham bharuvanukuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bandham bharuvanukuntaa"/>
</div>
<div class="lyrico-lyrics-wrapper">paruguku skaye haddhanukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paruguku skaye haddhanukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">valanti sankellannee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valanti sankellannee"/>
</div>
<div class="lyrico-lyrics-wrapper">naake thalanchi theeraalanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naake thalanchi theeraalanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee romba romba 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee romba romba "/>
</div>
<div class="lyrico-lyrics-wrapper">pinnale romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnale romba romba"/>
</div>
<div class="lyrico-lyrics-wrapper">thane romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thane romba romba"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnale romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnale romba romba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nanno bad girl ante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanno bad girl ante"/>
</div>
<div class="lyrico-lyrics-wrapper">life ki dhaanno gift anukunta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="life ki dhaanno gift anukunta"/>
</div>
<div class="lyrico-lyrics-wrapper">yelaanti swargam ayinaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelaanti swargam ayinaa"/>
</div>
<div class="lyrico-lyrics-wrapper">ipude ikade undhanukuntaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipude ikade undhanukuntaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee romba romba 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee romba romba "/>
</div>
<div class="lyrico-lyrics-wrapper">pinnale romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnale romba romba"/>
</div>
<div class="lyrico-lyrics-wrapper">ninne romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninne romba romba"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnale romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnale romba romba"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kodala kodala kodala kodala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodala kodala kodala kodala"/>
</div>
<div class="lyrico-lyrics-wrapper">komalamma kodalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komalamma kodalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">komalamma kodalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komalamma kodalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naay mudhulaa maradhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naay mudhulaa maradhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">komalamma kodalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komalamma kodalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naay mudhulaa maradhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naay mudhulaa maradhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">komalamma komalamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komalamma komalamma"/>
</div>
<div class="lyrico-lyrics-wrapper">komalamma komalamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komalamma komalamma"/>
</div>
<div class="lyrico-lyrics-wrapper">komalamma kodalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komalamma kodalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">naay mudhulaa maradhalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naay mudhulaa maradhalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">komalamma komalamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komalamma komalamma"/>
</div>
<div class="lyrico-lyrics-wrapper">komalamma komalamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="komalamma komalamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ninne paathaa romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninne paathaa romba"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnale paathaa romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnale paathaa romba"/>
</div>
<div class="lyrico-lyrics-wrapper">nee romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee romba romba"/>
</div>
<div class="lyrico-lyrics-wrapper">pinaale paathaa romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinaale paathaa romba"/>
</div>
<div class="lyrico-lyrics-wrapper">ninne paathaa romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninne paathaa romba"/>
</div>
<div class="lyrico-lyrics-wrapper">pinnale paathaa romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnale paathaa romba"/>
</div>
<div class="lyrico-lyrics-wrapper">romba romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba romba romba"/>
</div>
<div class="lyrico-lyrics-wrapper">romba romba romba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="romba romba romba"/>
</div>
</pre>
