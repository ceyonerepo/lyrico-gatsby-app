---
title: "aasa aasaiya song lyrics"
album: "Kannakkol"
artist: "Bobby"
lyricist: "Muthuvijayan"
director: "V.A.Kumaresan"
path: "/albums/kannakkol-lyrics"
song: "Aasa Aasaiya"
image: ../../images/albumart/kannakkol.jpg
date: 2018-06-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/iA6_CyIJtGA"
type: "love"
singers:
  - Vijay Prakash
  - Madhushree
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aasa aha aha ahaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa aha aha ahaye"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa aha aha ahaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa aha aha ahaye"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa aasaiyaai naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa aasaiyaai naan "/>
</div>
<div class="lyrico-lyrics-wrapper">unna pakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu peruma sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu peruma sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">poluthu pokalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poluthu pokalam"/>
</div>
<div class="lyrico-lyrics-wrapper">nethi potula nerama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nethi potula nerama"/>
</div>
<div class="lyrico-lyrics-wrapper">othu poganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="othu poganum"/>
</div>
<div class="lyrico-lyrics-wrapper">nenju kuliyila vilunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenju kuliyila vilunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">seththu poganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seththu poganum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">antha raani thorkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha raani thorkumo"/>
</div>
<div class="lyrico-lyrics-wrapper">alagula naan seeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagula naan seeni"/>
</div>
<div class="lyrico-lyrics-wrapper">paagu pol karaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paagu pol karaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">un nilalula ondi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nilalula ondi "/>
</div>
<div class="lyrico-lyrics-wrapper">kudithanm naan nadathuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudithanm naan nadathuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aasa aasaiyaai naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa aasaiyaai naan "/>
</div>
<div class="lyrico-lyrics-wrapper">unna pakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu peruma sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu peruma sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">poluthu pokalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poluthu pokalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">eera kaathu veesum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="eera kaathu veesum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">enna ennamo thonuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna ennamo thonuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">kaara saara mutham ketu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaara saara mutham ketu"/>
</div>
<div class="lyrico-lyrics-wrapper">udathu enna koluthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udathu enna koluthadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koora kozhi koovum satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koora kozhi koovum satham"/>
</div>
<div class="lyrico-lyrics-wrapper">kolam poda solluthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolam poda solluthaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">oora vacha neelu poola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oora vacha neelu poola"/>
</div>
<div class="lyrico-lyrics-wrapper">vetkam ennai keeruthaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetkam ennai keeruthaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">enna pathathum unaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pathathum unaku"/>
</div>
<div class="lyrico-lyrics-wrapper">enna thonuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna thonuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">parata thalaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="parata thalaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">thadava ennai theduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadava ennai theduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kiliye un alava paatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiliye un alava paatha"/>
</div>
<div class="lyrico-lyrics-wrapper">nenave maarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenave maarum"/>
</div>
<div class="lyrico-lyrics-wrapper">kanna moodava illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanna moodava illa"/>
</div>
<div class="lyrico-lyrics-wrapper">unna moodava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna moodava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">antha raani thorkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha raani thorkumo"/>
</div>
<div class="lyrico-lyrics-wrapper">alagula naan seeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagula naan seeni"/>
</div>
<div class="lyrico-lyrics-wrapper">paagu pol karaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paagu pol karaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">un nilalula ondi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nilalula ondi "/>
</div>
<div class="lyrico-lyrics-wrapper">kudithanm naan nadathuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudithanm naan nadathuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aasa aasaiyaai naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa aasaiyaai naan "/>
</div>
<div class="lyrico-lyrics-wrapper">unna pakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu peruma sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu peruma sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">poluthu pokalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poluthu pokalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kora pulla naan irupen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kora pulla naan irupen"/>
</div>
<div class="lyrico-lyrics-wrapper">aatu kutty nee valatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatu kutty nee valatha"/>
</div>
<div class="lyrico-lyrics-wrapper">koparaiyil naa siripen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koparaiyil naa siripen"/>
</div>
<div class="lyrico-lyrics-wrapper">thanee moka nee kuninja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanee moka nee kuninja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nee enaku venum munnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee enaku venum munnu"/>
</div>
<div class="lyrico-lyrics-wrapper">saami kitta vendikiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saami kitta vendikiten"/>
</div>
<div class="lyrico-lyrics-wrapper">kaanikaiyaa kanni nenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanikaiyaa kanni nenja"/>
</div>
<div class="lyrico-lyrics-wrapper">nan mudinju potukiten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan mudinju potukiten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unna sumanthu than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna sumanthu than"/>
</div>
<div class="lyrico-lyrics-wrapper">nadantha paaram theriyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadantha paaram theriyala"/>
</div>
<div class="lyrico-lyrics-wrapper">unga kooda than iruntha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unga kooda than iruntha"/>
</div>
<div class="lyrico-lyrics-wrapper">neram karaiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neram karaiyala"/>
</div>
<div class="lyrico-lyrics-wrapper">alage unna pirinja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alage unna pirinja"/>
</div>
<div class="lyrico-lyrics-wrapper">uyirum piriyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyirum piriyum"/>
</div>
<div class="lyrico-lyrics-wrapper">saami sathiyam adiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saami sathiyam adiye"/>
</div>
<div class="lyrico-lyrics-wrapper">saami sathiyam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saami sathiyam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">antha raani thorkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha raani thorkumo"/>
</div>
<div class="lyrico-lyrics-wrapper">alagula naan seeni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagula naan seeni"/>
</div>
<div class="lyrico-lyrics-wrapper">paagu pol karaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paagu pol karaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">un nilalula ondi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un nilalula ondi "/>
</div>
<div class="lyrico-lyrics-wrapper">kudithanm naan nadathuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudithanm naan nadathuren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aasa aasaiyaai naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa aasaiyaai naan "/>
</div>
<div class="lyrico-lyrics-wrapper">unna pakanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna pakanum"/>
</div>
<div class="lyrico-lyrics-wrapper">rendu peruma sernthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rendu peruma sernthu"/>
</div>
<div class="lyrico-lyrics-wrapper">poluthu pokalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poluthu pokalam"/>
</div>
</pre>
