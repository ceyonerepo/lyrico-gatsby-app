---
title: "porappo song lyrics"
album: "Koottali"
artist: "Britto Michael"
lyricist: "Kurinchi Prabha"
director: "SK Mathi"
path: "/albums/koottali-song-lyrics"
song: "Porappo"
image: ../../images/albumart/koottali.jpg
date: 2018-02-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vKn_p2mbZ5M"
type: "theme song"
singers:
  - Britto Michael
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">porappo porappo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porappo porappo"/>
</div>
<div class="lyrico-lyrics-wrapper">muraikama poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muraikama poda"/>
</div>
<div class="lyrico-lyrics-wrapper">narambellam murukerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narambellam murukerum"/>
</div>
<div class="lyrico-lyrics-wrapper">koottali nan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koottali nan da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">porappo porappo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="porappo porappo"/>
</div>
<div class="lyrico-lyrics-wrapper">muraikama poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="muraikama poda"/>
</div>
<div class="lyrico-lyrics-wrapper">narambellam murukerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="narambellam murukerum"/>
</div>
<div class="lyrico-lyrics-wrapper">koottali nan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koottali nan da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">koottali koottali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koottali koottali"/>
</div>
<div class="lyrico-lyrics-wrapper">koo koo koo koottali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koo koo koo koottali"/>
</div>
<div class="lyrico-lyrics-wrapper">koo koo koo koottali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koo koo koo koottali"/>
</div>
</pre>
