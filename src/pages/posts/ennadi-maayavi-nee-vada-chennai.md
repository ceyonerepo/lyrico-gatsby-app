---
title: "ennadi maayavi nee song lyrics"
album: "Vada Chennai"
artist: "Santhosh Narayanan"
lyricist: "Vivek"
director: "Vetrimaaran"
path: "/albums/vada-chennai-lyrics"
song: "Ennadi Maayavi Nee"
image: ../../images/albumart/vada-chennai.jpg
date: 2018-10-17
lang: tamil
youtubeLink: "https://www.youtube.com/embed/s20GxxYUhfA"
type: "love"
singers:
  - Sid Sriram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hmm hmm hmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei"/>
</div>
<div class="lyrico-lyrics-wrapper">En thalaikkerura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thalaikkerura"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon thadam podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon thadam podura"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyiraadura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyiraadura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadi maayavi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadi maayavi nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En nelam maathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nelam maathura"/>
</div>
<div class="lyrico-lyrics-wrapper">Andharamaakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharamaakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">En nejam kaatura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nejam kaatura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Patta kaththi thooki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patta kaththi thooki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo mittai narukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo mittai narukkura"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittaa nenja vaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittaa nenja vaari"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pattaa kirukkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pattaa kirukkura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tururu tutu tururu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tururu tutu tururu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tuttu tururu tutu tururu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tuttu tururu tutu tururu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaa oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaa oooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaa oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaa oooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaa oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaa oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei"/>
</div>
<div class="lyrico-lyrics-wrapper">En thalaikkerura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thalaikkerura"/>
</div>
<div class="lyrico-lyrics-wrapper">Pon thadam podura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pon thadam podura"/>
</div>
<div class="lyrico-lyrics-wrapper">En uyiraadura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En uyiraadura"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennadi maayavi nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadi maayavi nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En nelam maathura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nelam maathura"/>
</div>
<div class="lyrico-lyrics-wrapper">Andharamaakkura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharamaakkura"/>
</div>
<div class="lyrico-lyrics-wrapper">En nejam kaatura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nejam kaatura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandha suthum kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandha suthum kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna rendaa odaikkudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna rendaa odaikkudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa ninna kaadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa ninna kaadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla nandaa tholaikudhaeae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulla nandaa tholaikudhaeae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aeaeae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aeaeae ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aeaeae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aeaeae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinam kotti theekkavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam kotti theekkavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru muttaal megamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru muttaal megamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna suthi vaazhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna suthi vaazhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kottaa kaagamaa paraivayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kottaa kaagamaa paraivayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parandhu povamaa maranamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parandhu povamaa maranamae"/>
</div>
<div class="lyrico-lyrics-wrapper">Marandhu povamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marandhu povamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppu kaathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppu kaathula"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu panneer kaalamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu panneer kaalamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tururu tutu tururu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tururu tutu tururu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tutu tururu tutu tururu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tutu tururu tutu tururu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tururu tutu tururu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tururu tutu tururu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tutu tururu tutu tururu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tutu tururu tutu tururu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tururu tutu tururu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tururu tutu tururu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tutu tururu tutu tururu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tutu tururu tutu tururu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tutu tururu tutu tururu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tutu tururu tutu tururu"/>
</div>
<div class="lyrico-lyrics-wrapper">Tutu tururu tutu tururu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tutu tururu tutu tururu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaa oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaa oooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaa oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaa oooooo"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Hmm hmm hmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm hmm hmm"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaa oooooo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaa oooooo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei"/>
</div>
</pre>
