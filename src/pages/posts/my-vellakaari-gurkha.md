---
title: "my vellakaari song lyrics"
album: "Gurkha"
artist: "Raj Aryan"
lyricist: "Vignesh Shivan"
director: "Sam Anton"
path: "/albums/gurkha-lyrics"
song: "My Vellakaari"
image: ../../images/albumart/gurkha.jpg
date: 2019-07-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/guRFaVRB_7M"
type: "happy"
singers:
  - G.V. Prakash
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Love Is Pure In Your Eyes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Is Pure In Your Eyes"/>
</div>
<div class="lyrico-lyrics-wrapper">I Can See Me Flying High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Can See Me Flying High"/>
</div>
<div class="lyrico-lyrics-wrapper">Come With Me Hold My Hands
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come With Me Hold My Hands"/>
</div>
<div class="lyrico-lyrics-wrapper">We Can Dream Our Life In Sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Can Dream Our Life In Sky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">You Are Very Very Beautiful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are Very Very Beautiful"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Very Very Wonderful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Very Very Wonderful"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vaippen Endhan Kannukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vaippen Endhan Kannukkul"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Ukkaru En Nenjukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Ukkaru En Nenjukkul"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhaya Gate-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaya Gate-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Safe-ah Pooti Vecha Gurkha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Safe-ah Pooti Vecha Gurkha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Unakkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Unakkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ppen Panni Vechitaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ppen Panni Vechitaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Foreign Soap-ah Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Foreign Soap-ah Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart-ah Washing Senjitaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heart-ah Washing Senjitaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Showeril Daily
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Showeril Daily"/>
</div>
<div class="lyrico-lyrics-wrapper">Kulikka Mudivu Panitaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kulikka Mudivu Panitaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Vellakkariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Vellakkariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa Ginnunu Irukiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Ginnunu Irukiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Morattu Single-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Morattu Single-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Saachiputtiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Saachiputtiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Vellakkariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Vellakkariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Nachunnu Irukiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Nachunnu Irukiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Gurkha Manasathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Gurkha Manasathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Oorga Pottiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Oorga Pottiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maam Maam Maam Maam Maam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maam Maam Maam Maam Maam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maam Maam Maam Maam Maam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maam Maam Maam Maam Maam"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Parakkum Zoinnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Parakkum Zoinnn"/>
</div>
<div class="lyrico-lyrics-wrapper">Maam Maam Maam Maam Maam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maam Maam Maam Maam Maam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maam Maam Maam Maam Maam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maam Maam Maam Maam Maam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eppadi Parakkum Zoinnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Parakkum Zoinnn"/>
</div>
<div class="lyrico-lyrics-wrapper">Maam Maam Maam Maam Maam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maam Maam Maam Maam Maam"/>
</div>
<div class="lyrico-lyrics-wrapper">Maam Maam Maam Maam Maam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maam Maam Maam Maam Maam"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppadi Parakkum Zoinnn
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppadi Parakkum Zoinnn"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">English Tution Inaiyilirunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="English Tution Inaiyilirunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Poga Porenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poga Porenae"/>
</div>
<div class="lyrico-lyrics-wrapper">English Patta Mattumthaanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="English Patta Mattumthaanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekka Porenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekka Porenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fair And Lovely Thadava Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fair And Lovely Thadava Poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Burger Pizaa Nu Naan Maara Poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Burger Pizaa Nu Naan Maara Poren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edho Oru Edge La Irunthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edho Oru Edge La Irunthu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kannu Munna Yen Vandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kannu Munna Yen Vandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pola Oruthanuku Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pola Oruthanuku Ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Love Mela Oru Nambikkai Thantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Mela Oru Nambikkai Thantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizza La Cheese Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizza La Cheese Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa Maa Otti-irukkalaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Maa Otti-irukkalaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">My Vellakkariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Vellakkariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Summa Ginnunu Irukiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summa Ginnunu Irukiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Morattu Single-ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Morattu Single-ah"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Saachiputtiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Saachiputtiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">My Vellakkariyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My Vellakkariyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nalla Nachunnu Irukiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nalla Nachunnu Irukiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha Gurkha Manasathaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha Gurkha Manasathaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Oorga Pottiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Oorga Pottiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Love Is Pure In Your Eyes
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love Is Pure In Your Eyes"/>
</div>
<div class="lyrico-lyrics-wrapper">You Are Very Very Beautiful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You Are Very Very Beautiful"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Can See Me Flying High
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Can See Me Flying High"/>
</div>
<div class="lyrico-lyrics-wrapper">I Am Very Very Wonderful
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Am Very Very Wonderful"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come With Me Hold My Hands
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come With Me Hold My Hands"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Vaippen Enthan Kannukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Vaippen Enthan Kannukkul"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">We Can Dream Our Life In Sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We Can Dream Our Life In Sky"/>
</div>
<div class="lyrico-lyrics-wrapper">Vanthu Ukkaru En Nenjukkul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanthu Ukkaru En Nenjukkul"/>
</div>
</pre>
