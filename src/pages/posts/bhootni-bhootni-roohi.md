---
title: "bhootni bhootni song lyrics"
album: "Roohi"
artist: "Sachin-Jigar"
lyricist: "Amitabh Bhattacharya"
director: "Hardik Mehta"
path: "/albums/roohi-lyrics"
song: "Bhootni Bhootni"
image: ../../images/albumart/roohi.jpg
date: 2021-03-11
lang: hindi
youtubeLink: "https://www.youtube.com/embed/QYu0u4nkzKY"
type: "mass"
singers:
  - Mika Singh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Haye kya mera lena dena manmohini se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haye kya mera lena dena manmohini se"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho.. Kya mera lena dena manmohini se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho.. Kya mera lena dena manmohini se"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine bhootni bhootni bhootni ko dil diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine bhootni bhootni bhootni ko dil diya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laanat huzoor hirni pe morni pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laanat huzoor hirni pe morni pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Laanat huzoor hirni pe morni pe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laanat huzoor hirni pe morni pe"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine bhootni bhootni bhootni ko dil diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine bhootni bhootni bhootni ko dil diya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho kiya hai khud ko raazi agli puranmasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho kiya hai khud ko raazi agli puranmasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aane ka yaaron main wait karunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aane ka yaaron main wait karunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Banegi prem kahaani milegi bhooton ki raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banegi prem kahaani milegi bhooton ki raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Main usse date karunga haan haan haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main usse date karunga haan haan haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kiya hai khud ko raazi agli puranmasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kiya hai khud ko raazi agli puranmasi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aane ka yaaron main wait karunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aane ka yaaron main wait karunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Banegi prem kahaani milegi bhooton ki raani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Banegi prem kahaani milegi bhooton ki raani"/>
</div>
<div class="lyrico-lyrics-wrapper">Main usse date karunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main usse date karunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tanka bhida haye haye haye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanka bhida haye haye haye"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanka bhida hai daayan heroini se se se se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanka bhida hai daayan heroini se se se se"/>
</div>
<div class="lyrico-lyrics-wrapper">Tanka bhida hai daayan heroini se
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tanka bhida hai daayan heroini se"/>
</div>
<div class="lyrico-lyrics-wrapper">Maine bhootni bhootni bhootni ko dil diya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maine bhootni bhootni bhootni ko dil diya"/>
</div>
</pre>
