---
title: "nada da raja song lyrics"
album: "Vellaiilla Pattadhari 2"
artist: "Sean Roldan"
lyricist: "Dhanush - Yogi B"
director: "Soundarya Rajinikanth"
path: "/albums/vellaiilla-pattadhari-2-lyrics"
song: "Nada Da Raja"
image: ../../images/albumart/vellaiilla-pattadhari-2.jpg
date: 2017-11-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ZAiiTgfNXk8"
type: "Mass Intro"
singers:
  - Dhanush
  - Yogi B
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Watch out amul babies
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Watch out amul babies"/>
</div>
<div class="lyrico-lyrics-wrapper">It is raguvaran back again
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="It is raguvaran back again"/>
</div>
<div class="lyrico-lyrics-wrapper">I am no afraid of pain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am no afraid of pain"/>
</div>
<div class="lyrico-lyrics-wrapper">Hit it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hit it"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutti karanam adi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutti karanam adi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vagidu edutha mudi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vagidu edutha mudi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un motta maadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un motta maadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">See with tholai nokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="See with tholai nokki"/>
</div>
<div class="lyrico-lyrics-wrapper">Stars shine and bright varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Stars shine and bright varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai nokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai nokki"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthumaigal padaippaan uzhaippaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthumaigal padaippaan uzhaippaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraadi vendraan Vip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadi vendraan Vip"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">V I P…vip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="V I P…vip"/>
</div>
<div class="lyrico-lyrics-wrapper">Nada da raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nada da raja"/>
</div>
<div class="lyrico-lyrics-wrapper">You see my sweat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You see my sweat"/>
</div>
<div class="lyrico-lyrics-wrapper">But you know how is works that out
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="But you know how is works that out"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Illa edhu da raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Illa edhu da raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Take control
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Take control"/>
</div>
<div class="lyrico-lyrics-wrapper">We are making our own destiny
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We are making our own destiny"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idi da raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi da raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittadha pudi da raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittadha pudi da raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan kodi aanaigalin balam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan kodi aanaigalin balam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum nadaiyil adhirum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum nadaiyil adhirum"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha nilam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha nilam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilaiyadu idhu aadukalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilaiyadu idhu aadukalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam poraligalin inam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam poraligalin inam"/>
</div>
<div class="lyrico-lyrics-wrapper">Puratchi da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puratchi da"/>
</div>
<div class="lyrico-lyrics-wrapper">Samugha asthivaaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Samugha asthivaaram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tamilan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamilan da"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazharchi engal dhaagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazharchi engal dhaagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Raghuvara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raghuvara"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanayam thanmanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanayam thanmanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poradu poriyanandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poradu poriyanandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiramai mattum podhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiramai mattum podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimirada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimirada"/>
</div>
<div class="lyrico-lyrics-wrapper">Padaigal ondru koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padaigal ondru koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimiru da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimiru da"/>
</div>
<div class="lyrico-lyrics-wrapper">Veriudan velaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veriudan velaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattadhari daa…
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattadhari daa…"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudhusa poroppom da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusa poroppom da"/>
</div>
<div class="lyrico-lyrics-wrapper">Palasa madhippom da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palasa madhippom da"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinusa nadappom da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinusa nadappom da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ketka aalilla da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ketka aalilla da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhenamum sirippom da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhenamum sirippom da"/>
</div>
<div class="lyrico-lyrics-wrapper">Valia rasippom da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valia rasippom da"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhedama olaippom da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhedama olaippom da"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooyvu namakkilla da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooyvu namakkilla da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ezhunthu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Get up and stand up for your rights
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Get up and stand up for your rights"/>
</div>
<div class="lyrico-lyrics-wrapper">Erinthu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erinthu vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muyandru vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muyandru vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dont dont give up the fight
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dont dont give up the fight"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuninthu poraada vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuninthu poraada vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Elundhu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elundhu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha ulagam unadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha ulagam unadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Erindhu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Erindhu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhan udamai unadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhan udamai unadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Muandru vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muandru vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Varum vidiyal namadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varum vidiyal namadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunindhu poradada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunindhu poradada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">V I P
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="V I P"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Amul baby money bring a lot of enemies
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amul baby money bring a lot of enemies"/>
</div>
<div class="lyrico-lyrics-wrapper">We overcome tragic
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="We overcome tragic"/>
</div>
<div class="lyrico-lyrics-wrapper">And make opportunities
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="And make opportunities"/>
</div>
<div class="lyrico-lyrics-wrapper">Tea kada raja We are vip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tea kada raja We are vip"/>
</div>
<div class="lyrico-lyrics-wrapper">Vip…vip
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vip…vip"/>
</div>
</pre>
