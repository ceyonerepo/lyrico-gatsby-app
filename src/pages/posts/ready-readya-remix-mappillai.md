---
title: "ready readya remix song lyrics"
album: "Mappillai"
artist: "Mani Sharma"
lyricist: "Snehan"
director: "Suraj"
path: "/albums/mappillai-lyrics"
song: "Ready Readya Remix"
image: ../../images/albumart/mappillai.jpg
date: 2011-04-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/pR5feZpqk7o"
type: "love"
singers:
  - Ranjith
  - Saindhavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aadu puli aattam thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu puli aattam thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi pappoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi pappoma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadu inga jeichupudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu inga jeichupudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathuko maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathuko maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaha ini raa pagala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha ini raa pagala"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja leela than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja leela than"/>
</div>
<div class="lyrico-lyrics-wrapper">Aama ini thaeva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama ini thaeva illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veatti saela than aaann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veatti saela than aaann"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya aa aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya aa aan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadu puli aattam thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu puli aattam thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi pappoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi pappoma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadu inga jeichupudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu inga jeichupudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathuko maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathuko maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaha ini raa pagala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha ini raa pagala"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja leela than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja leela than"/>
</div>
<div class="lyrico-lyrics-wrapper">Aama ini thaeva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama ini thaeva illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veatti saela than aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veatti saela than aan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya aa aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya aa aan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothuva oru naalaikkingae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothuva oru naalaikkingae"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonu vela thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonu vela thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini mela sappuduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini mela sappuduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaru vela thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaru vela thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pothuva oru kattulukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothuva oru kattulukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Naalu kaalu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalu kaalu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnakkaaga pottirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnakkaaga pottirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhu kaalu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhu kaalu thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethana naala pattini potta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethana naala pattini potta"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethukudi kelappura enoda soota
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethukudi kelappura enoda soota"/>
</div>
<div class="lyrico-lyrics-wrapper">Aada poren naanum vaetta thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aada poren naanum vaetta thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kitta vaayendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kitta vaayendi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya aa aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya aa aan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadu puli aattam thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu puli aattam thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadi pappoma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadi pappoma"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadu inga jeichupudum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadu inga jeichupudum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paathuko maama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paathuko maama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaha ini raa pagala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaha ini raa pagala"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja leela than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja leela than"/>
</div>
<div class="lyrico-lyrics-wrapper">Aama ini thaeva illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aama ini thaeva illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Veatti saela than aann
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veatti saela than aann"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya aa aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya aa aan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puliya naan paaya poren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puliya naan paaya poren"/>
</div>
<div class="lyrico-lyrics-wrapper">Koochal podathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koochal podathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Patharamal neeyum irutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Patharamal neeyum irutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnum aagaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum aagaathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nijama naan kathinaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nijama naan kathinaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pavam paakaathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pavam paakaathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidinjaalum thappu illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidinjaalum thappu illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vittu pogathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vittu pogathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Guppunu yeruthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Guppunu yeruthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Odambula soodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odambula soodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodi katti parakkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi katti parakkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Maman moodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maman moodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachunu nee vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachunu nee vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Muthathai podendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muthathai podendi"/>
</div>
<div class="lyrico-lyrics-wrapper">En aasa pondatti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aasa pondatti"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya aa aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya aa aan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya aa aan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya aa aan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya readya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ready readya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ready readya"/>
</div>
<div class="lyrico-lyrics-wrapper">Readyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Readyaaa"/>
</div>
</pre>
