---
title: "rama rama song lyrics"
album: "Sehari"
artist: "Prashanth R Vihari"
lyricist: "Sanapati Bharadwaj Patrudu"
director: "Gnanasagar Dwaraka"
path: "/albums/sehari-lyrics"
song: "Rama Rama"
image: ../../images/albumart/sehari.jpg
date: 2022-02-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/k79YSaYkJpQ"
type: "happy"
singers:
  - Jassie Gift
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">O Rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammayao ammayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammayao ammayo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammayo evaree kurrollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammayo evaree kurrollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ammayo jathaga unnaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ammayo jathaga unnaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamari kosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamari kosam"/>
</div>
<div class="lyrico-lyrics-wrapper">Panuilu anne maanesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panuilu anne maanesi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanulu moosi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanulu moosi"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhookesaare rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhookesaare rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa Ravayya o rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Ravayya o rama"/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Boledanni seppaloi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boledanni seppaloi"/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Seppevi kadhanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seppevi kadhanta"/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayina gani septaale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayina gani septaale"/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkalu ravanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkalu ravanta"/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekkale vesthunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekkale vesthunte "/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhikkedhi ledhanta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhikkedhi ledhanta "/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhikkule chusthunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhikkule chusthunte "/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Kechalu ledanta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kechalu ledanta "/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Hechhule pothundhe 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hechhule pothundhe "/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkekki navvestha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkekki navvestha "/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Sakkaga muddhule 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sakkaga muddhule "/>
</div>
<div class="lyrico-lyrics-wrapper">chuttunte thippithunte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chuttunte thippithunte"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ooo iruvuri katha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooo iruvuri katha "/>
</div>
<div class="lyrico-lyrics-wrapper">irukula bontha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukula bontha"/>
</div>
<div class="lyrico-lyrics-wrapper">Savathula vale 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Savathula vale "/>
</div>
<div class="lyrico-lyrics-wrapper">saagenu santha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saagenu santha"/>
</div>
<div class="lyrico-lyrics-wrapper">Purushula gathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purushula gathi "/>
</div>
<div class="lyrico-lyrics-wrapper">kudithila padda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kudithila padda"/>
</div>
<div class="lyrico-lyrics-wrapper">Elkai poyinda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Elkai poyinda"/>
</div>
<div class="lyrico-lyrics-wrapper">Telavani pani suluvanukunna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telavani pani suluvanukunna"/>
</div>
<div class="lyrico-lyrics-wrapper">Kelike varike siluvayyana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kelike varike siluvayyana"/>
</div>
<div class="lyrico-lyrics-wrapper">Teliviga tega mosthu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teliviga tega mosthu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Saana hairana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saana hairana"/>
</div>
<div class="lyrico-lyrics-wrapper">Gandu cheemalalle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gandu cheemalalle "/>
</div>
<div class="lyrico-lyrics-wrapper">panchadara chuttu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panchadara chuttu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cherukunnaranta chusetattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cherukunnaranta chusetattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilakada eragani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilakada eragani "/>
</div>
<div class="lyrico-lyrics-wrapper">marakatha gallu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marakatha gallu"/>
</div>
<div class="lyrico-lyrics-wrapper">Orayyo gadusolley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orayyo gadusolley"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo Innavu kada rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo Innavu kada rama"/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Boledanni seppesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boledanni seppesa"/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Seppesina kuda 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seppesina kuda "/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Inkosari seppestha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inkosari seppestha "/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Ekkalu ravanta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkalu ravanta "/>
</div>
<div class="lyrico-lyrics-wrapper">o rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Lekkale vesindre 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lekkale vesindre "/>
</div>
<div class="lyrico-lyrics-wrapper">o rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhikkedhi ledanta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhikkedhi ledanta "/>
</div>
<div class="lyrico-lyrics-wrapper">o rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o rama rama"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkalenno chusindre
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkalenno chusindre"/>
</div>
<div class="lyrico-lyrics-wrapper">O rama rama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rama rama"/>
</div>
</pre>
