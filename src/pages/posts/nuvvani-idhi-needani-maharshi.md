---
title: "nuvvani idhi needani song lyrics"
album: "Maharshi"
artist: "Devi Sri Prasad"
lyricist: "Sri Mani"
director: "Vamshi Paidipally"
path: "/albums/maharshi-lyrics"
song: "Nuvvani Idhi Needani"
image: ../../images/albumart/maharshi.jpg
date: 2019-05-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/4dbiE-da82E"
type: "mass"
singers:
  - Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nuvvani idhi needhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvani idhi needhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi nizamani anukunnavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi nizamani anukunnavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhuga nuvvanukundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhuga nuvvanukundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi kaadhuga nuvvethikindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi kaadhuga nuvvethikindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhani badhuledhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhani badhuledhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka prashnala niluchunnava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka prashnala niluchunnava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalame venuthiraganidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalame venuthiraganidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivvadhu nuvvadiginadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvadhu nuvvadiginadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye veloo pattukuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye veloo pattukuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerchede nadakante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerchede nadakante"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontariga nerchaada yevadaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontariga nerchaada yevadaina"/>
</div>
<div class="lyrico-lyrics-wrapper">O saayam andhukoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O saayam andhukoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagedhey brathukante
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagedhey brathukante"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontariga brathikaada yevadaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontariga brathikaada yevadaina"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhuguru mecchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhuguru mecchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee aanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee aanandham"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee okkadidhainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee okkadidhainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu gelipinchina o chirunavve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu gelipinchina o chirunavve"/>
</div>
<div class="lyrico-lyrics-wrapper">Venukey dhaagenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venukey dhaagenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nuvvani idhi needhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvani idhi needhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi nizamani anukunnavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi nizamani anukunnavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhuga nuvvanukundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhuga nuvvanukundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi kaadhuga nuvvethikindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi kaadhuga nuvvethikindhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedhani badhuledhani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhani badhuledhani"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka prashnala niluchunnava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka prashnala niluchunnava"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalame venuthiraganidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalame venuthiraganidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivvadhu nuvvadiginadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivvadhu nuvvadiginadhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh oopiri mottham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh oopiri mottham"/>
</div>
<div class="lyrico-lyrics-wrapper">Uppenalaa pongindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uppenalaa pongindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee payanam malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee payanam malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotthaga modhalayyindhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotthaga modhalayyindhaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Innaallu aakasham aapesindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innaallu aakasham aapesindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa yetthe karigi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa yetthe karigi"/>
</div>
<div class="lyrico-lyrics-wrapper">Neley kanipinchindha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neley kanipinchindha"/>
</div>
<div class="lyrico-lyrics-wrapper">Gelupai o gelupai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gelupai o gelupai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee parugey poorthainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee parugey poorthainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gamyam migiley undhaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gamyam migiley undhaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Rammani ninu rammani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rammani ninu rammani"/>
</div>
<div class="lyrico-lyrics-wrapper">O snehame pilichindhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O snehame pilichindhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Yennadu ninu maruvanidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennadu ninu maruvanidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yeppudu ninu viduvanidhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeppudu ninu viduvanidhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh premani nee premani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh premani nee premani"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee kosame dhaachindhiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee kosame dhaachindhiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gundello guruthayyinadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gundello guruthayyinadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gaayamai mari vechinadhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gaayamai mari vechinadhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Lokaaley thalavanchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lokaaley thalavanchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninne keerthisthunnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninne keerthisthunnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv kore vijayam vere undha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv kore vijayam vere undha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gunde chappudukey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gunde chappudukey"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunama yedhantey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunama yedhantey"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv modhalayyina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv modhalayyina"/>
</div>
<div class="lyrico-lyrics-wrapper">Chotuni chupisthondha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chotuni chupisthondha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvvodhilesina ninnalaloki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvvodhilesina ninnalaloki"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugey saagenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugey saagenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuv saadhinchina santhoshaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuv saadhinchina santhoshaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ardham thelisenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ardham thelisenaa"/>
</div>
</pre>
