---
title: "boom boom song lyrics"
album: "Mirugaa"
artist: "Aruldev"
lyricist: "ARP Jayaraam"
director: "J.Parthiban"
path: "/albums/mirugaa-lyrics"
song: "Boom Boom"
image: ../../images/albumart/mirugaa.jpg
date: 2021-03-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/oUOjAlZYb2o"
type: "Mass"
singers:
  - Ranjith
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">I am a bad boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a bad boy"/>
</div>
<div class="lyrico-lyrics-wrapper">I am a bad boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a bad boy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">nan manusan illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan manusan illada"/>
</div>
<div class="lyrico-lyrics-wrapper">boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">nan kadavul illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan kadavul illada"/>
</div>
<div class="lyrico-lyrics-wrapper">boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">nan peyum illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan peyum illada"/>
</div>
<div class="lyrico-lyrics-wrapper">mirugam thanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mirugam thanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">nan suthu vadhu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan suthu vadhu da"/>
</div>
<div class="lyrico-lyrics-wrapper">boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">nan sutha asuran da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan sutha asuran da"/>
</div>
<div class="lyrico-lyrics-wrapper">boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">nan ratha rasigan da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan ratha rasigan da"/>
</div>
<div class="lyrico-lyrics-wrapper">mirugam thanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mirugam thanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aatha mangatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatha mangatha"/>
</div>
<div class="lyrico-lyrics-wrapper">suthattam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthattam than"/>
</div>
<div class="lyrico-lyrics-wrapper">ulleyum velliyeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulleyum velliyeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vettai nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vettai nan"/>
</div>
<div class="lyrico-lyrics-wrapper">vanjana thati thookuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanjana thati thookuven"/>
</div>
<div class="lyrico-lyrics-wrapper">kai vachana vasamakuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai vachana vasamakuven"/>
</div>
<div class="lyrico-lyrics-wrapper">vaideila padam ootuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaideila padam ootuven"/>
</div>
<div class="lyrico-lyrics-wrapper">entha kolukum thani kattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha kolukum thani kattuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I am a bad boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a bad boy"/>
</div>
<div class="lyrico-lyrics-wrapper">paavam nan parpathillada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paavam nan parpathillada"/>
</div>
<div class="lyrico-lyrics-wrapper">paname en pavama pothuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paname en pavama pothuda"/>
</div>
<div class="lyrico-lyrics-wrapper">aasa patta adaiya venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasa patta adaiya venum da"/>
</div>
<div class="lyrico-lyrics-wrapper">adaiya venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adaiya venum da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">acham unna alikum ethiri da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="acham unna alikum ethiri da"/>
</div>
<div class="lyrico-lyrics-wrapper">sattam athu ketta valiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sattam athu ketta valiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">kolaiyum inga kutham illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kolaiyum inga kutham illada"/>
</div>
<div class="lyrico-lyrics-wrapper">kutham illada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kutham illada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">aatha mangatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aatha mangatha"/>
</div>
<div class="lyrico-lyrics-wrapper">suthattam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthattam than"/>
</div>
<div class="lyrico-lyrics-wrapper">ulleyum velliyeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulleyum velliyeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vetta nan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vetta nan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vanjana thathi thookuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanjana thathi thookuven"/>
</div>
<div class="lyrico-lyrics-wrapper">kai vachana vasamakuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kai vachana vasamakuven"/>
</div>
<div class="lyrico-lyrics-wrapper">vaideyile padam otuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaideyile padam otuven"/>
</div>
<div class="lyrico-lyrics-wrapper">entha kolukum thanni kattuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="entha kolukum thanni kattuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I am a bad boy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am a bad boy"/>
</div>
</pre>
