---
title: "paan banaras song lyrics"
album: "Sollividava"
artist: "Jassie Gift"
lyricist: "Pa. Vijay"
director: "Arjun Sarja"
path: "/albums/sollividava-lyrics"
song: "Paan Banaras"
image: ../../images/albumart/sollividava.jpg
date: 2018-02-09
lang: tamil
youtubeLink: "https://www.youtube.com/embed/N-2mHaG_aWs"
type: "happy"
singers:
  - Karthik
  - Priya Himesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paanu banarasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanu banarasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Meri sapnoki rani kabhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meri sapnoki rani kabhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paanu banarasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanu banarasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapnoki rani kabhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapnoki rani kabhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hare paanu banarasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hare paanu banarasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapnoki rani kabhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapnoki rani kabhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hare paanu banarasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hare paanu banarasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapnoki rani kabhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapnoki rani kabhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puthu kickku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu kickku"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu looku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu looku"/>
</div>
<div class="lyrico-lyrics-wrapper">Life ingae picknikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life ingae picknikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Beautiful graphickku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beautiful graphickku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hare paanu banarasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hare paanu banarasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapnoki rani kabhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapnoki rani kabhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hare paanu banarasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hare paanu banarasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapnoki rani kabhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapnoki rani kabhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnugalae eppothum dangeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnugalae eppothum dangeru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachikku nachikku nachikku nachikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachikku nachikku nachikku nachikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Summavae seivanga torture-u
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summavae seivanga torture-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachikku nachikku nachikku nachikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachikku nachikku nachikku nachikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannilae nee paaru laseru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannilae nee paaru laseru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachikku nachikku nachikku nachikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachikku nachikku nachikku nachikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Thallipoo illena puncheru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thallipoo illena puncheru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachikku nachikku nachikku nachikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachikku nachikku nachikku nachikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kovam yenma julietu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kovam yenma julietu"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam sirichi pallakaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam sirichi pallakaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Loosa nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Loosa nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Cracka nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cracka nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Manga madaiyan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manga madaiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Moonja paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moonja paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandai kulla mannu seru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandai kulla mannu seru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh sanda vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh sanda vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Soundu vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soundu vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Jackie chaan-ah maara vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jackie chaan-ah maara vendam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannala suda vendam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannala suda vendam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paanu banarasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paanu banarasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapnoki rani kabhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapnoki rani kabhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey "/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hare paanu banarasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hare paanu banarasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapnoki rani kabhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapnoki rani kabhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Booouuuvaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Booouuuvaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa ahaaahaaa heyyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa ahaaahaaa heyyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaa ahaaahaaa heyyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaa ahaaahaaa heyyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei orappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei orappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei shabba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei shabba"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei orappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei orappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei shabbaheyyyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei shabbaheyyyyy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vazhkaiyae jooraana circusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhkaiyae jooraana circusu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachikku nachikku nachikku nachikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachikku nachikku nachikku nachikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellarukkum ullathoru businessu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellarukkum ullathoru businessu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachikku nachikku nachikku nachikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachikku nachikku nachikku nachikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vendamae manasukulla tensionu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vendamae manasukulla tensionu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachikku nachikku nachikku nachikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachikku nachikku nachikku nachikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kushiya irunthalae successu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kushiya irunthalae successu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachikku nachikku nachikku nachikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachikku nachikku nachikku nachikku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeni podu vaanathikku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeni podu vaanathikku"/>
</div>
<div class="lyrico-lyrics-wrapper">Veli yethukku kanavukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veli yethukku kanavukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Jeena yahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeena yahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Marna yahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marna yahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iske siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iske siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaana kahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaana kahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iske siva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iske siva"/>
</div>
<div class="lyrico-lyrics-wrapper">Jaana kahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaana kahan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chandha mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandha mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Chandha mama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chandha mama"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma life-eh oru cinema
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma life-eh oru cinema"/>
</div>
<div class="lyrico-lyrics-wrapper">Beautiful hungamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Beautiful hungamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hare paanu banarasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hare paanu banarasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapnoki rani kabhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapnoki rani kabhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hare paanu banarasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hare paanu banarasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapnoki rani kabhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapnoki rani kabhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Hoi Hey hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hoi Hey hey"/>
</div>
</pre>
