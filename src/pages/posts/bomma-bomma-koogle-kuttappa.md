---
title: "bomma bomma song lyrics"
album: "Koogle Kuttappa"
artist: "Ghibran"
lyricist: "Arivu"
director: "Sabari – Saravanan"
path: "/albums/koogle-kuttappa-lyrics"
song: "Bomma Bomma"
image: ../../images/albumart/koogle-kuttappa.jpg
date: 2022-05-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/S0jTgGa4kaw"
type: "happy"
singers:
  - Arivu
  - Sivaangi Krishnakumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma Bommaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma Bommaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma Robot Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma Robot Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma Robot Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma Robot Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Current Tu Kambi Robo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Current Tu Kambi Robo Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Galagalapaakum Pogo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Galagalapaakum Pogo Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Porandha Veedu Labo Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porandha Veedu Labo Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuru Thuru Hoo Laa Laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuru Thuru Hoo Laa Laa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Irandu Kaigal Equation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irandu Kaigal Equation"/>
</div>
<div class="lyrico-lyrics-wrapper">Minu Minu Kangal Rotation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minu Minu Kangal Rotation"/>
</div>
<div class="lyrico-lyrics-wrapper">Kurumbu Kaara Relation
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kurumbu Kaara Relation"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh No No Emotion
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh No No Emotion"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey ABCD 5G Daddy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey ABCD 5G Daddy"/>
</div>
<div class="lyrico-lyrics-wrapper">Odi Vandhan En Kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odi Vandhan En Kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nano Tech Il Style Ah Little
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nano Tech Il Style Ah Little"/>
</div>
<div class="lyrico-lyrics-wrapper">Human Robo Oravaada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Human Robo Oravaada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Battery Life
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Battery Life"/>
</div>
<div class="lyrico-lyrics-wrapper">Ten Percent
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ten Percent"/>
</div>
<div class="lyrico-lyrics-wrapper">Shock Adicha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shock Adicha"/>
</div>
<div class="lyrico-lyrics-wrapper">Bro No Tension
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bro No Tension"/>
</div>
<div class="lyrico-lyrics-wrapper">Time Irundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Irundha"/>
</div>
<div class="lyrico-lyrics-wrapper">Have Some Fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Have Some Fun"/>
</div>
<div class="lyrico-lyrics-wrapper">Come Dance With
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come Dance With"/>
</div>
<div class="lyrico-lyrics-wrapper">Koogle Kuttappan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koogle Kuttappan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttappa Kuttappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttappa Kuttappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koogle Kuttappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koogle Kuttappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttappa Kuttappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttappa Kuttappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koogle Kuttappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koogle Kuttappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma Robot Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma Robot Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma Robot Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma Robot Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pap Papa Papbaaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap Papa Papbaaba"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap Papa Papbaaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap Papa Papbaaba"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap Papa Papbaaba Pap Aabbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap Papa Papbaaba Pap Aabbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chamatha Sonna Pechelam Ketkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chamatha Sonna Pechelam Ketkuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Weight Ellam Thookudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Weight Ellam Thookudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Cycle Ottothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cycle Ottothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh Robo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Robo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanakka Oru Coffee Ya Pottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanakka Oru Coffee Ya Pottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sofia Neettuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sofia Neettuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sapdungo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sapdungo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
24×7 <div class="lyrico-lyrics-wrapper">Nee Online
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Online"/>
</div>
<div class="lyrico-lyrics-wrapper">So Enakkenna Kavalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="So Enakkenna Kavalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Okay Fine
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Okay Fine"/>
</div>
<div class="lyrico-lyrics-wrapper">Silandhikoru Spider man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Silandhikoru Spider man"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Kolandhaiku Peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Kolandhaiku Peru"/>
</div>
<div class="lyrico-lyrics-wrapper">Cyber Man
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cyber Man"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bedham Kidayadhu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bedham Kidayadhu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Robo Kannuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Robo Kannuku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Mudiyadhu Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Mudiyadhu Da"/>
</div>
<div class="lyrico-lyrics-wrapper">Indha Boomi Pandhukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indha Boomi Pandhukku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kuttappa Kuttappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttappa Kuttappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koogle Kuttappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koogle Kuttappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuttappa Kuttappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuttappa Kuttappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Koogle Kuttappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koogle Kuttappa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pap Papa Papbaaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap Papa Papbaaba"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap Papa Papbaaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap Papa Papbaaba"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap Papa Papbaaba Pap Aabbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap Papa Papbaaba Pap Aabbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma Robot Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma Robot Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pap Papa Papbaaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap Papa Papbaaba"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap Papa Papbaaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap Papa Papbaaba"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap Papa Papbaaba Pap Aabbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap Papa Papbaaba Pap Aabbaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma"/>
</div>
<div class="lyrico-lyrics-wrapper">Bomma Bomma Bomma Bomma Robot Da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bomma Bomma Bomma Bomma Robot Da"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pap Papa Papbaaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap Papa Papbaaba"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap Papa Papbaaba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap Papa Papbaaba"/>
</div>
<div class="lyrico-lyrics-wrapper">Pap Papa Papbaaba Pap Aabbaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pap Papa Papbaaba Pap Aabbaa"/>
</div>
</pre>
