---
title: "idhu thaana song lyrics"
album: "Saamy"
artist: "Harris Jayaraj "
lyricist: "Thamarai"
director: "Hari"
path: "/albums/saamy-song-lyrics"
song: "Idhu Thaana"
image: ../../images/albumart/saamy.jpg
date: 2003-05-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/EBrZOleZlpM"
type: "Love"
singers:
  - K. S. Chithra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhuthaana Idhuthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthaana Idhuthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethir Paartha Annaalum Idhuthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paartha Annaalum Idhuthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Thaana Ivan Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Thaana Ivan Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Malar Soottum Manavaalan Ivan Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar Soottum Manavaalan Ivan Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pagalilum Naan Kanda Kanavugal Nenavaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalilum Naan Kanda Kanavugal Nenavaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathaanen Naan Unathaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathaanen Naan Unathaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumana Naal Enni Nagarnthidum En Naattkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumana Naal Enni Nagarnthidum En Naattkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugaamana Oru Sumaiyaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugaamana Oru Sumaiyaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithazh Pirikkaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithazh Pirikkaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Kural Ezhuppaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kural Ezhuppaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Enakkaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enakkaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Paadal Paadi Kolven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Paadal Paadi Kolven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhuthana Idhuthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhuthana Idhuthana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethir Paartha Annaalum Idhuthaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paartha Annaalum Idhuthaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Thaana Ivan Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Thaana Ivan Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Malar Soottum Manavaalan Ivan Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar Soottum Manavaalan Ivan Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inimel Veetil Thinamum Nadakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inimel Veetil Thinamum Nadakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadagam Inithidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadagam Inithidume"/>
</div>
<div class="lyrico-lyrics-wrapper">Olinthidum Enaiye Unathu Vizhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Olinthidum Enaiye Unathu Vizhigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Thediye Azhainthidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thediye Azhainthidume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maadiyin Valaivinil Ennai Kandu Pidippaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maadiyin Valaivinil Ennai Kandu Pidippaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkaathavan Pol Sirappaai Nadippaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkaathavan Pol Sirappaai Nadippaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidumena Thirumbi En Idai Valaippaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidumena Thirumbi En Idai Valaippaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padigalin Adiyinil Ennai Alli Anaippaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padigalin Adiyinil Ennai Alli Anaippaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Achangalum Achapattu Marainthidumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achangalum Achapattu Marainthidumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetkkagalum Vetkka Pattu Olinthidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetkkagalum Vetkka Pattu Olinthidume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithuthana Ithuthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithuthana Ithuthana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethir Paartha Annaalum Idhuthana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethir Paartha Annaalum Idhuthana"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan Thaana Ivan Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan Thaana Ivan Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Malar Soottum Manavaalan Ivan Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar Soottum Manavaalan Ivan Thaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Niyayiru Mathiyam Samayal Unathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niyayiru Mathiyam Samayal Unathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Virumbi Nee Samaithiduvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virumbi Nee Samaithiduvaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vedikkai Paar Ena Ennai Amarthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vedikkai Paar Ena Ennai Amarthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunigalum Thuvaithiduvaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunigalum Thuvaithiduvaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oorukkul Anaivarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oorukkul Anaivarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Kandu Nadunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Kandu Nadunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Veetinil Nee Oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veetinil Nee Oru"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuzhanthaiyaai Sinunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuzhanthaiyaai Sinunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Perumaiyil En Mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perumaiyil En Mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Minuminukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Minuminukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravarin Ulagamum Iruvari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravarin Ulagamum Iruvari"/>
</div>
<div class="lyrico-lyrics-wrapper">Surunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Magizhchiyil Enthan Manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magizhchiyil Enthan Manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Malarnthidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malarnthidume"/>
</div>
<div class="lyrico-lyrics-wrapper">En Uyaramo Innum Konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Uyaramo Innum Konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarnthidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarnthidume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Malar Soottum Manavaalan Ivan Thaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malar Soottum Manavaalan Ivan Thaana"/>
</div>
<div class="lyrico-lyrics-wrapper">Pagalilum Naan Kanda Kanavugal Nenavaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pagalilum Naan Kanda Kanavugal Nenavaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Unathaanen Naan Unathaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unathaanen Naan Unathaanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumana Naal Enni Nagarnthidum En Naattkal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumana Naal Enni Nagarnthidum En Naattkal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugaamana Oru Sumaiyaanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugaamana Oru Sumaiyaanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ithazh Pirikkaamal Kural Ezhuppaamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithazh Pirikkaamal Kural Ezhuppaamal"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Enakkaana Oru Paadal Paadi Kolven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Enakkaana Oru Paadal Paadi Kolven"/>
</div>
</pre>
