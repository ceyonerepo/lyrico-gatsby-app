---
title: "uthiranae nee song lyrics"
album: "7"
artist: "Chaitan Bharadwaj"
lyricist: "Niranjan Bharathi"
director: "Nizar Shafi"
path: "/albums/7-song-lyrics"
song: "Uthiranae Nee"
image: ../../images/albumart/7.jpg
date: 2019-06-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/NWVk4zS9ZUc"
type: "love"
singers:
  - Haricharan
  - Alisha Thomas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">uthirane nee thodugaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uthirane nee thodugaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">paniyena naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paniyena naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">urugi valiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urugi valiya"/>
</div>
<div class="lyrico-lyrics-wrapper">udhayane pon nerupu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udhayane pon nerupu"/>
</div>
<div class="lyrico-lyrics-wrapper">ena elunthu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena elunthu vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">iniyane un viliganin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iniyane un viliganin"/>
</div>
<div class="lyrico-lyrics-wrapper">kalavaram kandu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalavaram kandu"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayam kulaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayam kulaiya"/>
</div>
<div class="lyrico-lyrics-wrapper">aniyane ne puyal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aniyane ne puyal "/>
</div>
<div class="lyrico-lyrics-wrapper">ena paranthu vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena paranthu vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">vilunthidum adai malaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vilunthidum adai malaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">sugam nilam maruthiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugam nilam maruthiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">paravidum pala viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paravidum pala viral"/>
</div>
<div class="lyrico-lyrics-wrapper">isai kulal veruthiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isai kulal veruthiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">kadal alai karam thodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal alai karam thodum"/>
</div>
<div class="lyrico-lyrics-wrapper">athai karai niruthiduma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athai karai niruthiduma"/>
</div>
<div class="lyrico-lyrics-wrapper">mayangiye vanthu kadal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mayangiye vanthu kadal"/>
</div>
<div class="lyrico-lyrics-wrapper">udal idham anaithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal idham anaithidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manamo elume magilchiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manamo elume magilchiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">ucham thodume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ucham thodume"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaiyena ethuvumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaiyena ethuvumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">idaividamale manam kondadume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaividamale manam kondadume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennavane en sagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennavane en sagalam"/>
</div>
<div class="lyrico-lyrics-wrapper">agilam aanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="agilam aanai"/>
</div>
<div class="lyrico-lyrics-wrapper">valiyai sugamai unarthidum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valiyai sugamai unarthidum "/>
</div>
<div class="lyrico-lyrics-wrapper">pon valvinai tharugirai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pon valvinai tharugirai"/>
</div>
<div class="lyrico-lyrics-wrapper">epoluthum endhan nagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="epoluthum endhan nagam"/>
</div>
<div class="lyrico-lyrics-wrapper">perum sugam pidikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perum sugam pidikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ipoluthum undhan viral
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipoluthum undhan viral"/>
</div>
<div class="lyrico-lyrics-wrapper">thodum varam kidaikirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thodum varam kidaikirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ninaithathu nodiyinil nijam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ninaithathu nodiyinil nijam"/>
</div>
<div class="lyrico-lyrics-wrapper">ena nigalkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ena nigalkirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">idhayamo varayarai vidai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhayamo varayarai vidai"/>
</div>
<div class="lyrico-lyrics-wrapper">pera magilthidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pera magilthidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">manamo elume magilchiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manamo elume magilchiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">ucham thodume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ucham thodume"/>
</div>
<div class="lyrico-lyrics-wrapper">thadaiyena ethuvumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thadaiyena ethuvumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">idaividamale manam kondadume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idaividamale manam kondadume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ini varum ennalum anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ini varum ennalum anbe"/>
</div>
<div class="lyrico-lyrics-wrapper">ne valthida nan valthida vendume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne valthida nan valthida vendume"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuve pothume endha naalilume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuve pothume endha naalilume"/>
</div>
</pre>
