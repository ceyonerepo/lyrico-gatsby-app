---
title: "o amma song lyrics"
album: "Kathanam"
artist: "Roshan Salur"
lyricist: "Rehman"
director: "Rajesh Nadendla"
path: "/albums/kathanam-lyrics"
song: "O Amma"
image: ../../images/albumart/kathanam.jpg
date: 2019-08-09
lang: telugu
youtubeLink: "https://www.youtube.com/embed/FO8qFiTFxKE"
type: "happy"
singers:
  - Kaala Bhairava
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Cheekati kondallona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheekati kondallona"/>
</div>
<div class="lyrico-lyrics-wrapper">Thurupu nuvvenamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thurupu nuvvenamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Gudise gundellona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gudise gundellona"/>
</div>
<div class="lyrico-lyrics-wrapper">Merupu nuvvenammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Merupu nuvvenammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pilavagane palukulhava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pilavagane palukulhava"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa kosamochchina devathave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa kosamochchina devathave"/>
</div>
<div class="lyrico-lyrics-wrapper">Amanu minchina ammavu neevai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Amanu minchina ammavu neevai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneellu thudichave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneellu thudichave"/>
</div>
<div class="lyrico-lyrics-wrapper">O ammaa aravindammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O ammaa aravindammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi ea janna runamammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi ea janna runamammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">O ammaa aravindammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O ammaa aravindammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaku marujanma nuvenammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaku marujanma nuvenammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ea sayamantoo leka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ea sayamantoo leka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ea okkarainaa raaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ea okkarainaa raaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vunname dhenanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vunname dhenanga"/>
</div>
<div class="lyrico-lyrics-wrapper">A eandamaavullanti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A eandamaavullanti"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa gunde baavullonaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa gunde baavullonaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimpave nuvvu gangaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimpave nuvvu gangaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanupapake o reppa neevai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanupapake o reppa neevai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaasinaave Kaapalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaasinaave Kaapalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee choopulo pasipaapalaagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee choopulo pasipaapalaagaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Velaganee maa bathukilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velaganee maa bathukilaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O ammaa aravindammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O ammaa aravindammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi ea janna runamammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi ea janna runamammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">O ammaa aravindammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O ammaa aravindammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaku marujanma nuvenammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaku marujanma nuvenammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O ammaa aravindammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O ammaa aravindammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi ea janna runamammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi ea janna runamammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">O ammaa aravindammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O ammaa aravindammaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maaku marujanma nuvenammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaku marujanma nuvenammaa"/>
</div>
</pre>
