---
title: "dhan dhan song lyrics"
album: "F2 Fun and Frustration"
artist: "Devi Sri Prasad"
lyricist: "Kasarla Shyam"
director: "Anil Ravipudi"
path: "/albums/f2-fun-and-frustration-lyrics"
song: "Dhan Dhan"
image: ../../images/albumart/f2-fun-and-frustration.jpg
date: 2019-01-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/rIQlde1Rrbs"
type: "happy"
singers:
  - Hemachandra
  - Sravana Bhargavi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Make up esi tudichinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Make up esi tudichinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Whatsapp DP marchinattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Whatsapp DP marchinattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Etta nanne odilestavey harika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etta nanne odilestavey harika"/>
</div>
<div class="lyrico-lyrics-wrapper">Aapaka pothe engagementu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aapaka pothe engagementu"/>
</div>
<div class="lyrico-lyrics-wrapper">Full bottle pai petti ottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full bottle pai petti ottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ille peeki vestam tentu choodika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ille peeki vestam tentu choodika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Made in India ni odileyoddhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Made in India ni odileyoddhey"/>
</div>
<div class="lyrico-lyrics-wrapper">China piece ni nammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="China piece ni nammi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattina thalini teseyodhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattina thalini teseyodhe"/>
</div>
<div class="lyrico-lyrics-wrapper">cheppaleda me mummy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheppaleda me mummy"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Harika pattavey opika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harika pattavey opika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Harika pattavey opika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harika pattavey opika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">First love ne maravodhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="First love ne maravodhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Na mata vinavey olammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na mata vinavey olammi"/>
</div>
<div class="lyrico-lyrics-wrapper">Second hand vi aypotavey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Second hand vi aypotavey"/>
</div>
<div class="lyrico-lyrics-wrapper">Jara agavey online rammi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jara agavey online rammi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oo honey champake premani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo honey champake premani"/>
</div>
<div class="lyrico-lyrics-wrapper">Oo honey champake premani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oo honey champake premani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jarugutunte celebration
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jarugutunte celebration"/>
</div>
<div class="lyrico-lyrics-wrapper">veyyalaney Venky aasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veyyalaney Venky aasan"/>
</div>
<div class="lyrico-lyrics-wrapper">How to control my Frustration
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How to control my Frustration"/>
</div>
<div class="lyrico-lyrics-wrapper">Perugutunade ma tension
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perugutunade ma tension"/>
</div>
<div class="lyrico-lyrics-wrapper">Chudalemey inko function
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chudalemey inko function"/>
</div>
<div class="lyrico-lyrics-wrapper">Dharna chestham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dharna chestham"/>
</div>
<div class="lyrico-lyrics-wrapper">Jam aypodhi junction junction
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jam aypodhi junction junction"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhan dhan dhan dhan dhan dhana dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhan dhan dhan dhan dhan dhana dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadinchestam metho temple run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadinchestam metho temple run"/>
</div>
<div class="lyrico-lyrics-wrapper">Yo yo yo yo yo yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo yo yo yo yo yo"/>
</div>
<div class="lyrico-lyrics-wrapper">Makemo full fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makemo full fun"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayyayyayyo mekey frustration
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyayyayyo mekey frustration"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mogani kosam Yammuni saitham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogani kosam Yammuni saitham"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanadu aapevarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanadu aapevarey"/>
</div>
<div class="lyrico-lyrics-wrapper">Jungle ki ayna Dangal ki ayna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jungle ki ayna Dangal ki ayna"/>
</div>
<div class="lyrico-lyrics-wrapper">Pellalu oche varey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pellalu oche varey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Harika jaldi raa ika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Harika jaldi raa ika"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Go to hell ani baitiki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Go to hell ani baitiki "/>
</div>
<div class="lyrico-lyrics-wrapper">tosina mogadivi nuvve ro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tosina mogadivi nuvve ro"/>
</div>
<div class="lyrico-lyrics-wrapper">Come home ani kanneellu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come home ani kanneellu "/>
</div>
<div class="lyrico-lyrics-wrapper">edithe malli etlostaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edithe malli etlostaro"/>
</div>
<div class="lyrico-lyrics-wrapper">My dear pettakoyi flower
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="My dear pettakoyi flower"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Laggalanni swargalloney 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Laggalanni swargalloney "/>
</div>
<div class="lyrico-lyrics-wrapper">munde rasincharey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="munde rasincharey"/>
</div>
<div class="lyrico-lyrics-wrapper">Rubber thoti nuditi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rubber thoti nuditi "/>
</div>
<div class="lyrico-lyrics-wrapper">ratani getla cheripestarey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ratani getla cheripestarey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O Honey Champake pramani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O Honey Champake pramani"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Left leg tho peetalu thanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Left leg tho peetalu thanni"/>
</div>
<div class="lyrico-lyrics-wrapper">Straight ga vellav kadaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Straight ga vellav kadaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Chatu ga ochi na kaalley patti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chatu ga ochi na kaalley patti"/>
</div>
<div class="lyrico-lyrics-wrapper">Begging chestavero
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Begging chestavero"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thella thouluga inthati pogaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thella thouluga inthati pogaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillanu iyyara maku inkevaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillanu iyyara maku inkevaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Head weight tho O mastaro
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Head weight tho O mastaro"/>
</div>
<div class="lyrico-lyrics-wrapper">Virra veegite Migiledi beeru baaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virra veegite Migiledi beeru baaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhan dhan dhan dhan dhan dhana dhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhan dhan dhan dhan dhan dhana dhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadinchestam metho temple run
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadinchestam metho temple run"/>
</div>
<div class="lyrico-lyrics-wrapper">Yo yo yo yo yo yo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yo yo yo yo yo yo"/>
</div>
<div class="lyrico-lyrics-wrapper">Makemo full fun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Makemo full fun"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyayyayyayyo mekey frustration
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyayyayyayyo mekey frustration"/>
</div>
</pre>
