---
title: "yelamala song lyrics"
album: "Kazhugu 2"
artist: "Yuvan Shankar Raja"
lyricist: "Pa. Vijay"
director: "Sathyasiva"
path: "/albums/kazhugu-2-lyrics"
song: "Yelamala"
image: ../../images/albumart/kazhugu-2.jpg
date: 2019-08-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/UwT0a3iEj5s"
type: "love"
singers:
  - MM. Manasi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">yelamala kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelamala kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">paathu veesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathu veesuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">moongle kooda poothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moongle kooda poothu"/>
</div>
<div class="lyrico-lyrics-wrapper">sethu pesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethu pesuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">koolangal melugaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koolangal melugaga"/>
</div>
<div class="lyrico-lyrics-wrapper">kulaiyuthe kulaiyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulaiyuthe kulaiyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">maramellam mayil thogai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maramellam mayil thogai"/>
</div>
<div class="lyrico-lyrics-wrapper">viriyuthe viriyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viriyuthe viriyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">malai aruvi thirumbi pakuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="malai aruvi thirumbi pakuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kurun kuruvi virumbi kekuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kurun kuruvi virumbi kekuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ithuku enna per ayya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithuku enna per ayya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yelamala kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelamala kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">paathu veesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathu veesuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">moongle kooda poothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moongle kooda poothu"/>
</div>
<div class="lyrico-lyrics-wrapper">sethu pesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethu pesuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">koolangal melugaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koolangal melugaga"/>
</div>
<div class="lyrico-lyrics-wrapper">kulaiyuthe kulaiyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulaiyuthe kulaiyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">maramellam mayil thogai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maramellam mayil thogai"/>
</div>
<div class="lyrico-lyrics-wrapper">viriyuthe viriyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viriyuthe viriyuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai thiti oosi kulirukul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai thiti oosi kulirukul"/>
</div>
<div class="lyrico-lyrics-wrapper">otti otti retta udathume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otti otti retta udathume"/>
</div>
<div class="lyrico-lyrics-wrapper">ottikichu konjam kiruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ottikichu konjam kiruka"/>
</div>
<div class="lyrico-lyrics-wrapper">ithe pichu vida vali iruka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ithe pichu vida vali iruka"/>
</div>
<div class="lyrico-lyrics-wrapper">pacha pullil kodai pidikira
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pacha pullil kodai pidikira"/>
</div>
<div class="lyrico-lyrics-wrapper">manja veyil ena nenaikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manja veyil ena nenaikuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">innum etho solla nenachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum etho solla nenachen"/>
</div>
<div class="lyrico-lyrics-wrapper">vekka thanni kulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vekka thanni kulla "/>
</div>
<div class="lyrico-lyrics-wrapper">enna tholachen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna tholachen"/>
</div>
<div class="lyrico-lyrics-wrapper">sollamale naanum aara
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollamale naanum aara"/>
</div>
<div class="lyrico-lyrics-wrapper">vandukul oodum neerai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandukul oodum neerai"/>
</div>
<div class="lyrico-lyrics-wrapper">nee odura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee odura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yelamala kaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yelamala kaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">paathu veesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paathu veesuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">moongle kooda poothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moongle kooda poothu"/>
</div>
<div class="lyrico-lyrics-wrapper">sethu pesuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethu pesuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">koolangal melugaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="koolangal melugaga"/>
</div>
<div class="lyrico-lyrics-wrapper">kulaiyuthe kulaiyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulaiyuthe kulaiyuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">maramellam mayil thogai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maramellam mayil thogai"/>
</div>
<div class="lyrico-lyrics-wrapper">viriyuthe viriyuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="viriyuthe viriyuthe"/>
</div>
</pre>
