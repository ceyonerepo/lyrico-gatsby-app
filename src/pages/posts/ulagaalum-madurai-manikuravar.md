---
title: "ulagaalum song lyrics"
album: "Madurai Manikuravar"
artist: "Isaignani Ilaiyaraaja"
lyricist: "Kavignar Muthulingam"
director: "K Raajarishi"
path: "/albums/madurai-manikuravar-lyrics"
song: "Ulagaalum"
image: ../../images/albumart/madurai-manikuravar.jpg
date: 2021-12-31
lang: tamil
youtubeLink: 
type: "devotional"
singers:
  - Mukesh
  - Rita
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">ulagalum naayagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagalum naayagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">udan varuvai thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udan varuvai thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">un thunaiyai naadi vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thunaiyai naadi vanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">arul tharuvai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arul tharuvai neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulagalum naayagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagalum naayagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">udan varuvai thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udan varuvai thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">un thunaiyai naadi vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thunaiyai naadi vanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">arul tharuvai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arul tharuvai neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">om kaariye om kaariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="om kaariye om kaariye"/>
</div>
<div class="lyrico-lyrics-wrapper">maariye kan thirapaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maariye kan thirapaye"/>
</div>
<div class="lyrico-lyrics-wrapper">engal neethiye aathiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal neethiye aathiye"/>
</div>
<div class="lyrico-lyrics-wrapper">vali thirapai ammamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali thirapai ammamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nonbirunthu anu kothikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nonbirunthu anu kothikum"/>
</div>
<div class="lyrico-lyrics-wrapper">thee mithipom vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee mithipom vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">then suvaiyil pongal ittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="then suvaiyil pongal ittu"/>
</div>
<div class="lyrico-lyrics-wrapper">padai alipom vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padai alipom vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulagalum naayagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagalum naayagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">udan varuvai thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udan varuvai thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">un thunaiyai naadi vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thunaiyai naadi vanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">arul tharuvai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arul tharuvai neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">un manjal nethi kannu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un manjal nethi kannu"/>
</div>
<div class="lyrico-lyrics-wrapper">engala paakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engala paakum"/>
</div>
<div class="lyrico-lyrics-wrapper">un santhanauthu theru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un santhanauthu theru"/>
</div>
<div class="lyrico-lyrics-wrapper">thunpatha pokkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunpatha pokkum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nan nethiyila itta en
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan nethiyila itta en"/>
</div>
<div class="lyrico-lyrics-wrapper">amma kungumam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amma kungumam"/>
</div>
<div class="lyrico-lyrics-wrapper">en puthiyila pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en puthiyila pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">thelivakum manthiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelivakum manthiram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oor ulagam unna maranthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oor ulagam unna maranthu"/>
</div>
<div class="lyrico-lyrics-wrapper">adichalum piduchalun 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adichalum piduchalun "/>
</div>
<div class="lyrico-lyrics-wrapper">unthan pillai than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan pillai than"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathalo viral kathalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalo viral kathalo"/>
</div>
<div class="lyrico-lyrics-wrapper">ennodu inge nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennodu inge nee"/>
</div>
<div class="lyrico-lyrics-wrapper">enthan anbile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enthan anbile"/>
</div>
<div class="lyrico-lyrics-wrapper">kootu satham kolava satham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kootu satham kolava satham"/>
</div>
<div class="lyrico-lyrics-wrapper">vattamittu kummi adikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vattamittu kummi adikum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethana ethana theru vilakugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethana ethana theru vilakugal"/>
</div>
<div class="lyrico-lyrics-wrapper">unaku ethi vachom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unaku ethi vachom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulagalum naayagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagalum naayagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">udan varuvai thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udan varuvai thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">un thunaiyai naadi vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thunaiyai naadi vanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">arul tharuvai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arul tharuvai neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru kutham illa nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru kutham illa nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">ingu enge iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ingu enge iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">oru pakthi illa nenju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru pakthi illa nenju"/>
</div>
<div class="lyrico-lyrics-wrapper">athu paalum iruku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu paalum iruku"/>
</div>
<div class="lyrico-lyrics-wrapper">ada thutu mattum thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada thutu mattum thane"/>
</div>
<div class="lyrico-lyrics-wrapper">engum suthi varuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engum suthi varuthu"/>
</div>
<div class="lyrico-lyrics-wrapper">athu muttalaiyum kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu muttalaiyum kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">mel ethi viduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mel ethi viduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">ther oodum veethiyile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ther oodum veethiyile"/>
</div>
<div class="lyrico-lyrics-wrapper">thevaram thiruvakum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thevaram thiruvakum "/>
</div>
<div class="lyrico-lyrics-wrapper">kekka venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kekka venum"/>
</div>
<div class="lyrico-lyrics-wrapper">puvi ellam ther eduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puvi ellam ther eduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">kulla than pothi unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kulla than pothi unna"/>
</div>
<div class="lyrico-lyrics-wrapper">paada venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paada venum"/>
</div>
<div class="lyrico-lyrics-wrapper">thotathu ellam tholangi nikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thotathu ellam tholangi nikka"/>
</div>
<div class="lyrico-lyrics-wrapper">vittathu ellam vilagi nika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vittathu ellam vilagi nika"/>
</div>
<div class="lyrico-lyrics-wrapper">suthanum suthanum amman
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suthanum suthanum amman"/>
</div>
<div class="lyrico-lyrics-wrapper">pakthiyum thesangal athanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pakthiyum thesangal athanaiyum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulagalum naayagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagalum naayagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">udan varuvai thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udan varuvai thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">un thunaiyai naadi vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thunaiyai naadi vanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">arul tharuvai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arul tharuvai neeye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">om kaariye om kaariye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="om kaariye om kaariye"/>
</div>
<div class="lyrico-lyrics-wrapper">maariye kan thirapaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maariye kan thirapaye"/>
</div>
<div class="lyrico-lyrics-wrapper">engal neethiye aathiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engal neethiye aathiye"/>
</div>
<div class="lyrico-lyrics-wrapper">vali thirapai ammamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vali thirapai ammamma"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nonbirunthu anu kothikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nonbirunthu anu kothikum"/>
</div>
<div class="lyrico-lyrics-wrapper">thee mithipom vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thee mithipom vaa vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">then suvaiyil pongal ittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="then suvaiyil pongal ittu"/>
</div>
<div class="lyrico-lyrics-wrapper">padai alipom vaa vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padai alipom vaa vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ulagalum naayagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulagalum naayagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">udan varuvai thaaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udan varuvai thaaye"/>
</div>
<div class="lyrico-lyrics-wrapper">un thunaiyai naadi vanthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thunaiyai naadi vanthom"/>
</div>
<div class="lyrico-lyrics-wrapper">arul tharuvai neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arul tharuvai neeye"/>
</div>
</pre>
