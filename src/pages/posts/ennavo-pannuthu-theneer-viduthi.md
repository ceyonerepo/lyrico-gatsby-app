---
title: "ennavo pannuthu song lyrics"
album: "Theneer Viduthi"
artist: "S.S. Kumaran"
lyricist: "Murugan Manthiram"
director: "S.S. Kumaran"
path: "/albums/theneer-viduthi-lyrics"
song: "Ennavo Pannuthu"
image: ../../images/albumart/theneer-viduthi.jpg
date: 2011-07-01
lang: tamil
youtubeLink: "https://www.youtube.com/embed/vMRgAwN7txc"
type: "love"
singers:
  - S.S. Kumaran
  - Chinmayi
  - S. Malavika
  - J.S.K. Shruthi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">O ennammo ennammo pannudhubulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O ennammo ennammo pannudhubulla"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkey yennu theriyavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkey yennu theriyavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">un kannula ennamo kallammirukku sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannula ennamo kallammirukku sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye vellaiyaa irundha manasukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye vellaiyaa irundha manasukkulla"/>
</div>
<div class="lyrico-lyrics-wrapper">kalaraa kanavu varudhu ulley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaraa kanavu varudhu ulley"/>
</div>
<div class="lyrico-lyrics-wrapper">idhu moththamaa suththama vilangavilla yendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="idhu moththamaa suththama vilangavilla yendaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">OhO kaththammannu mela oru mela oru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OhO kaththammannu mela oru mela oru"/>
</div>
<div class="lyrico-lyrics-wrapper">pavala pavala malli pooththu nirkkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pavala pavala malli pooththu nirkkudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pandhalukku nattuvacha nattuvacha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandhalukku nattuvacha nattuvacha"/>
</div>
<div class="lyrico-lyrics-wrapper">kambu kambu adhu saanjunirkkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kambu kambu adhu saanjunirkkudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennavo aagipoachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennavo aagipoachi"/>
</div>
<div class="lyrico-lyrics-wrapper">O ennavo ennavo pannudhubulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O ennavo ennavo pannudhubulla"/>
</div>
<div class="lyrico-lyrics-wrapper">sariyaa thavaraa theriyavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sariyaa thavaraa theriyavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">un un kannula ennamo kallammirukku sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un un kannula ennamo kallammirukku sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">HO neraaga paarthadhumey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="HO neraaga paarthadhumey"/>
</div>
<div class="lyrico-lyrics-wrapper">nooraaga naan murinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nooraaga naan murinjen"/>
</div>
<div class="lyrico-lyrics-wrapper">kannoaram valiyilley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannoaram valiyilley"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavula nanainji nananji medhandhen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavula nanainji nananji medhandhen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoathaana oru nodiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoathaana oru nodiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">thoalmela naan sarinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoalmela naan sarinjen"/>
</div>
<div class="lyrico-lyrics-wrapper">pollaadha udhadugal varaiyuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pollaadha udhadugal varaiyuren"/>
</div>
<div class="lyrico-lyrics-wrapper">ulara ulara thavichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulara ulara thavichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee paarthaadhum kaatrupoala manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarthaadhum kaatrupoala manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">lesaa parandhu poachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="lesaa parandhu poachey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee nerukkamaa nadakkumboadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee nerukkamaa nadakkumboadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalgal yedho kuzhambipoachey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalgal yedho kuzhambipoachey"/>
</div>
<div class="lyrico-lyrics-wrapper">ennai neeyum unnai naanum thedippaarthom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennai neeyum unnai naanum thedippaarthom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ennammo ennammo pannudhubulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ennammo ennammo pannudhubulla"/>
</div>
<div class="lyrico-lyrics-wrapper">enakkey yennu theriyavilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakkey yennu theriyavilla"/>
</div>
<div class="lyrico-lyrics-wrapper">un kannula ennamo kallammirukku sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kannula ennamo kallammirukku sollu"/>
</div>
</pre>
