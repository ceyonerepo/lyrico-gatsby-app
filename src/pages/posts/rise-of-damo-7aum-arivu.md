---
title: "rise of damo song lyrics"
album: "7aum Arivu"
artist: "Harris Jayaraj"
lyricist: "Madhan Karky"
director: "A.R. Murugadoss"
path: "/albums/7aum-arivu-lyrics"
song: "Rise Of Damo"
image: ../../images/albumart/7aum-arivu.jpg
date: 2011-10-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/wGZnPAi_qsY"
type: "mass"
singers:
  - Hao Wang
  - Sunitha Sarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Zhe yindu nanzi shi shui
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Zhe yindu nanzi shi shui"/>
</div>
<div class="lyrico-lyrics-wrapper">ta waisheme lai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ta waisheme lai"/>
</div>
<div class="lyrico-lyrics-wrapper">youren ma ta shi hehang
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="youren ma ta shi hehang"/>
</div>
<div class="lyrico-lyrics-wrapper">youren shuo ta shi shen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="youren shuo ta shi shen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ta zhi hao ni de wo de bing
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ta zhi hao ni de wo de bing"/>
</div>
<div class="lyrico-lyrics-wrapper">ta wei women zuo wanju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ta wei women zuo wanju"/>
</div>
<div class="lyrico-lyrics-wrapper">ta jiao women da jia waiyu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ta jiao women da jia waiyu"/>
</div>
<div class="lyrico-lyrics-wrapper">women chang tai mier
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="women chang tai mier"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaaye tamil-e vanangugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaye tamil-e vanangugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">unnoda thodangugiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnoda thodangugiren"/>
</div>
<div class="lyrico-lyrics-wrapper">ezhai enthan naavil neeye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ezhai enthan naavil neeye"/>
</div>
<div class="lyrico-lyrics-wrapper">kovil kondaaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kovil kondaaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ta hen qiguai hen qiguai hen qiguai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ta hen qiguai hen qiguai hen qiguai"/>
</div>
<div class="lyrico-lyrics-wrapper">ta chang ding zhe qiangbi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ta chang ding zhe qiangbi"/>
</div>
<div class="lyrico-lyrics-wrapper">ta yu niao lei he dongwu jiaoten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ta yu niao lei he dongwu jiaoten"/>
</div>
<div class="lyrico-lyrics-wrapper">women hen ai hen ai ta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="women hen ai hen ai ta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">damo hui bu huilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="damo hui bu huilai"/>
</div>
<div class="lyrico-lyrics-wrapper">damo hui bu huilai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="damo hui bu huilai"/>
</div>
</pre>
