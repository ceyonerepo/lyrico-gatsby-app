---
title: "chennai enga ooru song lyrics"
album: "Pattinapakkam"
artist: "Ishaan Dev"
lyricist: "Murugan Mandhiram"
director: "Jayadev"
path: "/albums/pattinapakkam-lyrics"
song: "Chennai Enga Ooru"
image: ../../images/albumart/pattinapakkam.jpg
date: 2018-11-23
lang: tamil
youtubeLink: 
type: "happy"
singers:
  - Ishaan Dev
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga ooru enga ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ooru enga ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu namma ooru namma ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu namma ooru namma ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu paaru vandhu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu paaru vandhu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhavaikkum nalla oorae yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhavaikkum nalla oorae yei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Singaara chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Singaara chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga ooru enga ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga ooru enga ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada namma ooru namma ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada namma ooru namma ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vandhu paaru vandhu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhu paaru vandhu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhavaikkum nalla oorae yei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhavaikkum nalla oorae yei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vellakaaran kattivecha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellakaaran kattivecha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kattivecha kottai undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kattivecha kottai undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottasuvar otti otti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottasuvar otti otti"/>
</div>
<div class="lyrico-lyrics-wrapper">Odugundra koovam undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Odugundra koovam undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekkachakka mozhigala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekkachakka mozhigala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesugira makkal undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesugira makkal undu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yegapatta kadavulum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yegapatta kadavulum"/>
</div>
<div class="lyrico-lyrics-wrapper">Saamigalum ingu undu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saamigalum ingu undu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh vanga kadal karaiyila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh vanga kadal karaiyila"/>
</div>
<div class="lyrico-lyrics-wrapper">MGR-u thoonguraaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="MGR-u thoonguraaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Japan naadum kaithattudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Japan naadum kaithattudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga aalu super staru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga aalu super staru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marinavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marinavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga perumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga perumai"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Tamil cinemaavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tamil cinemaavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga perumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga perumai"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Raajavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raajavum"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Rahumanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rahumanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Heyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Enga perumai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enga perumai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada enga ooru enga ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada enga ooru enga ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhu namma ooru namma ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhu namma ooru namma ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada enga ooru enga ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada enga ooru enga ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada namma ooru namma ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada namma ooru namma ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ChennaiAda enga ooru enga ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ChennaiAda enga ooru enga ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada namma ooru namma ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada namma ooru namma ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Heihei heihei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heihei heihei"/>
</div>
<div class="lyrico-lyrics-wrapper">He he ha he he ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He he ha he he ha"/>
</div>
<div class="lyrico-lyrics-wrapper">He he ha he he ha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="He he ha he he ha"/>
</div>
<div class="lyrico-lyrics-wrapper">Hehaaha he haaha he haaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hehaaha he haaha he haaha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Roatukada kai yenthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roatukada kai yenthi"/>
</div>
<div class="lyrico-lyrics-wrapper">Bavanula idly vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bavanula idly vada"/>
</div>
<div class="lyrico-lyrics-wrapper">Reddy kada annachi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Reddy kada annachi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadaiyila pongal vada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadaiyila pongal vada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei mela paaru metrovil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei mela paaru metrovil"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkudhu ooru sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkudhu ooru sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru sanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru sanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Keela paaru share auto
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keela paaru share auto"/>
</div>
<div class="lyrico-lyrics-wrapper">Pidhungudhu dhinam dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pidhungudhu dhinam dhinam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">IIT-um tidalparkkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="IIT-um tidalparkkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarkkudhu technology
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarkkudhu technology"/>
</div>
<div class="lyrico-lyrics-wrapper">OMR-um ECR-um
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="OMR-um ECR-um"/>
</div>
<div class="lyrico-lyrics-wrapper">Valarkkudhu loveology
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valarkkudhu loveology"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolla mazhaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolla mazhaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotti medhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotti medhakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellakadalil ooru medhakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellakadalil ooru medhakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethirthu ninnom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethirthu ninnom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhunthu ninnom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhunthu ninnom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naanga chennai tamilan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga chennai tamilan"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai tamilan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai tamilan"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai singaara chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai singaara chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai singaara chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai singaara chennai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada enga ooru enga ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada enga ooru enga ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada namma ooru namma ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada namma ooru namma ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada enga ooru enga ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada enga ooru enga ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada namma ooru namma ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada namma ooru namma ooru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei"/>
</div>
<div class="lyrico-lyrics-wrapper">Hei hei hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei hei hei"/>
</div>
</pre>
