---
title: 'oru nodiyinil uyir song lyrics'
album: 'Walter'
artist: 'Dharma Prakash'
lyricist: 'Arun Bharathi'
director: 'U Anbu'
path: '/albums/walter-song-lyrics'
song: 'Oru Nodiyinil Uyir'
image: ../../images/albumart/walter.jpg
date: 2020-03-13
lang: tamil
singers: 
- Chinmayi
youtubeLink: "https://www.youtube.com/embed/Qg2-iHOaofI"
type: 'sad'
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

  <div class="lyrico-lyrics-wrapper">oru nodiyinil uyir karainthiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodiyinil uyir karainthiduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">karu viliyinil kadal pugundhiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karu viliyinil kadal pugundhiduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">yeeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeeno"/>
</div>
<div class="lyrico-lyrics-wrapper">siru kanavathu perunkural tharuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru kanavathu perunkural tharuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">karuvarai athu kalangi ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvarai athu kalangi ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">yeeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeeno"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru nodi vanthu pogum saavai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodi vanthu pogum saavai"/>
</div>
<div class="lyrico-lyrics-wrapper">ovoru nodiyum thanthu ponai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ovoru nodiyum thanthu ponai"/>
</div>
<div class="lyrico-lyrics-wrapper">kasayadi pola vaarthai nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasayadi pola vaarthai nee"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthaye enai kondraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthaye enai kondraye"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir thindraye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir thindraye"/>
</div>
<div class="lyrico-lyrics-wrapper">thunaiye...
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thunaiye..."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru nodiyinil uyir karainthiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodiyinil uyir karainthiduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">karu viliyinil kadal pugundhiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karu viliyinil kadal pugundhiduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">yeeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeeno"/>
</div>
<div class="lyrico-lyrics-wrapper">iruthayam thee thaano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iruthayam thee thaano"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ah ah ah ah"/>
</div>
<div class="lyrico-lyrics-wrapper">ah ah ah ah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ah ah ah ah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thaluvum viralgalai kettal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaluvum viralgalai kettal"/>
</div>
<div class="lyrico-lyrics-wrapper">thalumbai thanthaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalumbai thanthaye"/>
</div>
<div class="lyrico-lyrics-wrapper">imai kilaiyil neerin kuruvi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="imai kilaiyil neerin kuruvi"/>
</div>
<div class="lyrico-lyrics-wrapper">thavithidum uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thavithidum uyire"/>
</div>
<div class="lyrico-lyrics-wrapper">suvai kodi kodi manathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suvai kodi kodi manathil"/>
</div>
<div class="lyrico-lyrics-wrapper">sulandrathu thalire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sulandrathu thalire"/>
</div>
<div class="lyrico-lyrics-wrapper">neeya thurathum theeya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neeya thurathum theeya"/>
</div>
<div class="lyrico-lyrics-wrapper">enaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oru nodiyinil uyir karainthiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru nodiyinil uyir karainthiduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">karu viliyinil kadal pugundhiduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karu viliyinil kadal pugundhiduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">yeeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeeno"/>
</div>
<div class="lyrico-lyrics-wrapper">siru kanavathu perunkural tharuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="siru kanavathu perunkural tharuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">karuvarai athu kalangi ponathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="karuvarai athu kalangi ponathe"/>
</div>
<div class="lyrico-lyrics-wrapper">yeeno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yeeno"/>
</div>
</pre>