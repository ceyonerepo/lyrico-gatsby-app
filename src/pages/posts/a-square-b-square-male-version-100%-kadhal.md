---
title: "a square b square male version song lyrics"
album: "100 percentage Kadhal"
artist: "G.V. Prakash Kumar"
lyricist: "Mohanrajan"
director: "M.M. Chandramouli"
path: "/albums/100-percentage-kadhal-lyrics"
song: "A Square B Square Male Version"
image: ../../images/albumart/100%-kadhal.jpg
date: 2019-10-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/MYnY5ajya-M"
type: "happy"
singers:
  - Meghdeep Bose
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">A Square B Square
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A Square B Square"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">A+B Whole Square
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A+B Whole Square"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaanaikku Poona Mela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaanaikku Poona Mela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jealous Aachi Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jealous Aachi Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">A Square B Square
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A Square B Square"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">A-B Whole Square
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="A-B Whole Square"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Googlum Yahoovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Googlum Yahoovum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muttikichu Paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muttikichu Paaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhillaa Ninnu Thaan Jeikaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhillaa Ninnu Thaan Jeikaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillu Mullu Thaan Senjaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillu Mullu Thaan Senjaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thillaalangadiya Aanaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thillaalangadiya Aanaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Four Twenty Velaiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Four Twenty Velaiya"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Day Nightu Senjaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Day Nightu Senjaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedakku Madakkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedakku Madakkaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tensiona Yethuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tensiona Yethuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Speedu Breaker Aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Speedu Breaker Aattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Torturu Pannuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Torturu Pannuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Counting Illaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Counting Illaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Confusu Pannuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Confusu Pannuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Full Stop Vekkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full Stop Vekkaama"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">BP ah Yethuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="BP ah Yethuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Over Actingu Thaangala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Over Actingu Thaangala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Semma Cheatingu Mudiyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Semma Cheatingu Mudiyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dishyum Fightingu Maarala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dishyum Fightingu Maarala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pakka Planingu Oyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pakka Planingu Oyala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhinusu dhinusaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinusu dhinusaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Time Table Pottu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Time Table Pottu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Clocku Mullaattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Clocku Mullaattam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thoogaama Suththuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoogaama Suththuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheating Cheating
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheating Cheating"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Firu Panju Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Firu Panju Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheating Cheating
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheating Cheating"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tigeru Spider Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tigeru Spider Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheating Cheating
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheating Cheating"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puyalu Flower Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puyalu Flower Kitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Cheating Cheating
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheating Cheating"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sky Kerchief Kitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sky Kerchief Kitta"/>
</div>
</pre>
