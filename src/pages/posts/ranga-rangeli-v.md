---
title: "ranga rangeli song lyrics"
album: "V"
artist: "Amit Trivedi"
lyricist: "Ramajogayya Sastry"
director: "Mohana Krishna - Indraganti"
path: "/albums/v-lyrics"
song: "Ranga Rangeli"
image: ../../images/albumart/v.jpg
date: 2020-09-05
lang: telugu
youtubeLink: "https://www.youtube.com/embed/JkR64ilv3Sc"
type: "happy"
singers:
  - Yazin Nizar
  - Nikhita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sala sara sarra vedekkindhi sayantram gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sala sara sarra vedekkindhi sayantram gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Sala sara sarra vedekkindhi sayantram gaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sala sara sarra vedekkindhi sayantram gaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Bira bira birra beachu ninda beerulu pongali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bira bira birra beachu ninda beerulu pongali"/>
</div>
<div class="lyrico-lyrics-wrapper">Bira bira birra beachu ninda beerulu pongali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bira bira birra beachu ninda beerulu pongali"/>
</div>
<div class="lyrico-lyrics-wrapper">Matthai povali gammathai povali kikkay povali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthai povali gammathai povali kikkay povali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranga rangeli ranga ranga rangeli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga rangeli ranga ranga rangeli"/>
</div>
<div class="lyrico-lyrics-wrapper">Mastho masthuga mabbula etthuku nichena veyali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mastho masthuga mabbula etthuku nichena veyali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranga rangeli ranga ranga rangeli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga rangeli ranga ranga rangeli"/>
</div>
<div class="lyrico-lyrics-wrapper">Racho rachaga pachiga muchata theerali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Racho rachaga pachiga muchata theerali"/>
</div>
<div class="lyrico-lyrics-wrapper">O rabba abbabba o rabba o rabba abbabba o rabba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rabba abbabba o rabba o rabba abbabba o rabba"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakalaka boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakalaka boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakalaka boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakalaka boom boom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Party party fun ka party
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Party party fun ka party"/>
</div>
<div class="lyrico-lyrics-wrapper">Touching touching chal modhaledadhama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touching touching chal modhaledadhama"/>
</div>
<div class="lyrico-lyrics-wrapper">Maza maza gaalla gajja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maza maza gaalla gajja"/>
</div>
<div class="lyrico-lyrics-wrapper">Sayyataadi climate vedi penchedama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sayyataadi climate vedi penchedama"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhe hangama line of control 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhe hangama line of control "/>
</div>
<div class="lyrico-lyrics-wrapper">haddulu meeri masthi cheddama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haddulu meeri masthi cheddama"/>
</div>
<div class="lyrico-lyrics-wrapper">Gurthuku techukoni okka chitta raayali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gurthuku techukoni okka chitta raayali"/>
</div>
<div class="lyrico-lyrics-wrapper">Pending unna fantasy laku tikkulu pettali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pending unna fantasy laku tikkulu pettali"/>
</div>
<div class="lyrico-lyrics-wrapper">Chill aipovali thrill ayipovali chill aipovali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chill aipovali thrill ayipovali chill aipovali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranga rangeli ranga ranga rangeli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga rangeli ranga ranga rangeli"/>
</div>
<div class="lyrico-lyrics-wrapper">Mastho masthuga mabbula etthuku nichena veyali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mastho masthuga mabbula etthuku nichena veyali"/>
</div>
<div class="lyrico-lyrics-wrapper">Racho rachaga pachiga muchata theerali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Racho rachaga pachiga muchata theerali"/>
</div>
<div class="lyrico-lyrics-wrapper">O rabba abbabba o rabba o rabba abbabba o rabba
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O rabba abbabba o rabba o rabba abbabba o rabba"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakalaka boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakalaka boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Shakalaka boom boom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shakalaka boom boom"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranga rangeli ranga ranga rangeli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga rangeli ranga ranga rangeli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranga rangeli ranga ranga rangeli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga rangeli ranga ranga rangeli"/>
</div>
<div class="lyrico-lyrics-wrapper">Ranga rangeli ranga ranga rangeli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ranga rangeli ranga ranga rangeli"/>
</div>
</pre>
