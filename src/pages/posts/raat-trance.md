---
title: "raat song lyrics"
album: "Trance"
artist: "Jackson Vijayan - Vinayakan"
lyricist: "	Kamal Karthik - Vinayak Sasikumar"
director: "Anwar Rasheed"
path: "/albums/trance-lyrics"
song: "Raat"
image: ../../images/albumart/trance.jpg
date: 2020-02-20
lang: malayalam
youtubeLink: "https://www.youtube.com/embed/wxrgHG9Tvy4"
type: "mass"
singers:
  - Sneha Khanwalkar
  - Neha S. Nair
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raat se bhi zyada hai nasheeli,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raat se bhi zyada hai nasheeli,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Band kamron ki hai woh ankahi,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Band kamron ki hai woh ankahi,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Qaid kar jaye sabko aisa khwaab hai woh,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Qaid kar jaye sabko aisa khwaab hai woh,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ishq sa ik zehar jo pila jaye,
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ishq sa ik zehar jo pila jaye,"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neelkkannil neelachuzhiyo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelkkannil neelachuzhiyo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neerchundil mounathirayo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neerchundil mounathirayo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaraarum aazhum saagaram aano nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaraarum aazhum saagaram aano nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaapathaal vaanathalayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaapathaal vaanathalayum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sooryanmaar ellaame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sooryanmaar ellaame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nin anthichoppil mungimaayunno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nin anthichoppil mungimaayunno"/>
</div>
</pre>
