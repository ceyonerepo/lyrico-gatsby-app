---
title: "sonna puriyadhu song lyrics"
album: "Velayudham"
artist: "Vijay Antony"
lyricist: "Siva Shanmugam"
director: "M. Raja"
path: "/albums/velayudham-lyrics"
song: "Sonna Puriyadhu"
image: ../../images/albumart/velayudham.jpg
date: 2011-10-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/0pwXCPUQHW0"
type: "mass"
singers:
  - Vijay Antony
  - Veera Shankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Andam Nadunadunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andam Nadunadunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Aagasam Kidukidunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagasam Kidukidunga"/>
</div>
<div class="lyrico-lyrics-wrapper">Chennai Sangamaththil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chennai Sangamaththil"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanga Medal Vaangiya Naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanga Medal Vaangiya Naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaarakudi Karakaatta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarakudi Karakaatta"/>
</div>
<div class="lyrico-lyrics-wrapper">Goshti Thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goshti Thaanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbittu Kupidurom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbittu Kupidurom"/>
</div>
<div class="lyrico-lyrics-wrapper">Koothaada Vaarumaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koothaada Vaarumaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaaveri Neerai Pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaaveri Neerai Pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Salangai Mani Kulungi Nirkka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Salangai Mani Kulungi Nirkka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thanjavur Thappatta Kuluvirukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanjavur Thappatta Kuluvirukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Panju Pothi Parakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panju Pothi Parakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Panchuneeyum Vaarumaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panchuneeyum Vaarumaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aataththil Kodi Parakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aataththil Kodi Parakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Aasanae Aadumaiyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aasanae Aadumaiyaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarakuruchi Nadaswaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarakuruchi Nadaswaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaaru Pudicha Urumi Melam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaru Pudicha Urumi Melam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirunelveli Seemai Aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirunelveli Seemai Aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodalamaadan Saami Aattam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodalamaadan Saami Aattam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokkanae Soora Kaaththa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkanae Soora Kaaththa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sulandu Sulandu Vaarumaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sulandu Sulandu Vaarumaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokki Vidum Makkal Koottam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokki Vidum Makkal Koottam"/>
</div>
<div class="lyrico-lyrics-wrapper">Supera Thaan Paadumaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Supera Thaan Paadumaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sokkanae Soora Kaaththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sokkanae Soora Kaaththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaarumaiyaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaarumaiyaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonna Puriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna Puriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollukulla Adangathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollukulla Adangathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mela Vacha Paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mela Vacha Paasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonna Puriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna Puriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollukulla Adangathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollukulla Adangathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mela Vacha Paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mela Vacha Paasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Poranthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Poranthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithupola Irukkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithupola Irukkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unga Mela Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unga Mela Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Nesam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenu Kannu Maimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenu Kannu Maimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virundhu Thinna Vaaran Ailesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virundhu Thinna Vaaran Ailesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Pullai Meenatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Pullai Meenatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaan Varaan Thookikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaan Varaan Thookikoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakitta Thakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakitta Thakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakitta Thakitta Thakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakitta Thakitta Thakitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velayutham Peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velayutham Peru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pathu Viral Velu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pathu Viral Velu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkathu Intha Kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkathu Intha Kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottiruchu Da Thelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottiruchu Da Thelu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sonna Puriyadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sonna Puriyadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollukulla Adangathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollukulla Adangathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Neenga Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neenga Ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Mela Vacha Paasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mela Vacha Paasam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Poranthalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Poranthalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ithupola Irukkathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ithupola Irukkathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Unga Mela Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Unga Mela Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vacha Nesam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vacha Nesam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenu Kannu Maimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenu Kannu Maimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virundhu Thinna Vaaran Ailesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virundhu Thinna Vaaran Ailesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Pullai Meenatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Pullai Meenatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaan Varaan Thookikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaan Varaan Thookikoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumsa Ye Gumsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumsa Ye Gumsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumsa Ye Gumsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumsa Ye Gumsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumsa Ye Gumsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumsa Ye Gumsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumsa Ye Gumsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumsa Ye Gumsa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jimmuku Jimmuku Jimmuku Jim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimmuku Jimmuku Jimmuku Jim"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimmuku Jimmuku Jimmuku Jim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimmuku Jimmuku Jimmuku Jim"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimmuku Jimmuku Jimmuku Jim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimmuku Jimmuku Jimmuku Jim"/>
</div>
<div class="lyrico-lyrics-wrapper">Jimmuku Jimmuku Jim
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jimmuku Jimmuku Jim"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Man U Hold Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Man U Hold Me"/>
</div>
<div class="lyrico-lyrics-wrapper">How You Control Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="How You Control Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Better You Fold Me
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Better You Fold Me"/>
</div>
<div class="lyrico-lyrics-wrapper">Everydaynananae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Everydaynananae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Osandha Malayeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Osandha Malayeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Enmela Nee Yeri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enmela Nee Yeri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudikka Kalleraki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudikka Kalleraki"/>
</div>
<div class="lyrico-lyrics-wrapper">Malara Ne Kudikurayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Malara Ne Kudikurayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaeyaeyae Yae Yae Yae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaeyaeyae Yae Yae Yae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thalayil Aadum Karagam Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalayil Aadum Karagam Irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaiyila Ganam Thaan Irunthathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaiyila Ganam Thaan Irunthathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Thara Thappattam Thaan Irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thara Thappattam Thaan Irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappana Attam Naan Pottathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappana Attam Naan Pottathilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Puli Vesham Pottukitu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli Vesham Pottukitu"/>
</div>
<div class="lyrico-lyrics-wrapper">Puli Aattam Adirukken
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puli Aattam Adirukken"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettai Aadi Mattum Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettai Aadi Mattum Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vazhnthathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vazhnthathilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandaiyila Mgr-Ru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandaiyila Mgr-Ru"/>
</div>
<div class="lyrico-lyrics-wrapper">Saataiyila Ayyanar
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saataiyila Ayyanar"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhil Irunthum Vambu Sandai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhil Irunthum Vambu Sandai"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottathilla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenu Kannu Maimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenu Kannu Maimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virundhu Thinna Vaaran Ailesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virundhu Thinna Vaaran Ailesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Pullai Meenatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Pullai Meenatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaan Varaan Thookikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaan Varaan Thookikoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumsa Ye Gumsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumsa Ye Gumsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumsa Ye Gumsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumsa Ye Gumsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumsa Ye Gumsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumsa Ye Gumsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumsa Ye Gumsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumsa Ye Gumsa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Varappa Midhichu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varappa Midhichu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raa Pagala Uzhachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raa Pagala Uzhachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhura Jananga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhura Jananga"/>
</div>
<div class="lyrico-lyrics-wrapper">Namma Katchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namma Katchi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ivanga Manasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivanga Manasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhosa Padutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhosa Padutha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thappu Nu Senjalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thappu Nu Senjalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Rightu Machi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rightu Machi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aadugira Aattathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadugira Aattathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Koodugira Koottathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koodugira Koottathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Eduthu Ippo Naanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Eduthu Ippo Naanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kumbuduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kumbuduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unga Veetu Chella Pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unga Veetu Chella Pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pola Yarum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pola Yarum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ungalathan Eppovumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ungalathan Eppovumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nambiduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nambiduren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Meenu Kannu Maimaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meenu Kannu Maimaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virundhu Thinna Vaaran Ailesaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virundhu Thinna Vaaran Ailesaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ae Pullai Meenatchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Pullai Meenatchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Athaan Varaan Thookikoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athaan Varaan Thookikoo"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakitta Thakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakitta Thakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakitta Thakitta Thakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakitta Thakitta Thakitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Velayutham Peru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velayutham Peru"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pathu Viral Velu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pathu Viral Velu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nikkathu Intha Kaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nikkathu Intha Kaalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottiruchu Da Thelu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottiruchu Da Thelu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakitta Thakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakitta Thakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Thakitta Thakitta Thakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thakitta Thakitta Thakitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Gumsa Hey Gumsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumsa Hey Gumsa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gumsa Hey Gumsa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gumsa Hey Gumsa"/>
</div>
</pre>
