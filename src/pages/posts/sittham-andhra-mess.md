---
title: "sittham song lyrics"
album: "Andhra Mess"
artist: "Prashant Pillai"
lyricist: "Kutty Revathi - Mohanraj"
director: "Jai"
path: "/albums/andhra-mess-lyrics"
song: "Sittham"
image: ../../images/albumart/andhra-mess.jpg
date: 2018-06-22
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2nH_zr-C3Is"
type: "melody"
singers:
  - unknown
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">sittam nitham pitham unnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sittam nitham pitham unnal"/>
</div>
<div class="lyrico-lyrics-wrapper">kothum mutham thanthaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kothum mutham thanthaye"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthaye kannal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthaye kannal"/>
</div>
<div class="lyrico-lyrics-wrapper">innum ennagum unnale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum ennagum unnale"/>
</div>
<div class="lyrico-lyrics-wrapper">en thegam thaanaga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en thegam thaanaga "/>
</div>
<div class="lyrico-lyrics-wrapper">kalaitherum than aadaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kalaitherum than aadaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">aagayam pole nirvaanam mele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagayam pole nirvaanam mele"/>
</div>
<div class="lyrico-lyrics-wrapper">theera theera mogam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theera theera mogam"/>
</div>
<div class="lyrico-lyrics-wrapper">kan moodi oosai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan moodi oosai"/>
</div>
<div class="lyrico-lyrics-wrapper">anbale ennai vaasi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbale ennai vaasi"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjodu ennai nesi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjodu ennai nesi"/>
</div>
<div class="lyrico-lyrics-wrapper">sugamai thodu thoduvai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sugamai thodu thoduvai"/>
</div>
<div class="lyrico-lyrics-wrapper">yugam thaan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yugam thaan thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kangal pirakum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kangal pirakum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pesatha mounam ul 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pesatha mounam ul "/>
</div>
<div class="lyrico-lyrics-wrapper">moochin payanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="moochin payanam"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosa nimidam neeluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosa nimidam neeluthe"/>
</div>
<div class="lyrico-lyrics-wrapper">rettai theeyai thegam thegam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="rettai theeyai thegam thegam"/>
</div>
<div class="lyrico-lyrics-wrapper">innum innum thevai koodum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum innum thevai koodum"/>
</div>
<div class="lyrico-lyrics-wrapper">perinban theerathe theerathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="perinban theerathe theerathe"/>
</div>
<div class="lyrico-lyrics-wrapper">un aasai nee enni po
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un aasai nee enni po"/>
</div>
<div class="lyrico-lyrics-wrapper">peraval potha potha potha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peraval potha potha potha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">sittam nitham pitham unnal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sittam nitham pitham unnal"/>
</div>
<div class="lyrico-lyrics-wrapper">kothum mutham thanthaye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kothum mutham thanthaye"/>
</div>
<div class="lyrico-lyrics-wrapper">thanthaye kannal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanthaye kannal"/>
</div>
</pre>
