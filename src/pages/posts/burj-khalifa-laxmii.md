---
title: "burj khalifa song lyrics"
album: "Laxmii"
artist: "Shashi - Dj Khushi"
lyricist: "Gagan Ahuja"
director: "Raghava Lawrence"
path: "/albums/laxmii-lyrics"
song: "Burj Khalifa"
image: ../../images/albumart/laxmii.jpg
date: 2020-11-09
lang: hindi
youtubeLink: "https://www.youtube.com/embed/a0TkeUhcVrM"
type: "love"
singers:
  - Shashi
  - Dj Khushi
  - Nikhita Gandhi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hot tere manner ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot tere manner ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhapte tere banner ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhapte tere banner ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Hot tere manner ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot tere manner ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhapte tere banner ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhapte tere banner ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Fashion tu kardi jivein kylie jenner ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Fashion tu kardi jivein kylie jenner ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho munda thakda nahi kar karke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho munda thakda nahi kar karke"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho munda thakda nahi kar karke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho munda thakda nahi kar karke"/>
</div>
<div class="lyrico-lyrics-wrapper">Teriyan tareefan..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teriyan tareefan.."/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jee karda dila doon tainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee karda dila doon tainu Burj Khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu Burj Khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu Burj Khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu Burj Khalifa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu Burj Khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu Burj Khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu Burj Khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu Burj Khalifa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Habibi habibi habibi habibi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Habibi habibi habibi habibi"/>
</div>
<div class="lyrico-lyrics-wrapper">Habibi habibi habibi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Habibi habibi habibi"/>
</div>
<div class="lyrico-lyrics-wrapper">In to jamila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In to jamila"/>
</div>
<div class="lyrico-lyrics-wrapper">Habibi habibi habibi habibi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Habibi habibi habibi habibi"/>
</div>
<div class="lyrico-lyrics-wrapper">Habibi habibi habibi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Habibi habibi habibi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho tainu vekh ke chadh gaye ishqe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu vekh ke chadh gaye ishqe"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu diamond wangu lishke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu diamond wangu lishke"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tainu vekh ke chadh gaye ishqe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu vekh ke chadh gaye ishqe"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu diamond wangu lishke
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu diamond wangu lishke"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Na karni siftan taj mahal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na karni siftan taj mahal"/>
</div>
<div class="lyrico-lyrics-wrapper">Jinni tere baare likhde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinni tere baare likhde"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho munde tainu follow karde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho munde tainu follow karde"/>
</div>
<div class="lyrico-lyrics-wrapper">Munde tainu follow karde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munde tainu follow karde"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhad gaye soccer fifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhad gaye soccer fifa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jee karda dila doon tainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jee karda dila doon tainu Burj Khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu Burj Khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu Burj Khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Tainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tainu Burj Khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tainu tainu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu tainu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho tainu tainu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho tainu tainu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yeah aye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yeah aye"/>
</div>
<div class="lyrico-lyrics-wrapper">Hot mere manner ni oh..
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot mere manner ni oh.."/>
</div>
<div class="lyrico-lyrics-wrapper">Chhapde mere banner ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhapde mere banner ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh yeah
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh yeah"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hot mere manner ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hot mere manner ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Chhapde mere banner ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chhapde mere banner ni"/>
</div>
<div class="lyrico-lyrics-wrapper">Look to jealous kardi ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Look to jealous kardi ae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kylie jenner ni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kylie jenner ni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mere husan de charche charche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere husan de charche charche"/>
</div>
<div class="lyrico-lyrics-wrapper">Mere husan de charche charche
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mere husan de charche charche"/>
</div>
<div class="lyrico-lyrics-wrapper">London ton america
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="London ton america"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mundeya dila de mainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mundeya dila de mainu Burj Khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu Burj Khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho mainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mainu Burj Khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa mainu Burj Khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa mainu Burj Khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Oh mainu burj khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh mainu burj khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mainu burj khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mainu burj khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ho mainu burj khalifa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho mainu burj khalifa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa mainu, in to jamila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa mainu, in to jamila"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Habibi habibi habibi habibi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Habibi habibi habibi habibi"/>
</div>
<div class="lyrico-lyrics-wrapper">Habibi habibi habibi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Habibi habibi habibi"/>
</div>
<div class="lyrico-lyrics-wrapper">In to jamila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In to jamila"/>
</div>
<div class="lyrico-lyrics-wrapper">Habibi habibi habibi habibi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Habibi habibi habibi habibi"/>
</div>
<div class="lyrico-lyrics-wrapper">Habibi habibi habibi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Habibi habibi habibi"/>
</div>
<div class="lyrico-lyrics-wrapper">In to jamila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="In to jamila"/>
</div>
</pre>
