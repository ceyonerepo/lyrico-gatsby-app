---
title: "poovendru song lyrics"
album: "Thuppakki Munai"
artist: "LV Muthu Ganesh"
lyricist: "Pulamai Pithan"
director: "Dinesh Selvaraj"
path: "/albums/thuppakki-munai-lyrics"
song: "Poovendru"
image: ../../images/albumart/thuppakki-munai.jpg
date: 2018-12-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/e7-JlmMxDoI"
type: "happy"
singers:
  - Sriram Parthasarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Poo vendru sonnaalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo vendru sonnaalum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasum ponnendrum sonnaalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasum ponnendrum sonnaalum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Magalendru sonnaalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magalendru sonnaalum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thaaiyendru sonnaalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thaaiyendru sonnaalum nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Miga nediyathu un kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miga nediyathu un kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu azhagiya porkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu azhagiya porkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaimurai pala nee kaanga kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaimurai pala nee kaanga kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En chellamae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chellamae neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thangamae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thangamae neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En vairamae neeyae neeyae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vairamae neeyae neeyae neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo vendru sonnaalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo vendru sonnaalum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasum ponnendrum sonnaalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasum ponnendrum sonnaalum nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sondha bantham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sondha bantham"/>
</div>
<div class="lyrico-lyrics-wrapper">Soththu sugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soththu sugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethuvum neethaanamma..aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethuvum neethaanamma..aa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vittaal solli kolla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vittaal solli kolla"/>
</div>
<div class="lyrico-lyrics-wrapper">Uravu ver yethamma..aa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uravu ver yethamma..aa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oru kanam unai kanaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru kanam unai kanaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Piriyinum manam thaangaathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Piriyinum manam thaangaathu"/>
</div>
<div class="lyrico-lyrics-wrapper">Iru vizhi imai moodaathamma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru vizhi imai moodaathamma"/>
</div>
<div class="lyrico-lyrics-wrapper">Periyavalena aanaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periyavalena aanaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru mazhalai kannae nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru mazhalai kannae nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Aruginil iru eppothumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aruginil iru eppothumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En chellamae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chellamae neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thangamae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thangamae neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En vairamae neeyae neeyae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vairamae neeyae neeyae neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poo vendru sonnaalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poo vendru sonnaalum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pasum ponnendrum sonnaalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pasum ponnendrum sonnaalum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Magalendru sonnaalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magalendru sonnaalum nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru thaaiyendru sonnaalum nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru thaaiyendru sonnaalum nee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Miga nediyathu un kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Miga nediyathu un kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu azhagiya porkaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu azhagiya porkaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaimurai pala nee kaanga kannae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaimurai pala nee kaanga kannae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En chellamae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En chellamae neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En thangamae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En thangamae neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">En vairamae neeyae neeyae neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vairamae neeyae neeyae neeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ahaaaaaaa yeahhaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ahaaaaaaa yeahhaaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaaaaaaaaaaaaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaaaaaaaaaaaaaa"/>
</div>
</pre>