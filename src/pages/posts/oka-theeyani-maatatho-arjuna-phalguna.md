---
title: "oka theeyani maatatho song lyrics"
album: "Arjuna Phalguna"
artist: "Priyadarshan Balasubramanian"
lyricist: "Chaitanya Prasad"
director: "Teja Marni"
path: "/albums/arjuna-phalguna-lyrics"
song: "Oka Theeyani Maatatho"
image: ../../images/albumart/arjuna-phalguna.jpg
date: 2021-12-31
lang: telugu
youtubeLink: "https://www.youtube.com/embed/x5dCfTryU7c"
type: "love"
singers:
  - Shashwat Singh
  - Shreya Iyer
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Oka theeyani maatatho kallu merise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka theeyani maatatho kallu merise"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka challani chooputho gunde thadise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka challani chooputho gunde thadise"/>
</div>
<div class="lyrico-lyrics-wrapper">Edha lopaala premala velluvegase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edha lopaala premala velluvegase"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvulaa poovula vellivirise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvulaa poovula vellivirise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu aluuginadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu aluuginadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Edha savvadi saginudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edha savvadi saginudhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasallari chesinadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasallari chesinadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee snehamlo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee snehamlo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oka theeyani maatatho kallu merise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka theeyani maatatho kallu merise"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka challani chooputho gunde thadise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka challani chooputho gunde thadise"/>
</div>
<div class="lyrico-lyrics-wrapper">Edha lopala premala velluvegase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edha lopala premala velluvegase"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvulaa poovula vellivirise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvulaa poovula vellivirise"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jinnaari meerchenri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jinnaari meerchenri"/>
</div>
<div class="lyrico-lyrics-wrapper">Kallaara kannaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kallaara kannaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaapaalu kuntaanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaapaalu kuntaanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kanti paapaplu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kanti paapaplu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee janta klaisaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee janta klaisaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee venta nadichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee venta nadichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee gunde terichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee gunde terichaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa gunde parichaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa gunde parichaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raavae vennelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raavae vennelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa kannae ladilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa kannae ladilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaatu maatu velalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaatu maatu velalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Sweetu mudhhulaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sweetu mudhhulaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nannu marachae nee ollu marachaenaey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nannu marachae nee ollu marachaenaey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninnu kalisaakapichi pillanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninnu kalisaakapichi pillanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa neevu nee nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa neevu nee nenu"/>
</div>
<div class="lyrico-lyrics-wrapper">Sundarudaa oo sundarudaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sundarudaa oo sundarudaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Endukuraa ee gunde dhade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endukuraa ee gunde dhade"/>
</div>
<div class="lyrico-lyrics-wrapper">Vindhulakae em thondarayaa ae ae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vindhulakae em thondarayaa ae ae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oka theeyani maatatho kallu merise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka theeyani maatatho kallu merise"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka challani chooputho gunde thadise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka challani chooputho gunde thadise"/>
</div>
<div class="lyrico-lyrics-wrapper">Edha lopala premala velluvegaase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edha lopala premala velluvegaase"/>
</div>
<div class="lyrico-lyrics-wrapper">Chirunavvula poovula vellivirise
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chirunavvula poovula vellivirise"/>
</div>
</pre>
