---
title: "aagaadha kaalam song lyrics"
album: "Kadamban"
artist: "Yuvan Shankar Raja"
lyricist: "Yugabharathi"
director: "Ragava"
path: "/albums/kadamban-lyrics"
song: "Aagaadha Kaalam"
image: ../../images/albumart/kadamban.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/-YB3MHaK3gc"
type: "sad"
singers:
  -	Ananthu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aagatha kalam onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagatha kalam onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyoda oora konnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyoda oora konnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothaichitu poyiduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothaichitu poyiduchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaneera keezha thallum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaneera keezha thallum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathikara kootam engha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathikara kootam engha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponanthinna koodiduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponanthinna koodiduchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodi kalaaga neenda engha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodi kalaaga neenda engha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kudusaiyum kooraiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudusaiyum kooraiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Theenjathu theeyala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theenjathu theeyala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ohoo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adiveraga vazhntha engha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiveraga vazhntha engha"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalamura gopuram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalamura gopuram"/>
</div>
<div class="lyrico-lyrics-wrapper">Saanjathu yarrala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanjathu yarrala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mudivula ponomae thothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mudivula ponomae thothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Velangalaiyae intha koothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velangalaiyae intha koothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagatha kalam onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagatha kalam onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyoda oora konnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyoda oora konnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothaichitu poyiduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothaichitu poyiduchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaneera keezha thallum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaneera keezha thallum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathikara kootam engha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathikara kootam engha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponanthinna koodiduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponanthinna koodiduchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Heyyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Heyyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Vellaama kaadu kaanja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vellaama kaadu kaanja"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru bogamthan pazha pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru bogamthan pazha pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannana kaadu theenja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannana kaadu theenja"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir ellamae oonam aagum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir ellamae oonam aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Annanthu partha vaanam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annanthu partha vaanam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazha sinthama yethu boomi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazha sinthama yethu boomi"/>
</div>
<div class="lyrico-lyrics-wrapper">Mallanthu pona neethi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mallanthu pona neethi"/>
</div>
<div class="lyrico-lyrics-wrapper">Verum mannagi pochae sami
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verum mannagi pochae sami"/>
</div>
<div class="lyrico-lyrics-wrapper">Thala onjomae aee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thala onjomae aee"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjaamalae vaazhnthomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaamalae vaazhnthomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemanthuthan manjomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemanthuthan manjomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagatha kalam onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagatha kalam onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyoda oora konnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyoda oora konnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothaichitu poyiduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothaichitu poyiduchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Appaviyana naanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appaviyana naanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Adipattomae naadae paakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adipattomae naadae paakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Kotthodu naanga saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kotthodu naanga saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru aalilla kelvi kekka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru aalilla kelvi kekka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munnala aanda kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munnala aanda kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mogamillama muli aanom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mogamillama muli aanom"/>
</div>
<div class="lyrico-lyrics-wrapper">Venneeru paanja vera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venneeru paanja vera"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhesa engeyum kaana ponom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhesa engeyum kaana ponom"/>
</div>
<div class="lyrico-lyrics-wrapper">Puliyannomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puliyannomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjaamalae vaazhnthomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaamalae vaazhnthomae"/>
</div>
<div class="lyrico-lyrics-wrapper">Yemanthuthan manjomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yemanthuthan manjomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aagatha kalam onnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aagatha kalam onnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adiyoda oora konnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adiyoda oora konnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pothaichitu poyiduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pothaichitu poyiduchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saaneera keezha thallum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaneera keezha thallum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sathikara kootam engha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sathikara kootam engha"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponanthinna koodiduchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponanthinna koodiduchae"/>
</div>
</pre>
