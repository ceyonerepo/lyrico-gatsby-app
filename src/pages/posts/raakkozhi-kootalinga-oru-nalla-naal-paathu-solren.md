---
title: "raakkozhi kootalinga song lyrics"
album: "Oru Nalla Naal Paathu Solren"
artist: "Justin Prabhakaran"
lyricist: "Muthamil"
director: "Arumuga Kumar"
path: "/albums/oru-nalla-naal-paathu-solren-lyrics"
song: "Raakkozhi Kootalinga"
image: ../../images/albumart/oru-nalla-naal-paathu-solren.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/c3YPCyO_AaY"
type: "happy"
singers:
  - Arivarasu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raakkozhi kootalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakkozhi kootalinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajangatha paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajangatha paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu raathiri engala pooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu raathiri engala pooti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaikka inga yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikka inga yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ragalennu vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragalennu vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vida mattom bantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vida mattom bantha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adichalum mithichalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adichalum mithichalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada marupadi adithadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada marupadi adithadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pannavae maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pannavae maattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jaicha thaan enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jaicha thaan enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Thothathaan enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thothathaan enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Valainjaalum nelinjaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valainjaalum nelinjaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naanga nenaichatha mudikkaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naanga nenaichatha mudikkaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Adanggavae maattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adanggavae maattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaarupu sattakkaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarupu sattakkaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaariyathil kettikkaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariyathil kettikkaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaarupu sattakkaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaarupu sattakkaran"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaariyathil kettikkaran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaariyathil kettikkaran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ekku thappa ekkachakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ekku thappa ekkachakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Vesham naanga pottom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vesham naanga pottom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottom pottom pottom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottom pottom pottom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadiya paatha kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadiya paatha kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Verra verra mugam kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Verra verra mugam kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaattum kaattum kaattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaattum kaattum kaattum"/>
</div>
<div class="lyrico-lyrics-wrapper">Entha ooru ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Entha ooru ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sontha baasha pesa mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sontha baasha pesa mattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Mattom mattom mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mattom mattom mattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha ooru meena pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha ooru meena pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Neechalathaan naanga pottom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neechalathaan naanga pottom"/>
</div>
<div class="lyrico-lyrics-wrapper">Pottom pottom pottom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pottom pottom pottom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Come on…come on come on
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Come on…come on come on"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Parunthukku vettai pathathappa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parunthukku vettai pathathappa"/>
</div>
<div class="lyrico-lyrics-wrapper">Parakkura speedum nikkathappaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Parakkura speedum nikkathappaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Virunthukku neram vanthachuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Virunthukku neram vanthachuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan vattam pottu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan vattam pottu "/>
</div>
<div class="lyrico-lyrics-wrapper">kattam pottu thookkuvan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattam pottu thookkuvan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pengal theenda mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pengal theenda mattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal thoondinaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal thoondinaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Naangal seenda mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naangal seenda mattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna aana enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna aana enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ellai thaanda mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ellai thaanda mattom"/>
</div>
<div class="lyrico-lyrics-wrapper">Seiyum tholizhlae deivam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seiyum tholizhlae deivam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kollgai mera mattom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kollgai mera mattom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raakkozhi kootalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakkozhi kootalinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajangatha paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajangatha paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu raathiri engala pooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu raathiri engala pooti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaikka inga yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikka inga yaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raakkozhi kootalinga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raakkozhi kootalinga"/>
</div>
<div class="lyrico-lyrics-wrapper">Rajangatha paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rajangatha paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadu raathiri engala pooti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadu raathiri engala pooti"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaikka inga yaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaikka inga yaaru"/>
</div>
</pre>
