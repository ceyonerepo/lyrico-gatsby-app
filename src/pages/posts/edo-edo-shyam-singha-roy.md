---
title: "edo edo song lyrics"
album: "Shyam Singha Roy"
artist: "Mickey J. Meyer"
lyricist: "Krishna Kanth"
director: "Rahul Sankrityan"
path: "/albums/shyam-singha-roy-lyrics"
song: "Edo Edo"
image: ../../images/albumart/shyam-singha-roy.jpg
date: 2021-12-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/wljleZlniKE"
type: "love"
singers:
  - Chaitra Ambadipudi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Edo edo teliyani lokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo edo teliyani lokama"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo edo taha taha maikama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo edo taha taha maikama"/>
</div>
<div class="lyrico-lyrics-wrapper">I am so into you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am so into you"/>
</div>
<div class="lyrico-lyrics-wrapper">I am so into you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am so into you"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch me like you do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch me like you do"/>
</div>
<div class="lyrico-lyrics-wrapper">Love me like you want it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love me like you want it"/>
</div>
<div class="lyrico-lyrics-wrapper">I am so into you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am so into you"/>
</div>
<div class="lyrico-lyrics-wrapper">I am so into you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am so into you"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaipe tera tegina padava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaipe tera tegina padava"/>
</div>
<div class="lyrico-lyrics-wrapper">Alajadula godava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alajadula godava"/>
</div>
<div class="lyrico-lyrics-wrapper">Lolopala maro theerame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lolopala maro theerame"/>
</div>
<div class="lyrico-lyrics-wrapper">Maree rammane era 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maree rammane era "/>
</div>
<div class="lyrico-lyrics-wrapper">vesina sayantrama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vesina sayantrama"/>
</div>
<div class="lyrico-lyrics-wrapper">Nuvve naa edhuruga unte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nuvve naa edhuruga unte"/>
</div>
<div class="lyrico-lyrics-wrapper">Ye madhurime thaake
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye madhurime thaake"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee adharame geese o chitrame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee adharame geese o chitrame"/>
</div>
<div class="lyrico-lyrics-wrapper">Haaye varadha nadhi teerunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haaye varadha nadhi teerunu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanula vodi cherenu Ee velana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanula vodi cherenu Ee velana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I am so into you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am so into you"/>
</div>
<div class="lyrico-lyrics-wrapper">I am so into you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am so into you"/>
</div>
<div class="lyrico-lyrics-wrapper">Touch me like you do
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Touch me like you do"/>
</div>
<div class="lyrico-lyrics-wrapper">Love me like you want it
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Love me like you want it"/>
</div>
<div class="lyrico-lyrics-wrapper">I am so into you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am so into you"/>
</div>
<div class="lyrico-lyrics-wrapper">I am so into you
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I am so into you"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pranam teese ee allare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranam teese ee allare"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalle moose dhyanale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalle moose dhyanale"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee chali chalitho ila ee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee chali chalitho ila ee"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondaralo o tamasha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondaralo o tamasha"/>
</div>
<div class="lyrico-lyrics-wrapper">Tegabaduthu parigeduthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tegabaduthu parigeduthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Urakalu vese athishayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urakalu vese athishayame"/>
</div>
<div class="lyrico-lyrics-wrapper">Perigenule konche komchem
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perigenule konche komchem"/>
</div>
<div class="lyrico-lyrics-wrapper">Antha sontham antu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Antha sontham antu"/>
</div>
<div class="lyrico-lyrics-wrapper">Don’t know why
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Don’t know why"/>
</div>
<div class="lyrico-lyrics-wrapper">You let the fire in my soul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="You let the fire in my soul"/>
</div>
<div class="lyrico-lyrics-wrapper">Karchicchu kallanchullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Karchicchu kallanchullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalalu kalabadaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalalu kalabadaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Moham talupu terichena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Moham talupu terichena"/>
</div>
<div class="lyrico-lyrics-wrapper">Telisi perigena ee vedhana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Telisi perigena ee vedhana"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo edo teliyani lokama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo edo teliyani lokama"/>
</div>
<div class="lyrico-lyrics-wrapper">Edo edo taha taha maikama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo edo taha taha maikama"/>
</div>
</pre>
