---
title: "narmada song lyrics"
album: "Kandaen"
artist: "Vijay Ebenezer"
lyricist: "Krish"
director: "A.C. Mugil"
path: "/albums/kandaen-lyrics"
song: "Narmada"
image: ../../images/albumart/kandaen.jpg
date: 2011-05-20
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4h-YHoFUiqc"
type: "love"
singers:
  - Haricharan
  - Suchitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Keduthaai Thookkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keduthaai Thookkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaai Swaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaai Swaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Idhazh Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Idhazh Oram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Yekkam Maraindhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Yekkam Maraindhadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Malarndhadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Malarndhadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikki Yeno Yeno Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki Yeno Yeno Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarka Paarka Nadukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarka Paarka Nadukkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal Urasa Urasa Neruppaagiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Urasa Urasa Neruppaagiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Panjaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Panjaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikki Yeno Yeno Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki Yeno Yeno Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarka Paarka Nadukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarka Paarka Nadukkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal Urasa Urasa Neruppaagiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Urasa Urasa Neruppaagiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Panjaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Panjaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Narmada Narmadha Narmada Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narmada Narmadha Narmada Sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Narmadha Thaagama Mogama Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narmadha Thaagama Mogama Sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Narmadha En Narmadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narmadha En Narmadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikki Yeno Yeno Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki Yeno Yeno Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarka Paarka Nadukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarka Paarka Nadukkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal Urasa Urasa Neruppaagiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Urasa Urasa Neruppaagiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Panjaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Panjaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keduthaai Thookkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keduthaai Thookkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaai Swaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaai Swaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Idhazh Oram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Idhazh Oram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi Moodinen Mugam Paarkkiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Moodinen Mugam Paarkkiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Madi Thedinen Narmadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madi Thedinen Narmadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Illai Endraal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Illai Endraal"/>
</div>
<div class="lyrico-lyrics-wrapper">En Pirappil Artham Yedhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Pirappil Artham Yedhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mazhai Peiyave Poo Pookkudhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhai Peiyave Poo Pookkudhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Endhan Perazhagan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Endhan Perazhagan"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaanathodu Neelam Pole
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathodu Neelam Pole"/>
</div>
<div class="lyrico-lyrics-wrapper">Ondraaga Vaazhvom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ondraaga Vaazhvom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sarigiradhu En Manadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarigiradhu En Manadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kai Koduthadhu En Nilavu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Koduthadhu En Nilavu"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Serudhu En Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Serudhu En Idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudhusaaga Nam Jananam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudhusaaga Nam Jananam"/>
</div>
<div class="lyrico-lyrics-wrapper">Narmadha Narmadha Narmadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narmadha Narmadha Narmadha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kadhaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kadhaliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikki Yeno Yeno Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki Yeno Yeno Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarka Paarka Nadukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarka Paarka Nadukkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal Urasa Urasa Neruppaagiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Urasa Urasa Neruppaagiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Panjaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Panjaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Azhagaai Aval Varuvaal Ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaai Aval Varuvaal Ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudippaal Enai Kandadhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudippaal Enai Kandadhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Innum Enna Solla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Innum Enna Solla"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Manadhil Naanum Sera
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Manadhil Naanum Sera"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Padhamaai Ival
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padhamaai Ival"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunathaal Avan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunathaal Avan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thandhaan Iru Kangalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thandhaan Iru Kangalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyil Unnai Thaangi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyil Unnai Thaangi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalam Muzhudhum Kaathukolven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Muzhudhum Kaathukolven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nizhal Pole Naan Varuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhal Pole Naan Varuven"/>
</div>
<div class="lyrico-lyrics-wrapper">En Thamizh Pol Uyir Koduppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Thamizh Pol Uyir Koduppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla Marandhen En Valiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla Marandhen En Valiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Purindhukollu Pada Padappai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Purindhukollu Pada Padappai"/>
</div>
<div class="lyrico-lyrics-wrapper">Narmada Narmada Narmada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narmada Narmada Narmada"/>
</div>
<div class="lyrico-lyrics-wrapper">En Kadhaliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Kadhaliye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikki Yeno Yeno Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki Yeno Yeno Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarka Paarka Nadukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarka Paarka Nadukkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal Urasa Urasa Neruppaagiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Urasa Urasa Neruppaagiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Panjaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Panjaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sikki Yeno Yeno Nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sikki Yeno Yeno Nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudikkiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudikkiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai Paarka Paarka Nadukkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai Paarka Paarka Nadukkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Udal Urasa Urasa Neruppaagiradhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udal Urasa Urasa Neruppaagiradhe"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Panjaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Panjaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Narmada Narmadha Narmada Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narmada Narmadha Narmada Sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Narmadha Thaagama Mogama Sol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narmadha Thaagama Mogama Sol"/>
</div>
<div class="lyrico-lyrics-wrapper">Narmadha En Narmadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narmadha En Narmadha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Keduthaai Thookkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keduthaai Thookkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaai Swaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaai Swaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">En Narmadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Narmadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Keduthaai Thookkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keduthaai Thookkam"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanaai Swaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanaai Swaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Narmadha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Narmadha"/>
</div>
<div class="lyrico-lyrics-wrapper">Keduthaai Thookkam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keduthaai Thookkam"/>
</div>
</pre>
