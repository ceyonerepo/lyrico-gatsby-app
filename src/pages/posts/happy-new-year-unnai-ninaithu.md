---
title: "happy new year song lyrics"
album: "Unnai Ninaithu"
artist: "Sirpy"
lyricist: "Kalaikumar"
director: "Vikraman"
path: "/albums/unnai-ninaithu-lyrics"
song: "Happy New Year"
image: ../../images/albumart/unnai-ninaithu.jpg
date: 2002-05-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/89BcG4nar7U"
type: "happy"
singers:
  - P. Unnikrishnan
  - Sujatha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aaa aaa  aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa  aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaa aaaa  aaaa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaaa  aaaa aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Happy new year
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy new year"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy new year vandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy new year vandhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbai solli aasai ullam thulludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbai solli aasai ullam thulludhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Surya gandhi poo polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surya gandhi poo polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam maarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam maarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugam serudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam serudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala lala lala lala lala lala lala laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala lala lala lala lala lala lala laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lala lala lala lala lala lala lala laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala lala lala lala lala lala lala laa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Happy new year
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy new year"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy new year vandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy new year vandhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbai solli aasai ullam thulludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbai solli aasai ullam thulludhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaa  aaaa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa  aaaa aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaaranam aayiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaaranam aayiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Soozha varam seidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soozha varam seidhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Naarana nambi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naarana nambi"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadakindran indrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadakindran indrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Poorana porkudam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poorana porkudam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaithu pooravengum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaithu pooravengum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoarana naata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoarana naata"/>
</div>
<div class="lyrico-lyrics-wrapper">Kana kanden thozhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kana kanden thozhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engal vaazhvai kavithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal vaazhvai kavithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thogupaai aakalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thogupaai aakalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjal valiyae ellorukum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjal valiyae ellorukum"/>
</div>
<div class="lyrico-lyrics-wrapper">Anupalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anupalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanathu megamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanathu megamai"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam sernthu pogalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam sernthu pogalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Poonkothai polaevae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poonkothai polaevae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam vaithu vaazhalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam vaithu vaazhalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nam siripai padam pidithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam siripai padam pidithu"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhthu madal vazhangidalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhthu madal vazhangidalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nootraandai thaandium
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nootraandai thaandium"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam perai naazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam perai naazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nizhaithidalaamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nizhaithidalaamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala lala lala lala lala lala lala laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala lala lala lala lala lala lala laa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Happy new year
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy new year"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy new year vandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy new year vandhadhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbai solli aasai ullam thulludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbai solli aasai ullam thulludhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aaa aaa  aaa aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaa aaa  aaa aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aaaa aaaa  aaaa aaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aaaa aaaa  aaaa aaaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mmm  mmm  mmmmm mmmmm
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mmm  mmm  mmmmm mmmmm"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thulasi chediyil mazhaiyin
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulasi chediyil mazhaiyin"/>
</div>
<div class="lyrico-lyrics-wrapper">Thuzhiyai polaevae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thuzhiyai polaevae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engal vizhiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engal vizhiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Aanandhathin eeramae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aanandhathin eeramae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Poovil oar paathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poovil oar paathiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Naam seithu paarkalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naam seithu paarkalaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paal sorai pottu naam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paal sorai pottu naam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravaikum ootalaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravaikum ootalaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kudai pidikum thennai maram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kudai pidikum thennai maram"/>
</div>
<div class="lyrico-lyrics-wrapper">Namathu gunam solliyadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Namathu gunam solliyadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhnaatkal yaavumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhnaatkal yaavumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasanthangal veesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasanthangal veesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaazhnthirupomae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhnthirupomae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala lala lala lala lala lala laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala lala lala lala lala lala laa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Happy new year
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy new year"/>
</div>
<div class="lyrico-lyrics-wrapper">Happy new year vandhadhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Happy new year vandhadhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anbai solli aasai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbai solli aasai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam thulludhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam thulludhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Surya gandhi poo polae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Surya gandhi poo polae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mugam maarudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam maarudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sugam serudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sugam serudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Lala lala lala lala lala lala laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala lala lala lala lala lala laa"/>
</div>
<div class="lyrico-lyrics-wrapper">Lala lala lala lala lala lala laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lala lala lala lala lala lala laa"/>
</div>
</pre>
