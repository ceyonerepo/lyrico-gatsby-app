---
title: "idhudhaan song lyrics"
album: "Sivappu Manjal Pachai"
artist: "Siddhu Kumar"
lyricist: "Mohan Rajan"
director: "Sasi"
path: "/albums/sivappu-manjal-pachai-lyrics"
song: "Idhudhaan"
image: ../../images/albumart/sivappu-manjal-pachai.jpg
date: 2019-09-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/7ydI06TnEJs"
type: "love"
singers:
  - Naresh Iyer
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Idhudhaan Idhudhaan Idhudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhudhaan Idhudhaan Idhudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarum Kaana Thudiththa Naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarum Kaana Thudiththa Naalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhudhaan Idhudhaan Idhudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhudhaan Idhudhaan Idhudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarum Sera Thuditha Naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarum Sera Thuditha Naalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vizhi Aadhil Vizhuvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhi Aadhil Vizhuvenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini Unna Viduvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Unna Viduvenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Viraludan Ada Viral Kadathiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viraludan Ada Viral Kadathiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala Marappenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala Marappenaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Udan Varuvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udan Varuvenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Thoduvenaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Thoduvenaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilai Nuniyila Oru Thuliyena
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilai Nuniyila Oru Thuliyena"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkiren Sarithaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkiren Sarithaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenaikkum Nodi Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenaikkum Nodi Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil Irukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil Irukkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil Iruka Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil Iruka Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukki Pidikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukki Pidikkanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkai Maruganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkai Maruganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkul Uruganum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkul Uruganum"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyire"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Manasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adam Pidikkira Kozhandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adam Pidikkira Kozhandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adha En Iduppil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adha En Iduppil"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Sumanthida Aasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Sumanthida Aasa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Sirikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Sirikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Ada Edhukku Nee Marandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Edhukku Nee Marandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Naan Ketkkum Mellisa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Naan Ketkkum Mellisa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unakkena Vaazha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unakkena Vaazha"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyir Yengudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyir Yengudhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Ninaivondrae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Ninaivondrae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai Thaangudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai Thaangudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhilum Neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhilum Neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhuvum Neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhuvum Neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Poda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhudhaan Idhudhaan Idhudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhudhaan Idhudhaan Idhudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarum Kaana Thudiththa Naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarum Kaana Thudiththa Naalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhudhaan Idhudhaan Idhudhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhudhaan Idhudhaan Idhudhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruvarum Sera Thuditha Naalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruvarum Sera Thuditha Naalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenaikkum Nodi Ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenaikkum Nodi Ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil Irukkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil Irukkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugil Iruka Nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugil Iruka Nee"/>
</div>
<div class="lyrico-lyrics-wrapper">Irukki Pidikkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Irukki Pidikkanum"/>
</div>
</pre>
