---
title: "seemaraja swag song lyrics"
album: "Seemaraja"
artist: "D Imman"
lyricist: "Yugabharathi"
director: "Ponram"
path: "/albums/seemaraja-lyrics"
song: "Seemaraja Swag"
image: ../../images/albumart/seemaraja.jpg
date: 2018-09-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4Vv59YCexhM"
type: "theme"
singers:
  - Shenbagaraj
  - Vignesh Narayanan
  - Santosh Hariharan
  - Deepak
  - Swetha Suresh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hei hei
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hei hei"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kodutha adiya adiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodutha adiya adiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Thiruppi thiruppi kodukkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruppi thiruppi kodukkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Siruthai puliya puliya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siruthai puliya puliya"/>
</div>
<div class="lyrico-lyrics-wrapper">Madiyil anachi nadakkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madiyil anachi nadakkuraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edutha mudiva mudiva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edutha mudiva mudiva"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruthi varaikkum mathikkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruthi varaikkum mathikkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Adutha usuru kalanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adutha usuru kalanga"/>
</div>
<div class="lyrico-lyrics-wrapper">Manasu vathanga thudikkiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu vathanga thudikkiran"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seema seema raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema seema raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan unga thanga raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan unga thanga raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Seema seema raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema seema raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan enga singa raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan enga singa raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seema seema raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema seema raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan unga thanga raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan unga thanga raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Seema seema raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seema seema raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Ivan enga singa raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ivan enga singa raja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Podae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podae"/>
</div>
</pre>
