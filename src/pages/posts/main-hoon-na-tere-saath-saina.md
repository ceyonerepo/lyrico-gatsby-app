---
title: "main hoon na tere saath song lyrics"
album: "Saina"
artist: "Amaal Mallik"
lyricist: "Kunaal Vermaa"
director: "Amole Gupte"
path: "/albums/saina-lyrics"
song: "Main Hoon Na Tere Saath"
image: ../../images/albumart/saina.jpg
date: 2021-03-26
lang: hindi
youtubeLink: "https://www.youtube.com/embed/RCuhI0X0toQ"
type: "Motivational"
singers:
  - Armaan Malik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Teri in saanson ke ehsaason ko
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri in saanson ke ehsaason ko"/>
</div>
<div class="lyrico-lyrics-wrapper">Yun kareeb rakhta hun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yun kareeb rakhta hun"/>
</div>
<div class="lyrico-lyrics-wrapper">Teri har khushi ko aur
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teri har khushi ko aur"/>
</div>
<div class="lyrico-lyrics-wrapper">Dardon ko main apna samajhta hoon
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dardon ko main apna samajhta hoon"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tu mushkuraye dil yehi chaahe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu mushkuraye dil yehi chaahe"/>
</div>
<div class="lyrico-lyrics-wrapper">Darne ki hai nahi ab koyi baat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darne ki hai nahi ab koyi baat"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Main hoon na tere saath tere saath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main hoon na tere saath tere saath"/>
</div>
<div class="lyrico-lyrics-wrapper">Na koyi tha na hoga tere baad
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Na koyi tha na hoga tere baad"/>
</div>
<div class="lyrico-lyrics-wrapper">Main hoon na tere saath tere saath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main hoon na tere saath tere saath"/>
</div>
<div class="lyrico-lyrics-wrapper">Kar le tu poore saare tere khwab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar le tu poore saare tere khwab"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dekho na tum zara koyi nahi yahan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dekho na tum zara koyi nahi yahan"/>
</div>
<div class="lyrico-lyrics-wrapper">Dono hi bekaraar hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dono hi bekaraar hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Aise kabhi na thi kisi ki aadatein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aise kabhi na thi kisi ki aadatein"/>
</div>
<div class="lyrico-lyrics-wrapper">Jo bhi hai pehli baar hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jo bhi hai pehli baar hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jao kahin bhi main teri yeh khushbuyein
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jao kahin bhi main teri yeh khushbuyein"/>
</div>
<div class="lyrico-lyrics-wrapper">Rehti hamesha sath hain
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rehti hamesha sath hain"/>
</div>
<div class="lyrico-lyrics-wrapper">Maangi nahi kabhi maine koyi dua
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maangi nahi kabhi maine koyi dua"/>
</div>
<div class="lyrico-lyrics-wrapper">Tu jabse mere paas hai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tu jabse mere paas hai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hmm baahon mein teri barbaad honge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hmm baahon mein teri barbaad honge"/>
</div>
<div class="lyrico-lyrics-wrapper">Chahe subah se ho jaaye raat
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chahe subah se ho jaaye raat"/>
</div>
<div class="lyrico-lyrics-wrapper">Main hoon na tere saath tere saath
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Main hoon na tere saath tere saath"/>
</div>
<div class="lyrico-lyrics-wrapper">Kar le tu poore saare tere khwab
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kar le tu poore saare tere khwab"/>
</div>
</pre>
