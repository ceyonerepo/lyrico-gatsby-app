---
title: "maagaa maagaa song lyrics"
album: "Kannum Kannum Kollaiyadithaal"
artist: "Masala Coffee - Harshavardhan Rameshwar"
lyricist: "V Lakshmi Priya"
director: "Desingh Periyasamy"
path: "/albums/kannum-kannum-kollaiyadithaal-lyrics"
song: "Maagaa Maagaa"
image: ../../images/albumart/kannum-kannum-kollaiyadithaal.jpg
date: 2020-02-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/65rb-Rj5yx8"
type: "Love"
singers:
  - Janani S.V.
  - Kavya Ajit
  - Harshavardhan Rameshwar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">I Feel I Feel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel I Feel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Feel Like Flying In The Sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel Like Flying In The Sky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Feel Like Flying In The Sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel Like Flying In The Sky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Feel I Feel
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel I Feel"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Feel Like Flying In The Sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel Like Flying In The Sky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">I Feel Like Flying In The Sky
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="I Feel Like Flying In The Sky"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaan Thodum Paravai Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaan Thodum Paravai Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Manam Parakkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Manam Parakkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennadi Ivvulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennadi Ivvulagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Endru Alaikkudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Endru Alaikkudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugal Vannangalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Vannangalaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maaridum Neramidhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maaridum Neramidhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">En Mana Vaanil Pudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Mana Vaanil Pudhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaanavil Thondrudhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanavil Thondrudhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mamaa Maaga Mamaama Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamaa Maaga Mamaama Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaa Magaa Mammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaa Magaa Mammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mamaa Maaga Mamaama Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamaa Maaga Mamaama Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaa Magaa Mammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaa Magaa Mammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yethedho Aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethedho Aasaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mamaa Maaga Mamaama Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamaa Maaga Mamaama Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaa Magaa Mammaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaa Magaa Mammaa Aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mamaa Maaga Mamaama Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamaa Maaga Mamaama Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaa Magaa Mammaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaa Magaa Mammaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Netraiya Nodigalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netraiya Nodigalum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaatrinil Poi Vittadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrinil Poi Vittadhey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naalai Nammai Ingu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naalai Nammai Ingu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaa Vaa Endralaikkuthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa Vaa Endralaikkuthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thaai Madi Serndhidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaai Madi Serndhidum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Siru Pillai Aanene Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Pillai Aanene Naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kavalaigal En Manadhil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kavalaigal En Manadhil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ini Illai Illai Illaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ini Illai Illai Illaiye"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Porkkalam Pol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Porkkalam Pol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkaiyin Nodigal Yen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkaiyin Nodigal Yen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugal Ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Ellaam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mounamaai Ponadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounamaai Ponadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalam Un
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalam Un"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nanbanaai Maarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbanaai Maarume"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhvin Neelam Poga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhvin Neelam Poga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Inbam Inbam Inbam Inbam Thaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inbam Inbam Inbam Inbam Thaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho Aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho Aasaigal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaam Ma Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaam Ma Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mamaa Maaga Mamaama Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamaa Maaga Mamaama Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaa Magaa Mammaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaa Magaa Mammaa Aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mamaa Maaga Mamaama Maagaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamaa Maaga Mamaama Maagaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maamaa Magaa Mammaa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maamaa Magaa Mammaa Aaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yedhedho Aasaigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedhedho Aasaigal"/>
</div>
</pre>
