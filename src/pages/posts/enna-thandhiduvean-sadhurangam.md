---
title: "enna thandhiduvean song lyrics"
album: "Sadhurangam"
artist: "Vidyasagar"
lyricist: "Pa. Vijay"
director: "Karu Pazhaniappan"
path: "/albums/sadhurangam-lyrics"
song: "Enna Thandhiduvean"
image: ../../images/albumart/sadhurangam.jpg
date: 2011-10-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/w_wHver1OTc"
type: "love"
singers:
  - Karthik
  - Srilekha Parthasarathy
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Solla vanththai Solla vanthathai sollavillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla vanththai Solla vanthathai sollavillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollum varai sollum varai kaadhal thollai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollum varai sollum varai kaadhal thollai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna thanthiduven naan ennai thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thanthiduven naan ennai thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam thanthiduven naan uyirai thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam thanthiduven naan uyirai thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee vaanavil thanthaal naan vaanam thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee vaanavil thanthaal naan vaanam thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee or idam thanthaal naan ulagai thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee or idam thanthaal naan ulagai thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Un aayul kaalam theerum pothen aayul thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un aayul kaalam theerum pothen aayul thanthiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna thanthiduven naan ennai thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thanthiduven naan ennai thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam thanthiduven naan uyirai thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam thanthiduven naan uyirai thanthiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Viralgal nee thanthaal naan sparisam thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viralgal nee thanthaal naan sparisam thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal nee thanthaal naan kanavu thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal nee thanthaal naan kanavu thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nodigal nee thanthaal naan yugangal thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nodigal nee thanthaal naan yugangal thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Vidhaigal nee thanthaal naan virutcham thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vidhaigal nee thanthaal naan virutcham thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee koba paarvai paarkkum podhu konchal thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee koba paarvai paarkkum podhu konchal thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">En tholil vandhu neeyum saaya thottil thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En tholil vandhu neeyum saaya thottil thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee paarththudum podhu paaraamal naan paarvai thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee paarththudum podhu paaraamal naan paarvai thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pesidum podhu pesaamal naan mounam thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pesidum podhu pesaamal naan mounam thanthiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enna thanthiduven naan ennai thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna thanthiduven naan ennai thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullam thanthiduven naan uyirai thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullam thanthiduven naan uyirai thanthiduven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iragu nee thanthaal naan thogai thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iragu nee thanthaal naan thogai thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaigal nee thanthaal uyil regai thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaigal nee thanthaal uyil regai thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Boomi nee thanthaal than pookkal thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Boomi nee thanthaal than pookkal thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalaigal nee thanthaal Naan kiligal thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalaigal nee thanthaal Naan kiligal thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Un netri varudi kesam odhukki kaatru thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un netri varudi kesam odhukki kaatru thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee iruttil nadakka endhan vizhiyil velicham thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee iruttil nadakka endhan vizhiyil velicham thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee jannalin oram nindridum podhu saaral thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee jannalin oram nindridum podhu saaral thanthiduven"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thoongidum neram lesaai ketkum paadal thanthiduven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee thoongidum neram lesaai ketkum paadal thanthiduven"/>
</div>
</pre>
