---
title: "kaathirukkum koottathukku song lyrics"
album: "Kaathiruppor Pattiyal"
artist: "Sean Roldan"
lyricist: "Sean Roldan"
director: "Balaiya D. Rajasekhar"
path: "/albums/kaathiruppor-pattiyal-song-lyrics"
song: "Kaathirukkum Koottathukku"
image: ../../images/albumart/kaathiruppor-pattiyal.jpg
date: 2018-05-04
lang: tamil
youtubeLink: "https://www.youtube.com/embed/gcTBUSd2xjs"
type: "love"
singers:
  - Harihara Sudan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kathirukum kootathirku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathirukum kootathirku"/>
</div>
<div class="lyrico-lyrics-wrapper">katti thangam nee thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti thangam nee thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">kattangati kathu nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattangati kathu nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ketti karan naan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketti karan naan thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">sagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ye  pene un per thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye  pene un per thane"/>
</div>
<div class="lyrico-lyrics-wrapper">nan padum pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan padum pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">kanne nee konjam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne nee konjam than"/>
</div>
<div class="lyrico-lyrics-wrapper">kan jaada kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan jaada kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatu kaatu kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu kaatu kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">katti thangam nee thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti thangam nee thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">ketti karan naan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketti karan naan thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">katti thangam nee thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti thangam nee thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">ketti karan naan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketti karan naan thandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kitta vanthu enna konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kitta vanthu enna konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">thitti theethu poyendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thitti theethu poyendi"/>
</div>
<div class="lyrico-lyrics-wrapper">dakkunu than pakkam vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dakkunu than pakkam vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">pathikuven nan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathikuven nan thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">engathan ponalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="engathan ponalum"/>
</div>
<div class="lyrico-lyrics-wrapper">vida maaten nanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vida maaten nanum"/>
</div>
<div class="lyrico-lyrics-wrapper">okey nu sollu nee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="okey nu sollu nee "/>
</div>
<div class="lyrico-lyrics-wrapper">mattum than venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mattum than venum"/>
</div>
<div class="lyrico-lyrics-wrapper">venum venum venum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venum venum venum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thitti theethu poyendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thitti theethu poyendi"/>
</div>
<div class="lyrico-lyrics-wrapper">pathikuven nan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathikuven nan thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">thitti theethu poyendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thitti theethu poyendi"/>
</div>
<div class="lyrico-lyrics-wrapper">pathikuven nan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathikuven nan thandi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooru fulla kettu pathen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru fulla kettu pathen"/>
</div>
<div class="lyrico-lyrics-wrapper">ucha kattam nee thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ucha kattam nee thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">un pinne vandhu oonji 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un pinne vandhu oonji "/>
</div>
<div class="lyrico-lyrics-wrapper">poyi micham meethi aanendi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poyi micham meethi aanendi"/>
</div>
<div class="lyrico-lyrics-wrapper">yendi un pinnale suthuren 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yendi un pinnale suthuren "/>
</div>
<div class="lyrico-lyrics-wrapper">nanum oru varthai sonnalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanum oru varthai sonnalum"/>
</div>
<div class="lyrico-lyrics-wrapper">santhosam neelum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="santhosam neelum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathirukum kootathirku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathirukum kootathirku"/>
</div>
<div class="lyrico-lyrics-wrapper">katti thangam nee thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti thangam nee thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">kiliye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kiliye"/>
</div>
<div class="lyrico-lyrics-wrapper">kattangati kathu nikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kattangati kathu nikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ketti karan naan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketti karan naan thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">sagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">ye  pene un per thane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye  pene un per thane"/>
</div>
<div class="lyrico-lyrics-wrapper">nan padum pattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nan padum pattu"/>
</div>
<div class="lyrico-lyrics-wrapper">kanne nee konjam than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanne nee konjam than"/>
</div>
<div class="lyrico-lyrics-wrapper">kan jaada kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kan jaada kaatu"/>
</div>
<div class="lyrico-lyrics-wrapper">kaatu kaatu kaatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaatu kaatu kaatu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">katti thangam nee thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti thangam nee thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">ketti karan naan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketti karan naan thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">katti thangam nee thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti thangam nee thandi"/>
</div>
<div class="lyrico-lyrics-wrapper">ketti karan naan thandi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketti karan naan thandi"/>
</div>
</pre>
