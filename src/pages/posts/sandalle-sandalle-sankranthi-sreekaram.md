---
title: "sandalle sandalle song lyrics"
album: "Sreekaram"
artist: "Mickey J. Meyer"
lyricist: "Sanapati Bharadwaj Patrudu"
director: "Kishor B"
path: "/albums/sreekaram-lyrics"
song: "Sandalle Sandalle Sankranthi Sandalle"
image: ../../images/albumart/sreekaram.jpg
date: 2021-03-11
lang: telugu
youtubeLink: "https://www.youtube.com/embed/hxBQJPXZpEk"
type: "love"
singers:
  - Anurag Kulkarni
  - Mohana Bhogaraju
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sandhalle Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhalle Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Angaranga Vaibhavamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angaranga Vaibhavamgaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhalle Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhalle Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Angaranga Vaibhavamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angaranga Vaibhavamgaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Sandhalle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mana Ooritho Samayaannilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Ooritho Samayaannilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadipeyadam Oka Saradhaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadipeyadam Oka Saradhaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Vaaritho Kalisundadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Vaaritho Kalisundadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Varameraa Aa Aaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Varameraa Aa Aaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Maravani Choopulenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Maravani Choopulenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Nadipina Dhaarulennenno
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Nadipina Dhaarulennenno"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanu Malachina Ooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanu Malachina Ooru"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennenno Guruthulanichhinadhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenno Guruthulanichhinadhe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhalle Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhalle Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Angaranga Vaibhavamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angaranga Vaibhavamgaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhalle Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhalle Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Angaranga Vaibhavamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angaranga Vaibhavamgaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Sandhalle"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Muggumeedha Kaalu Veyyagaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Muggumeedha Kaalu Veyyagaane"/>
</div>
<div class="lyrico-lyrics-wrapper">Rayyimantu Kayyimanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rayyimantu Kayyimanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Aadapilla Mukku Meedhakochhe Kopam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aadapilla Mukku Meedhakochhe Kopam"/>
</div>
<div class="lyrico-lyrics-wrapper">Bhogimanta Mundhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bhogimanta Mundhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nilchonundi Challagaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilchonundi Challagaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Ontine Vechhagaa Thaakuthondhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ontine Vechhagaa Thaakuthondhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Thamburaalatho Chidatha Paadenanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thamburaalatho Chidatha Paadenanta"/>
</div>
<div class="lyrico-lyrics-wrapper">Gangireddhulaatalo Dolu Sannaayanta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gangireddhulaatalo Dolu Sannaayanta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pedda Pandagochhenoyantu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pedda Pandagochhenoyantu"/>
</div>
<div class="lyrico-lyrics-wrapper">Musthaabu Ayyindhi Choodaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Musthaabu Ayyindhi Choodaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru Ichhataa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Ichhataa"/>
</div>
<div class="lyrico-lyrics-wrapper">Inti Gadapa Undhi Swaagathinchadaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inti Gadapa Undhi Swaagathinchadaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Veedhi Arugu Undhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veedhi Arugu Undhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Maata Kalapadaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maata Kalapadaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Rachhabanda Undhi Theerpu Cheppadaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rachhabanda Undhi Theerpu Cheppadaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ooru Undhi Chintha Dheniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ooru Undhi Chintha Dheniki"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mana Ooritho Samayaannilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Ooritho Samayaannilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadipeyadam Oka Saradhaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadipeyadam Oka Saradhaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Vaaritho Kalisundadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Vaaritho Kalisundadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Varameraa Oo OoOo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Varameraa Oo OoOo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Debbalaatalona Odipothe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Debbalaatalona Odipothe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodipunju Poyyimeeda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodipunju Poyyimeeda"/>
</div>
<div class="lyrico-lyrics-wrapper">Kooralaagaa Thaanu Maadipodhaa Paapam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kooralaagaa Thaanu Maadipodhaa Paapam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nela Meedhi NUndi Gaalipatam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nela Meedhi NUndi Gaalipatam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ningi Dhaakaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ningi Dhaakaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhaarame Thokagaa Eguruthundhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhaarame Thokagaa Eguruthundhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Edla Bandipai Ekku Chinna Peddaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edla Bandipai Ekku Chinna Peddaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Golagola Cheyyadam Entha Baagundhata
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Golagola Cheyyadam Entha Baagundhata"/>
</div>
<div class="lyrico-lyrics-wrapper">Roju Maaripoyinnagaani Thaggedhi Ledhantaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roju Maaripoyinnagaani Thaggedhi Ledhantaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthataa Sambaraale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthataa Sambaraale"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vindhu Bhojanaalu Chesi Raavadaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vindhu Bhojanaalu Chesi Raavadaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Nachhinattu Oorilona Thiragadaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nachhinattu Oorilona Thiragadaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Anthamandhinokkasaari Kalavadaaniki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anthamandhinokkasaari Kalavadaaniki"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaalavanta Moodu Rojulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaalavanta Moodu Rojulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mana Ooritho Samayaannilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Ooritho Samayaannilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadipeyadam Oka Saradhaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadipeyadam Oka Saradhaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Vaaritho Kalisundadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Vaaritho Kalisundadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Varameraa Aa AaAa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Varameraa Aa AaAa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Ooritho Samayaannilaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Ooritho Samayaannilaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadipeyadam Oka Saradhaaraa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadipeyadam Oka Saradhaaraa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mana Vaaritho Kalisundadam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mana Vaaritho Kalisundadam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oka Varameraa Oo OoOo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oka Varameraa Oo OoOo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandhalle Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhalle Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Angaranga Vaibhavamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angaranga Vaibhavamgaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhalle Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhalle Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Sandhalle"/>
</div>
<div class="lyrico-lyrics-wrapper">Angaranga Vaibhavamgaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angaranga Vaibhavamgaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankranthi Sandhalle
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankranthi Sandhalle"/>
</div>
</pre>
