---
title: "secret window song lyrics"
album: "Pyaar Prema Kaadhal"
artist: "Yuvan Shankar Raja"
lyricist: "Elan"
director: "Elan"
path: "/albums/pyaar-prema-kaadhal-lyrics"
song: "Secret Window"
image: ../../images/albumart/pyaar-prema-kaadhal.jpg
date: 2018-08-10
lang: tamil
youtubeLink: "https://www.youtube.com/embed/a2Mov0NETK0"
type: "love"
singers:
  - Al Rufian
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nilavae naan theyuren kayuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavae naan theyuren kayuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo ithu thaan kaadhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo ithu thaan kaadhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae naan yenguren nonguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae naan yenguren nonguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal ennai paarkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal ennai paarkumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katti anaikuren unnai manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti anaikuren unnai manasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti munaguren thoonga manasilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutti munaguren thoonga manasilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla nerunguren thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla nerunguren thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuraiyala puriyalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuraiyala puriyalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyalayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thikki thinaruren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikki thinaruren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa mozhi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa mozhi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli thiriyiren poga vazhi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli thiriyiren poga vazhi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella uraiyuren neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella uraiyuren neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerala puriyalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerala puriyalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyalayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavae naan theyuren kayuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavae naan theyuren kayuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo ithu thaan kaadhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo ithu thaan kaadhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae naan yenguren nonguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae naan yenguren nonguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal ennai paarkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal ennai paarkumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Neeyae enai patriya theeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyae enai patriya theeyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeraai enai sutriya theevae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeraai enai sutriya theevae"/>
</div>
<div class="lyrico-lyrics-wrapper">Engeyo kooti sellumKaram nee thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engeyo kooti sellumKaram nee thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En varam neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En varam neethaan"/>
</div>
<div class="lyrico-lyrics-wrapper">En jannal thiranthu vaithen naanae aeey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En jannal thiranthu vaithen naanae aeey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavae naan theyuren kayuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavae naan theyuren kayuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo ithu thaan kaadhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo ithu thaan kaadhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae naan yenguren nonguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae naan yenguren nonguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal ennai paarkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal ennai paarkumo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katti anaikuren unnai manasula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katti anaikuren unnai manasula"/>
</div>
<div class="lyrico-lyrics-wrapper">Mutti munaguren thoonga manasilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mutti munaguren thoonga manasilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla nerunguren thooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla nerunguren thooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuraiyala puriyalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuraiyala puriyalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyalayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thikki thinaruren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thikki thinaruren"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa mozhi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa mozhi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thulli thiriyiren poga vazhi illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thulli thiriyiren poga vazhi illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mella uraiyuren neram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mella uraiyuren neram"/>
</div>
<div class="lyrico-lyrics-wrapper">Theerala puriyalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theerala puriyalayae"/>
</div>
<div class="lyrico-lyrics-wrapper">Puriyalayae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puriyalayae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nilavae naan theyuren kayuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nilavae naan theyuren kayuren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyo ithu thaan kaadhalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyo ithu thaan kaadhalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae naan yenguren nonguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae naan yenguren nonguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangal ennai paarkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangal ennai paarkumo"/>
</div>
</pre>
