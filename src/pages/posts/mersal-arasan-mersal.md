---
title: "mersal arasan song lyrics"
album: "Mersal"
artist: "A.R. Rahman"
lyricist: "Vivek"
director: "Atlee"
path: "/albums/mersal-lyrics"
song: "Mersal Arasan"
image: ../../images/albumart/mersal.jpg
date: 2017-10-18
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Wxqu1eVJ4Vs"
type: "Mass Intro"
singers:
  - G.V. Prakash Kumar
  - Naresh Iyer
  - Sharanya Srinivas
  - Vishwaprasadh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Adchchu Gaali Pannum Dhillu Dhillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adchchu Gaali Pannum Dhillu Dhillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pudichchu Kuda Nippom Sollu Sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudichchu Kuda Nippom Sollu Sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">Isthu Keeya Vudum Allu Sillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isthu Keeya Vudum Allu Sillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yei Allu Sillu Setharu Setharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yei Allu Sillu Setharu Setharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ada Vaanaam Vaanaamma Allu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ada Vaanaam Vaanaamma Allu"/>
</div>
<div class="lyrico-lyrics-wrapper">Side Valuchukko Sillu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Side Valuchukko Sillu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalapathy Entry Idhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalapathy Entry Idhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Setharu Setharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Setharu Setharu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Secene Aavum Avan Vantaannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Secene Aavum Avan Vantaannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Podi Iskoolu Pullingo Setharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi Iskoolu Pullingo Setharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theatru Therikka Yaaringa Gelikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theatru Therikka Yaaringa Gelikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Solti Bigiladi Meral Arasan Vaaraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solti Bigiladi Meral Arasan Vaaraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaa Vaaraan Scenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaa Vaaraan Scenu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suguraa Poluppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suguraa Poluppaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Periya Kainnaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Periya Kainnaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Peri Eduppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Peri Eduppaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottu Steppa Vasta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Steppa Vasta"/>
</div>
<div class="lyrico-lyrics-wrapper">All Centru Adhagalam Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="All Centru Adhagalam Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Edthu Keesi Paaththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edthu Keesi Paaththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaththi Saarpu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Saarpu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaththi Aanaa Keechathilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaththi Aanaa Keechathilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Noi Vettum Saami Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Noi Vettum Saami Thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Yezha Paala Vaazha Veppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yezha Paala Vaazha Veppaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Keenja Vaazhka Theppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Keenja Vaazhka Theppaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Anachchu Nippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anachchu Nippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Thalaivan Aada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalaivan Aada"/>
</div>
<div class="lyrico-lyrics-wrapper">Isaipuyal Onnu Pirikkuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Isaipuyal Onnu Pirikkuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Eguru Allu Sillu Etti Setharanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eguru Allu Sillu Etti Setharanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eguru Allu Sillu Etti Saetharanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eguru Allu Sillu Etti Saetharanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eguru Allu Sillu Etti Setharanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eguru Allu Sillu Etti Setharanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Allu Sillu Setharanumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allu Sillu Setharanumda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manushan Undaakkum Ellaam Saayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manushan Undaakkum Ellaam Saayum"/>
</div>
<div class="lyrico-lyrics-wrapper">Panam Mattum Enna Athu Verum Maayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panam Mattum Enna Athu Verum Maayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuththa Thaandi Uththu Paaththaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuththa Thaandi Uththu Paaththaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvum Paperu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvum Paperu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vanangi Santhosam Kekkura Neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vanangi Santhosam Kekkura Neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirumbi Paar Suththi Aayiram Kaayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirumbi Paar Suththi Aayiram Kaayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavichcha Manasil Sirippa Vethachchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavichcha Manasil Sirippa Vethachchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Manusan Neethaandaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manusan Neethaandaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paasam Kaatti Pinnaal Vanthaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paasam Kaatti Pinnaal Vanthaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiya Anbaa Koppen daa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiya Anbaa Koppen daa"/>
</div>
<div class="lyrico-lyrics-wrapper">Aduththa Usura Vaazha Vechchaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aduththa Usura Vaazha Vechchaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil Vachchu Kaappenda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil Vachchu Kaappenda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Secene Aavum Avan Vantaannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Secene Aavum Avan Vantaannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Podi Iskoolu Pullingo Setharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podi Iskoolu Pullingo Setharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Theatru Therikka Yaaringa Gelikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theatru Therikka Yaaringa Gelikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Solti Bigiladi Meral Arasan Vaaraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solti Bigiladi Meral Arasan Vaaraan"/>
</div>
<div class="lyrico-lyrics-wrapper">Inthaa Vaaraan Scenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Inthaa Vaaraan Scenu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Suguraa Poluppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Suguraa Poluppaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Perya Kainnaalum Perti Eduppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Perya Kainnaalum Perti Eduppaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thottu Steppa Vasta All Centru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thottu Steppa Vasta All Centru"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhagalam Dhaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhagalam Dhaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Edthu Keesi Paaththaa Kaththi Saarpu Thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edthu Keesi Paaththaa Kaththi Saarpu Thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ae Eguru Allu Sillu Etti Setharanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ae Eguru Allu Sillu Etti Setharanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eguru Allu Sillu Etti Setharanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eguru Allu Sillu Etti Setharanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eguru Allu Sillu Etti Setharanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eguru Allu Sillu Etti Setharanum"/>
</div>
<div class="lyrico-lyrics-wrapper">Allu Sillu Setharanumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Allu Sillu Setharanumda"/>
</div>
</pre>
