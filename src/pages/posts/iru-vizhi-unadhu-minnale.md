---
title: "iru vizhi unadhu song lyrics"
album: "Minnale"
artist: "Harris Jayaraj"
lyricist: "Thamarai"
director: "Gautham Menon"
path: "/albums/minnale-lyrics"
song: "Iru Vizhi Unadhu"
image: ../../images/albumart/minnale.jpg
date: 2001-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/ErR1MgdYGrY"
type: "sad"
singers:
  - Devan Ekambaram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Iru Vizhi Unadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Vizhi Unadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaigalum Unadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigalum Unadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugal Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhe Enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhe Enadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Vizhi Unadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Vizhi Unadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaigalum Unadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigalum Unadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugal Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhe Enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhe Enadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatkal Neeluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatkal Neeluthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Engo Ponathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Engo Ponathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Thandanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thandanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan inge Vaazhvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan inge Vaazhvathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Niyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Niyabagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Niyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Niyabagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iru Vizhi Unadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iru Vizhi Unadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Imaigalum Unadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Imaigalum Unadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavugal Mattum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavugal Mattum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhe Enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhe Enadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naatkal Neeluthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naatkal Neeluthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Engo Ponathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Engo Ponathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yen Thandanai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yen Thandanai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan inge Vaazhvathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan inge Vaazhvathum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoo Oh Orey Niyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo Oh Orey Niyabagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohoo Oh Unthan Niyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohoo Oh Unthan Niyabagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal Kaayam Nerumbothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Kaayam Nerumbothu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thookkam Ingu Yethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thookkam Ingu Yethu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Niyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Niyabagam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orey Niyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orey Niyabagam"/>
</div>
</pre>
