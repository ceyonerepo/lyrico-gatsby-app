---
title: "venpani malare female version song lyrics"
album: "Pa Paandi"
artist: "Sean Roldan"
lyricist: "Dhanush"
director: "Dhanush"
path: "/albums/pa-paandi-lyrics"
song: "Venpani Malare Female Version"
image: ../../images/albumart/pa-paandi.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rXmnjuZ6KC4"
type: "happy"
singers:
  -	Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Venpani malarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venpani malarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaasam uyiril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaasam uyiril"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu swavasam tharuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu swavasam tharuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un iru vizhiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un iru vizhiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">En aaiyul reghai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aaiyul reghai"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu vazhvu peruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu vazhvu peruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalangal oiyntha pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangal oiyntha pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Valibam theintha pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valibam theintha pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kucham dhaan enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kucham dhaan enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katril parakkum kathaadi nanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katril parakkum kathaadi nanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu vayathaai koothaadinenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu vayathaai koothaadinenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaintha ilai naan pachai aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaintha ilai naan pachai aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalaivanam naan neer vizhchi aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalaivanam naan neer vizhchi aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katril parakkum kathaadi nanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katril parakkum kathaadi nanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu vayathaai koothaadinenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu vayathaai koothaadinenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaintha ilai naan pachai aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaintha ilai naan pachai aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalaivanam naan neer vizhchi aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalaivanam naan neer vizhchi aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venpani malarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venpani malarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un iru vizhiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un iru vizhiyal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thediya tharunangal ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thediya tharunangal ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thediyae varukirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thediyae varukirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhegathin surukangal ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhegathin surukangal ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">Sirikindrathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sirikindrathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vandhathum vazhndhathum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vandhathum vazhndhathum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan munnae therikirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan munnae therikirathae"/>
</div>
<div class="lyrico-lyrics-wrapper">Andha naal gnyabagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andha naal gnyabagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjilae pookkirathae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjilae pookkirathae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Baaram paintha nenjukulae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Baaram paintha nenjukulae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eeram paiyuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eeram paiyuthae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naraigalum marainthidavae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naraigalum marainthidavae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venpani malarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venpani malarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un iru vizhiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un iru vizhiyal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venpani malarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venpani malarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Un vaasam uyiril
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un vaasam uyiril"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu swavasam tharuthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu swavasam tharuthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un iru vizhiyal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un iru vizhiyal"/>
</div>
<div class="lyrico-lyrics-wrapper">En aaiyul reghai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En aaiyul reghai"/>
</div>
<div class="lyrico-lyrics-wrapper">Puthu vazhvu peruthae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Puthu vazhvu peruthae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalangal oiyntha pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalangal oiyntha pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal enna"/>
</div>
<div class="lyrico-lyrics-wrapper">Valibam theintha pinnum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Valibam theintha pinnum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kucham dhaan enna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kucham dhaan enna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katril parakkum kathaadi nanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katril parakkum kathaadi nanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu vayathaai koothaadinenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu vayathaai koothaadinenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaintha ilai naan pachai aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaintha ilai naan pachai aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalaivanam naan neer vizhchi aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalaivanam naan neer vizhchi aanen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Katril parakkum kathaadi nanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katril parakkum kathaadi nanae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu vayathaai koothaadinenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu vayathaai koothaadinenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaintha ilai naan pachai aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaintha ilai naan pachai aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalaivanam naan neer vizhchi aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalaivanam naan neer vizhchi aanen"/>
</div>
</pre>
