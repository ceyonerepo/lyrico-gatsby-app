---
title: "kabadadaari song lyrics"
album: "Kabadadaari"
artist: "Simon K.King"
lyricist: "Arun Bharathi"
director: "Pradeep Krishnamoorthy"
path: "/albums/kabadadaari-song-lyrics"
song: "Kabadadaari"
image: ../../images/albumart/kabadadaari.jpg
date: 2021-01-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/bz6GOpogP3M"
type: "Motivational"
singers:
  - Niranj Suresh
  - Shilvi Sharon
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">puriyaatha paathai ondru therigirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puriyaatha paathai ondru therigirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">payanangal thoorangal thaaninge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="payanangal thoorangal thaaninge"/>
</div>
<div class="lyrico-lyrics-wrapper">theriyaatha vettai ondru thodarkirathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theriyaatha vettai ondru thodarkirathe"/>
</div>
<div class="lyrico-lyrics-wrapper">vegam neekondu modhinge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vegam neekondu modhinge"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">unnai pole iruppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai pole iruppaan"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan nizhalai nadappaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan nizhalai nadappaan"/>
</div>
<div class="lyrico-lyrics-wrapper">nanban pole sirippan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nanban pole sirippan"/>
</div>
<div class="lyrico-lyrics-wrapper">thinamum thinamum ivane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thinamum thinamum ivane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pinnal valaiyai virippaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pinnal valaiyai virippaan"/>
</div>
<div class="lyrico-lyrics-wrapper">undhan thalaiyai eduppaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undhan thalaiyai eduppaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kannai kattipogum avane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannai kattipogum avane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kabadadaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadadaari"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadadaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadadaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">adaiyaalam theriyaa ondru thuraththidave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adaiyaalam theriyaa ondru thuraththidave"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjam alaipola thadumaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjam alaipola thadumaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">nari vedam pottukondu padhungidume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nari vedam pottukondu padhungidume"/>
</div>
<div class="lyrico-lyrics-wrapper">kapadam unakkullum vilaiyaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kapadam unakkullum vilaiyaadum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">saayam poosum ulagail
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saayam poosum ulagail"/>
</div>
<div class="lyrico-lyrics-wrapper">niyayam needhi theruvil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="niyayam needhi theruvil"/>
</div>
<div class="lyrico-lyrics-wrapper">thaayam pole unamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaayam pole unamai"/>
</div>
<div class="lyrico-lyrics-wrapper">suzhalum suzhalum thinamum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suzhalum suzhalum thinamum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">soozhachi kannai maraikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soozhachi kannai maraikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">suyanalam konde nadikkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suyanalam konde nadikkum"/>
</div>
<div class="lyrico-lyrics-wrapper">puththan pondra eththan avane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="puththan pondra eththan avane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kabadadaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadadaari"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadadaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadadaari"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kazhugin kangal konde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kazhugin kangal konde"/>
</div>
<div class="lyrico-lyrics-wrapper">kadandhu povaan ivan ange
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadandhu povaan ivan ange"/>
</div>
<div class="lyrico-lyrics-wrapper">kavanam sidharum inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavanam sidharum inge"/>
</div>
<div class="lyrico-lyrics-wrapper">vazhimuraigal aththanaiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vazhimuraigal aththanaiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">kondrupudhaippavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kondrupudhaippavan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kabadadaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadadaari"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadadaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadadaari"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadadaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadadaari"/>
</div>
<div class="lyrico-lyrics-wrapper">kabadadaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kabadadaari"/>
</div>
</pre>
