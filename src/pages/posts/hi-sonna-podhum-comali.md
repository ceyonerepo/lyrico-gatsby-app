---
title: 'hi sonna podhum song lyrics'
album: 'Comali'
artist: 'Hiphop Tamizha'
lyricist: 'Pradeep Ranganathan'
director: 'Pradeep Ranganathan'
path: '/albums/comali-song-lyrics'
song: 'Hi Sonna Podhum'
image: ../../images/albumart/comali.jpg
date: 2019-08-15
lang: tamil
youtubeLink: "https://www.youtube.com/embed/G_UEVscPspo"
type: 'love'
singers: 
- Kaushik krish
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
  <div class="lyrico-lyrics-wrapper">Nee hai sonna podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee hai sonna podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru bodha onnu yerum
<input type="checkbox" class="lyrico-select-lyric-line" value="Oru bodha onnu yerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee thottalae podhum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee thottalae podhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Manam jivvu-nu thaan aagum
<input type="checkbox" class="lyrico-select-lyric-line" value="Manam jivvu-nu thaan aagum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee sirichaalum morchaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee sirichaalum morchaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Heart-u beat-u yerum
<input type="checkbox" class="lyrico-select-lyric-line" value="Heart-u beat-u yerum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vekkam maanam edhuvum ilama
<input type="checkbox" class="lyrico-select-lyric-line" value="Vekkam maanam edhuvum ilama"/>
</div>
<div class="lyrico-lyrics-wrapper">Pinnadi suthuven naanum
<input type="checkbox" class="lyrico-select-lyric-line" value="Pinnadi suthuven naanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey cycle-u dhaan vehicle-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey cycle-u dhaan vehicle-u"/>
</div>
<div class="lyrico-lyrics-wrapper">School bathroom sevathula kirukkalu
<input type="checkbox" class="lyrico-select-lyric-line" value="School bathroom sevathula kirukkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Canteenukku vara sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="Canteenukku vara sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">En billa avalaiyae thara sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="En billa avalaiyae thara sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ava pogum bodhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava pogum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En pera kaththu
<input type="checkbox" class="lyrico-select-lyric-line" value="En pera kaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava sirichitaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava sirichitaana"/>
</div>
<div class="lyrico-lyrics-wrapper">En love-u settu
<input type="checkbox" class="lyrico-select-lyric-line" value="En love-u settu"/>
</div>
<div class="lyrico-lyrics-wrapper">En classukulla
<input type="checkbox" class="lyrico-select-lyric-line" value="En classukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan romba veththu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan romba veththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini aaga poren da
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini aaga poren da"/>
</div>
<div class="lyrico-lyrics-wrapper">School-u geththu
<input type="checkbox" class="lyrico-select-lyric-line" value="School-u geththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naa summavae scene-u di
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa summavae scene-u di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini schoolukku don-u di (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini schoolukku don-u di"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naa summavae scene-u di
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa summavae scene-u di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini schoolukku don-u di (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini schoolukku don-u di"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Book-u mela
<input type="checkbox" class="lyrico-select-lyric-line" value="Book-u mela"/>
</div>
<div class="lyrico-lyrics-wrapper">Book ah veppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Book ah veppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pogum bodhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Nee pogum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Look-ah veppen
<input type="checkbox" class="lyrico-select-lyric-line" value="Look-ah veppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Nalla paiyan
<input type="checkbox" class="lyrico-select-lyric-line" value="Nalla paiyan"/>
</div>
<div class="lyrico-lyrics-wrapper">Pola nadippen
<input type="checkbox" class="lyrico-select-lyric-line" value="Pola nadippen"/>
</div>
<div class="lyrico-lyrics-wrapper">Edamirunthaalum
<input type="checkbox" class="lyrico-select-lyric-line" value="Edamirunthaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna idippen
<input type="checkbox" class="lyrico-select-lyric-line" value="Unna idippen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ink bottle manasu unakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Ink bottle manasu unakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulla kaadhal kotti kedakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Ulla kaadhal kotti kedakku"/>
</div>
<div class="lyrico-lyrics-wrapper">Ink pennu summa irukku
<input type="checkbox" class="lyrico-select-lyric-line" value="Ink pennu summa irukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal ah dhaan oothu enakku
<input type="checkbox" class="lyrico-select-lyric-line" value="Kaadhal ah dhaan oothu enakku"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Goli urunda kannu size-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Goli urunda kannu size-u"/>
</div>
<div class="lyrico-lyrics-wrapper">Rollu cap ah vedikkuthu manasu
<input type="checkbox" class="lyrico-select-lyric-line" value="Rollu cap ah vedikkuthu manasu"/>
</div>
<div class="lyrico-lyrics-wrapper">Callu panni kurala kettu
<input type="checkbox" class="lyrico-select-lyric-line" value="Callu panni kurala kettu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thookaththukku vechaa vettu
<input type="checkbox" class="lyrico-select-lyric-line" value="Thookaththukku vechaa vettu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Pakathu class-u pasanga munnaal
<input type="checkbox" class="lyrico-select-lyric-line" value="Pakathu class-u pasanga munnaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhilla nippen di
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhilla nippen di"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhilla nippen di
<input type="checkbox" class="lyrico-select-lyric-line" value="Dhilla nippen di"/>
</div>
<div class="lyrico-lyrics-wrapper">Vera evana vambu panna
<input type="checkbox" class="lyrico-select-lyric-line" value="Vera evana vambu panna"/>
</div>
<div class="lyrico-lyrics-wrapper">Palla odaippen di
<input type="checkbox" class="lyrico-select-lyric-line" value="Palla odaippen di"/>
</div>
<div class="lyrico-lyrics-wrapper">Palla odaippen di
<input type="checkbox" class="lyrico-select-lyric-line" value="Palla odaippen di"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naa summavae scene-u di
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa summavae scene-u di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini schoolukku don-u di (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini schoolukku don-u di"/></div>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Hey cycle-u dhaan vehicle-u
<input type="checkbox" class="lyrico-select-lyric-line" value="Hey cycle-u dhaan vehicle-u"/>
</div>
<div class="lyrico-lyrics-wrapper">School bathroom sevathula kirukkalu
<input type="checkbox" class="lyrico-select-lyric-line" value="School bathroom sevathula kirukkalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Canteenukku vara sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="Canteenukku vara sollu"/>
</div>
<div class="lyrico-lyrics-wrapper">En billa avalaiyae thara sollu
<input type="checkbox" class="lyrico-select-lyric-line" value="En billa avalaiyae thara sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Ava pogum bodhu
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava pogum bodhu"/>
</div>
<div class="lyrico-lyrics-wrapper">En pera kaththu
<input type="checkbox" class="lyrico-select-lyric-line" value="En pera kaththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ava sirichitaana
<input type="checkbox" class="lyrico-select-lyric-line" value="Ava sirichitaana"/>
</div>
<div class="lyrico-lyrics-wrapper">En love-u settu
<input type="checkbox" class="lyrico-select-lyric-line" value="En love-u settu"/>
</div>
<div class="lyrico-lyrics-wrapper">En classukulla
<input type="checkbox" class="lyrico-select-lyric-line" value="En classukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan romba veththu
<input type="checkbox" class="lyrico-select-lyric-line" value="Naan romba veththu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini aaga poren da
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini aaga poren da"/>
</div>
<div class="lyrico-lyrics-wrapper">School-u geththu
<input type="checkbox" class="lyrico-select-lyric-line" value="School-u geththu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
  <div class="lyrico-lyrics-wrapper">Naa summavae scene-u di
<input type="checkbox" class="lyrico-select-lyric-line" value="Naa summavae scene-u di"/>
</div>
<div class="lyrico-lyrics-wrapper">Ini schoolukku don-u di (2 times)</div>
<input type="checkbox" class="lyrico-select-lyric-line" value="Ini schoolukku don-u di"/></div>
</div>
</pre>