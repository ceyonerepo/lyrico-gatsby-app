---
title: "valaiyadi valaiyai song lyrics"
album: "Theethum Nandrum"
artist: "C. Sathya"
lyricist: "Muthamizh"
director: "Rasu Ranjith"
path: "/albums/theethum-nandrum-lyrics"
song: "Valaiyadi Valaiyai"
image: ../../images/albumart/theethum-nandrum.jpg
date: 2021-03-12
lang: tamil
youtubeLink: "https://www.youtube.com/embed/w7pUZx4HNdg"
type: "Celebration"
singers:
  - Gana Bala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">valaiyadi valaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valaiyadi valaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">neenga valanume jodiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga valanume jodiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">aalukoru pathiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalukoru pathiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">neenga aalanume aasaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga aalanume aasaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pulli vacha kolamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulli vacha kolamai"/>
</div>
<div class="lyrico-lyrics-wrapper">pookanume solaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookanume solaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">ullirunthu pesanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullirunthu pesanum"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu senthu osaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu senthu osaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pulli vacha kolamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulli vacha kolamai"/>
</div>
<div class="lyrico-lyrics-wrapper">pookanume solaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookanume solaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">ullirunthu pesanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullirunthu pesanum"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu senthu osaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu senthu osaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">valaiyadi valaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valaiyadi valaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">neenga valanume jodiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga valanume jodiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">aalukoru pathiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalukoru pathiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">neenga aalanume aasaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga aalanume aasaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">alagana aarambam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alagana aarambam"/>
</div>
<div class="lyrico-lyrics-wrapper">athivega santhosam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athivega santhosam"/>
</div>
<div class="lyrico-lyrics-wrapper">anbale neenga allikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="anbale neenga allikanum"/>
</div>
<div class="lyrico-lyrics-wrapper">arivaga konjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arivaga konjam"/>
</div>
<div class="lyrico-lyrics-wrapper">thelivaga ellam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thelivaga ellam"/>
</div>
<div class="lyrico-lyrics-wrapper">kanivaga nenjam sinthikanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanivaga nenjam sinthikanum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">suruku suruku nu kovapadathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="suruku suruku nu kovapadathe"/>
</div>
<div class="lyrico-lyrics-wrapper">naruku naruku nu pesi thallathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naruku naruku nu pesi thallathe"/>
</div>
<div class="lyrico-lyrics-wrapper">ne suruku suruku nu kovapadathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne suruku suruku nu kovapadathe"/>
</div>
<div class="lyrico-lyrics-wrapper">naruku naruku nu pesi thallathe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naruku naruku nu pesi thallathe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ne kedacha valkaiya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ne kedacha valkaiya "/>
</div>
<div class="lyrico-lyrics-wrapper">padichu paaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="padichu paaru"/>
</div>
<div class="lyrico-lyrics-wrapper">naduchu vaalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naduchu vaalama"/>
</div>
<div class="lyrico-lyrics-wrapper">piduchu vaalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="piduchu vaalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ammi mithuchu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ammi mithuchu"/>
</div>
<div class="lyrico-lyrics-wrapper">arunthathi paathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arunthathi paathu"/>
</div>
<div class="lyrico-lyrics-wrapper">iyar thaaliya 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iyar thaaliya "/>
</div>
<div class="lyrico-lyrics-wrapper">kodutharu ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kodutharu ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">gold chain la than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gold chain la than"/>
</div>
<div class="lyrico-lyrics-wrapper">thaaliya then matigunu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaaliya then matigunu"/>
</div>
<div class="lyrico-lyrics-wrapper">fashion ah thiriyuthunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="fashion ah thiriyuthunga"/>
</div>
<div class="lyrico-lyrics-wrapper">ipo ipo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ipo ipo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaasu panatha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasu panatha "/>
</div>
<div class="lyrico-lyrics-wrapper">pathu pathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pathu pathu"/>
</div>
<div class="lyrico-lyrics-wrapper">kannan karuthuma sethukanga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannan karuthuma sethukanga"/>
</div>
<div class="lyrico-lyrics-wrapper">kallam kabadam illama than
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kallam kabadam illama than"/>
</div>
<div class="lyrico-lyrics-wrapper">katti unmaiya kaapathunga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katti unmaiya kaapathunga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yogam vantha sogam vidu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yogam vantha sogam vidu"/>
</div>
<div class="lyrico-lyrics-wrapper">sogam vantha thola kodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sogam vantha thola kodu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjam koduthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjam koduthu "/>
</div>
<div class="lyrico-lyrics-wrapper">vanjatha othuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vanjatha othuku"/>
</div>
<div class="lyrico-lyrics-wrapper">vitu kodutha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vitu kodutha "/>
</div>
<div class="lyrico-lyrics-wrapper">ketu than pogathu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ketu than pogathu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">valaiyadi valaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valaiyadi valaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">neenga valanume jodiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga valanume jodiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">aalukoru pathiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aalukoru pathiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">neenga aalanume aasaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neenga aalanume aasaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">pulli vacha kolamai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pulli vacha kolamai"/>
</div>
<div class="lyrico-lyrics-wrapper">pookanume solaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookanume solaiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">ullirunthu pesanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullirunthu pesanum"/>
</div>
<div class="lyrico-lyrics-wrapper">onnu senthu osaiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="onnu senthu osaiyai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thanana nane nanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanana nane nanana"/>
</div>
<div class="lyrico-lyrics-wrapper">thanana nane nanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanana nane nanana"/>
</div>
<div class="lyrico-lyrics-wrapper">thanana nane nanana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thanana nane nanana"/>
</div>
</pre>
