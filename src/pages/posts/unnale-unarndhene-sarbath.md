---
title: "unnale unarndhene song lyrics"
album: "Sarbath"
artist: "Ajesh"
lyricist: "Ku. Karthick"
director: "Prabhakaran"
path: "/albums/sarbath-song-lyrics"
song: "Unnale Unarndhene"
image: ../../images/albumart/sarbath.jpg
date: 2021-04-11
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ylo_6bsjMwo"
type: "Love"
singers:
  - Ajesh
  - Haricharan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ha Idhayangal Thara Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Idhayangal Thara Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Udayaamal Thoda Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Udayaamal Thoda Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Thuyarangal Marappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Thuyarangal Marappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Uyarathil Parappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uyarathil Parappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Konjam Medhuva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Konjam Medhuva"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ha Manamengum Siragaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Manamengum Siragaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Medhuvaaga Thira Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Medhuvaaga Thira Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ha Idhu Otti Pirandhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ha Idhu Otti Pirandhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhai Vittu Kadandhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhai Vittu Kadandhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjai Thoda Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjai Thoda Vaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kai Meerum Manadhai… Kai Thooki Vidavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kai Meerum Manadhai… Kai Thooki Vidavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaanaadha Nodiyai… Kadandhu Kadandhu Varavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaanaadha Nodiyai… Kadandhu Kadandhu Varavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Meerum Thuliyai… Konjam Thudaithida Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Meerum Thuliyai… Konjam Thudaithida Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerum Inithaal… Ulagam Adhai Vidumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerum Inithaal… Ulagam Adhai Vidumaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnale Unarndhene… Nizhalaagum Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnale Unarndhene… Nizhalaagum Urave"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivai Sumandhe… Nedundhooram Nadappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivai Sumandhe… Nedundhooram Nadappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaale Urumaarum… Indha Vazhkai Pudhire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaale Urumaarum… Indha Vazhkai Pudhire"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai Marandhe… Pudhu Ellai Kadappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Marandhe… Pudhu Ellai Kadappom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Van Kanneer Thuli Jannal Pani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Van Kanneer Thuli Jannal Pani"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelaadhada Adhai Thurandhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelaadhada Adhai Thurandhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaayam Thannai Kaatril Karaindhidum Kaalam Allava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaayam Thannai Kaatril Karaindhidum Kaalam Allava"/>
</div>
<div class="lyrico-lyrics-wrapper">Neer Thaazhaamale Thayin Madi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer Thaazhaamale Thayin Madi"/>
</div>
<div class="lyrico-lyrics-wrapper">Seraadhadaa Tharai Vizhundha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seraadhadaa Tharai Vizhundha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pdhum Ennai Thaangi Pidithidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pdhum Ennai Thaangi Pidithidum"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanbanaaga Vaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanbanaaga Vaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Nam Karuvizhi Kalanguvadhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nam Karuvizhi Kalanguvadhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kan Viralukku Solvadhillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kan Viralukku Solvadhillai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un Aruginil Nadakkayil… Valigalum Pidikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Aruginil Nadakkayil… Valigalum Pidikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhaivida Edhuvumillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhaivida Edhuvumillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanai Murai Azhudhiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanai Murai Azhudhiruppom"/>
</div>
<div class="lyrico-lyrics-wrapper">Athanai Murai Adhai Marandhom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athanai Murai Adhai Marandhom"/>
</div>
<div class="lyrico-lyrics-wrapper">Akkarai Endrum Ucham Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Akkarai Endrum Ucham Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Iruppadhum Thucham Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iruppadhum Thucham Illai"/>
</div>
<div class="lyrico-lyrics-wrapper">Nimidangal Rasithiruppom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nimidangal Rasithiruppom"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnale Unarndhene… Nizhalaagum Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnale Unarndhene… Nizhalaagum Urave"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivai Sumandhe… Nedundhooram Nadappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivai Sumandhe… Nedundhooram Nadappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaale Urumaarum… Indha Vazhkai Pudhire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaale Urumaarum… Indha Vazhkai Pudhire"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravodum Pagalodum Kalandhidum Anbennum Velicham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravodum Pagalodum Kalandhidum Anbennum Velicham"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnale Unarndhene… Nizhalaagum Urave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnale Unarndhene… Nizhalaagum Urave"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaivai Sumandhe… Nedundhooram Nadappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaivai Sumandhe… Nedundhooram Nadappom"/>
</div>
<div class="lyrico-lyrics-wrapper">Thannaale Urumaarum… Indha Vazhkai Pudhire
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thannaale Urumaarum… Indha Vazhkai Pudhire"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulagai Marandhe… Pudhu Ellai Kadappom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagai Marandhe… Pudhu Ellai Kadappom"/>
</div>
</pre>
