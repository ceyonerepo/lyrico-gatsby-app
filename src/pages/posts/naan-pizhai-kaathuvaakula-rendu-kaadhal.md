---
title: "naan pizhai song lyrics"
album: "Kaathuvaakula Rendu Kaadhal"
artist: "Anirudh Ravichander"
lyricist: "Vignesh Shivan"
director: "Vignesh Shivan"
path: "/albums/kaathuvaakula-rendu-kaadhal-lyrics"
song: "Naan Pizhai"
image: ../../images/albumart/kaathuvaakula-rendu-kaadhal.jpg
date: 2022-04-28
lang: tamil
youtubeLink: "https://www.youtube.com/embed/r9lR5hLgEnI"
type: "love"
singers:
  - Ravi G
  - Shashaa Tirupati
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Naan Pizhai Nee Mazhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pizhai Nee Mazhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkul Nee Irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkul Nee Irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Thavarey Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Thavarey Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ilai Naan Paruva Mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ilai Naan Paruva Mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Siru Thuliyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Siru Thuliyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhum Tharunam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhum Tharunam Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhiyil Irundhu Alasi Eduthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhiyil Irundhu Alasi Eduthene"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaikalam Amaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaikalam Amaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagundhavan Dhaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagundhavan Dhaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Azhaga Siricha Mugame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Azhaga Siricha Mugame"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Nenacha Thonum Idame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Nenacha Thonum Idame"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Azhaga Siricha Mugame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Azhaga Siricha Mugame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenacha Thonum Idame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenacha Thonum Idame"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Pirandha Dhiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pirandha Dhiname"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedacha Varamae Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedacha Varamae Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Pizhai Nee Mazhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pizhai Nee Mazhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkul Nee Irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkul Nee Irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Thavarey Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Thavarey Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ilai Naan Paruva Mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ilai Naan Paruva Mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Siru Thuliyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Siru Thuliyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhum Tharunam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhum Tharunam Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aval Vizhi Mozhiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Vizhi Mozhiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Padikum Maanavan Aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padikum Maanavan Aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">Aval Nadai Muraiyai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aval Nadai Muraiyai"/>
</div>
<div class="lyrico-lyrics-wrapper">Rasikum Rasiganum Aanen Ho Ho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Rasikum Rasiganum Aanen Ho Ho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avan Aruginilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Aruginilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanal Mael Pani Thuli Aanaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanal Mael Pani Thuli Aanaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Avan Aruginilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avan Aruginilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Neer Thodum Thamarai Aanaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer Thodum Thamarai Aanaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Avalodirukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalodirukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vidha Snehidhan Aanaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vidha Snehidhan Aanaen"/>
</div>
<div class="lyrico-lyrics-wrapper">Avalukku Piditha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avalukku Piditha"/>
</div>
<div class="lyrico-lyrics-wrapper">Oru Vagai Sevagan Aanaen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oru Vagai Sevagan Aanaen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aazhiyil Irundhu Alasi Eduthene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aazhiyil Irundhu Alasi Eduthene"/>
</div>
<div class="lyrico-lyrics-wrapper">Adaikalam Amaikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adaikalam Amaikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Thagundhavan Dhaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thagundhavan Dhaane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Adi Azhaga Siricha Mugame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Azhaga Siricha Mugame"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa Nenacha Thonum Idame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa Nenacha Thonum Idame"/>
</div>
<div class="lyrico-lyrics-wrapper">Adi Azhaga Siricha Mugame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adi Azhaga Siricha Mugame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenacha Thonum Idame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenacha Thonum Idame"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan Pirandha Dhiname
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pirandha Dhiname"/>
</div>
<div class="lyrico-lyrics-wrapper">Kedacha Varamae Oh Oh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kedacha Varamae Oh Oh"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naan Pizhai Nee Mazhalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan Pizhai Nee Mazhalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enakkul Nee Irundhaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enakkul Nee Irundhaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhu Thavarey Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhu Thavarey Illai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nee Ilai Naan Paruva Mazhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Ilai Naan Paruva Mazhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Siru Thuliyaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Siru Thuliyaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhum Tharunam Illai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhum Tharunam Illai"/>
</div>
</pre>
