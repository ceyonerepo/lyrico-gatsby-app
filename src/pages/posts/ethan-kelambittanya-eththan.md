---
title: "ethan kelambittanya song lyrics"
album: "Eththan"
artist: "Taj Noor"
lyricist: "S. Nanakaravel"
director: "L Suresh"
path: "/albums/eththan-lyrics"
song: "Ethan Kelambittanya"
image: ../../images/albumart/eththan.jpg
date: 2011-05-27
lang: tamil
youtubeLink: "https://www.youtube.com/embed/WULM0UzK0d8"
type: "mass"
singers:
  - M.L.R. Karthikeyan
  - Bhagyaraj
  - Ramesh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ethan ethan ivan kadhaiyakketta suthum suthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethan ethan ivan kadhaiyakketta suthum suthum"/>
</div>
<div class="lyrico-lyrics-wrapper">namma thalaiye suthundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma thalaiye suthundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">hey panamnnu lesaa sonnaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey panamnnu lesaa sonnaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">ada ponamum kooda vaaya therakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada ponamum kooda vaaya therakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">ethan pera sonnaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ethan pera sonnaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">andha panamey perusaa vaaya polakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andha panamey perusaa vaaya polakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">hey thodarndhu yaarum sonnaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey thodarndhu yaarum sonnaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">adhakkekura manasu kadhikalakkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adhakkekura manasu kadhikalakkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethan pera sonnaaley
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethan pera sonnaaley"/>
</div>
<div class="lyrico-lyrics-wrapper">kadanpattavan vayire kadabudangum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadanpattavan vayire kadabudangum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethan Ethan aahaa kelambittaanyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethan Ethan aahaa kelambittaanyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethan Ethan ivan panathai kumbidum bakthan bakthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethan Ethan ivan panathai kumbidum bakthan bakthan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pookkada pakkathula poo vaasam veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookkada pakkathula poo vaasam veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">ye meenkadai pakkathula meen vaasam veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ye meenkadai pakkathula meen vaasam veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">vi vi vi vi vi veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vi vi vi vi vi veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethanoada panathula kadan vaasam veesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethanoada panathula kadan vaasam veesum"/>
</div>
<div class="lyrico-lyrics-wrapper">un kitta thappi padhunga karuvaadu vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kitta thappi padhunga karuvaadu vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan kitta thappippadhu karuppatti vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan kitta thappippadhu karuppatti vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethan kitta thappaadhu panavaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethan kitta thappaadhu panavaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">hey panatha padhukki vachaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="hey panatha padhukki vachaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan scanar kannil pidichiduvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan scanar kannil pidichiduvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">pasubic kadalil maraichaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pasubic kadalil maraichaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">ivan pattunnu paanji eduthudivaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ivan pattunnu paanji eduthudivaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethan Ethan Ethan Ethan Ethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethan Ethan Ethan Ethan Ethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethan Ethan Ethan Ethan Ethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethan Ethan Ethan Ethan Ethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jithan Jithan Jithan Jithan Jithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jithan Jithan Jithan Jithan Jithan"/>
</div>
<div class="lyrico-lyrics-wrapper">Jithan Jithan Jithan Jithan Jithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jithan Jithan Jithan Jithan Jithan"/>
</div>
<div class="lyrico-lyrics-wrapper">jithan kadan vasool vettaiyila jithan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jithan kadan vasool vettaiyila jithan"/>
</div>
<div class="lyrico-lyrics-wrapper">vekkathaiyum maanathaiyum kappal yethuvaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vekkathaiyum maanathaiyum kappal yethuvaan"/>
</div>
<div class="lyrico-lyrics-wrapper">panam illaadha kadanaali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panam illaadha kadanaali"/>
</div>
<div class="lyrico-lyrics-wrapper">Retion kadai manuvula roasham thedinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Retion kadai manuvula roasham thedinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">konji konji konji konji konji vaanguraai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="konji konji konji konji konji vaanguraai"/>
</div>
<div class="lyrico-lyrics-wrapper">ada koduthakkaasa thiruppikkettaa paarkkuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ada koduthakkaasa thiruppikkettaa paarkkuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">pallakkaatturaan paadaappaduthuraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pallakkaatturaan paadaappaduthuraan"/>
</div>
<div class="lyrico-lyrics-wrapper">annan Ethan kitta koavikkiraan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="annan Ethan kitta koavikkiraan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ethan ethan ivan kadhaiyakketta suthum suthum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethan ethan ivan kadhaiyakketta suthum suthum"/>
</div>
<div class="lyrico-lyrics-wrapper">namma thalaiye suthundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="namma thalaiye suthundaa"/>
</div>
</pre>
