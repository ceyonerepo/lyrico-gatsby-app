---
title: "pudikale pudikudhu song lyrics"
album: "Venghai"
artist: "Devi Sri Prasad"
lyricist: "Hari"
director: "Hari"
path: "/albums/venghai-lyrics"
song: "Pudikale Pudikudhu"
image: ../../images/albumart/venghai.jpg
date: 2011-07-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/jkjbyol43U8"
type: "love"
singers:
  - Mukesh
  - Suchitra
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Ye Pethavanga Paarthu Vacha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Pethavanga Paarthu Vacha "/>
</div>
<div class="lyrico-lyrics-wrapper">Ponna Enaku Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna Enaku Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Ponna Konjam Pudichalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponna Konjam Pudichalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Thaali Katta Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaali Katta Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaali Katta Nenachalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaali Katta Nenachalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly Panna Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly Panna Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Jolly Panni Mudichalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jolly Panni Mudichalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Sernthu Vaazha Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sernthu Vaazha Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Matum Pudikuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Matum Pudikuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kanna Matum Pudikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanna Matum Pudikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Aambalainga Vachirukum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Aambalainga Vachirukum "/>
</div>
<div class="lyrico-lyrics-wrapper">Meesai Enaku Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesai Enaku Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Meesa Konjam Pudichalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Meesa Konjam Pudichalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa Enaku Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa Enaku Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pesa Konjam Nenachalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pesa Konjam Nenachalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhaga Enaku Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhaga Enaku Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Pazhagi Paarthu Tholachalum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pazhagi Paarthu Tholachalum "/>
</div>
<div class="lyrico-lyrics-wrapper">Pallaanathu Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pallaanathu Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Matum Pudikuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Matum Pudikuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kanna Matum Pudikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kanna Matum Pudikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Thoothukudiyila Thudipana Aaloruthan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Thoothukudiyila Thudipana Aaloruthan "/>
</div>
<div class="lyrico-lyrics-wrapper">Thorathi Vanthaanae Enaku Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thorathi Vanthaanae Enaku Pudikala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Hey Karaikudiyila Kalaiyana 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Hey Karaikudiyila Kalaiyana "/>
</div>
<div class="lyrico-lyrics-wrapper">Penoruthi Kanadichalae Enaku Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Penoruthi Kanadichalae Enaku Pudikala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa Haan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa Haan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Sela Katta Pudikala Seepeduthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Sela Katta Pudikala Seepeduthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Thalavaari Pinna Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thalavaari Pinna Pudikala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Vetti Satta Pudikala Vitha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Vetti Satta Pudikala Vitha "/>
</div>
<div class="lyrico-lyrics-wrapper">Vithama Jeans Vangi Poda Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vithama Jeans Vangi Poda Pudikala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Palagaram Pudikala Pala Vaaram Thoongala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palagaram Pudikala Pala Vaaram Thoongala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaku Ennaiyae Kooda Sila Neram Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaku Ennaiyae Kooda Sila Neram Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unna Matum Pudikuthu Un Kanna Matum Pudikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Matum Pudikuthu Un Kanna Matum Pudikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna Matum Pudikuthu Un Kanna Matum Pudikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna Matum Pudikuthu Un Kanna Matum Pudikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Ketta Palakam Anjaaru Vachiruntha Sathiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Ketta Palakam Anjaaru Vachiruntha Sathiyama"/>
</div>
<div class="lyrico-lyrics-wrapper">Onnum Ippo Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onnum Ippo Pudikala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudikala Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikala Pudikala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Hey Hey Nalla Pullanu Oorellam Pereduthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Hey Nalla Pullanu Oorellam Pereduthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaapathikolla Ippo Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaapathikolla Ippo Pudikala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pudikala Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pudikala Pudikala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ye Oora Sutha Pudikala Kabadiyila 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ye Oora Sutha Pudikala Kabadiyila "/>
</div>
<div class="lyrico-lyrics-wrapper">Jeichalum Katha Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jeichalum Katha Pudikala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ko Ko Kolam Poda Pudikala Kummi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ko Ko Kolam Poda Pudikala Kummi "/>
</div>
<div class="lyrico-lyrics-wrapper">Paatu Ketaalum Aada Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paatu Ketaalum Aada Pudikala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kutralam Pudikala Kodaikanal Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutralam Pudikala Kodaikanal Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Goa Ooty Mysore Darjeeling Pudikala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Goa Ooty Mysore Darjeeling Pudikala"/>
</div>
<div class="lyrico-lyrics-wrapper">Darling Unna Pudikuthu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Darling Unna Pudikuthu "/>
</div>
<div class="lyrico-lyrics-wrapper">Manam Davadika Thudikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manam Davadika Thudikuthu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Machan Unna Pudikuthu Yen Manasu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machan Unna Pudikuthu Yen Manasu "/>
</div>
<div class="lyrico-lyrics-wrapper">Ippo Thudikuthu Thudikuthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippo Thudikuthu Thudikuthu"/>
</div>
</pre>
