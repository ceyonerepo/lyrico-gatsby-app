---
title: "yemaindhi song lyrics"
album: "Rangula Ratnam"
artist: "Sricharan Pakala"
lyricist: "Rakendu Mouli"
director: "Shreeranjani"
path: "/albums/rangula-ratnam-lyrics"
song: "Yemaindhi"
image: ../../images/albumart/rangula-ratnam.jpg
date: 2018-01-14
lang: telugu
youtubeLink: "https://www.youtube.com/embed/pOaPZcTS5vA"
type: "love"
singers:
  - Yazin Nizar
  - Yamini Ghantasala
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Emaindee bujji kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaindee bujji kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Premaindaa cheppamannaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premaindaa cheppamannaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetote nenu unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetote nenu unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandeham vaddu chinnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandeham vaddu chinnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayibaaboy premekkinattuga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayibaaboy premekkinattuga "/>
</div>
<div class="lyrico-lyrics-wrapper">picche pongene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="picche pongene"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa intlonaa e kanta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa intlonaa e kanta "/>
</div>
<div class="lyrico-lyrics-wrapper">vintalu edu choodanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vintalu edu choodanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey raa mudduliyyavaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey raa mudduliyyavaa "/>
</div>
<div class="lyrico-lyrics-wrapper">haddu daatavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haddu daatavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuddame lipto cheyyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuddame lipto cheyyavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edo gilee maayarogamaa malli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edo gilee maayarogamaa malli"/>
</div>
<div class="lyrico-lyrics-wrapper">Gadilokelli antaa gicchey mallee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gadilokelli antaa gicchey mallee"/>
</div>
<div class="lyrico-lyrics-wrapper">Idi nyaayamaa chelee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi nyaayamaa chelee"/>
</div>
<div class="lyrico-lyrics-wrapper">poraa don't be silly
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poraa don't be silly"/>
</div>
<div class="lyrico-lyrics-wrapper">Chali gaalike bali
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chali gaalike bali"/>
</div>
<div class="lyrico-lyrics-wrapper">ore kougili
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ore kougili"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey raa mudduliyyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey raa mudduliyyavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">haddu daatavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haddu daatavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuddame lipto cheyyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuddame lipto cheyyavaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Andaala aada pilla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andaala aada pilla "/>
</div>
<div class="lyrico-lyrics-wrapper">taakaavo aggi pulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="taakaavo aggi pulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Choopulto gunde gulla 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Choopulto gunde gulla "/>
</div>
<div class="lyrico-lyrics-wrapper">cheddam raa gola gola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheddam raa gola gola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maato padaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maato padaa"/>
</div>
<div class="lyrico-lyrics-wrapper">olle kovvekindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="olle kovvekindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Feelingu lendaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Feelingu lendaa"/>
</div>
<div class="lyrico-lyrics-wrapper">neekedo ayyindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neekedo ayyindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Emainaa baagundaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emainaa baagundaa"/>
</div>
<div class="lyrico-lyrics-wrapper">po poraa nee bondaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="po poraa nee bondaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andaalu ooristunte 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andaalu ooristunte "/>
</div>
<div class="lyrico-lyrics-wrapper">picche pattadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="picche pattadaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Emaindee bujji kanna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Emaindee bujji kanna"/>
</div>
<div class="lyrico-lyrics-wrapper">Premaindaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Premaindaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Neetote nenu unnaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neetote nenu unnaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandeham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandeham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayibaaboy premekkinattuga 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayibaaboy premekkinattuga "/>
</div>
<div class="lyrico-lyrics-wrapper">picche pongene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="picche pongene"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa intlonaa e kanta 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa intlonaa e kanta "/>
</div>
<div class="lyrico-lyrics-wrapper">vintalu edu choodanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vintalu edu choodanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey raa mudduliyyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey raa mudduliyyavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">haddu daatavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="haddu daatavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuddame lipto cheyyavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuddame lipto cheyyavaa"/>
</div>
</pre>
