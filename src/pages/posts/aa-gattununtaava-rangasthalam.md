---
title: "aa gattununtaava song lyrics"
album: "Rangasthalam"
artist: "Devi Sri Prasad"
lyricist: "Chandrabose"
director: "Sukumar"
path: "/albums/rangasthalam-lyrics"
song: "Aa Gattununtaava"
image: ../../images/albumart/rangasthalam.jpg
date: 2022-06-30
lang: telugu
youtubeLink: "https://www.youtube.com/embed/yeRAwhtyD3g"
type: "love"
singers:
  -	Shiva Nagulu
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Aa gattununtaava naa ganna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gattununtaava naa ganna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gattukotava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gattukotava"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa gattununtaava naa ganna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gattununtaava naa ganna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gattukotava naa ganna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gattukotava naa ganna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aii Aa gattununtaava naa ganna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aii Aa gattununtaava naa ganna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gattukotava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gattukotava"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa gattununtaava naa ganna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gattununtaava naa ganna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gattukotava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gattukotava"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa gattunemo seesadu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gattunemo seesadu "/>
</div>
<div class="lyrico-lyrics-wrapper">saradu undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saradu undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kundedu kallu undi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kundedu kallu undi "/>
</div>
<div class="lyrico-lyrics-wrapper">oodedu brandi undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oodedu brandi undi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee gattunemo mundedanta manchiga undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gattunemo mundedanta manchiga undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa gattununtaava naa ganna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gattununtaava naa ganna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gattukotava oyy
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gattukotava oyy"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa gattununtaava naa ganna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gattununtaava naa ganna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gattukotava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gattukotava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa dibbanuntava naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dibbanuntava naa "/>
</div>
<div class="lyrico-lyrics-wrapper">ganna ee dibbakostava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganna ee dibbakostava"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dibbanuntava naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dibbanuntava naa "/>
</div>
<div class="lyrico-lyrics-wrapper">ganna ee dibbakostava Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganna ee dibbakostava Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dibbanemo thodela dandu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dibbanemo thodela dandu "/>
</div>
<div class="lyrico-lyrics-wrapper">undi nakkala mukka undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undi nakkala mukka undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Pandikoppula gumpu undi ee 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pandikoppula gumpu undi ee "/>
</div>
<div class="lyrico-lyrics-wrapper">dibbanemo bovula danda undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dibbanemo bovula danda undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dibbanuntava naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dibbanuntava naa "/>
</div>
<div class="lyrico-lyrics-wrapper">ganna ee dibbakostava Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganna ee dibbakostava Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa dibbanuntava naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa dibbanuntava naa "/>
</div>
<div class="lyrico-lyrics-wrapper">ganna ee dibbakostava Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganna ee dibbakostava Hey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa gadapana untava naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gadapana untava naa "/>
</div>
<div class="lyrico-lyrics-wrapper">ganna ee gadapakostava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganna ee gadapakostava"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa gadapana untava naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gadapana untava naa "/>
</div>
<div class="lyrico-lyrics-wrapper">ganna ee gadapakostava Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganna ee gadapakostava Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa gadapanemo kaneru 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gadapanemo kaneru "/>
</div>
<div class="lyrico-lyrics-wrapper">baspu undi gurrau dekka undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="baspu undi gurrau dekka undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ganjai metta undi Aii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ganjai metta undi Aii"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ee gadapanemo gandapu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gadapanemo gandapu "/>
</div>
<div class="lyrico-lyrics-wrapper">chekka undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chekka undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa gadapana untava 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gadapana untava "/>
</div>
<div class="lyrico-lyrics-wrapper">naa ganna ee gadapakostava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa ganna ee gadapakostava"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa gadapana untava naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gadapana untava naa "/>
</div>
<div class="lyrico-lyrics-wrapper">ganna ee gadapakostava Hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganna ee gadapakostava Hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee aepukostava naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee aepukostava naa "/>
</div>
<div class="lyrico-lyrics-wrapper">ganna aa epunauntava naa ganna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganna aa epunauntava naa ganna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee aepukostava naa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee aepukostava naa "/>
</div>
<div class="lyrico-lyrics-wrapper">ganna aa epunauntava hey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ganna aa epunauntava hey"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee aepunemo nyayam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee aepunemo nyayam "/>
</div>
<div class="lyrico-lyrics-wrapper">undi dharmam undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="undi dharmam undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Battam undi suttam undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Battam undi suttam undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa aepuki anitiki mundala aavu undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa aepuki anitiki mundala aavu undi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aa gattununtaava naa ganna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gattununtaava naa ganna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gattukotava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gattukotava"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa gattununtaava naa ganna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa gattununtaava naa ganna"/>
</div>
<div class="lyrico-lyrics-wrapper">Ee gattukotava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ee gattukotava"/>
</div>
</pre>
