---
title: "pataas pilla song lyrics"
album: "DJ Tillu"
artist: "Ram Miriyala"
lyricist: "Kittu Vissapragada"
director: "Vimal Krishna"
path: "/albums/dj-tillu-lyrics"
song: "Pataas Pilla"
image: ../../images/albumart/dj-tillu.jpg
date: 2022-02-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/Bdxe7C4x4Zk"
type: "happy"
singers:
  - Anirudh Ravichander
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raja raja item raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja item raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Roja roja crazy roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja roja crazy roja"/>
</div>
<div class="lyrico-lyrics-wrapper">Lazy lazy gundallona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lazy lazy gundallona"/>
</div>
<div class="lyrico-lyrics-wrapper">Dj dj kotte sintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj dj kotte sintha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mind e atu itu ani oogindi ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind e atu itu ani oogindi ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde tega egabadi aadindi ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde tega egabadi aadindi ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Life ey tikka makka padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life ey tikka makka padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Single u step esu maarindi ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single u step esu maarindi ga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaas pilla pattaas pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaas pilla pattaas pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaas pilla thaakaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaas pilla thaakaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaas pilla Pattaas pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaas pilla Pattaas pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil antha thillaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil antha thillaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaas pilla pattaas pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaas pilla pattaas pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaas pilla sootiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaas pilla sootiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaas pilla pattaas pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaas pilla pattaas pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Tent yesi koosunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tent yesi koosunda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kalise nadiche daarullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalise nadiche daarullo"/>
</div>
<div class="lyrico-lyrics-wrapper">Range chere needallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Range chere needallo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chaatarulona uli besham laaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chaatarulona uli besham laaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Naadum choose vela 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naadum choose vela "/>
</div>
<div class="lyrico-lyrics-wrapper">naram aane sinthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naram aane sinthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Manase kaale jaarene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manase kaale jaarene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja raja item raja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja raja item raja"/>
</div>
<div class="lyrico-lyrics-wrapper">Roja roja crazy roja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Roja roja crazy roja"/>
</div>
<div class="lyrico-lyrics-wrapper">Lazy lazy gundallona
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Lazy lazy gundallona"/>
</div>
<div class="lyrico-lyrics-wrapper">Dj dj kotte sintha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dj dj kotte sintha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mind e atu itu ani oogindi ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mind e atu itu ani oogindi ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Gunde tega egabadi aadindi ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gunde tega egabadi aadindi ga"/>
</div>
<div class="lyrico-lyrics-wrapper">Life ey tikka makka padi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Life ey tikka makka padi"/>
</div>
<div class="lyrico-lyrics-wrapper">Single u step esu maarindi ga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Single u step esu maarindi ga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaas pilla pattaas pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaas pilla pattaas pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaas pilla thaakaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaas pilla thaakaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaas pilla Pattaas pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaas pilla Pattaas pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Dil antha thillaana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dil antha thillaana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pattaas pilla pattaas pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaas pilla pattaas pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaas pilla sootiga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaas pilla sootiga"/>
</div>
<div class="lyrico-lyrics-wrapper">Pattaas pilla pattaas pilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pattaas pilla pattaas pilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Tent yesi koosunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tent yesi koosunda"/>
</div>
</pre>
