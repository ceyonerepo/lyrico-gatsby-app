---
title: "iva ooru kallakurichi song lyrics"
album: "Dhoni Kabadi Kuzhu"
artist: "Roshan Joseph"
lyricist: "N Rasa"
director: "P Iyyappan"
path: "/albums/dhoni-kabadi-kuzhu-lyrics"
song: "Iva Ooru Kallakurichi"
image: ../../images/albumart/dhoni-kabadi-kuzhu.jpg
date: 2018-12-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rAmCCpxUHd0"
type: "love"
singers:
  - V V Prasanna
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">iva ooru kalla kurichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva ooru kalla kurichi"/>
</div>
<div class="lyrico-lyrics-wrapper">pora en nenja kilichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pora en nenja kilichi"/>
</div>
<div class="lyrico-lyrics-wrapper">en ooru paathooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ooru paathooru"/>
</div>
<div class="lyrico-lyrics-wrapper">aane naan kolaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aane naan kolaaru"/>
</div>
<div class="lyrico-lyrics-wrapper">iva alagu etho panuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva alagu etho panuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">enna pichi eno thinuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enna pichi eno thinuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">enakulle thooral poduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakulle thooral poduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">yen pulla thooram oodura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yen pulla thooram oodura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iva ooru kalla kurichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva ooru kalla kurichi"/>
</div>
<div class="lyrico-lyrics-wrapper">pora en nenja kilichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pora en nenja kilichi"/>
</div>
<div class="lyrico-lyrics-wrapper">en ooru paathooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ooru paathooru"/>
</div>
<div class="lyrico-lyrics-wrapper">aane naan kolaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aane naan kolaaru"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kathal pootha 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathal pootha "/>
</div>
<div class="lyrico-lyrics-wrapper">cheiya irunthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheiya irunthen"/>
</div>
<div class="lyrico-lyrics-wrapper">unna paarthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna paarthu"/>
</div>
<div class="lyrico-lyrics-wrapper">maram aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maram aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">kaathil unthan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaathil unthan"/>
</div>
<div class="lyrico-lyrics-wrapper">kuralum ketka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuralum ketka"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku kidaitha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku kidaitha"/>
</div>
<div class="lyrico-lyrics-wrapper">varam aanen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="varam aanen"/>
</div>
<div class="lyrico-lyrics-wrapper">kanavil unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanavil unna"/>
</div>
<div class="lyrico-lyrics-wrapper">thedi than alanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thedi than alanjen"/>
</div>
<div class="lyrico-lyrics-wrapper">kavithai ezhuthi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavithai ezhuthi"/>
</div>
<div class="lyrico-lyrics-wrapper">kavingna thirinjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kavingna thirinjen"/>
</div>
<div class="lyrico-lyrics-wrapper">paarkum munbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paarkum munbu"/>
</div>
<div class="lyrico-lyrics-wrapper">silaiyaai kidanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="silaiyaai kidanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">neyum paarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="neyum paarka"/>
</div>
<div class="lyrico-lyrics-wrapper">uyir vanthu nadanthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="uyir vanthu nadanthen"/>
</div>
<div class="lyrico-lyrics-wrapper">un thaavani 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un thaavani "/>
</div>
<div class="lyrico-lyrics-wrapper">theendum pothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="theendum pothu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan puthayal aagiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan puthayal aagiren"/>
</div>
<div class="lyrico-lyrics-wrapper">un kai viral 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un kai viral "/>
</div>
<div class="lyrico-lyrics-wrapper">urasum pothu naan 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urasum pothu naan "/>
</div>
<div class="lyrico-lyrics-wrapper">pookalai pookuren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pookalai pookuren"/>
</div>
<div class="lyrico-lyrics-wrapper">enakulle saami aaduren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enakulle saami aaduren"/>
</div>
<div class="lyrico-lyrics-wrapper">enaku nee saami aagura
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="enaku nee saami aagura"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">iva ooru kalla kurichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="iva ooru kalla kurichi"/>
</div>
<div class="lyrico-lyrics-wrapper">pora en nenja kilichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pora en nenja kilichi"/>
</div>
<div class="lyrico-lyrics-wrapper">en ooru paathooru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en ooru paathooru"/>
</div>
<div class="lyrico-lyrics-wrapper">aane naan kolaaru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aane naan kolaaru"/>
</div>
</pre>