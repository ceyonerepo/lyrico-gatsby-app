---
title: "papa atthili papa song lyrics"
album: "Where is the Venkatalakshmi"
artist: "Hari Gowra"
lyricist: "Suresh Banisetty"
director: "	Kishore (Ladda)"
path: "/albums/where-is-the-venkatalakshmi-lyrics"
song: "Papa Atthili Papa"
image: ../../images/albumart/where-is-the-venkatalakshmi.jpg
date: 2019-03-15
lang: telugu
youtubeLink: "https://www.youtube.com/embed/fHX_5DWGavw"
type: "happy"
singers:
  - Mangli
  - Hari Gowra
  - Lokeshwar
  - Saicharan
  - Arun
  - Raghu Ram
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Papaa Neekedante Istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papaa Neekedante Istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Hey Papa Neekedante Istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hey Papa Neekedante Istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Papa Ey Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papa Ey Papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Papa Neekedante Istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papa Neekedante Istam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mamidipalla Jaampalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamidipalla Jaampalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizza Burger Naatu Liquor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizza Burger Naatu Liquor"/>
</div>
<div class="lyrico-lyrics-wrapper">Mamidipalla Jaampalla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mamidipalla Jaampalla"/>
</div>
<div class="lyrico-lyrics-wrapper">Pizza Burger Naatu Liquor
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pizza Burger Naatu Liquor"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapa Atthili Papa Neekedante Istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapa Atthili Papa Neekedante Istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Papa Neekedante Istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Papa Neekedante Istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Atthili Papa Neekedante Istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Atthili Papa Neekedante Istam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thokkudu Billa Ghootee Billa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokkudu Billa Ghootee Billa"/>
</div>
<div class="lyrico-lyrics-wrapper">Astaa Chemma Achoo Bommaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Astaa Chemma Achoo Bommaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Thokkudu Billa Ghootee Billa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thokkudu Billa Ghootee Billa"/>
</div>
<div class="lyrico-lyrics-wrapper">Astaa Chemma Achoo Bommaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Astaa Chemma Achoo Bommaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapa Atthili Papa Neekedante Istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapa Atthili Papa Neekedante Istam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Panimaala Cheppalenti Yemkaavalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Panimaala Cheppalenti Yemkaavalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Nanuchusthe Ardamkada Yemiyyalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nanuchusthe Ardamkada Yemiyyalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruvalu Pettesaka Arachethilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvalu Pettesaka Arachethilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Yevadina Adguthada Yem Cheyaalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yevadina Adguthada Yem Cheyaalo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Manasu Madathettave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manasu Madathettave "/>
</div>
<div class="lyrico-lyrics-wrapper">Vayasu Padagottaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vayasu Padagottaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuluku Yedurettaave 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuluku Yedurettaave "/>
</div>
<div class="lyrico-lyrics-wrapper">Kunuku Chedagottaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kunuku Chedagottaave"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapa Neekedante Istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapa Neekedante Istam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ey Sittharalu Choopistanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ey Sittharalu Choopistanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Soodi Mandu Choope Guchi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Soodi Mandu Choope Guchi"/>
</div>
<div class="lyrico-lyrics-wrapper">Gingiraalu Thippisthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gingiraalu Thippisthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Chepamandu Mudhe Ichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chepamandu Mudhe Ichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Yethi Yethi Kudipesthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yethi Yethi Kudipesthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Matthumandu Kougillichi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Matthumandu Kougillichi"/>
</div>
<div class="lyrico-lyrics-wrapper">Racha Racha Lepesthanu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Racha Racha Lepesthanu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nallamande Nenu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallamande Nenu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Motham Mandugundu Saamanatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Motham Mandugundu Saamanatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Neelo Unde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neelo Unde"/>
</div>
<div class="lyrico-lyrics-wrapper">Apude Emi Choosaarinkaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Apude Emi Choosaarinkaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Shaana Unde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Shaana Unde"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkaleni Sanga Seva
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkaleni Sanga Seva"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeke Unde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeke Unde"/>
</div>
<div class="lyrico-lyrics-wrapper">Yekkada Yekkilosthe Akkada 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yekkada Yekkilosthe Akkada "/>
</div>
<div class="lyrico-lyrics-wrapper">Nenu Undaalsinde
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenu Undaalsinde"/>
</div>
<div class="lyrico-lyrics-wrapper">Cheere Kattina Seethakoka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Cheere Kattina Seethakoka"/>
</div>
<div class="lyrico-lyrics-wrapper">Sevalu Yenno Andichaka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sevalu Yenno Andichaka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedo Okati Evvakunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedo Okati Evvakunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Pampam Khaleegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pampam Khaleegaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paapa Atthili Papa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapa Atthili Papa"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapa Neekedante Istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapa Neekedante Istam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapa Neekedante Istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapa Neekedante Istam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Chukkala Cheera Jumkaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkala Cheera Jumkaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddaanaala Vankeela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddaanaala Vankeela"/>
</div>
<div class="lyrico-lyrics-wrapper">Chukkala Cheera Jumkaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chukkala Cheera Jumkaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaddaanaala Vankeela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaddaanaala Vankeela"/>
</div>
<div class="lyrico-lyrics-wrapper">Paapa… Atthili Papa Neekedante Istam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paapa… Atthili Papa Neekedante Istam"/>
</div>
</pre>
