---
title: "thoova mazhai mannil song lyrics"
album: "Meendum Oru Mariyathai"
artist: "N. R. Raghunanthan - Sharran Surya - Yuvan Shankar Raja"
lyricist: "Kabilan Vairamuthu"
director: "Bharathiraja"
path: "/albums/meendum-oru-mariyathai-lyrics"
song: "Thoova Mazhai Mannil"
image: ../../images/albumart/meendum-oru-mariyathai.jpg
date: 2020-02-21
lang: tamil
youtubeLink: "https://www.youtube.com/embed/6ycLb95oRTE"
type: "melody"
singers:
  - Vandana Srinivasan
  - Sarath Santhosh
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">thoova mazhai mannil thoovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoova mazhai mannil thoovum"/>
</div>
<div class="lyrico-lyrics-wrapper">en vergalil punnagai paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vergalil punnagai paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">thoova mazhai mannil thoovi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoova mazhai mannil thoovi"/>
</div>
<div class="lyrico-lyrics-wrapper">minnal soodi aaduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal soodi aaduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjoram irangi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjoram irangi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pallam moodi poguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pallam moodi poguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nallorin aalaabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallorin aalaabam"/>
</div>
<div class="lyrico-lyrics-wrapper">innum boomiyil uyyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum boomiyil uyyum"/>
</div>
<div class="lyrico-lyrics-wrapper">athu illamal poyinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu illamal poyinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam tharkolai seyyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam tharkolai seyyum"/>
</div>
<div class="lyrico-lyrics-wrapper">intha saaral ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha saaral ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">oru paadal peyyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru paadal peyyum"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai theernthaalum kaadhodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai theernthaalum kaadhodu"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalam thithikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalam thithikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thoova mazhai mannil thoovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoova mazhai mannil thoovum"/>
</div>
<div class="lyrico-lyrics-wrapper">en vergalil punnagai paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vergalil punnagai paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">thoova mazhai mannil thoovi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoova mazhai mannil thoovi"/>
</div>
<div class="lyrico-lyrics-wrapper">minnal soodi aaduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal soodi aaduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjoram irangi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjoram irangi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pallam moodi poguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pallam moodi poguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">andru en meethu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andru en meethu"/>
</div>
<div class="lyrico-lyrics-wrapper">sinthitta thooralgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sinthitta thooralgal"/>
</div>
<div class="lyrico-lyrics-wrapper">veru niramallavaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veru niramallavaa"/>
</div>
<div class="lyrico-lyrics-wrapper">indru enai nokki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru enai nokki"/>
</div>
<div class="lyrico-lyrics-wrapper">veezhgindra megangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veezhgindra megangal"/>
</div>
<div class="lyrico-lyrics-wrapper">veru mugamallava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veru mugamallava"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">antha serodu mummaari meyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="antha serodu mummaari meyum"/>
</div>
<div class="lyrico-lyrics-wrapper">intha thaarodu ottam kollum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha thaarodu ottam kollum"/>
</div>
<div class="lyrico-lyrics-wrapper">kadal yaathaaginum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadal yaathaaginum"/>
</div>
<div class="lyrico-lyrics-wrapper">udal yaaraginum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="udal yaaraginum"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai peranbu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai peranbu"/>
</div>
<div class="lyrico-lyrics-wrapper">maaraamal serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maaraamal serum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">nallorin aalaabam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nallorin aalaabam"/>
</div>
<div class="lyrico-lyrics-wrapper">innum boomiyil uyyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="innum boomiyil uyyum"/>
</div>
<div class="lyrico-lyrics-wrapper">athu illamal poyinaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu illamal poyinaal"/>
</div>
<div class="lyrico-lyrics-wrapper">vaanam tharkolai seyyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaanam tharkolai seyyum"/>
</div>
<div class="lyrico-lyrics-wrapper">intha saaral ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="intha saaral ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">oru paadal peyyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oru paadal peyyum"/>
</div>
<div class="lyrico-lyrics-wrapper">mazhai theernthaalum kaadhodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mazhai theernthaalum kaadhodu"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalam thithikum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalam thithikum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thoova mazhai mannil thoovum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoova mazhai mannil thoovum"/>
</div>
<div class="lyrico-lyrics-wrapper">en vergalil punnagai paayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="en vergalil punnagai paayum"/>
</div>
<div class="lyrico-lyrics-wrapper">thoova mazhai mannil thoovi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoova mazhai mannil thoovi"/>
</div>
<div class="lyrico-lyrics-wrapper">minnal soodi aaduthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="minnal soodi aaduthey"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjoram irangi vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjoram irangi vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">pallam moodi poguthey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pallam moodi poguthey"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kaalgal kaalangal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalgal kaalangal"/>
</div>
<div class="lyrico-lyrics-wrapper">paazhaagi ponaalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="paazhaagi ponaalum"/>
</div>
<div class="lyrico-lyrics-wrapper">saalai pookindrathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saalai pookindrathey"/>
</div>
<div class="lyrico-lyrics-wrapper">naadhi illamal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naadhi illamal"/>
</div>
<div class="lyrico-lyrics-wrapper">thalladum prayathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalladum prayathil"/>
</div>
<div class="lyrico-lyrics-wrapper">thaalaatu ketkindrathey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thaalaatu ketkindrathey"/>
</div>
<div class="lyrico-lyrics-wrapper">nitham vaazhvodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nitham vaazhvodu"/>
</div>
<div class="lyrico-lyrics-wrapper">naan urayada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naan urayada"/>
</div>
<div class="lyrico-lyrics-wrapper">indru vaanodu vilaiyadave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="indru vaanodu vilaiyadave"/>
</div>
<div class="lyrico-lyrics-wrapper">nilam kaadaagidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nilam kaadaagidum"/>
</div>
<div class="lyrico-lyrics-wrapper">manam aadaagidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manam aadaagidum"/>
</div>
<div class="lyrico-lyrics-wrapper">athu nogaamal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="athu nogaamal "/>
</div>
<div class="lyrico-lyrics-wrapper">kaanaamal pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaanaamal pogum"/>
</div>
</pre>
