---
title: "dum dare coir song lyrics"
album: "Kirrak Party"
artist: "B Ajaneesh Loknath"
lyricist: "Vanamali"
director: "Sharan Koppisetty"
path: "/albums/kirrak-party-lyrics"
song: "Dum Dare Coir"
image: ../../images/albumart/kirrak-party.jpg
date: 2018-03-16
lang: telugu
youtubeLink: "https://www.youtube.com/embed/jSOF83ThNXQ"
type: "happy"
singers:
  -	B Ajaneesh Loknath
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Dum dare dum dare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dare dum dare dumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dare dumda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dare dum dumdare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dumdare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dare dum dum dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dare dum dum dum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dum dare dum dare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dare dum dare dumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dare dumda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dare dum dumdare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dumdare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dare dum dum dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dare dum dum dum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yennenni aashalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yennenni aashalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Adugaduguna ee college lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adugaduguna ee college lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Campus lo fightlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Campus lo fightlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Coffee shop treatlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Coffee shop treatlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Saagelee saradhalalo ee rojulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saagelee saradhalalo ee rojulu"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship kai parugulu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship kai parugulu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dum dare dum dare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dare dum dare dumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dare dumda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dare dum dumdare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dumdare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dare dum dum dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dare dum dum dum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One by four lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One by four lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisindi sneham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisindi sneham"/>
</div>
<div class="lyrico-lyrics-wrapper">Canteen chai la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Canteen chai la"/>
</div>
<div class="lyrico-lyrics-wrapper">Attendance thaggi pokunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attendance thaggi pokunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Undiga proxy formula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undiga proxy formula"/>
</div>
<div class="lyrico-lyrics-wrapper">Superstar first show ki maaku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superstar first show ki maaku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mass bunk manthram undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mass bunk manthram undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Exams lona backlogs valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Exams lona backlogs valla"/>
</div>
<div class="lyrico-lyrics-wrapper">Experience yenthentho perigindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Experience yenthentho perigindhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">One by four lo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="One by four lo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalisindi sneham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalisindi sneham"/>
</div>
<div class="lyrico-lyrics-wrapper">Canteen chai la
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Canteen chai la"/>
</div>
<div class="lyrico-lyrics-wrapper">Attendance thaggi pokunda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Attendance thaggi pokunda"/>
</div>
<div class="lyrico-lyrics-wrapper">Undiga proxy formula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undiga proxy formula"/>
</div>
<div class="lyrico-lyrics-wrapper">Superstar first show ki maku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Superstar first show ki maku"/>
</div>
<div class="lyrico-lyrics-wrapper">Mass bunk manthram undi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mass bunk manthram undi"/>
</div>
<div class="lyrico-lyrics-wrapper">Exams lonaa backlogs valla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Exams lonaa backlogs valla"/>
</div>
<div class="lyrico-lyrics-wrapper">Experience enthentho perigindhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Experience enthentho perigindhi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dum dare dum dare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dare dum dare dumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dare dumda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dare dum dumdare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dumdare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dare dum dum dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dare dum dum dum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ennenni aashalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennenni aashalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninu chusthunte ma manasulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninu chusthunte ma manasulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa chilipu navvulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa chilipu navvulo"/>
</div>
<div class="lyrico-lyrics-wrapper">Padipoya chupulo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padipoya chupulo"/>
</div>
<div class="lyrico-lyrics-wrapper">O meeraaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O meeraaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa andhari kala neevegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa andhari kala neevegaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Maa yedhalo neevegaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maa yedhalo neevegaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Branchullona thedalu unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Branchullona thedalu unna"/>
</div>
<div class="lyrico-lyrics-wrapper">Batch-u okkatte
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Batch-u okkatte"/>
</div>
<div class="lyrico-lyrics-wrapper">Machhaa annaa mamaa annaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Machhaa annaa mamaa annaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Friendship okkate
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Friendship okkate"/>
</div>
<div class="lyrico-lyrics-wrapper">Saadinchaalanundedo heatu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saadinchaalanundedo heatu"/>
</div>
<div class="lyrico-lyrics-wrapper">Em cheyyalo doubt
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Em cheyyalo doubt"/>
</div>
<div class="lyrico-lyrics-wrapper">College life lo chaduvu lite
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="College life lo chaduvu lite"/>
</div>
<div class="lyrico-lyrics-wrapper">Full time memu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Full time memu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodathamu le sightu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodathamu le sightu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dum dare dum dare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dare dum dare dumda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dare dumda"/>
</div>
<div class="lyrico-lyrics-wrapper">Dum dare dum dumdare
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dum dare dum dumdare"/>
</div>
<div class="lyrico-lyrics-wrapper">Dare dum dum dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dare dum dum dum"/>
</div>
</pre>
