---
title: "kasu mela kasu song lyrics"
album: "Kasu Mela Kasu"
artist: "M.S. Pandian"
lyricist: "Karuppaiah"
director: "K.S. Pazhani"
path: "/albums/kasu-mela-kasu-lyrics"
song: "Kasu Mela Kasu"
image: ../../images/albumart/kasu-mela-kasu.jpg
date: 2018-07-06
lang: tamil
youtubeLink: "https://www.youtube.com/embed/2t4vB7ziwc0"
type: "mass"
singers:
  - Mugesh
  - Gayathri
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kasu kasu kasu kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu kasu kasu kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu kasu kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu kasu kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu mela kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu mela kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">aagidalaam bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagidalaam bossu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu kasu kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu kasu kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu mela kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu mela kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu thanda ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu thanda ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">oorukulle maas uh
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorukulle maas uh"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu mela kasu kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu mela kasu kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu iruntha vaalkai lesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu iruntha vaalkai lesu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu mela kasu kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu mela kasu kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">sethu putta ellam thusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethu putta ellam thusu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu mela kasu kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu mela kasu kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">vachu iruntha vaalkai lesu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vachu iruntha vaalkai lesu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu mela kasu kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu mela kasu kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">sethu putta ellam thusu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sethu putta ellam thusu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu kasu kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu kasu kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu mele kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu mele kasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kasu kasu kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu kasu kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu mela kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu mela kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">aagidalaam bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagidalaam bossu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kasu iruntha kakkavum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha kakkavum"/>
</div>
<div class="lyrico-lyrics-wrapper">kacheri pannalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kacheri pannalam"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha kuruviyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha kuruviyum"/>
</div>
<div class="lyrico-lyrics-wrapper">gaanaa paatu padalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gaanaa paatu padalam"/>
</div>
<div class="lyrico-lyrics-wrapper">otha kaalil ninnu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="otha kaalil ninnu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">kuthaatam podalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kuthaatam podalam"/>
</div>
<div class="lyrico-lyrics-wrapper">thom tha thagida thagida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thom tha thagida thagida"/>
</div>
<div class="lyrico-lyrics-wrapper">thom tha thagida thom
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thom tha thagida thom"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha jokerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha jokerum"/>
</div>
<div class="lyrico-lyrics-wrapper">jaagura vaangalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jaagura vaangalam"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha nonjalun
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha nonjalun"/>
</div>
<div class="lyrico-lyrics-wrapper">arnald ah maaralam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="arnald ah maaralam"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha makkanum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha makkanum"/>
</div>
<div class="lyrico-lyrics-wrapper">nattamai pannalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nattamai pannalam"/>
</div>
<div class="lyrico-lyrics-wrapper">thutu mela thutu vantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thutu mela thutu vantha"/>
</div>
<div class="lyrico-lyrics-wrapper">thookam pochu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thookam pochu da"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu panam illayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu panam illayina"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalkai pochu da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalkai pochu da"/>
</div>
<div class="lyrico-lyrics-wrapper">kasa mattum pakuravan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasa mattum pakuravan"/>
</div>
<div class="lyrico-lyrics-wrapper">manusan illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manusan illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu illatha manisanuku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu illatha manisanuku"/>
</div>
<div class="lyrico-lyrics-wrapper">mathipu illa da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mathipu illa da"/>
</div>
<div class="lyrico-lyrics-wrapper">yei kasu kasu kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei kasu kasu kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu mela kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu mela kasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kasu kasu kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu kasu kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu mela kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu mela kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">aagidalaam bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagidalaam bossu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kasu iruntha thangathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha thangathula"/>
</div>
<div class="lyrico-lyrics-wrapper">court thachu podalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="court thachu podalam"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha tea kudika
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha tea kudika"/>
</div>
<div class="lyrico-lyrics-wrapper">aeroplane la pogalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aeroplane la pogalam"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha aaya vayum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha aaya vayum"/>
</div>
<div class="lyrico-lyrics-wrapper">andriava mathalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andriava mathalam"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha kai naatum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha kai naatum"/>
</div>
<div class="lyrico-lyrics-wrapper">college kattalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="college kattalam"/>
</div>
<div class="lyrico-lyrics-wrapper">andraadam kaatchiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andraadam kaatchiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">katchi thalaivan aagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="katchi thalaivan aagalam"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha vetiyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha vetiyum"/>
</div>
<div class="lyrico-lyrics-wrapper">vip aagalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vip aagalam"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha kalutha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha kalutha"/>
</div>
<div class="lyrico-lyrics-wrapper">kooda kadavul thanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kooda kadavul thanada"/>
</div>
<div class="lyrico-lyrics-wrapper">kadavulaiyum pakanum na
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavulaiyum pakanum na"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu venum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu venum da"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha boomi kuda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha boomi kuda"/>
</div>
<div class="lyrico-lyrics-wrapper">unna suthum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unna suthum da"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu illati sontham kooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu illati sontham kooda"/>
</div>
<div class="lyrico-lyrics-wrapper">thalli nikkum da
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalli nikkum da"/>
</div>
<div class="lyrico-lyrics-wrapper">yei kasu kasu kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yei kasu kasu kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu mela kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu mela kasu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kasu kasu kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu kasu kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu mela kasu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu mela kasu"/>
</div>
<div class="lyrico-lyrics-wrapper">kasu iruntha neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kasu iruntha neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">aagidalaam bossu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagidalaam bossu"/>
</div>
</pre>
