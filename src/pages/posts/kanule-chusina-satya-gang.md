---
title: "kanule chusina song lyrics"
album: "Satya Gang"
artist: "Prabhas"
lyricist: "Mahesh Khanna"
director: "Nimmala Prabhas"
path: "/albums/satya-gang-lyrics"
song: "Kanule Chusina"
image: ../../images/albumart/satya-gang.jpg
date: 2018-04-06
lang: telugu
youtubeLink: "https://www.youtube.com/embed/HmF7bvThpFc"
type: "love"
singers:
  -	Karthik
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">kanule chusina devata oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanule chusina devata oo"/>
</div>
<div class="lyrico-lyrics-wrapper">elalo oorvasi oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elalo oorvasi oo"/>
</div>
<div class="lyrico-lyrics-wrapper">venelai virisina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venelai virisina "/>
</div>
<div class="lyrico-lyrics-wrapper">menaka oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="menaka oo"/>
</div>
<div class="lyrico-lyrics-wrapper">nakai vachavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakai vachavo"/>
</div>
<div class="lyrico-lyrics-wrapper">yellora silpala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yellora silpala"/>
</div>
<div class="lyrico-lyrics-wrapper">andaale neevaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="andaale neevaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">aa andam chuste 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aa andam chuste "/>
</div>
<div class="lyrico-lyrics-wrapper">manmathadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manmathadu"/>
</div>
<div class="lyrico-lyrics-wrapper">o chinuku neevaithe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="o chinuku neevaithe"/>
</div>
<div class="lyrico-lyrics-wrapper">naa madiloni premantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa madiloni premantha"/>
</div>
<div class="lyrico-lyrics-wrapper">amrutamai paarele
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="amrutamai paarele"/>
</div>
<div class="lyrico-lyrics-wrapper">oo nadila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo nadila"/>
</div>
<div class="lyrico-lyrics-wrapper">adbutha roopam neede
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="adbutha roopam neede"/>
</div>
<div class="lyrico-lyrics-wrapper">veedani moham naade
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="veedani moham naade"/>
</div>
<div class="lyrico-lyrics-wrapper">valapula valavee nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="valapula valavee nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">oo sogasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo sogasa"/>
</div>
<div class="lyrico-lyrics-wrapper">maleela varsham nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maleela varsham nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">vennela teeram nene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vennela teeram nene"/>
</div>
<div class="lyrico-lyrics-wrapper">manasuke gaalam vesta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="manasuke gaalam vesta"/>
</div>
<div class="lyrico-lyrics-wrapper">oo sakiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo sakiya"/>
</div>
<div class="lyrico-lyrics-wrapper">venelai virisina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venelai virisina"/>
</div>
<div class="lyrico-lyrics-wrapper">menaka oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="menaka oo"/>
</div>
<div class="lyrico-lyrics-wrapper">nakai vachavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakai vachavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">oorvasi oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oorvasi oo"/>
</div>
<div class="lyrico-lyrics-wrapper">menaka oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="menaka oo"/>
</div>
<div class="lyrico-lyrics-wrapper">nakai puttina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakai puttina"/>
</div>
<div class="lyrico-lyrics-wrapper">sundari oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sundari oo"/>
</div>
<div class="lyrico-lyrics-wrapper">ooha vo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooha vo"/>
</div>
<div class="lyrico-lyrics-wrapper">maya vo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maya vo"/>
</div>
<div class="lyrico-lyrics-wrapper">na madi dohina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na madi dohina"/>
</div>
<div class="lyrico-lyrics-wrapper">maguva nuvvo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maguva nuvvo"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo ninne chusane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo ninne chusane"/>
</div>
<div class="lyrico-lyrics-wrapper">nalo nuvve daagaave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nalo nuvve daagaave"/>
</div>
<div class="lyrico-lyrics-wrapper">naalo vunna manasantha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naalo vunna manasantha"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvenaa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvenaa "/>
</div>
<div class="lyrico-lyrics-wrapper">nakshathranne choosano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakshathranne choosano"/>
</div>
<div class="lyrico-lyrics-wrapper">devakanyane choosano
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="devakanyane choosano"/>
</div>
<div class="lyrico-lyrics-wrapper">naa kannullo daagunnave
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naa kannullo daagunnave"/>
</div>
<div class="lyrico-lyrics-wrapper">na cheliya nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="na cheliya nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">oo laalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo laalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oolala oola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oolala oola"/>
</div>
<div class="lyrico-lyrics-wrapper">oo laalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo laalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oo laalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo laalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oolala oola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oolala oola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanule chusina devata oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanule chusina devata oo"/>
</div>
<div class="lyrico-lyrics-wrapper">elalo oorvasi oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elalo oorvasi oo"/>
</div>
<div class="lyrico-lyrics-wrapper">venelai virisina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venelai virisina "/>
</div>
<div class="lyrico-lyrics-wrapper">menaka oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="menaka oo"/>
</div>
<div class="lyrico-lyrics-wrapper">nakai vachavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakai vachavo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">yennadu chudani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yennadu chudani"/>
</div>
<div class="lyrico-lyrics-wrapper">vannela chinnela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vannela chinnela"/>
</div>
<div class="lyrico-lyrics-wrapper">soyagamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soyagamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">mallela jallula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mallela jallula"/>
</div>
<div class="lyrico-lyrics-wrapper">maimarapinche 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maimarapinche "/>
</div>
<div class="lyrico-lyrics-wrapper">poovanamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="poovanamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">urike aa 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urike aa "/>
</div>
<div class="lyrico-lyrics-wrapper">nadhi vompullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadhi vompullo"/>
</div>
<div class="lyrico-lyrics-wrapper">cheli nee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cheli nee"/>
</div>
<div class="lyrico-lyrics-wrapper">nadume choosane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nadume choosane"/>
</div>
<div class="lyrico-lyrics-wrapper">alanai yegasi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="alanai yegasi "/>
</div>
<div class="lyrico-lyrics-wrapper">yavvana yedhapai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="yavvana yedhapai"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalaane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalaane"/>
</div>
<div class="lyrico-lyrics-wrapper">chilipiga navve navvullo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chilipiga navve navvullo"/>
</div>
<div class="lyrico-lyrics-wrapper">roja poole choosane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="roja poole choosane"/>
</div>
<div class="lyrico-lyrics-wrapper">thenale jelle 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenale jelle "/>
</div>
<div class="lyrico-lyrics-wrapper">nee maatallo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nee maatallo"/>
</div>
<div class="lyrico-lyrics-wrapper">marichane cheli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="marichane cheli"/>
</div>
<div class="lyrico-lyrics-wrapper">oo laalaa oolala laa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo laalaa oolala laa"/>
</div>
<div class="lyrico-lyrics-wrapper">oo laalaa oo laalaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oo laalaa oo laalaa"/>
</div>
<div class="lyrico-lyrics-wrapper">oolala oola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="oolala oola"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">kanule chusina devata oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanule chusina devata oo"/>
</div>
<div class="lyrico-lyrics-wrapper">elalo oorvasi oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="elalo oorvasi oo"/>
</div>
<div class="lyrico-lyrics-wrapper">venelai virisina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="venelai virisina "/>
</div>
<div class="lyrico-lyrics-wrapper">menaka oo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="menaka oo"/>
</div>
<div class="lyrico-lyrics-wrapper">nakai vachavo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nakai vachavo"/>
</div>
</pre>
