---
title: "orey oru varthai song lyrics"
album: "Venghai"
artist: "Devi Sri Prasad"
lyricist: "Viveka"
director: "Hari"
path: "/albums/venghai-lyrics"
song: "Orey Oru Varthai"
image: ../../images/albumart/venghai.jpg
date: 2011-07-08
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LuyoHg2fazA"
type: "sad"
singers:
  - Tippu
  - Harini
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Yaaro Manasa Ulukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Manasa Ulukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Udaindhu Valikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Udaindhu Valikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naano Thanithu Nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naano Thanithu Nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyo Mounamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyo Mounamaaga"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orae Oru Vaarthaikkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Oru Vaarthaikkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyaama Kaathiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyaama Kaathiruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae Oru Paarvaikkaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Oru Paarvaikkaaga"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalum Thavamiruppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum Thavamiruppen"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae Oru Nodikkooda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Oru Nodikkooda"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodudhaan Vaazhvenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodudhaan Vaazhvenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae Oru Uyir Adhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Oru Uyir Adhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Kaiyil Thandhu Saayuven
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Kaiyil Thandhu Saayuven"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orae Oru Vaarthaiyaalaae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Oru Vaarthaiyaalaae"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenju Kalaigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenju Kalaigiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae Oru Paarvaiyaalae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Oru Paarvaiyaalae"/>
</div>
<div class="lyrico-lyrics-wrapper">Ulloora Karaigiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulloora Karaigiradhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro Manasa Ulukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Manasa Ulukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Udaindhu Valikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Udaindhu Valikka"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oh Sikkikkondu Sikkikkondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oh Sikkikkondu Sikkikkondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavikkum Oru Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavikkum Oru Idhayam"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettikkondu Vettikkondu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettikkondu Vettikkondu"/>
</div>
<div class="lyrico-lyrics-wrapper">Thavirkkum Oru Idhayam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thavirkkum Oru Idhayam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaadhal Ennum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Ennum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaikkuzhandhai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaikkuzhandhai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhari Azhugiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhari Azhugiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Urava Nenaichu Ullam Ippo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urava Nenaichu Ullam Ippo"/>
</div>
<div class="lyrico-lyrics-wrapper">Poraadudhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Poraadudhae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orae Oru Vaarthaikkettu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Oru Vaarthaikkettu"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenju Vedichiruchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenju Vedichiruchae"/>
</div>
<div class="lyrico-lyrics-wrapper">Orae Oru Paarvai Puyalaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Oru Paarvai Puyalaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Enmel Adichiruchae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enmel Adichiruchae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ullukkulla Mulla Vachu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukkulla Mulla Vachu"/>
</div>
<div class="lyrico-lyrics-wrapper">Edhukku Nee Siricha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhukku Nee Siricha"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal Yenum Peracholli
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal Yenum Peracholli"/>
</div>
<div class="lyrico-lyrics-wrapper">Kazhutha Nee Nericha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kazhutha Nee Nericha"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Onna Nenacha Paavathukku
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Onna Nenacha Paavathukku"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhudhaan Thandanaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhudhaan Thandanaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Peththa Dheivathukkae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Peththa Dheivathukkae"/>
</div>
<div class="lyrico-lyrics-wrapper">Sodhanaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sodhanaiyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orae Oru Vaarthai Pesa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Oru Vaarthai Pesa"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaala Mudiyalaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaala Mudiyalaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Orae Oru Throgam Thaangha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Orae Oru Throgam Thaangha"/>
</div>
<div class="lyrico-lyrics-wrapper">En Nenjil Balamillaiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En Nenjil Balamillaiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaaro Manasa Ulukka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaaro Manasa Ulukka"/>
</div>
<div class="lyrico-lyrics-wrapper">Yedho Udaindhu Valikka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yedho Udaindhu Valikka"/>
</div>
<div class="lyrico-lyrics-wrapper">Naano Thaniththu Nadakka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naano Thaniththu Nadakka"/>
</div>
<div class="lyrico-lyrics-wrapper">Neeyo Mounamaaga
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neeyo Mounamaaga"/>
</div>
</pre>
