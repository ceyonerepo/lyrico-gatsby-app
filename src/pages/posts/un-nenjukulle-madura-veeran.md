---
title: "un nenjukulle song lyrics"
album: "Madura Veeran"
artist: "Santhosh Dhayanidhi"
lyricist: "Yugabharathi"
director: "P.G. Muthiah"
path: "/albums/madura-veeran-lyrics"
song: "Un Nenjukulle"
image: ../../images/albumart/madura-veeran.jpg
date: 2018-02-02
lang: tamil
youtubeLink: "https://www.youtube.com/embed/06CgWrY5T7I"
type: "love"
singers:
  - Chinmayi
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unn Nenjukulla Vaazha Naan Yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Nenjukulla Vaazha Naan Yenguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Ninnaa Moochu Yen Vaanguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Ninnaa Moochu Yen Vaanguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Nenjukulla Vaazha Naan Yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Nenjukulla Vaazha Naan Yenguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Ninnaa Moochu Yen Vaanguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Ninnaa Moochu Yen Vaanguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raasave Unna Kattikollum Aasayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasave Unna Kattikollum Aasayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalaagi Ninnen Kannu Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaagi Ninnen Kannu Munnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaku Yetha Aambala Evanum Illa Oorula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaku Yetha Aambala Evanum Illa Oorula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala Kaatudaa Konjam Enmela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala Kaatudaa Konjam Enmela"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unn Nenjukulla Vaazha Naan Yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Nenjukulla Vaazha Naan Yenguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Ninnaa Moochu Yen Vaanguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Ninnaa Moochu Yen Vaanguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unnmela Pullivachi Sikkikiten Kolamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnmela Pullivachi Sikkikiten Kolamaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaiyaala Meenkuzhambum Vachithaaren Podhumaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaiyaala Meenkuzhambum Vachithaaren Podhumaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnpera Solla Solla Aanene Kaaramaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnpera Solla Solla Aanene Kaaramaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Summaa Nee Enna Thinnu Soodaana Saadhamaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Summaa Nee Enna Thinnu Soodaana Saadhamaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Aiyaa Nee Paathaale Paaloothar Aagividum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aiyaa Nee Paathaale Paaloothar Aagividum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paarkaama Ponaale Dhegam Nogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarkaama Ponaale Dhegam Nogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Eppodhu Unn Vaasam Enn Udamba Moodavarum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eppodhu Unn Vaasam Enn Udamba Moodavarum"/>
</div>
<div class="lyrico-lyrics-wrapper">Appodhe Ennoda Yekam Theerum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Appodhe Ennoda Yekam Theerum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unn Nenjukulla Vaazha Naan Yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Nenjukulla Vaazha Naan Yenguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Ninnaa Moochu Yen Vaanguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Ninnaa Moochu Yen Vaanguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Anjaaru Pullakutty Unna Otha Jaadaiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaaru Pullakutty Unna Otha Jaadaiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Anjaama Petheduka Ennavittaa Yaaruyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anjaama Petheduka Ennavittaa Yaaruyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Jillaave Kannuvaika Unnkooda Jodiyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jillaave Kannuvaika Unnkooda Jodiyaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bandhaavaa Suthivandhu Vaazhvene Raaniyaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bandhaavaa Suthivandhu Vaazhvene Raaniyaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Annaandhu Naan Paartha Aambalaiyo Yaarum Illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaandhu Naan Paartha Aambalaiyo Yaarum Illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnaala Mallaandhu Saanjen Keezha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnaala Mallaandhu Saanjen Keezha"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Nee Kekaama Nee Ponaa Nyaayamilla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Nee Kekaama Nee Ponaa Nyaayamilla"/>
</div>
<div class="lyrico-lyrics-wrapper">Oor Thaandi Pogaadha Koyil Kaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oor Thaandi Pogaadha Koyil Kaala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unn Nenjukulla Vaazha Naan Yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Nenjukulla Vaazha Naan Yenguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Ninnaa Moochu Yen Vaanguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Ninnaa Moochu Yen Vaanguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Unn Nenjukulla Vaazha Naan Yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unn Nenjukulla Vaazha Naan Yenguren"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Vandhu Ninnaa Moochu Yen Vaanguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Vandhu Ninnaa Moochu Yen Vaanguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raasave Unna Kattikollum Aasayila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasave Unna Kattikollum Aasayila"/>
</div>
<div class="lyrico-lyrics-wrapper">Aalaagi Ninnen Kannu Munnaala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aalaagi Ninnen Kannu Munnaala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enaku Yetha Aambala Evanum Illa Oorula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enaku Yetha Aambala Evanum Illa Oorula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhala Kaatudaa Konjam Enmela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhala Kaatudaa Konjam Enmela"/>
</div>
</pre>
