---
title: "anbum aranum song lyrics"
album: "Raa Raa"
artist: "Srikanth Deva"
lyricist: "Thiruvalluvar"
director: "Sandilya"
path: "/albums/raa-raa-lyrics"
song: "Anbum Aranum"
image: ../../images/albumart/raa-raa.jpg
date: 2011-10-07
lang: tamil
youtubeLink: "https://www.youtube.com/embed/rC_egumtrAg"
type: "happy"
singers:
  - Harish Ragavendra
  - Surmuki Raman
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Anbum aranum udaithaayin ilvaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbum aranum udaithaayin ilvaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">panbum paayanum adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panbum paayanum adhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbum aranum udaithaayin ilvaazhkai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbum aranum udaithaayin ilvaazhkai"/>
</div>
<div class="lyrico-lyrics-wrapper">panbum paayanum adhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panbum paayanum adhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Yaagaavaaraayinum naagaakka kaavaakkaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaagaavaaraayinum naagaakka kaavaakkaal"/>
</div>
<div class="lyrico-lyrics-wrapper">soagaappar solli izhukkappattu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="soagaappar solli izhukkappattu"/>
</div>
<div class="lyrico-lyrics-wrapper">Epporul yaar yaar vaai ketpinum apporul
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Epporul yaar yaar vaai ketpinum apporul"/>
</div>
<div class="lyrico-lyrics-wrapper">meipporul kaanbadhu arivu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meipporul kaanbadhu arivu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Theeyinaar suttappun ullaarum aaraadhey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeyinaar suttappun ullaarum aaraadhey"/>
</div>
<div class="lyrico-lyrics-wrapper">naavinaal sutta vadu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naavinaal sutta vadu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mugam aga natpadhu natpaagaadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mugam aga natpadhu natpaagaadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">nenjathu aga naga natpadhu natpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nenjathu aga naga natpadhu natpu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udukkai izhandhavan kaipoala angey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udukkai izhandhavan kaipoala angey"/>
</div>
<div class="lyrico-lyrics-wrapper">irukkan kalaivadhaam natpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukkan kalaivadhaam natpu"/>
</div>
<div class="lyrico-lyrics-wrapper">Udukkai izhandhavan kaipoala angey
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Udukkai izhandhavan kaipoala angey"/>
</div>
<div class="lyrico-lyrics-wrapper">irukkan kalaivadhaam natpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="irukkan kalaivadhaam natpu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Naana naana naana naana naana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naana naana naana naana naana"/>
</div>
<div class="lyrico-lyrics-wrapper">naana naana naana naana naanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="naana naana naana naana naanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dumukku dumukku dummaa dummaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dumukku dumukku dummaa dummaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dumukku dum dumukku dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dumukku dum dumukku dum"/>
</div>
<div class="lyrico-lyrics-wrapper">dumukku dum dum dum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dumukku dum dum dum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sollunga sollir payanudaiya sollarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollunga sollir payanudaiya sollarka"/>
</div>
<div class="lyrico-lyrics-wrapper">sollir payanilaa chol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollir payanilaa chol"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollunga sollir payanudaiya sollarka
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollunga sollir payanudaiya sollarka"/>
</div>
<div class="lyrico-lyrics-wrapper">sollir payanilaa chol
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sollir payanilaa chol"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Seyarkanaiya malar neettam maandhar tham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seyarkanaiya malar neettam maandhar tham"/>
</div>
<div class="lyrico-lyrics-wrapper">ulladhu anayaa uyarvu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ulladhu anayaa uyarvu"/>
</div>
</pre>
