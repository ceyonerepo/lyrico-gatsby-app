---
title: "sandakkaari song lyrics"
album: "Kadaikutty Singam"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "Pandiraj"
path: "/albums/kadaikutty-singam-lyrics"
song: "Sandakkaari"
image: ../../images/albumart/kadaikutty-singam.jpg
date: 2018-07-13
lang: tamil
youtubeLink: "https://www.youtube.com/embed/XtD3KDmstzg"
type: "love"
singers:
  - D Imman
  - Vandana Srinivasan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sandakkaari vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakkaari vaadi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai adakki aaluren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai adakki aaluren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty potta poonai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty potta poonai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam adikka yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam adikka yenguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandakkaari vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakkaari vaadi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai adakki aaluren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai adakki aaluren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty potta poonai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty potta poonai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam adikka yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam adikka yenguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu vachu neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu vachu neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona sokkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona sokkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu patti raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu patti raasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechum thikkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechum thikkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottiyirukka otthukkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottiyirukka otthukkadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandakkaari vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakkaari vaadi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai adakki aaluren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai adakki aaluren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty potta poonai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty potta poonai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam adikka yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam adikka yenguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandakkaari vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakkaari vaadi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai adakki aaluren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai adakki aaluren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty potta poonai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty potta poonai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam adikka yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam adikka yenguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jin jina kitta jinakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jin jina kitta jinakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jin jina kitta jinakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jin jina kitta jinakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jin jina kitta jinakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jin jina kitta jinakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jin jina kitta jinakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jin jina kitta jinakitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Mathalatha pola neethan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mathalatha pola neethan"/>
</div>
<div class="lyrico-lyrics-wrapper">Nenjaiyum thatturiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjaiyum thatturiyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Metteduthu naanum paada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Metteduthu naanum paada"/>
</div>
<div class="lyrico-lyrics-wrapper">Thondaiya mutturiyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thondaiya mutturiyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ottu moththa oorumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottu moththa oorumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Uchu kottum aambala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uchu kottum aambala"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai kanda aasaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai kanda aasaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Nippadhenna rottula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nippadhenna rottula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saami sathiyama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saami sathiyama"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai vittu vazha thonala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai vittu vazha thonala"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhu kuthala naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhu kuthala naa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kitta vandhu kelu thevala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kitta vandhu kelu thevala"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nadandhuchu manasukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nadandhuchu manasukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandakkaari vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakkaari vaadi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai adakki aaluren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai adakki aaluren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty potta poonai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty potta poonai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam adikka yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam adikka yenguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Daalu daaleela daalu daalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daalu daaleela daalu daalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Daalu daaleela daalu daalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daalu daaleela daalu daalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Daalu daaleela daalu daalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daalu daaleela daalu daalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Daalu daaleela daalu daalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daalu daaleela daalu daalu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Jin jina kitta jinakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jin jina kitta jinakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jin jina kitta jinakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jin jina kitta jinakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jin jina kitta jinakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jin jina kitta jinakitta"/>
</div>
<div class="lyrico-lyrics-wrapper">Jin jina kitta jinakitta
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Jin jina kitta jinakitta"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haahaaa haaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haahaaa haaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Andharathil oonjal poottu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Andharathil oonjal poottu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai nee konjanumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai nee konjanumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kichi kichi mootti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kichi kichi mootti"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan un kannatha killanumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan un kannatha killanumae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Etti ninnu neeyumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Etti ninnu neeyumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Seivadhenna bhavanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seivadhenna bhavanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Mandhiricha kozhiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mandhiricha kozhiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vetta enna yosanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vetta enna yosanaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kozhi engirindhu vanthathinnu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kozhi engirindhu vanthathinnu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kekkum oorula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kekkum oorula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaadhal vandha vazhi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaadhal vandha vazhi"/>
</div>
<div class="lyrico-lyrics-wrapper">Solla ingu yaarumae illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Solla ingu yaarumae illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nadandhuchu manasukkulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nadandhuchu manasukkulla"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandakkaari vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakkaari vaadi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai adakki aaluren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai adakki aaluren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty potta poonai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty potta poonai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam adikka yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam adikka yenguren"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ettu vachu neeyum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu vachu neeyum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pona sokkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pona sokkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ettu patti raasa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ettu patti raasa"/>
</div>
<div class="lyrico-lyrics-wrapper">Pechum thikkuthadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pechum thikkuthadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ottiyirukka otthukkadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ottiyirukka otthukkadi"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sandakkaari vaadi vaadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandakkaari vaadi vaadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai adakki aaluren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai adakki aaluren"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutty potta poonai pola
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutty potta poonai pola"/>
</div>
<div class="lyrico-lyrics-wrapper">Vattam adikka yenguren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vattam adikka yenguren"/>
</div>
</pre>
