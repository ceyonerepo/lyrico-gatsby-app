---
title: "aadi paade song lyrics"
album: "Dorasaani"
artist: "Prashanth R Vihari"
lyricist: "Ramajogayya Sastri"
director: "KVR Mahendra"
path: "/albums/dorasaani-lyrics"
song: "Aadi Paade"
image: ../../images/albumart/dorasaani.jpg
date: 2019-07-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/0Qnu92YnjcM"
type: "sad"
singers:
  - Lokeshwar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">aadi paade kota gadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aadi paade kota gadi"/>
</div>
<div class="lyrico-lyrics-wrapper">bisipoye kala maari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="bisipoye kala maari"/>
</div>
<div class="lyrico-lyrics-wrapper">odipoye veedipoye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="odipoye veedipoye"/>
</div>
<div class="lyrico-lyrics-wrapper">sambaraalu pedhavanchu jaari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sambaraalu pedhavanchu jaari"/>
</div>
<div class="lyrico-lyrics-wrapper">tharalipove tharalipove 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="tharalipove tharalipove "/>
</div>
<div class="lyrico-lyrics-wrapper">thallileni dorasaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thallileni dorasaani"/>
</div>
<div class="lyrico-lyrics-wrapper">mannu minnu ekamainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="mannu minnu ekamainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">maarchaleve ee nijaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="maarchaleve ee nijaanni"/>
</div>
<div class="lyrico-lyrics-wrapper">upiraage baadhe ayina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="upiraage baadhe ayina"/>
</div>
<div class="lyrico-lyrics-wrapper">orchukove sukumaaree
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="orchukove sukumaaree"/>
</div>
<div class="lyrico-lyrics-wrapper">dhaachukunna guruthulanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhaachukunna guruthulanni"/>
</div>
<div class="lyrico-lyrics-wrapper">chusiraave kadasari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chusiraave kadasari"/>
</div>
<div class="lyrico-lyrics-wrapper">chinnapranam peddha kastam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="chinnapranam peddha kastam"/>
</div>
<div class="lyrico-lyrics-wrapper">thenchalevee panjaraanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenchalevee panjaraanni"/>
</div>
<div class="lyrico-lyrics-wrapper">kannu podiche mulla theeru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kannu podiche mulla theeru"/>
</div>
<div class="lyrico-lyrics-wrapper">musirinaaye kanneellannee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="musirinaaye kanneellannee"/>
</div>
<div class="lyrico-lyrics-wrapper">urukove urukove apalevu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urukove urukove apalevu"/>
</div>
<div class="lyrico-lyrics-wrapper">nuvvee kshanaanni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="nuvvee kshanaanni"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">thenchukunte theeripodhe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thenchukunte theeripodhe"/>
</div>
<div class="lyrico-lyrics-wrapper">penchukunna prema bandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="penchukunna prema bandham"/>
</div>
<div class="lyrico-lyrics-wrapper">urikainaa kannorikainaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="urikainaa kannorikainaa"/>
</div>
<div class="lyrico-lyrics-wrapper">pattadhanta nee aanandham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattadhanta nee aanandham"/>
</div>
<div class="lyrico-lyrics-wrapper">pattubatti evare gani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pattubatti evare gani"/>
</div>
<div class="lyrico-lyrics-wrapper">cherapaleru neelo vaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="cherapaleru neelo vaani"/>
</div>
<div class="lyrico-lyrics-wrapper">latha prema sangathulannee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="latha prema sangathulannee"/>
</div>
<div class="lyrico-lyrics-wrapper">gnapakaalai netho ranee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="gnapakaalai netho ranee"/>
</div>
<div class="lyrico-lyrics-wrapper">edabatai ningiki egase
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edabatai ningiki egase"/>
</div>
<div class="lyrico-lyrics-wrapper">edhamantalu challaarelaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="edhamantalu challaarelaa"/>
</div>
<div class="lyrico-lyrics-wrapper">dhingamingave neelo nuvve
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhingamingave neelo nuvve"/>
</div>
<div class="lyrico-lyrics-wrapper">kanneelannee padamaralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanneelannee padamaralo"/>
</div>
<div class="lyrico-lyrics-wrapper">vekuva raanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vekuva raanee"/>
</div>
<div class="lyrico-lyrics-wrapper">thalarathalu maaravugaanee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thalarathalu maaravugaanee"/>
</div>
<div class="lyrico-lyrics-wrapper">sarlemmani neelo manase 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sarlemmani neelo manase "/>
</div>
<div class="lyrico-lyrics-wrapper">sardukuponee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sardukuponee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">ooru dhaati kanneru dhaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru dhaati kanneru dhaati"/>
</div>
<div class="lyrico-lyrics-wrapper">saagipove dorasaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saagipove dorasaani"/>
</div>
<div class="lyrico-lyrics-wrapper">kanche dhaati anchu dhaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kanche dhaati anchu dhaati"/>
</div>
<div class="lyrico-lyrics-wrapper">kadhilipove dorasaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadhilipove dorasaani"/>
</div>
<div class="lyrico-lyrics-wrapper">ooru dhaati kanneru dhaati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ooru dhaati kanneru dhaati"/>
</div>
<div class="lyrico-lyrics-wrapper">saagipove dorasaani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="saagipove dorasaani"/>
</div>
</pre>
