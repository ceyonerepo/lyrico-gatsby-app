---
title: "iravu pagalai theda song lyrics"
album: "Kannukkul Nilavu"
artist: "Ilaiyaraaja"
lyricist: "Palani Bharathi"
director: "Fazil"
path: "/albums/kannukkul-nilavu-lyrics"
song: "Iravu Pagalai Theda"
image: ../../images/albumart/kannukkul-nilavu.jpg
date: 2000-01-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/LCIbClMwO5E"
type: "intro"
singers:
  - K. J. Yesudas
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Iravu Pagalai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Pagalai Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Ondrai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Ondrai Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigal Amaithi Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigal Amaithi Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal Vazhiyai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Vazhiyai Theda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu Pagalai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Pagalai Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Ondrai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Ondrai Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigal Amaithi Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigal Amaithi Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal Vazhiyai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Vazhiyai Theda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutrukindrathe Thendral Dhinam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutrukindrathe Thendral Dhinam Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Manathai Konjam Sumakkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Manathai Konjam Sumakkumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vimmukindrathe Vinnil Natchathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vimmukindrathe Vinnil Natchathiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Kanavai Solli Azhaikkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Kanavai Solli Azhaikkumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Achacho Achacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achacho Achacho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu Pagalai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Pagalai Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Ondrai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Ondrai Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigal Amaithi Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigal Amaithi Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal Vazhiyai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Vazhiyai Theda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vaazhkai Ennum Payanam Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaazhkai Ennum Payanam Inge"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhooram Dhooram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhooram Dhooram"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Maarum Enge Serum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Maarum Enge Serum"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollum Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollum Kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thendral Vanthu Pookkal Aadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thendral Vanthu Pookkal Aadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athuvoru Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athuvoru Kaalam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mannil Sinthi Pookal Vaadum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mannil Sinthi Pookal Vaadum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ilaiyuthir Kaalam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ilaiyuthir Kaalam"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kolangal Aadum Vaasalgal Vendum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kolangal Aadum Vaasalgal Vendum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thaniyaaga Azhagillaiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thaniyaaga Azhagillaiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadalai Seraa Nadhiyai Kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadalai Seraa Nadhiyai Kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyil Aadum Meenai Kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyil Aadum Meenai Kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Otrai Kuyilin Sogam Kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Otrai Kuyilin Sogam Kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Achachcho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achachcho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu Pagalai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Pagalai Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Ondrai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Ondrai Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigal Amaithi Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigal Amaithi Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal Vazhiyai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Vazhiyai Theda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veesum Kaatru Oivai Thedi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veesum Kaatru Oivai Thedi"/>
</div>
<div class="lyrico-lyrics-wrapper">Enge Pogum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enge Pogum"/>
</div>
<div class="lyrico-lyrics-wrapper">Pookal Pesa Vaai Irunthal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pookal Pesa Vaai Irunthal"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna Pesum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna Pesum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maalai Neram Paravai Kootam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maalai Neram Paravai Kootam"/>
</div>
<div class="lyrico-lyrics-wrapper">Koottai Thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koottai Thedum"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravi Ponaal Paravai Koodu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravi Ponaal Paravai Koodu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarai Thedum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarai Thedum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nadodi Megam Ododi Inge
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadodi Megam Ododi Inge"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaarodu Uravaadumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaarodu Uravaadumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Annaiyillaa Pillai Kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annaiyillaa Pillai Kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Pillailaa Annai Kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pillailaa Annai Kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Anbeillaa Ulagam Kandaal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Anbeillaa Ulagam Kandaal"/>
</div>
<div class="lyrico-lyrics-wrapper">Achachcho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achachcho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu Pagalai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Pagalai Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Ondrai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Ondrai Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigal Amaithi Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigal Amaithi Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal Vazhiyai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Vazhiyai Theda"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sutrukindrathe Thendral Dhinam Dhinam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sutrukindrathe Thendral Dhinam Dhinam"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Manathai Konjam Sumakkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Manathai Konjam Sumakkumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Vimmukindrathe Vinnil Natchathiram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vimmukindrathe Vinnil Natchathiram"/>
</div>
<div class="lyrico-lyrics-wrapper">Enthan Kanavai Solli Azhaikkumo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enthan Kanavai Solli Azhaikkumo"/>
</div>
<div class="lyrico-lyrics-wrapper">Achachoo Achacho
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achachoo Achacho"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Iravu Pagalai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravu Pagalai Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhayam Ondrai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhayam Ondrai Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Alaigal Amaithi Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Alaigal Amaithi Theda"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizhigal Vazhiyai Theda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizhigal Vazhiyai Theda"/>
</div>
</pre>
