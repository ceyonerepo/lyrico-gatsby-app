---
title: "paarthen song lyrics"
album: "Pa Paandi"
artist: "Sean Roldan"
lyricist: "Selvaraghavan"
director: "Dhanush"
path: "/albums/pa-paandi-lyrics"
song: "Paarthen"
image: ../../images/albumart/pa-paandi.jpg
date: 2017-04-14
lang: tamil
youtubeLink: "https://www.youtube.com/embed/4cogdBU5sLc"
type: "love"
singers:
  -	Sean Roldan
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavu pona nilava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavu pona nilava"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanjen"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna sugam saanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna sugam saanjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathu sillunu veesudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu sillunu veesudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal imbuttu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal imbuttu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaral sangathi kaatudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaral sangathi kaatudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Imbuttu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Imbuttu thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idi minnal adikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi minnal adikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Velichathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichathula"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavu pona nilava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavu pona nilava"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanjen"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna sugam saanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna sugam saanjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thiruvizha onnu munnae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thiruvizha onnu munnae"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatchithaan kodukiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatchithaan kodukiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Eththana piravi thavamo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Eththana piravi thavamo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannu munna nadakkiradhae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannu munna nadakkiradhae"/>
</div>
<div class="lyrico-lyrics-wrapper">Tharaiyila kaalum illa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tharaiyila kaalum illa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanavula midhakuranae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavula midhakuranae"/>
</div>
<div class="lyrico-lyrics-wrapper">Mazhaiyila mannin vaasam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mazhaiyila mannin vaasam"/>
</div>
<div class="lyrico-lyrics-wrapper">Mayangi poi kedakkurenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mayangi poi kedakkurenae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Venduna saami ellaam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Venduna saami ellaam"/>
</div>
<div class="lyrico-lyrics-wrapper">Varama thandha thunai neethaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varama thandha thunai neethaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nenjukuli thavikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nenjukuli thavikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagae unna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagae unna"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthen paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthen paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Saanjen saanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanjen saanjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paarthen"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalavu pona nilava
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalavu pona nilava"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan paarthen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan paarthen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Saanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saanjen"/>
</div>
<div class="lyrico-lyrics-wrapper">En nenjukulla
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En nenjukulla"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna sugam saanjen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna sugam saanjen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaathu sillunu veesudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaathu sillunu veesudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal imbuttu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal imbuttu thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Saaral sangathi kaatudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saaral sangathi kaatudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal Imbuttu thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal Imbuttu thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idi minnal adikkudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idi minnal adikkudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Velichathula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velichathula"/>
</div>
</pre>
