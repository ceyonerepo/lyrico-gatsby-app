---
title: "pogaadhey kanmaniye song lyrics"
album: "Veera"
artist: "Leon James"
lyricist: "Na. Muthu Kumar"
director: "Rajaraman"
path: "/albums/veera-lyrics"
song: "Pogaadhey Kanmaniye"
image: ../../images/albumart/veera.jpg
date: 2018-02-16
lang: tamil
youtubeLink: "https://www.youtube.com/embed/O7WXcDeO7L0"
type: "love"
singers:
  - Pradeep Kumar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Unna naan pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna naan pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Usurodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurodaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkooda sirichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda sirichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa rasichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa rasichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nizhalukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nizhalukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koda pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koda pudichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna naan pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna naan pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Usurodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurodaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkooda sirichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda sirichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa rasichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa rasichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nizhalukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nizhalukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koda pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koda pudichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogaathae kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathae kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaathae kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathae kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaathae kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathae kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pona ver engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pona ver engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan povadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan povadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un pechum un moochum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pechum un moochum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalum en nenja vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum en nenja vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaathae kannae aee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathae kannae aee"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaathae kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathae kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaathae kanmaniyae aee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathae kanmaniyae aee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna naan pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna naan pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Usurodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurodaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkooda sirichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda sirichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa rasichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa rasichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nizhalukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nizhalukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koda pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koda pudichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna naan pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna naan pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Usurodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurodaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkooda sirichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda sirichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa rasichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa rasichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nizhalukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nizhalukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koda pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koda pudichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ponnae en kooda va
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ponnae en kooda va"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannil onna poththi kaapen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannil onna poththi kaapen"/>
</div>
<div class="lyrico-lyrics-wrapper">Enna nee paarkum nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enna nee paarkum nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Podhum vera enna keppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Podhum vera enna keppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Urangaama urangaama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Urangaama urangaama"/>
</div>
<div class="lyrico-lyrics-wrapper">Un azhaga rasippenae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un azhaga rasippenae"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa en madiyil saaya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa en madiyil saaya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaa peranbil thoya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaa peranbil thoya"/>
</div>
<div class="lyrico-lyrics-wrapper">Unai enai inaithadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unai enai inaithadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanuvugal thaanae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanuvugal thaanae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogaathae kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathae kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaathae kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathae kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaathae kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathae kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee pona ver engae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee pona ver engae"/>
</div>
<div class="lyrico-lyrics-wrapper">Naan povadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naan povadhu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un pechum un moochum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pechum un moochum"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennaalum en nenja vittu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennaalum en nenja vittu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaathae kannae aee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathae kannae aee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Pogaathae kanmaniyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathae kanmaniyae"/>
</div>
<div class="lyrico-lyrics-wrapper">Pogaathae kanmaniyae aee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pogaathae kanmaniyae aee"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaee aeeeee aee aeeeee
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaee aeeeee aee aeeeee"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna naan pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna naan pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Usurodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurodaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkooda sirichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda sirichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa rasichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa rasichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nizhalukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nizhalukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koda pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koda pudichen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unna naan pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unna naan pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa pudichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Usurodaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Usurodaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Unkooda sirichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unkooda sirichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Naa rasichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naa rasichen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nizhalukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nizhalukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Koda pudichen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Koda pudichen"/>
</div>
</pre>
