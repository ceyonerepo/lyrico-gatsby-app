---
title: "maayangal naanada song lyrics"
album: "Ratsasan"
artist: "Ghibran"
lyricist: "GKB"
director: "Ram Kumar"
path: "/albums/ratsasan-lyrics"
song: "Maayangal Naanada"
image: ../../images/albumart/ratsasan.jpg
date: 2018-10-05
lang: tamil
youtubeLink: "https://www.youtube.com/embed/FoSEaG26070"
type: "theme song"
singers:
  - Yazin Nizar
  - Ranjth
  - Shabir
  - Shenbagaraj
  - Narayanan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Hahahahahaha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Hahahahahaha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee ennai pudichalum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee ennai pudichalum"/>
</div>
<div class="lyrico-lyrics-wrapper">Athu naana irukka matten
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athu naana irukka matten"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Maayangal naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maayangal naanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam thinnum maanida
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam thinnum maanida"/>
</div>
<div class="lyrico-lyrics-wrapper">Katchi pizhai naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katchi pizhai naanada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kandavar yaar koorada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kandavar yaar koorada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Oduthal yenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oduthal yenada"/>
</div>
<div class="lyrico-lyrics-wrapper">Theduthal veenada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theduthal veenada"/>
</div>
<div class="lyrico-lyrics-wrapper">Arugilae naanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Arugilae naanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Ariyavillai neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ariyavillai neeyada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un kangalil naan enai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kangalil naan enai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kangiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kangiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Un manathilae bayam vithai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un manathilae bayam vithai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thoovinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thoovinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Bommaigalaai naan unai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bommaigalaai naan unai"/>
</div>
<div class="lyrico-lyrics-wrapper">Maatrinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maatrinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Nool mattum en kaigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nool mattum en kaigalil"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallavan endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallavan endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai mattum nambum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai mattum nambum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukullae oruvan sirikkiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukullae oruvan sirikkiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanae ratsasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanae ratsasan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaikkum unakkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaikkum unakkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna idaiveliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna idaiveliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutram sayil kondurikkiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram sayil kondurikkiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha ratsasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ratsasan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallavan endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallavan endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai mattum nambum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai mattum nambum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukullae oruvan sirikkiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukullae oruvan sirikkiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanae ratsasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanae ratsasan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaikkum unakkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaikkum unakkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna idaiveliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna idaiveliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutram sayil kondurikkiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram sayil kondurikkiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha ratsasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ratsasan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ratsasan neeyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ratsasan neeyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Ragasiyangal naanda
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ragasiyangal naanda"/>
</div>
<div class="lyrico-lyrics-wrapper">Nerunginen unarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nerunginen unarada"/>
</div>
<div class="lyrico-lyrics-wrapper">Vettai mudiyum paarada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vettai mudiyum paarada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Satchigal poiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Satchigal poiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Saathiyangal meiyada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Saathiyangal meiyada"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kannil bayamada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kannil bayamada"/>
</div>
<div class="lyrico-lyrics-wrapper">Maranam tharuven naanada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Maranam tharuven naanada"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Un idhayam en kaigalil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un idhayam en kaigalil"/>
</div>
<div class="lyrico-lyrics-wrapper">Ezhuthinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ezhuthinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Athyayangal thinam athai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athyayangal thinam athai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kootinen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kootinen"/>
</div>
<div class="lyrico-lyrics-wrapper">Un pola naan illaiyae vizhikiren
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un pola naan illaiyae vizhikiren"/>
</div>
<div class="lyrico-lyrics-wrapper">Ethileyum samharamae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ethileyum samharamae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ohooonallavan endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ohooonallavan endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai mattum nambum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai mattum nambum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukullae oruvan sirikkiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukullae oruvan sirikkiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanae ratsasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanae ratsasan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaikkum unakkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaikkum unakkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna idaiveliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna idaiveliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutram sayil kondurikkiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram sayil kondurikkiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha ratsasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ratsasan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Nallavan endru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nallavan endru"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnai mattum nambum bothu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnai mattum nambum bothu"/>
</div>
<div class="lyrico-lyrics-wrapper">Ullukullae oruvan sirikkiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ullukullae oruvan sirikkiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Avanae ratsasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Avanae ratsasan"/>
</div>
<div class="lyrico-lyrics-wrapper">Unmaikkum unakkumae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unmaikkum unakkumae"/>
</div>
<div class="lyrico-lyrics-wrapper">Chinna idaiveliyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chinna idaiveliyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Kutram sayil kondurikkiran
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kutram sayil kondurikkiran"/>
</div>
<div class="lyrico-lyrics-wrapper">Intha ratsasan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Intha ratsasan"/>
</div>
</pre>
