---
title: 'vaanamai naan song lyrics'
album: 'Pon Magal Vandhal'
artist: 'Govind Vasantha'
lyricist: 'Uma Devi'
director: 'J J Fredrick'
path: '/albums/pon-magal-vandhal-song-lyrics'
song: 'Vaanamai Naan Aagiye'
image: ../../images/albumart/ponmagal-vandhal.jpg
date: 2020-05-29
lang: tamil
youtubeLink: "https://www.youtube.com/embed/scE1TAx9Ykk"
type: 'affection'
singers: 
- Saindhavi
- Govind Vasantha
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">

<div class="lyrico-lyrics-wrapper">Vaanamai Naan Aagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaanamai Naan Aagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru Vaanai Theendi Naanum Kaappen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru Vaanai Theendi Naanum Kaappen"/>
</div>
<div class="lyrico-lyrics-wrapper">Thozhi Pol Naan Aagiye
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thozhi Pol Naan Aagiye"/>
</div>
<div class="lyrico-lyrics-wrapper">Un Thevai Thedi Thedi Serppen
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un Thevai Thedi Thedi Serppen"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Therilla Silai Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Therilla Silai Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Neer Illa Niram Naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neer Illa Niram Naan"/>
</div>
<div class="lyrico-lyrics-wrapper">Seriya Idhu Poiya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Seriya Idhu Poiya"/>
</div>
<div class="lyrico-lyrics-wrapper">Ingu Edhu Naan Anbe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ingu Edhu Naan Anbe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Marainthe Pookuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Marainthe Pookuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Niraintha Poodume Unatha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Niraintha Poodume Unatha"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee Kidaithai Maganai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee Kidaithai Maganai"/>
</div>
</pre>