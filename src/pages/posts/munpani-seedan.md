---
title: "munpani song lyrics"
album: "Seedan"
artist: "Dhina"
lyricist: "Pa. Vijay"
director: "Subramaniam Siva"
path: "/albums/seedan-lyrics"
song: "Munpani"
image: ../../images/albumart/seedan.jpg
date: 2011-02-25
lang: tamil
youtubeLink: "https://www.youtube.com/embed/QtznOPsVFVI"
type: "happy"
singers:
  - Shreya Ghoshal
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Sandhee dhee dhee ra nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhee dhee dhee ra nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhee dhee dhee ra nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhee dhee dhee ra nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeraananaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraananaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheeraanananaaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheeraanananaaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhee dhee dhee ra nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhee dhee dhee ra nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sandhee dhee dhee ra nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sandhee dhee dhee ra nana"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munpani kaala poovilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munpani kaala poovilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan poo mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan poo mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal veiyil saaralil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal veiyil saaralil"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan maaniram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan maaniram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho sindhudhu nadhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho sindhudhu nadhigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivakkudhu mugilgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivakkudhu mugilgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvadhu nee thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvadhu nee thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyilae konjam sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilae konjam sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munpani kaala poovilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munpani kaala poovilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan poo mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan poo mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal veiyil saaralil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal veiyil saaralil"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan maaniram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan maaniram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Sa sa paa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sa sa paa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dha dha ma
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dha dha ma"/>
</div>
<div class="lyrico-lyrics-wrapper">Pa paa rii
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pa paa rii"/>
</div>
<div class="lyrico-lyrics-wrapper">Ga ga saa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ga ga saa"/>
</div>
<div class="lyrico-lyrics-wrapper">Chum chum chum chum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chum chum chum chum"/>
</div>
<div class="lyrico-lyrics-wrapper">Chum chum chum chum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chum chum chum chum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa pachchai pachcha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa pachchai pachcha"/>
</div>
<div class="lyrico-lyrics-wrapper">Pullin nuniyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pullin nuniyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Paniyena padarudhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paniyena padarudhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un peyarthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un peyarthaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Chum chum chum chum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chum chum chum chum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho ichchai ichchai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho ichchai ichchai"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuruviyin siraginil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuruviyin siraginil"/>
</div>
<div class="lyrico-lyrics-wrapper">Iragena iruppadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iragena iruppadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un imaithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un imaithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Chum chum chum chum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chum chum chum chum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho varudi varudi ullam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho varudi varudi ullam"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirudi thirudi chellum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirudi thirudi chellum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vasandha kaatralaiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vasandha kaatralaiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un viral thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un viral thaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Midhandhu midhandhu manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Midhandhu midhandhu manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalandhu kalandhu varum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalandhu kalandhu varum"/>
</div>
<div class="lyrico-lyrics-wrapper">Iniya isaiyil svaram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iniya isaiyil svaram"/>
</div>
<div class="lyrico-lyrics-wrapper">Un kural thaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un kural thaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Iravum pagalum neeyae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Iravum pagalum neeyae"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munpani kaala poovilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munpani kaala poovilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan poo mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan poo mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal veiyil saaralil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal veiyil saaralil"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan maaniram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan maaniram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dhum thaarae dhum thaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum thaarae dhum thaarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum thaa dhum thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum thaa dhum thaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum thaarae dhum thaarae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum thaarae dhum thaarae"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhum thaa dhum thaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhum thaa dhum thaa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Dheranana nana dheranana nana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheranana nana dheranana nana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheranana nana dheranana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheranana nana dheranana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheranana nana dheranana nana nana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheranana nana dheranana nana nana"/>
</div>
<div class="lyrico-lyrics-wrapper">Dheranana nana dhera nana naa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dheranana nana dhera nana naa"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Haa vaanil thirandha
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Haa vaanil thirandha"/>
</div>
<div class="lyrico-lyrics-wrapper">Vatta chimizhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vatta chimizhaai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thirigira nilavil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thirigira nilavil"/>
</div>
<div class="lyrico-lyrics-wrapper">Un mugamthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un mugamthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Veettil yettriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Veettil yettriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Vilakkin thiriyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vilakkin thiriyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Sudarena sudarvadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sudarena sudarvadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Un nizhalthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un nizhalthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho enadhu enadhu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho enadhu enadhu"/>
</div>
<div class="lyrico-lyrics-wrapper">Siru araiyil araiyil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Siru araiyil araiyil"/>
</div>
<div class="lyrico-lyrics-wrapper">Dhinam irukkum irukkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dhinam irukkum irukkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Thunai un uruthaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thunai un uruthaan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Edhilum edhilum undhan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Edhilum edhilum undhan"/>
</div>
<div class="lyrico-lyrics-wrapper">Uruvam uruvam kanden
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Uruvam uruvam kanden"/>
</div>
<div class="lyrico-lyrics-wrapper">Neril paarppadheppo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Neril paarppadheppo"/>
</div>
<div class="lyrico-lyrics-wrapper">En vizhithaan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="En vizhithaan"/>
</div>
<div class="lyrico-lyrics-wrapper">Azhagaa neeyae sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Azhagaa neeyae sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munpani kaala poovilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munpani kaala poovilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan poo mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan poo mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal veiyil saaralil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal veiyil saaralil"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan maaniram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan maaniram"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ho sindhudhu nadhigal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ho sindhudhu nadhigal"/>
</div>
<div class="lyrico-lyrics-wrapper">Sivakkudhu mugilgal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sivakkudhu mugilgal"/>
</div>
<div class="lyrico-lyrics-wrapper">Varuvadhu nee thaanaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Varuvadhu nee thaanaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuyilae konjam sollu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuyilae konjam sollu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Munpani kaala poovilae
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Munpani kaala poovilae"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan poo mugam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan poo mugam"/>
</div>
<div class="lyrico-lyrics-wrapper">Manjal veiyil saaralil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Manjal veiyil saaralil"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan maaniram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan maaniram"/>
</div>
<div class="lyrico-lyrics-wrapper">Undhan maaniram
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Undhan maaniram"/>
</div>
</pre>
