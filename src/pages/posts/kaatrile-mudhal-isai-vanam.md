---
title: "kaatrile mudhal isai song lyrics"
album: "Vanam"
artist: "Ron Ethan Yohann"
lyricist: "S. Gnanakaravel"
director: "Srikantan Anand"
path: "/albums/vanam-lyrics"
song: "Kaatrile Mudhal Isai"
image: ../../images/albumart/vanam.jpg
date: 2021-11-26
lang: tamil
youtubeLink: "https://www.youtube.com/embed/Ur_lkogkpCs"
type: "melody"
singers:
  - Ron Ethan Yohann
  - Shweta Mohan
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kaatrile mudhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrile mudhal "/>
</div>
<div class="lyrico-lyrics-wrapper">isai vazhinthoduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isai vazhinthoduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal devathai isai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal devathai isai "/>
</div>
<div class="lyrico-lyrics-wrapper">meettum neramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meettum neramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Ninaithathum manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithathum manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthidum kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthidum kadhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaana tharagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaana tharagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral meettum thooramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral meettum thooramo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kannadiyai idhayam viriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadiyai idhayam viriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal mugam malarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal mugam malarume"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollamale piriyum tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamale piriyum tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerum theane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerum theane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal ennum siragal ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal ennum siragal ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamellam parakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamellam parakkume"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar varum bodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar varum bodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Yaar sollum bodhum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yaar sollum bodhum"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaasalgal moodathu kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaasalgal moodathu kadhal"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaalathin kannadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalathin kannadi"/>
</div>
<div class="lyrico-lyrics-wrapper">Un roobamai vanthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Un roobamai vanthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Paalyaththai mun vaikkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paalyaththai mun vaikkuthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Nil endru sonnalume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nil endru sonnalume"/>
</div>
<div class="lyrico-lyrics-wrapper">Thimirudan nenjam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thimirudan nenjam"/>
</div>
<div class="lyrico-lyrics-wrapper">Unnodu pin selluthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Unnodu pin selluthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kenjalai konjalai unnai ketkum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kenjalai konjalai unnai ketkum"/>
</div>
<div class="lyrico-lyrics-wrapper">Kodhai nizhal kodi muththam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kodhai nizhal kodi muththam"/>
</div>
<div class="lyrico-lyrics-wrapper">Oonjalai aattiye un ennam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oonjalai aattiye un ennam"/>
</div>
<div class="lyrico-lyrics-wrapper">Ennai thaalaattuthe innum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ennai thaalaattuthe innum"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kaana kaanum manam thedum 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaana kaanum manam thedum "/>
</div>
<div class="lyrico-lyrics-wrapper">unthan kadhal madi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unthan kadhal madi"/>
</div>
<div class="lyrico-lyrics-wrapper">Vizha kolam manam poonam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vizha kolam manam poonam "/>
</div>
<div class="lyrico-lyrics-wrapper">unnai kaanum nodi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnai kaanum nodi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrile mudhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrile mudhal "/>
</div>
<div class="lyrico-lyrics-wrapper">isai vazhinthoduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isai vazhinthoduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal devathai isai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal devathai isai "/>
</div>
<div class="lyrico-lyrics-wrapper">meettum neramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meettum neramo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Engo nee vaazhnthaye thoorathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Engo nee vaazhnthaye thoorathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Angum vaazhnthene oorathile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Angum vaazhnthene oorathile"/>
</div>
<div class="lyrico-lyrics-wrapper">Swasathai neegatha un ninaivugal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Swasathai neegatha un ninaivugal"/>
</div>
<div class="lyrico-lyrics-wrapper">Indru nenjodu paal vaarkuthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Indru nenjodu paal vaarkuthe"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Natpile vittathe meendum indru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Natpile vittathe meendum indru"/>
</div>
<div class="lyrico-lyrics-wrapper">Nee korkkirai kadhalale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nee korkkirai kadhalale"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhalum thaaimayum verillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhalum thaaimayum verillai"/>
</div>
<div class="lyrico-lyrics-wrapper">Endru unnale kandene
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Endru unnale kandene"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Athe paavai athe paarvai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athe paavai athe paarvai "/>
</div>
<div class="lyrico-lyrics-wrapper">meendum en munnile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum en munnile"/>
</div>
<div class="lyrico-lyrics-wrapper">Athe kadhal athe thedal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Athe kadhal athe thedal "/>
</div>
<div class="lyrico-lyrics-wrapper">meendum un kannile
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meendum un kannile"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaatrile mudhal 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaatrile mudhal "/>
</div>
<div class="lyrico-lyrics-wrapper">isai vazhinthoduthe
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="isai vazhinthoduthe"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal devathai isai 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal devathai isai "/>
</div>
<div class="lyrico-lyrics-wrapper">meettum neramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="meettum neramo"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ninaithathum manam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ninaithathum manam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paranthidum kadhale
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paranthidum kadhale"/>
</div>
<div class="lyrico-lyrics-wrapper">Vaana tharagai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vaana tharagai"/>
</div>
<div class="lyrico-lyrics-wrapper">Viral meettum thooramo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Viral meettum thooramo"/>
</div>
<div class="lyrico-lyrics-wrapper">Kannadiyai idhayam viriya
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kannadiyai idhayam viriya"/>
</div>
<div class="lyrico-lyrics-wrapper">Kadhal mugam malarume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal mugam malarume"/>
</div>
<div class="lyrico-lyrics-wrapper">Sollamale piriyum tharunam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sollamale piriyum tharunam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kanneerum theane
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanneerum theane"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kadhal ennum siragal ulagam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kadhal ennum siragal ulagam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kaalamellam parakkume
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kaalamellam parakkume"/>
</div>
<div class="lyrico-lyrics-wrapper">Ovvoru naalum ovvoru thedal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ovvoru naalum ovvoru thedal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyathu theerathu kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyathu theerathu kadhal"/>
</div>
<div class="lyrico-lyrics-wrapper">Oyathu theerathu kadhal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Oyathu theerathu kadhal"/>
</div>
</pre>
