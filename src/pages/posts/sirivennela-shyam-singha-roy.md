---
title: "sirivennela song lyrics"
album: "Shyam Singha Roy"
artist: "Mickey J. Meyer"
lyricist: "Sirivennela Seetharama Sastry"
director: "Rahul Sankrityan"
path: "/albums/shyam-singha-roy-lyrics"
song: "Sirivennela"
image: ../../images/albumart/shyam-singha-roy.jpg
date: 2021-12-24
lang: telugu
youtubeLink: "https://www.youtube.com/embed/-19EvIcr9ZA"
type: "love"
singers:
  - Anurag Kulkarni
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Nela rajuni ila ranini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nela rajuni ila ranini"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalipindi kada sirivennela
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalipindi kada sirivennela"/>
</div>
<div class="lyrico-lyrics-wrapper">Doorama doorama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Doorama doorama"/>
</div>
<div class="lyrico-lyrics-wrapper">Theeramai cheruma nadi rathirilo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Theeramai cheruma nadi rathirilo"/>
</div>
<div class="lyrico-lyrics-wrapper">Teralu terichinadi niddhuralo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Teralu terichinadi niddhuralo"/>
</div>
<div class="lyrico-lyrics-wrapper">Magata marichi udhayinchinada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Magata marichi udhayinchinada"/>
</div>
<div class="lyrico-lyrics-wrapper">Kuluku loluku cheli modati kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kuluku loluku cheli modati kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Tana navvulalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tana navvulalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Taluku taluku tana chempalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taluku taluku tana chempalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chemaku chemaku tana muvvalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chemaku chemaku tana muvvalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaku janaku sari kottha kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaku janaku sari kottha kala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">O changure inthatidha naa siri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="O changure inthatidha naa siri"/>
</div>
<div class="lyrico-lyrics-wrapper">Annadi ee sharadha ratiri
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Annadi ee sharadha ratiri"/>
</div>
<div class="lyrico-lyrics-wrapper">Mila mila cheli kannula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mila mila cheli kannula"/>
</div>
<div class="lyrico-lyrics-wrapper">Tana kalalanu kanugoni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tana kalalanu kanugoni"/>
</div>
<div class="lyrico-lyrics-wrapper">Achheruvuna murisi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Achheruvuna murisi"/>
</div>
<div class="lyrico-lyrics-wrapper">Ayyaha enthatidi sundari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ayyaha enthatidi sundari"/>
</div>
<div class="lyrico-lyrics-wrapper">Evvaru raru kada tanasari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Evvaru raru kada tanasari"/>
</div>
<div class="lyrico-lyrics-wrapper">Srustike addham 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Srustike addham "/>
</div>
<div class="lyrico-lyrics-wrapper">choopaga puttinadhemo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choopaga puttinadhemo"/>
</div>
<div class="lyrico-lyrics-wrapper">Naari sukumari
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Naari sukumari"/>
</div>
<div class="lyrico-lyrics-wrapper">Idhi ningiki nelaki 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhi ningiki nelaki "/>
</div>
<div class="lyrico-lyrics-wrapper">jarigina parichayame
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="jarigina parichayame"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Tera daati chera daati
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tera daati chera daati"/>
</div>
<div class="lyrico-lyrics-wrapper">Velugu chusthunna bhamani
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Velugu chusthunna bhamani"/>
</div>
<div class="lyrico-lyrics-wrapper">Sarisaati edha meeti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sarisaati edha meeti"/>
</div>
<div class="lyrico-lyrics-wrapper">Palakaristhunna jamuni
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Palakaristhunna jamuni"/>
</div>
<div class="lyrico-lyrics-wrapper">Priyamara gamanisthu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Priyamara gamanisthu"/>
</div>
<div class="lyrico-lyrics-wrapper">Pulakaristhundi yamini
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pulakaristhundi yamini"/>
</div>
<div class="lyrico-lyrics-wrapper">Kalabosi oosule viraboose aashalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kalabosi oosule viraboose aashalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Navarathiri poosina vekuva rekalu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navarathiri poosina vekuva rekalu"/>
</div>
<div class="lyrico-lyrics-wrapper">Raasinadi navala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raasinadi navala"/>
</div>
<div class="lyrico-lyrics-wrapper">Mounale mamathalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mounale mamathalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhurala kavithalai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhurala kavithalai"/>
</div>
<div class="lyrico-lyrics-wrapper">Thudhi cherani kaburula
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thudhi cherani kaburula"/>
</div>
<div class="lyrico-lyrics-wrapper">Kathakali kadhilenu repati 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kathakali kadhilenu repati "/>
</div>
<div class="lyrico-lyrics-wrapper">kathalaku munnudila
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kathalaku munnudila"/>
</div>
<div class="lyrico-lyrics-wrapper">Tana navvulalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tana navvulalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Taluku taluku tana chempalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taluku taluku tana chempalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chemaku chemaku tana muvvalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chemaku chemaku tana muvvalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaku janaku sari kottha kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaku janaku sari kottha kala"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Idhila ani evaraina 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Idhila ani evaraina "/>
</div>
<div class="lyrico-lyrics-wrapper">choopane ledu kantiki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="choopane ledu kantiki"/>
</div>
<div class="lyrico-lyrics-wrapper">Adhelago tanakaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Adhelago tanakaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Tochene ledu maataki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tochene ledu maataki"/>
</div>
<div class="lyrico-lyrics-wrapper">Ippudippude manasaina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ippudippude manasaina"/>
</div>
<div class="lyrico-lyrics-wrapper">Repu dorikindi choopuki
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Repu dorikindi choopuki"/>
</div>
<div class="lyrico-lyrics-wrapper">Santhosham sarasana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Santhosham sarasana"/>
</div>
<div class="lyrico-lyrics-wrapper">Sankocham merisina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Sankocham merisina"/>
</div>
<div class="lyrico-lyrics-wrapper">Aa rentiki minchina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aa rentiki minchina"/>
</div>
<div class="lyrico-lyrics-wrapper">Paravasha leelalu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paravasha leelalu "/>
</div>
<div class="lyrico-lyrics-wrapper">kaadhani anagalama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaadhani anagalama"/>
</div>
<div class="lyrico-lyrics-wrapper">Katha kadhile varasana
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katha kadhile varasana"/>
</div>
<div class="lyrico-lyrics-wrapper">Tama edhalem tadisina
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tama edhalem tadisina"/>
</div>
<div class="lyrico-lyrics-wrapper">Gatha janmala poduvuna
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Gatha janmala poduvuna"/>
</div>
<div class="lyrico-lyrics-wrapper">Daachina daaham
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Daachina daaham"/>
</div>
<div class="lyrico-lyrics-wrapper">Ipude veeriki parichayama
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ipude veeriki parichayama"/>
</div>
<div class="lyrico-lyrics-wrapper">Tana navvulalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tana navvulalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Taluku taluku tana chempalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taluku taluku tana chempalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chemaku chemaku tana muvvalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chemaku chemaku tana muvvalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaku janaku sari kottha kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaku janaku sari kottha kala"/>
</div>
<div class="lyrico-lyrics-wrapper">Tana navvulalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Tana navvulalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Taluku taluku tana chempalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Taluku taluku tana chempalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Chemaku chemaku tana muvvalalo
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chemaku chemaku tana muvvalalo"/>
</div>
<div class="lyrico-lyrics-wrapper">Janaku janaku sari kottha kala
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Janaku janaku sari kottha kala"/>
</div>
</pre>
