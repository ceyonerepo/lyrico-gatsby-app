---
title: "raaja raaja mahaaraaja song lyrics"
album: "Maharaja"
artist: "D. Imman"
lyricist: "Yugabharathi"
director: "D. Manoharan"
path: "/albums/maharaja-lyrics"
song: "Raaja Raaja Mahaaraaja"
image: ../../images/albumart/maharaja.jpg
date: 2011-12-30
lang: tamil
youtubeLink: 
type: "mass"
singers:
  - Karthik
  - M.L.R. Karthikeyan
  - Solar Sai
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Raja Raja Magaaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaraja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagil ulladhai ellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil ulladhai ellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">ularacheidhida vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ularacheidhida vandhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaraja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Kanavil kandadhaiyellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kanavil kandadhaiyellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">kadavul poalavey thandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kadavul poalavey thandhaai"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaraja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Enadhu manadhiley ilaiya vayadhupoal
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Enadhu manadhiley ilaiya vayadhupoal"/>
</div>
<div class="lyrico-lyrics-wrapper">dhinamum siragaiye viritheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="dhinamum siragaiye viritheney"/>
</div>
<div class="lyrico-lyrics-wrapper">unadhu varugaiyaal ulagil marubadi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unadhu varugaiyaal ulagil marubadi"/>
</div>
<div class="lyrico-lyrics-wrapper">pudhiya piraviyum edutheney
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="pudhiya piraviyum edutheney"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaal unnaal aanen Ilaignan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaal unnaal aanen Ilaignan naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaraja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Paadhaalathil andru vizhundhu kidandhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paadhaalathil andru vizhundhu kidandhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">aagaayathil indru parakkirenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aagaayathil indru parakkirenadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">seesaavaipoal andru udaindhu kidandhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="seesaavaipoal andru udaindhu kidandhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">peesaavaiyum indru rusikkirenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="peesaavaiyum indru rusikkirenadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vaalibam unnidathil 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaalibam unnidathil "/>
</div>
<div class="lyrico-lyrics-wrapper">vandhadhendru vandhadhendru
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vandhadhendru vandhadhendru"/>
</div>
<div class="lyrico-lyrics-wrapper">vaazhvadhil ondrumillai neendhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vaazhvadhil ondrumillai neendhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">aayudham thathuvithu thathuvithu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aayudham thathuvithu thathuvithu"/>
</div>
<div class="lyrico-lyrics-wrapper">unnidathil unnidathil
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnidathil unnidathil"/>
</div>
<div class="lyrico-lyrics-wrapper">thoandrida kaaranam yaaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="thoandrida kaaranam yaaradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">un paadhaiyil sella kaal thaavidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un paadhaiyil sella kaal thaavidum"/>
</div>
<div class="lyrico-lyrics-wrapper">un peraiye solli vaai paadidum
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="un peraiye solli vaai paadidum"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaal unnaal aanen Ilaignan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaal unnaal aanen Ilaignan naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaaraja"/>
</div>
<div class="lyrico-lyrics-wrapper">Raja Raja Magaraja
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Raja Raja Magaraja"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Thanneerukkey andru thadhumbikkidandhavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Thanneerukkey andru thadhumbikkidandhavan"/>
</div>
<div class="lyrico-lyrics-wrapper">panneeriley indru kulikkirenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="panneeriley indru kulikkirenadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">sanniyaasi poal andru ulagai veruthavan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="sanniyaasi poal andru ulagai veruthavan"/>
</div>
<div class="lyrico-lyrics-wrapper">ullaasiyaai indru jolikkirenadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ullaasiyaai indru jolikkirenadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">boomiyil inbam undu thunbamillai thunbamillai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="boomiyil inbam undu thunbamillai thunbamillai"/>
</div>
<div class="lyrico-lyrics-wrapper">aasaikku ellai ingu yedhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="aasaikku ellai ingu yedhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaasikku kaavi katti sellugindra sellugindra
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaasikku kaavi katti sellugindra sellugindra"/>
</div>
<div class="lyrico-lyrics-wrapper">kaalathil ekkuthappaa paaradaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaalathil ekkuthappaa paaradaa"/>
</div>
<div class="lyrico-lyrics-wrapper">vedhaandhi poal vaai pesaadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="vedhaandhi poal vaai pesaadhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">kaattaaru naan nee poottaadhadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="kaattaaru naan nee poottaadhadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">unnaal unnaal aanen Ilaignan naan
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="unnaal unnaal aanen Ilaignan naan"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Ulagil ulladhai ellaam 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Ulagil ulladhai ellaam "/>
</div>
<div class="lyrico-lyrics-wrapper">ularacheidhida vandhaai
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="ularacheidhida vandhaai"/>
</div>
</pre>
