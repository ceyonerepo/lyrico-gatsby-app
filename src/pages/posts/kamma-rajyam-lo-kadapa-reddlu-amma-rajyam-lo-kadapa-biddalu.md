---
title: "kamma rajyam lo kadapa reddlu song lyrics"
album: "Amma Rajyam Lo Kadapa Biddalu"
artist: "Ravi Shankar"
lyricist: "Sirasri"
director: "Siddhartha Thatholu"
path: "/albums/amma-rajyam-lo-kadapa-biddalu-lyrics"
song: "Kamma Rajyam Lo Kadapa Reddlu"
image: ../../images/albumart/amma-rajyam-lo-kadapa-biddalu.jpg
date: 2019-12-12
lang: telugu
youtubeLink: "https://www.youtube.com/embed/HEa4aG-YBRE"
type: "title track"
singers:
  - Ravi Shankar
---

<pre class="lyrics-native">
</pre>

<pre class="lyrics-english">
<div class="lyrico-lyrics-wrapper">Kamma Rajyam Lo Kadapa Reddlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamma Rajyam Lo Kadapa Reddlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamma Rajyam Lo Kadapa Reddlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamma Rajyam Lo Kadapa Reddlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katthulu Levipudu Chinde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katthulu Levipudu Chinde "/>
</div>
<div class="lyrico-lyrics-wrapper">Netthuru Ledipudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netthuru Ledipudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuddam Chese Paddathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuddam Chese Paddathi "/>
</div>
<div class="lyrico-lyrics-wrapper">Mottham Maarindi Ipudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottham Maarindi Ipudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha Yuddam Idi Kottha Yuddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Yuddam Idi Kottha Yuddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha Yuddam Idi Kottha Yuddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Yuddam Idi Kottha Yuddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhyamame Dhalamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhyamame Dhalamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadiche Chattame Aayudam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiche Chattame Aayudam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruvu, Panamu, Pranalu Thodese Ranamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvu, Panamu, Pranalu Thodese Ranamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamma Rajyam Lo Kadapa Reddlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamma Rajyam Lo Kadapa Reddlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamma Rajyam Lo Kadapa Reddlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamma Rajyam Lo Kadapa Reddlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Navvuthu Vese Etthugada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Navvuthu Vese Etthugada"/>
</div>
<div class="lyrico-lyrics-wrapper">Chappudu Lenidi Ee Ragada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Chappudu Lenidi Ee Ragada"/>
</div>
<div class="lyrico-lyrics-wrapper">Prathyardhulaku Gunde Dhada
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Prathyardhulaku Gunde Dhada"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayataku Daari Ledikkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayataku Daari Ledikkadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Bayataku Daari Ledikkadaa
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Bayataku Daari Ledikkadaa"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha Yuddam Idi Kottha Yuddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Yuddam Idi Kottha Yuddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha Yuddam Idi Kottha Yuddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Yuddam Idi Kottha Yuddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamma Rajyam Lo Kadapa Reddlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamma Rajyam Lo Kadapa Reddlu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">C.B.I Lu Pilichi Vestharu Bheti
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="C.B.I Lu Pilichi Vestharu Bheti"/>
</div>
<div class="lyrico-lyrics-wrapper">Mental Torcheretti Laagutharu Koopi
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mental Torcheretti Laagutharu Koopi"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha Yuddam Idi Kottha Yuddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Yuddam Idi Kottha Yuddam"/>
</div>
<div class="lyrico-lyrics-wrapper">TV La Balamu Anchorla Madhamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="TV La Balamu Anchorla Madhamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Schemulesi Dinchi Scamulo Irikistharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Schemulesi Dinchi Scamulo Irikistharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamma Rajyam Lo Kadapa Reddlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamma Rajyam Lo Kadapa Reddlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katthulu Levipudu Chinde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katthulu Levipudu Chinde "/>
</div>
<div class="lyrico-lyrics-wrapper">Netthuru Ledipudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netthuru Ledipudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuddam Chese Paddathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuddam Chese Paddathi "/>
</div>
<div class="lyrico-lyrics-wrapper">Mottham Maarindi Ipudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottham Maarindi Ipudu"/>
</div>
<span class="lyrico-song-lyrics-breaker"></span>
<div class="lyrico-lyrics-wrapper">Vote Vese Varake Pourudu Raju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Vote Vese Varake Pourudu Raju"/>
</div>
<div class="lyrico-lyrics-wrapper">Electionla Varake Prajaswamya Moju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Electionla Varake Prajaswamya Moju"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha Yuddam Idi Kottha Yuddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Yuddam Idi Kottha Yuddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Padavi Vachinaaka Yele Vaadu Raju
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Padavi Vachinaaka Yele Vaadu Raju"/>
</div>
<div class="lyrico-lyrics-wrapper">Dongalantha Bediri Saranu Vedutharu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Dongalantha Bediri Saranu Vedutharu"/>
</div>
<div class="lyrico-lyrics-wrapper">Officerla Maarpu Gelichinodi Theerpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Officerla Maarpu Gelichinodi Theerpu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nayakule Bediri Partyla Jumpu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nayakule Bediri Partyla Jumpu"/>
</div>
<div class="lyrico-lyrics-wrapper">Leaderlu Antha Aadu Circussu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Leaderlu Antha Aadu Circussu"/>
</div>
<div class="lyrico-lyrics-wrapper">Aidella Daaka Janam Audience
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Aidella Daaka Janam Audience"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamma Rajyam Lo Kadapa Reddlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamma Rajyam Lo Kadapa Reddlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Katthulu Levipudu Chinde 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Katthulu Levipudu Chinde "/>
</div>
<div class="lyrico-lyrics-wrapper">Netthuru Ledipudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Netthuru Ledipudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Yuddam Chese Paddathi 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Yuddam Chese Paddathi "/>
</div>
<div class="lyrico-lyrics-wrapper">Mottham Maarindi Ipudu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Mottham Maarindi Ipudu"/>
</div>
<div class="lyrico-lyrics-wrapper">Madhyamame Dhalamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Madhyamame Dhalamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Nadiche Chattame Aayudam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Nadiche Chattame Aayudam"/>
</div>
<div class="lyrico-lyrics-wrapper">Paruvu Panamu 
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Paruvu Panamu "/>
</div>
<div class="lyrico-lyrics-wrapper">Pranalu Thodese Ranamu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Pranalu Thodese Ranamu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha Yuddam Idi Kottha Yuddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Yuddam Idi Kottha Yuddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamma Rajyam Lo Kadapa Reddlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamma Rajyam Lo Kadapa Reddlu"/>
</div>
<div class="lyrico-lyrics-wrapper">Kottha Yuddam Idi Kottha Yuddam
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kottha Yuddam Idi Kottha Yuddam"/>
</div>
<div class="lyrico-lyrics-wrapper">Kamma Rajyam Lo Kadapa Reddlu
<input type="checkbox" class="lyrico-select-lyric-line d-print-none" value="Kamma Rajyam Lo Kadapa Reddlu"/>
</div>
</pre>
